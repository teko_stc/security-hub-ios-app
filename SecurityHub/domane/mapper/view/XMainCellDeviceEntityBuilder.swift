//
//  XMainCellDeviceEntityBuilder.swift
//  SecurityHub
//
//  Created by Timerlan on 29.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit


class XMainCellDeviceEntityBuilder: XMainCellBaseEntityBuilder {
    static func create(device d: Devices) -> XMainCellDeviceEntityBuilder{
        let eb = XMainCellDeviceEntityBuilder(d)
        eb.ce.id = d.id
        eb.ce.title = d.name
        return eb
    }
    override func updateType(_ updateType: NUpdateType) -> XMainCellDeviceEntityBuilder {
        return super.updateType(updateType) as! XMainCellDeviceEntityBuilder
    }
    override func time(_ time: Int64) -> XMainCellDeviceEntityBuilder {
        return super.time(time) as! XMainCellDeviceEntityBuilder
    }
    override func siteName(_ name: String?) -> XMainCellDeviceEntityBuilder {
        return super.siteName(name) as! XMainCellDeviceEntityBuilder
    }
    func affects(affects: [DAffectEntity], image: String) -> XMainCellDeviceEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.site_alarm_big_name
        } else {
            ce.imageColor = DEFAULT_UNSELECTED_UINT
            ce.image = image
        }
        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    func status(_ status: DDeviceEntity.ArmStatus) -> XMainCellDeviceEntityBuilder {
        switch status {
        case .armed:
            ce.smallImage = XImages.site_armed_big_name
            ce.status = "arm_statement_titles[1]".localized()
        case .notFullArmed:
            ce.smallImage = XImages.site_c_armed_big_name
            ce.status = "arm_statement_titles[2]".localized()
        case .disarmed:
            ce.smallImage = XImages.site_disarmed_big_name
            ce.status = "arm_statement_titles[0]".localized()
        }
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Buttons
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func buttonCommands(arm: @escaping ((_ obj: Any, _ value: Any)->Void), disarm: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellDeviceEntityBuilder{
        ce.buttons.append(
            XObjectEntity.Button(title: "EA_ARMING".localized(),
                                  color: DEFAULT_COLOR_GREEN_UINT,
                                  obj: obj,
                                  value: "arm",
                                  void: arm)
        )
        ce.buttons.append(
            XObjectEntity.Button(title: "EA_DISARMING".localized(),
                                  color: DEFAULT_COLOR_RED_UINT,
                                  obj: obj,
                                  value: "disarm",
                                  void: disarm)
        )
        return self
    }
    func buttonRele(_ showReles: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        if (XTargetUtils.target != .myuvo)
        {
            ce.buttons.append(
                       XObjectEntity.Button(title: "RELAY".localized(),
                                            color: DEFAULT_COLOR_MAIN_UINT,
                                            obj: obj,
                                            value: "showRele",
                                            void: showReles)
                   )
        }
       
        return self
    }
    func buttonListHozorgan(_ list_hozorgan: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: "BUTTON_USERS".localized(),
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj: obj,
                                 value: "show",
                                 void: list_hozorgan)
        )
        return self
    }
    func buttonSection(_ showSections: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: "BUTTON_SECTIONS".localized(),
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj:   obj,
                                 value: "show",
                                 void:  showSections)
        )
        return self
    }
    func buttonZone(_ showZones: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: "BUTTON_DEVICES".localized(),
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj:   obj,
                                 value: "show",
                                 void:  showZones)
        )
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Menu
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func menuAbout(_ about: @escaping ((_ obj: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: "device_menu_values[0]".localized(), obj: obj, void: about)
        )
        return self
    }
    func menuScrips(_ scripts:  @escaping ((_ obj: Any)->Void)) -> XMainCellDeviceEntityBuilder {
        if (XTargetUtils.isScripts)
        {
            ce.menu.append(
                       XObjectEntity.Menu(title: "menu_device_scripts".localized(), obj: obj, void: scripts)
                   )
        }
        return self
    }
    func menuDelete(_ delete: @escaping ((_ obj: Any)->Void)) {
        ce.menu.append(
            XObjectEntity.Menu(title: "device_menu_values[4]".localized(), obj: obj, void: delete)
        )
    }
    func menu(ussd: @escaping ((_ obj: Any)->Void),
             delete: @escaping ((_ obj: Any)->Void),
             reboot: ((_ obj: Any)->Void)?,
             update: ((_ obj: Any)->Void)?) -> XMainCellDeviceEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: "device_menu_values[1]".localized(), obj: obj, void: ussd)
        )
        ce.menu.append(
            XObjectEntity.Menu(title: "device_menu_values[4]".localized(), obj: obj, void: delete)
        )
        if let reboot = reboot {
            ce.menu.append(
                XObjectEntity.Menu(title: "device_menu_values[2]".localized(), obj: obj, void: reboot)
            )
        }
        if let update = update {
            ce.menu.append(
                XObjectEntity.Menu(title: "device_menu_values[3]".localized(), obj: obj, void: update)
            )
        }
        return self
    }
}
