//
//  DataEntityMapper.swift
//  SecurityHub
//
//  Created by Timerlan on 05/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import SQLite


class DataEntityMapper {
    static func dSite(_ site: Sites, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DSiteEntity.ArmStatus) -> DSiteEntity {
        return DSiteEntity(id: site.id, site: site, type: type, affects: affects, armStatus: armStatus)
    }
    static func dSite(_ nSite: NSiteEntity, _ affects: [DAffectEntity], _ armStatus: DSiteEntity.ArmStatus) -> DSiteEntity {
        return DSiteEntity(id: nSite.id, site: nSite.site, type: nSite.type, affects: affects, armStatus: armStatus)
    }
    static func nSite(_ site: Sites, _ type: NUpdateType) -> NSiteEntity {
        return NSiteEntity(id: site.id, site: site, type: type)
    }
    
    static func dDevice(_ di: DDeviceWithSitesInfo, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DDeviceEntity.ArmStatus) -> DDeviceEntity {
        return DDeviceEntity(id: di.device.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: type, affects: affects, armStatus: armStatus)
    }
    static func dDevice(_ di: NDeviceEntity, _ affects: [DAffectEntity], _ armStatus: DDeviceEntity.ArmStatus) -> DDeviceEntity {
        return DDeviceEntity(id: di.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: di.type, affects: affects, armStatus: armStatus)
    }
    static func nDevice(_ di: DDeviceWithSitesInfo, _ type: NUpdateType) -> NDeviceEntity {
        return NDeviceEntity(id: di.device.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: type)
    }
    
    static func dSection(_ si: DSectionWithSitesDevicesInfo, _ type: NUpdateType, _ affects: [DAffectEntity]) -> DSectionEntity {
        let armStatus: DSectionEntity.ArmStatus = si.section.arm == 0 ? .disarmed : .armed
        return DSectionEntity(id: si.section.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.section.device, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, section: si.section, type: type, affects: affects, armStatus: armStatus)
    }
    static func dSection(_ si: NSectionEntity, _ affects: [DAffectEntity]) -> DSectionEntity {
        let armStatus: DSectionEntity.ArmStatus = si.section.arm == 0 ? .disarmed : .armed
        return DSectionEntity(id: si.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.deviceId, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, section: si.section, type: si.type, affects: affects, armStatus: armStatus)
    }
    static func nSection(_ si: DSectionWithSitesDevicesInfo, _ type: NUpdateType) -> NSectionEntity {
        return NSectionEntity(id: si.section.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.section.device, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, sectionId: si.section.section, section: si.section, type: type)
    }
    
    static func dZone(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType, _ affects: [DAffectEntity], _ beenAlarm: DAffectEntity?) -> DZoneEntity {
        var armStatus: DZoneEntity.ArmStatus?
        if zi.sectionDetector == HubConst.SECTION_TYPE_SECURITY { armStatus = zi.sectionArm > 0 ? .armed : .disarmed }
        if zi.zone.section == 0 && (zi.zone.zone == 0 || zi.zone.zone == 100 ) { armStatus = zi.sectionArm == 2 ? .notFullArmed : zi.sectionArm == 1 ? .armed : .disarmed }
        return DZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneId: zi.zone.zone, zoneUid: zi.zone.uidType, zoneDetector: zi.zone.detector, zone: zi.zone, type: type, affects: affects, armStatus: armStatus, beenAlarm: beenAlarm)
    }
    static func dZone(_ zi: NZoneEntity, _ affects: [DAffectEntity], _ beenAlarm: DAffectEntity?) -> DZoneEntity {
        var armStatus: DZoneEntity.ArmStatus?
        if zi.sectionDetector == HubConst.SECTION_TYPE_SECURITY { armStatus = zi.sectionArm > 0 ? .armed : .disarmed }
        if zi.zone.section == 0 && (zi.zone.zone == 0 || zi.zone.zone == 100 ) { armStatus = zi.sectionArm == 2 ? .notFullArmed : zi.sectionArm == 1 ? .armed : .disarmed }
        return DZoneEntity(id: zi.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.deviceId, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.sectionId, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneId: zi.zoneId, zoneUid: zi.zoneUid, zoneDetector: zi.zoneDetector, zone: zi.zone, type: zi.type, affects: affects, armStatus: armStatus, beenAlarm: beenAlarm)
    }
    static func nZone(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType) -> NZoneEntity {
        return NZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, sectionArm: zi.sectionArm, zoneId: zi.zone.zone, zoneUid: zi.zone.uidType, zoneDetector: zi.zone.detector, zone: zi.zone, type: type)
    }
    
    static func dRelay(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DZoneEntity.ArmStatus, _ script: DScriptInfo?) -> DZoneEntity {
        return DZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneId: zi.zone.zone, zoneUid: zi.zone.uidType, zoneDetector: zi.zone.detector, zone: zi.zone, type: type, affects: affects, armStatus: armStatus, script: script,beenAlarm: nil)
    }
    static func dRelay(_ zi: NZoneEntity, _ affects: [DAffectEntity], _ armStatus: DZoneEntity.ArmStatus, _ script: DScriptInfo?) -> DZoneEntity {
        return DZoneEntity(id: zi.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.deviceId, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.sectionId, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneId: zi.zoneId, zoneUid: zi.zoneUid, zoneDetector: zi.zoneDetector, zone: zi.zone, type: zi.type, affects: affects, armStatus: armStatus, script: script, beenAlarm: nil)
    }
    
    static func dAffect(obj: Array<Optional<Binding>>) -> DAffectEntity {
        let icon = obj[4] as? String ?? "ic_action_info_grey_small"
        let device = obj[0] as? String ?? R.strings.device_no_name
        let sectionZone = getSectionZone(section: obj[1] as? String, zone: obj[2] as? String)
        let desc = obj[3] as? String ?? R.strings.no_affect_desc
        let _class = obj[5] as? Int64 ?? 17
        let id = obj[6] as? Int64 ?? 0
        let detector    = obj[7] as? Int64 ?? 0
        let reason      = obj[8] as? Int64 ?? 0
        let active      = obj[9] as? Int64 ?? 0
        let jdata       = obj[10] as? String ?? ""
        let time        = obj[11] as? Int64 ?? 0
        return DAffectEntity(id: id, icon: icon, device: device, sectionZone: sectionZone, desc: desc, _class: _class, detector: detector, reason: reason, active: active, jdata: jdata.toJson(), time: time)
    }
    
    static func dScript(_ script: DScriptInfo, library: HubLibraryScripts, type: NUpdateType) -> DScriptEntity {
        let library = library.items.first(where: { $0.uid == script.uid })?.data
        return DScriptEntity(deviceId: script.deviceId, configVersion: script.configVersion, id: script.id, uid: script.uid, bind: script.bind, zoneName: script.zoneName, enabled: script.enabled, params: script.params, library: library, type: type)
    }
    
    static func deviceWithSiteInfo(_ obj: Array<Optional<Binding>>) -> DDeviceWithSiteInfo? {
        if  let id = obj[0] as? Int64,
            let domain = obj[2] as? Int64,
            let account = obj[3] as? Int64,
            let cluster_id = obj[4] as? Int64,
            let configVersion = obj[5] as? Int64,
            let time = obj[6] as? Int64,
            let siteId = obj[7] as? Int64,
            let siteName = obj[8] as? String {
            let device = Devices(id: id, domain: domain, account: account, cluster_id: cluster_id, configVersion: configVersion, time: time)
            return DDeviceWithSiteInfo(device: device, siteId: siteId, siteName: siteName)
        }
        return nil
    }
    
    static func sectionWithSiteDeviceInfo(_ obj: Array<Optional<Binding>>) -> DSectionWithSiteDeviceInfo? {
        if  let device = obj[1] as? Int64,
            let section = obj[2] as? Int64,
            let name = obj[3] as? String,
            let detector = obj[4] as? Int64,
            let arm = obj[5] as? Int64,
            let time = obj[6] as? Int64,
						let autoarmId = obj[7] as? Int64,
            let siteId = obj[8] as? Int64,
            let siteName = obj[9] as? String,
            let configVersion = obj[10] as? Int64,
            let cluster_id = obj[11] as? Int64,
            let deviceName = obj[12] as? String {
            let section = Sections(device: device, section: section, name: name, detector: detector, arm: arm, time: time, autoarmId: autoarmId)
            return DSectionWithSiteDeviceInfo(section: section, siteId: siteId, siteName: siteName, deviceName: deviceName, configVersion: configVersion, cluster_id: cluster_id)
        }
        return nil
    }
    
    static func zoneWithSiteDeviceSectionInfo(_ obj: Array<Optional<Binding>>) -> DZoneWithSiteDeviceSectionInfo? {
        if  let device = obj[1] as? Int64,
            let section = obj[2] as? Int64,
            let zone = obj[3] as? Int64,
            let name = obj[4] as? String,
            let delay = obj[5] as? Int64,
            let detector = obj[6] as? Int64,
            let physic = obj[7] as? Int64,
            let uidType = obj[8] as? Int64,
            let time = obj[9] as? Int64,
						let bypass = obj[10] as? Int64,
            let siteId = obj[11] as? Int64,
            let siteName = obj[12] as? String,
            let deviceName = obj[13] as? String,
            let sectionName = obj[14] as? String,
            let sectionDetector = obj[15] as? Int64,
            let sectionArm = obj[16] as? Int64,
            let configVersion = obj[17] as? Int64,
            let cluster = obj[18] as? Int64 {
            let zone = Zones(device: device, section: section, zone: zone, name: name, delay: delay, detector: detector, physic: physic, uidType: uidType, time: time, bypass: bypass)
            return DZoneWithSiteDeviceSectionInfo(zone: zone, siteId: siteId, siteName: siteName, deviceName: deviceName, configVersion: configVersion, cluster: cluster, sectionName: sectionName, sectionDetector: sectionDetector, sectionArm: sectionArm)
                }
        return nil
    }
    
    static func zoneInfoAffects(_ obj: Array<Optional<Binding>>) -> DAffectEntity? {
        let i = 19
        if  let icon = obj[i + 4] as? String,
            let device = obj[i + 0] as? String,
            let sectionZone = getSectionZone(section: obj[i + 1] as? String, zone: obj[i + 2] as? String),
            let desc = obj[i + 3] as? String,
            let _class = obj[i + 5] as? Int64,
            let id = obj[i + 6] as? Int64,
            let detector    = obj[i + 7] as? Int64,
            let reason      = obj[i + 8] as? Int64,
            let active      = obj[i + 9] as? Int64,
            let jdata       = obj[i + 10] as? String,
            let time        = obj[i + 11] as? Int64 {
                return DAffectEntity(id: id, icon: icon, device: device, sectionZone: sectionZone, desc: desc, _class: _class, detector: detector, reason: reason, active: active, jdata: jdata.toJson(), time: time)
            }
        return nil
    }
    
    static func sectionInfoAffects(_ obj: Array<Optional<Binding>>) -> DAffectEntity? {
        let i = 13
        if  let icon = obj[i + 3] as? String,
            let device = obj[i + 0] as? String,
            let sectionZone = getSectionZone(section: obj[i + 1] as? String, zone: nil),
            let desc = obj[i + 2] as? String,
            let _class = obj[i + 4] as? Int64,
            let id = obj[i + 5] as? Int64,
            let detector    = obj[i + 6] as? Int64,
            let reason      = obj[i + 7] as? Int64,
            let active      = obj[i + 8] as? Int64,
            let jdata       = obj[i + 9] as? String,
            let time        = obj[i + 10] as? Int64 {
                return DAffectEntity(id: id, icon: icon, device: device, sectionZone: sectionZone, desc: desc, _class: _class, detector: detector, reason: reason, active: active, jdata: jdata.toJson(), time: time)
            }
        return nil
    }
    
    static func affectRelation(_ obj: Array<Optional<Binding>>) -> DAffectRelation? {
        if  let device  = obj[0] as? Int64,
            let section = obj[1] as? Int64,
            let zone    = obj[2] as? Int64 {
            return DAffectRelation(deviceId: device, sectionId: section, zoneId: zone)
        }
        return nil
    }
    
    static func affectRemove(_ obj: Array<Optional<Binding>>) -> DAffectRemove? {
        if  let device  = obj[0] as? Int64,
            let section = obj[1] as? Int64,
            let zone    = obj[2] as? Int64,
            let _class  = obj[3] as? Int64,
            let reason  = obj[4] as? Int64 {
            return DAffectRemove(deviceId: device, sectionId: section, zoneId: zone, _class: _class, reason: reason)
        }
        return nil
    }
    
    static func script(_ obj: Array<Optional<Binding>>) -> DScriptInfo? {
        if  let device = obj[0] as? Int64,
            let configVersion = obj[1] as? Int64,
            let id = obj[2] as? Int64,
            let uid = obj[3] as? String,
            let bind = obj[4] as? Int64,
            let enabled = obj[5] as? Int64,
            let params = obj[6] as? String,
            let extra = obj[7] as? String,
            let zoneName = obj[8] as? String,
            let name = obj[9] as? String{
            return DScriptInfo(deviceId: device, configVersion: configVersion, id: id, uid: uid, bind: bind, zoneName: zoneName, enabled: enabled == 1, params: params.toJsonArray(), extra: extra.toJson(), name: name)
        }
        return nil
    }
    
    private static func getSectionZone(section: String?, zone: String?) -> String?{
        var location: String?
        if let section = section { location = section }
        if let zone = zone, let _ = section { location! += " → \(zone)" }
        else if let zone = zone { location = zone }
        return location
    }
    
    static func zoneFullName(_ obj: Array<Optional<Binding>>) -> DZoneFullNameEntity? {
        if  let siteId = obj[0] as? Int64,
            let siteName = obj[1] as? String,
            let deviceId = obj[2] as? Int64,
            let deviceName = obj[3] as? String,
            let sectionId = obj[4] as? Int64,
            let sectionName = obj[5] as? String,
            let zoneId = obj[6] as? Int64,
            let zoneName = obj[7] as? String {
            return DZoneFullNameEntity(siteId: siteId, siteName: siteName, deviceId: deviceId, deviceName: deviceName, sectionId: sectionId, sectionName: sectionName, zoneId: zoneId, zoneName: zoneName)
        }
        return nil
    }
    
    static func eventCamera(_ obj: Array<Optional<Binding>>) -> DEventCameraEntity? {
        if  let eventId = obj[0] as? Int64,
            let eventIcon = obj[1] as? String,
            let eventIconBig = obj[2] as? String,
            let eventDesc = obj[3] as? String,
            let eventTime = obj[4] as? Int64,
            let ivVideoResult = obj[5] as? String,
            let ivVideoIsError = obj[6] as? Int64,
            let siteName = obj[7] as? String,
            let deviceName = obj[8] as? String,
            let sectionName = obj[9] as? String,
            let zoneName = obj[10] as? String {
            return DEventCameraEntity(eventId: eventId, icon: eventIcon, iconBig: eventIconBig, description: eventDesc, time: eventTime, record: ivVideoIsError == 0 ? ivVideoResult : nil, siteName: siteName, deviceName: deviceName, sectionName: sectionName, zoneName: zoneName, location: nil)
        }
        return nil
    }
    
    static func dEvent(obj: Array<Optional<Binding>>) -> DEventEntity {
        let id = obj[0] as? Int64 ?? 0
        let _class = obj[1] as? Int64 ?? 0
        let reason = obj[2] as? Int64 ?? 0
        let detector = obj[3] as? Int64 ?? 0
        let active = obj[4] as? Int64 ?? 0
        let affect = obj[5] as? Int64 ?? 0
        let device = obj[6] as? Int64 ?? 0
        let section = obj[7] as? Int64 ?? 0
        let zone = obj[8] as? Int64 ?? 0
        let time = obj[9] as? Int64 ?? 0
        let jdata = obj[10] as? String ?? ""
        let icon = obj[11] as? String ?? "ic_action_info_grey_small"
        let iconBig = obj[12] as? String ?? "ic_action_info_grey_small"
        let affect_desc = obj[13] as? String ?? R.strings.no_affect_desc
        let dropped = obj[14] as? Int64 ?? 0
        
        let site = obj[15] as? Int64 ?? 0
        let siteName = obj[16] as? String ?? ""
        let deviceName = obj[17] as? String ?? ""
        let sectionName = obj[18] as? String
        let zoneName = obj[19] as? String

        let videoRecord = obj[20] as? String

        return DEventEntity(id: id, _class: _class, reason: reason, detector: detector, active: active, affect: affect, site: site, device: device, section: section, zone: zone, time: time, jdata: jdata, icon: icon, iconBig: iconBig, affect_desc: affect_desc, dropped: dropped, siteName: siteName, deviceName: deviceName, sectionName: sectionName, zoneName: zoneName, videoRecord: videoRecord, isVideoError: nil)
    }
}
