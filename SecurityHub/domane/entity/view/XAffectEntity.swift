//
//  XAffectEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 03/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

struct XEventEntity {
    var siteName: String
    var deviceName: String
    var sectionZoneName: String?
    var imgName: String
    var time: String
}
