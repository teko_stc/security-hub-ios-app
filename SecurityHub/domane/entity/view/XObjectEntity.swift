//
//  XMainEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

struct XObjectEntity{
    var id: Int64 = 0
    var updateType: NUpdateType = .insert
    var isFolding = true
    var foldingInfo: Folding = Folding(isOpen: false, openHeight: 600, closeHeight: 140)
    
    var image: String = "no"
    var imageColor: UInt? = nil
    var smallImage: String? = nil
    var title: String = ""
    var info: String = ""
    var status: String = ""
    var time: String = "--:--:--"
    
    var objectName: String?
    var deviceName: String?
    var sectionName: String?
    
    var affects: [Affect] = []
    var affectClick: ((_ affect_id: Int64) -> Void)? = nil
    var affectsMini: [String] = []
    
    var buttons: [Button] = []
    var menu: [Menu] = []
    
    struct Affect {
        let id: Int64
        let icon: String
        let device: String
        let sectionZone: String?
        let desc: String
    }
    struct Button {
        let title: String
        let color: UInt
        let obj: Any
        let value: Any
        let void: ((_ obj: Any, _ value: Any)->Void)
    }
    struct Menu {
        let title: String
        let obj: Any
        let void: ((_ obj: Any)->Void)
    }
    struct Folding {
        var isOpen: Bool
        var openHeight: Float
        var closeHeight: Float
        
        var height: Float {
            get { return isOpen ? openHeight : closeHeight}
        }
    }
}
