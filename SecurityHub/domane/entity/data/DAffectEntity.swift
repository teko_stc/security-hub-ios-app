//
//  DAffect.swift
//  SecurityHub
//
//  Created by Timerlan on 05/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

struct DAffectEntity {
    let id: Int64
    let icon: String
    let device: String
    let sectionZone: String?
    let desc: String
    let _class: Int64
    let detector: Int64
    let reason: Int64
    let active: Int64
    let jdata: [String:AnyObject]
    let time: Int64
}

struct DAffectRelation {
    var deviceId: Int64
    var sectionId: Int64
    var zoneId: Int64
}

struct DAffectRemove {
    var deviceId: Int64
    var sectionId: Int64
    var zoneId: Int64
    var _class: Int64
    var reason: Int64
}
