//
//  DCommandResult.swift
//  SecurityHub
//
//  Created by Timerlan on 17/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

struct DCommandResult {
    var success: Bool
    var code: Int64
    var message: String
    
    static var NO_SIGNAL: DCommandResult {
        get { return DCommandResult(success: false, code: HubResponseCode.NO_SIGNAL_CODE, message: "D3_ERRROR".localized())}
    }
    static var NO_SIGNAL_DEVICE: DCommandResult {
        get { return DCommandResult(success: false, code: HubResponseCode.NO_SIGNAL_DEVICE_CODE, message: "N_FUNC_NO_CONNECTION".localized())}
    }
    static var DEVICE_ARMED: DCommandResult {
        get { return DCommandResult(success: false, code: HubResponseCode.DEVICE_ARMED_CODE, message: "N_SECTION_NEED_DISARM".localized())}
    }
    static var ERROR: DCommandResult {
        get { return DCommandResult(success: false, code: 0, message: HubResponseCode.getError(error: 0))}
    }
    static func ERROR(result: Int64) -> DCommandResult {
        return DCommandResult(success: false, code: result, message: "\("ERROR_COMMAND".localized()) \(HubResponseCode.getError(error: result))")
    }
    static func ERROR(command_update_result: Int64) -> DCommandResult {
        if command_update_result > 0 {
            return DCommandResult(success: false, code: command_update_result, message: "\("ERROR_COMMAND".localized()) \(HubConst.getCommandResults(code: command_update_result))")
        }
        return DCommandResult(success: false, code: command_update_result, message: "\("ERROR_COMMAND".localized()) \(HubConst.getCommandResultsNegative(code: command_update_result))")
    }
    static var SUCCESS: DCommandResult {
        get { return DCommandResult(success: true, code: 1, message: "") }
    }
    static func SUCCESS(result: Int64) -> DCommandResult {
        return DCommandResult(success: true, code: result, message: "")
    }
    static func SUCCESS(command_update_result: Int64) -> DCommandResult {
        if command_update_result == 1 {
            return DCommandResult(success: true, code: command_update_result, message: "")
        }
        return DCommandResult(success: true, code: command_update_result, message: "\("ERROR_COMMAND".localized()) \(HubConst.getCommandResults(code: command_update_result))")
    }
}

struct DInteractiveModeResult {
    enum InteractiveStatus { case finish, not_start, error, start }
    var status: InteractiveStatus
    var message: String
    var uid_type: Int64? = nil
    var uid: String? = nil
    
    static var START: DInteractiveModeResult {
        get { return DInteractiveModeResult(status: .start, message: "") }
    }
    static var NOT_START: DInteractiveModeResult {
        get { return DInteractiveModeResult(status: .not_start, message: "WIZ_USER_INTERACTIVE_TIMEOUT".localized()) }
    }
    static var FINISH : DInteractiveModeResult {
        get { return DInteractiveModeResult(status: .finish, message: "WIZ_USER_INTERACTIVE_TIMEOUT".localized()) }
    }
    static func ERROR(message: String) -> DInteractiveModeResult {
        return DInteractiveModeResult(status: .error, message: message)
    }
    static func FINISH(uid_type: Int64, uid: String) -> DInteractiveModeResult {
        return DInteractiveModeResult(status: .finish, message: "", uid_type: uid_type, uid: uid)
    }
}
