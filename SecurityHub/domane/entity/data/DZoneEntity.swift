//
//  DZoneEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 08/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

struct DZoneEntity {
    let id: String
    let siteIds: [Int64]
    let siteNames: String
    let deviceId: Int64
    let deviceName: String
    let configVersion: Int64
    let cluster: Int64
    let sectionId: Int64
    let sectionName: String
    let sectionDetector: Int64
    let zoneId: Int64
    let zoneUid: Int64
    let zoneDetector: Int64
    let zone: Zones
    let type: NUpdateType
    let affects: [DAffectEntity]
    let armStatus: ArmStatus?
    var script: DScriptInfo?
    var beenAlarm: DAffectEntity?
    
    enum ArmStatus {
        case armed, disarmed, notFullArmed
    }
}

struct DZoneWithSiteDeviceSectionInfo {
    let zone: Zones
    let siteId: Int64
    let siteName: String
    let deviceName: String
    let configVersion: Int64
    let cluster: Int64
    let sectionName: String
    let sectionDetector: Int64
    let sectionArm: Int64
}

struct DZoneWithSitesDeviceSectionInfo {
    let zone: Zones
    var siteIds: [Int64]
    var siteNames: String
    var deviceName: String
    let configVersion: Int64
    let cluster: Int64
    var sectionName: String
    var sectionDetector: Int64
    var sectionArm: Int64
    
    var affects: [DAffectEntity]?
}

struct DZoneSitesDeviceSectionInfo {
    var siteIds: [Int64]
    var siteNames: String
    var deviceName: String
    var configVersion: Int64
    var cluster: Int64
    var sectionName: String
    var sectionDetector: Int64
    var sectionArm: Int64
}

