//
//  DScriptEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 21/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation
import Localize_Swift

struct DScriptEntity {
    let deviceId: Int64
    let configVersion: Int64
    let id: Int64
    let uid: String
    let bind: Int64
    let zoneName: String
    let enabled: Bool
    let params: [[String : Any]]
    let library: HubLibraryScript.Data?
    let type: NUpdateType
    
    func getName() -> String? {
        return library?.name.first{ $0.iso == Localize.currentLanguage() }?.value ?? library?.name.first?.value
    }
}

struct DScriptInfo {
    let deviceId: Int64
    let configVersion: Int64
    let id: Int64
    let uid: String
    let bind: Int64
    let zoneName: String
    let enabled: Bool
    let params: [[String : Any]]
    let extra: [String : AnyObject]
    let name: String

    func getCommands() -> [String : Int64] {
        var result: [String : Int64] = [:]
        guard let e = HubScript.Extra(json: extra) else { return result }
        let _es = e._switch?.rows.flatMap{ $0.items }
        for e in _es ?? [] {
            let name = e.content.first { $0.iso == Localize.currentLanguage() }?.value ?? e.content.first { _ in true }?.value ?? "COMMAND_UNKNOWN_TYPE".localized()
            result.updateValue(e.value, forKey: name)
        }
        return result
    }
}

