//
//  DEventEntity.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 09.09.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

struct DEventEntity {
    var id : Int64
    var _class : Int64
    var reason : Int64
    var detector : Int64
    var active : Int64
    var affect : Int64
    var site : Int64
    var device : Int64
    var section : Int64
    var zone : Int64
    var time : Int64
    var jdata : String
    var icon : String
    var iconBig : String
    var affect_desc : String
    var dropped : Int64
    
    var siteName: String
    var deviceName: String
    var sectionName: String?
    var zoneName: String?
    
    var videoRecord: String?
    var isVideoError: Bool?
}
