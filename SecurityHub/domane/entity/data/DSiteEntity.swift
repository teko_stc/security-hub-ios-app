//
//  DSiteEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 05/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation


struct DSiteEntity {
    let id: Int64
    let site: Sites?
    let type: NUpdateType
    let affects: [DAffectEntity]
    let armStatus: ArmStatus
    
    enum ArmStatus {
        case armed, disarmed, notFullArmed
    }
}

struct DSiteInfo {
    var operatorCount: Int = 0
    var deviceInfos: [DDeviceInfo] = []
}
