//
//  DZoneTypeEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 17/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Gloss

class DTypesEntity: Glossy {
    var inputTypes: [DInputTypeEntity] = []
    var outputTypes: [DItemTypeEntity] = []
    var zoneTypes: [DZoneTypeEntity] = []
    var deviceTypes: [DItemTypeEntity] = []
    var sectionTypes: [DItemTypeEntity] = []
    
    //MARK: Decodable
    public required init(json: JSON){
        if let inputTypes : [DInputTypeEntity] = "inputTypes" <~~ json { self.inputTypes = inputTypes }
        if let outputTypes : [DItemTypeEntity] = "outputTypes" <~~ json { self.outputTypes = outputTypes }
        if let zoneTypes : [DZoneTypeEntity] = "zoneTypes" <~~ json { self.zoneTypes = zoneTypes }
        if let deviceTypes : [DItemTypeEntity] = "deviceTypes" <~~ json { self.deviceTypes = deviceTypes }
        if let sectionTypes : [DItemTypeEntity] = "sectionTypes" <~~ json { self.sectionTypes = sectionTypes }
    }
    func toJSON() -> JSON? { return nil }
}

class DInputTypeEntity : Glossy {
    var id: Int = 0
    var title: String = ""
    var icons: [String] = []
    var detectorTypes: [DDetectorTypeEntity] = []
    
    //MARK: Decodable
    public required init(json: JSON){
        if let id : Int = "id" <~~ json { self.id = id }
        if let title : String = "title" <~~ json { self.title = title }
        if let icons : [String] = "icons" <~~ json { self.icons = icons }
        if let detectorTypes : [DDetectorTypeEntity] = "detectorTypes" <~~ json { self.detectorTypes = detectorTypes }
    }
    func toJSON() -> JSON? { return nil }
}

class DZoneTypeEntity : Glossy {
    var id: Int = 0
    var title: String = ""
    var subTitle: String = ""
    var img: String = ""
    var icons: [String] = []
    var detectorTypes: [DDetectorTypeEntity] = []
    
    //MARK: Decodable
    public required init(json: JSON){
        if let id : Int = "id" <~~ json { self.id = id }
        if let title : String = "title" <~~ json { self.title = title }
        if let subTitle : String = "subTitle" <~~ json { self.subTitle = subTitle }
        if let img : String = "img" <~~ json { self.img = img }
        if let icons : [String] = "icons" <~~ json { self.icons = icons }
        if let detectorTypes : [DDetectorTypeEntity] = "detectorTypes" <~~ json { self.detectorTypes = detectorTypes }
    }
    func toJSON() -> JSON? { return nil }
}

class DDetectorTypeEntity : Glossy {
    var id: Int = 0
    var title: String = ""
    var sectionMask: Int = 0
    var sectionDefault: Int = 0
    var isDefaultDetector: Bool = false
    var alarmTypes: [DAlarmTypeEntity] = []

    //MARK: Decodable
    public required init(json: JSON){
        if let id : Int = "id" <~~ json { self.id = id }
        if let title : String = "title" <~~ json { self.title = title }
        if let sectionMask : String = "sectionMask" <~~ json { self.sectionMask = Int(sectionMask) ?? 0 }
        if let sectionDefault : Int = "sectionDefault" <~~ json { self.sectionDefault = sectionDefault }
        if let isDefaultDetector : Bool = "isDefaultDetector" <~~ json { self.isDefaultDetector = isDefaultDetector }
        if let alarmTypes : [DAlarmTypeEntity] = "alarmTypes" <~~ json { self.alarmTypes = alarmTypes }
    }
    func toJSON() -> JSON? { return nil }
    
    func alarmNames() -> [String] {
        var result: [String] = []
        for a in alarmTypes {
            result.append(a.title + "\n" + title)
        }
        return result
    }
    
    func getSectionMask() -> Int { return sectionMask }
    
    func defaultAlarmName() -> String? {
        if let at = alarmTypes.first(where: { (at) -> Bool in return at.isDefaultAlarm }) {
            return at.title + "\n" + title
        }
        return nil
    }
}

class DAlarmTypeEntity : Glossy {
    var id: Int = 0
    var title: String = ""
    var icons: [String] = []
    var isDefaultAlarm: Bool = false
    
    //MARK: Decodable
    public required init(json: JSON){
        if let id : Int = "id" <~~ json { self.id = id }
        if let title : String = "title" <~~ json { self.title = title }
        if let icons : [String] = "icons" <~~ json { self.icons = icons }
        if let isDefaultAlarm : Bool = "isDefaultAlarm" <~~ json { self.isDefaultAlarm = isDefaultAlarm }
    }
    func toJSON() -> JSON? { return nil }
}

class DItemTypeEntity : Glossy {
    var id: Int = 0
    var title: String = ""
    var icons: [String] = []
    
    //MARK: Decodable
    public required init(json: JSON){
        if let id : Int = "id" <~~ json { self.id = id }
        if let title : String = "title" <~~ json { self.title = title }
        if let icons : [String] = "icons" <~~ json { self.icons = icons }
    }
    func toJSON() -> JSON? { return nil }
}
