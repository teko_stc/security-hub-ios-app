//
//  DAutoArmEntity.swift
//  SecurityHub
//
//  Created by Stefan on 08.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

struct DAutoArmEntity {
		let id: Int64
		var site: Int64
		let device: Int64
		let section: Int64
		let runAt: Int64
		let active: Int64
		let daysOfWeek: Int64
		let hour: Int64
		let minute: Int64
		let timezone: Int64
		let lastExecuted: Int64
}
