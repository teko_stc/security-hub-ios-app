//
//  DSelectorEntity.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 21.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Gloss

class DSelectorEntity : Glossy {
    var items: [DSelectorItemEntity] = []
    
    //MARK: Decodable
    public required init(json: JSON){
        if let items : [DSelectorItemEntity] = "item" <~~ json {
            self.items = items
        } else if let item : DSelectorItemEntity = "item" <~~ json {
            self.items = [item]
        }
    }
    func toJSON() -> JSON? { return nil }
}


class DSelectorItemEntity : Glossy {
    var drawable: String = ""
    var stateSelected: Bool = false
    
    //MARK: Decodable
    public required init(json: JSON){
        if let drawable : String = "android:drawable" <~~ json { self.drawable = drawable }
        if let stringValue: String = "android:state_selected" <~~ json { self.stateSelected = NSString(string: stringValue).boolValue }
    }
    func toJSON() -> JSON? { return nil }
}
