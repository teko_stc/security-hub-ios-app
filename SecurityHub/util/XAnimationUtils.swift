//
//  XAnimationUtils.swift
//  SecurityHub
//
//  Created by Timerlan on 26.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XAnimationUtils {
    static func playBounceAnimation(_ icon : UIView) {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        bounceAnimation.duration = TimeInterval(0.5)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        icon.layer.add(bounceAnimation, forKey: "bounceAnimation")
    }
}

