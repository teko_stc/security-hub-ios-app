//
//  XTargetUtils.swift
//  SecurityHub
//
//  Created by Timerlan on 04/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

class XTargetUtils {

    static var target: XTargets { return XTargets(rawValue: Bundle.main.bundleIdentifier!) ?? XTargets.security_hub }

    static var server: String{
        XServerChanger.url()
    }
	
		static var regServer: String{
				XServerChanger.regUrl()
		}
    
    static var name: String{
        switch target {
        case .test,.dev, .security_hub: return "SECURITY HUB"
        case .tattelecom:   return "Tattelecom"
        case .myuvo:        return "Myuvo"
        case .omicron:      return "Omicron"
        case .krasnodar:    return "Krasnodar"
        case .saratov:      return "Saratov"
        case .samara:       return "Samara"
        case .kuzet:        return "Kuzet"
        case .favorit:      return "Favorit"
        case .ksb:          return "Ksb"
        case .rostov:       return "Rostov"
        case .legion:       return "Legion"
        case .teleplus:     return "Teleplus"
        case .secmatrix:    return "Secmatrix"
        case .akm:          return "Akm"
        }
    }
    
    static var defaultTarger: String {
        switch target {
        case .test:             return "test.security-hub.ru"
        case .dev:              return "dev.security-hub.ru"
        case .security_hub:     return "cloud.security-hub.ru"
        case .tattelecom:       return "control.tattelecom.ru"
        case .myuvo:            return "95.165.234.85"
        case .omicron:          return "80.76.185.205"
        case .krasnodar:        return "83.239.21.74"
        case .saratov:          return "88.147.147.88"
        case .samara:           return "80.234.39.160"
        case .kuzet:            return "178.91.48.5"
        case .favorit:          return "91.226.173.4"
        case .ksb:              return "78.110.158.251"
        case .rostov:           return "87.117.003.162"
        case .legion:           return "81.89.126.26"
        case .teleplus:         return "217.114.191.34"
        case .secmatrix:        return "206.54.190.168"
        case .akm:              return "212.175.226.118"
        }
    }
    
    static var defaultName: String {
        switch target {
        case .security_hub: return "SECURITY HUB"
        case .dev:          return "Cloud/dev"
        case .test:         return "Cloud/test"
        case .tattelecom:   return "Tattelecom"
        case .myuvo:        return "Myuvo"
        case .omicron:      return "Omicron"
        case .krasnodar:    return "Krasnodar"
        case .saratov:      return "Saratov"
        case .samara:       return "Samara"
        case .kuzet:        return "Kuzet"
        case .favorit:      return "Favorit"
        case .ksb:          return "Ksb"
        case .rostov:       return "Rostov"
        case .legion:       return "Legion"
        case .teleplus:     return "Teleplus"
        case .secmatrix:    return "Secmatrix"
        case .akm:          return "Akm"
        }
    }
    
    static var regLink: String? {
        switch target {
        case .test:             return "https://test.security-hub.ru"
        case .dev:              return "https://dev.security-hub.ru"
        case .security_hub:     return "https://cloud.security-hub.ru"
        case .tattelecom:       return "https://control.tattelecom.ru"
        case .myuvo:            return nil
        case .omicron:          return "https://sh.omicron57.ru"
        case .krasnodar:        return nil
//        case .saratov:          return "https://saratov.security-hub.ru"
        case .saratov:          return nil
        case .samara:           return "https://samara.security-hub.ru"
        case .kuzet:            return "http://kuzethub.kz"
        case .favorit:          return nil
        case .ksb:              return "http://www.ksb-rso.ru"
        case .rostov:           return "http://охранавнг.рф"
        case .legion:           return nil //"https://49legion.ru/"
        case .teleplus:         return nil
        case .secmatrix:        return "https://sistemasdeseguridad.club/reg"
        case .akm:              return "https://cloud.akmguvenlik.com.tr/reg"
        }
    }
    
    static var ivideon: String? {
        XServerChanger.ivideon?.url
    }
    
    static var ivideonId: String? {
        XServerChanger.ivideon?.id
    }
    
    static var isHubTarget: Bool{
        switch target {
        case .test, .dev, .security_hub: return true
        default: return false
        }
    }
    static var isScripts: Bool{
        switch target {
        case .test, .dev, .security_hub, .saratov, .krasnodar, .teleplus, .secmatrix, .myuvo, .akm: return true
        default: return false
        }
    }
}

enum XTargets: String {
    case test = "com.teko.dpk"
    case dev = "com.teko.dev"
    case security_hub = "com.teko.sh"
    case tattelecom = "com.ttk.dpk"
    case myuvo = "com.teko.myuvo"
    case omicron = "com.teko.omicron"
    case krasnodar = "com.teko.uvo.krasnodar"
    case saratov = "com.teko.uvo.saratov"
    case samara = "com.teko.uvo.samara"
    case kuzet = "com.teko.kuzet.monitoring"
    case favorit = "com.teko.favorit"
    case ksb = "com.teko.ksb"
    case rostov = "com.teko.uvo.rostov"
    case legion = "com.teko.legion"
    case teleplus = "com.teko.teleplus"
    case secmatrix = "com.teko.secmatrix"
    case akm = "com.teko.akmsistem"
}
