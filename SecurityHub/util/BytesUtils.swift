//
//  BytesUtils.swift
//  SecurityHub
//
//  Created by Daniil on 18.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class BytesUtils {
    
    public static func invertBytes(_ bytes: [Int8]) -> [Int8] {
        var bytes = bytes
        
        for i in 0..<bytes.count / 2 {
            let byte = bytes[i]
            
            bytes[i] = bytes[bytes.count - i - 1]
            bytes[bytes.count - i - 1] = byte
        }
        
        return bytes
    }
    
    public static func byteArrayToUInt32(_ array: [Int8]) -> Int32 {
        let bigEndianValue = array.withUnsafeBufferPointer {
                 ($0.baseAddress!.withMemoryRebound(to: Int32.self, capacity: 1) { $0 })
        }.pointee
        
        return Int32(bigEndian: bigEndianValue)
    }
    
    public static func byteArrayToInt16(_ array: [Int8]) -> Int16 {
        return Data(int8ArrayToUInt8(array)).reduce(0) { v, byte in
            return v << 8 | Int16(byte)
        }
    }
    
    public static func byteArrayToInt(_ array: [Int8]) -> Int {
        return Data(int8ArrayToUInt8(array)).reduce(0) { v, byte in
            return v << 8 | Int(byte)
        }
    }
    
    public static func intToBytesArray(_ value: Int, arraySize: Int) -> [Int8] {
        let byteBuffer = ByteBuffer(size: arraySize)
        
        byteBuffer.order(.little)
        byteBuffer.put(value)

        return uInt8ArrayToInt8(byteBuffer.getArray())
    }
    
    public static func stringToBytesArray(_ value: String, arraySize: Int) -> [Int8] {
        var data = Data([UInt8] (value.utf8))
        data.count = arraySize
        
        return uInt8ArrayToInt8([UInt8] (data))
    }
    
    public static func uInt8ArrayToInt8(_ array: [UInt8]) -> [Int8] {
        return array.map { Int8(bitPattern: $0) }
    }
    
    public static func int8ArrayToUInt8(_ array: [Int8]) -> [UInt8] {
        return array.map { UInt8(bitPattern: $0) }
    }
    
    public static func deleteZeros(_ array: [Int8]) -> [Int8] {
        return array.filter { $0 != 0 }
    }
    
    public static func deleteZeros(_ array: [UInt8]) -> [UInt8] {
        return array.filter { $0 != 0 }
    }
    
    public static func decodeUtf8(_ array: [Int8]) -> String {
        return String(data: Data(deleteZeros(int8ArrayToUInt8(array))), encoding: .utf8) ?? ""
    }
}
