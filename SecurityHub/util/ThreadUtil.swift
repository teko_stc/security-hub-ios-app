//
//  ThreadUtil.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import RxSwift

class ThreadUtil {
    public static let shared = ThreadUtil()
    let backScheduler = ConcurrentDispatchQueueScheduler.init(qos: .utility)
    let mainScheduler = MainScheduler.init()
    let backOpetarion = DispatchQueue.global(qos: .utility)
}
