//
//  AlertHelpers.swift
//  SecurityHub
//
//  Created by Timerlan on 12.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import UIKit
import RxSwift
import SCLAlertView
import AVFoundation


class AlertUtil {
    static let appearance = SCLAlertView.SCLAppearance(
        kTitleFont: UIFont(name: "HelveticaNeue", size: 16)!,
        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!
    )
    
    static func urlAlert(_ title: String, message: String, url: String, nV: UINavigationController?){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_CONTINUE".localized(), style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: url)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string: url)!)
            }
        }))
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        nV?.present(alert, animated: false)
    }
    
    static func exitAlert(_ exitListener: (()->Void)?, mes: String = "PA_EXIT_DIALOG_TITLE_1".localized()) -> UIAlertController{
        let alert = UIAlertController.init(title: "title_confirm".localized(), message: mes, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_YES".localized(), style: .default, handler: { (action) in exitListener?() }))
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        return alert
    }
    
    static func errorAlert(title: String = "ERROR".localized(), _ message: String){
        SCLAlertView(appearance: AlertUtil.appearance).showError(title, subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized())
    }
    
    static func successAlert(title: String = "SUCCESS_SET".localized(), _ message: String){
        SCLAlertView(appearance: AlertUtil.appearance).showSuccess(title, subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized())
    }
    
    static func infoAlert(title: String = "SUCCESS_SET".localized(), _ message: String){
         SCLAlertView(appearance: AlertUtil.appearance).showInfo(title, subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized(), colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func infoAlertUser(_ message: String, name: String, listener: @escaping ((_ text: String?)->Void), change: @escaping ((_ text: String?)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton("RENAME".localized(), action: { renameXAlert(text: name, listener) })
        alert.addButton("PLA_CHANGE_PASS".localized(), action: { changePasAlert(change) })
        alert.showInfo("", subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized(),colorStyle: UIColor.hubMainColorUINT)
    }
    static func infoAlertApp(_ message: String){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.showInfo("", subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized(),colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func warAlert(title: String = "title_warning".localized(), _ message: String) {
        SCLAlertView(appearance: AlertUtil.appearance).showNotice(title, subTitle: message, closeButtonTitle: "N_BUTTON_OK".localized())
    }
    
    static func warAlert(title: String = "title_warning".localized(), message: String,
                         actionText: String, actionVoid:   @escaping (() -> Void),
                         canceled: @escaping (() -> Void)) {
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(actionText, action: { actionVoid() })
        alert.showTitle(title, subTitle: message, timeout: nil, completeText: message,
                               style: .notice, colorStyle: SCLAlertViewStyle.notice.defaultColorInt, colorTextButton: 0xFFFFFF,
                               circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    static func myXAlert(_ title: String? = nil, messages: [String], voids: [(()->Void)?], complited: (()->Void)? = nil) -> UIAlertController{
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for mes in messages.enumerated() {
            alert.addAction(UIAlertAction.init(title: mes.element, style: .default, handler: { (action) in voids[mes.offset]?() }))
        }
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        return alert
    }
    
    struct XAlertAction { var message: String; var void: (()->Void) }
    static func myXAlert(_ title: String? = nil, actions: [XAlertAction]) -> UIAlertController {
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for action in actions{ alert.addAction(UIAlertAction.init(title: action.message, style: .default, handler: { _ in action.void() })) }
        alert.addAction(UIAlertAction.init(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        return alert
    }
    
    static func delegateAlert(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("alert_input_delegation".localized())
        alert.addButton("button_add_delegation".localized()) {
            _ = DataManager.shared.delegate_Req(txt.text)
            .subscribe(onNext: { r in
                successAlert("alert_delegation_success".localized())
                _ = Observable<Int>.timer(.milliseconds(500), scheduler: MainScheduler.init())
                .subscribe(onNext: { t in  DataManager.shared.updateAffects() })
            });
        }
        alert.showEdit("title_add_site".localized(), subTitle: "alert_delegation_desc".localized(), closeButtonTitle: "N_BUTTON_CANCEL".localized(),
                       colorStyle: UIColor.hubMainColorUINT, animationStyle: .topToBottom)
    }
    
    static func renameXAlert(text: String, asciiCapable: Bool = false, _ listener: @escaping ((_ text: String)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("alert_input_name".localized())
        txt.text = text
        if asciiCapable { txt.keyboardType = .asciiCapable }
        alert.addButton("RENAME".localized()) {
            if txt.text?.count ?? 0 == 0 {
                errorAlert("error_input_name".localized())
            }else{
                listener(txt.text!)
            }
        }
        alert.showEdit("title_rename".localized(), subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized(), colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func changePasAlert(_ listener: @escaping ((_ text: String?)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("alert_input_password".localized())
        alert.addButton("PLA_CHANGE_PASS".localized()) { listener(txt.text) }
        alert.showEdit("title_change_password".localized(), subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized(), colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func ussdAlert(_ device: Int64){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("")
        txt.text = "*100#"
        txt.keyboardType = .numbersAndPunctuation
        alert.addButton("ADB_USSD".localized()) {
            _ = DataManager.shared.ussd(device: device, text: txt.text ?? "*100#").subscribe()
        }
        alert.showWait("USSD_DIALOG_TITLE".localized(), subTitle: "USSD_DIALOG_MESSAGE".localized(), closeButtonTitle: "N_BUTTON_CANCEL".localized())
    }
    
    static func deleteAlert(_ listener: @escaping (()->Void), text: String, canceled: (()->Void)? = nil){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton("N_DELETE".localized()) { listener() }
        alert.showWarning(text, subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized())
    }
    
    static func deleteXAlert(text: String, canceled: (()->Void)? = nil, actionText: String = "N_DELETE".localized(),_ listener: @escaping (()->Void)) {
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(actionText) { listener() }
        alert.showWarning(text, subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized())
    }
    
    static func alertDatePicker(_ listener: @escaping (( _ date: Date)->Void), max: Int64, min: Int64, _default: Int64? = nil) -> UIAlertController {
        let datePicker: UIDatePicker = UIDatePicker()
//        datePicker.frame = CGRect(x: 10, y: 30, width: UIDevice.current.userInterfaceIdiom == .pad ? 250 : UIScreen.main.bounds.size.width - 40, height: UIDevice.current.userInterfaceIdiom == .pad ? 170 : 190)
        datePicker.locale = Locale(identifier: DataManager.defaultHelper.language)
        datePicker.timeZone = NSTimeZone.local;datePicker.datePickerMode = .date;datePicker.backgroundColor = UIColor.clear
        if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels }
        if let _default = _default { datePicker.date = Date.init(timeIntervalSince1970: TimeInterval(_default)) }
        datePicker.minimumDate = Date.init(timeIntervalSince1970: TimeInterval(min))
        datePicker.maximumDate = Date.init(timeIntervalSince1970: TimeInterval(max))
        let message = "\n\n\n\n\n\n\n\n"
        let alert = UIAlertController(title: "alert_select_date".localized(), message: message, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet )
        alert.isModalInPopover = true
        alert.view.addSubview(datePicker)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.topAnchor.constraint(equalTo: alert.view.topAnchor, constant: 30).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 170).isActive = true
        datePicker.centerXAnchor.constraint(equalTo: alert.view.centerXAnchor).isActive = true
        alert.addAction(UIAlertAction(title: "button_select".localized(), style: .default, handler: {_ in
            listener(datePicker.date)
        }))
        alert.addAction( UIAlertAction(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        return alert
    }

    static func progress(_ title: String, text: String, canceled: (()->Void)? = nil) -> SCLAlertView{
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
//        if canceled == nil {
//            alert.show = false
//        }
        alert.showWait( title, subTitle: text, closeButtonTitle: "N_BUTTON_CANCEL".localized())
        return alert
    }
}

// Helper function inserted by Swift 4.2 migrator.
func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
