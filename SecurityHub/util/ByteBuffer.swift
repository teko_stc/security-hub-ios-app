//
//  ByteBuffer.swift
//  SecurityHub
//
//  Created by Daniil on 08.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

public class ByteBuffer {
    
    private var array = [UInt8] ()
    private var size: Int = 8

    private var currentEndianness: Endianness = .little

    init(size: Int) {
        self.size = size
        
        array.reserveCapacity(size)
    }

    public func order(_ endianness: Endianness) {
        currentEndianness = endianness
    }

    public func put(_ value: UInt8) {
        array.append(value)
    }
    
    public func put(_ value: Int) {
        array.append(contentsOf: to(currentEndianness == .little ? value.littleEndian : value.bigEndian))
    }

    public func getArray() -> [UInt8] {
        return [UInt8] (Data(bytes: array, count: size))
    }

    private func to<T>(_ value: T) -> [UInt8] {
        var value = value
        
        return withUnsafeBytes(of: &value, Array.init)
    }
    
    public enum Endianness {
        case little
        case big
    }
}
