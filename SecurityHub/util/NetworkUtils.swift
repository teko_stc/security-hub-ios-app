//
//  NetworkUtils.swift
//  SecurityHub
//
//  Created by Daniil on 18.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class NetworkUtils {
 
    private struct NetworkInterfaceInfo {
        let name: String
        let ip: String
    }
    
    private static func getInterfacesInfo() -> [NetworkInterfaceInfo] {
        var interfaces = [NetworkInterfaceInfo] ()
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            
            while (ptr != nil) {
                let flags = Int32(ptr!.pointee.ifa_flags)
                var addr = ptr!.pointee.ifa_addr.pointee

                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                        var mask = ptr!.pointee.ifa_netmask.pointee
                        let zero  = CChar(0)
                        var hostname = [CChar](repeating: zero, count: Int(NI_MAXHOST))
                        var netmask =  [CChar](repeating: zero, count: Int(NI_MAXHOST))
                        
                        if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            let address = String(cString: hostname)
                            let name = ptr!.pointee.ifa_name!
                            let ifname = String(cString: name)

                            if (getnameinfo(&mask, socklen_t(mask.sa_len), &netmask, socklen_t(netmask.count), nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                                let info = NetworkInterfaceInfo(name: ifname, ip: address)
                                interfaces.append(info)
                            }
                        }
                    }
                }
                
                ptr = ptr!.pointee.ifa_next
            }
            
            freeifaddrs(ifaddr)
        }
        
        return interfaces
    }
    
    public static func getWiFiAddress() -> String? {
        return getInterfacesInfo().filter { $0.name.starts(with: "en") }.first?.ip
    }
    
    public static func validateIpAddress(ip: String) -> Bool {
        var sin = sockaddr_in()
        var sin6 = sockaddr_in6()

        if ip.withCString({ cstring in inet_pton(AF_INET6, cstring, &sin6.sin6_addr) }) == 1 {
            return true
        } else if ip.withCString({ cstring in inet_pton(AF_INET, cstring, &sin.sin_addr) }) == 1 {
            return true
        }

        return false
    }
}
