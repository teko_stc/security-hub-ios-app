//
//  NotificationUtil.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation

class HubNotification{
    public static let connectionStateChange = NSNotification.Name(rawValue: "connectionStateChange");
    public static let SITES = NSNotification.Name(rawValue: "SITES");
    public static func delegationUpdate(_ site: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "delegationUpdate" + String(site))}
//    public static func sectionsUpdate(_ device: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "sectionsUpdate" + String(device))}
//    public static func deviceUpdate(_ site: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "deviceUpdate" + String(site))}
//    public static func zonesUpdate(_ section: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "zonesUpdate" + String(section))}
    public static func hubUserUpdate(_ deviceSiteId: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "hubUserUpdate" + String(deviceSiteId))}
    //public static let affect = NSNotification.Name(rawValue: "affect");
    public static func ivVideo() -> Notification.Name{ return NSNotification.Name(rawValue: "ivVideo") }
    public static func ivVideo(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "ivVideo" + String(id))}
    public static func ivVideo(camId: String) -> Notification.Name{ return NSNotification.Name(rawValue: "ivVideo" + camId)}
    public static let operatorsUpdate = NSNotification.Name(rawValue: "operatorsUpdate");
    public static func deviceUsersUpdate(_ site: Int64) -> Notification.Name{ return NSNotification.Name(rawValue:"deviceUsersUpdate" + String(site)) }
    public static func cellHeight(_ cellId: String) -> Notification.Name{ return NSNotification.Name(rawValue: cellId) }
    
    public static func commandWhithId(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "commandWhithId" + String(id)) }
    public static func commandWhithResultId(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "commandWhithResultId" + String(id))}

    
    public static func INTERACTIVE_MODE_START(_ device: Int64) -> Notification.Name { return NSNotification.Name(rawValue: "INTERACTIVE_MODE_START" + String(device)) }
    public static func INTERACTIVE_MODE_END(_ device: Int64) -> Notification.Name { return NSNotification.Name(rawValue: "INTERACTIVE_MODE_END" + String(device)) }
    
    public static let eventUpdate       = NSNotification.Name(rawValue: "eventUpdate")
		public static let eventsLoadEnd       = NSNotification.Name(rawValue: "eventsLoadEnd")
    public static let iviAuth           = NSNotification.Name(rawValue: "iviAuth")
    public static let eventHistoryUpdate      = NSNotification.Name(rawValue: "eventHistoryUpdate")
    public static let objectListStructUpdate = NSNotification.Name(rawValue: "objectListStructUpdate")
    
    ///////    ///////    ///////    ///////    ///////    ///////    ///////    ///////

    public static let siteUpdate        = NSNotification.Name(rawValue: "siteUpdate")
    public static let deviceUpdate      = NSNotification.Name(rawValue: "deviceUpdate")
    public static let sectionsUpdate    = NSNotification.Name(rawValue: "sectionsUpdate")
    public static let zonesUpdate       = NSNotification.Name(rawValue: "zonesUpdate")
    public static let navigation        = NSNotification.Name(rawValue: "navigation")
    public static let notReadyZone      = NSNotification.Name(rawValue: "notReadyZone")
    public static let scriptUpdate       = NSNotification.Name(rawValue: "scriptUpdate")

    
    public static func REFERENCES(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "REFERENCES" + String(id)) }
    public static func COMMAND_RESULT_UPD(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "COMMAND_RESULT_UPD" + String(id)) }
    public static func COMMAND_RESULT(_ id: Int64) -> Notification.Name{ return NSNotification.Name(rawValue: "COMMAND_RESULT" + String(id)) }
}

extension NSNotification.Name {
    public static let eventHistoryUpdate        = NSNotification.Name(rawValue: "eventHistoryUpdate")
    public static let objectListStructUpdate    = NSNotification.Name(rawValue: "objectListStructUpdate")
}

enum NUpdateType{
    case insert, delete, update
}

struct NSiteEntity {
    let id: Int64
    let site: Sites?
    let type: NUpdateType
}

struct NDeviceEntity {
    let id: Int64
    let siteIds: [Int64]
    let siteNames: String
    let device: Devices?
    let type: NUpdateType
}

struct NSectionEntity {
    let id: Int64
    let siteIds: [Int64]
    let siteNames: String
    let deviceId: Int64
    let deviceName: String
    let configVersion: Int64
    let cluster_id: Int64
    let sectionId: Int64
    let section: Sections
    let type: NUpdateType
}

struct NZoneEntity {
    let id: String
    let siteIds: [Int64]
    let siteNames: String
    let deviceId: Int64
    let deviceName: String
    let configVersion: Int64
    let cluster: Int64
    let sectionId: Int64
    let sectionName: String
    let sectionDetector: Int64
    let sectionArm: Int64
    let zoneId: Int64
    let zoneUid: Int64
    let zoneDetector: Int64
    let zone: Zones
    let type: NUpdateType    
}

struct NNotReadyZone {
    let zi: DZoneWithSitesDeviceSectionInfo
    let affect_decs: String
}

struct NCommandResult {
    var result: Int64
    var value: [String : Any] = [:]
}

struct NScriptEntity {
    let deviceId: Int64
    let configVersion: Int64 //
    let id: Int64
    let uid: String
    let bind: Int64
    let zoneName: String //
    let enabled: Bool
    let params: [[String : Any]]
    let extra: [String : AnyObject]
    let name: String
    let type: NUpdateType
}
