//
//  SimpleTouch.swift
//  SimpleTouch
//
//  Created by Hamish Rickerby on 19/10/2015.
//  Copyright © 2015 Simple Machines. All rights reserved.
//
import LocalAuthentication

public typealias TouchIDPresenterCallback = (TouchIDResponse) -> Void

public struct SimpleTouch {
    
    public static var hardwareSupportsTouchID: TouchIDResponse {
        let response = evaluateTouchIDPolicy
        guard response != TouchIDResponse.error(.touchIDNotAvailable) else {
            return response
        }
        return .success
    }
    
    public static var isTouchIDEnabled: TouchIDResponse {
        return evaluateTouchIDPolicy
    }
    
    fileprivate static var evaluateTouchIDPolicy: TouchIDResponse {
        let context = LAContext()
        var error: NSError?
        guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            return TouchIDResponse.error(TouchIDError.createError(error))
        }
        return TouchIDResponse.success
    }
    
    public static func presentTouchID(_ reason: String, callback: @escaping TouchIDPresenterCallback) {
        let context = LAContext()
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { _, error in
            guard error == nil else {
                DispatchQueue.main.async(execute: {
                    callback(.error(TouchIDError.createError(error as NSError?)))
                })
                return
            }
            DispatchQueue.main.async(execute: {
                callback(.success)
            })
        }
    }
    
}

public enum TouchIDError: Error {
    // An error we may get when we don't have a real error, but policy evaluation fails.
    // Will hopefully never happen.
    case undeterminedState
    
    // Error when the code is not a defined LAError type.
    // Will hopefully never happen.
    case unknownError(NSError)
    
    // LAError cases
    case appCancel
    case authenticationFailed
    case invalidContext
    case passcodeNotSet
    case systemCancel
    case touchIDLockout
    case touchIDNotAvailable
    case touchIDNotEnrolled
    case userCancel
    case userFallback
    
    internal static func createError(_ error: NSError?) -> TouchIDError {
        guard let tidError = error else {
            return TouchIDError.undeterminedState
        }
        
        guard let type: LAError.Code = tidError.code.toEnum() else {
            return TouchIDError.unknownError(tidError)
        }
        
        switch type {
        case .appCancel:
            return TouchIDError.appCancel
        case .authenticationFailed:
            return TouchIDError.authenticationFailed
        case .invalidContext:
            return TouchIDError.invalidContext
        case .passcodeNotSet:
            return TouchIDError.passcodeNotSet
        case .systemCancel:
            return TouchIDError.systemCancel
        case .touchIDLockout:
            return TouchIDError.touchIDLockout
        case .touchIDNotAvailable:
            return TouchIDError.touchIDNotAvailable
        case .touchIDNotEnrolled:
            return TouchIDError.touchIDNotEnrolled
        case .userCancel:
            return TouchIDError.userCancel
        case .userFallback:
            return TouchIDError.userFallback
        case .notInteractive:
            return TouchIDError.touchIDNotEnrolled
        default:
            return TouchIDError.touchIDNotEnrolled
        }
    }
    
}

extension TouchIDError: Equatable {}

public func ==(lhs: TouchIDError, rhs: TouchIDError) -> Bool {
    switch (lhs, rhs) {
    case (.undeterminedState, .undeterminedState):
        return true
    case (let .unknownError(e1), let .unknownError(e2)):
        return e1 == e2
    case (.appCancel, .appCancel):
        return true
    case (.authenticationFailed, .authenticationFailed):
        return true
    case (.invalidContext, .invalidContext):
        return true
    case (.passcodeNotSet, .passcodeNotSet):
        return true
    case (.systemCancel, .systemCancel):
        return true
    case (.touchIDLockout, .touchIDLockout):
        return true
    case (.touchIDNotAvailable, .touchIDNotAvailable):
        return true
    case (.touchIDNotEnrolled, .touchIDNotEnrolled):
        return true
    case (.userCancel, .userCancel):
        return true
    case (.userFallback, .userFallback):
        return true
    default:
        return false
    }
}

public enum TouchIDResponse {
    case success
    case error(TouchIDError)
}

extension TouchIDResponse: Equatable {}

public func == (lhs: TouchIDResponse, rhs: TouchIDResponse) -> Bool {
    switch (lhs, rhs) {
    case (.success, .success):
        return true
    case (let .error(e1), let .error(e2)):
        return e1 == e2
    default:
        return false
    }
}

extension Int {
    internal func toEnum<Enum: RawRepresentable>() -> Enum? where Enum.RawValue == Int {
        return Enum(rawValue: self)
    }
}
