//
//  String+MD5.swift
//  Unistroy
//
//  Created by Anton Savelev on 25.04.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import Foundation

extension String {
    var md5: String {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, self, CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate()
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
    
    func trim() -> String { return self.trimmingCharacters(in: NSCharacterSet.whitespaces) }
    
    func toJson() -> [String:AnyObject] {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject] ?? [:]
            } catch { }
        }
        return [:]
    }
    func toJsonArray() -> [[String:Any]] {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String:Any]] ?? []
            } catch { }
        }
        return []
    }
}

extension Int64 {
    func getDateStringFromUnixTime(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeStyle = timeStyle
        return dateFormatter.string(from: Date(timeIntervalSince1970: Double(self)))
    }
    
    var int: Int {
        get {
            Int(self)
        }
    }
}


extension Int {
    var int64: Int64 {
        get {
            Int64(self)
        }
    }
    var string: String {
        get {
            String(self)
        }
    }
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        if r.upperBound > self.count { return "" } 
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start ..< end])
    }
    
}

extension String {
    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }
}

extension String {
    var hexaToArray: [UInt32] {
        let hexa = Array(self)
        var bytesArray = stride(from: 0, to: self.count, by: 2).compactMap { UInt32(String(hexa[$0..<$0.advanced(by: 2)]), radix: 16) }
        bytesArray = bytesArray.reversed()
        return bytesArray
    }
    var hexaToInt: Int {
        let hexa = Array(self)
        var bytesArray = stride(from: 0, to: self.count, by: 2).compactMap { UInt8(String(hexa[$0..<$0.advanced(by: 2)]), radix: 16) }
        bytesArray = bytesArray.reversed()
        var value : Int = 0
        for byte in bytesArray {
            value = value << 8
            value = value | Int(byte)
        }
        return value
    }
}

extension Data
{
    public func toString() -> String
    {
        return String(data: self, encoding: .utf8)!
    }
}

extension Array {
    var lastItem: Element? {
        get {
            if count == 0 { return nil }
            return self[count - 1]
        }
        set {
            if count == 0 || newValue == nil { return }
            self[count - 1] = newValue!
        }
    }
    
    func get(index: Int) -> Element? {
        if (count <= index) { return nil }
        return self[index]
    }
    
    func join(_ array: [Element]) -> [Element] {
        var _self = self
        _self.append(contentsOf: array)
        return _self
    }
}

extension String {
    func toInt() -> Int? {
        Int(self)
    }
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    var noon2: Date {
        return Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
