//
//  Date+Localtime.swift
//  SecurityHub
//
//  Created by Anton Savelev on 19.07.17.
//  Copyright © 2017 Tattelecom. All rights reserved.
//

import Foundation

extension Date {
    
    static func date(withServerTimestamp timestamp: Int) -> Date {
        let unixtime = timestamp + 978307200
        return Date(timeIntervalSince1970: TimeInterval(unixtime))
    }
    
    func timestamp() -> Int {
        let unixtime = self.timeIntervalSince1970
        return Int(unixtime) - 978307200
    }
    
}

extension DateFormatter {
    static func defaultFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss dd.MM.yyyy"
        return formatter
    }
    
    static func shortFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }
}
