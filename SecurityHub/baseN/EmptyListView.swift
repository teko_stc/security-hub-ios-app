//
//  CameraListView.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//


import UIKit

class EmptyListView: XBaseView {
    
    lazy var table : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        view.backgroundColor = DEFAULT_BACKGROUND
        return view
    }()
    
    var refreshControl: UIRefreshControl = { return UIRefreshControl() }()
    
    override func setContent() {
        table.backgroundView = refreshControl
        xView.addSubview(table)
    }
    
    override func setConstraints() {
        table.snp.remakeConstraints{make in
            make.top.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
}

