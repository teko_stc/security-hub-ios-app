//
//  BaseController.swift
//  SecurityHub
//
//  Created by Timerlan on 27.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import UIKit
import RappleProgressHUD
import AudioToolbox

class BaseController<T: BaseView>: UIViewController {
    var mainView: T! { return view as? T }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    override func loadView() {
        super.loadView()
//        navigationController?.navigationBar.barTintColor = UIColor.hubMainColor
//        navigationController?.navigationBar.backgroundColor = UIColor.hubMainColor
        navigationController?.navigationBar.tintColor = DEFAULT_COLOR_MAIN //DEFAULT_SELECTED
        navigationController?.navigationBar.isTranslucent = false
//        navigationController?.navigationBar.shadowImage = UIImage() // IOS 11
        navigationController?.navigationBar.barStyle = UIBarStyle.default //UIBarStyle.black
        view = T()
        
        let backButton = UIBarButtonItem()
        backButton.tintColor = DEFAULT_SELECTED
        backButton.title = "SCRIPT_SET_BACK".localized()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        mainView.nV = navigationController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    public func rightButton(_ img: UIImage? = XImages.view_plus?.withColor(color: DEFAULT_SELECTED)) {
        let rightButton = ZFRippleButton()
        rightButton.rippleBackgroundColor = UIColor.hubMainColor
        rightButton.rippleColor = UIColor.hubDarkMainColor
        rightButton.ripplePercent = 1
        rightButton.shadowRippleEnable = false
        rightButton.setImage( img, for: UIControl.State.normal)
        rightButton.addTarget(self, action: #selector(rightClick), for: UIControl.Event.touchUpInside)
        if #available(iOS 11, *) { rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        }else{ rightButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40) }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton)
    }
    @objc func rightClick(){}
    
    func showLoader(){ RappleActivityIndicatorView.startAnimating() }
    
    func hideLoader(){ RappleActivityIndicatorView.stopAnimation() }
    
    func vibrate(){ AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) }
}



