//
//  BaseListView.swift
//  SecurityHub test
//
//  Created by Timerlan on 13.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import FoldingCell

class BaseFoldingListView<OPEN : RotatedView, CLOSE: UIView,T:BaseFoldingCell<OPEN,CLOSE>>: BaseView, UITableViewDelegate, UITableViewDataSource {
    private var objs: [FoldingElement] = []
    private var cellId = ""
    private var closeHeight: CGFloat = T().closeHeight
    private var foldingListener: FoldingListener?
    
    private lazy var refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        view.addTarget(self, action: #selector(update), for: UIControl.Event.valueChanged)
        return view
    }()
    
    private lazy var table : UITableView = {
        let view = UITableView()
        view.estimatedRowHeight = closeHeight
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none;
        view.backgroundColor = UIColor.hubBackgroundColor
        view.backgroundView = refreshControl
        view.delegate = self
        view.dataSource = self
        return view
    }()
    @objc func update(){
        DataManager.shared.updateAffects()
        DataManager.shared.updateSites()
    }
    
    override func setContent() {
        contentView.alwaysBounceVertical = false
        contentView.addSubview(table)
    }
    override func updateConstraints() {
        super.updateConstraints()
        table.snp.remakeConstraints{make in
            make.top.left.right.equalTo(0)
            make.width.height.height.equalToSuperview()
        }
    }
    
    func initTable(cellId: String, foldingListener: FoldingListener){
        self.cellId = cellId
        self.foldingListener = foldingListener
        table.register(T.self, forCellReuseIdentifier: cellId)
        table.reloadData()
    }
    
    func updateElement(_ obj: FoldingElement, type: UpdateType) {
        let anim: UITableView.RowAnimation = DataManager.settingsHelper.needAnimation ? .automatic : .none
        refreshControl.endRefreshing()
        switch type{
        case .insert:
            self.objs.append(obj)
            table.beginUpdates();table.insertRows(at: [IndexPath(row: self.objs.count - 1, section: 0)], with: anim); table.endUpdates()
        case .update:
            guard let pos = self.objs.index(where: {$0.id == obj.id}) else{ break }
            self.objs[pos].openHeight = obj.openHeight
            table.beginUpdates();table.reloadRows(at: [IndexPath(row: pos, section: 0)], with: anim); table.endUpdates()
        case .delete:
            guard let pos = self.objs.index(where: {$0.id == obj.id}) else{ break }
            self.objs.remove(at: pos)
            table.beginUpdates();table.deleteRows(at: [IndexPath(row: pos, section: 0)], with: anim); table.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return objs.count }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return objs[indexPath.row].isOpen ? objs[indexPath.row].openHeight : closeHeight }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { return tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! T }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as T = cell else { return }
        cell.unfold(objs[indexPath.row].isOpen, animated: false, completion: nil)
        cell.setConstraints(openHeight: objs[indexPath.row].openHeight)
        foldingListener?.setContent(cell: cell, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath as IndexPath) as! FoldingCell
        objs[indexPath.row].isOpen = !objs[indexPath.row].isOpen
//        if objs[indexPath.row].openHeight >= closeHeight * 2 {
//            cell.unfold(objs[indexPath.row].isOpen, animated: true, completion: nil)
//        }else{
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                cell.unfold(self.objs[indexPath.row].isOpen, animated: false, completion: nil)
            })
//        }
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: { () -> Void in tableView.beginUpdates();tableView.endUpdates() }, completion: nil)
        //tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
}

struct FoldingElement {
    let id: Int64
    var isOpen: Bool
    var openHeight: CGFloat
}

protocol FoldingListener: class {
    func setContent<T>(cell: T, index: Int)
}
