//
//  BaseView.swift
//  SecurityHub
//
//  Created by Timerlan on 27.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class BaseView: UIView{
    var connectionStateChange: Any?
    var nV: UINavigationController?
    
    lazy var contentView: UIScrollView = {
        var view = UIScrollView()
        view.backgroundColor = UIColor.hubBackgroundColor
        view.alwaysBounceVertical = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setBottomBackgroundColor(UIColor.black)
        addSubview(contentView)
        setContent()
        updateConstraints()
        connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main){ notification in
            let code = notification.object as? Int ?? 0
            if code == -6 {
                NavigationHelper.shared.show(LoginController())
                return
            }
        }
    }
    
    var hhh = -24
    public func removeConnactionStateChange(){
        if connectionStateChange != nil {
            NotificationCenter.default.removeObserver(connectionStateChange!)
            connectionStateChange = nil
        }
        hhh = 0
    }
    
    public func setBottomBackgroundColor(_ color: UIColor){ backgroundColor = color}
    
    func setContent(){}
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        contentView.snp.remakeConstraints{make in
            make.edges.equalToSuperview()
        }
    }
    
    func setNV(_ nV: UINavigationController?) {
        self.nV = nV
    }
}


