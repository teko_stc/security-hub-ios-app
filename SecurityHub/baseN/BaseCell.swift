//
//  BaseCell.swift
//  SecurityHub
//
//  Created by Timerlan on 05.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class BaseCell<T: UIView>: UITableViewCell {
    var mainView = T()
    var nV: UINavigationController?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        selectionStyle = .none
//        backgroundColor = UIColor.white
//        contentView.backgroundColor = UIColor.white
        contentView.addSubview(mainView)
        mainView.snp.remakeConstraints{make in make.top.leading.bottom.trailing.equalTo(contentView)}
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    open func setContent(_ obj: SQLCommand, isEnd: Bool = false){}
    
    func setNV(_ nV: UINavigationController?) {
        self.nV = nV
    }
}
