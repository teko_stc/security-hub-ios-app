//
//  AppDelegate.swift
//  SecurityHub
//
//  Created by Admin on 21.06.17.
//  Copyright © 2017 Tattelecom. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Foundation
import RxSwift
import GoogleSignIn
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    internal var window: UIWindow?
		var orientationLock: UIInterfaceOrientationMask = UIDevice.current.userInterfaceIdiom == .pad ? .all : .portrait
    private var timeLock: Double?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DataManager.shared = DataManager()
        self.window = NavigationHelper.shared.window
        NavigationHelper.shared.show(XSplashController())
        UIApplication.shared.applicationIconBadgeNumber = 0
				GMSServices.provideAPIKey(GOOGLE_MAPS_API_KEY)
			
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if let timeLock = timeLock, (timeLock + 30) < Date().timeIntervalSince1970, DataManager.settingsHelper.needPin {
            NavigationHelper.shared.showNew(XSplashController())
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        timeLock = Date().timeIntervalSince1970
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask { return orientationLock }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        // Override point for customization after application launch.
        registerForRichNotifications()
    }
    
    func registerForRichNotifications() {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            if granted {
                print("Permission granted")
            } else {
                print("Permission not granted")
            }
        }
        
        //actions defination
        let action1 = UNNotificationAction(identifier: "action1", title: "Action First", options: [.foreground])
        let action2 = UNNotificationAction(identifier: "action2", title: "Action Second", options: [.foreground])
        
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [action1,action2], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
    }
	
		func application(_ app: UIApplication,
			open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool {
			var handled: Bool

			handled = GIDSignIn.sharedInstance.handle(url)
			if handled {
				return true
			}
			
			return false
		}
}

//Need to FCM PushHelper.swift
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        UIApplication.shared.applicationIconBadgeNumber = 1
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        UIApplication.shared.applicationIconBadgeNumber = 1
        print("Firebase registration token: \(fcmToken)")
    }
    @objc(applicationReceivedRemoteMessage:) func application(received remoteMessage: MessagingRemoteMessage) {
        print("Firebase \(remoteMessage.appData)" )
    }
}
//Need to FCM PushHelper.swift
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}


struct AppUtility {
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate { delegate.orientationLock = orientation }
    }
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
}
