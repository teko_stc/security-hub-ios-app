//
//  RegistrationConstants.swift
//  SecurityHub
//
//  Created by Stefan on 11.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

let REG_VK_APP_ID = "51877255"
let REG_VK_APP_SEC_KEY = "qm6TQCNTCqb14fc9E244"

enum RegResultCode: Int {
	case SUCCESS = 1
	case CANCEL = 2
	case ERR_NET = -1
	case ERR_JSON = -2
	case ERR = -101
	case ERR_USER_EXISTS = -102
	case ERR_DB = -106
	case ERR_DOMAIN_EXISTS = -107
	case ERR_WRONG_LOGIN = -108
	case ERR_USER_PHONE_EXISTS = -201
	case ERR_WRONG_CODE = -202
	case ERR_USER_PHONE_DOESNT_EXIST = -203
	case ERR_CODE_EXISTS = -204
	case ERR_CURL = -205
	case ERR_BAN = -206
	case ERR_CODE_DOESNT_EXIST = -207
	case ERR_CODE_EXPIRED = -208
	case ERR_DEVICE_DOESNT_EXIST = -301
	case ERR_WRONG_PIN = -302
	case ERR_DEVICE_TAKEN = -303
	case ERR_DEVICE_BAN = -304
	case ERR_DEVICE_NOT_TAKEN = -305
	case ERR_TOKEN_NOT_FOUND = -306
	case ERR_WRONG_ANSWERS = -307
	case ERR_LOGIN_MISMATCH = -309
	case ERR_TOKEN_EXPIRED = -310
	case ERR_TOKEN_BAN = -311
	case ERR_NOT_BINDED_TO_SITE = -312
	case ERR_BAD_CREDENTIALS = -401
	case ERR_SOCIAL_MISMATCH = -402
	case ERR_SOCIAL_NOT_FOUND = -403
	case ERR_UNKNOWN = -1000
}

struct RegUserData
{
	var phone: UInt = 0
	var sn: UInt = 0
	var code: UInt = 0
	var token = ""
	var name = ""
	var login = ""
	var passwordHash = ""
	var pinHash = ""
}

struct RegResponce
{
	var result: RegResultCode = .SUCCESS
	var errorCode: Int = 0
	var token: String = ""
	var timeout: Int = 0
	var message = ""
	var localizedMessage = ""
}
