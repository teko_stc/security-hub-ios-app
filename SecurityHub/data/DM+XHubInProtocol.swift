//
//  DM+XHubInProtocol.swift
//  SecurityHub
//
//  Created by Timerlan on 23/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

extension DataManager : XHubInProtocol {
    func pushMask() -> Int { return DataManager.settingsHelper.pushMask }
    func lastEventTime() -> Int64 { return dbHelper.getLastEventTime() }
    func fcmToken() -> String { return pushHelper?.getFcmToken() ?? "" }
    func getReferenceName(_ command_id: Int64) -> String? { return dbHelper.getReferenceName(command_id)}
    func language() -> Int {return DataManager.defaultHelper.lang_iso}
}
