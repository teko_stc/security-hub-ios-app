//
//  DataManager+Hub.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift
import Foundation

extension DataManager {
    public func SECTION_ZONE_RMV(obj: HubSectionZone, time: Int64){
        updateSites()
				if obj.zone == 0 && obj.autoarmId < 1
					{ removeAutoArm(device: obj.device, section: obj.section) }
//        if obj.zone == 0, let site_id = obj.site { dbHelper.removeSection(site_id: site_id, device_id: obj.device, section_id: obj.section) }
//        else if obj.zone != 0 { dbHelper.clearZones(device_id: obj.device, section_id: obj.section, zone_id: obj.zone) }
        //TODO remove events
    }
		
    public func SECTION_ZONE_UPD(obj: HubSectionZone, time: Int64){
        //updateSites()
			
//        hubHelper.setCommand(HubCommand.SITES, D: ["site_id" : obj.])
        if obj.zone == 0 {
					//DispatchQueue.main.async {
						self.dbHelper.upd(section: CastHelper.section(obj, time: time)) //}
					if obj.autoarmId < 1 { removeAutoArm(device: obj.device, section: obj.section) } }
        else { dbHelper.add(zone: CastHelper.zone(obj, time: time)) }
    }
    
    public func SITE_RMV(siteId: Int64){
        updateSites()
				removeAutoArm(site: siteId)
//        dbHelper.removeSite(id: siteId)
        //TODO remove events
    }
    
    func SITE_UPD(obj: HubSite, time : Int64){
        updateSites()
//        siteUpdate(obj, time: time)
//        clearOld(site_id: obj.id, time: time)
    }
    
    func SITES(_ obj: HubSites, time : Int64){
        var key: CompositeDisposable.DisposeKey? = nil
        let obs = Observable.from(obj.items)
            .do(onNext: { (hSite) in self.siteUpdate(hSite, time: time) })
            .do(onCompleted: { self.clearOld(time: time) })
            .do(onCompleted: { self.dispose(key) })
            .do(onCompleted: { DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubSitesEnd"), object: nil) })
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
        key = disposables.insert(obs.subscribe())
    }
    private func siteUpdate(_ hSite: HubSite, time : Int64) {
        dbHelper.add(site: CastHelper.site(hSite, time: time, domainId: getUser().domainId))

        hSite.devices.forEach { (hDevice) in
            let device = CastHelper.device(hDevice, time: time)
            hDevice.sections.forEach { (hSection) in
                let deviceSection = CastHelper.deviceSection(hSite, hDevice, hSection, time: time)
                let section =  CastHelper.section(hDevice, hSection, time: time)
                dbHelper.add(deviceSection: deviceSection, device: device, section: section, siteName: hSite.name)
                hSection.zones.forEach { (hZone) in
                    dbHelper.add(zone: CastHelper.zone(hDevice, hSection, hZone, time: time))
                }
                
                if !HubConst.isHub(device.configVersion, cluster: device.cluster_id) {
                    dbHelper.add(zone: Zones(device: device.id, section: 0, zone: 0, name: "", delay: 0, detector: 0, physic: 0, uidType: 0, time: time, bypass: 0))
                }
            }
            //TODO
            hDevice.users.forEach{ (hUser) in
                dbHelper.add(deviceUsers: CastHelper.deviceUser(hDevice, hUser, time: time))
                hUser.sections.forEach{ (hUserSection) in
                    dbHelper.add(userSection: CastHelper.userSection(hDevice, hUser, hUserSection, time: time))
                }
            }
        }
    }
    private func clearOld(time: Int64){
        dbHelper.clearZones(time: time)
        dbHelper.clearDeviceSection(time: time)
        dbHelper.deleteDeviceUsers(site_id: nil, time: time)
        dbHelper.clearSites(time: time)
        //TODO remove events
    }
    private func clearOld(site_id: Int64, time: Int64){
        dbHelper.clearDeviceSection(site_id: site_id, time: time)
//        dbHelper.clearZones(time: time)
        dbHelper.deleteDeviceUsers(site_id: site_id, time: time)
        //TODO remove events
    }
    
    func SCRIPTS(_ obj: HubScripts, device_id: Int64, time: Int64) {
        var key: CompositeDisposable.DisposeKey? = nil
        let obs = Observable.from(obj.items)
            .map({ CastHelper.script($0, device: device_id, time: time) })
            .do(onNext: { (script) in self.dbHelper.add(script: script) })
            .do(onCompleted: { self.dbHelper.clearOldScripts(device_id: device_id, time: time) })
            .do(onCompleted: { self.dispose(key) })
        key = disposables.insert(obs.subscribe())
    }
    func SCRIPT(_ obj: HubScript, time: Int64) {
        var key: CompositeDisposable.DisposeKey? = nil
        let obs = Observable.just(obj)
            .map({ CastHelper.script($0, device: obj.device_id ?? 0, time: time) })
            .do(onNext: { (script) in self.dbHelper.add(script: script) })
            .do(onCompleted: { self.dispose(key) })
        key = disposables.insert(obs.subscribe())
    }
    func SCRIPT_RMV(device_id: Int64, bind: Int64, id: Int64) {
        dbHelper.deleteScript(device_id: device_id, bind: bind, id: id)
    }
}
