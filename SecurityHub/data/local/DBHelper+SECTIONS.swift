//
//  DBHelper+SECTIONS.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//SECTIONS
extension DBHelper{
    func upd(section: Sections) {
        let statusS = section.getDbStatus(main.db, table: main.sections)
        switch statusS {
        case .needInsert: section.insert(main.db, table: main.sections)
        case .needUpdate: section.update(main.db, table: main.sections)
        case .noNeed: section.updateTime(main.db, table: main.sections)
        }
        if statusS == .needInsert || statusS == .needUpdate, let di = getDevice(device_id: section.device) {
            for site_id in di.siteIds {
								let deviceSection = DeviceSection(site: site_id, device: section.device, section: section.section, parted: 0)
                switch deviceSection.getDbStatus(main.db, table: main.deviceSection) {
                case .needInsert: deviceSection.insert(main.db, table: main.deviceSection)
                case .needUpdate: deviceSection.update(main.db, table: main.deviceSection)
                case .noNeed: deviceSection.updateTime(main.db, table: main.deviceSection)
                }
            }
            let nS = NSectionEntity(id: section.id, siteIds: di.siteIds, siteNames: di.siteNames, deviceId: section.device, deviceName: di.device.name, configVersion: di.device.configVersion, cluster_id: di.device.cluster_id, sectionId: section.section, section: section, type: statusS == .needInsert ? .insert : .update)
            NotificationCenter.default.post(name: HubNotification.sectionsUpdate, object: nS)
        }
    }
    
    func removeSection(site_id: Int64, device_id: Int64, section_id: Int64) {
        guard let rows = try? main.db.prepare(main.deviceSection.filter(DeviceSection.site == site_id && DeviceSection.device == device_id && DeviceSection.section == section_id)) else { return }
        let relations = rows.map { (row) -> (s:Int64,d:Int64,sec:Int64) in return (s: row[DeviceSection.site], d: row[DeviceSection.device], sec: row[DeviceSection.section]) }
        guard let _ = try? main.db.run(main.deviceSection.filter(DeviceSection.device == device_id && DeviceSection.section == section_id).delete()) else { return }
        relations.forEach { (rel) in
            //TODO Не корректное удаление девайса и раздела, если будет две связи
            let nS = NSectionEntity(id: Sections.getId(rel.d, rel.sec), siteIds: [rel.s], siteNames: "", deviceId: rel.d, deviceName: "", configVersion: 0, cluster_id: 0, sectionId: rel.sec, section: Sections(device: rel.d, section: rel.sec, name: "", detector: 0, arm: 0, time: 0, autoarmId: -1), type: .delete)
            NotificationCenter.default.post(name: HubNotification.sectionsUpdate, object: nS)
            if getSectionSitesDevicesInfo(device_id: rel.d, section_id: rel.sec).siteIds.count == 0 {
                let _ = try? main.db.run(main.sections.filter(Sections.device == rel.d && Sections.section == rel.sec).delete())
            }
            if rel.sec == 0 {
                let nD = NDeviceEntity(id: rel.d, siteIds: [rel.s], siteNames: "", device: nil, type: .delete)
                NotificationCenter.default.post(name: HubNotification.deviceUpdate, object: nD)
                if getDeviceSitesInfo(device_id: rel.d).siteIds.count == 0 {
                    let _ = try? main.db.run(main.device.filter(Devices.id == rel.d).delete())
                }
            }
        }
    }
    
    func getSections(searchText: String? = nil) -> [DSectionWithSitesDevicesInfo] {
        var result: [DSectionWithSitesDevicesInfo] = []
        var searchTextQuery = ""
        if let searchText = searchText {
            searchTextQuery = "AND ( (LOWER(Sections.lname) LIKE '%\(searchText.lowercased())%') OR (LOWER(Sites.lname) LIKE '%\(searchText.lowercased())%') ) "
        }
        let query = "SELECT \(Sections.all), Sites.id, Sites.name, Devices.configVersion, Devices.cluster_id, Devices.name FROM Sites, Devices, DeviceSection, Sections WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section " +
        searchTextQuery +
        "ORDER BY Sections.id"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let sectionInfo = DataEntityMapper.sectionWithSiteDeviceInfo(row) else { break; }
            if let last = result.last, last.section.id == sectionInfo.section.id {
                result[result.count - 1].siteIds.append(sectionInfo.siteId)
                result[result.count - 1].siteNames += ", \(sectionInfo.siteName)"
            } else {
                result.append(DSectionWithSitesDevicesInfo(section: sectionInfo.section, siteIds: [sectionInfo.siteId], siteNames: sectionInfo.siteName, deviceName: sectionInfo.deviceName, configVersion: sectionInfo.configVersion, cluster_id: sectionInfo.cluster_id))
            }
        }
        return result
    }
    
    func getSections(priority: Int, searchText: String? = nil) -> [DSectionWithSitesDevicesInfo] {
        var result: [DSectionWithSitesDevicesInfo] = []
        var searchTextQuery = ""
        if let searchText = searchText {
            searchTextQuery = "AND ( (Sections.lname LIKE '%\(searchText.lowercased())%') OR (Sites.lname LIKE '%\(searchText.lowercased())%') ) "
        }
        let query1 = "SELECT \(Sections.all), Sites.id, Sites.name, Devices.configVersion, Devices.cluster_id, Devices.name, " +
            "Devices.name, Sections.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time " +
            "FROM Sites, Devices, DeviceSection, Sections " +
            "LEFT OUTER JOIN Events ON Events.affect = 1 AND Events.device = Sections.device AND Events.section = Sections.section " +
            "AND (Events._class < 5 OR (Events._class = 5 AND (Events.reason = 24 OR Events.reason = 27 OR Events.reason = 23)) OR (Events._class = 6 AND (Events.reason = 3 OR Events.reason = 4 OR Events.reason = 7)) OR (Events._class = 7 AND Events.reason = 1)) " +
            "WHERE Sites.id = \(priority) AND DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.section <> 0 " +
            searchTextQuery +
            "ORDER BY Sites.id, Sections.device, Sections.section"
        
        guard let rows = try? main.db.prepare(query1) else { return [] }
        for row in rows {
            guard   let sectionInfo = DataEntityMapper.sectionWithSiteDeviceInfo(row) else {
                break
            }

            if let last = result.last, last.section.id == sectionInfo.section.id {
                if !result[result.count - 1].siteIds.contains(sectionInfo.siteId) {
                    result[result.count - 1].siteIds.append(sectionInfo.siteId)
                    result[result.count - 1].siteNames += ", \(sectionInfo.siteName)"
                }
                if  let affect = DataEntityMapper.sectionInfoAffects(row) {
                    result[result.count - 1].affects?.append(affect)
                }
            } else {
                var se = DSectionWithSitesDevicesInfo(section: sectionInfo.section, siteIds: [sectionInfo.siteId], siteNames: sectionInfo.siteName, deviceName: sectionInfo.deviceName, configVersion: sectionInfo.configVersion, cluster_id: sectionInfo.cluster_id)
                se.affects = []
                if  let affect = DataEntityMapper.sectionInfoAffects(row) {
                    se.affects?.append(affect)
                }
                result.append(se)
            }
        }
        
        return result
    }
    
    func getSection(device_id: Int64, section_id: Int64) -> DSectionWithSitesDevicesInfo? {
        var result: DSectionWithSitesDevicesInfo?
        let query = "SELECT \(Sections.all), Sites.id, Sites.name, Devices.configVersion, Devices.cluster_id, Devices.name FROM Sites, Devices, DeviceSection, Sections WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.device = \(device_id) AND Sections.section = \(section_id)"
        
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            guard let sectionInfo = DataEntityMapper.sectionWithSiteDeviceInfo(row) else { break; }
            if result != nil {
                result!.siteIds.append(sectionInfo.siteId)
                result!.siteNames += ", \(sectionInfo.siteName)"
            } else {
                result = DSectionWithSitesDevicesInfo(section: sectionInfo.section, siteIds: [sectionInfo.siteId], siteNames: sectionInfo.siteName, deviceName: sectionInfo.deviceName, configVersion: sectionInfo.configVersion, cluster_id: sectionInfo.cluster_id)
            }
        }
        return result
    }
    
    func getSectionss(device_id: Int64) -> [DSectionWithSitesDevicesInfo] {
        var result: [DSectionWithSitesDevicesInfo] = []
        let query = "SELECT \(Sections.all), Sites.id, Sites.name, Devices.configVersion, Devices.cluster_id, Devices.name, " +
        "Devices.name, Sections.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time " +
        "FROM DeviceSection " +
        "JOIN Sites ON DeviceSection.site = Sites.id " +
        "JOIN Devices ON DeviceSection.device = Devices.id " +
        "JOIN Sections ON DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section " +
        "LEFT OUTER JOIN Events ON Events.device = DeviceSection.device AND Events.section = DeviceSection.section AND Events.affect = 1 AND (Events._class < 5 OR (Events._class = 5 AND (Events.reason = 24 OR Events.reason = 27 OR Events.reason = 23)) OR (Events._class = 6 AND (Events.reason = 3 OR Events.reason = 4 OR Events.reason = 7)) OR (Events._class = 7 AND Events.reason = 1))" +
        "WHERE DeviceSection.device = \(device_id)"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let sectionInfo = DataEntityMapper.sectionWithSiteDeviceInfo(row) else {
                continue
            }

            if let last = result.last, last.section.id == sectionInfo.section.id {
                if !result[result.count - 1].siteIds.contains(sectionInfo.siteId) {
                    result[result.count - 1].siteIds.append(sectionInfo.siteId)
                    result[result.count - 1].siteNames += ", \(sectionInfo.siteName)"
                }
                if  let affect = DataEntityMapper.sectionInfoAffects(row) {
                    result[result.count - 1].affects?.append(affect)
                }
            } else {
                var se = DSectionWithSitesDevicesInfo(section: sectionInfo.section, siteIds: [sectionInfo.siteId], siteNames: sectionInfo.siteName, deviceName: sectionInfo.deviceName, configVersion: sectionInfo.configVersion, cluster_id: sectionInfo.cluster_id)
                se.affects = []
                if  let affect = DataEntityMapper.sectionInfoAffects(row) {
                    se.affects?.append(affect)
                }
                result.append(se)
            }
        }
        return result
    }
    
    func getAffects(device_id: Int64, section_id: Int64) -> [DAffectEntity] {
        let query = "SELECT Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time From Devices, Sections, Events LEFT OUTER JOIN Zones ON Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone Where Events.device = Devices.id AND Events.device = Sections.device AND Events.section = Sections.section AND Events.device = \(device_id) AND Events.section = \(section_id) AND Events.affect = 1 AND (Events._class < 5 OR (Events._class = 5 AND (Events.reason = 24 OR Events.reason = 27 OR Events.reason = 23)) OR (Events._class = 6 AND (Events.reason = 3 OR Events.reason = 4 OR Events.reason = 7)) OR (Events._class = 7 AND Events.reason = 1)) ORDER BY Events._class"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        return rows.map { (obj) -> DAffectEntity in return DataEntityMapper.dAffect(obj: obj) }
    }
    
    func getSectionSitesDevicesInfo(device_id: Int64, section_id: Int64) -> DSectionSitesDevicesInfo {
        var result = DSectionSitesDevicesInfo(siteIds: [], siteNames: "")
        let query = "SELECT Sites.id, Sites.name FROM Sites, DeviceSection WHERE DeviceSection.site = Sites.id AND DeviceSection.device = \(device_id) AND DeviceSection.section = \(section_id)"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            if  let siteId = row[0] as? Int64,
                let siteName = row[1] as? String {
                result.siteIds.append(siteId)
                if result.siteNames.count == 0 { result.siteNames = siteName } else { result.siteNames += ", \(siteName)" }
            }
        }
        return result
    }
    
    func isHaveAlarmTenMinAgo(deviceId: Int64, sectionId: Int64) -> Bool {
        let query = "SELECT COUNT(*) From Events WHERE Events.device = \(deviceId) AND Events.section = \(sectionId) AND Events._class = \(HubConst.CLASS_ALARM) AND Events.active = 1 AND Events.dropped = 0 AND Events.time > \(Int64(NSDate().timeIntervalSince1970 - 600))"
        
        guard let scalar = try? self.main.db.scalar(query), let count = scalar as? Int64 else { return false }
        return count > 0
    }
    
    func dropLastAlarms(deviceId: Int64, sectionId: Int64) {
        let query = "UPDATE Events SET dropped = 1 WHERE Events.device = \(deviceId) AND Events.section = \(sectionId) AND Events._class = \(HubConst.CLASS_ALARM)"
        try? main.db.execute(query)
    }
    
    func getSectionName(device_id: Int64, section_id: Int64) -> String? {
        if let name = try? main.db.scalar("SELECT name From Sections Where device = \(device_id) AND section = \(section_id)") as? String { return name }
        return nil
    }
    
    func setArmSections(arm: Int64, device_id: Int64, section_ids: [Int64], time: Int64) -> [Int64] {
        var result: [Int64] = []
        if section_ids.count == 0 { return result }
        var sections_ids_string = "\(section_ids)"
        sections_ids_string.removeFirst()
        sections_ids_string.removeLast()
        let query = "SELECT section FROM Sections WHERE device = \(device_id) AND section IN (\(sections_ids_string))"
        let update_query = "UPDATE Sections SET arm = \(arm) WHERE device = \(device_id) AND section IN (\(sections_ids_string))"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows { if let section_id = row[0] as? Int64{ result.append(section_id) } }
        
        let _ = try? main.db.run(update_query)
        return result
    }
    
    func getSectionsNames(device_id: Int64) -> [Int64:DSectionNameAndTypeEntity] {
        var result: [Int64:DSectionNameAndTypeEntity] = [:]
        let query = "SELECT section, name, detector FROM Sections WHERE device = \(device_id)"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            if  let section = row[0] as? Int64,
                let name = row[1] as? String,
                let type_id = row[2] as? Int64,
                let type = HubConst.getSectionsHUBTypes().first(where: { $0.id == type_id })
            {
                result.updateValue(DSectionNameAndTypeEntity(name: name, type: type), forKey: section)
            }
        }
        return result
    }
    
    ////////////////////////////////////////////////////
    
    func get(device_id: Int64, section_id: Int64) -> Sections? {
        if let q = ((try? main.db.pluck(main.sections.filter( Sections.device == device_id && Sections.section == section_id))) as Row??),
            let row = q { return Sections.fromRow(row) }
        return nil
    }

    func getSections(site_id: Int64? = nil, device_id: Int64? = nil) -> [Sections] {
        var result: [Sections] = []
        guard let rows = try? main.db.prepare(main.deviceSection.filter(site_id == nil || DeviceSection.site == site_id ?? 0).filter(device_id == nil || DeviceSection.device == device_id ?? 0)
            .group(DeviceSection.device,DeviceSection.section).order(DeviceSection.device,DeviceSection.section)) else {
            return result
        }
        for row in rows {
            let ds: DeviceSection = DeviceSection.fromRow(row)
            if let sec = get(device_id: ds.device, section_id: ds.section){ result.append(sec) }
        }
        return result
    }
}
