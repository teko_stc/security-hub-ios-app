//
//  DefaultsHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation

class DefaultsHelper {
    private let defaults = UserDefaults.standard
    private let LOGIN_NAME = "LOGINN"
    private let PASS_NAME = "PASSWORD"
    private let PIN = "PIN"
    private let ID = "ID"
    private let DOMAIN_ID = "DOMAIN_ID"
    private let ORG_ID = "ORG_ID"
    private let ROLES = "ROLES"
    private let LANGUAGE = "LANGUAGE"
    private let LANGUAGE_ISO = "LANGUAGE_ISO"
    private let NEEDSHOWPERSONAGREEMENT = "needShowPersonAgreement"

    var login : String {
        get { return defaults.string(forKey: LOGIN_NAME) ?? "" }
        set {
            if newValue != defaults.string(forKey: LOGIN_NAME){
                set(value: newValue, forKey: LOGIN_NAME)
                DataManager.settingsHelper.setDefaults();
            }
        }
    }
    
    var password : String {
        get { return defaults.string(forKey: PASS_NAME) ?? "" }
        set { set(value: newValue, forKey: PASS_NAME)}
    }
    
    var pin : String {
        get { return defaults.string(forKey: PIN) ?? "" }
        set { set(value: newValue, forKey: PIN)}
    }
    
    var id : Int64{
        get{ return Int64(defaults.integer(forKey: ID)) }
        set{ set(value: newValue, forKey: ID) }
    }
    
    var domainId : Int64{
        get{ return Int64(defaults.integer(forKey: DOMAIN_ID)) }
        set{ set(value: newValue, forKey: DOMAIN_ID) }
    }
    
    var orgId : Int64{
        get{ return Int64(defaults.integer(forKey: ORG_ID)) }
        set{ set(value: newValue, forKey: ORG_ID) }
    }
    
    var roles : Int64{
        get{ return Int64(defaults.integer(forKey: ROLES)) }
        set{ set(value: newValue, forKey: ROLES) }
    }
    
    func getPreferredLocale() -> Locale {
        guard let preferredIdentifier = Locale.preferredLanguages.first else {
            return Locale.current
        }
        return Locale(identifier: preferredIdentifier)
    }
    
    var language : String{
        get{
            let val = defaults.string(forKey: "LANGUAGE") ?? (getPreferredLocale().languageCode ?? "en")
            switch val
            {
                case "en", "de", "es", "ru", "tr": return val
                default: return "en"
            }
        }
        set{ set(value: newValue, forKey: LANGUAGE) }
    }
    
    var lang_iso : Int {
        get{
            switch defaults.string(forKey: "LANGUAGE") ?? (getPreferredLocale().languageCode ?? "en")
            {
                case "en": return 37
                case "de": return 33
                case "es": return 148
                case "ru": return 136
                case "tr": return 168
                default: return 37
            }
            }
        set{ set(value: newValue, forKey: LANGUAGE_ISO) }
    }
    
    var domainLangIso: String {
        get{ return defaults.string(forKey: "DomainLangIso") ?? (Locale.current.languageCode ?? "en")}
        set{ set(value: newValue, forKey: "DomainLangIso") }
    }
    
    var needShowPersonAgreement: Bool {
        get { !defaults.bool(forKey: NEEDSHOWPERSONAGREEMENT) }
        set { set(value: !newValue, forKey: NEEDSHOWPERSONAGREEMENT) }
    }
    
    public func setUserInfo(id: Int64, domainId: Int64, orgId: Int64, roles: Int64, domainLangIso: String){
        self.id = id
        self.domainId = domainId
        self.orgId = orgId
        self.roles = roles
        self.domainLangIso = domainLangIso
    }

    public func clear(){
        login = ""
        password = ""
        pin = ""
        id = 0
        domainId = 0
        orgId = 0
        roles = 0
        domainLangIso = ""
    }
    
    private func set(value: Any, forKey: String) {
        defaults.set(value, forKey: forKey)
        defaults.synchronize()
    }
}

class Roles{
    //static let ORG_ADMIN: Int64  = 16
		static let ORG_ADMIN: Int64  = 0 // Disabled for mobile phone
    static let ORG_BKS_OPER: Int64  = 32
    static let ORG_LAYER: Int64  = 64
    static let ORG_BUH: Int64  = 128 //no
    static let ORG_INGEN: Int64  = 256
    static let UNKNOWN_2: Int64  = 512
    static let UNKNOWN_3: Int64  = 1024
    static let DOMEN_ADMIN: Int64  = 2048
    static let DOMEN_PCO: Int64  = 4096 //operator
    static let DOMEN_OPER: Int64  = 8192
    static let DOMEN_INGEN: Int64  = 16384
    static let DOMEN_HOZ_ORG: Int64  = 32768
    static let DOMEN_LAYER: Int64  = 65536
    static let DOMEN_OFICCER: Int64  = 110072 //no
    
    public static var manageSites: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & ORG_ADMIN != 0 || roles & DOMEN_ADMIN != 0 || roles & ORG_LAYER != 0 || roles & DOMEN_LAYER != 0 || roles & ORG_INGEN != 0 || roles & DOMEN_INGEN != 0
        }
    }
    
    public static var manageDevices: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & ORG_INGEN != 0 || roles & DOMEN_INGEN != 0
        }
    }
    
    public static var alarms: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & ORG_BKS_OPER != 0 || roles & DOMEN_PCO != 0 || roles & DOMEN_OPER != 0
        }
    }
    
    public static var manageSectionZones: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & ORG_INGEN != 0 || roles & DOMEN_INGEN != 0
        }
    }
    
    public static var sendCommands: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & DOMEN_HOZ_ORG != 0 || roles & DOMEN_INGEN != 0
        }
    }
    
    public static var manageOperators: Bool {
        get {
            let roles = DataManager.defaultHelper.roles
            return roles & ORG_ADMIN != 0 || roles & DOMEN_ADMIN != 0
        }
    }
}

public func userDefaults() -> UserDefaults {
    let u = UserDefaults(suiteName: DataManager.defaultHelper.login + "v.3")
    return u ?? UserDefaults.standard
}
