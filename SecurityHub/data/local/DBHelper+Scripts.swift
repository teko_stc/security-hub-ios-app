//
//  DBHelper+Scripts.swift
//  SecurityHub
//
//  Created by Timerlan on 19/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import SQLite

//Scripts
extension DBHelper{
    func add(script: Script) {
        var nScript: NScriptEntity?
        switch script.getDbStatus(main.db, table: main.scripts) {
        case .needInsert:
        script.insert(main.db, table: main.scripts)
        guard let si = getScriptInfo(device_id: script.device, zone_id: script.bind) else { return }
            nScript = NScriptEntity(deviceId: script.device, configVersion: si.configVersion, id: script.id, uid: script.uid, bind: script.bind, zoneName: si.zoneName, enabled: script.enabled, params: script.params.toJsonArray(), extra: script.extra.toJson(), name: script.name, type: .insert)
        case .needUpdate:
        script.update(main.db, table: main.scripts)
        guard let si = getScriptInfo(device_id: script.device, zone_id: script.bind) else { return }
        nScript = NScriptEntity(deviceId: script.device, configVersion: si.configVersion, id: script.id, uid: script.uid, bind: script.bind, zoneName: si.zoneName, enabled: script.enabled, params: script.params.toJsonArray(), extra: script.extra.toJson(), name: script.name, type: .update)
        case .noNeed: script.updateTime(main.db, table: main.scripts)
        }
        if let o = nScript { NotificationCenter.default.post(name: HubNotification.scriptUpdate, object: o) }
    }
    
    func clearOldScripts(device_id: Int64, time: Int64) {
        guard let rows = try? main.db.prepare(main.scripts.filter(Script.device == device_id && Script.time < time)) else { return }
        let ids = rows.map { (row) -> (Int64,Int64,Int64) in (row[Script.device],row[Script.bind],row[Script.id]) }
        guard let _ = try? main.db.run(main.scripts.filter(Script.device == device_id && Script.time < time).delete()) else { return }
        ids.forEach { (id) in
            NotificationCenter.default.post(name: HubNotification.scriptUpdate, object: NScriptEntity(deviceId: id.0, configVersion: 0, id: id.2, uid: "", bind: id.1, zoneName: "", enabled: false, params: [], extra: [:], name: "", type: .delete))
        }
    }
    
    func deleteScript(device_id: Int64, bind: Int64, id: Int64) {
        guard let _ = try? main.db.run(main.scripts.filter(Script.device == device_id && Script.id == id).delete()) else { return }
        NotificationCenter.default.post(name: HubNotification.scriptUpdate, object: NScriptEntity(deviceId: device_id, configVersion: 0, id: id, uid: "", bind: bind, zoneName: "", enabled: false, params: [], extra: [:], name: "", type: .delete))
    }
    
    func getScripts(device_ids: [Int64]? = nil) -> [DScriptInfo] {
        var result: [DScriptInfo] = []
        var query = "SELECT Devices.id, Devices.configVersion, Scripts.id, Scripts.uid, Scripts.bind, Scripts.enabled, Scripts.params, Scripts.extra, Zones.name, Scripts.name FROM Scripts, Devices, Zones WHERE Scripts.device = Devices.id AND Zones.zone = Scripts.bind AND Zones.device = Devices.id"
        if let device_ids = device_ids {
            var device_ids_query = "\(device_ids)"
            device_ids_query.removeFirst();  device_ids_query.removeLast()
            query += " AND Devices.id IN (\(device_ids_query))"
        }
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let script = DataEntityMapper.script(row) else { break; }
            result.append(script)
        }
        return result
    }
    
    func getScript(device_id: Int64, zone_id: Int64) -> DScriptInfo? {
        let query = "SELECT Devices.id, Devices.configVersion, Scripts.id, Scripts.uid, Scripts.bind, Scripts.enabled, Scripts.params, Scripts.extra, Zones.name, Scripts.name FROM Scripts, Devices, Zones WHERE Scripts.device = Devices.id AND Zones.zone = Scripts.bind AND Zones.device = Devices.id AND Devices.id = \(device_id) AND Zones.zone = \(zone_id)"
        guard let rows = try? main.db.prepare(query) else { return nil }
        for row in rows {
            guard let script = DataEntityMapper.script(row) else { break; }
            return script
        }
        return nil
    }
    
    func getScriptInfo(device_id: Int64, zone_id: Int64) -> (configVersion: Int64, zoneName: String)? {
        let query = "SELECT Devices.configVersion, Zones.name FROM Scripts, Devices, Zones WHERE Scripts.device = Devices.id AND Zones.zone = Scripts.bind AND Zones.device = Devices.id AND Devices.id = \(device_id) AND Zones.zone = \(zone_id)"
        guard let rows = try? main.db.prepare(query) else { return nil }
        for row in rows {
            guard let configVersion = row[0] as? Int64, let zoneName = row[1] as? String else { break; }
            return (configVersion: configVersion, zoneName: zoneName)
        }
        return nil
    }
}
