//
//  DBHelper_SITES.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//SITES
extension DBHelper {
    func add(site: Sites) {
        switch site.getDbStatus(main.db, table: main.sites) {
        case .needInsert:
            site.insert(main.db, table: main.sites)
            NotificationCenter.default.post(name: HubNotification.siteUpdate, object: NSiteEntity(id: site.id, site: site, type: .insert))
        case .needUpdate:
            site.update(main.db, table: main.sites)
            NotificationCenter.default.post(name: HubNotification.siteUpdate, object: NSiteEntity(id: site.id, site: site, type: .update))
        case .noNeed:
            site.updateTime(main.db, table: main.sites)
        }
    }
    
    func clearSites(time: Int64) {
        guard let rows = try? main.db.prepare(main.sites.select(Sites.id).filter(Sites.time < time)) else { return }
        let ids = rows.map { (row) -> Int64 in row[Sites.id]}
        guard let _ = try? main.db.run(main.sites.filter(Sites.time < time).delete()) else { return }
        ids.forEach { (id) in
            NotificationCenter.default.post(name: HubNotification.siteUpdate, object: NSiteEntity(id: id, site: nil, type: .delete))
        }
    }
    
    func getSites() -> [Sites] {
        var result: [Sites] = []
        guard let rows = try? main.db.prepare(main.sites.order(Sites.id.asc)) else { return result }
        for row in rows{ result.append(Sites.fromRow(row)) }
         return result
    }
    
    func getSite(site_id: Int64) -> Sites? {
        guard let row = try? main.db.pluck(main.sites.filter(Sites.id == site_id)) else  { return nil }
        return Sites.fromRow(row)
    }
    
    func getAffects(site_id: Int64) -> [DAffectEntity] {
        let query = "SELECT Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time From Devices, Sites, DeviceSection, Sections, Events LEFT OUTER JOIN Zones ON Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone Where Sites.id = \(site_id) AND Sites.id = DeviceSection.site AND Devices.id = DeviceSection.device AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Events.device = DeviceSection.device AND Events.section = DeviceSection.section AND Events.affect = 1 AND (Events._class < 5 OR (Events._class = 5 AND (Events.reason = 24 OR Events.reason = 27 OR Events.reason = 23)) OR (Events._class = 6 AND (Events.reason = 3 OR Events.reason = 4 OR Events.reason = 7)) OR (Events._class = 7 AND Events.reason = 1)) ORDER BY Events._class"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        let r = rows.map { (obj) -> DAffectEntity in return DataEntityMapper.dAffect(obj: obj) }
        return r
    }
    
    
    func getArmStatus(site_id: Int64) -> DSiteEntity.ArmStatus  {
        let query = "SELECT COUNT(*), SUM(case when Sections.arm > 0 then 1 else 0 end) From Sites, DeviceSection, Sections WHERE Sites.id = \(site_id) AND DeviceSection.site = Sites.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.detector = \(HubConst.SECTION_TYPE_SECURITY)"

        guard   let row = try? main.db.prepare(query).first(where: { _ -> Bool in  return true }),
                let total = row[0] as? Int64, let armed = row[1] as? Int64 else { return .disarmed }
        return  armed == 0 ? .disarmed : total == armed ? .armed : .notFullArmed
    }
    
    func getSiteInfo(site_id: Int64) -> DSiteInfo {
        var result = DSiteInfo()
        result.operatorCount = (try? main.db.scalar(main.operators.count)) ?? 0
        
        let query = "SELECT Devices.id, Devices.name, COUNT(*) FROM Devices, Sections, DeviceSection WHERE DeviceSection.site = \(site_id) AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section GROUP BY Devices.id, Devices.name"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            guard   let device_id = row[0] as? Int64,
                    let device_name = row[1] as? String,
                    let sectionCount = row[2] as? Int64 else { break }
            let qZoneCount = "SELECT COUNT(*) FROM Zones, DeviceSection WHERE DeviceSection.site = \(site_id) AND DeviceSection.device = \(device_id) AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.section <> 0"
            let lastEventTime   = try? main.db.scalar(main.events.filter(Events.device == device_id ).select(Events.time.max))
            let zoneCount       = (try? main.db.scalar(qZoneCount) as? Int64) ?? 0
            let isConnected     = isDeviceConnected(device_id: device_id)
            result.deviceInfos.append( DDeviceInfo(name: device_name, sectionCount: sectionCount - 1, zoneCount: zoneCount, isConnected: isConnected, lastEventTime: lastEventTime) )
        }
        return result
    }
    
    func removeSite(id: Int64) {
        guard let _ = try? main.db.run(main.sites.filter(Sites.id == id).delete()) else { return }
        NotificationCenter.default.post(name: HubNotification.siteUpdate, object: NSiteEntity(id: id, site: nil, type: .delete))
    }
}
