//
//  DBHelper+ZONES.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//ZONES
extension DBHelper{
    func add(zone: Zones){
        var nZone: NZoneEntity?
        switch zone.getDbStatus(main.db, table: main.zones) {
        case .needInsert:
            zone.insert(main.db, table: main.zones)
            let zi = getZoneSitesDeviceSectionInfo(device_id: zone.device, section_id: zone.section, zone_id: zone.zone)
            nZone = NZoneEntity(id: zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, sectionArm: zi.sectionArm, zoneId: zone.zone, zoneUid: zone.uidType, zoneDetector: zone.detector, zone: zone, type: .insert)
            break;
        case .needUpdate:
            zone.update(main.db, table: main.zones)
            let zi = getZoneSitesDeviceSectionInfo(device_id: zone.device, section_id: zone.section, zone_id: zone.zone)
            nZone = NZoneEntity(id: zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, sectionArm: zi.sectionArm, zoneId: zone.zone, zoneUid: zone.uidType, zoneDetector: zone.detector, zone: zone, type: .update)
            break;
        case .noNeed: 
            zone.updateTime(main.db, table: main.zones)
            break;
        }
        if let o = nZone { NotificationCenter.default.post(name: HubNotification.zonesUpdate, object: o) }
    }
		
    func getZones() -> [DZoneWithSitesDeviceSectionInfo] {
        var result: [DZoneWithSitesDeviceSectionInfo] = []
        let query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section ORDER BY Zones.id"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                result[result.count - 1].siteIds.append(zoneInfo.siteId)
                result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                result.append(DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm))
            }
        }
        return result
    }
    
    func getZones(priority: Int, searchText: String? = nil) -> [DZoneWithSitesDeviceSectionInfo] {
        var result: [DZoneWithSitesDeviceSectionInfo] = []
        var searchTextQuery = ""
        if let searchText = searchText {
            searchTextQuery = "AND ( (Zones.lname LIKE '%\(searchText.lowercased())%') OR (Sections.lname LIKE '%\(searchText.lowercased())%') OR (Sites.lname LIKE '%\(searchText.lowercased())%') OR (Devices.lname LIKE '%\(searchText.lowercased())%' AND Zones.section = 0 AND ( Zones.zone = 0 OR Zones.zone = 100 ) ) ) "
        }
        let query1 = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id, " + "Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time " +
            "FROM Sites, Devices, DeviceSection, Sections, Zones " +
            "LEFT OUTER JOIN Events ON Events.device = Zones.device AND Events.affect = 1 AND " +
                "((Events.section = Zones.section AND (Events.zone = Zones.zone OR (Events.zone = 0 AND Zones.zone = 100))) OR (Events.section = 0 AND (Events.zone = 0 OR Events.zone = 100) AND Events._class = \(HubConst.CLASS_MALFUNCTION) AND Events.reason = \(HubConst.REASON_MALF_LOST_CONNECTION))) " +
            "WHERE Sites.id = \(priority) AND DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section " +
            searchTextQuery +
            "ORDER BY Sites.id, Zones.device, Zones.section, Zones.lid"
        
        guard let rows = try? main.db.prepare(query1) else { return [] }
        for row in rows {
            guard   let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else {
                break
            }

            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                if !result[result.count - 1].siteIds.contains(zoneInfo.siteId) {
                    result[result.count - 1].siteIds.append(zoneInfo.siteId)
                    result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
                }
                if  let affect = DataEntityMapper.zoneInfoAffects(row) {
                    result[result.count - 1].affects?.append(affect)
                }
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                var ze = DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm)
                ze.affects = []
                if  let affect = DataEntityMapper.zoneInfoAffects(row) {
                    ze.affects?.append(affect)
                }
                result.append(ze)
            }
        }
        
        return result
    }
    
    func getZoness(device_id: Int64, section_id: Int64) -> [DZoneWithSitesDeviceSectionInfo] {
        var result: [DZoneWithSitesDeviceSectionInfo] = []
        let query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.device = \(device_id) AND Zones.section = \(section_id)"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                result[result.count - 1].siteIds.append(zoneInfo.siteId)
                result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                result.append(DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm))
            }
        }
        return result
    }
    
    func getZoness(device_id: Int64) -> [DZoneWithSitesDeviceSectionInfo] {
        var result: [DZoneWithSitesDeviceSectionInfo] = []
        let query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.device = \(device_id)"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                result[result.count - 1].siteIds.append(zoneInfo.siteId)
                result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                result.append(DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm))
            }
        }
        return result
    }
    
    func getDeviceZoness(device_id: Int64) -> [DZoneWithSitesDeviceSectionInfo] {
        var result: [DZoneWithSitesDeviceSectionInfo] = []
        let query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.device = \(device_id) AND Zones.section = 0 AND ( Zones.zone = 0 OR Zones.zone = 100 )"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                result[result.count - 1].siteIds.append(zoneInfo.siteId)
                result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                result.append(DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm))
            }
        }
        return result
    }
    
    func getZone(device_id: Int64, section_id: Int64, zone_id: Int64) -> DZoneWithSitesDeviceSectionInfo? {
        var result: DZoneWithSitesDeviceSectionInfo?
        let query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.device = \(device_id) AND Zones.section = \(section_id) AND Zones.zone = \(zone_id)"
        
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if result != nil {
                result!.siteIds.append(zoneInfo.siteId)
                result!.siteNames += ", \(zoneInfo.siteName)"
            } else {
                var deviceArm: Int64? = nil
                if zoneInfo.zone.section == 0 && (zoneInfo.zone.zone == 0 || zoneInfo.zone.zone == 100) {
                    let arm = getArmStatus(device_id: zoneInfo.zone.device)
                    deviceArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
                }
                result = DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: deviceArm ?? zoneInfo.sectionArm)
            }
        }
        return result
    }
    
    func getAffects(device_id: Int64, section_id: Int64, zone_id: Int64) -> [DAffectEntity] {
        //для контроллера аффекты могут быьт в зонах 0 и 100
        let query = "SELECT Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time From Devices, Sections, Events LEFT OUTER JOIN Zones ON Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone Where Events.device = Devices.id AND Events.device = Sections.device AND Events.section = Sections.section AND Events.affect = 1 AND Events.device = \(device_id) AND " + "((Events.section = \(section_id) AND (Events.zone = \(zone_id) OR (Events.zone = 0 AND \(zone_id) = 100))) OR (Events.section = 0 AND (Events.zone = 0 OR Events.zone = 100) AND Events._class = \(HubConst.CLASS_MALFUNCTION) AND Events.reason = \(HubConst.REASON_MALF_LOST_CONNECTION))) " +
            "ORDER BY Events._class"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        return rows.map { (obj) -> DAffectEntity in return DataEntityMapper.dAffect(obj: obj) }
    }
    
    func getLastAlarm(device_id: Int64, section_id: Int64, zone_id: Int64) -> DAffectEntity? {
        //для контроллера аффекты могут быьт в зонах 0 и 100
        let _zone_id = zone_id == 100 ? 0 : zone_id
        let query = "SELECT Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time From Devices, Sections, Events LEFT OUTER JOIN Zones ON Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone Where Events.device = Devices.id AND Events.device = Sections.device AND Events.section = Sections.section AND Events.device = \(device_id) AND Events.section = \(section_id) AND (Events.zone = \(_zone_id) OR Events.zone = \(_zone_id + 100)) AND Events._class = \(HubConst.CLASS_ALARM) AND Events.dropped = 0 AND Events.time > \(Int64(NSDate().timeIntervalSince1970 - 600)) ORDER BY Events._class DESC"
        
        guard let rows = try? main.db.prepare(query) else { return nil }
        return rows.map { (obj) -> DAffectEntity in return DataEntityMapper.dAffect(obj: obj) }.first
    }
    
    func isHaveAlarmTenMinAgo(deviceId: Int64, sectionId: Int64, zoneId: Int64) -> Bool {
        let query = "SELECT COUNT(*) From Events WHERE Events.device = \(deviceId) AND Events.section = \(sectionId) AND ( Events.zone = \(zoneId) OR Events.zone = \(zoneId + 100) ) AND Events._class = \(HubConst.CLASS_ALARM) AND Events.active = 1 AND Events.dropped = 0 AND Events.time > \(Int64(NSDate().timeIntervalSince1970 - 600))"
        
        guard let scalar = try? self.main.db.scalar(query), let count = scalar as? Int64 else { return false }
        return count > 0
    }
    
    func dropLastAlarm(device_id: Int64, section_id: Int64, zone_id: Int64) {
        let _zone_id = zone_id == 100 ? 0 : zone_id
        let query = "UPDATE Events SET dropped = 1 WHERE Events.device = \(device_id) AND Events.section = \(section_id) AND (Events.zone = \(_zone_id) OR Events.zone = \(_zone_id + 100)) AND Events._class = \(HubConst.CLASS_ALARM)"
        try? main.db.execute(query)
    }
    
    func getRelayStatus(device_id: Int64, section_id: Int64, zone_id: Int64) -> DZoneEntity.ArmStatus  {
        let query = "SELECT COUNT(*) From Events WHERE Events.device = \(device_id) AND Events.section = \(section_id) AND Events.zone = \(zone_id) AND Events._class = 5 AND Events.reason = 6 AND Events.active = 1 AND Events.affect = 1"

        guard   let row = try? main.db.prepare(query).first(where: { _ -> Bool in  return true }),
            let total = row[0] as? Int64 else { return .disarmed }
        return  total == 0 ? .disarmed : .armed
    }

    private func getZoneSitesDeviceSectionInfo(device_id: Int64, section_id: Int64, zone_id: Int64) -> DZoneSitesDeviceSectionInfo {
        var result = DZoneSitesDeviceSectionInfo(siteIds: [], siteNames: "", deviceName: "", configVersion: 0, cluster: 0, sectionName: "", sectionDetector: 0, sectionArm: 0)
        let query = "SELECT Sites.id, Sites.name, Devices.name, Devices.configVersion, Devices.cluster_id, Sections.name, Sections.detector, Sections.arm FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section AND Zones.device = \(device_id) AND Zones.section = \(section_id) AND Zones.zone = \(zone_id)"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            if  let siteId = row[0] as? Int64,
                let siteName = row[1] as? String,
                let deviceName = row[2] as? String,
                let configVersion = row[3] as? Int64,
                let cluster = row[4] as? Int64,
                let sectionName = row[5] as? String,
                let sectionDetector = row[6] as? Int64,
                let sectionArm = row[7] as? Int64 {
                result.deviceName = deviceName
                result.configVersion = configVersion
                result.cluster = cluster
                result.sectionName = sectionName
                result.sectionDetector = sectionDetector
                result.sectionArm = sectionArm
                result.siteIds.append(siteId)
                if result.siteNames.count == 0 { result.siteNames = siteName } else { result.siteNames += ", \(siteName)" }
            }
        }
        if section_id == 0 && (zone_id == 0 || zone_id == 100) {
            let arm = getArmStatus(device_id: device_id)
            result.sectionArm = arm == .disarmed ? 0 : arm == .armed ? 1 : 2
        }
        return result
    }
    
    func clearZones(device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil, time: Int64? = nil) {
        var query = "SELECT \(Zones.all), Sites.id, Sites.name, Devices.name, Sections.name, Sections.detector, Sections.arm, Devices.configVersion, Devices.cluster_id FROM Sites, Devices, DeviceSection, Sections, Zones WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND DeviceSection.device = Zones.device AND DeviceSection.section = Zones.section"
        if let device_id = device_id { query += " AND Zones.device = \(device_id)" }
        if let section_id = section_id { query += " AND Zones.section = \(section_id)" }
        if let zone_id = zone_id { query += " AND Zones.zone = \(zone_id)" }
        if let time = time { query += " AND Zones.time < \(time)" }

        var result: [DZoneWithSitesDeviceSectionInfo] = []
        guard let rows = try? main.db.prepare(query) else { return }
        for row in rows {
            guard let zoneInfo = DataEntityMapper.zoneWithSiteDeviceSectionInfo(row) else { break; }
            if let last = result.last, last.zone.id == zoneInfo.zone.id {
                result[result.count - 1].siteIds.append(zoneInfo.siteId)
                result[result.count - 1].siteNames += ", \(zoneInfo.siteName)"
            } else {
                result.append(DZoneWithSitesDeviceSectionInfo(zone: zoneInfo.zone, siteIds: [zoneInfo.siteId], siteNames: zoneInfo.siteName, deviceName: zoneInfo.deviceName, configVersion: zoneInfo.configVersion, cluster: zoneInfo.cluster, sectionName: zoneInfo.sectionName, sectionDetector: zoneInfo.sectionDetector, sectionArm: zoneInfo.sectionArm))
            }
        }
        var query_delete = main.zones
        if let device_id = device_id { query_delete = query_delete.filter(Zones.device == device_id) }
        if let section_id = section_id { query_delete = query_delete.filter(Zones.section == section_id) }
        if let zone_id = zone_id { query_delete = query_delete.filter(Zones.zone == zone_id) }
        if let time = time { query_delete = query_delete.filter(Zones.time < time) }
        guard let _ = try? main.db.run(query_delete.delete()) else { return }
        
        result.forEach { (row) in
            let nZ = NZoneEntity(id: row.zone.id, siteIds: row.siteIds, siteNames: row.siteNames, deviceId: row.zone.device, deviceName: row.deviceName, configVersion: row.configVersion, cluster: row.cluster, sectionId: row.zone.section, sectionName: row.sectionName, sectionDetector: row.sectionDetector, sectionArm: row.sectionArm, zoneId: row.zone.zone, zoneUid: row.zone.uidType, zoneDetector: row.zone.detector, zone: row.zone, type: .delete)
            NotificationCenter.default.post(name: HubNotification.zonesUpdate, object: nZ)
        }
    }
    
    func getZoneIds(device_id: Int64, section_id: Int64) -> [Int64] {
        var result: [Int64] = []
        let query = "SELECT zone FROM Zones WHERE device = \(device_id) AND section = \(section_id)"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows { if let id = row[0] as? Int64 { result.append(id) } }
        return result
    }
    
    //////////////////////////////////////////////////////////////////
    
    func get(device_id: Int64, section_id: Int64, zone_id: Int64) -> Zones? {
        if let q = ((try? main.db.pluck(main.zones.filter( Zones.device == device_id && Zones.section == section_id && Zones.zone == zone_id ))) as Row??),
            let row = q { return Zones.fromRow(row) }
        return nil
    }
    
    func getZones(device_id: Int64, section_id: Int64) -> [Zones] {
        var result: [Zones] = []
        guard let rows = try? main.db.prepare(main.zones.filter( Zones.device == device_id && Zones.section == section_id).order(Zones.zone)) else {
            return result
        }
        for obj in rows {
            result.append(Zones.fromRow(obj))
        }
        return result
    }
    
    func getZones(device_id: Int64) -> [Zones] {
        var result: [Zones] = []
        guard let rows = try? main.db.prepare(main.zones.filter( Zones.device == device_id).order(Zones.zone)) else {
            return result
        }
        for obj in rows {
            result.append(Zones.fromRow(obj))
        }
        return result
    }
}
