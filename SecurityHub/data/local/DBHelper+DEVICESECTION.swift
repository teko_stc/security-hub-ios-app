//
//  DBHelper+DEVICESECTION.swift
//  SecurityHub
//
//  Created by Timerlan on 06/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import SQLite

//DEVICESECTION
extension DBHelper {
    func add(deviceSection: DeviceSection, device: Devices, section: Sections, siteName: String){
        let statusDS = deviceSection.getDbStatus(main.db, table: main.deviceSection)
        let statusD = device.getDbStatus(main.db, table: main.device)
        let statusS = section.getDbStatus(main.db, table: main.sections)
        
        switch statusDS {
        case .needInsert: deviceSection.insert(main.db, table: main.deviceSection)
        case .needUpdate: deviceSection.update(main.db, table: main.deviceSection)
        case .noNeed: deviceSection.updateTime(main.db, table: main.deviceSection)
        }
        switch statusS {
        case .needInsert: section.insert(main.db, table: main.sections)
        case .needUpdate: section.update(main.db, table: main.sections)
        case .noNeed: section.updateTime(main.db, table: main.sections)
        }
        
        var nS: Any?
        if statusDS == .needInsert && statusS == .needInsert {
            nS = NSectionEntity(id: section.id, siteIds: [deviceSection.site], siteNames: siteName, deviceId: device.id, deviceName: device.name, configVersion: device.configVersion, cluster_id: device.cluster_id, sectionId: section.section, section: section, type: .insert)
        } else if statusDS == .needInsert || statusS == .needUpdate {
            let sI = getSectionSitesDevicesInfo(device_id: device.id, section_id: section.section)
             nS = NSectionEntity(id: section.id, siteIds: sI.siteIds, siteNames: sI.siteNames, deviceId: device.id, deviceName: device.name, configVersion: device.configVersion, cluster_id: device.cluster_id, sectionId: section.section, section: section, type: .update)
        }
        if let o = nS { NotificationCenter.default.post(name: HubNotification.sectionsUpdate, object: o) }

        if deviceSection.section != 0 { return }
        
        switch statusD {
        case .needInsert: device.insert(main.db, table: main.device)
        case .needUpdate: device.update(main.db, table: main.device)
        case .noNeed: device.updateTime(main.db, table: main.device)
        }
        
        var nD: Any?
        if statusDS == .needInsert && statusD == .needInsert {
            nD = NDeviceEntity(id: device.id, siteIds: [deviceSection.site], siteNames: siteName, device: device, type: .insert)
        } else if statusDS == .needInsert || statusD == .needUpdate {
            let sitesInfo = getDeviceSitesInfo(device_id: device.id)
            nD = NDeviceEntity(id: device.id, siteIds: sitesInfo.siteIds, siteNames: sitesInfo.siteNames, device: device, type: .update)
        }
        if let o = nD { NotificationCenter.default.post(name: HubNotification.deviceUpdate, object: o) }
    }
    
    func clearDeviceSection(site_id: Int64? = nil, time: Int64) {
        guard let rows = try? main.db.prepare(main.deviceSection.filter(site_id == nil || DeviceSection.site == site_id ?? -777).filter(DeviceSection.time < time)) else { return }
        let relations = rows.map { (row) -> (s:Int64,d:Int64,sec:Int64) in return (s: row[DeviceSection.site], d: row[DeviceSection.device], sec: row[DeviceSection.section]) }
        guard let _ = try? main.db.run(main.deviceSection.filter(site_id == nil || DeviceSection.site == site_id ?? -777).filter(DeviceSection.time < time).delete()) else { return }
        relations.forEach { (rel) in
            //TODO Не корректное удаление девайса и раздела, если будет две связи
            let nS = NSectionEntity(id: Sections.getId(rel.d, rel.sec), siteIds: [rel.s], siteNames: "", deviceId: rel.d, deviceName: "", configVersion: 0, cluster_id: 0, sectionId: rel.sec, section: Sections(device: rel.d, section: rel.sec, name: "", detector: 0, arm: 0, time: 0, autoarmId: -1), type: .delete)
            NotificationCenter.default.post(name: HubNotification.sectionsUpdate, object: nS)
            if getSectionSitesDevicesInfo(device_id: rel.d, section_id: rel.sec).siteIds.count == 0 {
                let _ = try? main.db.run(main.sections.filter(Sections.device == rel.d && Sections.section == rel.sec).delete())
            }
            if rel.sec == 0 {
                let nD = NDeviceEntity(id: rel.d, siteIds: [rel.s], siteNames: "", device: nil, type: .delete)
                NotificationCenter.default.post(name: HubNotification.deviceUpdate, object: nD)
                if getDeviceSitesInfo(device_id: rel.d).siteIds.count == 0 {
                    let _ = try? main.db.run(main.device.filter(Devices.id == rel.d).delete())
                }
            }
        }
    }
    
    func getSiteIds(device_id: Int64, section_id: Int64) -> [Int64] {
        var result: [Int64] = []
        let query = "SELECT site FROM DeviceSection WHERE device = \(device_id) AND section = \(section_id) GROUP BY site"
        guard let rows = try? main.db.prepare(query) else { return result }
        for obj in rows { if let id = obj[0] as? Int64{ result.append(id) }  }
        return result
    }
	
		func getDeviceSection(site: Int64,device: Int64,section: Int64) -> DeviceSection? {
			guard let row = try? main.db.pluck(main.deviceSection.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section)) else  { return nil }
				return DeviceSection.fromRow(row)
		}
}
