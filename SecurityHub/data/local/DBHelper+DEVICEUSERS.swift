//
//  DBHelper+DEVICEUSERS.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//DEVICEUSERS
extension DBHelper{
    func getHozOrganName(device_id: Int64, user_index: Int64) -> String? {
        if let name = try? main.db.scalar("SELECT name From DeviceUsers Where device = \(device_id) AND userIndex = \(user_index)") as? String { return name }
        return nil
    }
    
    ////////////////
    func add(deviceUsers: DeviceUsers){
        NotificationCenter.default.post(name: HubNotification.deviceUsersUpdate(deviceUsers.device), object: (deviceUsers: [deviceUsers], type: self.add(deviceUsers, table: main.deviceUser) ))
    }
    func add(userSection: UserSection){ _ = self.add(userSection, table: main.userSection) }
    
    func deleteDeviceUsers(site_id: Int64? = nil, time: Int64? = nil) {
        guard let rowsS = try? main.db.prepare(main.sites.filter(site_id == nil || Sites.id == site_id ?? 0)) else {
            return
        }
        for rowS in rowsS {
            let site: Sites = Sites.fromRow(rowS)
            getDeviceIds(site_id: site.id).forEach { (device_id) in
                getDeviceUsers(device_ids: [device_id]).filter({ time == nil || $0.time != time ?? 0}).forEach{ (du) in
                    deleteDeviceUsers(device_id: du.device, userIndex: du.userIndex)
                }
                getDeviceUsers(device_ids: [device_id])
                    .forEach{ (du) in
                        do{
                            try main.db.run(main.userSection.filter(UserSection.userId == du.id).filter(time == nil || UserSection.time < time ?? 0).delete())
                        }catch {}
                }
            }
        }
    }
    
    func deleteDeviceUsers(device_id: Int64, userIndex: Int64) {
        guard let du = get(device_id: device_id, userIndex: userIndex) else{ return }
        getUserSectionIds(deviceUser: du).forEach({ (id) in
            do{
                try main.db.run(main.userSection.filter( UserSection.userId == du.id && UserSection.sectionId == id ).delete())
            } catch {}
        })
        NotificationCenter.default.post(name: HubNotification.deviceUsersUpdate(du.device), object: (deviceUsers: [du], type: NUpdateType.delete ))
        du.delete(main.db, table: main.deviceUser)
    }
    
    func deleteUserSection(userId: Int64) {
        do{
            try main.db.run(main.userSection.filter(UserSection.userId == userId).delete())
        } catch {
            print("Error UserSection")
        }
    }
    
    func updateDeviceUsers(deviceUsers: DeviceUsers) {
        NotificationCenter.default.post(
            name: HubNotification.deviceUsersUpdate(deviceUsers.device),
            object: (deviceUsers: [deviceUsers], type: NUpdateType.update)
        )
        
        deviceUsers.update(main.db, table: main.deviceUser)
    }
    
    func get(device_id: Int64, userIndex: Int64) -> DeviceUsers? {
        if let q = ((try? main.db.pluck(main.deviceUser.filter( DeviceUsers.device == device_id && DeviceUsers.userIndex == userIndex))) as Row??),
            let row = q { return DeviceUsers.fromRow(row) }
        return nil
    }
    
    func getDeviceUsers(device_ids: [Int64]) -> [DeviceUsers] {
        var result: [DeviceUsers] = []
        device_ids.forEach { (device_id) in
            if let rows = try? main.db.prepare(
                main.deviceUser
                    .filter(device_id == DeviceUsers.device)
                    .order(DeviceUsers.device)
                ) {
                for row in rows { result.append(DeviceUsers.fromRow(row)) }
            }
        }
        return result
    }
    
    func getUserSectionIds(deviceUser: DeviceUsers) -> [Int64] {
        var result: [Int64] = []
        let query = "SELECT UserSection.sectionId FROM UserSection INNER JOIN DeviceUsers ON UserSection.userId = DeviceUsers.id WHERE UserSection.userId = \(deviceUser.id)"
        guard let rows = try? self.main.db.prepare(query) else { return result }
        for obj in rows {
            if let row = try? self.main.db.pluck(self.main.sections.filter(Sections.id == obj[0] as! Int64)) { result.append(row[Sections.section]) }
        }
        return result
    }
    
    func setUserSections(deviceUser: DeviceUsers, userSections: [UserSection]) {
        try! main.db.run(
            main.userSection.filter(UserSection.userId == deviceUser.id).delete()
        )

        for section in userSections {
            add(userSection: section)
        }
    }
}
