//
//  D3ConstRepository.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

struct D3IconModel {
    let list, main, big: String
}

struct D3TypeModel {
    let id: Int
    let name: String
    let icons: D3IconModel
}

struct D3SensorInfoModel {
    let model: D3TypeModel
    let typeDescription: String?
    let detectorDescription: String?
    let alartDescription: String?
}

struct D3EventModel {
    let name: String
    let description: String
    let color: String?
    var icons: D3IconModel
}

struct D3ClassModel {
    let id: Int
    let caption, description, iconSmall, iconBig, affect: String
}

protocol D3TypesRepositoryProtocol {
    func getSensorType(uid: Int) -> Int
    
    func getDeviceType(type: Int) -> D3TypeModel?
    
    func getDeviceIcons(type: Int) -> D3IconModel
    
    func getOutputType(detector: Int) -> D3TypeModel?
    
    func getInputType(type: Int, detector: Int) -> D3TypeModel?
    
    func getSensorType(type: Int, detector: Int) -> D3TypeModel?
    
    func getSensorIcons(uid: Int, detector: Int) -> D3IconModel
    
    func getSectionType(detector: Int) -> D3TypeModel?
    
    func getSectionIcons(detector: Int) -> D3IconModel

    func getEventType(_class: Int, detector: Int, reason: Int, statment: Int) -> D3EventModel?
    
    func getEventIcons(_class: Int, detector: Int, reason: Int, statment: Int) -> D3IconModel
    
    func getSencorInfo(uid: Int, detector: Int) -> D3SensorInfoModel?
    
    func getClassTypes() -> [D3ClassModel]
    
    func getSectionTypes() -> [D3TypeModel]
}

class D3ConstRepository: D3TypesRepositoryProtocol {
    let xmlConstRepository: XMLConstRepository
        
    init(languageIso: String, parserDidEndClasses: @escaping ([XMLConstRepository.ClassModel]) -> ()) {
        xmlConstRepository = XMLConstRepository(languageIso: languageIso, parserDidEndClasses: parserDidEndClasses)
    }
    
    private func cast(xmlDeviceType dt: XMLConstRepository.DeviceTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: dt.id,
            name: dt.caption,
            icons: D3IconModel(
                list: dt.icons.get(index: 0) ?? "ic_nil",
                main: dt.icons.get(index: 1) ?? dt.icons.get(index: 0) ?? "ic_nil",
                big: dt.icons.get(index: 2) ?? dt.icons.get(index: 1) ?? dt.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlOutputType ot: XMLConstRepository.OutputTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: ot.id,
            name: ot.caption,
            icons: D3IconModel(
                list: ot.icons.get(index: 0) ?? "ic_nil",
                main: ot.icons.get(index: 1) ?? ot.icons.get(index: 0) ?? "ic_nil",
                big: ot.icons.get(index: 2) ?? ot.icons.get(index: 1) ?? ot.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlInputType it: XMLConstRepository.InputTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: it.id,
            name: it.caption,
            icons: D3IconModel(
                list: it.icons.get(index: 0) ?? "ic_nil",
                main: it.icons.get(index: 1) ?? it.icons.get(index: 0) ?? "ic_nil",
                big: it.icons.get(index: 2) ?? it.icons.get(index: 1) ?? it.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlAlarmType at: XMLConstRepository.AlarmTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: at.id,
            name: at.caption,
            icons: D3IconModel(
                list: at.icons.get(index: 0) ?? "ic_nil",
                main: at.icons.get(index: 1) ?? at.icons.get(index: 0) ?? "ic_nil",
                big: at.icons.get(index: 2) ?? at.icons.get(index: 1) ?? at.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlZoneType st: XMLConstRepository.ZoneTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: st.id,
            name: st.caption,
            icons: D3IconModel(
                list: st.icons.get(index: 0) ?? "ic_nil",
                main: st.icons.get(index: 1) ?? st.icons.get(index: 0) ?? "ic_nil",
                big: st.icons.get(index: 2) ?? st.icons.get(index: 1) ?? st.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlSectionType st: XMLConstRepository.SectionTypeModel) -> D3TypeModel {
        D3TypeModel(
            id: st.id,
            name: st.caption,
            icons: D3IconModel(
                list: st.icons.get(index: 0) ?? "ic_nil",
                main: st.icons.get(index: 1) ?? st.icons.get(index: 0) ?? "ic_nil",
                big: st.icons.get(index: 2) ?? st.icons.get(index: 1) ?? st.icons.get(index: 0) ?? "ic_nil"
            )
        )
    }
    
    private func cast(xmlStatmentModel sm: XMLConstRepository.StatementModel) -> D3EventModel {
        D3EventModel(
            name: sm.caption,
            description: sm.description,
            color: sm.color,
            icons: D3IconModel(
                list: sm.icons.get(index: 0) ?? sm.iconS,
                main: sm.icons.get(index: 1) ?? sm.icons.get(index: 0) ?? sm.iconS,
                big: sm.icons.get(index: 2) ?? sm.icons.get(index: 1) ?? sm.icons.get(index: 0) ?? sm.iconS
            )
        )
    }
    
    private func cast(xmlReasontModel rm: XMLConstRepository.ReasonModel) -> D3EventModel {
        D3EventModel(
            name: rm.caption,
            description: rm.affect,
            color: nil,
            icons: D3IconModel(
                list: rm.iconSmall,
                main: rm.iconBig,
                big: rm.iconBig
            )
        )
    }
    
    private func cast(xmlDetectorModel dm: XMLConstRepository.DetectorModel, xmlClassModel cm: XMLConstRepository.ClassModel) -> D3EventModel {
        D3EventModel(
            name: dm.caption,
            description: dm.caption,
            color: nil,
            icons: D3IconModel(
                list: cm.iconSmall,
                main: cm.iconBig,
                big: cm.iconBig
            )
        )
    }
    
    private func cast(icons: [String]) -> D3IconModel {
        D3IconModel(
            list: icons.get(index: 0) ?? "ic_nil",
            main: icons.get(index: 1) ?? icons.get(index: 0) ?? "ic_nil",
            big: icons.get(index: 2) ?? icons.get(index: 1) ?? icons.get(index: 0) ?? "ic_nil"
        )
    }
    
    private func cast(xmlClassModel cm: XMLConstRepository.ClassModel) -> D3EventModel {
        D3EventModel(
            name: cm.caption,
            description: cm.affect,
            color: nil,
            icons: D3IconModel(
                list: cm.iconSmall,
                main: cm.iconBig,
                big: cm.iconBig
            )
        )
    }
    
    public func getSensorType(uid: Int) -> Int {
        switch uid >> 24 & 0xff {
        case 1:     return uid & 0xff
        case 2:     return uid & 0xffff
        case 4:     return HubConst.SENSORTYPE_WIRED_INPUT.int
        case 5:     return HubConst.SENSORTYPE_WIRED_OUTPUT.int
        default:    return HubConst.SENSORTYPE_UNKNOWN.int
        }
    }
    
    func getDeviceType(type: Int) -> D3TypeModel? {
        xmlConstRepository.deviceTypes.first(where: { dt in dt.id == type}).map({ dt in cast(xmlDeviceType: dt) })
    }
    
    func getOutputType(detector: Int) -> D3TypeModel? {
        xmlConstRepository.outputTypes.first(where: { ot in ot.id == detector}).map({ ot in cast(xmlOutputType: ot) })
    }
    
    func getInputType(type: Int, detector: Int) -> D3TypeModel? {
        guard let it = xmlConstRepository.inputTypes.first(where: { it in it.id == type}) else { return nil }
        guard let dt = it.detectorTypes.first(where: { dt in dt.id == (detector & 0xff) }), let at = dt.alarmTypes.first(where: { at in at.id == ( detector >> 8 & 0xff ) }) else { return cast(xmlInputType: it) }
        return cast(xmlAlarmType: at)
    }
    
    func getSensorType(type: Int, detector: Int) -> D3TypeModel? {
        guard let st = xmlConstRepository.zoneTypes.first(where: { st in st.id == type}) else { return nil }
        guard let dt = st.detectorTypes.first(where: { dt in dt.id == (detector & 0xff) }), let at = dt.alarmTypes.first(where: { at in at.id == ( detector >> 8 & 0xff ) }) else { return cast(xmlZoneType: st) }
        return cast(xmlAlarmType: at)
    }
  
    func getSencorInfo(uid: Int, detector: Int) -> D3SensorInfoModel? {
        let type = getSensorType(uid: uid)
        switch (type){
        case HubConst.SENSORTYPE_WIRED_OUTPUT.int:
            if let outputType = xmlConstRepository.outputTypes.first(where: { ot in ot.id == detector}) {
                return D3SensorInfoModel(model: cast(xmlOutputType: outputType), typeDescription: nil, detectorDescription: nil, alartDescription: nil)
            } else if let detType = xmlConstRepository.detectorTypes.first(where: { $0.id == detector & 0xff }) {
                let icons = D3IconModel(list: "ic_nil", main: "ic_nil", big: "ic_nil")
                return D3SensorInfoModel(model: D3TypeModel(id: detType.id, name: detType.caption, icons: icons), typeDescription: nil, detectorDescription: nil, alartDescription: nil)
            }
            return nil
        case HubConst.SENSORTYPE_WIRED_INPUT.int:
            let t = uid & 0xff
            if let inputType = xmlConstRepository.inputTypes.first(where: { ot in ot.id == t}) {
                let detType = inputType.detectorTypes.first(where: { dt in dt.id == (detector & 0xff) }), alartType = detType?.alarmTypes.first(where: { at in at.id == ( detector >> 8 & 0xff ) })
                return D3SensorInfoModel(model: D3TypeModel(id: inputType.id, name: inputType.caption, icons: cast(icons: inputType.icons)), typeDescription: nil, detectorDescription: detType?.caption, alartDescription: alartType?.caption)
            } else if let detType = xmlConstRepository.detectorTypes.first(where: { $0.id == detector & 0xff }) {
                let icons = D3IconModel(list: "ic_nil", main: "ic_nil", big: "ic_nil")
                return D3SensorInfoModel(model: D3TypeModel(id: detType.id, name: detType.caption, icons: icons), typeDescription: nil, detectorDescription: nil, alartDescription: nil)
            }
            return nil
        case HubConst.SENSORTYPE_UNKNOWN.int:
            if let detType = xmlConstRepository.detectorTypes.first(where: { $0.id == detector & 0xff }) {
               let icons = D3IconModel(list: "ic_nil", main: "ic_nil", big: "ic_nil")
                return D3SensorInfoModel(model: D3TypeModel(id: detType.id, name: detType.caption, icons: icons), typeDescription: nil, detectorDescription: nil, alartDescription: nil)
            }
            return nil
        default:
            if let zoneType = xmlConstRepository.zoneTypes.first(where: { st in st.id == type}) {
                let detType = zoneType.detectorTypes.first(where: { dt in dt.id == (detector & 0xff) }), alartType = detType?.alarmTypes.first(where: { at in at.id == ( detector >> 8 & 0xff ) })
                return D3SensorInfoModel(model: D3TypeModel(id: zoneType.id, name: zoneType.caption_e, icons: cast(icons: zoneType.icons)), typeDescription: zoneType.caption, detectorDescription: detType?.caption, alartDescription: alartType?.caption)
            } else if let detType = xmlConstRepository.detectorTypes.first(where: { $0.id == detector & 0xff }) {
                let icons = D3IconModel(list: "ic_nil", main: "ic_nil", big: "ic_nil")
                return D3SensorInfoModel(model: D3TypeModel(id: detType.id,name: detType.caption, icons: icons), typeDescription: nil, detectorDescription: nil, alartDescription: nil)
            }
            return nil
        }
    }
    
    func getDeviceIcons(type: Int) -> D3IconModel {
        getDeviceType(type: type)?.icons ?? D3IconModel(list: "n_image_controller_common_list_selector", main: "n_image_controller_common_main_selector", big: "n_image_controller_common_big_selector")
    }
    
    func getSensorIcons(uid: Int, detector: Int) -> D3IconModel {
        let type = getSensorType(uid: uid)
        switch (type){
        case HubConst.SENSORTYPE_WIRED_OUTPUT.int:
            if let result = getOutputType(detector: detector)?.icons { return result }
            switch (detector & 0xff){
            case HubConst.DETECTOR_AUTO_RELAY.int, HubConst.DETECTOR_MANUAL_RELAY.int:
                return D3IconModel(list: "n_image_relay_list_selector", main: "n_image_relay_main_selector", big: "n_image_relay_big_selector")
            case HubConst.DETECTOR_KEYPAD.int:
                return D3IconModel(list: "n_image_keypad_list_selector", main: "n_image_keypad_main_selector", big: "n_image_keypad_big_selector")
            default:
                return D3IconModel(list: "n_image_wired_output_common_list_selector", main: "n_image_wired_output_common_main_selector", big: "n_image_wired_output_common_big_selector")
            }
        case HubConst.SENSORTYPE_WIRED_INPUT.int:
            return getInputType(type: uid & 0xff, detector: detector)?.icons ?? D3IconModel(list: "n_image_wired_input_common_list_selector", main: "n_image_wired_input_common_main_selector", big: "n_image_wired_input_common_big_selector")
        case HubConst.SENSORTYPE_UNKNOWN.int:
            return D3IconModel(list: "n_image_device_common_list_selector", main: "n_image_device_common_main_selector", big: "n_image_device_common_big_selector")
        default:
            return getSensorType(type: type, detector: detector)?.icons ?? D3IconModel(list: "n_image_device_common_list_selector", main: "n_image_device_common_main_selector", big: "n_image_device_common_big_selector")
        }
    }
    
    func getSectionType(detector: Int) -> D3TypeModel? {
        xmlConstRepository.sectionTypes.first(where: { $0.id == detector }).map({ cast(xmlSectionType: $0) })
    }
    
    func getSectionTypes() -> [D3TypeModel] {
        xmlConstRepository.sectionTypes.map({ cast(xmlSectionType: $0) })
    }
    
    func getSectionIcons(detector: Int) -> D3IconModel {
        getSectionType(detector: detector)?.icons ?? D3IconModel(list: "ic_section_common_list", main: "ic_section_common_main", big: "ic_section_common_big")
    }
    
    func getEventType(_class: Int, detector: Int, reason: Int, statment: Int) -> D3EventModel? {
        guard let _classType = xmlConstRepository.classes.first(where: { $0.id == _class }) else { return nil }
        if let statmentType = _classType.detectors.first(where: { $0.id == detector })?.reasons.first(where: { $0.id == reason })?.statements.first(where: { $0.id == statment }) {
            var _st = cast(xmlStatmentModel: statmentType)
            if statmentType.icons.count > 0 { return _st }
            if let statmentType2 = _classType.reasons.first(where: { $0.id == reason })?.statements.first(where: { $0.id == statment }) {
                let _st2 = cast(xmlStatmentModel: statmentType2)
                if statmentType2.icons.count > 0 { _st.icons = _st2.icons; return _st }
                if let statmentType3 = _classType.statements.first(where: { $0.id == statment }) {
                    let _st3 = cast(xmlStatmentModel: statmentType3)
                    if statmentType3.icons.count > 0 { _st.icons = _st3.icons; return _st }
                }
            }
            return _st
        }
        if let statmentType = _classType.reasons.first(where: { $0.id == reason })?.statements.first(where: { $0.id == statment }) {
            var _st = cast(xmlStatmentModel: statmentType)
            if statmentType.icons.count > 0 { return _st }
            if let statmentType2 = _classType.statements.first(where: { $0.id == statment }) {
                let _st2 = cast(xmlStatmentModel: statmentType2)
                if statmentType2.icons.count > 0 { _st.icons = _st2.icons; return _st }
            }
            return _st
        }
        if let statmentType = _classType.statements.first(where: { $0.id == statment }) {
            return cast(xmlStatmentModel: statmentType)
        }
        return cast(xmlClassModel: _classType)
    }
    
    func getEventIcons(_class: Int, detector: Int, reason: Int, statment: Int) -> D3IconModel {
        getEventType(_class: _class, detector: detector, reason: reason, statment: statment)?.icons ?? D3IconModel(list: "ic_nil", main: "ic_nil", big: "ic_nil")
    }
    
    func getClassTypes() -> [D3ClassModel] {
        xmlConstRepository.classes.map({ D3ClassModel(id: $0.id, caption: $0.caption, description: $0.description, iconSmall: $0.iconSmall, iconBig: $0.iconBig, affect: $0.affect) })
    }
}
