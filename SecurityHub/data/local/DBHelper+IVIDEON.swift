//
//  DBHelper+IVIDEON.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//IVIDEON
extension DBHelper{
    func add(ivVideo: IvVideo){
        let _ = add(ivVideo, table: self.main.ivVideo)
        NotificationCenter.default.post(name: HubNotification.ivVideo(), object: ivVideo)
        NotificationCenter.default.post(name: HubNotification.ivVideo(ivVideo.eventId), object: ivVideo)
        NotificationCenter.default.post(name: HubNotification.ivVideo(camId: ivVideo.camId), object: ivVideo)
    }
    
    func get(ivVideo_id: Int64) -> IvVideo? {
        if let row = try? main.db.pluck(main.ivVideo.filter(IvVideo.eventId == ivVideo_id)){
            return IvVideo.fromRow(row) }
        return nil
    }
    
    func getEvents(cam_id: String) -> [(event: Events, iv: IvVideo)] {
        var result: [(event: Events, iv: IvVideo)] = []
        guard let rows = try? main.db.prepare(main.ivVideo.filter(IvVideo.camId == cam_id)) else { return result }
        for row in rows {
            let video: IvVideo = IvVideo.fromRow(row)
            if let r = try? main.db.pluck(main.events.filter(Events.id == video.eventId)) {
                let event: Events = Events.fromRow(r)
                result.append((event: event, iv: video))
            }
        }
        return result
    }
    
    func getEvents(cameraId: String) -> [DEventCameraEntity] {
        let query = "SELECT Events.id, Events.icon, Events.iconBig, Events.affect_desc, Events.time, IvVideo.result, IvVideo.isError, Sites.name, Devices.name, Sections.name, Zones.name " +
            "FROM IvVideo, Events, Sites, Devices, DeviceSection, Sections, Zones " +
            "WHERE IvVideo.camId = '\(cameraId)' AND Events.id = IvVideo.eventId AND DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.device = Zones.device AND Sections.section = Zones.section AND Zones.device = Events.device AND Zones.section = Events.section AND Zones.zone = Events.zone ORDER BY Events.id"
        var result: [DEventCameraEntity] = []
        var ids: [Int64] = []
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows { if let ec = DataEntityMapper.eventCamera(row), !ids.contains(ec.eventId) { result.append(ec); ids.append(ec.eventId) } }
        return result
    }
    
    func getCameraEvent(eventId: Int64) -> DEventCameraEntity? {
        let query = "SELECT Events.id, Events.icon, Events.iconBig, Events.affect_desc, Events.time, IvVideo.result, IvVideo.isError, Sites.name, Devices.name, Sections.name, Zones.name " +
            "FROM IvVideo, Events, Sites, Devices, DeviceSection, Sections, Zones " +
            "WHERE Events.id = \(eventId) AND Events.id = IvVideo.eventId AND DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.device = Zones.device AND Sections.section = Zones.section AND Zones.device = Events.device AND Zones.section = Events.section AND Zones.zone = Events.zone"
        guard let rows = try? main.db.prepare(query) else { return nil }
        for row in rows { if let ec = DataEntityMapper.eventCamera(row) { return ec } }
        return nil
    }
    
    func getCamSet(cam_id: String) -> CamSet{
        if let row = try? self.main.db.pluck(self.main.camSet.filter(CamSet.camid == cam_id)) {
            return CamSet.fromRow(row)
        }else{
            let newSet = CamSet(camid: cam_id)
            newSet.insert(main.db, table: main.camSet)
            return newSet
        }
    }
    
    func updCamSet(camSet: CamSet){ camSet.update(main.db, table: main.camSet) }
}
