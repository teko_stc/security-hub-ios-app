//
//  DBHelper+DEVICES.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//DEVICES
extension DBHelper{
    func getDevices() -> [DDeviceWithSitesInfo] {
        var result: [DDeviceWithSitesInfo] = []
        let query = "SELECT \(Devices.all), Sites.id, Sites.name FROM Devices, Sites, DeviceSection WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.section = 0 ORDER BY Devices.id"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            guard let deviceInfo = DataEntityMapper.deviceWithSiteInfo(row) else { break; }
            if let last = result.last, last.device.id == deviceInfo.device.id {
                result[result.count - 1].siteIds.append(deviceInfo.siteId)
                result[result.count - 1].siteNames += ", \(deviceInfo.siteName)"
            } else {
                result.append(DDeviceWithSitesInfo(device: deviceInfo.device, siteIds: [deviceInfo.siteId], siteNames: deviceInfo.siteName))
            }
        }
        return result
    }
    
    func getDevice(device_id: Int64) -> DDeviceWithSitesInfo? {
        var result: DDeviceWithSitesInfo?
        let query = "SELECT \(Devices.all), Sites.id, Sites.name FROM Devices, Sites, DeviceSection WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.section = 0 AND Devices.id = \(device_id)"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            guard let deviceInfo = DataEntityMapper.deviceWithSiteInfo(row) else { break; }
            if result != nil  {
                result!.siteIds.append(deviceInfo.siteId)
                result!.siteNames += ", \(deviceInfo.siteName)"
            } else {
                result = DDeviceWithSitesInfo(device: deviceInfo.device, siteIds: [deviceInfo.siteId], siteNames: deviceInfo.siteName)
            }
        }
        return result
    }
    
    func getAffects(site_id: Int64? = nil, device_id: Int64) -> [DAffectEntity] {
        let site_query = site_id != nil ? "Sites.id = \(site_id!) AND" : ""
        let query = "SELECT Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class, Events.id, Events.detector, Events.reason, Events.active, Events.jdata, Events.time From Devices, Sites, DeviceSection, Sections, Events LEFT OUTER JOIN Zones ON Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone Where \(site_query) DeviceSection.device = \(device_id) AND Sites.id = DeviceSection.site AND Devices.id = DeviceSection.device AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Events.device = DeviceSection.device AND Events.section = DeviceSection.section AND Events.affect = 1 AND (Events._class < 5 OR (Events._class = 5 AND (Events.reason = 24 OR Events.reason = 27 OR Events.reason = 23)) OR (Events._class = 6 AND (Events.reason = 3 OR Events.reason = 4 OR Events.reason = 7)) OR (Events._class = 7 AND Events.reason = 1)) GROUP BY Devices.name, Sections.name, Zones.name, Events.affect_desc, Events.icon, Events._class ORDER BY Events._class"
        
        guard let rows = try? main.db.prepare(query) else { return [] }
        return rows.map { (obj) -> DAffectEntity in return DataEntityMapper.dAffect(obj: obj) }
    }
    
    func getArmStatus(site_id: Int64? = nil, device_id: Int64) -> DDeviceEntity.ArmStatus  {
        let site_query = site_id != nil ? "Sites.id = \(site_id!) AND" : ""
        let query = "SELECT COUNT(*), SUM(case when Sections.arm > 0 then 1 else 0 end) From Sites, DeviceSection, Sections WHERE \(site_query) DeviceSection.device = \(device_id) AND DeviceSection.site = Sites.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.detector = \(HubConst.SECTION_TYPE_SECURITY)"
        
        guard   let row = try? main.db.prepare(query).first(where: { _ -> Bool in  return true }),
            let total = row[0] as? Int64, let armed = row[1] as? Int64 else { return .disarmed }
        return  armed == 0 ? .disarmed : total == armed ? .armed : .notFullArmed
    }
    
    func getDeviceSitesInfo(device_id: Int64) -> DDeviceSitesInfo {
        var result = DDeviceSitesInfo(siteIds: [], siteNames: "")
        let query = "SELECT Sites.id, Sites.name FROM Sites, DeviceSection WHERE DeviceSection.site = Sites.id AND DeviceSection.device = \(device_id) AND DeviceSection.section = 0"
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows {
            if  let siteId = row[0] as? Int64,
                let siteName = row[1] as? String {
                result.siteIds.append(siteId)
                if result.siteNames.count == 0 { result.siteNames = siteName } else { result.siteNames += ", \(siteName)" }
            }
        }
        return result
    }
    
    func isDeviceConnected(device_id: Int64) -> Bool {
        let query = "SELECT COUNT(*) FROM Events Where _class = 2 AND detector = 8 AND reason = 3 AND affect = 1 AND device = \(device_id) AND section = 0"
        return (try? main.db.scalar(query) as? Int64) == 0
    }
    
    func getDeviceIds(site_id: Int64? = nil, onlyHubId: Bool = false) -> [Int64] {
        var result: [Int64] = []
        var query = "SELECT Devices.id, Devices.cluster_id, Devices.configVersion FROM Devices, DeviceSection WHERE DeviceSection.device = Devices.id "
        if let site_id = site_id { query += "AND DeviceSection.site = \(site_id)" }
        query += " GROUP BY Devices.id"
        guard let rows = try? main.db.prepare(query) else { return result }
        for obj in rows {
            if let device_id = obj[0] as? Int64, let cluster = obj[1] as? Int64, let cV = obj[2] as? Int64, !(!HubConst.isHub(cV, cluster: cluster) && onlyHubId) {
                result.append(device_id)
            }
        }
        return result
    }
    
    func isHub(device_id: Int64) -> Bool {
        let query = "SELECT Devices.cluster_id, Devices.configVersion FROM Devices WHERE Devices.id = \(device_id)"
        guard let rows = try? main.db.prepare(query) else { return false }
        for obj in rows {
            if let cluster = obj[0] as? Int64, let cV = obj[1] as? Int64, HubConst.isHub(cV, cluster: cluster) {
                return true
            }
        }
        return false
    }
    
    /////////////////////////////////////////////////////////////////////////////
    
    
    func get(device_id: Int64) -> Devices? {
        if let q = ((try? main.db.pluck(main.device.filter( Devices.id == device_id ))) as Row??),
            let row = q
            { return Devices.fromRow(row) }
        return nil
    }

    func getSiteIds(_ deviceId: Int64) -> [Int64] {
        var result: [Int64] = []
        let query = "SELECT site FROM DeviceSection WHERE device = \(deviceId) GROUP BY DeviceSection.site"
        guard let rows = try? main.db.prepare(query) else { return result }
        for obj in rows {
            if let id = obj[0] as? Int64{ result.append(id) }
        }
        return result
    }
    
    func getSiteDeviceSectionIds(device: Int64) -> [(site: Int64, device: Int64, section: Int64)] {
        var result: [(site: Int64, device: Int64, section: Int64)] = []
        let query = "SELECT site, section FROM DeviceSection WHERE device = \(device) ORDER BY device"
        guard let rows = try? main.db.prepare(query) else { return result }
        for obj in rows {
            if let site = obj[0] as? Int64, let section = obj[1] as? Int64 { result.append((site: site, device: device, section: section)) }
        }
        return result
    }
    
    func getSiteDeviceSectionIds(site: Int64) -> [(site: Int64, device: Int64, section: Int64)] {
        var result: [(site: Int64, device: Int64, section: Int64)] = []
        let query = "SELECT device, section FROM DeviceSection WHERE site = \(site) ORDER BY device"
        guard let rows = try? main.db.prepare(query) else { return result }
        for obj in rows {
            if let device = obj[0] as? Int64, let section = obj[1] as? Int64 { result.append((site: site, device: device, section: section)) }
        }
        return result
    }
}
