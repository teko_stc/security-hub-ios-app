//
//  DBHelper+SettingsDB.swift
//  SecurityHub
//
//  Created by Timerlan on 22.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite
import RxSwift

extension DBHelper{
    func needJson() -> Bool {
        guard let c = ((try? self.classes.db.scalar("SELECT count(*) FROM \(jClasses.tableName())") as? Int64) as Int64??) else { return true }
        return c == 0
    }
    
    func addJson(_ jClasses : [jClasses]) {
        try? classes.db.transaction { jClasses.forEach({ (jc) in jc.insert(classes.db, table: classes.jclasses) }) }
    }
    
    func get(_class: Int64, reason: Int64, detector: Int64, active: Int64) -> jClasses {
        do{
            if let jC = try self.classes.db.pluck(self.classes.jclasses.filter(jClasses._class == _class && jClasses.reasons == reason && jClasses.detectors == detector && jClasses.statements == active)){
                return jClasses.fromRow(jC)
            }else if let jC = try self.classes.db.pluck(self.classes.jclasses.filter(jClasses._class == _class && jClasses.reasons == reason && jClasses.detectors == nil && jClasses.statements == active)){
                return jClasses.fromRow(jC)
            }else if let jC = try self.classes.db.pluck(self.classes.jclasses.filter(jClasses._class == _class && jClasses.reasons == nil && jClasses.detectors == detector && jClasses.statements == active)){
                return jClasses.fromRow(jC)
            }else if let jC = try self.classes.db.pluck(self.classes.jclasses.filter(jClasses._class == _class && jClasses.reasons == nil && jClasses.detectors == nil && jClasses.statements == active)){
                return jClasses.fromRow(jC)
            }
        }catch {  }
        return jClasses()
    }
    
    func getAllClasses() -> [(id: Int64, name: String)]{
        var result: [(id: Int64, name: String)] = []
        guard let rows = try? self.classes.db.prepare(self.classes.jclasses.filter(jClasses.detectors == -1 && jClasses.reasons == -1 && jClasses.statements == -1 ).order(jClasses._class.asc)) else {
            return result
        }
        for row in rows {
            result.append((id: row[jClasses._class], name: row[jClasses.name]))
        }
        return result
    }
    
    func clearJClass() {
        do {
            try classes.db.run(classes.jclasses.delete())
        } catch {}
    }
}

