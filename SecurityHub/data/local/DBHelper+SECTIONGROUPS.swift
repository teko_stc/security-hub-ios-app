//
//  DBHelper+SECTIONGROUPS.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import SQLite

//SECTIONGROUPS
extension DBHelper{
    func add(sectionGroup: SectionGroups) {
        switch sectionGroup.getDbStatus(main.db, table: main.sectionGroups) {
        case .needInsert:
            sectionGroup.insert(main.db, table: main.sectionGroups)
        case .needUpdate:
            sectionGroup.update(main.db, table: main.sectionGroups)
        case .noNeed:
            break
        }
    }
    
    func getSectionGroups() -> [SectionGroups] {
        var result : [SectionGroups] = []
        
        guard let rows = try? main.db.prepare(main.sectionGroups.order(SectionGroups.id.asc)) else { return result }
        for row in rows{ result.append(SectionGroups.fromRow(row)) }
        return result
    }
    
    func getSectionGroup(id: Int64) -> SectionGroups? {
        guard let row = try? main.db.pluck(main.sectionGroups.filter(SectionGroups.id == id)) else  { return nil }
        return SectionGroups.fromRow(row)
    }
    
    func removeSectionGroup(id: Int64) {
        guard let _ = try? main.db.run(main.sectionGroups.filter(SectionGroups.id == id).delete()) else { return }
    }
}
