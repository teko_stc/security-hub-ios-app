//
//  DBHelper+AUTOARM.swift
//  SecurityHub
//
//  Created by Stefan on 12.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import SQLite

//AUTOARM
extension DBHelper{
		func add(autoArm: AutoArm) {
				switch autoArm.getDbStatus(main.db, table: main.autoArm) {
				case .needInsert:
					autoArm.insert(main.db, table: main.autoArm)
				case .needUpdate:
					autoArm.update(main.db, table: main.autoArm)
				case .noNeed:
						break
				}
		}
		
		func getAutoArms() -> [AutoArm] {
				var result : [AutoArm] = []
				
				guard let rows = try? main.db.prepare(main.autoArm.order(AutoArm.id.asc)) else { return result }
				for row in rows{ result.append(AutoArm.fromRow(row)) }
				return result
		}
		
		func getAutoArm(id: Int64) -> AutoArm? {
				guard let row = try? main.db.pluck(main.autoArm.filter(AutoArm.id == id)) else  { return nil }
				return AutoArm.fromRow(row)
		}
		
		func removeAutoArm(id: Int64) {
				guard let _ = try? main.db.run(main.autoArm.filter(AutoArm.id == id).delete()) else { return }
		}
}

