//
//  Settings.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

class SettingsHelper {
    private let NEED_PUSH = "NEED_PUSH"
    private let NEED_PIN = "NEED_PIN"
    private let NEED_SHARED_MAIN = "NEED_SHARED_MAIN"
    private let MASK_PUSH = "MASK_PUSH"
    private let MASK_STORY = "MASK_STORY"
    private let STRING_IVI_TOKEN = "STRING_IVI_TOKEN"
    private let NEED_TOUCH_ID = "NEED_TOUCH_ID"
		private let SEND_LOCATION = "SEND_LOCATION"
    
    private let NEED_ANIMATION = "NEED_ANIMATION"
    private let NEED_DEVICES = "NEED_DEVICES"
    private let NEED_SECTIONS = "NEED_SECTIONSS"
    private let NEED_EXPERT = "NEED_EXPERT"
    
    private let NEED_ALARM_MONITOR = "NEED_ALARM_MONITOR"
    
    private let NEED_RAITING_IN_XABOUTCONTROLLER = "NEED_RAITING_IN_XABOUTCONTROLLER"
    
    var pushMask : Int {
        get {
            let o = userDefaults().integer(forKey: MASK_PUSH)
            if o == 0 { return 31 }
            if o == -1 { return 0 }
            return o
        }
        set {
            if newValue == 0 {
                set(value: -1, forKey: MASK_PUSH)
            }else{
                set(value: newValue, forKey: MASK_PUSH)
            }
        }
    }
    
    var storyMask : Int {
        get {
            let o = userDefaults().integer(forKey: MASK_STORY)
            if o == 0 { return 31 }
            if o == -1 { return 0 }
            return o
        }
        set {
            if newValue == 0 {
                set(value: -1, forKey: MASK_STORY)
            }else{
                set(value: newValue, forKey: MASK_STORY)
            }
        }
    }
    
    var ivideonToken : String {
        get { 
            XTargetUtils.ivideon != nil ? (userDefaults().string(forKey: STRING_IVI_TOKEN) ?? "") : ""
        }
        set { set(value: newValue, forKey: STRING_IVI_TOKEN)}
    }
    
    var needPin : Bool {
        get { return !userDefaults().bool(forKey: NEED_PIN) }
        set { set(value: !newValue, forKey: NEED_PIN) }
    }
    
    var needSharedMain : Bool {
        get { return userDefaults().bool(forKey: NEED_SHARED_MAIN) }
        set { set(value: newValue, forKey: NEED_SHARED_MAIN) }
    }
    
    var touchId : Bool {
        get { return userDefaults().bool(forKey: NEED_TOUCH_ID) }
        set { set(value: newValue, forKey: NEED_TOUCH_ID) }
    }
	
		var sendLocation : Bool {
				get { return userDefaults().bool(forKey: SEND_LOCATION) }
				set { set(value: newValue, forKey: SEND_LOCATION) }
		}
    
    var needAnimation : Bool {
        get { return userDefaults().bool(forKey: NEED_ANIMATION) }
        set { set(value: newValue, forKey: NEED_ANIMATION) }
    }
    
    var needDevices : Bool {
        get {
            return userDefaults().bool(forKey: NEED_DEVICES)
        }
        set { set(value: newValue, forKey: NEED_DEVICES) }
    }
    
    var needSections : Bool {
        get {
            return userDefaults().bool(forKey: NEED_SECTIONS)
        }
        set { set(value: newValue, forKey: NEED_SECTIONS) }
    }
    
    var needRatingInXAboutController : Bool {
        get { return !userDefaults().bool(forKey: NEED_RAITING_IN_XABOUTCONTROLLER) }
        set { set(value: !newValue, forKey: NEED_RAITING_IN_XABOUTCONTROLLER) }
    }
    
    var needAlarmMonitor: Bool {
        get { return !userDefaults().bool(forKey: NEED_ALARM_MONITOR) }
        set { set(value: !newValue, forKey: NEED_ALARM_MONITOR) }
    }
    
    func setPushSound(class_id: Int, value: String) {
        set(value: value, forKey: "PUSH_CLASS" + class_id.string)
    }
    
    func getPushSound(class_id: Int) -> String {
        userDefaults().string(forKey: "PUSH_CLASS" + class_id.string) ?? "default"
    }
    
    func setDefaults(){
//        defaults.register(defaults: [
//            NEED_PUSH: true,
//            NEED_PIN: true,
//            MASK_PUSH: 31,
//            MASK_STORY: 31,
//            NEED_TOUCH_ID: false,
//            NEED_ANIMATION: true,
//            NEED_DEVICES: false,
//            NEED_SECTIONS: false
//        ])
    }

    private func set(value: Any, forKey: String) {
        userDefaults().set(value, forKey: forKey)
        userDefaults().synchronize()
    }
}
