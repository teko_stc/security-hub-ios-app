//
//  D3ConstRepository.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XMLConstRepository: NSObject {
    public var classes: [ClassModel] = []
    public var physics: [PhysicModel] = []
    public var alarmTypes: [AlarmTypeModel] = []
    public var detectorTypes: [DetectorTypeModel] = []
    public var zoneTypes: [ZoneTypeModel] = []
    public var sectionTypes: [SectionTypeModel] = []
    public var inputTypes: [InputTypeModel] = []
    public var outputTypes: [OutputTypeModel] = []
    public var ioTypes: [IOTypeModel] = []
    public var deviceTypes: [DeviceTypeModel] = []
    
    public let parserDidEndClasses: (([ClassModel]) -> ())
    
    private var isOpenDetector: Bool = false
    private var isOpenReason: Bool = false
    private var isOpenStatement: Bool = false
    private var isOpenZoneType: Bool = false
    private var isOpenAlarmType: Bool = false
    private var isOpenSectionType: Bool = false
    private var isOpenInputType: Bool = false
    private var isOpenOutputType: Bool = false
    private var isOpenDeviceType: Bool = false

    init(languageIso: String, parserDidEndClasses: @escaping ([ClassModel]) -> ()) {
        self.parserDidEndClasses = parserDidEndClasses
        super.init()
        var xml: XMLParser? = nil
        if let p = Bundle.main.path(forResource: languageIso, ofType: "lproj"), let b = Bundle(path: p), let path = b.path(forResource: "x_proto_event_descript", ofType: "xml") {
            xml = XMLParser(contentsOf: URL(fileURLWithPath: path))
        } else if let path = Bundle.main.path(forResource: "x_proto_event_descript", ofType: "xml") {
            xml = XMLParser(contentsOf: URL(fileURLWithPath: path))
        }
        xml?.delegate = self
        xml?.parse()
    }
    
    private var ricons = Set<String>()
}

extension XMLConstRepository: XMLParserDelegate {
    
    private func wDr(_ value: String?) -> String? {
        if (value != nil) { ricons.insert(value!) }
        return value?.replacingOccurrences(of: "@drawable/", with: "")
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "class" {
            let _class = ClassModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, description: attributeDict["description"]!, iconSmall: wDr(attributeDict["iconSmall"])!, iconBig: wDr(attributeDict["iconBig"])!, affect: attributeDict["affect"]!)
            classes.append(_class)
        } else if elementName == "detectors" {
            isOpenDetector = true
        } else if elementName == "detector" {
            let detector = DetectorModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            classes.lastItem?.detectors.append(detector)
        } else if elementName == "reasons" {
            isOpenReason = true
        } else if elementName == "reason" {
            let reason = ReasonModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, iconSmall: wDr(attributeDict["iconSmall"])!, iconBig: wDr(attributeDict["iconBig"])!, backGroundColor: attributeDict["backGroundColor"]!, affect: attributeDict["affect"]!)
            if (isOpenDetector) { classes.lastItem?.detectors.lastItem?.reasons.append(reason)}
            else { classes.lastItem?.reasons.append(reason) }
        } else if elementName == "statements" {
            isOpenStatement = true
        } else if elementName == "statement" {
            let statement = StatementModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, description: attributeDict["description"]!, iconS: wDr(attributeDict["iconS"])!, color: attributeDict["color"])
            if (isOpenDetector && isOpenReason) {
                classes.lastItem?.detectors.lastItem?.reasons.lastItem?.statements.append(statement)
            } else if (isOpenDetector) {
                classes.lastItem?.detectors.lastItem?.statements.append(statement)
            } else if (isOpenReason) {
                classes.lastItem?.reasons.lastItem?.statements.append(statement)
            } else {
                classes.lastItem?.statements.append(statement)
            }
        } else if elementName == "icon" && isOpenStatement {
            let icon = wDr(attributeDict["src"])!
            if (isOpenDetector && isOpenReason) {
                classes.lastItem?.detectors.lastItem?.reasons.lastItem?.statements.lastItem?.icons.append(icon)
            } else if (isOpenDetector) {
                classes.lastItem?.detectors.lastItem?.statements.lastItem?.icons.append(icon)
            } else if (isOpenReason) {
                classes.lastItem?.reasons.lastItem?.statements.lastItem?.icons.append(icon)
            } else {
                classes.lastItem?.statements.lastItem?.icons.append(icon)
            }
        } else if elementName == "physic" {
            let physic = PhysicModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            physics.append(physic)
        } else if elementName == "zonetypes" {
            isOpenZoneType = true
        } else if elementName == "zonetype" {
            let zoneType = ZoneTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, caption_e: attributeDict["caption_e"]!, img: wDr(attributeDict["img"])!)
            zoneTypes.append(zoneType)
        } else if elementName == "detectortype" {
            let detectorType = DetectorTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, sectionMask: attributeDict["sectionmask"]?.toInt(), sectionDefault: attributeDict["sectiondefault"]?.toInt(), _default: attributeDict["default"]?.toInt())
            if (isOpenZoneType) { zoneTypes.lastItem?.detectorTypes.append(detectorType) }
            else if (isOpenInputType) { inputTypes.lastItem?.detectorTypes.append(detectorType) }
            else {
                detectorTypes.append(detectorType)
            }
        } else if elementName == "alarmtypes" {
            isOpenAlarmType = true
        } else if elementName == "alarmtype" {
            let alarmType = AlarmTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, icon: wDr(attributeDict["icon"])!, _default: attributeDict["default"]?.toInt())
            if (isOpenZoneType) { zoneTypes.lastItem?.detectorTypes.lastItem?.alarmTypes.append(alarmType) }
            else if (isOpenInputType) { inputTypes.lastItem?.detectorTypes.lastItem?.alarmTypes.append(alarmType) }
            else {
                alarmTypes.append(alarmType)
            }
        } else if elementName == "icon" {
            let icon = wDr(attributeDict["src"])!
            if (isOpenZoneType && isOpenAlarmType) { zoneTypes.lastItem?.detectorTypes.lastItem?.alarmTypes.lastItem?.icons.append(icon) }
            else if (isOpenZoneType) { zoneTypes.lastItem?.icons.append(icon) }
            else if (isOpenInputType && isOpenAlarmType) { inputTypes.lastItem?.detectorTypes.lastItem?.alarmTypes.lastItem?.icons.append(icon) }
            else if (isOpenInputType) { inputTypes.lastItem?.icons.append(icon) }
            else if (isOpenSectionType) { sectionTypes.lastItem?.icons.append(icon) }
            else if (isOpenOutputType) { outputTypes.lastItem?.icons.append(icon) }
            else if (isOpenDeviceType) { deviceTypes.lastItem?.icons.append(icon) }
        } else if elementName == "sectiontypes" {
            isOpenSectionType = true
        } else if elementName == "sectiontype" {
            let sectionType = SectionTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            sectionTypes.append(sectionType)
        } else if elementName == "inputtypes" {
            isOpenInputType = true
        } else if elementName == "inputtype" {
            let inputType = InputTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            inputTypes.append(inputType)
        } else if elementName == "outputtypes" {
            isOpenOutputType = true
        } else if elementName == "outputtype" {
            let outputType = OutputTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!, detector: attributeDict["detector"]!.toInt()!, oType: attributeDict["o_type"]!.toInt()!)
            outputTypes.append(outputType)
        } else if elementName == "devicetypes" {
            isOpenDeviceType = true
        } else if elementName == "devicetype" {
            let deviceType = DeviceTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            deviceTypes.append(deviceType)
        } else if elementName == "iotype" {
            let ioType = IOTypeModel(id: Int(attributeDict["id"]!)!, caption: attributeDict["caption"]!)
            ioTypes.append(ioType)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "detectors" {
            isOpenDetector = false
        } else if elementName == "reasons" {
            isOpenReason = false
        } else if elementName == "statements" {
            isOpenStatement = false
        } else if elementName == "zonetypes" {
            isOpenZoneType = false
        } else if elementName == "alarmtypes" {
            isOpenAlarmType = false
        } else if elementName == "sectiontypes" {
            isOpenSectionType = false
        } else if elementName == "inputtypes" {
            isOpenInputType = false
        } else if elementName == "outputtypes" {
            isOpenOutputType = false
        } else if elementName == "devicetypes" {
            isOpenDeviceType = false
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.parserDidEndClasses(classes)
//        let jsonEncoder = JSONEncoder()
//        jsonEncoder.outputFormatting = .prettyPrinted
//        let jsonData = try! jsonEncoder.encode(ricons)
//        let json = String(decoding: jsonData, as: UTF8.self)
//        print(json)
    }
}

extension XMLConstRepository {
    struct ClassModel: Codable {
        let id: Int
        let caption: String
        let description: String
        let iconSmall: String
        let iconBig: String
        let affect: String
        var detectors:  [DetectorModel] = []
        var reasons:    [ReasonModel] = []
        var statements: [StatementModel] = []
    }
    
    struct DetectorModel: Codable {
        let id: Int
        let caption: String
        var reasons:    [ReasonModel] = []
        var statements: [StatementModel] = []
    }

    struct ReasonModel: Codable {
        let id: Int
        let caption: String
        let iconSmall: String
        let iconBig: String
        let backGroundColor: String
        let affect: String
        var statements: [StatementModel] = []
    }

    struct StatementModel: Codable {
        let id: Int
        let caption: String
        let description: String
        let iconS: String
        let color: String?
        var icons: [String] = []
    }

    struct PhysicModel: Codable {
        let id: Int
        let caption: String
    }

    struct AlarmTypeModel: Codable {
        let id: Int
        let caption: String
        let icon: String
        let _default: Int?
        var icons: [String] = []
    }

    struct DetectorTypeModel: Codable {
        let id: Int
        let caption: String
        let sectionMask: Int?
        let sectionDefault: Int?
        let _default: Int?
        var alarmTypes: [AlarmTypeModel] = []
    }

    struct ZoneTypeModel: Codable {
        let id: Int
        let caption: String
        let caption_e: String
        let img: String
        var icons: [String] = []
        var detectorTypes: [DetectorTypeModel] = []
    }

    struct SectionTypeModel: Codable {
        let id: Int
        let caption: String
        var icons: [String] = []
    }

    struct InputTypeModel: Codable {
        let id: Int
        let caption: String
        var icons: [String] = []
        var detectorTypes: [DetectorTypeModel] = []
    }

    struct OutputTypeModel: Codable {
        let id: Int
        let caption: String
        let detector: Int
        let oType: Int
        var icons: [String] = []
    }

    struct IOTypeModel: Codable {
        let id: Int
        let caption: String
    }

    struct DeviceTypeModel: Codable {
        let id: Int
        let caption: String
        var icons: [String] = []
    }

}
