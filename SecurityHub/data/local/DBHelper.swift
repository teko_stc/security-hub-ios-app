//
//  RealmHelper.swift
//  SecurityHub test
//
//  Created by Timerlan on 13.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import SQLite
import RxSwift

class DBHelper {
    var main: DB
    let classes: DBClasses = DBClasses()
    
    init(login: String) {
        main = DB(login: login)
    }
    
    func add<T:SQLCommand>(_ obj: T, table: Table) -> NUpdateType{
        let inDB = obj.isWasInDb(self.main.db, table: table)
        if !inDB { obj.insert(self.main.db, table: table) }else{ obj.update(self.main.db , table: table) }
        return inDB ? NUpdateType.update : NUpdateType.insert
    }
    
    func getName(device_id: Int64, section_id: Int64 = 0, zone_id: Int64 = 0) -> String{
        if  let q = ((try? self.main.db.scalar("SELECT name FROM Devices Where id = \(device_id)")) as Binding??),
            let name = q as? String  { return name
        } else if section_id != 0 {
            guard let q = ((try? self.main.db.scalar("SELECT name FROM Sections Where device = \(device_id) AND section = \(section_id)")) as Binding??),
                let name = q as? String else { return "no_name" }
            return "\(name)(\(section_id)), \(getName(device_id: device_id))"
        } else if zone_id != 0 {
            guard let q = ((try? self.main.db.scalar("SELECT name FROM Zones Where device = \(device_id) AND section = \(section_id) AND zone = \(zone_id)")) as Binding??),
                let name = q as? String
                else { return "no_name" }
            return "\(name)(\(zone_id)), \(getName(device_id: device_id, section_id: section_id))"
        } else{ return "no_name" }
    }
    
    func getAllNames() -> [DZoneFullNameEntity] {
        let query = "SELECT Sites.id, Sites.name, Devices.id, Devices.name, Sections.section, Sections.name, Zones.zone, Zones.name " +
            "FROM Sites, Devices, DeviceSection, Sections, Zones " +
            "WHERE DeviceSection.site = Sites.id AND DeviceSection.device = Devices.id AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section AND Sections.device = Zones.device AND Sections.section = Zones.section"
        var result: [DZoneFullNameEntity] = []
        guard let rows = try? main.db.prepare(query) else { return result }
        for row in rows { if let zoneFullName = DataEntityMapper.zoneFullName(row) { result.append(zoneFullName) } }
        return result
    }
    
    func getAllAlarmAffectsInfo() -> (Int,Bool) {
        if let count = try? main.db.scalar(main.events.filter(Events._class < 4).filter(Events.affect == 1).filter(Events.active == 1).count),
           let alarm = try? main.db.scalar(main.events.filter(Events._class == 0).filter(Events.affect == 1).filter(Events.active == 1).count) { return (count,alarm>0) }
        else { return (0,false) }
    }
    
    func getAffects(site_id: Int64? = nil, device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> [Events]{
        if site_id != nil && device_id == nil && section_id == nil && zone_id == nil { return getAffects(site_id: site_id!) }
        if site_id == nil && device_id != nil && section_id == nil && zone_id == nil { return getAffects(device_id: device_id!) }
        if site_id == nil && device_id != nil && section_id != nil && zone_id == nil { return getAffects(device_id: device_id!, section_id: section_id!) }
        if site_id == nil && device_id != nil && section_id != nil && zone_id != nil { return getAffects(device_id: device_id!, section_id: section_id!, zone_id: zone_id!) }
        return []
    }
}

enum ARMSTATUS { case arm, disarm, notfull, notsecure, device, on, off }
