//
//  DBHelper+EVENTS.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//EVENTS
extension DBHelper{
    func isEventInDb(_ id: Int64) -> Bool {
        if let _ = try? main.db.pluck(main.events.filter(Events.id == id)) { return true }
        return false
    }
    
    func getReferenceName(_ command_id: Int64) -> String? {
        if let name = try? main.db.scalar("SELECT name FROM [References] WHERE command_id = \(command_id)") as? String { return name }
        return nil
    }
    
    func addReference(command_id: Int64, name: String) {
        References(command_id: command_id, name: name).insert(main)
    }
    
    func clearAffects() -> [DAffectRemove]{
        let query = "SELECT device, section, zone, _class, reason From Events Where affect = 1 GROUP BY device, section, zone"
        
        var result: [DAffectRemove] = []
        guard let rows = try? main.db.prepare(query) else { return result}
        for row in rows { if let aInfo = DataEntityMapper.affectRemove(row) { result.append(aInfo) } }
        let _ = try? main.db.run(main.events.update(Events.affect <- 0))
        return result
    }
    
    func setAffect(_ event_id: Int64) {
        let _ = try? main.db.run(main.events.filter(Events.id == event_id).update(Events.affect <- 1))
    }
    
    
    func clearAffect(_ event_id: Int64) -> DAffectRemove? {
        let query = "SELECT device, section, zone, _class, reason FROM Events WHERE id = \(event_id)"
        
        var result: DAffectRemove?
        guard let rows = try? main.db.prepare(query) else { return result}
        if  let row = rows.first(where: { (_) -> Bool in return true }),
            let aInfo = DataEntityMapper.affectRemove(row) {
            result = aInfo
        }
        let _ = try? main.db.run(main.events.filter(Events.id == event_id).update(Events.affect <- 0))
        if let q = ((try? main.db.pluck(main.events.filter(Events.id == event_id))) as Row??), let row = q {
            var event: Events = Events.fromRow(row)
            NotificationCenter.default.post(name: HubNotification.eventUpdate, object: (event: event, type: NUpdateType.update))
        }
        return result
    }
    
    ////////////////////////////////////////////////////////////
    
    
    func add(event: Events){
        if event._class == 15 && event.detector == 0 && (event.reason == 2 || event.reason == 1) {
            return
        }
        NotificationCenter.default.post(name: HubNotification.eventUpdate, object: (event: event, type: add(event, table: main.events)))
    }
  
    func deleteEvents(device_id: Int64? = nil, section_id: Int64? =  nil, zone_id: Int64? = nil) {
        do{
            try main.db.run( main.events.filter(device_id == nil || Events.device == device_id ?? 0).filter(section_id == nil || Events.section == section_id ?? 0).filter(zone_id == nil || Events.zone == zone_id ?? 0).delete())
        }catch {}
    }
    
    func get(_class: Int64, reason: Int64, active: Int64, device_id: Int64) -> Events? {
        let query = "SELECT * FROM Events WHERE device = \(device_id) AND _class = \(_class) AND reason = \(reason) AND active = \(active) AND id = (SELECT MAX(id) FROM Events WHERE device = \(device_id) AND _class = \(_class) AND reason = \(reason) AND active = \(active))"
        guard let rows = try? main.db.prepare(query), let row = rows.first(where: { _ in return true }) else { return nil }
        return CastHelper.events(obj: row)
    }
    
    func get(event_id: Int64) -> Events? {
        if let row = try? main.db.pluck(main.events.filter(Events.id == event_id)) { return Events.fromRow(row) }
        return nil
    }
    
    func rmvAffects(event_id: Int64) -> Events?{
        if let q = ((try? main.db.pluck(main.events.filter(Events.id == event_id))) as Row??),
            let row = q {
            var event: Events = Events.fromRow(row)
            event.affect = 0; event.update(main.db, table: main.events)
            return Events.fromRow(row)
        };return nil
    }
    
    func rmvAffects(){
        do {
            try main.db.run(main.events.update(Events.affect <- 0))
        }catch {}
    }
    
    func getLastEventTime() -> Int64 {
        do { if let time = try main.db.scalar(main.events.select(Events.time.max)) { return time } }
        catch {  return 0 }
        return 0
    }
    
    func getMaxTime(device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> Int64{
        return (try? main.db.scalar(
            main.events
                .filter(device_id == nil || Events.device == device_id ?? 0)
                .filter(section_id == nil || Events.section == section_id ?? 0)
                .filter(zone_id == nil || Events.zone == zone_id ?? 0)
                .select(Events.time.max)
            )) ?? 0
    }
    
    func getEvents(device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> [Events]{
        var result: [Events] = []
        guard let rows = try? main.db.prepare(
            main.events
                .filter(device_id == nil || Events.device == device_id ?? 0)
                .filter(section_id == nil || Events.section == section_id ?? 0)
                .filter(zone_id == nil || Events.zone == zone_id ?? 0)
                .order(Events.time.desc)
            ) else { return result }
        for row in rows { result.append(Events.fromRow(row)) }
        return result
    }
    
    func getEvents(_class: Int64, active: Int64) -> [Events]{
        var result: [Events] = []
        guard let rows = try? main.db.prepare(
            main.events
                .filter(Events._class == _class)
                .filter(Events.active == active)
                .order(Events.time.desc)
            ) else { return result }
        for row in rows { result.append(Events.fromRow(row)) }
        return result
    }
    
    func getEvents(startTime: Int64, endTime: Int64) -> [Events]{
        var result: [Events] = []
        guard let rows = try? main.db.prepare(
            main.events
                .filter(Events.time >=  startTime && endTime >= Events.time)
                .order(Events.time.desc)
            ) else { return result }
        for row in rows { result.append(Events.fromRow(row)) }
        return result
    }
    
    func getEventsXAlarmAffectsController() -> [Events]{
        var result: [Events] = []
        guard let rows = try? main.db.prepare(
            main.events
                .filter(Events._class < 4)
                .filter(Events.active == 1)
                .order(Events.time.desc)
            ) else { return result }
        for row in rows { result.append(Events.fromRow(row)) }
        return result
    }
    
    func getDEvents(startTime: Int64, endTime: Int64, _class: Int64?, siteId: Int64?) -> [DEventEntity] {
        let eventParams = "Events.id, Events._class, Events.reason, Events.detector, Events.active, Events.affect, Events.device, Events.section, Events.zone, Events.time, Events.jdata, Events.icon, Events.iconBig, Events.affect_desc, Events.dropped"
        let deviceSectionParams = "DeviceSection.site"
        let siteParams = "Sites.name"
        let deviceParams = "Devices.name"
        let sectionParams = "Sections.name"
        let zoneParams = "Zones.name"
        let videoParams = "IvVideo.result, IvVideo.isError"

        let timeCondition = "Events.time >= \(startTime) AND Events.time <= \(endTime)"
        
        var _classCondition = "(Events._class <= 5 OR Events._class = 8 OR (Events._class = 10 AND Events.reason != 4) OR Events._class = 11)"

        if let _class = _class, _class == 10 {
            _classCondition = "(Events._class = \(_class) AND Events.reason != 4)"
        } else if let _class = _class {
            _classCondition = "Events._class = \(_class)"
        }
        
        let eventDeviceSectionCondition = "Events.device = DeviceSection.device AND Events.section = DeviceSection.section"
        let siteDeviceSectionCondition = "DeviceSection.site = Sites.id"
        let eventDeviceCondition = "Events.device = Devices.id AND DeviceSection.device = Devices.id"
        let eventSectionCondition = "Events.device = Sections.device AND Events.section = Sections.section AND DeviceSection.device = Sections.device AND DeviceSection.section = Sections.section"
        let eventZoneCondition = "Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone"
        let eventVideoCondition = "Events.id = IvVideo.eventId"

        var siteIdCondition = "(1 > 0)"
        if let siteId = siteId {
            siteIdCondition = "DeviceSection.site = \(siteId)"
        }
        
        let query = "SELECT \(eventParams), \(deviceSectionParams), \(siteParams), \(deviceParams), \(sectionParams), \(zoneParams), \(videoParams) " +
                    "FROM Events " +
                    "INNER JOIN DeviceSection ON \(eventDeviceSectionCondition) " +
                    "INNER JOIN Sites ON \(siteDeviceSectionCondition) " +
                    "INNER JOIN Devices ON \(eventDeviceCondition) " +
                    "LEFT OUTER JOIN Sections ON \(eventSectionCondition) " +
                    "LEFT OUTER JOIN Zones ON \(eventZoneCondition) " +
                    "LEFT OUTER JOIN IvVideo ON \(eventVideoCondition) " +
                    "WHERE \(timeCondition) AND \(_classCondition) AND \(siteIdCondition) " +
                    "GROUP BY Events.id " +
                    "ORDER BY Events.time DESC"
    
        guard let rows = try? main.db.prepare(query) else { return [] }
        return rows.map { (obj) -> DEventEntity in return DataEntityMapper.dEvent(obj: obj) }
    }
    
    func getDEvent(id: Int64) -> DEventEntity? {
        let eventParams = "Events.id, Events._class, Events.reason, Events.detector, Events.active, Events.affect, Events.device, Events.section, Events.zone, Events.time, Events.jdata, Events.icon, Events.iconBig, Events.affect_desc, Events.dropped"
        let deviceSectionParams = "DeviceSection.site"
        let siteParams = "Sites.name"
        let deviceParams = "Devices.name"
        let sectionParams = "Sections.name"
        let zoneParams = "Zones.name"
        let videoParams = "IvVideo.result, IvVideo.isError"

        let eventCondition = "Events.id = \(id)"
        
        let eventDeviceSectionCondition = "Events.device = DeviceSection.device AND Events.section = DeviceSection.section"
        let siteDeviceSectionCondition = "DeviceSection.site = Sites.id"
        let eventDeviceCondition = "Events.device = Devices.id"
        let eventSectionCondition = "Events.device = Sections.device AND Events.section = Sections.section"
        let eventZoneCondition = "Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone"
        let eventVideoCondition = "Events.id = IvVideo.eventId"

        let query = "SELECT \(eventParams), \(deviceSectionParams), \(siteParams), \(deviceParams), \(sectionParams), \(zoneParams), \(videoParams) " +
                    "FROM Events " +
                    "INNER JOIN DeviceSection ON \(eventDeviceSectionCondition) " +
                    "INNER JOIN Sites ON \(siteDeviceSectionCondition) " +
                    "INNER JOIN Devices ON \(eventDeviceCondition) " +
                    "LEFT OUTER JOIN Sections ON \(eventSectionCondition) " +
                    "LEFT OUTER JOIN Zones ON \(eventZoneCondition) " +
                    "LEFT OUTER JOIN IvVideo ON \(eventVideoCondition) " +
                    "WHERE \(eventCondition) " +
                    "GROUP BY \(eventParams), \(deviceSectionParams), \(siteParams), \(deviceParams), \(sectionParams), \(zoneParams), \(videoParams) " +
                    "ORDER BY Events.time DESC"
    
        guard let rows = try? main.db.prepare(query) else { return nil }
        return rows.map { (obj) -> DEventEntity in return DataEntityMapper.dEvent(obj: obj) }.first
    }
		
    func getDEvents(deviceId: Int64, zoneId: Int64, _class: Int64?) -> [DEventEntity] {
        let eventParams = "Events.id, Events._class, Events.reason, Events.detector, Events.active, Events.affect, Events.device, Events.section, Events.zone, Events.time, Events.jdata, Events.icon, Events.iconBig, Events.affect_desc, Events.dropped"
        let deviceSectionParams = "DeviceSection.site"
        let siteParams = "Sites.name"
        let deviceParams = "Devices.name"
        let sectionParams = "Sections.name"
        let zoneParams = "Zones.name"
        let videoParams = "IvVideo.result, IvVideo.isError"

        let deviceIdCondition = "Events.device = \(deviceId)"
        
        var zoneIdCondition = "Events.zone = \(zoneId)"
        if zoneId == 0 { zoneIdCondition = "(Events.zone = 0 OR Events.zone = 100)" }

        var _classCondition = "(1 > 0)"
        if let _class = _class { _classCondition = "Events._class = \(_class)" }
        
        let eventDeviceSectionCondition = "Events.device = DeviceSection.device AND Events.section = DeviceSection.section"
        let siteDeviceSectionCondition = "DeviceSection.site = Sites.id"
        let eventDeviceCondition = "Events.device = Devices.id"
        let eventSectionCondition = "Events.device = Sections.device AND Events.section = Sections.section"
        let eventZoneCondition = "Events.device = Zones.device AND Events.section = Zones.section AND Events.zone = Zones.zone"
        let eventVideoCondition = "Events.id = IvVideo.eventId"
        
        let query = "SELECT \(eventParams), \(deviceSectionParams), \(siteParams), \(deviceParams), \(sectionParams), \(zoneParams), \(videoParams) " +
                    "FROM Events " +
                    "INNER JOIN DeviceSection ON \(eventDeviceSectionCondition) " +
                    "INNER JOIN Sites ON \(siteDeviceSectionCondition) " +
                    "INNER JOIN Devices ON \(eventDeviceCondition) " +
                    "LEFT OUTER JOIN Sections ON \(eventSectionCondition) " +
                    "LEFT OUTER JOIN Zones ON \(eventZoneCondition) " +
                    "LEFT OUTER JOIN IvVideo ON \(eventVideoCondition) " +
                    "WHERE \(deviceIdCondition) AND \(zoneIdCondition) AND \(_classCondition) " +
                    "GROUP BY Events.id " +
                    "ORDER BY Events.time DESC"
    
        guard let rows = try? main.db.prepare(query) else { return [] }
        return rows.map { (obj) -> DEventEntity in return DataEntityMapper.dEvent(obj: obj) }
    }
}
