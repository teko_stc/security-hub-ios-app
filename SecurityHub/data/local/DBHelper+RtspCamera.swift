//
//  DBHelper+RtspCamera.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import SQLite
import RxSwift

enum XRxModelState {
    case insert
    case update
    case delete
}

class XRTSPCameraDataLayerMapper {
    static func cast(data: XRTSPCameraDataLayerModel) -> RtspCamera {
        return RtspCamera(id: Int64(data.id ?? 0), name: data.name, url: data.url)
    }
    static func cast(rtspCamera: RtspCamera, state: XRxModelState) -> XRTSPCameraDataLayerRxModel {
        return XRTSPCameraDataLayerRxModel(id: Int(rtspCamera.id), name: rtspCamera.name, url: rtspCamera.url, state: state)
    }
    static func cast(rtspCamera: RtspCamera) -> XRTSPCameraDataLayerModel {
        return XRTSPCameraDataLayerModel(id: Int(rtspCamera.id), name: rtspCamera.name, url: rtspCamera.url)
    }
    static func cast(rxmodel: XRTSPCameraDataLayerRxModel) -> XRTSPCameraDataLayerModel {
        return XRTSPCameraDataLayerModel(id: rxmodel.id, name: rxmodel.name, url: rxmodel.url)
    }
}

struct XRTSPCameraDataLayerModel {
    var id: Int? = nil
    var name: String
    var url: String
}

struct XRTSPCameraDataLayerRxModel {
    let id: Int
    let name: String
    let url: String
    let state: XRxModelState
}

protocol XRTSPCameraDataLayerProtocol {
    func add(data: XRTSPCameraDataLayerModel) -> Bool
    func update(id: Int, name: String?, url: String?) -> Bool
    func update(id: Int, name: String?) -> Bool
    func update(id: Int, url: String?) -> Bool
    func delete(id: Int) -> Bool
    func items() -> Observable<XRTSPCameraDataLayerRxModel>
    func item(id: Int) -> Observable<XRTSPCameraDataLayerRxModel>
}

//RTSPCamera
extension DBHelper: XRTSPCameraDataLayerProtocol {
    func add(data: XRTSPCameraDataLayerModel) -> Bool {
        var rtspCamera = XRTSPCameraDataLayerMapper.cast(data: data)
        if let id = rtspCamera.insertWithResult(main.db, table: main.rtspCamera) {
            rtspCamera.id = id
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RtspCamera"), object: XRTSPCameraDataLayerMapper.cast(rtspCamera: rtspCamera, state: .insert))
            return true
        }
        return false
    }
    
    func update(id: Int, name: String?, url: String?) -> Bool {
        var values: [SQLite.Setter] = []
        if let name = name { values.append(RtspCamera.nameId <- name) }
        if let url = url { values.append(RtspCamera.urlId <- url) }
        if let _ = try? main.db.run(main.rtspCamera.filter(RtspCamera.id == Int64(id)).update(values)) {
            if let row = try? self.main.db.pluck(self.main.rtspCamera.filter(RtspCamera.id == Int64(id))) {
                let rtspxCamera: RtspCamera = RtspCamera.fromRow(row)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RtspCamera"), object: XRTSPCameraDataLayerMapper.cast(rtspCamera: rtspxCamera, state: .update))
            }
            return true
        }
        return false
    }
    
    func update(id: Int, name: String?) -> Bool { return update(id: id, name: name, url: nil) }
    
    func update(id: Int, url: String?) -> Bool { return update(id: id, name: nil, url: url) }
    
    func delete(id: Int) -> Bool {
        let rtspCamera = RtspCamera(id: Int64(id))
        rtspCamera.delete(main.db, table: main.rtspCamera)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RtspCamera"), object: XRTSPCameraDataLayerMapper.cast(rtspCamera: rtspCamera, state: .delete))
        return true
    }
    
    func items() -> Observable<XRTSPCameraDataLayerRxModel> {
        Observable.create{ observer -> Disposable in
            if let rows = try? self.main.db.prepare(self.main.rtspCamera) {
                rows.map { XRTSPCameraDataLayerMapper.cast(rtspCamera: RtspCamera.fromRow($0), state: .insert) }.forEach { value in observer.onNext(value) }
            }
            let o = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name(rawValue: "RtspCamera"), object: nil, queue: nil) { n in
                if let value = n.object as? XRTSPCameraDataLayerRxModel { observer.onNext(value) }
            }
            return Disposables.create{ DataManager.shared.nCenter.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func item(id: Int) -> Observable<XRTSPCameraDataLayerRxModel> {
        Observable.create{ observer -> Disposable in
            if let row = try? self.main.db.pluck(self.main.rtspCamera.filter(RtspCamera.id == Int64(id))) {
                observer.onNext(XRTSPCameraDataLayerMapper.cast(rtspCamera: RtspCamera.fromRow(row), state: .insert))
            }
            let o = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name(rawValue: "RtspCamera"), object: nil, queue: nil) { n in
                if let value = n.object as? XRTSPCameraDataLayerRxModel { observer.onNext(value) }
            }
            return Disposables.create{ DataManager.shared.nCenter.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
}
