//
//  DBHelper+SECTIONGROUPSECTION.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import SQLite

//SECTIONGROUPSECTION
extension DBHelper{
    func add(sectionGroupSection: SectionGroupSection) {
        switch sectionGroupSection.getDbStatus(main.db, table: main.sectionGroupSections) {
        case .needInsert:
            sectionGroupSection.insert(main.db, table: main.sectionGroupSections)
        case .needUpdate:
            sectionGroupSection.update(main.db, table: main.sectionGroupSections)
        case .noNeed:
            break
        }
    }
    
    func getSectionIds(sectionGroupId: Int64) -> [Int64] {
        var result : [Int64] = []

        guard let rows = try? main.db.prepare(main.sectionGroupSections.order(SectionGroupSection.id.asc)) else { return result }
        for row in rows{
            if row[SectionGroupSection.sectionGroupId] == sectionGroupId {
                result.append(row[SectionGroupSection.sectionId])
            }
        }
        return result
    }
    
    func removeSectionGroupSection(id: Int64) {
        guard let _ = try? main.db.run(main.sectionGroups.filter(SectionGroupSection.id == id).delete()) else { return }
    }
}
