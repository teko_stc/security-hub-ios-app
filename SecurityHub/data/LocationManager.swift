//
//  LocationManager.swift
//  SecurityHub
//
//  Created by Stefan on 28.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

class LocationManager: NSObject, CLLocationManagerDelegate {
	private let locationManager = CLLocationManager()
	private var locationCompletionHandler: (((latitude: Double,longitude: Double), _ error: String?)->Void)?
	
	override init() {
		super.init()

		self.locationManager.activityType = .automotiveNavigation
		self.locationManager.delegate = self
	}
	
	class func getLocation(jdata: String?) -> (Double,Double)? {
		guard let jdata else { return nil }
			
		let data = Data (jdata.utf8)
		
		do {
			if let json = try JSONSerialization.jsonObject(with: data) as? [String:Any] {
				if let latitude = json["lat"] as? Double, let longitude = json["lon"] as? Double {
					return (latitude,longitude) }
			}
		} catch { }
		
		return nil
	}
	
	public func requestUserLocation (completionHandler: @escaping ((latitude: Double,longitude: Double), _ error: String?)->Void) -> Bool
		{
			locationCompletionHandler = completionHandler
			
			let status = CLLocationManager.authorizationStatus()
			
			if status == .authorizedWhenInUse || status == .authorizedAlways || status == .notDetermined {
				
				if status == .notDetermined {
					locationManager.requestWhenInUseAuthorization()
				} else {
					locationManager.startUpdatingLocation()
				}
				
				return true
			}
			
			return false
		}
	
		public func stopUpdateLocation ()
		{
			locationManager.stopUpdatingLocation()
			locationCompletionHandler = nil
		}
	
		func locationManager (_ manager: CLLocationManager,
													didChangeAuthorization status: CLAuthorizationStatus) {
			switch status {
				case .authorizedWhenInUse, .authorizedAlways:
					manager.startUpdatingLocation()
				case .notDetermined: return
				
				default:
					DispatchQueue.main.async {
						self.locationCompletionHandler? ((latitude: 0, longitude: 0),"") }
			}
		}
	
		func locationManager (_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
		{
			if let location = locations.lastItem {
				DispatchQueue.main.async {
					self.locationCompletionHandler? ((latitude: location.coordinate.latitude, longitude: location.coordinate.longitude),nil) }
			}
		}
	
		func locationManager (_ manager: CLLocationManager, didFailWithError error: Error) {
			DispatchQueue.main.async {
				self.locationCompletionHandler? ((latitude: 0, longitude: 0),error.localizedDescription) }
		}
}
