//
//  DataManager+DB.swift
//  SecurityHub
//
//  Created by Timerlan on 24/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension DataManager {
    func getSiteInfoSingle(site_id: Int64) -> Single<DSiteInfo>{
        let single = Single<DSiteInfo>.create { event -> Disposable in
            let dSI = self.dbHelper.getSiteInfo(site_id: site_id)
            event(SingleEvent.success(dSI))
            return Disposables.create { }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func isAvailableDevice(device_id: Int64) -> Single<DCommandResult> {
        let single = Single<DCommandResult>.create { event -> Disposable in
            if !self.dbHelper.isDeviceConnected(device_id: device_id) { event(SingleEvent.success(DCommandResult.NO_SIGNAL_DEVICE)) }
            else if self.dbHelper.getArmStatus(device_id: device_id) != .disarmed { event(SingleEvent.success(DCommandResult.DEVICE_ARMED)) }
            else { event(SingleEvent.success(DCommandResult.SUCCESS)) }
            return Disposables.create { }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func getHubDeviceIds(site_id: Int64? = nil) -> [Int64] {
        return dbHelper.getDeviceIds(site_id: site_id, onlyHubId: true)
    }
    
    func isHub(device_id: Int64) -> Bool {
        return dbHelper.isHub(device_id: device_id)
    }
    
    func getDevices(site_id: Int64) -> [Devices] {
        return dbHelper.getDevices().filter { $0.siteIds.contains(site_id) }.map { $0.device }
    }
    
    func getDevice(site_id: Int64, device_id: Int64) -> Devices? {
        return getDevices(site_id: site_id).first(where: { $0.id == device_id })
    }
    
    func getSections(site_id: Int64? = nil, device_id: Int64? = nil) -> [Sections] {
        return dbHelper.getSections(site_id: site_id, device_id: device_id)
    }
    
    func getSections(site_id: Int64) -> [Sections] {
        var sections = [Sections]()
        let devices = getDevices(site_id: site_id)
        
        devices.forEach { device in
            let deviceSections = getSections(site_id: site_id, device_id: device.id)
            sections.append(contentsOf: deviceSections)
        }
        
        return sections
    }
    
    func getSection(site_id: Int64, section_id: Int64) -> Sections? {
        return getSections(site_id: site_id).first(where: { $0.id == section_id })
    }
    
    func getZones(site_id: Int64) -> [Zones] {
        var zones = [Zones]()
        let devices = getDevices(site_id: site_id)
        
        devices.forEach { device in
            let deviceZones = dbHelper.getZones(device_id: device.id)
            zones.append(contentsOf: deviceZones)
        }
        
        return zones
    }
    
    func getZone(site_id: Int64, zone_id: String) -> Zones? {
        return getZones(site_id: site_id).first(where: { $0.id == zone_id })
    }
    
    func getDeviceIds(site_id: Int64) -> [Int64] {
        return dbHelper.getDeviceIds(site_id: site_id)
    }
    
    func getDeviceInfo(device_id: Int64) -> DDeviceWithSitesInfo? {
        return dbHelper.getDevice(device_id: device_id)
    }
    
    func getDeviceSections(device_id: Int64) -> [Int64: DSectionNameAndTypeEntity] {
        return dbHelper.getSectionsNames(device_id: device_id)
    }
    
    func getScriptCommands(device_id: Int64, zone_id: Int64) -> [String : Int64] {
        return dbHelper.getScripts(device_ids: [device_id]).first { $0.bind == zone_id }?.getCommands() ?? [:]
    }
    
    ///ADD SCRIPT UI
    func getOnlyZonesScriptUi(device_id: Int64, notSelectValue: String? = nil) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ : ]
        if let notSelectValue = notSelectValue { result.updateValue(notSelectValue, forKey: 0) }
        dbHelper.getZones()
            .filter { $0.zone.device == device_id && $0.zone.zone != 100 && !HubConst.isRelay($0.zone.detector) && !HubConst.isExit($0.zone.detector) }
            .forEach { result.updateValue($0.zone.name, forKey: $0.zone.zone) }
        return Array(result).sorted { $0.key < $1.key }
    }
    func getSectionsScriptUi(device_id: Int64, notSelectValue: String? = nil) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ : ]
        if let notSelectValue = notSelectValue { result.updateValue(notSelectValue, forKey: 0) }
        dbHelper.getSections()
            .filter{ $0.section.device == device_id && $0.section.section != 0 }
            .forEach { result.updateValue($0.section.name, forKey: $0.section.section) }
        return Array(result).sorted { $0.key < $1.key }
    }
    func getAutoRelayScriptUi(device_id: Int64, notSelectValue: String) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ 0 : notSelectValue ]
        dbHelper.getZones()
            .filter { $0.zone.device == device_id && $0.zone.detector == 13 }
            .forEach { result.updateValue($0.zone.name, forKey: $0.zone.zone) }
        return Array(result).sorted { $0.key < $1.key }
    }
    
    // MARK: - Site
    
    func getDBSites(completed: @escaping (([Sites]) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let result = self.dbHelper.getSites()
            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getSiteEntity(site_id: Int64, completed: @escaping ((DSiteEntity?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            guard let site = self.dbHelper.getSite(site_id: site_id) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }

            let affects = self.dbHelper.getAffects(site_id: site.id)
            let armStatus = self.dbHelper.getArmStatus(site_id: site.id)
            let result = DataEntityMapper.dSite(site, .insert, affects, armStatus)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDeviceEntity(device_id: Int64, completed: @escaping ((DDeviceEntity?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            guard let di = self.dbHelper.getDevice(device_id: device_id) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }

            let affects = self.dbHelper.getAffects(device_id: device_id, section_id: 0, zone_id: 0)
                .join(self.dbHelper.getAffects(device_id: device_id, section_id: 0, zone_id: 100))
            let armStatus = self.dbHelper.getArmStatus(device_id: device_id)
            let result = DataEntityMapper.dDevice(di, .insert, affects, armStatus)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getSectionEntity(l_section_id: Int64, completed: @escaping ((DSectionEntity?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let ids = Sections.getIds(l_section_id)
            guard let si = self.dbHelper.getSection(device_id: ids.device_id, section_id: ids.section_id) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }

            let affects = self.dbHelper.getAffects(device_id: ids.device_id, section_id: ids.section_id)
            let result = DataEntityMapper.dSection(si, .insert, affects)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getSectionGroupEntity(id: Int64, completed: @escaping (((sectionGroup: SectionGroups, armStatus: DSiteEntity.ArmStatus, affects: [DAffectEntity])?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            guard var sectionGroup = self.dbHelper.getSectionGroup(id: id) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }
            
            let sectionIds = self.dbHelper.getSectionIds(sectionGroupId: id)
            var sections = [Sections]()
            var affects = [DAffectEntity]()
            var siteNames = ""
            for sectionId in sectionIds {
                let ids = Sections.getIds(sectionId)
                if let section = self.dbHelper.get(device_id: ids.device_id, section_id: ids.section_id) {
                    sections.append(section)
                    let sectionAffects = self.dbHelper.getAffects(device_id: section.device, section_id: section.section)
                    affects.append(contentsOf: sectionAffects)
                    if siteNames.isEmpty { siteNames = self.getNameSites(section.device) }
                }
            }
            let groupArmStatus: DSiteEntity.ArmStatus
            if sections.allSatisfy({ $0.arm > 0 }) {
                groupArmStatus = .armed
            } else if sections.allSatisfy({ $0.arm == 0 }) {
                groupArmStatus = .disarmed
            } else {
                groupArmStatus = .notFullArmed
            }

            sectionGroup.siteName = siteNames
            let result = (sectionGroup: sectionGroup, armStatus: groupArmStatus, affects: affects)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getSectionGroupSections(id: Int64, completed: @escaping (([Sections]) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let sectionIds = self.dbHelper.getSectionIds(sectionGroupId: id)
            var sections = [Sections]()
            for sectionId in sectionIds {
                let ids = Sections.getIds(sectionId)
                if let section = self.dbHelper.get(device_id: ids.device_id, section_id: ids.section_id) {
                    sections.append(section)
                }
            }

            let result = sections

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDashboardRelay(id: String, completed: @escaping (((relay: DZoneWithSitesDeviceSectionInfo, hasScripts: Bool, affects: [DAffectEntity])?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let ids = Zones.getIds(id: id)

            guard let relay = self.dbHelper.getZone(device_id: ids.device_id, section_id: ids.section_id, zone_id: ids.zone_id) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }
            
            let affects = self.dbHelper.getAffects(device_id: relay.zone.device, section_id: relay.zone.section, zone_id: relay.zone.zone)
            let hasScripts = self.dbHelper.getScript(device_id: relay.zone.device, zone_id: relay.zone.zone) != nil
            let result = (relay: relay, hasScripts: hasScripts, affects: affects)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDahsboardZone(id: String, completed: @escaping (((zone: DZoneWithSitesDeviceSectionInfo, section: Sections, affects: [DAffectEntity])?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let ids = Zones.getIds(id: id)
            guard let zone = self.dbHelper.getZone(device_id: ids.device_id, section_id: ids.section_id, zone_id: ids.zone_id),
                  let section = self.dbHelper.get(device_id: zone.zone.device, section_id: zone.zone.section) else {
                DispatchQueue.main.async {
                    completed(nil)
                }
                return
            }
            
            let affects = self.dbHelper.getAffects(device_id: zone.zone.device, section_id: zone.zone.section, zone_id: zone.zone.zone)
            let result = (zone: zone, section: section, affects: affects)

            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDahsboardCamera(id: Int64, completed: @escaping ((Camera?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            
        }
    }

    func getDBSitesSingle() -> Single<[Sites]> {
        let single = Single<[Sites]>.create { event -> Disposable in
            let result = self.dbHelper.getSites()
            event(SingleEvent.success(result))
            return Disposables.create { }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    // MARK: - Device
    
    func getDBDevice(device_id: Int64) -> Single<DDeviceWithSitesInfo> {
        let single = Single<DDeviceWithSitesInfo>.create { event -> Disposable in
            if let result = self.dbHelper.getDevice(device_id: device_id) {
                event(SingleEvent.success(result))
            } else {
                event(SingleEvent.error(NSError(domain: "Empty DDeviceWithSitesInfo", code: 0)))
            }
            return Disposables.create { }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func getOnlyHubDBDeviceIds(completed: @escaping (([Int64]) -> Void)){
        ThreadUtil.shared.backOpetarion.async {
            let result = self.dbHelper.getDeviceIds(site_id: nil, onlyHubId: true)
            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    // MARK: - Events
    
    func getDBEvents(startTime: Int64, endTime: Int64, _class: Int64?, siteId: Int64?, completed: @escaping (([DEventEntity]) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let result = self.dbHelper.getDEvents(startTime: startTime, endTime: endTime, _class: _class, siteId: siteId)
            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDBEvent(id: Int64, completed: @escaping ((DEventEntity?) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let result = self.dbHelper.getDEvent(id: id)
            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
    
    func getDBEvents(deviceId: Int64, zoneId: Int64, _class: Int64?, completed: @escaping (([DEventEntity]) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let result = self.dbHelper.getDEvents(deviceId: deviceId, zoneId: zoneId, _class: _class)
            DispatchQueue.main.async {
                completed(result)
            }
        }
    }
	
		// MARK: - AutoArm
	
		func getAutoArm (site: Int64) -> DAutoArmEntity {
			let ar = dbHelper.getAutoArm(id: -site)
			
			if let ar = ar {
				return DAutoArmEntity (id: 1, site: site, device: 0, section: 0, runAt: ar.runAt, active: ar.active, daysOfWeek: ar.daysOfWeek, hour: ar.hour, minute: ar.minute, timezone: ar.timezone, lastExecuted: ar.lastExecuted)
			}
			
			return DAutoArmEntity (id: 0, site: site, device: 0, section: 0, runAt: 0, active: 0, daysOfWeek: 0, hour: 0, minute: 0, timezone: 0, lastExecuted: 0)
		}
	
		func getAutoArm (device: Int64, section: Int64) -> DAutoArmEntity {
			let ar = dbHelper.getAutoArm(id: device * 100 + section)
			
			if let ar = ar {
				return DAutoArmEntity (id: 1, site: 0, device: device, section: section, runAt: ar.runAt, active: ar.active, daysOfWeek: ar.daysOfWeek, hour: ar.hour, minute: ar.minute, timezone: ar.timezone, lastExecuted: ar.lastExecuted)
			}
			
			return DAutoArmEntity (id: 0, site: 0, device: device, section: section, runAt: 0, active: 0, daysOfWeek: 0, hour: 0, minute: 0, timezone: 0, lastExecuted: 0)
		}
    
		func updateAutoArm(autoArm: DAutoArmEntity) {
			var id = autoArm.site
			if id == 0 { id = autoArm.device * 100 + autoArm.section } else { id = -id }
			
			if autoArm.id < 1 {
				dbHelper.removeAutoArm(id: id)
			} else {
				dbHelper.add(autoArm: AutoArm(id: id, runAt: autoArm.runAt, active: autoArm.active, daysOfWeek: autoArm.daysOfWeek, hour: autoArm.hour, minute: autoArm.minute, timezone: autoArm.timezone, lastExecuted: autoArm.lastExecuted))
			}
		}
	
		func removeAutoArm (site: Int64) {
			dbHelper.removeAutoArm(id: -site)
		}
	
		func removeAutoArm (device: Int64, section: Int64) {
			dbHelper.removeAutoArm(id: device * 100 + section)
		}
}
