//
//  CastHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 20.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import RxSwift
import SQLite

class CastHelper {
    public static func site(_ obj: HubSite, time: Int64, domainId: Int64) -> Sites{
			return Sites(id: obj.id, name: obj.name, domain: obj.domain, delegated: obj.delegated == 0 ? 0 : domainId == obj.domain ? 2 : 1, time: time, autoarmId: obj.autoarmId)
    }
    
    public static func device(_ obj: HubDevice, time: Int64) -> Devices{
        return Devices(id: obj.id, domain: obj.domain, account: obj.account, cluster_id: obj.cluster_id, configVersion: obj.configVersion, time: time)
    }
    
    public static func deviceSection(_ obj: HubSite, _ devObj: HubDevice, _ secObj: HubSection, time: Int64) -> DeviceSection {
        return DeviceSection(site: obj.id, device: devObj.id, section: secObj.section, parted: devObj.parted, time: time)
    }
  
    public static func section(_ devObj: HubDevice, _ secObj: HubSection, time: Int64) -> Sections{
			return Sections(device: devObj.id, section: secObj.section, name: secObj.name, detector: secObj.detector, arm: secObj.arm, time: time, autoarmId: secObj.autoarmId)
    }
    public static func section(_ obj: HubSectionZone, time: Int64) -> Sections{
			return Sections(device: obj.device, section: obj.section, name: obj.name, detector: obj.detector, arm: obj.arm, time: time, autoarmId: obj.autoarmId)
    }
    
    public static func zone(_ devObj: HubDevice, _ secObj: HubSection, _ zObj: HubZone, time: Int64) -> Zones{
				return Zones(device: devObj.id, section: secObj.section, zone: zObj.zone, name: zObj.name, delay: zObj.delay, detector: zObj.detector, physic: zObj.physic, uidType: zObj.uid_type, time: time, bypass: zObj.bypass)
    }
    public static func zone(_ obj: HubSectionZone, time: Int64) -> Zones{
				return Zones(device: obj.device, section: obj.section, zone: obj.zone, name: obj.name, delay: obj.delay, detector: obj.detector, physic: obj.physic, uidType: obj.uid_type, time: time, bypass: obj.bypass)
    }
    
    public static func event(_ o: HubEvent, icon: String, iconBig: String, affect_desc: String) -> Events {
        return Events(id: o.id, _class: o.classField, reason: o.reason, detector: o.detector, active: o.active, affect: o.affect, device: o.device, section: o.section, zone: o.zone, time: o.time, jdata: o.jdata, icon: icon, iconBig: iconBig, affect_desc: affect_desc, dropped: 0)
    }
    
    public static func script(_ obj: HubScript, device: Int64, time: Int64) -> Script{
        return Script(id: obj.id, device: device, name: obj.name, program: obj.program, compile_program: obj.compile_program, bind: obj.bind, enabled: obj.enabled, uid: obj.uid, params: obj.params, extra: obj.extra, time: time)
    }
    
    /////////////////////////////////////////////////////////////////////
    
    public static func deviceUser( _ devObj: HubDevice, _ userObj: HubUser, time: Int64) -> DeviceUsers{
        return DeviceUsers(id: DeviceUsers.getId(devObj.id, userObj.user_index), device: devObj.id, name: userObj.name, comment: userObj.comment, phone: userObj.phone, userIndex: userObj.user_index, time: time)
    }
    
    public static func userSection( _ devObj: HubDevice, _ userObj: HubUser, _ userSection: Int64, time: Int64) -> UserSection{
        return UserSection(id: UserSection.getId(devObj.id, userObj.user_index, userSection),
                           userId: DeviceUsers.getId(devObj.id, userObj.user_index),
                           sectionId: Sections.getId(devObj.id, userSection), time: time)
    }
    
    public static func operators(obj: HubOperator, time: Int64) -> Operators{
        return Operators(id: obj.id, domain: obj.domain, name: obj.name, active: obj.active, contacts: obj.contacts, login: obj.login, org: obj.org, roles: obj.roles, time: time)
    }
    
    public static func events(obj: Array<Optional<Binding>>) -> Events{
        return Events(id:       obj[0] as! Int64,
                      _class:   obj[1] as! Int64,
                      reason:   obj[2] as! Int64,
                      detector: obj[3] as! Int64,
                      active:   obj[4] as! Int64,
                      affect:   obj[5] as! Int64,
                      device:   obj[6] as! Int64,
                      section:  obj[7] as! Int64,
                      zone:     obj[8] as! Int64,
                      time:     obj[9] as! Int64,
                      jdata:    obj[10] as! String,
                      icon:     obj[11] as! String,
                      iconBig:  obj[12] as! String,
                      affect_desc: obj[13] as! String,
                      dropped: obj[14] as! Int64)
    }
    
    public static func site(obj: Array<Optional<Binding>>) -> Sites{
        return Sites(id: obj[0] as! Int64, name: obj[1] as! String, domain: obj[2] as! Int64, delegated: obj[3] as! Int64, time: obj[4] as! Int64, autoarmId: obj[5] as! Int64)
    }

    public static func deviceUser( _ userObj: HubUser, time: Int64) -> DeviceUsers{
        return DeviceUsers(id: DeviceUsers.getId(userObj.device_id, userObj.user_index), device: userObj.device_id, name: userObj.name, comment: userObj.comment, phone: userObj.phone, userIndex: userObj.user_index, time: time)
    }
    
    public static func userSection( _ userObj: HubUser, userSection: Int64, time: Int64) -> UserSection{
        return UserSection(id: UserSection.getId(userObj.device_id, userObj.user_index, userSection),
                           userId: DeviceUsers.getId(userObj.device_id, userObj.user_index),
                           sectionId: Sections.getId(userObj.device_id, userSection), time: time)
    }
    
    public static func castUser( obj: HubUser, time : Int64) -> Observable<SQLCommand>{
        return Observable.create{ observer in
            observer.onNext(CastHelper.deviceUser( obj, time: time))
            obj.sections.forEach({ (userSection) in observer.onNext(CastHelper.userSection( obj, userSection: userSection,time: time)) })
            observer.onCompleted();return Disposables.create{}
        }
    }
}
