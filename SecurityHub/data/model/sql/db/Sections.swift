//
//  Sections.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Sections: SQLCommand {
    static let id = Expression<Int64>("id")
    static let device = Expression<Int64>("device")
    static let section = Expression<Int64>("section")
    static let name = Expression<String>("name")
    static let detector = Expression<Int64>("detector")
    static let arm = Expression<Int64>("arm")
    static let time = Expression<Int64>("time")
    static let lname = Expression<String>("lname")
		static let autoarmId = Expression<Int64>("autoarmId")
    
    var id: Int64
    var device: Int64
    var section: Int64
    var name: String
    var detector: Int64
    var arm: Int64
    var time: Int64
		var autoarmId: Int64
    
    init(device: Int64, section: Int64, name: String, detector: Int64, arm: Int64, time: Int64, autoarmId: Int64) {
        self.id = Sections.getId(device, section)
        self.name = name
        self.device = device
        self.section = section
        self.detector = detector
        self.arm = arm
        self.time = time
				self.autoarmId = autoarmId
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(Sections.id <- id, Sections.device <- device, Sections.section <- section, Sections.name <- name, Sections.detector <- detector, Sections.arm <- arm, Sections.time <- time, Sections.lname <- name.lowercased(), Sections.autoarmId <- autoarmId))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sections.id == id).update(Sections.device <- device, Sections.section <- section, Sections.name <- name, Sections.detector <- detector, Sections.arm <- arm, Sections.time <- time, Sections.lname <- name.lowercased(), Sections.autoarmId <- autoarmId))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sections.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(Sections.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Sections.id == id)) else { return .needInsert }
        if row[Sections.name] == name && row[Sections.detector] == detector && row[Sections.arm] == arm && row[Sections.autoarmId] == autoarmId { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sections.id == id).update(Sections.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Sections(device: row[device], section: row[section], name: row[name], detector: row[detector], arm: row[arm], time: row[time], autoarmId: row[autoarmId]) as! T
    }
    
    static let all = "Sections.id, Sections.device, Sections.section, Sections.name, Sections.detector, Sections.arm, Sections.time, Sections.autoarmId"
    
    static func tableName() -> String{ return "Sections"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(device)
            t.column(section)
            t.column(name)
            t.column(detector)
            t.column(arm)
            t.column(time)
            t.column(lname)
						t.column(autoarmId)
        })
        return table
    }
    
    static func getId(_ device: Int64, _ section: Int64) -> Int64{ return device * 100 + section }
    
    static func getIds(_ id: Int64) -> (device_id: Int64, section_id: Int64) { return (device_id: Int64(id / 100), section_id: Int64(id % 100))}
}

