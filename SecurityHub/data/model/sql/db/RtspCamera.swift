//
//  RTSP.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import SQLite

struct RtspCamera: SQLCommand {
    static let id = Expression<Int64>("id")
    static let nameId = Expression<String>("nameId")
    static let urlId = Expression<String>("urlId")
    
    public var id: Int64
    public var name: String
    public var url: String
    
    init(id: Int64 = 0, name: String = "", url: String = "") {
        self.id = id
        self.name = name
        self.url = url
    }

    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            RtspCamera.nameId <- name,
            RtspCamera.urlId <- url
        ))
    }
    
    mutating func insertWithResult(_ db: Connection, table: Table) -> Int64? {
        return try? db.run(table.insert(RtspCamera.nameId <- name, RtspCamera.urlId <- url))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(RtspCamera.id == id).update(
            RtspCamera.nameId <- name,
            RtspCamera.urlId <- url
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(RtspCamera.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(RtspCamera.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return RtspCamera(id: row[id], name: row[nameId], url: row[urlId]) as! T
    }
    
    static func tableName() -> String{ return "RtspCamera"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: .autoincrement)
            t.column(nameId)
            t.column(urlId)
        })
        return table
    }
}

