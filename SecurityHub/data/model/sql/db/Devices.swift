//
//  Devices.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Devices: SQLCommand {
    static let id = Expression<Int64>("id")
    static let name = Expression<String>("name")
    static let domain = Expression<Int64>("domain")
    static let account = Expression<Int64>("account")
    static let cluster_id = Expression<Int64>("cluster_id")
    static let configVersion = Expression<Int64>("configVersion")
    static let time = Expression<Int64>("time")
    static let lname = Expression<String>("lname")

    var id: Int64
    var name: String
    var domain: Int64
    var account: Int64
    var cluster_id: Int64
    var configVersion: Int64
    var time: Int64
    
    init(id: Int64, domain: Int64, account: Int64, cluster_id: Int64, configVersion: Int64, time: Int64) {
        self.id = id
        self.name = HubConst.getDeviceTypeInfo(configVersion, cluster: cluster_id).name + " S/N:\(account)"
        self.domain = domain
        self.account = account
        self.cluster_id = cluster_id
        self.configVersion = configVersion
        self.time = time
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(Devices.id <- id, Devices.name <- name, Devices.domain <- domain, Devices.account <- account, Devices.cluster_id <- cluster_id, Devices.configVersion <- configVersion, Devices.time <- time, Devices.lname <- name.lowercased()))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Devices.id == id).update(Devices.name <- name, Devices.domain <- domain, Devices.account <- account, Devices.configVersion <- configVersion, Devices.cluster_id <- cluster_id, Devices.time <- time, Devices.lname <- name.lowercased()))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Devices.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool {
        if let _ = try? db.pluck(table.filter(Devices.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Devices.id == id)) else { return .needInsert }
        if row[Devices.domain] == domain && row[Devices.account] == account && row[Devices.cluster_id] == cluster_id && row[Devices.configVersion] == configVersion { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Devices.id == id).update(Devices.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Devices(id: row[id], domain: row[domain], account: row[account], cluster_id: row[cluster_id], configVersion: row[configVersion], time: row[time]) as! T
    }
    
    static let all = "Devices.id, Devices.name, Devices.domain, Devices.account, Devices.cluster_id, Devices.configVersion, Devices.time"
    
    static func tableName() -> String { return "Devices" }
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(name)
            t.column(domain)
            t.column(account)
            t.column(cluster_id)
            t.column(configVersion)
            t.column(time)
            t.column(lname)
        })
        return table
    }
}
