//
//  Events.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Events: SQLCommand {
    static let id = Expression<Int64>("id")
    static let _class = Expression<Int64>("_class")
    static let reason = Expression<Int64>("reason")
    static let detector = Expression<Int64>("detector")
    static let active = Expression<Int64>("active")
    static let affect = Expression<Int64>("affect")
    static let device = Expression<Int64>("device")
    static let section = Expression<Int64>("section")
    static let zone = Expression<Int64>("zone")
    static let time = Expression<Int64>("time")
    static let jdata = Expression<String>("jdata")
    static let icon = Expression<String>("icon")
    static let iconBig = Expression<String>("iconBig")
    static let affect_desc = Expression<String>("affect_desc")
    static let dropped = Expression<Int64>("dropped")
    
    var id: Int64
    var _class: Int64
    var reason: Int64
    var detector: Int64
    var active: Int64
    var affect: Int64
    var device: Int64
    var section: Int64
    var zone: Int64
    var time: Int64
    
    var jdata: String
    var icon: String
    var iconBig: String
    var affect_desc: String
    
    var dropped: Int64
    
    init(id: Int64, _class: Int64, reason: Int64, detector: Int64, active: Int64, affect: Int64, device: Int64, section: Int64, zone: Int64, time: Int64, jdata: String, icon: String, iconBig: String, affect_desc: String, dropped: Int64) {
        self.id = id
        self._class = _class
        self.reason = reason
        self.detector = detector
        self.affect = affect
        self.active = active
        self.device = device
        self.section = section
        self.zone = zone
        self.time = time
        self.jdata = jdata
        self.icon = icon
        self.iconBig = iconBig
        self.affect_desc = affect_desc
        self.dropped = dropped
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(Events.id <- id, Events._class <- _class, Events.reason <- reason, Events.detector <- detector, Events.active <- active, Events.affect <- affect, Events.device <- device, Events.section <- section, Events.zone <- zone, Events.time <- time, Events.jdata <- jdata, Events.icon <- icon, Events.iconBig <- iconBig, Events.affect_desc <- affect_desc, Events.dropped <- dropped))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Events.id == id).update(Events._class <- _class, Events.detector <- detector, Events.active <- active, Events.affect <- affect, Events.device <- device, Events.section <- section, Events.zone <- zone, Events.time <- time, Events.jdata <- jdata, Events.icon <- icon, Events.iconBig <- iconBig, Events.affect_desc <- affect_desc, Events.dropped <- dropped))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Events.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(Events.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let _ = try? db.pluck(table.filter(Events.id == id)) else { return .needInsert }
        return .noNeed
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Events(id: row[id], _class: row[_class], reason: row[reason], detector: row[detector], active: row[active], affect: row[affect], device: row[device], section: row[section], zone: row[zone], time: row[time], jdata: row[jdata], icon: row[icon], iconBig: row[iconBig], affect_desc: row[affect_desc], dropped: row[dropped]) as! T
    }
    
    static func tableName() -> String{ return "Events"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id)
            t.column(_class)
            t.column(reason)
            t.column(detector)
            t.column(active)
            t.column(affect)
            t.column(device)
            t.column(section)
            t.column(zone)
            t.column(time)
            t.column(jdata)
            t.column(icon)
            t.column(iconBig)
            t.column(affect_desc)
            t.column(dropped)
        })
        return table
    }
}

extension Events{
    func isMainAffect() -> Bool {
        return affect == 1 &&
            (_class < 5 ||
            (_class == 5 && (reason == 24 || reason == 27)) ||
            (_class == 6 && (reason == 3  || reason == 4 || reason == 7)) ||
            (_class == 7 && reason == 1))
    }
//    static func toIcons(_ events: [Events], armStatus: ARMSTATUS) -> Set<String>{
//        var result: Set<String> = []
//        events.filter({ (e) -> Bool in
//            return e._class < 5 || (e._class == 5 && (e.reason == 24 || e.reason == 27)) || (e._class == 6 && (e.reason == 3 || e.reason == 4 || e.reason == 7)) || (e._class == 7 && e.reason == 1)
//        }).forEach({ (event) in result.insert(event.icon) })
//        if armStatus == .notfull {
//            if events.contains(where: { $0._class == 0}) { result.insert("ic_partly_small")}
//            result.remove("ic_restriction_shield_grey_500_small_24")
//            result.remove("ic_security_checked_green_500_small_24")
//        }
//        if !events.contains(where: { $0._class == 0}) && armStatus == .arm { result.remove("ic_security_checked_green_500_small_24") }
//        if !events.contains(where: { $0._class == 0}) && armStatus == .disarm {result.remove("ic_restriction_shield_grey_500_small_24")}
//        result.remove("ic_alert_red")
//        return result
//    }
//    static func toEventElements(_ events: [(Events,String)]) -> [EventElement]{
//        var result: [EventElement] = []
//        events.forEach({ (e) in result.append(EventElement(icon: e.0.icon, location: e.1, desc: e.0.affect_desc)) })
//        return result
//    }
}
