//
//  IvVideo.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct IvVideo: SQLCommand {
    var id: Int64
    var camId: String
    var eventId: Int64
    var result: String
    var isError: Bool
    
    init(id: Int64 = 0, camId: String = "", eventId: Int64 = 0, result: String = "", isError: Bool = false) {
        self.id = id
        self.camId = camId
        self.eventId = eventId
        self.result = result
        self.isError = isError
    }
    
    static let id = Expression<Int64>("id")
    static let camId = Expression<String>("camId")
    static let eventId = Expression<Int64>("eventId")
    static let result = Expression<String>("result")
    static let isError = Expression<Bool>("isError")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            IvVideo.camId <- camId,
            IvVideo.eventId <- eventId,
            IvVideo.result <- result,
            IvVideo.isError <- isError
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(IvVideo.id == id).update(
            IvVideo.camId <- camId,
            IvVideo.eventId <- eventId,
            IvVideo.result <- result,
            IvVideo.isError <- isError
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(IvVideo.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(IvVideo.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return IvVideo(id: row[id], camId: row[camId], eventId: row[eventId], result: row[result], isError: row[isError]) as! T
    }
    
    static func tableName() -> String{ return "IvVideo"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: .autoincrement)
            t.column(camId)
            t.column(eventId)
            t.column(result)
            t.column(isError)
        })
        return table
    }
}
