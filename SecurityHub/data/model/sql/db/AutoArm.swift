//
//  AutoArm.swift
//  SecurityHub
//
//  Created by Stefan on 11.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import SQLite

struct AutoArm: SQLCommand {
		static let id = Expression<Int64>("id")
		static let runAt = Expression<Int64>("runAt")
		static let active = Expression<Int64>("active")
		static let daysOfWeek = Expression<Int64>("daysOfWeek")
		static let hour = Expression<Int64>("hour")
		static let minute = Expression<Int64>("minute")
		static let timezone = Expression<Int64>("timezone")
		static let lastExecuted = Expression<Int64>("lastExecuted")

		var id: Int64
		var runAt: Int64
		var active: Int64
		var daysOfWeek: Int64
		var hour: Int64
		var minute: Int64
		var timezone: Int64
		var lastExecuted: Int64
		
		init(id: Int64, runAt: Int64, active: Int64, daysOfWeek: Int64, hour: Int64, minute: Int64, timezone: Int64, lastExecuted: Int64) {
				self.id = id
				self.runAt = runAt
				self.active = active
				self.daysOfWeek = daysOfWeek
				self.hour = hour
				self.minute = minute
				self.timezone = timezone
				self.lastExecuted = lastExecuted
		}

		func insert(_ db: Connection, table: Table) {
				let _ = try? db.run(table.insert( AutoArm.id <- id, AutoArm.runAt <- runAt, AutoArm.active <- active, AutoArm.daysOfWeek <- daysOfWeek, AutoArm.hour <- hour, AutoArm.minute <- minute, AutoArm.timezone <- timezone, AutoArm.lastExecuted <- lastExecuted ))
		}
		
		func update(_ db: Connection, table: Table) {
				let _ = try? db.run(table.filter(AutoArm.id == id).update( AutoArm.runAt <- runAt, AutoArm.active <- active, AutoArm.daysOfWeek <- daysOfWeek, AutoArm.hour <- hour, AutoArm.minute <- minute, AutoArm.timezone <- timezone, AutoArm.lastExecuted <- lastExecuted))
		}
		
		func delete(_ db: Connection, table: Table) {
				let _ = try? db.run(table.filter(AutoArm.id == id).delete())
		}
		
		func isWasInDb(_ db: Connection, table: Table) -> Bool {
				if let _ = try? db.pluck(table.filter(AutoArm.id == id)) { return true }
				return false
		}
		
		func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
				guard let row = try? db.pluck(table.filter(AutoArm.id == id)) else { return .needInsert }
				if row[AutoArm.runAt] == runAt && row[AutoArm.active] == active && row[AutoArm.daysOfWeek] == daysOfWeek && row[AutoArm.hour] == hour && row[AutoArm.minute] == minute && row[AutoArm.timezone] == timezone && row[AutoArm.lastExecuted] == lastExecuted { return .noNeed }
				return .needUpdate
		}
		
		func updateLastExecuted(_ db: Connection, table: Table) {
				let _ = try? db.run(table.filter(AutoArm.id == id).update(AutoArm.lastExecuted <- lastExecuted))
		}
		
		static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
				return AutoArm(id: row[id], runAt: row[runAt], active: row[active], daysOfWeek: row[daysOfWeek], hour: row[hour], minute: row[minute], timezone: row[timezone], lastExecuted: row[lastExecuted]) as! T
		}
		
		static let all = "AutoArm.id, AutoArm.runAt, AutoArm.active, AutoArm.daysOfWeek, AutoArm.hour, AutoArm.minute, AutoArm.timezone, AutoArm.lastExecuted"
		
		static func tableName() -> String{ return "AutoArm"}
		
		static func createTable(_ db: Connection) -> Table {
				let table = Table(tableName())
				_ = try? db.run(table.create(ifNotExists: true) { t in
						t.column(id, primaryKey: true)
						t.column(runAt)
						t.column(active)
						t.column(daysOfWeek)
						t.column(hour)
						t.column(minute)
						t.column(timezone)
						t.column(lastExecuted)
				})
				return table
		}
}

