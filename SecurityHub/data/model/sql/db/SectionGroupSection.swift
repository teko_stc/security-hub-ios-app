//
//  SectionGroupSection.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import SQLite

struct SectionGroupSection: SQLCommand {
    
    static let id = Expression<Int64>("id")
    static let sectionGroupId = Expression<Int64>("section_group_id")
    static let sectionId = Expression<Int64>("section_id")
    
    var id: Int64
    var sectionGroupId: Int64
    var sectionId: Int64
    
    init(sectionGroupId: Int64, sectionId: Int64) {
        self.id = SectionGroupSection.getId(sectionGroupId, sectionId)
        self.sectionGroupId = sectionGroupId
        self.sectionId = sectionId
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(SectionGroupSection.id <- id, SectionGroupSection.sectionGroupId <- sectionGroupId, SectionGroupSection.sectionId <- sectionId))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(SectionGroups.id == id).update(SectionGroupSection.sectionGroupId <- sectionGroupId, SectionGroupSection.sectionId <- sectionId))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(SectionGroupSection.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(SectionGroupSection.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(SectionGroupSection.id == id)) else { return .needInsert }
        if row[SectionGroupSection.sectionGroupId] == sectionGroupId && row[SectionGroupSection.sectionId] == sectionId { return .noNeed }
        return .needUpdate
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return SectionGroupSection(sectionGroupId: row[sectionGroupId], sectionId: row[sectionId]) as! T
    }
    
    static func tableName() -> String{ return "SectionGroupSection"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(sectionGroupId)
            t.column(sectionId)
        })
        return table
    }
    
    static func getId(_ sectionGroupId: Int64, _ sectionId: Int64) -> Int64 {
        return (sectionGroupId * 100) + sectionId
    }
}
