//
//  Settings.swift
//  SecurityHub test
//
//  Created by Timerlan on 13.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import SQLite

class DB {
    private let VERSION: Int64 = 78

    let db: Connection
    
    let sites: Table
    let deviceUser: Table
    let userSection: Table
    let deviceSection: Table
    let device: Table
    let sections: Table
    let sectionGroups: Table
    let sectionGroupSections: Table
    let zones: Table
    let operators: Table
    let events: Table
    let ivVideo: Table
    let camSet: Table
    let references: Table
    let scripts: Table
    let rtspCamera: Table
		let autoArm: Table

    let pathDb: String
    
    init(login: String) {
			let path : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first ?? NSString("~/Documents").expandingTildeInPath as String
				if FileManager().fileExists(atPath: path) == false {
					try! FileManager().createDirectory(atPath: path, withIntermediateDirectories: true) }
        let name = login.count == 0 ? "q" : login
        pathDb = "\(path)/\(name.lowercased()).sqlite3"

        db = try! Connection(pathDb)
        if db.userVersion != VERSION {
            db.userVersion = VERSION
            try! db.run(Sites.createTable(db).drop(ifExists: true))
            try! db.run(UserSection.createTable(db).drop(ifExists: true))
            try! db.run(DeviceUsers.createTable(db).drop(ifExists: true))
            try! db.run(DeviceSection.createTable(db).drop(ifExists: true))
            try! db.run(Devices.createTable(db).drop(ifExists: true))
            try! db.run(Sections.createTable(db).drop(ifExists: true))
            try! db.run(SectionGroups.createTable(db).drop(ifExists: true))
            try! db.run(SectionGroupSection.createTable(db).drop(ifExists: true))
            try! db.run(Zones.createTable(db).drop(ifExists: true))
            try! db.run(Operators.createTable(db).drop(ifExists: true))
            try! db.run(Events.createTable(db).drop(ifExists: true))
            try! db.run(IvVideo.createTable(db).drop(ifExists: true))
            try! db.run(CamSet.createTable(db).drop(ifExists: true))
            try! db.run(References.createTable(db).drop(ifExists: true))
            try! db.run(Script.createTable(db).drop(ifExists: true))
            try! db.run(RtspCamera.createTable(db).drop(ifExists: true))
						try! db.run(AutoArm.createTable(db).drop(ifExists: true))
        }
        sites = Sites.createTable(db)
        userSection = UserSection.createTable(db)
        deviceSection = DeviceSection.createTable(db)
        deviceUser = DeviceUsers.createTable(db)
        device = Devices.createTable(db)
        sections = Sections.createTable(db)
        sectionGroups = SectionGroups.createTable(db)
        sectionGroupSections = SectionGroupSection.createTable(db)
        zones = Zones.createTable(db)
        operators = Operators.createTable(db)
        events = Events.createTable(db)
        ivVideo = IvVideo.createTable(db)
        camSet = CamSet.createTable(db)
        references = References.createTable(db)
        scripts = Script.createTable(db)
        rtspCamera = RtspCamera.createTable(db)
				autoArm = AutoArm.createTable(db)
    }

    func connection() -> Connection {
        try! Connection(pathDb)
    }
}















