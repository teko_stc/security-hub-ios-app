//
//  SectionGroups.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import SQLite

struct SectionGroups: SQLCommand {
    static let id = Expression<Int64>("id")
    static let name = Expression<String>("name")
    
    var id: Int64
    var name: String
    var siteName: String = ""
    
    init(id: Int64, name: String) {
        self.id = id
        self.name = name
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(SectionGroups.id <- id, SectionGroups.name <- name))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(SectionGroups.id == id).update(SectionGroups.name <- name))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(SectionGroups.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(SectionGroups.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(SectionGroups.id == id)) else { return .needInsert }
        if row[SectionGroups.name] == name { return .noNeed }
        return .needUpdate
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return SectionGroups(id: row[id], name: row[name]) as! T
    }
    
    static func tableName() -> String{ return "SectionGroups"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(name)
        })
        return table
    }
}
