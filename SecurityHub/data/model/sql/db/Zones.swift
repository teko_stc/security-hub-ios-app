//
//  Zones.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Zones {
    static let id = Expression<String>("id")
    static let lid = Expression<Int64>("lid")
    static let section = Expression<Int64>("section")
    static let device = Expression<Int64>("device")
    static let zone = Expression<Int64>("zone")
    static let name = Expression<String>("name")
    static let delay = Expression<Int64>("delay")
    static let detector = Expression<Int64>("detector")
    static let physic = Expression<Int64>("physic")
    static let time = Expression<Int64>("time")
    static let uidType = Expression<Int64>("uidType")
    static let lname = Expression<String>("lname")
		static let bypass = Expression<Int64>("bypass")
    
    var id: String
    var lid: Int64
    var device: Int64
    var section: Int64
    var zone: Int64
    var name: String
    var delay: Int64
    var detector: Int64
    var physic: Int64
    var time: Int64
    var uidType: Int64
		var bypass: Int64
    
    init(device: Int64, section: Int64, zone: Int64, name: String, delay: Int64, detector: Int64, physic: Int64, uidType: Int64, time: Int64, bypass: Int64) {
        self.id = Zones.getId(device, section, zone)
        self.lid = zone == 100 ? -1 : zone
        self.device = device
        self.section = section
        self.zone = zone
        self.name = name
        self.delay = delay
        self.detector = detector
        self.physic = physic
        self.time = time
        self.uidType = uidType
				self.bypass = bypass
    }
    
    func insert(_ db: Connection, table: Table) {
			let _ = try? db.run(table.insert(Zones.id <- id, Zones.device <- device, Zones.section <- section, Zones.zone <- zone, Zones.name <- name, Zones.delay <- delay, Zones.detector <- detector, Zones.physic <- physic, Zones.time <- time, Zones.uidType <- uidType, Zones.lname <- name.lowercased(), Zones.lid <- lid, Zones.bypass <- bypass))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).update(Zones.device <- device, Zones.section <- section, Zones.zone <- zone, Zones.name <- name, Zones.delay <- delay, Zones.detector <- detector, Zones.physic <- physic, Zones.time <- time, Zones.uidType <- uidType, Zones.lname <- name.lowercased(), Zones.lid <- lid, Zones.bypass <- bypass))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(Zones.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Zones.id == id)) else { return .needInsert }
        if row[Zones.name] == name && row[Zones.delay] == delay && row[Zones.detector] == detector && row[Zones.physic] == physic && row[Zones.uidType] == uidType && row[Zones.bypass] == bypass { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).update(Zones.time <- time))
    }
    
    static func fromRow(_ row: Row) -> Zones {
        return Zones(device: row[device], section: row[section], zone: row[zone], name: row[name], delay: row[delay], detector: row[detector], physic: row[physic], uidType: row[uidType], time: row[time], bypass: row[bypass])
    }
		
    static let all = "Zones.id, Zones.device, Zones.section, Zones.zone, Zones.name, Zones.delay, Zones.detector, Zones.physic, Zones.uidType, Zones.time, Zones.bypass"
    
    static func tableName() -> String{ return "Zones"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(device)
            t.column(section)
            t.column(zone)
            t.column(name)
            t.column(delay)
            t.column(detector)
            t.column(physic)
            t.column(uidType)
            t.column(time)
            t.column(lname)
            t.column(lid)
						t.column(bypass)
        })
        return table
    }
    
    static func getId(_ device: Int64, _ section: Int64, _ zone: Int64) -> String { String(format: "%08X %08X %08X", device, section, zone) }
    
    static func getIds(id: String) -> (device_id: Int64, section_id: Int64, zone_id: Int64) {
        let arr = id.split(separator: " ")
        guard arr.count == 3 else {
            return (device_id: -1, section_id: -1, zone_id: -1)
        }

        return (device_id:  Int64(arr[0], radix: 16) ?? 0,
                section_id: Int64(arr[1], radix: 16) ?? 0,
                zone_id:    Int64(arr[2], radix: 16) ?? 0)
        
    }
    
    func getType() -> Int64 {
        guard uidType != 0 else { return 0 }
        return uidType >> 24
    }
    
    func getInputType() -> DInputTypeEntity? {
        let type = uidType & 0xFF
        return R.inputTypes.first { $0.id == type }
    }
    
    func getRelayInputType() -> DInputTypeEntity? {
        return R.inputTypes.first { $0.id == detector }
    }
    
    func getOutputType() -> DItemTypeEntity? {
        return R.outputTypes.first { $0.id == detector }
    }
}
