//
//  Operators.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Operators: SQLCommand {
    var id: Int64
    var domain: Int64
    var name: String
    var active: Int64
    var contacts: String
    var login: String
    var org: Int64
    var roles: Int64
    var time: Int64
    
    init(id: Int64 = 0, domain: Int64 = 0, name: String = "", active: Int64 = 0, contacts: String = "", login: String = "", org: Int64 = 0, roles: Int64 = 0, time: Int64 = 0) {
        self.id = id
        self.domain = domain
        self.name = name
        self.active = active
        self.contacts = contacts
        self.login = login
        self.org = org
        self.roles = roles
        self.time = time
    }
    
    static let id = Expression<Int64>("id")
    static let domain = Expression<Int64>("domain")
    static let name = Expression<String>("name")
    static let active = Expression<Int64>("active")
    static let contacts = Expression<String>("contacts")
    static let login = Expression<String>("login")
    static let org = Expression<Int64>("org")
    static let roles = Expression<Int64>("roles")
    static let time = Expression<Int64>("time")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Operators.id <- id,
            Operators.domain <- domain,
            Operators.name <- name,
            Operators.active <- active,
            Operators.contacts <- contacts,
            Operators.login <- login,
            Operators.org <- org,
            Operators.roles <- roles,
            Operators.time <- time
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Operators.id == id).update(
            Operators.domain <- domain,
            Operators.name <- name,
            Operators.active <- active,
            Operators.contacts <- contacts,
            Operators.login <- login,
            Operators.org <- org,
            Operators.roles <- roles,
            Operators.time <- time
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Operators.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Operators.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Operators(id: row[id], domain: row[domain], name: row[name], active: row[active], contacts: row[contacts], login: row[login], org: row[org], roles: row[roles], time: row[time]) as! T
    }
    
    static func tableName() -> String{ return "Operators"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(domain)
            t.column(name)
            t.column(active)
            t.column(contacts)
            t.column(login)
            t.column(org)
            t.column(roles)
            t.column(time)
        })
        return table
    }
}
