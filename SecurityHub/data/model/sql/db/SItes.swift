//
//  SItes.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Sites: SQLCommand {
    static let id = Expression<Int64>("id")
    static let name = Expression<String>("name")
    static let domain = Expression<Int64>("domain")
    static let delegated = Expression<Int64>("delegated")
    static let time = Expression<Int64>("time")
    static let lname = Expression<String>("lname")
		static let autoarmId = Expression<Int64>("autoarmId")

    var id: Int64
    var name: String
    var domain: Int64
    var delegated: Int64
    var time: Int64
		var autoarmId: Int64
    
    init(id: Int64, name: String, domain: Int64,  delegated: Int64, time: Int64, autoarmId: Int64) {
        self.id = id
        self.domain = domain
        self.name = name
        self.delegated = delegated
        self.time = time
				self.autoarmId = autoarmId
    }

    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert( Sites.id <- id, Sites.name <- name, Sites.domain <- domain, Sites.delegated <- delegated, Sites.time <- time, Sites.lname <- name.lowercased(), Sites.autoarmId <- autoarmId ))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sites.id == id).update( Sites.name <- name, Sites.domain <- domain, Sites.delegated <- delegated, Sites.time <- time, Sites.lname <- name.lowercased(), Sites.autoarmId <- autoarmId ))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sites.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool {
        if let _ = try? db.pluck(table.filter(Sites.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Sites.id == id)) else { return .needInsert }
        if row[Sites.name] == name && row[Sites.domain] == domain && row[Sites.delegated] == delegated && row[Sites.autoarmId] == autoarmId { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Sites.id == id).update(Sites.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Sites(id: row[id], name: row[name], domain: row[domain], delegated: row[delegated], time: row[time], autoarmId: row[autoarmId]) as! T
    }
    
    static let all = "Sites.id, Sites.name, Sites.domain, Sites.delegated, Sites.time, Sites.autoarmId"
    
    static func tableName() -> String{ return "Sites"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        _ = try? db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(name)
            t.column(domain)
            t.column(delegated)
            t.column(time)
            t.column(lname)
						t.column(autoarmId)
        })
        return table
    }
}
