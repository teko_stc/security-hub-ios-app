//
//  jClasses.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct jClasses: SQLCommand{
    var id: Int64 = 0
    var _class : Int64
    var reasons : Int64?
    var detectors : Int64?
    var statements : Int64?
    var name : String
    var icon : String
    var iconBig : String
    var affect_desc : String
    
    init( id : Int64? = nil, _class : Int64 = 0, reasons : Int64? = nil, detectors : Int64? = nil, statements : Int64? = nil,
          name : String = "", icon : String = "ic_action_info_grey", iconBig : String = "ic_action_info_grey", affect_desc : String = "" ){
        if id != nil { self.id = id! }
        self._class = _class
        self.reasons = reasons
        self.detectors = detectors
        self.statements = statements
        self.name = name
        self.icon = icon
        self.iconBig = iconBig
        self.affect_desc = affect_desc
    }
    
    static let id = Expression<Int64>("id")
    static let _class = Expression<Int64>("_class")
    static let reasons = Expression<Int64?>("reasons")
    static let detectors = Expression<Int64?>("detectors")
    static let statements = Expression<Int64?>("statements")
    static let name = Expression<String>("name")
    static let icon = Expression<String>("icon")
    static let iconBig = Expression<String>("iconBigv")
    static let affect_desc = Expression<String>("affect_desc")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            jClasses._class <- _class,
            jClasses.reasons <- reasons,
            jClasses.detectors <- detectors,
            jClasses.statements <- statements,
            jClasses.name <- name,
            jClasses.icon <- icon,
            jClasses.iconBig <- iconBig,
            jClasses.affect_desc <- affect_desc
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(jClasses._class == _class && jClasses.reasons == reasons && jClasses.detectors == detectors && jClasses.statements == statements).update(
            jClasses.name <- name,
            jClasses.icon <- icon,
            jClasses.iconBig <- iconBig,
            jClasses.affect_desc <- affect_desc
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(jClasses._class == _class && jClasses.reasons == reasons && jClasses.detectors == detectors && jClasses.statements == statements).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(jClasses._class == _class && jClasses.reasons == reasons && jClasses.detectors == detectors && jClasses.statements == statements)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return jClasses(id: row[id], _class: row[_class], reasons: row[reasons], detectors: row[detectors], statements: row[statements], name: row[name], icon: row[icon], iconBig: row[iconBig], affect_desc: row[affect_desc]) as! T
    }
    
    static func tableName() -> String{ return "jClasses"}
    
    static func createTable(_ db: Connection) -> Table{
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: .autoincrement)
            t.column(_class)
            t.column(reasons)
            t.column(detectors)
            t.column(statements)
            t.column(name)
            t.column(icon)
            t.column(iconBig)
            t.column(affect_desc)
        })
        return table
    }
}
