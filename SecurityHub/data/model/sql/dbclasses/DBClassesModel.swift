//
//  jsonModel.swift
//  SecurityHub test
//
//  Created by Timerlan on 19.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

class DBClasses {
    private let VERSION: Int64 = 20
    let db: Connection
    let jclasses: Table

    init() {
        let path: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        db = try! Connection("\(path)/settings_kurva.sqlite3")
        if db.userVersion != VERSION {
            db.userVersion = VERSION
            try! db.run(jClasses.createTable(db).drop(ifExists: true))
        }
        jclasses = jClasses.createTable(db)
    }
}
