//
//  PKCESecrets.swift
//  SecurityHub
//
//  Created by Stefan on 12.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import CommonCrypto
import Foundation

internal struct PKCESecrets {
    internal enum CodeChallengeMethod: String {
        case sha256
    }

    let codeVerifier: String
    let codeChallenge: String
    let codeChallengeMethod: CodeChallengeMethod
    let state = UUID().uuidString
}

internal protocol PKCESecretsGenerator {
    func generateSecrets() throws -> PKCESecrets
}

internal final class PKCESecretsSHA256Generator: PKCESecretsGenerator {
    internal enum Error: Swift.Error {
        case securityServicesError(OSStatus)
        case failedToGenerateChallenge
    }

    func generateSecrets() throws -> PKCESecrets {
        let octets = try self.generateRandomBytes(length: 32)
        let verifier = octets.base64URLEncoded()
        let challenge = try generateChallenge(for: verifier)
        return PKCESecrets(
            codeVerifier: verifier,
            codeChallenge: challenge,
            codeChallengeMethod: .sha256
        )
    }

    private func generateRandomBytes(length: Int) throws -> [UInt8] {
        var octets = [UInt8](repeating: 0, count: length)
        let status = SecRandomCopyBytes(kSecRandomDefault, octets.count, &octets)
        if status == errSecSuccess {
            return octets
        } else {
            throw Error.securityServicesError(status)
        }
    }

    private func generateChallenge(for verifier: String) throws -> String {
        let challenge = verifier
            .data(using: .ascii)?
            .sha256()
            .base64URLEncoded()
        if let challenge {
            return challenge
        }
        throw Error.failedToGenerateChallenge
    }
}

extension Sequence<UInt8> {
    fileprivate func base64URLEncoded() -> String {
        Data(self)
            .base64EncodedString() // Regular base64 encoder
            .replacingOccurrences(of: "=", with: "") // Remove any trailing '='s
            .replacingOccurrences(of: "+", with: "-") // 62nd char of encoding
            .replacingOccurrences(of: "/", with: "_") // 63rd char of encoding
            .trimmingCharacters(in: .whitespaces)
    }
}

extension Data {
    fileprivate func sha256() -> Self {
        var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        self.withUnsafeBytes {
            _ = CC_SHA256($0.baseAddress, CC_LONG(self.count), &hash)
        }
        return Data(hash)
    }
}
