//
//  HubIvideonToken.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import Gloss

//MARK: - IV_GET_TOKEN
public class HubIvideonToken: Glossy {
    public var access_token : String?
    public var domain : Int64 = 0
  
    //MARK: Default Initializer
    init()
    {
        access_token = nil
        domain = 0
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let access_token : String = "access_token" <~~ json {
            self.access_token = access_token
        }
        if let domain : Int64 = "domain" <~~ json {
            self.domain = domain
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "access_token" ~~> access_token,
            "domain" ~~> domain,
            ])
    }
}
