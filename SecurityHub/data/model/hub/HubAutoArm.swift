//
//  HubAutoArm.swift
//  SecurityHub
//
//  Created by Stefan on 12.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation
import Gloss

public class HubAutoArm: Glossy {
		public var runAt : Int64 = 0
		public var active : Int64 = 0
		public var daysOfWeek : Int64 = 0
		public var hour : Int64 = 0
		public var minute : Int64 = 0
		public var timezone : Int64 = 0
		public var lastExecuted : Int64 = 0
		
		//MARK: Default Initializer
		init() {}
		
		//MARK: Decodable
		public required init?(json: JSON){
				if let runAt : Int64 = "run_at" <~~ json { self.runAt = runAt }
				if let active : Int64 = "active" <~~ json { self.active = active }
				if let daysOfWeek : Int64 = "days_of_week" <~~ json { self.daysOfWeek = daysOfWeek }
				if let hour : Int64 = "hour" <~~ json { self.hour = hour }
				if let minute : Int64 = "minute" <~~ json { self.minute = minute }
				if let timezone : Int64 = "timezone" <~~ json { self.timezone = timezone }
				if let lastExecuted : Int64 = "last_executed" <~~ json { self.lastExecuted = lastExecuted }
		}
		
		//MARK: Encodable
		public func toJSON() -> JSON? {
				return jsonify([
						"run_at" ~~> runAt,
						"active" ~~> active,
						"days_of_week" ~~> daysOfWeek,
						"hour" ~~> hour,
						"minute" ~~> minute,
						"timezone" ~~> timezone,
						"last_executed" ~~> lastExecuted
						])
		}
}
