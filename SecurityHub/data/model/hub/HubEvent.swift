//
//  HubEvent.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubEvent
public class HubEvent: Glossy {
    public var id : Int64 = 0
    public var classField : Int64 = 0
    public var reason : Int64 = 0
    public var detector : Int64 = 0
    public var affect : Int64 = 0
    public var active : Int64 = 0
    
    public var site : Int64 = 0
    public var device : Int64 = 0
    public var section : Int64 = 0
    public var zone : Int64 = 0
    public var time : Int64 = 0
    
    public var jdata : String = ""
    
    public var channel : Int64 = 0
    public var isClosed : Int64 = 0
    public var localtime : Int64 = 0
    public var operatorId : Int64 = 0
    public var reviewId : Int64 = 0
    public var reviewState : Int64 = 0
    
   
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let active : Int64 = "active" <~~ json {
            self.active = active
        }
        if let affect : Int64 = "affect" <~~ json {
            self.affect = affect
        }
        if let channel : Int64 = "channel" <~~ json {
            self.channel = channel
        }
        if let classField : Int64 = "class" <~~ json {
            self.classField = classField
        }
        if let detector : Int64 = "detector" <~~ json {
            self.detector = detector
        }
        if let device : Int64 = "device" <~~ json {
            self.device = device
        }
        if let id : Int64 = "id" <~~ json {
            self.id = id
        }
        if let isClosed : Int64 = "is_closed" <~~ json {
            self.isClosed = isClosed
        }
        if let jdata : String = "jdata" <~~ json {
            self.jdata = jdata
        }
        if let localtime : Int64 = "localtime" <~~ json {
            self.localtime = localtime
        }
        if let operatorId : Int64 = "operator_id" <~~ json {
            self.operatorId = operatorId
        }
        if let reason : Int64 = "reason" <~~ json {
            self.reason = reason
        }
        if let reviewId : Int64 = "review_id" <~~ json {
            self.reviewId = reviewId
        }
        if let reviewState : Int64 = "review_state" <~~ json {
            self.reviewState = reviewState
        }
        if let section : Int64 = "section" <~~ json {
            self.section = section
        }
        if let site : Int64 = "site" <~~ json {
            self.site = site
        }
        if let time : Int64 = "time" <~~ json {
            self.time = time
        }
        if let zone : Int64  = "zone" <~~ json {
            self.zone = zone
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "active" ~~> active,
            "affect" ~~> affect,
            "channel" ~~> channel,
            "class" ~~> classField,
            "detector" ~~> detector,
            "device" ~~> device,
            "id" ~~> id,
            "is_closed" ~~> isClosed,
            "jdata" ~~> jdata,
            "localtime" ~~> localtime,
            "operator_id" ~~> operatorId,
            "reason" ~~> reason,
            "review_id" ~~> reviewId,
            "review_state" ~~> reviewState,
            "section" ~~> section,
            "site" ~~> site,
            "time" ~~> time,
            "zone" ~~> zone,
            ])
    }
    
}


