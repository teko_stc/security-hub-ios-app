//
//  HubOptions.swift
//  SecurityHub
//
//  Copyright © 2023 TEKO. All rights reserved.
//

import Gloss

// MARK: - HubOperators

public class HubOptions: Glossy {

    public var modules : [String] = []
    public var serverInfo : [HubOptionsServerInfo] = []
    
    public required init?(json: JSON){
        if let modules: [String] = "modules" <~~ json {
            self.modules = modules
        }
        if let serverInfo: [HubOptionsServerInfo] = "server_info" <~~ json {
            self.serverInfo = serverInfo
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "modules" ~~> modules,
            "server_info" ~~> serverInfo,
        ])
    }
}

public class HubOptionsServerInfo: Glossy {

    public var name: String = ""
    public var value: String = ""
    
    public required init?(json: JSON){
        if let name: String = "name" <~~ json {
            self.name = name
        }
        if let value: String = "value" <~~ json {
            self.value = value
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> name,
            "value" ~~> value,
        ])
    }
}
