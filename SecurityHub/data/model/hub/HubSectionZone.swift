//
//  HubSectionZone.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubSectionZone
public class HubSectionZone: Glossy {
    public var device : Int64 = 0
    public var section : Int64 = 0
    public var zone : Int64 = 0
    public var site : Int64?
    public var name : String = ""
    public var detector : Int64 = 0
    public var physic : Int64 = 0
    public var scheduleId : Int64 = 0
    public var tracert : Int64 = 0
    public var flags : Int64 = 0
    public var delay : Int64 = 0
		public var bypass : Int64 = 0
    public var arm : Int64 = 0
    public var uid_type: Int64 = 0
		public var autoarmId: Int64 = 0
   
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let device : Int64 = "device" <~~ json { self.device = device }
        if let section : Int64 = "section" <~~ json { self.section = section }
        if let zone : Int64 = "zone" <~~ json { self.zone = zone }
        if let site : Int64 = "site" <~~ json { self.site = site }
        if let name : String = "name" <~~ json { self.name = name }
        if let detector : Int64 = "detector" <~~ json { self.detector = detector }
        if let scheduleId : Int64 = "scheduleId" <~~ json { self.scheduleId = scheduleId }
        if let tracert : Int64 = "tracert" <~~ json { self.tracert = tracert }
        if let flags : Int64 = "flags" <~~ json { self.flags = flags }
        if let delay : Int64 = "delay" <~~ json { self.delay = delay }
				if let bypass : Int64 = "bypass" <~~ json { self.bypass = bypass }
        if let arm : Int64 = "arm" <~~ json { self.arm = arm }
        if let uid_type : Int64 = "uid_type" <~~ json { self.uid_type = uid_type }
				if let autoarmId : Int64 = "autoarm_id" <~~ json { self.autoarmId = autoarmId }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "device" ~~> device,
            "section" ~~> section,
            "zone" ~~> zone,
            "name" ~~> name,
            "detector" ~~> detector,
            "scheduleId" ~~> scheduleId,
            "tracert" ~~> tracert,
            "flags" ~~> flags,
            "delay" ~~> delay,
						"bypass" ~~> bypass,
            "arm" ~~> arm,
            "site" ~~> site,
            "uid_type" ~~> uid_type,
						"autoarm_id" ~~> autoarmId
            ])
    }
}

