//
//  HubSites.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
//
//  HubSites.swift
//  Model Generated using http://www.jsoncafe.com/

import Foundation
import Gloss

//MARK: - HubSites
public class HubSites: Glossy {
    public var count : Int64 = 0
    public var items : [HubSite] = []
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json { self.count = count }
        if let items : [HubSite] = "items" <~~ json { self.items = items }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}

//MARK: - HubSite
public class HubSite: Glossy {
    public var address : HubAddres = HubAddres()
    public var armMode : Int64 = 0
    public var category : Int64 = 0
    public var comment : String = ""
    public var contractId : Int64 = 0
    public var contractStatus : Int64 = 0
    public var delegated : Int64 = 0
    public var devices : [HubDevice] = []
    public var domain : Int64 = 0
    public var extra : String = ""
    public var id : Int64 = 0
    public var isCrossing : Int64 = 0
    public var name : String = ""
    public var type : Int64 = 0
		public var autoarmId : Int64 = 0
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let address : HubAddres = "address" <~~ json { self.address = address }
        if let armMode : Int64 = "arm_mode" <~~ json { self.armMode = armMode }
        if let category : Int64 = "category" <~~ json { self.category = category }
        if let comment : String = "comment" <~~ json { self.comment = comment }
        if let contractId : Int64 = "contract_id" <~~ json { self.contractId = contractId }
        if let contractStatus : Int64 = "contract_status" <~~ json { self.contractStatus = contractStatus }
        if let delegated : Int64 = "delegated" <~~ json { self.delegated = delegated }
        if let devices : [HubDevice] = "devices" <~~ json { self.devices = devices }
        if let domain : Int64 = "domain" <~~ json { self.domain = domain }
        if let extra : String = "extra" <~~ json { self.extra = extra }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let isCrossing : Int64 = "is_crossing" <~~ json { self.isCrossing = isCrossing }
        if let name : String = "name" <~~ json { self.name = name }
        if let type : Int64 = "type" <~~ json { self.type = type }
				if let autoarmId : Int64 = "autoarm_id" <~~ json { self.autoarmId = autoarmId }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "address" ~~> address,
            "arm_mode" ~~> armMode,
            "category" ~~> category,
            "comment" ~~> comment,
            "contract_id" ~~> contractId,
            "contract_status" ~~> contractStatus,
            "delegated" ~~> delegated,
            "devices" ~~> devices,
            "domain" ~~> domain,
            "extra" ~~> extra,
            "id" ~~> id,
            "is_crossing" ~~> isCrossing,
            "name" ~~> name,
            "type" ~~> type,
						"autoarm_id" ~~> autoarmId
            ])
    }
}

//MARK: - HubDevice
public class HubDevice: Glossy {
    public var account : Int64 = 0
    public var configVersion : Int64 = 0
    public var domain : Int64 = 0
    public var id : Int64 = 0
    public var sections : [HubSection] = []
    public var parted: Int64 = 0
    public var users : [HubUser] = []
    public var cluster_id: Int64 = 0
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let account : Int64 = "account" <~~ json { self.account = account }
        if let configVersion : Int64 = "config_version" <~~ json { self.configVersion = configVersion }
        if let domain : Int64 = "domain" <~~ json { self.domain = domain }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let sections : [HubSection] = "sections" <~~ json { self.sections = sections }
        if let users : [HubUser] = "users" <~~ json { self.users = users }
        if let parted : Int64 = "parted" <~~ json { self.parted = parted }
        if let cluster_id : Int64 = "cluster_id" <~~ json { self.cluster_id = cluster_id }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "account" ~~> account,
            "config_version" ~~> configVersion,
            "domain" ~~> domain,
            "id" ~~> id,
            "sections" ~~> sections,
            "users" ~~> users,
            "parted" ~~> parted,
            "cluster_id" ~~> cluster_id,
            ])
    }
}

//MARK: - HubSection
public class HubSection: Glossy {
    public var flags : Int64 = 0
    public var name : String = ""
    public var scheduleId : Int64 = 0
    public var section : Int64 = 0
    public var tracert : Int64 = 0
    public var zones : [HubZone] = []
    public var detector: Int64 = 0
    public var arm : Int64 = 0
		public var autoarmId : Int64 = 0
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let flags : Int64 = "flags" <~~ json { self.flags = flags }
        if let name : String = "name" <~~ json { self.name = name }
        if let scheduleId : Int64 = "schedule_id" <~~ json { self.scheduleId = scheduleId }
        if let section : Int64 = "id" <~~ json { self.section = section }
        if let tracert : Int64 = "tracert" <~~ json { self.tracert = tracert }
        if let zones : [HubZone] = "zones" <~~ json { self.zones = zones }
        if let detector : Int64 = "detector" <~~ json { self.detector = detector }
        if let arm : Int64 = "arm" <~~ json { self.arm = arm }
				if let autoarmId : Int64 = "autoarm_id" <~~ json { self.autoarmId = autoarmId }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "flags" ~~> flags,
            "name" ~~> name,
            "schedule_id" ~~> scheduleId,
            "id" ~~> section,
            "tracert" ~~> tracert,
            "zones" ~~> zones,
            "detector" ~~> detector,
            "arm" ~~> arm,
						"autoarm_id" ~~> autoarmId
            ])
    }
}

//MARK: - HubZone
public class HubZone: Glossy {
    public var delay : Int64 = 0
		public var bypass : Int64 = 0
    public var detector : Int64 = 0
    public var flags : Int64 = 0
    public var form : Int64 = 0
    public var name : String = ""
    public var scheduleId : Int64 = 0
    public var tracert : Int64 = 0
    public var physic : Int64 = 0
    public var zone : Int64 = 0
    public var uid_type: Int64 = 0
	
    //MARK: Default Initializer
    init() { }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let delay : Int64 = "delay" <~~ json { self.delay = delay }
				if let bypass : Int64 = "bypass" <~~ json { self.bypass = bypass }
        if let detector : Int64 = "detector" <~~ json { self.detector = detector }
        if let flags : Int64 = "flags" <~~ json { self.flags = flags }
        if let form : Int64 = "form" <~~ json { self.form = form }
        if let name : String = "name" <~~ json { self.name = name }
        if let scheduleId : Int64 = "schedule_id" <~~ json { self.scheduleId = scheduleId }
        if let tracert : Int64 = "tracert" <~~ json { self.tracert = tracert }
        if let physic : Int64 = "physic" <~~ json { self.physic = physic }
        if let zone : Int64 = "id" <~~ json { self.zone = zone }
        if let uid_type : Int64 = "uid_type" <~~ json { self.uid_type = uid_type }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "delay" ~~> delay,
						"bypass" ~~> bypass,
            "detector" ~~> detector,
            "flags" ~~> flags,
            "form" ~~> form,
            "name" ~~> name,
            "schedule_id" ~~> scheduleId,
            "tracert" ~~> tracert,
            "physic" ~~> physic,
            "id" ~~> zone,
            "uid_type" ~~> uid_type,
            ])
    }
}

//MARK: - User
public class HubUser: Glossy {
    public var comment : String = ""
    public var name : String = ""
    public var phone : String = ""
    public var sections : [Int64] = []
    public var user_index : Int64 = 0
    public var device_id : Int64 = 0
    
    //MARK: Default Initializer
    init() {}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let comment : String = "comment" <~~ json { self.comment = comment }
        if let name : String = "name" <~~ json { self.name = name }
        if let phone : String = "phone" <~~ json { self.phone = phone }
        if let sections : [Int64] = "sections" <~~ json { self.sections = sections }
        if let user_index : Int64 = "user_index" <~~ json { self.user_index = user_index }
        if let device_id : Int64 = "device_id" <~~ json { self.device_id = device_id }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "comment" ~~> comment,
            "name" ~~> name,
            "phone" ~~> phone,
            "sections" ~~> sections,
            "user_index" ~~> user_index,
            "device_id" ~~> device_id,
            ])
    }
}

//MARK: - Addres
public class HubAddres: Glossy {
    public var apartmentNumber : String = ""
    public var fullAddress : [HubFullAddres] = []
    public var houseNumber : String = ""
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let apartmentNumber : String = "apartment_number" <~~ json { self.apartmentNumber = apartmentNumber }
        if let fullAddress : [HubFullAddres] = "full_address" <~~ json { self.fullAddress = fullAddress }
        if let houseNumber : String = "house_number" <~~ json { self.houseNumber = houseNumber }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "apartment_number" ~~> apartmentNumber,
            "full_address" ~~> fullAddress,
            "house_number" ~~> houseNumber,
            ])
    }
}

//MARK: - FullAddres
public class HubFullAddres: Glossy {
    public var addrType : String = ""
    public var canonical : String = ""
    public var code : Int64 = 0
    public var level : Int64 = 0
    public var name : String = ""
    public var parentCode : Int64 = 0
    
    //MARK: Default Initializer
    init() { }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let addrType : String = "addr_type" <~~ json { self.addrType = addrType }
        if let canonical : String = "canonical" <~~ json { self.canonical = canonical }
        if let code : Int64 = "code" <~~ json { self.code = code }
        if let level : Int64 = "level" <~~ json { self.level = level }
        if let name : String = "name" <~~ json { self.name = name }
        if let parentCode : Int64 = "parent_code" <~~ json { self.parentCode = parentCode }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "addr_type" ~~> addrType,
            "canonical" ~~> canonical,
            "code" ~~> code,
            "level" ~~> level,
            "name" ~~> name,
            "parent_code" ~~> parentCode,
            ])
    }
}
