//
//  HubDevices.swift
//  SecurityHub
//
//  Created by Timerlan on 14.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Gloss

//MARK: - HubDevices
public class HubDevices: Glossy {
    public var count : Int64 = 0
    public var items : [HubDevice] = []
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json { self.count = count }
        if let items : [HubDevice] = "items" <~~ json { self.items = items }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}
