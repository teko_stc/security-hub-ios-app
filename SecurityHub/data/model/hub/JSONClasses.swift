//
//  JSONClasses.swift
//  SecurityHub test
//
//  Created by Timerlan on 19.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import Gloss

//MARK: - JSONClasses
public class JSONClasses: Glossy {
    public var classes : [Classe] = []

    //MARK: Decodable
    public required init?(json: JSON){
        if let classes : [Classe] = "classes" <~~ json { self.classes = classes }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "classes" ~~> classes,
            ])
    }
}


//MARK: - Classe
public class Classe: Glossy {
    public var id : Int64 = 0
    public var name : String = ""
    public var iconBig : String = ""
    public var iconSmall : String = ""
    public var affect : String = ""
    public var reasons : [Reason] = []
    public var detectors : [Detector] = []
    public var statements : [Statement] = []
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let affect : String = "affect" <~~ json { self.affect = affect }
        if let detectors : [Detector] = "detectors" <~~ json { self.detectors = detectors }
        if let iconBig : String = "iconBig" <~~ json { self.iconBig = iconBig }
        if let iconSmall : String = "iconSmall" <~~ json { self.iconSmall = iconSmall }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let name : String = "name" <~~ json { self.name = name }
        if let reasons : [Reason] = "reasons" <~~ json { self.reasons = reasons }
        if let statements : [Statement] = "statements" <~~ json { self.statements = statements }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "affect" ~~> affect,
            "detectors" ~~> detectors,
            "iconBig" ~~> iconBig,
            "iconSmall" ~~> iconSmall,
            "id" ~~> id,
            "name" ~~> name,
            "reasons" ~~> reasons,
            "statements" ~~> statements,
            ])
    }
}


//MARK: - Reason
public class Reason: Glossy {
    public var id : Int64 = 0
    public var name : String = ""
    public var iconBig : String = ""
    public var iconSmall : String = ""
    public var affect : String = ""
    public var detectors : [Detector] = []
    public var statements : [Statement] = []
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let affect : String = "affect" <~~ json { self.affect = affect }
        if let detectors : [Detector] = "detectors" <~~ json { self.detectors = detectors }
        if let iconBig : String = "iconBig" <~~ json { self.iconBig = iconBig }
        if let iconSmall : String = "iconSmall" <~~ json { self.iconSmall = iconSmall }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let name : String = "name" <~~ json { self.name = name }
        if let statements : [Statement] = "statements" <~~ json { self.statements = statements }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "affect" ~~> affect,
            "detectors" ~~> detectors,
            "iconBig" ~~> iconBig,
            "iconSmall" ~~> iconSmall,
            "id" ~~> id,
            "name" ~~> name,
            "statements" ~~> statements,
            ])
    }
}


//MARK: - Detector
public class Detector: Glossy {
    public var id : Int64 = 0
    public var name : String = ""
    public var iconBig : String = ""
    public var iconSmall : String = ""
    public var affect : String = ""
    public var statements : [Statement] = []
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let affect : String = "affect" <~~ json { self.affect = affect }
        if let iconBig : String = "iconBig" <~~ json { self.iconBig = iconBig }
        if let iconSmall : String = "iconSmall" <~~ json { self.iconSmall = iconSmall }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let name : String = "name" <~~ json { self.name = name }
        if let statements : [Statement] = "statements" <~~ json { self.statements = statements }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "affect" ~~> affect,
            "iconBig" ~~> iconBig,
            "iconSmall" ~~> iconSmall,
            "id" ~~> id,
            "name" ~~> name,
            "statements" ~~> statements,
            ])
    }
}


//MARK: - Statement
public class Statement: Glossy {
    public var id : Int64 = 0
    public var name : String = ""
    public var icon : String = ""
    public var icons : [String] = []
    public var desc : String = ""

    
    //MARK: Decodable
    public required init?(json: JSON){
        if let desc : String = "desc" <~~ json { self.desc = desc }
        if let icon : String = "icon" <~~ json { self.icon = icon }
        if let icons : [String] = "icons" <~~ json { self.icons = icons }
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let name : String = "name" <~~ json { self.name = name }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "desc" ~~> desc,
            "icon" ~~> icon,
            "icons" ~~> icons,
            "id" ~~> id,
            "name" ~~> name,
            ])
    }
}
