//
//  HubAlarms.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 02.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import Gloss

//MARK: - HubAlarms
public class HubAlarms: Glossy {
    public var count : Int64 = 0
    public var items : [HubAlarm] = []
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json { self.count = count }
        if let items : [HubAlarm] = "items" <~~ json { self.items = items }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}

//MARK: - HubAlarm
public class HubAlarm: Glossy {
    public var alarm_id : Int64 = 0
    public var device_id : Int64 = 0
    public var state : Int64 = 0
    public var create_time : Int64 = 0
    public var type : Int64 = 0
    public var managed_user : Int64 = 0
    public var extra : String = ""
    public var event : HubEvent?

    //MARK: Decodable
    public required init?(json: JSON){
        if let alarm_id     : Int64     = "alarm_id" <~~ json       { self.alarm_id = alarm_id }
        if let device_id    : Int64     = "device_id" <~~ json      { self.device_id = device_id }
        if let state        : Int64     = "state" <~~ json          { self.state = state }
        if let create_time  : Int64     = "create_time" <~~ json    { self.create_time = create_time }
        if let type         : Int64     = "type" <~~ json           { self.type = type }
        if let managed_user : Int64     = "managed_user" <~~ json   { self.managed_user = managed_user }
        if let extra        : String    = "extra" <~~ json          { self.extra = extra }
        if let event        : HubEvent  = "event" <~~ json          { self.event = event }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "alarm_id"      ~~> alarm_id,
            "device_id"     ~~> device_id,
            "state"         ~~> state,
            "create_time"   ~~> create_time,
            "type"          ~~> type,
            "managed_user"  ~~> managed_user,
            "extra"         ~~> extra,
            "event"         ~~> event,
        ])
    }
}
