//
//  HubEmpty.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import Foundation
import Gloss

//MARK: - Empty Object
public class HubEmpty: Glossy {
  
    //MARK: Default Initializer
    init() {}
    
    //MARK: Decodable
    public required init?(json: JSON){}
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([])
    }
}
