//
//  HubOperators.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubOperators
public class HubOperators: Glossy {
    public var count : Int64 = 0
    public var items : [HubOperator] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubOperator] = "items" <~~ json {
            self.items = items
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
    
}
