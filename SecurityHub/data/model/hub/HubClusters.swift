//
//  HubEvents.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubClusters
public class HubClusters: Glossy {
    public var count : Int64 = 0
    public var items : [HubCluster] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubCluster] = "items" <~~ json {
            self.items = items
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
    
}


//MARK: - HubCluster
public class HubCluster: Glossy {
    public var id : Int64 = 0
    public var system : Int64 = 0
    public var cluster : Int64 = 0
    public var name : String = ""
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : Int64 = "id" <~~ json {
            self.id = id
        }
        if let system : Int64 = "system" <~~ json {
            self.system = system
        }
        if let cluster : Int64 = "cluster" <~~ json {
            self.cluster = cluster
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "system" ~~> system,
            "cluster" ~~> cluster,
            "name" ~~> name,
            ])
    }
}
