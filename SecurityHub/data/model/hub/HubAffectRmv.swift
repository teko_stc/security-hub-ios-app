//
//  HubAffectRmv.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubAffecrRmv
public class HubAffecrRmv: Glossy {
    public var deviceId : Int64 = 0
    public var deviceSection : Int64 = 0
    public var id : Int64 = 0
    
    //MARK: Default Initializer
    init()
    {
        deviceId = 0
        deviceSection = 0
        id = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let deviceId : Int64 = "device_id" <~~ json {
            self.deviceId = deviceId
        }
        if let deviceSection : Int64 = "device_section" <~~ json {
            self.deviceSection = deviceSection
        }
        if let id : Int64 = "id" <~~ json {
            self.id = id
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "device_id" ~~> deviceId,
            "device_section" ~~> deviceSection,
            "id" ~~> id,
            ])
    }
    
}

