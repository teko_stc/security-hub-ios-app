//
//  IvideonResponse.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import Foundation
import Gloss

//MARK: - CameraListResult
public struct IvideonResponse: Glossy {
    public var result : JSON!
    public var message : String!
    public var code : String!
    public var success : Bool!
    
    //MARK: Decodable
    public init?(json: JSON){
        result = "result" <~~ json
        message = "message" <~~ json
        code = "code" <~~ json
        success = "success" <~~ json
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "result" ~~> result,
            "success" ~~> success,
            "message" ~~> message,
            "code" ~~> code,
            ])
    }
}

