//
//  CameraList.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import Foundation
import Gloss
import RxSwift

//MARK: - CameraListResult
public struct CameraList: Glossy {
    public var items : [Camera]!
    
    //MARK: Decodable
    public init?(json: JSON){
        items = "items" <~~ json
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "items" ~~> items,
            ])
    }
}

//MARK: - Item
public struct Camera: Glossy {
    public var archiveSync : AnyObject!
    public var audioCodec : String!
    public var cloudArchiveMode : AnyObject!
    public var config : AnyObject!
    public var configState : String!
    public var connected : Bool!
    public var createdAt : AnyObject!
    public var explicitGranteeCount : AnyObject!
    public var features : [String]!
    public var geo : AnyObject!
    public var granteeCount : AnyObject!
    public var height : Int!
    public var id : String!
    public var mode : String!
    public var name : String!
    public var online : Bool!
    public var owner : String!
    public var ownerName : AnyObject!
    public var permissions : AnyObject!
    public var plan : String!
    public var rotation : Int!
    public var server : String!
    public var services : AnyObject!
    public var soundEnabled : Bool!
    public var timezone : String!
    public var type : String!
    public var uin : AnyObject!
    public var upsideDown : Bool!
    public var videoCodec : String!
    public var width : Int!
        
    //MARK: Decodable
    public init?(json: JSON){
        archiveSync = "archive_sync" <~~ json
        audioCodec = "audio_codec" <~~ json
        cloudArchiveMode = "cloud_archive_mode" <~~ json
        config = "config" <~~ json
        configState = "config_state" <~~ json
        connected = "connected" <~~ json
        createdAt = "created_at" <~~ json
        explicitGranteeCount = "explicit_grantee_count" <~~ json
        features = "features" <~~ json
        geo = "geo" <~~ json
        granteeCount = "grantee_count" <~~ json
        height = "height" <~~ json
        id = "id" <~~ json
        mode = "mode" <~~ json
        name = "name" <~~ json
        online = "online" <~~ json
        owner = "owner" <~~ json
        ownerName = "owner_name" <~~ json
        permissions = "permissions" <~~ json
        plan = "plan" <~~ json
        rotation = "rotation" <~~ json
        server = "server" <~~ json
        services = "services" <~~ json
        soundEnabled = "sound_enabled" <~~ json
        timezone = "timezone" <~~ json
        type = "type" <~~ json
        uin = "uin" <~~ json
        upsideDown = "upside_down" <~~ json
        videoCodec = "video_codec" <~~ json
        width = "width" <~~ json
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "archive_sync" ~~> archiveSync,
            "audio_codec" ~~> audioCodec,
            "cloud_archive_mode" ~~> cloudArchiveMode,
            "config" ~~> config,
            "config_state" ~~> configState,
            "connected" ~~> connected,
            "created_at" ~~> createdAt,
            "explicit_grantee_count" ~~> explicitGranteeCount,
            "features" ~~> features,
            "geo" ~~> geo,
            "grantee_count" ~~> granteeCount,
            "height" ~~> height,
            "id" ~~> id,
            "mode" ~~> mode,
            "name" ~~> name,
            "online" ~~> online,
            "owner" ~~> owner,
            "owner_name" ~~> ownerName,
            "permissions" ~~> permissions,
            "plan" ~~> plan,
            "rotation" ~~> rotation,
            "server" ~~> server,
            "services" ~~> services,
            "sound_enabled" ~~> soundEnabled,
            "timezone" ~~> timezone,
            "type" ~~> type,
            "uin" ~~> uin,
            "upside_down" ~~> upsideDown,
            "video_codec" ~~> videoCodec,
            "width" ~~> width,
            ])
    }
    
    func getPreview() -> Observable<UIImage> {
        return Observable.create({ (obs) -> Disposable in
            obs.onNext(IvideonApi.getCameraPreviewImg(accessToken: DataManager.settingsHelper.ivideonToken, id: self.id) ?? #imageLiteral(resourceName: "camera_btn")); obs.onCompleted()
            return Disposables.create {}
        }).subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getPreviewUrl(accessToken: String) -> String {
        return "\(IvideonApi.urlIvideonApi)/cameras/\(id ?? "0")/live_preview?access_token=\(accessToken)"
    }
    
    func getLiveUrl() -> Observable<String> {
        return Observable.create({ (obs) -> Disposable in
            obs.onNext(IvideonApi.getLiveUrl(accessToken: DataManager.settingsHelper.ivideonToken, id: self.id)); obs.onCompleted()
            return Disposables.create {}
        }).subscribeOn(ThreadUtil.shared.backScheduler)
    }
}


