//
//  Company.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct Company: Codable {
    let caption, logo: String
    let message: String?
    let departs: Departs?
    let addresses: Addresses?
    let cphones: Cphones?
    let emails: Emails?
    let sites: WebSites?
    let phones: Addresses?

    enum CodingKeys: String, CodingKey {
        case caption = "-caption"
        case logo = "-logo"
        case message = "-message"
        case departs, addresses, cphones, emails, sites, phones
    }
}

struct Addresses: Codable {
    let length: String
    let string: [String]

    enum CodingKeys: String, CodingKey {
        case length = "-length"
        case string
    }
}

struct Cphones: Codable {
    let cphone: [Cphone]
}

struct Cphone: Codable {
    let caption, phone, selfClosing: String

    enum CodingKeys: String, CodingKey {
        case caption = "-caption"
        case phone = "-phone"
        case selfClosing = "-self-closing"
    }
}

struct Departs: Codable {
    let depart: [Depart]
}

struct Depart: Codable {
    let subject, center: String
    let rcs: RCS

    enum CodingKeys: String, CodingKey {
        case subject = "-subject"
        case center = "-center"
        case rcs
    }
}

struct RCS: Codable {
    let rc: [RC]
}

struct RC: Codable {
    let caption, city: String
    let phones: Addresses
    let sites: WebSites

    enum CodingKeys: String, CodingKey {
        case caption = "-caption"
        case city = "-city"
        case phones, sites
    }
}

struct WebSites: Codable {
    let length: String
    let string: String

    enum CodingKeys: String, CodingKey {
        case length = "-length"
        case string
    }
}

struct Emails: Codable {
    let length: String?
    let string: String
    let legnth: String?

    enum CodingKeys: String, CodingKey {
        case length = "-length"
        case string
        case legnth = "-legnth"
    }
}
