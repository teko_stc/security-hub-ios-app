//
//  DataManager.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD
import RxSwift
import Localize_Swift
import RxCocoa

class DataManager {
    var disposables: CompositeDisposable = CompositeDisposable()
    let queue = DispatchQueue.global(qos: .utility)
    let nCenter = NotificationCenter.default

    func dispose(_ key: CompositeDisposable.DisposeKey?) {
        guard let key = key else { return }
        disposables.remove(for: key)
    }
        
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static var shared: DataManager!
    static let defaultHelper = DefaultsHelper()
    static var settingsHelper = SettingsHelper()
    let hubHelper: HubHelper
    private let ivideonApi: IvideonApi = IvideonApi()
    var dbHelper: DBHelper
    var pushHelper: PushHelper?
    var d3Const: D3ConstRepository
    private var connectionStateChange: Any?
		var isEventsLoading = false
    
    var scriptsLibrary: [Int64 : HubLibraryScripts] = [:]
    
	 	init() {
        Localize.setCurrentLanguage(DataManager.defaultHelper.language)
        dbHelper = DBHelper(login: UserDefaults.standard.string(forKey: "LOGINN") ?? "nil")
			
				d3Const = D3ConstRepository(languageIso: Localize.currentLanguage(), parserDidEndClasses: { classes in

        })
			
				hubHelper = HubHelper()
					
				ThreadUtil.shared.backOpetarion.async {
						if self.dbHelper.needJson(){
								self.xmlParse(self.d3Const.xmlConstRepository.classes)
						}
				}
				LocalConnection.instance = LocalConnection(port: LocalConnection.defaultPort)
    }

    private func xmlParse(_ classes: [XMLConstRepository.ClassModel]) {
        var result: [jClasses] = []
        for cl in classes {
            for clDet in cl.detectors {
                for clDetRea in clDet.reasons {
                    for clDetReaSt in clDetRea.statements{
                        result.append(
                            jClasses(
                                _class: cl.id.int64,
                                reasons: clDetRea.id.int64,
                                detectors: clDet.id.int64,
                                statements: clDetReaSt.id.int64,
                                name: clDetReaSt.caption,
                                icon: clDetReaSt.icons.count == 1 ? clDetReaSt.icons[0] : clDetReaSt.iconS,
                                iconBig: clDetReaSt.icons.count == 3 ? clDetReaSt.icons[2] : clDetReaSt.iconS,
                                affect_desc: clDetReaSt.description
                            )
                        )
                    }
                }
                for clDetStat in clDet.statements{
                    result.append(
                        jClasses(
                            _class: cl.id.int64,
                            reasons: nil,
                            detectors: clDet.id.int64,
                            statements: clDetStat.id.int64,
                            name: clDetStat.caption,
                            icon: clDetStat.icons.count == 1 ? clDetStat.icons[0] : clDetStat.iconS,
                            iconBig: clDetStat.icons.count == 3 ? clDetStat.icons[2] : clDetStat.iconS,
                            affect_desc: clDetStat.description
                        )
                    )
                }
            }
            
            for clRea in cl.reasons{
                for clReaStat in clRea.statements{
                    result.append(
                        jClasses(
                            _class: cl.id.int64,
                            reasons: clReaStat.id.int64,
                            detectors: nil,
                            statements: clReaStat.id.int64,
                            name: clReaStat.caption,
                            icon: clReaStat.icons.count == 1 ? clReaStat.icons[0] : clReaStat.iconS,
                            iconBig: clReaStat.icons.count == 3 ? clReaStat.icons[2] : clReaStat.iconS,
                            affect_desc: clReaStat.description
                        )
                    )
                }
            }
           
            for clStat in cl.statements{
                result.append(
                    jClasses(
                        _class: cl.id.int64,
                        reasons: nil,
                        detectors: nil,
                        statements: clStat.id.int64,
                        name: clStat.caption,
                        icon: clStat.icons.count == 1 ? clStat.icons[0] : clStat.iconS,
                        iconBig: clStat.icons.count == 3 ? clStat.icons[2] : clStat.iconS,
                        affect_desc: clStat.description
                    )
                )
            }
            result.append(
                jClasses(
                    _class: cl.id.int64,
                    reasons: -1,
                    detectors: -1,
                    statements: -1,
                    name: cl.caption,
                    icon: cl.iconSmall,
                    iconBig: cl.iconSmall,
                    affect_desc: cl.affect
                )
            )
        }
        dbHelper.addJson(result)
    }
    
    private func getJson() {
        var bundle = Bundle.main;
        if let path = Bundle.main.path(forResource: Localize.currentLanguage(), ofType: "lproj"), let bun = Bundle(path: path) { bundle = bun }
        if let path = bundle.path(forResource: "events", ofType: "json"), let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
            let classes = JSONClasses(json: try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! [String : Any])
            var result: [jClasses] = []
            for cl in classes!.classes {
                for cRea in cl.reasons{
                    for cDet in cRea.detectors{
                        for cStat in cDet.statements{
                            result.append(jClasses(_class: cl.id, reasons: cRea.id, detectors: cDet.id, statements: cStat.id, name: cStat.name, icon: cDet.iconSmall, iconBig: cStat.icon, affect_desc: cStat.desc))
                        }
                    }
                    for cReaStat in cRea.statements{
                         result.append(jClasses(_class: cl.id, reasons: cRea.id, detectors: nil, statements: cReaStat.id, name: cReaStat.name, icon: cRea.iconSmall, iconBig: cReaStat.icon, affect_desc: cReaStat.desc))
                    }
                }
                for clDet in cl.detectors{
                    for clDetStat in clDet.statements{
                        result.append(jClasses(_class: cl.id, reasons: nil, detectors: clDet.id, statements: clDetStat.id, name: clDetStat.name, icon: clDet.iconSmall, iconBig: clDetStat.icon, affect_desc: clDetStat.desc))
                    }
                }
                for clStat in cl.statements{
                     result.append(jClasses(_class: cl.id, reasons: nil, detectors: nil, statements: clStat.id, name: clStat.name, icon: cl.iconSmall, iconBig: clStat.icon, affect_desc: clStat.desc))
                }
                result.append(jClasses(_class: cl.id, reasons: -1, detectors: -1, statements: -1, name: cl.name, icon: cl.iconSmall, iconBig: cl.iconSmall, affect_desc: cl.affect))
            }
            dbHelper.addJson(result)
        }
    }
    
    func getZoneTypes() -> DTypesEntity {
        var bundle = Bundle.main;
        if  let path = Bundle.main.path(forResource: Localize.currentLanguage(), ofType: "lproj"),
            let bun = Bundle(path: path) { bundle = bun }
        if  let path = bundle.path(forResource: "types", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String : Any] {
            return DTypesEntity(json: json)
        }
        return DTypesEntity(json: [:])
    }
    
    func getSelector(name: String) -> DSelectorEntity? {
        if  let path = Bundle.main.path(forResource: name, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String : Any] {
            if let selectorJson = json["selector"] as? [String : Any] {
                return DSelectorEntity(json: selectorJson)
            } else {
                return nil
            }
        }
        return nil
    }
    
    public func getAllClasses() -> Observable<[(id: Int64, name: String)]>{
        return Observable.just(dbHelper.getAllClasses()).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getClasses() -> [(id: Int64, name: String)]{
        return dbHelper.getAllClasses()
    }
    
    public static var connectDisposable: Disposable?
    public func connect() -> Observable<Bool>{
        return Observable<Bool>.create{ observer in
            if self.hubHelper.isAlive() {
                observer.onNext(true)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubSitesEnd"), object: nil)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubEventsEnd"), object: nil)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubOptionsEnd"), object: nil)
                observer.onCompleted()
                return Disposables.create{}
            }
            if self.getUser().login.count > 0 {
                self.hubHelper.connect(login: self.getUser().login, pass: self.getUser().password, self, self)
                self.connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main){ notification in
                    NotificationCenter.default.removeObserver(self.connectionStateChange!)
                    let code = notification.object as? Int ?? 0
                    if code > 0 || code == HubResponseCode.RECONNECT_CODE {
                        self.dbHelper = DBHelper(login: self.getUser().login)
//                        DataManager.settingsHelper.setDefaults()
                    }
                    observer.onNext(code > 0 || code == HubResponseCode.RECONNECT_CODE); observer.onCompleted()
                    //UI message in BASEVIEW
                }
            }else{
                observer.onNext(false)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubSitesEnd"), object: nil)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubEventsEnd"), object: nil)
                DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubOptionsEnd"), object: nil)
                observer.onCompleted()
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func connect(login: String, password: String) -> Observable<(success: Bool, errorMes: String?)>{
        return Observable.create{ observer in
            self.hubHelper.connect(login: login, pass: password, self, self)
            self.connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main ){ notification in
                NotificationCenter.default.removeObserver(self.connectionStateChange!)
                let code = notification.object as? Int64 ?? 0
                if code > 0 {
                    DataManager.defaultHelper.login = login
                    DataManager.defaultHelper.password = password
                    self.dbHelper = DBHelper(login: login)
                }
                observer.onNext((success: code > 0, errorMes: HubResponseCode.getError(error: code)));observer.onCompleted()
            };
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public static var disconnectDisposable: Disposable?
    public func disconnect() -> Observable<Any>{
        let void = XServerChanger.fcm == nil ? Single.just(DCommandResult.SUCCESS) : hubHelper.setCommandWithResult(.FIREBASE_ID_DEL, D: [:])
        return void.asObservable()
            .concatMap({ _ -> Observable<Any> in
                return Observable<Any>.create{ observer in
                    self.pushHelper = nil
                    self.hubHelper.disconnect()
                    DataManager.defaultHelper.clear()
                    DataManager.settingsHelper.setDefaults()
                    self.dbHelper = DBHelper(login: "nil")
                    observer.onNext(1); observer.onCompleted();
                    return Disposables.create{}
                }
            }).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private func disconnect2() -> Observable<Any>{
        Observable<Any>.create { observer in
            self.hubHelper.disconnect()
            DataManager.defaultHelper.clear()
            DataManager.settingsHelper.setDefaults()
            self.dbHelper = DBHelper(login: "nil")
            observer.onNext(1)
            observer.onCompleted()
            return Disposables.create{}
        }
    }
    
    public func getUser() -> User{
        let dH = DataManager.defaultHelper
        return User(login: dH.login, password: dH.password, pin: dH.pin, id: dH.id, domainId: dH.domainId, orgId: dH.orgId, roles: dH.roles)
    }
    
    public func clearAfterLang(){
        dbHelper.clearJClass()
        dbHelper.deleteEvents()
        d3Const = D3ConstRepository(languageIso: Localize.currentLanguage(), parserDidEndClasses: parserDidEndClasses)
        hubHelper.disconnect()
    }
        
    private func parserDidEndClasses(_ xml: [XMLConstRepository.ClassModel]) {
        ThreadUtil.shared.backOpetarion.async {
            if self.dbHelper.needJson(){
                self.xmlParse(xml)
            }
        }
    }
    
    public func setPin(_ pin: String){ DataManager.defaultHelper.pin = pin }
    
    public func getSites(completable: Bool = false)->Observable<(site: Sites, type: NUpdateType)>{
        return Observable.create{ obs in
            self.dbHelper.getSites().forEach({ (s) in
                obs.onNext((site: s, type: .insert))
            })
            if completable { obs.onCompleted() }
            let o = NotificationCenter.default.addObserver(forName: HubNotification.siteUpdate, object: nil, queue: OperationQueue.main) { not in
							if let r = not.object as? NSiteEntity { obs.onNext((site: r.site ?? Sites(id: r.id, name: "", domain: 0, delegated: 0, time: 0, autoarmId: -1), type: r.type)) }
            }
            return Disposables.create{ NotificationCenter.default.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getSections(site: Int64? = nil, device: Int64)->Observable<[Sections]>{
        return Observable.just(dbHelper.getSections(site_id: site, device_id: device)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getSections(site_id: Int64? = nil, device_id: Int64)-> [Sections] {
        return dbHelper.getSections(site_id: site_id, device_id: device_id)
    }
    
    public func getEmptySectionIndex(device_id: Int64)-> Int64? {
        var indexes: [Int64] = Array(1...8)
        dbHelper.getSections(site_id: nil, device_id: device_id).forEach { (s) in
            indexes = indexes.filter({ (i) -> Bool in i != s.section })
        }
        return indexes.first
    }
    
    public func getSection(device: Int64, section: Int64) -> Observable<Sections?> {
        return Observable.just(dbHelper.get(device_id: device, section_id: section)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getSection(device: Int64, section: Int64) -> Sections? { return dbHelper.get(device_id: device, section_id: section) }
    
    public func getZone(device: Int64, section: Int64, zone: Int64) -> Zones? { return dbHelper.get(device_id: device, section_id: section, zone_id: zone) }
    
    public func getName(device: Int64, section: Int64 = 0, zone: Int64 = 0) -> Observable<String>{
        return Observable.just(dbHelper.getName(device_id: device, section_id: section, zone_id: zone)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getDeviceName(device: Int64) -> String{
        return dbHelper.getName(device_id: device)
    }
   
    public func getEvent(_class: Int64, reason: Int64, active: Int64, device: Int64) -> Observable<Events?>{
        return Observable.just(dbHelper.get(_class: _class, reason: reason, active: active, device_id: device)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getEvents() -> Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEvents().forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                guard siteIds.count > 0 else { return }
                obs.onNext((event: e, siteIds: siteIds, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: e.event, siteIds: siteIds, type: e.type, isFromBD: false))
            }
            let o2 = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.main) { notification in
                guard let ivVideo = notification.object as? IvVideo,
                      let event = self.dbHelper.get(event_id: ivVideo.eventId) else { return }
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: event.device)
                    .filter{ (rel) -> Bool in return rel.device == event.device && rel.section == event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: event, siteIds: siteIds, type: NUpdateType.update, isFromBD: false))
            }
            return Disposables.create{
                NotificationCenter.default.removeObserver(o)
                NotificationCenter.default.removeObserver(o2)
            }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getEvents(startTime: Int64, endTime: Int64) -> Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEvents(startTime: startTime, endTime: endTime).forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.device, section_id: e.section, zone_id: e.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )

                obs.onNext((event: e, siteIds: siteIds, names: names, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.current) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                guard e.event.time >= startTime, e.event.time <= endTime else { return }
                
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.event.device, section_id: e.event.section, zone_id: e.event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: e.event, siteIds: siteIds, names: names, type: e.type, isFromBD: false))
            }
            let o2 = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.current) { notification in
                guard let ivVideo = notification.object as? IvVideo,
                      let event = self.dbHelper.get(event_id: ivVideo.eventId) else { return }
                guard event.time >= startTime, event.time <= endTime else { return }
                
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: event.device)
                    .filter{ (rel) -> Bool in return rel.device == event.device && rel.section == event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: event, siteIds: siteIds, names: names, type: NUpdateType.update, isFromBD: false))
            }
            return Disposables.create{
                NotificationCenter.default.removeObserver(o)
                NotificationCenter.default.removeObserver(o2)
            }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getEvents(device_id: Int64) -> Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEvents(device_id: device_id).forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.device, section_id: e.section, zone_id: e.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )

                obs.onNext((event: e, siteIds: siteIds, names: names, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.current) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                guard e.event.device == device_id else { return }
                
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.event.device, section_id: e.event.section, zone_id: e.event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: e.event, siteIds: siteIds, names: names, type: e.type, isFromBD: false))
            }
            let o2 = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.current) { notification in
                guard let ivVideo = notification.object as? IvVideo,
                      let event = self.dbHelper.get(event_id: ivVideo.eventId) else { return }
                guard event.device == device_id else { return }

                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: event.device)
                    .filter{ (rel) -> Bool in return rel.device == event.device && rel.section == event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: event, siteIds: siteIds, names: names, type: NUpdateType.update, isFromBD: false))
            }
            return Disposables.create{
                NotificationCenter.default.removeObserver(o)
                NotificationCenter.default.removeObserver(o2)
            }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getEvents(_class: Int64, active: Int64) -> Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], names: XEventsViewCellModel.Names, type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEvents(_class: _class, active: active).forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.device, section_id: e.section, zone_id: e.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )

                obs.onNext((event: e, siteIds: siteIds, names: names, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.current) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                guard e.event._class == _class, e.event.active == active  else { return }
                
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: e.event.device, section_id: e.event.section, zone_id: e.event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: e.event, siteIds: siteIds, names: names, type: e.type, isFromBD: false))
            }
            let o2 = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.current) { notification in
                guard let ivVideo = notification.object as? IvVideo,
                      let event = self.dbHelper.get(event_id: ivVideo.eventId) else { return }
                guard event._class == _class, event.active == active  else { return }

                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: event.device)
                    .filter{ (rel) -> Bool in return rel.device == event.device && rel.section == event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                
                let zi = self.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone)
                let names = XEventsViewCellModel.Names(
                    site: zi?.siteNames ?? "",
                    device: zi?.deviceName ?? "",
                    section: zi?.sectionName ?? "",
                    zone: zi?.zone.name ?? ""
                )
                
                obs.onNext((event: event, siteIds: siteIds, names: names, type: NUpdateType.update, isFromBD: false))
            }
            return Disposables.create{
                NotificationCenter.default.removeObserver(o)
                NotificationCenter.default.removeObserver(o2)
            }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getEventsXAlarmAffectsController() -> Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEventsXAlarmAffectsController().forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                guard siteIds.count > 0 else { return }
                obs.onNext((event: e, siteIds: siteIds, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                guard e.event._class < 4 && e.event.active == 1 else { return }
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: e.event, siteIds: siteIds, type: e.type, isFromBD: false))
            }
            let o2 = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.main) { notification in
                guard let ivVideo = notification.object as? IvVideo,
                      let event = self.dbHelper.get(event_id: ivVideo.eventId) else { return }
                guard event._class < 4 && event.active == 1 else { return }
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: event.device)
                    .filter{ (rel) -> Bool in return rel.device == event.device && rel.section == event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: event, siteIds: siteIds, type: NUpdateType.update, isFromBD: false))
            }
            return Disposables.create{
                NotificationCenter.default.removeObserver(o)
                NotificationCenter.default.removeObserver(o2)
            }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getAlarmAffectsCount() -> Observable<(Int,Bool)> {
        Observable<(Int,Bool)>.create { obs in
            obs.onNext(self.dbHelper.getAllAlarmAffectsInfo())
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main) { notification in
                obs.onNext(self.dbHelper.getAllAlarmAffectsInfo())
            }
            return Disposables.create{ NotificationCenter.default.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public func getNameSites(_ device_id: Int64) -> String {
        return dbHelper.getDeviceSitesInfo(device_id: device_id).siteNames
    }
    
    public func findVideo(_ id: Int64) -> Observable<IvVideo?>{
        return Observable.just(dbHelper.get(ivVideo_id: id)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getZoneRelation(cam: String) -> Observable<[SelectElement]> {
        return Observable.create{ observer in
            var result: [SelectElement] = []
            for site in self.dbHelper.getSites() {
                var seS: [SelectElement] = []
                for sec in self.dbHelper.getSections(site_id: site.id){
                    var zS: [SelectElement] = []
                    for zone in self.dbHelper.getZones(device_id: sec.device, section_id: sec.section){
                        if let rel = self.relation.first(where: {$0.device == zone.device && $0.section == zone.section && $0.zone == zone.zone}){
                            if rel.camId == cam{ zS.append(SelectElement(name: zone.name, selected: true, obj: zone)) }
                        }else{
                            zS.append(SelectElement(name: zone.name, selected: false, obj: zone))
                            
                        }
                    }
                    seS.append(SelectElement(name: sec.name, list: zS))
                }
                result.append(SelectElement(name: site.name, list: seS))
            }
            observer.onNext(result);
            observer.onCompleted()
        return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func getCamSet(_ cam: String) -> Observable<CamSet> {
        return Observable.just(dbHelper.getCamSet(cam_id: cam))
    }
    
    func updCamSet(_ set: CamSet) { ThreadUtil.shared.backOpetarion.sync { self.dbHelper.updCamSet(camSet: set) }  }
    
    func getEvents(camId: String) -> Observable<[(event: Events, iv: IvVideo)]> {
        return Observable.create{ observer in
            observer.onNext(self.dbHelper.getEvents(cam_id: camId))
            NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(camId: camId), object: nil, queue: OperationQueue.main){ notification in
                //let video = notification.object as! IvVideo
                observer.onNext(self.dbHelper.getEvents(cam_id: camId))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func getEvents(cameraId: String) -> Observable<DEventCameraEntity> {
        return Observable.create{ observer in
            self.dbHelper.getEvents(cameraId: cameraId).forEach { ec in observer.onNext(ec) }
            let o = self.nCenter.addObserver(forName: HubNotification.ivVideo(camId: cameraId), object: nil, queue: OperationQueue.current) { notification in
                guard let video = notification.object as? IvVideo, let ec = self.dbHelper.getCameraEvent(eventId: video.eventId) else { return }
                observer.onNext(ec)
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getEvent(id: Int64) -> Events? { return self.dbHelper.get(event_id: id) }
    
    // TODO: Fail
    func getOperator() -> Operators {
        let defaults = DataManager.defaultHelper
        return dbHelper.get(operator_id: getUser().id) ?? Operators(id: defaults.id, domain: defaults.domainId, name: defaults.login, active: 1, contacts: "", login: defaults.login, org: 0, roles: defaults.roles, time: 0)
    }
    
    func getOperator(_ id: Int64) -> Operators? { return dbHelper.get(operator_id:id) }
    func getSitesCount() -> Int { return dbHelper.getSites().count }
    
    private var operatorNotification: Any?
    public func getOperators()->Observable<(operators: [Operators], type: NUpdateType)>{
        return Observable.create{ observer in
            observer.onNext((operators: self.dbHelper.getOperators().filter({ (op) -> Bool in op.id != self.getUser().id }),
                             type: .insert))
            if self.operatorNotification != nil{ NotificationCenter.default.removeObserver(self.operatorNotification!) }
            self.operatorNotification = NotificationCenter.default.addObserver(forName: HubNotification.operatorsUpdate, object: nil, queue: OperationQueue.main){ notification in
                let op = notification.object as! (operators: [Operators], type: NUpdateType)
                observer.onNext((operators: op.operators.filter({ (op) -> Bool in op.id != self.getUser().id }),
                                 type: op.type))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getOperatorObservable()->Observable<Operators>{
        return Observable.create{ observer in
            observer.onNext(self.dbHelper.get(operator_id: self.getUser().id) ?? self.getOperator())
            let o = self.nCenter.addObserver(forName: HubNotification.operatorsUpdate, object: nil, queue: OperationQueue.main){ notification in
                let op = notification.object as! (operators: [Operators], type: NUpdateType)
                if let _op = op.operators.first(where: { (op) -> Bool in op.id == self.getUser().id }) {
                    observer.onNext(_op)
                }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public var clusters: HubClusters?
    public var relation: [HubRelation] = []
    public var sounds: [String:Any] = [:]
    
    public func getDevice(device: Int64)->Observable<Devices?>{
        return Observable.just(dbHelper.get(device_id: device)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getDeviceUsers(device_ids: [Int64]) -> Observable<(deviceUsers: [DeviceUsers], type: NUpdateType)>{
        return Observable.create{ observer in
            observer.onNext((deviceUsers: self.dbHelper.getDeviceUsers(device_ids: device_ids), type: .insert))
            device_ids.forEach({ (device) in
                NotificationCenter.default.addObserver(forName: HubNotification.deviceUsersUpdate(device), object: nil, queue: OperationQueue.main){ notification in
                    observer.onNext(notification.object as! (deviceUsers: [DeviceUsers], type: NUpdateType)) }
            })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getUserSection(user: DeviceUsers) -> Observable<[Int64]>{
        return Observable.just(dbHelper.getUserSectionIds(deviceUser: user)).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    public func addIvVideo(_ ivVideo: IvVideo){ ThreadUtil.shared.backOpetarion.sync { self.dbHelper.add(ivVideo: ivVideo) } }
    
//    func add(_ obj: SQLCommand) -> Observable<Bool>{
//        return Observable.create{ obs in self.dbHelper.add(obj);obs.onNext(true); obs.onCompleted(); return Disposables.create{} }
//        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.backScheduler)
//    }

    private func rmvAffects(id: Int64? = nil) -> Observable<Events?>{
        return Observable.create{ obs in
            if id != nil{ obs.onNext(self.dbHelper.rmvAffects(event_id: id!))
            }else{ self.dbHelper.rmvAffects();obs.onNext(nil)
            }; obs.onCompleted(); return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    public func getZonesWithoutBindingCamera() -> Single<[DZoneWithSitesDeviceSectionInfo]> {
        Single<[DZoneWithSitesDeviceSectionInfo]>.create{ event -> Disposable in
            let zones: [DZoneWithSitesDeviceSectionInfo] = self.dbHelper.getZones()
                .filter({ dz in !HubConst.isRelay(dz.zone.detector) && !HubConst.isExit(dz.zone.detector) })
                .filter({ dz in !self.relation.contains(where: { r in r.device == dz.zone.device && r.section == dz.zone.section && r.zone == dz.zone.zone }) })
            event(SingleEvent.success(zones))
            return Disposables.create{ }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
}

//HUBHELPER Command
extension DataManager{
    func updateSites() { hubHelper.setCommand(HubCommand.SITES) }
    func updateOperators() { hubHelper.setCommand(HubCommand.OPERATORS) }
    func updateAffects() {
        self.hubHelper.setCommand(HubCommand.EVENTS, D: ["affect" : 1], id: HubHelper.AFFECT_COMMAND_ID)
    }
//    func updateEvents() {
//         _ = getMaxTime().subscribe(onNext: { time in
//            let ntime = Int64(NSDate().timeIntervalSince1970 - 86400 * 4)
////            self.hubHelper.setCommand(HubCommand.EVENTS, D: ["tmin": time > ntime ? time : ntime])
//            self.hubHelper.setCommand(HubCommand.EVENTS, D: ["tmin": time > ntime, "affect" : 0])
//         })
//    }
    func updateClusters() { hubHelper.setCommand(HubCommand.CLUSTERS) }
    
    
    func delete(site_id: Int64) -> Observable<Bool> { return hubHelper.setCommandWhithId(.SITE_DEL, D: ["site": site_id]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    func addSite(name: String, sn: String, cluster: Int) -> Observable<Bool> {
        let pincode = Int(sn[sn.count-4..<sn.count]) ?? 0
        let account = Int(sn[0..<sn.count-4]) ?? 0
        return hubHelper.setCommandWhithId(.DEVICE_ASSIGN_DOMAIN, D: ["account": account, "pin_code": pincode, "new_site_name": name, "cluster_id": cluster])
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    func addDevice(site: Int64, sn: String, cluster: Int) -> Observable<Bool> {
        let pincode = Int(sn[sn.count-4..<sn.count]) ?? 0
        let account = Int(sn[0..<sn.count-4]) ?? 0
        return hubHelper.setCommandWhithResult(.DEVICES, D: [:])
        .map({ (r) -> [HubDevice] in return HubDevices.init(json: r.d)?.items ?? [] })
        .concatMap({ (devices) -> Observable<Bool> in
            if let d = devices.first(where: {$0.account == account }){ return self.hubHelper.setCommandWhithId(.DEVICE_ASSIGN_SITE, D: ["site_id": site, "device_id": d.id]) }
            else{ return self.hubHelper.setCommandWhithId(.DEVICE_ASSIGN_DOMAIN, D: ["account": account, "pin_code": pincode, "cluster": cluster], whithError: false)
                .catchError({ (e) -> Observable<Bool> in return Observable.just(true) })
                .concatMap({ (b) -> Observable<Bool> in return self.addDevice(site: site, sn: sn, cluster: cluster) }) }
        }).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    func removeDevice(device_id: Int64, site: Int64 = 0) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.DEVICE_REVOKE_DOMAIN, D: ["device_id" : device_id, "site": site]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func ussd(device: Int64, text: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["command": ["USSD":["text":text]], "device":device]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    func addSection(device: Int64, name: String?,  index: Int64? = nil, type: Int64? = nil, needError: Bool = true) -> Observable<Bool> {
        var params: [String:Any] = [:]
        if name != nil { params.updateValue(name!, forKey: "name") }
        if index != nil { params.updateValue(index!, forKey: "index") }
        if type != nil { params.updateValue(type!, forKey: "type") }
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["SECTION_SET" : params ]], whithError: needError).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)}
    func deleteSectionZone(device: Int64, section: Int64, zone: Int64 = 0) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.ZONE_DEL, D: ["device_id" : device, "device_section": section, "zone": zone]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    
    func deleteSection(device: Int64, index: Int64) -> Observable<Bool> {
        return Observable<Any>.create({ obs in
            if self.dbHelper.getZones(device_id: device, section_id: index).count == 0 { obs.onNext(true) }
            else{ obs.onError(MyError.haveZones) }
            obs.onCompleted();return Disposables.create { }
        }).concatMap({ (b) -> Observable<Bool> in
            return self.hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["SECTION_DEL" : ["index" : index]] ])
        }).subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func updateZoneName(device: Int64, section: Int64, zone: Int64, name: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.ZONE_NAME, D: ["device_id": device, "section" : section, "zone" : zone, "name" : name ]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }

    func startInteractiveMode(device_id: Int64) {
        ThreadUtil.shared.backOpetarion.sync { hubHelper.setCommand(.COMMAND_SET, D: ["device" : device_id, "command" : ["INTERACTIVE" : 1]]) }
    }
    
    func stopInteractiveMode(device_id: Int64) {
        ThreadUtil.shared.backOpetarion.sync { hubHelper.setCommand(.COMMAND_SET, D: ["device" : device_id, "command" : ["INTERACTIVE" : 0]]) }
    }
    
    func reset(device: Int64, section: Int64, zone: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["RESET" : ["section" : section, "zone" : zone]]])
        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }

    func addZone(device: Int64, uid: String, section_id: Int64, name: String, delay: Int? = nil , detector: Int? = nil) -> Observable<Bool> {
        let delay = delay == nil ? "" : ", \"delay\": \(delay!)"
        let detector = detector == nil ? "" : ", \"detector\":\(detector!)"
        let text = "{ \"ZONE_SET\": { \"uid\": \"\(uid)\", \"section\": \(section_id), \"name\": \"\(name)\" \(delay) \(detector) }}"
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command": text])
        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func deregisterZone(device: Int64, section: Int64, zone: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["ZONE_DEL" : ["section" : section, "index" : zone]] ]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    
    func delegate_Req(_ user_code: String?) -> Observable<Bool> { return hubHelper.setCommandWhithId(.DELEGATE_REQ, D: ["user_code": user_code ?? "as"]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    func delegate_delete(site: Int64, domain_id: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.DELEGATE_REVOKE, D: ["site_id": site, "domain_id" : domain_id ]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler) }
    func delegate_new(site_id: Int64) -> Observable<(result: Int64, user_code: String )> {
        return hubHelper.setCommandWhithResult(.DELEGATE_NEW, D: ["site_id": site_id ])
            .map({ (resp) -> (result: Int64, user_code: String ) in
                if let c = resp.d["user_code"] { return (result: resp.r, user_code: c as! String ) }else{ return (result: resp.r, user_code: "" ) }
            }).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    func delegate_sites(site_id: Int64) -> Observable<[HubSiteDelegate]> {
        return hubHelper.setCommandWhithResult(.SITE_DELEGATES, D: ["site_id": site_id ])
            .map({ (resp) -> [HubSiteDelegate] in return HubSiteDelegates(json: resp.d)!.items })
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func reboot(device: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["REBOOT" : [] ]]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func getIviRelations() { hubHelper.setCommand(.IV_GET_RELATIONS, D: ["device": 0, "section": 0, "zone": 0]) }
    
    func getIviSaveSession() -> Observable<String> {
        return hubHelper.setCommandWhithResult(.OPERATOR_SAFE_SESSION, D: [:])
        .map{ (resp) -> String in return resp.d["session"] as? String ?? "k" }
        .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getCameraList() -> Observable<[Camera]?> {
       return hubHelper.setCommandWhithResult(.IV_GET_TOKEN, D: ["domain": getUser().domainId ])
        .map{ (resp) -> String? in return resp.d["access_token"] as? String }
        .concatMap{ s -> Observable<[Camera]?> in
            if s == nil { return Observable.just(nil)
            }else{
                DataManager.settingsHelper.ivideonToken = s!
                return self.ivideonApi.getCameraList(accessToken: s!) }
        }.subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func bindCam(zone: Zones, cam: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.IV_BIND_CAM, D: ["device": zone.device, "section": zone.section, "zone": zone.zone, "id": cam]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func delRelation(zone: Zones) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.IV_DEL_RELATION, D: ["device": zone.device, "section": zone.section, "zone": zone.zone]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func changeOperator(id: Int64, value: Any, name: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_SET, D: ["id": id, name: value]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func delOperator(id: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_DEL, D: ["id": id]).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func addOperator(name: String, login: String, password: String, role: Int64 ) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_SET, D: ["name" : name, "login" : login, "password" : password.md5, "roles" : role, "active" : true, "contacts" : ""])
        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func exitIvi() -> Observable<Bool>{
        return hubHelper.setCommandWhithId(.IV_DEL_USER, D: ["domain" : getUser().domainId ] ).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func setLocale(_ lan: String) -> Observable<Bool>{
        let items: [(String, Int64)] = [ ("ru", 136), ("es", 148), ("de", 33), ("en", 37)]
        if let loc = items.first(where: { (i) -> Bool in return i.0 == lan }) {
            return self.hubHelper.setCommandWhithId(.SET_LOCALE, D: ["locale_id" : loc.1 ])
                .subscribeOn(ThreadUtil.shared.backScheduler)
                .observeOn(ThreadUtil.shared.mainScheduler)
        }
        return Observable.just(false).observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    func setHozOrgan(device: Int64, name: String,  index: Int64? = nil, sections: [Int64]? ) -> Observable<Bool> {
        var params: [String:Any] = ["name" : name]
        if index != nil { params.updateValue(index!, forKey: "index") }
        if sections != nil {  params.updateValue(sections!, forKey: "sections") }
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: [ "device" : device, "command" : ["USER_SET" : params ]])
        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
    func delHozOrgan(device: Int64, index: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["USER_DEL" : ["index" : index] ] ] )
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
    }
}

//HUBHELPER RESULT
extension DataManager{
    public func ROLES_GET(obj: HubUserRole){
        if obj.roles & Roles.ORG_INGEN != 0 || obj.roles & Roles.ORG_ADMIN != 0 || obj.roles & Roles.ORG_BUH != 0 || obj.roles & Roles.ORG_LAYER != 0 || obj.roles & Roles.ORG_BKS_OPER != 0{
            DispatchQueue.main.async {
                //TODO localization
                AlertUtil.warAlert("Вы зашли под меж-доменным инженером. Мы не гарантируем работоспособность приложания. Некоторый функционал для вас будет ограничен. В приложении не будут работать push-уведомления, из-за большого их количества!".localized())
            }
        }
        DataManager.defaultHelper.setUserInfo(id: obj.id, domainId: obj.domainId, orgId: obj.orgId, roles: obj.roles, domainLangIso: obj.locale.iso)
    }
    
    public func OPERATORS(obj: HubOperators, time : Int64){
        _ = Observable.from(obj.items)
            .map({ (obj) -> Operators in return CastHelper.operators(obj: obj, time: time )})
            .do(onNext: { (oper) in self.dbHelper.add(operators: oper) })
            .toArray()
            .asObservable()
            .do(onNext:{ r in return self.dbHelper.deleteOperators(time: time) })
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func OPERATOR_UPD(obj: HubOperator, time : Int64){
        _ = Observable.just(obj)
            .map({ (obj) -> Operators in return CastHelper.operators(obj: obj, time: time )})
            .do(onNext: { (oper) in self.dbHelper.add(operators: oper) })
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func OPERATOR_RMV(operatorId: Int64){
        ThreadUtil.shared.backOpetarion.sync { self.dbHelper.deleteOperators(operator_id: operatorId) }
    }
    
    public func CONNECTION_DROP(){
        RappleActivityIndicatorView.startAnimating()
        DataManager.disconnectDisposable = disconnect2()
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ a in
                DataManager.disconnectDisposable?.dispose()
                RappleActivityIndicatorView.stopAnimation()
                NavigationHelper.shared.show(XSplashController())
                AlertUtil.errorAlert("CONNECTION_DROP".localized())
            })
    }
    
    public func CLUSTERS(_ obj: HubClusters){ clusters = obj }
    
    public func IV_GET_RELATIONS(obj: HubRelations){
        relation = obj.items }
		
    public func ROLES_UPD(role: Int64){ }
    
    public func DEVICE_SITES(_ obj: HubDeviceSites){ }
    
    public func DEVICE_SITE_UPD(_ obj: HubDeviceSite){
        ThreadUtil.shared.backOpetarion.asyncAfter(deadline: .now() + 1, execute: {
            self.hubHelper.setCommand(.SITES)
        })
    }
    
    public func DEVICE_USER_UPD(_ obj: HubUser, time: Int64){
        _ = CastHelper.castUser(obj: obj, time: time)
            .do(onNext: { (sql) in
                if let deviceUsers = sql as? DeviceUsers{ self.dbHelper.add(deviceUsers: deviceUsers) }
                else if let userSection = sql as? UserSection{ self.dbHelper.add(userSection: userSection) }
            })
            .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func DEVICE_USER_RMV(_ obj: HubUser){
        ThreadUtil.shared.backOpetarion.sync { self.dbHelper.deleteDeviceUsers(device_id: obj.device_id, userIndex: obj.user_index) }
    }
    
    public func DEVICE_SITE_RMV(){
        nCenter.post(Notification(name:  NSNotification.Name("DashboardPrioritySiteIdUpdate")))
    }
    
    func FIREBASE_GET_SOUNDS(_ obj: [String:Any]) {
        
    }

    func OPTIONS(obj: HubOptions) {
        defer {
            nCenter.post(name: NSNotification.Name(rawValue: "HubOptionsEnd"), object: nil)
        }

        obj.modules.forEach { module in
            if
                module == "m-ivideon",
                let ivideon_url = obj.serverInfo.first(where: { $0.name == "iv_url" })?.value,
                let iv_app_id = obj.serverInfo.first(where: { $0.name == "iv_app_id" })?.value
            {
                XServerChanger.ivideon = (url: ivideon_url, id: iv_app_id)
                hubHelper.setCommand(.IV_GET_RELATIONS, D: ["device": 0, "section": 0, "zone": 0])
            } else if
                module == "m-fcm",
                let gcm_sender_id = obj.serverInfo.first(where: { $0.name == "gcm_sender_id" })?.value,
                let application_id = obj.serverInfo.first(where: { $0.name == "application_id_ios" })?.value,
                let api_key = obj.serverInfo.first(where: { $0.name == "api_key_ios" })?.value
            {
                XServerChanger.fcm = (gcm_sender_id: gcm_sender_id, application_id: application_id, api_key: api_key)
                if pushHelper == nil { pushHelper = PushHelper() }
            }
        }

    }
}
