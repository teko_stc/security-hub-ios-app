//
//  ConfigConstants.swift
//  SecurityHub
//
//  Created by Daniil on 14.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

class ConfigConstants {
    
    public static let hubVersion1: Int = 711
    public static let hubVersion2: Int = 777
    public static let hubVersion3: Int = 793
    public static let hubVersion4: Int = 727
    
    public static let delayList: [Int8] = [20, 30, 45, 60, 90, 120]
    
    public static let apnServer: String = "APN_SERVER"
    public static let apnLogin: String = "APN_LOGIN"
    public static let apnPassword: String = "APN_PASSWORD"
    
    public static let apn2Server: String = "APN2_SERVER"
    public static let apn2Login: String = "APN2_LOGIN"
    public static let apn2Password: String = "APN2_PASSWORD"
    
    public static let apnList: [String] = [apnServer, apnLogin, apnPassword, apn2Server, apn2Login, apn2Password]
    public static let ipList: [String] = [serverIp, gatewayIp, maskIp, staticIp, reserveIp]
    public static let tempList: [String] = [lowTemp1, highTemp1, lowTemp2, highTemp2, lowTemp3, highTemp3, lowTemp4, highTemp4, lowTemp5, highTemp5, lowTemp6, highTemp6, lowTemp7, highTemp7, lowTemp8, highTemp8]
    
    public static let pinCode: String = "CONFIG_PIN_CODE"
    public static let reservePort: String = "CONFIG_RESERVE_PORT"
    public static let mainPort: String = "CONFIG_MAIN_PORT"
    public static let radioOffset: String = "CONFIG_RADIO_OFFSET"
    public static let serverIp: String = "CONFIG_SERVER_IP"
    public static let gatewayIp: String = "CONFIG_GATEWAY_IP"
    public static let maskIp: String = "CONFIG_MASK_IP"
    public static let staticIp: String = "CONFIG_STATIC_IP"
    public static let reserveIp: String = "CONFIG_RESERVE_IP"
    public static let radioLiter: String = "CONFIG_RADIO_LITER"
    public static let waitReviewed: String = "CONFIG_WAIT_REVIEWED"
    public static let armServer: String = "CONFIG_ARM_SERVER"
    public static let disarmServer: String = "CONFIG_DISARM_SERVER"
    public static let roaming: String = "CONFIG_ROAMING"
    public static let entryDelay: String = "CONFIG_ENTRY_DELAY"
    public static let exitDelay: String = "CONFIG_EXIT_DELAY"
    
    public static let lowTemp1: String = "LOW_TEMP_1"
    public static let highTemp1: String = "HIGH_TEMP_1"
    public static let lowTemp2: String = "LOW_TEMP_2"
    public static let highTemp2: String = "HIGH_TEMP_2"
    public static let lowTemp3: String = "LOW_TEMP_3"
    public static let highTemp3: String = "HIGH_TEMP_3"
    public static let lowTemp4: String = "LOW_TEMP_4"
    public static let highTemp4: String = "HIGH_TEMP_4"
    public static let lowTemp5: String = "LOW_TEMP_5"
    public static let highTemp5: String = "HIGH_TEMP_5"
    public static let lowTemp6: String = "LOW_TEMP_6"
    public static let highTemp6: String = "HIGH_TEMP_6"
    public static let lowTemp7: String = "LOW_TEMP_7"
    public static let highTemp7: String = "HIGH_TEMP_7"
    public static let lowTemp8: String = "LOW_TEMP_8"
    public static let highTemp8: String = "HIGH_TEMP_8"
}
