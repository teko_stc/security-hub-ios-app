//
//  HubConfig3.swift
//  SecurityHub
//
//  Created by Daniil on 14.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class HubConfig3: HubConfig2 {
    
    override init(config: [Int8]) {
        super.init(config: config)
        
        processConfig()
    }
    
    private func processConfig() {
        lowTemp1 = String(config[0x307])
        highTemp1 = String(config[0x308])
        lowTemp2 = String(config[0x309])
        highTemp2 = String(config[0x30A])
        lowTemp3 = String(config[0x30B])
        highTemp3 = String(config[0x30C])
        lowTemp4 = String(config[0x30D])
        highTemp4 = String(config[0x30E])
        lowTemp5 = String(config[0x30F])
        highTemp5 = String(config[0x310])
        lowTemp6 = String(config[0x311])
        highTemp6 = String(config[0x312])
        lowTemp7 = String(config[0x313])
        highTemp7 = String(config[0x314])
        lowTemp8 = String(config[0x315])
        highTemp8 = String(config[0x316])
        
        updateLists()
    }
    
    override func setConfigValue(_ configItem: String, value: String) {
        let intValue = Int8(value) ?? 0
        
        switch configItem {
        case ConfigConstants.lowTemp1:
            config[0x307] = intValue
        case ConfigConstants.highTemp1:
            config[0x308] = intValue
        case ConfigConstants.lowTemp2:
            config[0x309] = intValue
        case ConfigConstants.highTemp2:
            config[0x30A] = intValue
        case ConfigConstants.lowTemp3:
            config[0x30B] = intValue
        case ConfigConstants.highTemp3:
            config[0x30C] = intValue
        case ConfigConstants.lowTemp4:
            config[0x30D] = intValue
        case ConfigConstants.highTemp4:
            config[0x30E] = intValue
        case ConfigConstants.lowTemp5:
            config[0x30F] = intValue
        case ConfigConstants.highTemp5:
            config[0x310] = intValue
        case ConfigConstants.lowTemp6:
            config[0x311] = intValue
        case ConfigConstants.highTemp6:
            config[0x312] = intValue
        case ConfigConstants.lowTemp7:
            config[0x313] = intValue
        case ConfigConstants.highTemp7:
            config[0x314] = intValue
        case ConfigConstants.lowTemp8:
            config[0x315] = intValue
        case ConfigConstants.highTemp8:
            config[0x316] = intValue
        default:
            return
        }
        
        processConfig()
    }
}
