//
//  HubConfig1.swift
//  SecurityHub
//
//  Created by Daniil on 11.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class HubConfig1: BaseConfig {
    
    init(config: [Int8]) {
        super.init()
        
        self.config = config
        
        processConfig()
    }
    
    private func processConfig() {
        let flags = BytesUtils.int8ArrayToUInt8(getArraySlice(offset: 0x08, size: 4))
        
        apnServer = BytesUtils.decodeUtf8(getArraySlice(offset: 0x274, size: 32))
        apnLogin = BytesUtils.decodeUtf8(getArraySlice(offset: 0x294, size: 16))
        apnPassword = BytesUtils.decodeUtf8(getArraySlice(offset: 0x2A4, size: 16))
        
        pinCode = String(BytesUtils.byteArrayToInt(BytesUtils.invertBytes(getArraySlice(offset: 0x2B6, size: 4))))
        reservePort = String(BytesUtils.byteArrayToInt(BytesUtils.invertBytes(getArraySlice(offset: 0x18, size: 2))))
        mainPort = String(BytesUtils.byteArrayToInt(BytesUtils.invertBytes(getArraySlice(offset: 0x12, size: 2))))
        radioOffset = String(BytesUtils.byteArrayToInt16(BytesUtils.invertBytes(getArraySlice(offset: 0x0C, size: 2))))
        serverIp = getIpFromBytesArray(getArraySlice(offset: 0x0E, size: 4))
        gatewayIp = getIpFromBytesArray(getArraySlice(offset: 0x2C2, size: 4))
        maskIp = getIpFromBytesArray(getArraySlice(offset: 0x2BE, size: 4))
        staticIp = getIpFromBytesArray(getArraySlice(offset: 0x2BA, size: 4))
        reserveIp = getIpFromBytesArray(getArraySlice(offset: 0x14, size: 4))
        radioLiter = getFlagState(flags[0], shift: 5)
        waitReviewed = getFlagState(flags[0], shift: 7)
        armServer = getFlagState(flags[1], shift: 0)
        disarmServer = getFlagState(flags[1], shift: 1)
        roaming = getFlagState(flags[1], shift: 2)
        entryDelay = String(config[0x2B4])
        exitDelay = String(config[0x2B5])
        
        updateLists()
    }
    
    override func setConfigValue(_ configItem: String, value: String) {
        switch configItem {
        case ConfigConstants.apnServer:
            config.replaceSubrange(0x274..<0x274 + 32, with: BytesUtils.stringToBytesArray(value, arraySize: 32))
        case ConfigConstants.apnLogin:
            config.replaceSubrange(0x294..<0x294 + 16, with: BytesUtils.stringToBytesArray(value, arraySize: 16))
        case ConfigConstants.apnPassword:
            config.replaceSubrange(0x2A4..<0x2A4 + 16, with: BytesUtils.stringToBytesArray(value, arraySize: 16))
        case ConfigConstants.pinCode:
            config.replaceSubrange(0x2B6..<0x2B6 + 4, with: BytesUtils.intToBytesArray(value.toInt() ?? 0, arraySize: 4))
        case ConfigConstants.reservePort:
            config.replaceSubrange(0x18..<0x18 + 2, with: BytesUtils.intToBytesArray(value.toInt() ?? 0, arraySize: 2))
        case ConfigConstants.mainPort:
            config.replaceSubrange(0x12..<0x12 + 2, with: BytesUtils.intToBytesArray(value.toInt() ?? 0, arraySize: 2))
        case ConfigConstants.radioOffset:
            config.replaceSubrange(0x0C..<0x0C + 2, with: BytesUtils.intToBytesArray(value.toInt() ?? 0, arraySize: 2))
        case ConfigConstants.serverIp:
            config.replaceSubrange(0x0E..<0x0E + 4, with: ipStringToBytesArray(value))
        case ConfigConstants.gatewayIp:
            config.replaceSubrange(0x2C2..<0x2C2 + 4, with: ipStringToBytesArray(value))
        case ConfigConstants.maskIp:
            config.replaceSubrange(0x2BE..<0x2BE + 4, with: ipStringToBytesArray(value))
        case ConfigConstants.staticIp:
            config.replaceSubrange(0x2BA..<0x2BA + 4, with: ipStringToBytesArray(value))
        case ConfigConstants.reserveIp:
            config.replaceSubrange(0x14..<0x14 + 4, with: ipStringToBytesArray(value))
        case ConfigConstants.entryDelay:
            config[0x2B4] = Int8(value) ?? ConfigConstants.delayList.first!
        case ConfigConstants.exitDelay:
            config[0x2B5] = Int8(value) ?? ConfigConstants.delayList.first!
        default:
            return
        }
        
        processConfig()
    }
    
    override func setConfigValue(_ configItem: String, value: Bool) {
        switch configItem {
        case ConfigConstants.radioLiter:
            changeFlagState(offset: 0x08, shift: 5, value: value)
        case ConfigConstants.waitReviewed:
            changeFlagState(offset: 0x08, shift: 7, value: value)
        case ConfigConstants.armServer:
            changeFlagState(offset: 0x08 + 1, shift: 0, value: value)
        case ConfigConstants.disarmServer:
            changeFlagState(offset: 0x08 + 1, shift: 1, value: value)
        case ConfigConstants.roaming:
            changeFlagState(offset: 0x08 + 1, shift: 2, value: value)
        default:
            return
        }
        
        processConfig()
    }
}
