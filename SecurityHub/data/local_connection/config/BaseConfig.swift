//
//  BaseConfig.swift
//  SecurityHub
//
//  Created by Daniil on 11.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class BaseConfig {
    
    public var config: [Int8] = []
    
    public var apnServer: String = ""
    public var apnLogin: String = ""
    public var apnPassword: String = ""
    
    public var apn2Server: String? = nil
    public var apn2Login: String? = nil
    public var apn2Password: String? = nil
    public var apn2List: [String?] = []
    
    public var pinCode: String = ""
    public var reservePort: String = ""
    public var mainPort: String = ""
    public var radioOffset: String = ""
    public var serverIp: String = ""
    public var gatewayIp: String = ""
    public var maskIp: String = ""
    public var staticIp: String = ""
    public var reserveIp: String = ""
    public var radioLiter: Bool = false
    public var waitReviewed: Bool = true
    public var armServer: Bool = true
    public var disarmServer: Bool = true
    public var roaming: Bool = true
    public var entryDelay: String = ""
    public var exitDelay: String = ""
    
    public var lowTemp1: String? = nil
    public var highTemp1: String? = nil
    public var lowTemp2: String? = nil
    public var highTemp2: String? = nil
    public var lowTemp3: String? = nil
    public var highTemp3: String? = nil
    public var lowTemp4: String? = nil
    public var highTemp4: String? = nil
    public var lowTemp5: String? = nil
    public var highTemp5: String? = nil
    public var lowTemp6: String? = nil
    public var highTemp6: String? = nil
    public var lowTemp7: String? = nil
    public var highTemp7: String? = nil
    public var lowTemp8: String? = nil
    public var highTemp8: String? = nil
    public var tempList: [String?] = []
    
    public func setConfigValue(_ configItem: String, value: String) { }
    public func setConfigValue(_ configItem: String, value: Bool) { }
    
    public func updateLists() {
        apn2List = [apn2Server, apn2Login, apn2Password]
        tempList = [lowTemp1, highTemp1, lowTemp2, highTemp2, lowTemp3, highTemp3, lowTemp4, highTemp4, lowTemp5, highTemp5, lowTemp6, highTemp6, lowTemp7, highTemp7, lowTemp8, highTemp8]
    }
    
    public func getArraySlice(offset: Int, size: Int) -> [Int8] {
        return [Int8] (config[offset..<offset + size])
    }
    
    public func getIpFromBytesArray(_ array: [Int8]) -> String {
        let array = BytesUtils.int8ArrayToUInt8(array)
        
        return "\(array[0] & 0xFF).\(array[1] & 0xFF).\(array[2] & 0xFF).\(array[3] & 0xFF)"
    }
    
    public func getFlagState(_ byte: UInt8, shift: UInt8) -> Bool {
        return (1 << shift & byte) > 0
    }
    
    public func changeFlagState(offset: Int, shift: Int8, value: Bool) {
        let mask: Int8 = (1 << shift)
        
        config[offset] = value ? (config[offset] | mask) : (config[offset] & ~mask)
    }
    
    public func ipStringToBytesArray(_ string: String) -> [Int8] {
        return BytesUtils.uInt8ArrayToInt8(string.components(separatedBy: ".").map { UInt8($0) ?? 0 })
    }
}
