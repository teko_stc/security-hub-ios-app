//
//  HubConfig4.swift
//  SecurityHub
//
//  Created by Daniil on 14.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class HubConfig4: HubConfig1 {
    
    override init(config: [Int8]) {
        super.init(config: config)
        
        processConfig()
    }
    
    private func processConfig() {
        lowTemp1 = String(config[0x2C6])
        highTemp1 = String(config[0x2C7])
        lowTemp2 = String(config[0x2C8])
        highTemp2 = String(config[0x2C9])
        lowTemp3 = String(config[0x2CA])
        highTemp3 = String(config[0x2CB])
        lowTemp4 = String(config[0x2CC])
        highTemp4 = String(config[0x2CD])
        lowTemp5 = String(config[0x2CE])
        highTemp5 = String(config[0x2CF])
        lowTemp6 = String(config[0x2D0])
        highTemp6 = String(config[0x2D1])
        lowTemp7 = String(config[0x2D2])
        highTemp7 = String(config[0x2D3])
        lowTemp8 = String(config[0x2D4])
        highTemp8 = String(config[0x2D5])
        
        updateLists()
    }
    
    override func setConfigValue(_ configItem: String, value: String) {
        let intValue = Int8(value) ?? 0
        
        switch configItem {
        case ConfigConstants.lowTemp1:
            config[0x2C6] = intValue
        case ConfigConstants.highTemp1:
            config[0x2C7] = intValue
        case ConfigConstants.lowTemp2:
            config[0x2C8] = intValue
        case ConfigConstants.highTemp2:
            config[0x2C9] = intValue
        case ConfigConstants.lowTemp3:
            config[0x2CA] = intValue
        case ConfigConstants.highTemp3:
            config[0x2CB] = intValue
        case ConfigConstants.lowTemp4:
            config[0x2CC] = intValue
        case ConfigConstants.highTemp4:
            config[0x2CD] = intValue
        case ConfigConstants.lowTemp5:
            config[0x2CE] = intValue
        case ConfigConstants.highTemp5:
            config[0x2CF] = intValue
        case ConfigConstants.lowTemp6:
            config[0x2D0] = intValue
        case ConfigConstants.highTemp6:
            config[0x2D1] = intValue
        case ConfigConstants.lowTemp7:
            config[0x2D2] = intValue
        case ConfigConstants.highTemp7:
            config[0x2D3] = intValue
        case ConfigConstants.lowTemp8:
            config[0x2D4] = intValue
        case ConfigConstants.highTemp8:
            config[0x2D5] = intValue
        default:
            return
        }
        
        processConfig()
    }
}
