//
//  LocalConnection.swift
//  SecurityHub
//
//  Created by Daniil on 14.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import CocoaAsyncSocket

class LocalConnection: NSObject {
    
    public static var instance: LocalConnection!
    
    public static let broadcastAddress: String = "255.255.255.255"
    public static let defaultPort: UInt16 = 22239
    
    private let defaultTimeout: Double = 10.0
    
    private var socket: GCDAsyncUdpSocket!
    
    private var port: UInt16
    private var debugEnabled: Bool = false
    private var receiveHandler: ((_ data: Data, _ host: String) -> Void)?
    private var errorHandler: ((_ error: String) -> Void)?
    private var closeHandler: ((_ error: String) -> Void)?
    
    init(port: UInt16) {
        self.port = port
        
        super.init()
        createSocket()
    }
    
    public func createSocket() {
        socket = GCDAsyncUdpSocket(delegate: self, delegateQueue: .global())
        socket.setPreferIPv4()
        socket.setIPv6Enabled(false)
        
        do {
            try socket.enableBroadcast(true)
            try socket.enableReusePort(true)
            try socket.bind(toPort: port)
            try socket.beginReceiving()
        } catch let socketError {
            print("Error creating socket: \(socketError.localizedDescription)")
        }
    }
    
    public func closeSocket() {
        socket.close()
    }
    
    public func isClosed() -> Bool {
        return socket.isClosed()
    }
    
    public func sendData(host: String, data: Data, timeout: TimeInterval? = nil) {
        socket.send(
            data,
            toHost: host, port: self.port,
            withTimeout: timeout ?? self.defaultTimeout,
            tag: -1
        )
    }
    
    public func getHost(address: Data) -> String {
        var host: NSString? = nil
        var port: UInt16 = 0
        
        GCDAsyncUdpSocket.getHost(&host, port: &port, fromAddress: address)
        
        return (host ?? "0.0.0.0") as String
    }
    
    public func setHandlers(receiveHandler: ((_ data: Data, _ host: String) -> Void)? = nil, errorHandler: ((_ error: String) -> Void)? = nil, closeHandler: ((_ error: String) -> Void)? = nil) {
        self.receiveHandler = receiveHandler
        self.errorHandler = errorHandler
        self.closeHandler = closeHandler
    }
    
    public func debugEnabled(_ enabled: Bool) {
        self.debugEnabled = enabled
    }
}

extension LocalConnection: GCDAsyncUdpSocketDelegate {    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didConnectToAddress address: Data) {
        if debugEnabled {
            print("Socket Message: didConnectToAddress")
        }
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotConnect error: Error?) {
        if debugEnabled {
            print("Socket Error: didNotConnect (Description: \(error?.localizedDescription ?? "Unknown error"))")
        }
        
        errorHandler?(error?.localizedDescription ?? "Unknown error")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didSendDataWithTag tag: Int) {
        if debugEnabled {
            print("Socket Message: didSendDataWithTag")
        }
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotSendDataWithTag tag: Int, dueToError error: Error?) {
        if debugEnabled {
            print("Socket Error: didNotSendDataWithTag (Description: \(error?.localizedDescription ?? "Unknown error"))")
        }
        
        errorHandler?(error?.localizedDescription ?? "Unknown error")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didReceive data: Data, fromAddress address: Data, withFilterContext filterContext: Any?) {
        if debugEnabled {
            print("Socket Message: didReceive")
        }
        
        let sender = getHost(address: address)
        receiveHandler?(data, sender)
    }
    
    func udpSocketDidClose(_ sock: GCDAsyncUdpSocket, withError error: Error?) {
        if debugEnabled {
            print("Socket Message: didClose (Description: \(error?.localizedDescription ?? "Unknown error"))")
        }
        
        closeHandler?(error?.localizedDescription ?? "Unknown error")
    }
}
