//
//  LocalDevice.swift
//  SecurityHub
//
//  Created by Daniil on 16.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

struct LocalDevice {
    let name: String
    let serialNumber: String
    let ip: String
    
    var pin: Int = 0
}
