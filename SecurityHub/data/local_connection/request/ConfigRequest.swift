//
//  ConfigRequest.swift
//  SecurityHub
//
//  Created by Daniil on 14.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class ConfigRequest {
    
    public static let command: Int8 = 4
    
    private var bytesArray: [Int8] = []
    
    init(pin: Int) {
        bytesArray.append(contentsOf: BytesUtils.intToBytesArray(pin, arraySize: 4))
        bytesArray.append(ConfigRequest.command)
    }
    
    public func toData() -> Data {
        return Data(BytesUtils.int8ArrayToUInt8(bytesArray))
    }
}
