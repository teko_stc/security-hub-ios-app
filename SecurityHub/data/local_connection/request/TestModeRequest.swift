//
//  TestModeRequest.swift
//  SecurityHub
//
//  Created by Daniil on 24.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class TestModeRequest {
    
    public static let command: Int8 = 1
    
    private var bytesArray: [Int8] = []
    
    init(pin: Int, state: TestModeState) {
        bytesArray.append(contentsOf: BytesUtils.intToBytesArray(pin, arraySize: 4))
        bytesArray.append(TestModeRequest.command)
        bytesArray.append(state == TestModeState.on ? 1 : 0)
    }
    
    public func toData() -> Data {
        return Data(BytesUtils.int8ArrayToUInt8(bytesArray))
    }
    
    enum TestModeState {
        case on
        case off
    }
}
