//
//  SendConfigRequest.swift
//  SecurityHub
//
//  Created by Daniil on 23.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class SetConfigRequest {
    
    private let command: Int8 = 5
    
    private var bytesArray: [Int8] = []
    
    init(pin: Int, config: [Int8]) {
        bytesArray.append(contentsOf: BytesUtils.intToBytesArray(pin, arraySize: 4))
        bytesArray.append(command)
        bytesArray.append(contentsOf: config)
    }
    
    public func toData() -> Data {
        return Data(BytesUtils.int8ArrayToUInt8(bytesArray))
    }
}
