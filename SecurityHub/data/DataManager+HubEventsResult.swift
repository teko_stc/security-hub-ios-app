//
//  DataManager+HubEventsResult.swift
//  SecurityHub
//
//  Created by Timerlan on 10/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//


import RxSwift

extension DataManager {
    func EVENTS(_ obj: HubEvents, time: Int64) {
				isEventsLoading = true
        var key: CompositeDisposable.DisposeKey? = nil
        let obs = Observable.from(obj.items)
            .filter({ (hubEvent) -> Bool in return self.dbHelper.isEventInDb(hubEvent.id) == false })
            .do(onNext: { (hubEvent) in self.eventUpdate(hubEvent, isNew: false) })
            .do(onCompleted: { self.checkVideos(obj) })
            .do(onCompleted: {
							self.dispose(key)
							self.isEventsLoading = false
							DataManager.shared.nCenter.post(name: HubNotification.eventsLoadEnd, object: nil) })
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
        key = disposables.insert(obs.subscribe())
    }
    
    func GET_ALL_AFFECTS(_ objs: HubEvents, time : Int64) {
				isEventsLoading = true
        DispatchQueue.global(qos: .utility).async {
            var relations: [DAffectRelation] = []
            //TODO подумать насколько корректно \/
            self.dbHelper.clearAffects().forEach { r in
                if (r.sectionId == 0 && (r.zoneId == 0 || r.zoneId == 100) && r._class == HubConst.CLASS_MALFUNCTION && r.reason == HubConst.REASON_MALF_LOST_CONNECTION) {
                    self.dbHelper.getZones(device_id: r.deviceId).forEach({ z in
                        if !relations.contains(where: { $0.deviceId == z.device && $0.sectionId == z.section && $0.zoneId == z.zone }) {
                            relations.append(DAffectRelation(deviceId: z.device, sectionId: z.section, zoneId: z.zone))
                        }
                    })
                }
            }
            for obj in objs.items {
                let r = self.oldAffectUpdate(obj)
                if !relations.contains(where: { (_r) -> Bool in return _r.deviceId == r.deviceId && _r.sectionId == r.sectionId && _r.zoneId == r.zoneId}) {
                    relations.append(r)
                }
                //TODO подумать насколько корректно \/
                if (obj.section == 0 && (obj.zone == 0 || obj.zone == 100) && obj.classField == HubConst.CLASS_MALFUNCTION && obj.reason == HubConst.REASON_MALF_LOST_CONNECTION) {
                    self.dbHelper.getZones(device_id: obj.device).forEach({ z in
                        if !relations.contains(where: { $0.deviceId == z.device && $0.sectionId == z.section && $0.zoneId == z.zone }) {
                            relations.append(DAffectRelation(deviceId: z.device, sectionId: z.section, zoneId: z.zone))
                        }
                    })
                }
            }
            self.sendAllAffect(relations)
            DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "HubEventsEnd"), object: nil)
        }
    }
    
    func AFFECT_RMV(_ obj: HubAffecrRmv) {
        DispatchQueue.global(qos: .utility).async {
            if let r = self.dbHelper.clearAffect(obj.id) {
                var relations = [DAffectRelation(deviceId: r.deviceId, sectionId: r.sectionId, zoneId: r.zoneId)]
                //TODO подумать насколько корректно \/
                if (r.sectionId == 0 && (r.zoneId == 0 || r.zoneId == 100) && r._class == HubConst.CLASS_MALFUNCTION && r.reason == HubConst.REASON_MALF_LOST_CONNECTION) {
                    self.dbHelper.getZones(device_id: r.deviceId).forEach({ z in
                        if !relations.contains(where: { $0.deviceId == z.device && $0.sectionId == z.section && $0.zoneId == z.zone }) {
                            relations.append(DAffectRelation(deviceId: z.device, sectionId: z.section, zoneId: z.zone))
                        }
                    })
                }
                self.sendAllAffect(relations)
            } else { self.AFFECT_RMV(obj) }
        }
    }
    
    func EVENT(_ obj: HubEvent) {
        var key: CompositeDisposable.DisposeKey? = nil
        let obs = Observable.just(obj)
            .do(onNext: { (hubEvent) in self.eventUpdate(hubEvent, isNew: true) })
            .do(onNext: { (hubEvent) in self.sendAffect(hubEvent) })
            .do(onNext: { (hubEvent) in self.checkVideo(hubEvent) })
            .do(onCompleted: { self.dispose(key) })
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
        key = disposables.insert(obs.subscribe())
    }
    
    //TODO NEED
    private func sendNotifications(_ event: Events){
        if event._class == 5 && event.reason == 26 && event.active == 2,
            let zi = dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone){
            NotificationCenter.default.post(name: HubNotification.navigation, object: NNotReadyZone(zi: zi, affect_decs: event.affect_desc))
            NotificationCenter.default.post(name: HubNotification.notReadyZone, object: NNotReadyZone(zi: zi, affect_decs: event.affect_desc))
        }else if event._class == 5 && event.reason == 23 && event.active == 1 {
            NotificationCenter.default.post(name: HubNotification.INTERACTIVE_MODE_START(event.device), object: event)
        }else if event._class == 5 && event.reason == 23 && event.active == 0 {
            NotificationCenter.default.post(name: HubNotification.INTERACTIVE_MODE_END(event.device), object: event)
        }
    }
    
    private func checkArm(_ event: Events) {
        guard event._class == 4, let section_ids = event.jdata.toJson()["sections"] as? [Int64] else { return }
        var relations: [DAffectRelation] = []
        let arm = event.active == 1 ? event.id : 0
        for section in dbHelper.setArmSections(arm: arm, device_id: event.device, section_ids: section_ids, time: event.time) {
            relations.append(DAffectRelation(deviceId: event.device, sectionId: section, zoneId: 0))
            relations.append(DAffectRelation(deviceId: event.device, sectionId: 0, zoneId: 0))
            relations.append(DAffectRelation(deviceId: event.device, sectionId: 0, zoneId: 100))
            for zone in dbHelper.getZoneIds(device_id: event.device, section_id: section) {
                relations.append(DAffectRelation(deviceId: event.device, sectionId: section, zoneId: zone))
            }
        }
        sendAllAffect(relations)
    }
    
    //TODO NEED IVIDION REFACT
    private func checkVideos(_ events: HubEvents) {
        events.items.filter { event -> Bool in return event.classField == 15 }.forEach { event in checkVideo(event) }
    }
    private func checkVideo(_ event: HubEvent) {
        if (event.classField == 15 && event.detector == 0 && event.reason == 1),
            let url = event.jdata.toJson()["url"] as? String,
            let video_event_id = event.jdata.toJson()["event_id"] as? Int64,
            let video_event = DataManager.shared.getEvent(id: video_event_id),
            let rel = DataManager.shared.relation.first(where: { (hr) -> Bool in return hr.device == video_event.device && hr.section == video_event.section && hr.zone == video_event.zone })
        {
            DataManager.shared.addIvVideo(IvVideo(camId: rel.camId, eventId: video_event_id, result: url))
        } else if (event.classField == 15 && event.detector == 0 && event.reason == 2),
            let video_event_id = event.jdata.toJson()["event_id"] as? Int64,
            let notification = event.jdata.toJson()["notification"] as? String,
            let video_event = DataManager.shared.getEvent(id: video_event_id),
            let rel = DataManager.shared.relation.first(where: { (hr) -> Bool in return hr.device == video_event.device && hr.section == video_event.section && hr.zone == video_event.zone })
        {
            DataManager.shared.addIvVideo(IvVideo(camId: rel.camId, eventId: video_event_id, result: notification, isError: true))
        }
    }
    
    private func sendAffect(_ hubEvent: HubEvent) {
        if hubEvent.affect != 1 { return }
        var relations = [DAffectRelation(deviceId: hubEvent.device, sectionId: hubEvent.section, zoneId: hubEvent.zone)]
        //TODO подумать насколько корректно \/
        if (hubEvent.section == 0 && (hubEvent.zone == 0 || hubEvent.zone == 100) && hubEvent.classField == HubConst.CLASS_MALFUNCTION && hubEvent.reason == HubConst.REASON_MALF_LOST_CONNECTION) {
            self.dbHelper.getZones(device_id: hubEvent.device).forEach({ z in
                if !relations.contains(where: { $0.deviceId == z.device && $0.sectionId == z.section && $0.zoneId == z.zone }) {
                    relations.append(DAffectRelation(deviceId: z.device, sectionId: z.section, zoneId: z.zone))
                }
            })
        }
        NotificationCenter.default.post(name: NSNotification.Name("Affect"), object: hubEvent)
        sendAllAffect(relations)
    }
    
    private func sendAllAffect(_ relations: [DAffectRelation]){
        var relationSections: [DAffectRelation] = []
        var relationDevice: [DAffectRelation] = []
        var siteIds: Set<Int64> = []
        for r in relations {
            self.sendZoneAffect(relation: r)
            if !relationSections.contains(where: { (_r) -> Bool in return _r.deviceId == r.deviceId && _r.sectionId == r.sectionId}) {
                dbHelper.getSiteIds(device_id: r.deviceId, section_id: r.sectionId).forEach { (id) in siteIds.insert(id) }
                relationSections.append(r)
                sendSectionAffect(relation: r)
                if !relationDevice.contains(where: { (_r) -> Bool in return _r.deviceId == r.deviceId}) {
                    relationDevice.append(r)
                    sendDeviceAffect(relation: r)
                }
            }
        }
        for site_id in siteIds { sendSiteAffect(site_id: site_id) }
    }
    
    private func sendSiteAffect(site_id: Int64){
        guard let site = dbHelper.getSite(site_id: site_id) else { return }
        nCenter.post(name: HubNotification.siteUpdate, object: DataEntityMapper.nSite(site, .update))
    }
    
    private func sendDeviceAffect(relation r: DAffectRelation){
        guard let di = dbHelper.getDevice(device_id: r.deviceId) else { return }
        nCenter.post(name: HubNotification.deviceUpdate, object: DataEntityMapper.nDevice(di, .update))
    }
    
    private func sendSectionAffect(relation r: DAffectRelation){
        guard let seci = dbHelper.getSection(device_id: r.deviceId, section_id: r.sectionId) else { return }
        nCenter.post(name: HubNotification.sectionsUpdate, object: DataEntityMapper.nSection(seci, .update))
    }
    
    private func sendZoneAffect(relation r: DAffectRelation){
        guard let zi = dbHelper.getZone(device_id: r.deviceId, section_id: r.sectionId, zone_id: r.zoneId) else { return }
        nCenter.post(name: HubNotification.zonesUpdate, object: DataEntityMapper.nZone(zi, .update))
    }
    
    private func oldAffectUpdate(_ hubEvent: HubEvent) -> DAffectRelation {
        if !dbHelper.isEventInDb(hubEvent.id) { eventUpdate(hubEvent, isNew: false)}
        else if hubEvent.affect == 1 { dbHelper.setAffect(hubEvent.id) }
        return DAffectRelation(deviceId: hubEvent.device, sectionId: hubEvent.section, zoneId: hubEvent.zone)
    }
    
    private func eventUpdate(_ hubEvent: HubEvent, isNew: Bool) {
        if let command_id = hubEvent.jdata.toJson()["command_id"] as? Int64 {
            let single = hubHelper.getReference(command_id)
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .do(onSuccess: { name in
                    self.eventUpdate(hubEvent, command_name: name, isNew: isNew)
                })
            _ = disposables.insert(single.subscribe())
        } else {
            eventUpdate(hubEvent, command_name: nil, isNew: isNew)
        }
    }
    
    private func eventUpdate(_ e: HubEvent, command_name: String?, isNew: Bool) {
        let ev = d3Const.getEventType(_class: e.classField.int, detector: e.detector.int, reason: e.reason.int, statment: e.active.int)
        if let operator_id = e.jdata.toJson()["operator_id"] as? Int64,
           dbHelper.getOperatorName(operator_id) == nil,
           UserDefaults.standard.string(forKey: "operator_id_\(operator_id)") == nil
        {
            let single = hubHelper.getReferenceName(id: operator_id)
                .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .do(onSuccess: { name in
                    UserDefaults.standard.set(name, forKey: "operator_id_\(operator_id)")

                    let affect_desc = self.getEventDescription(jdata: e.jdata, class_desc: ev?.description, device_id: e.device, reason: e.reason, commandName: command_name)
                    let event = CastHelper.event(e, icon: ev?.icons.list ?? "ic_nil", iconBig: ev?.icons.big ?? "ic_nil", affect_desc: affect_desc)
                    if isNew {
                        self.checkArm(event)
                        self.sendNotifications(event)
                    }
                    self.dbHelper.add(event: event)
                })
            _ = disposables.insert(single.subscribe())

            return
        }
        
        let affect_desc = getEventDescription(jdata: e.jdata, class_desc: ev?.description, device_id: e.device, reason: e.reason, commandName: command_name)
        let event = CastHelper.event(e, icon: ev?.icons.list ?? "ic_nil", iconBig: ev?.icons.big ?? "ic_nil", affect_desc: affect_desc)
        if isNew {
            checkArm(event)
            sendNotifications(event)
            
            // Подумать
            if let high_temp = event.jdata.toJson()["high_temp"] as? Int64 {
                userDefaults().set("\(high_temp)", forKey: "high_temp_d_\(event.device)_s\(event.section)")
            }
            if let low_temp = event.jdata.toJson()["low_temp"] as? Int64 {
                userDefaults().set("\(low_temp)", forKey: "low_temp_d_\(event.device)_s\(event.section)")
            }
        }
        dbHelper.add(event: event)
    }
    
    public func getEventDescription(jdata: String, class_desc: String?, device_id: Int64, reason: Int64, commandName: String? = nil) -> String {
        var affect_desc: String = class_desc ?? ""
        while let start = affect_desc.firstIndex(of: "<"), let end = affect_desc.firstIndex(of: ">") {
            let start_i = affect_desc.distance(from: affect_desc.startIndex, to: start)
            var s = String(affect_desc[start..<affect_desc.index(after: end)])
            if let att_start = s.firstIndex(of: "{"), let att_end = s.firstIndex(of: "}") {
                let att = String(s[s.index(after: att_start)..<att_end])
                var value = jdata.toJson()[att]
                if value == nil && att == "reason_id" { value = reason as AnyObject }
                if value == nil && att == "user_id" {
                    value = reason as AnyObject
                }
                if let value = value {
                    let new_value = getEventValueForAttr(att: att, value: value, device_id: device_id, commandName: commandName)
                    s.removeSubrange(att_start..<s.index(after: att_end))
                    s.insert(contentsOf: new_value, at: att_start)
                    s.removeFirst()
                    s.removeLast()
                }else{ s = "" }
            } else { s = "" }
            affect_desc.removeSubrange(start..<affect_desc.index(after: end))
            affect_desc.insert(contentsOf: s, at: affect_desc.index(affect_desc.startIndex, offsetBy: start_i))
        }
        return affect_desc
    }
    
    private func getEventValueForAttr(att: String, value: AnyObject, device_id: Int64, commandName: String? = nil) -> String {
        var new_value = ""
        switch att {
        case "operator_id":
            if let operator_id = value as? Int64, let name = dbHelper.getOperatorName(operator_id) {
                new_value = name
            } else if let operator_id = value as? Int64, let name = UserDefaults.standard.string(forKey: "operator_id_\(operator_id)") {
                new_value = name
            }
        case "sections":
            if let ids = value as? [Int64], ids.count > 0 {
                ids.forEach { (id) in if let name = dbHelper.getSectionName(device_id: device_id, section_id: id) { new_value +=  name + ", " } }
            }
        case "delay":
            if let _delay = value as? Int64 {  new_value = "\( _delay == 0 ? 0 : 60 )" }
        case "user_id":
            if let id = value as? Int64, let name = dbHelper.getHozOrganName(device_id: device_id, user_index: id) {
                new_value = name
            }
        case "user":
            if let id = value as? Int64, let name = dbHelper.getHozOrganName(device_id: device_id, user_index: id) {
                new_value = "\(id) - \(name)"
            }
        case "reason_id":
            new_value = "\(value)";
        case "physic":
            if let physic = value as? Int64 { new_value = HubConst.getPhysicTypes(type: physic) }
        case "charging":
            if let charging = value as? Int64, let charging_text = HubConst.getСhargingText()[charging] { new_value = charging_text }
        case "reboot_reason":
            if let reboot_reason = value as? Int64, let reboot_text = HubConst.getRebootText()[reboot_reason] { new_value = reboot_text }
        case "status":
            if let status = value as? Int64 {
                if status > 0 { new_value = HubConst.getCommandResults(code: status) }
                else { new_value = HubConst.getCommandResultsNegative(code: status) }
            }
        case "command_id":
            if let command = commandName, let _ = value as? Int64 { new_value = "\(value) - \(command.localized())" }
        default:
            new_value = "\(value)";
        }
        return new_value
    }
}
