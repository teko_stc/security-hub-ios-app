//
//  RegistrationManager.swift
//  SecurityHub
//
//  Created by Stefan on 04.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation
import SwiftHTTP
import AuthenticationServices
import SafariServices

class RegistrationManager: NSObject, ASWebAuthenticationPresentationContextProviding
{
	private let errorStrings = [
		RegResultCode.ERR:"REG_ERR",
		RegResultCode.ERR_USER_EXISTS:"REG_ERR_USER_EXISTS",
		RegResultCode.ERR_DB:"REG_ERR_DB",
		RegResultCode.ERR_DOMAIN_EXISTS:"REG_ERR_DOMAIN_EXISTS",
		RegResultCode.ERR_WRONG_LOGIN:"REG_ERR_WRONG_LOGIN",
		RegResultCode.ERR_USER_PHONE_EXISTS:"REG_ERR_USER_PHONE_EXISTS",
		RegResultCode.ERR_WRONG_CODE:"REG_ERR_WRONG_CODE",
		RegResultCode.ERR_USER_PHONE_DOESNT_EXIST:"REG_ERR_USER_PHONE_DOESNT_EXIST",
		RegResultCode.ERR_CODE_EXISTS:"REG_ERR_CODE_EXISTS",
		RegResultCode.ERR_CURL:"REG_ERR_CURL",
		RegResultCode.ERR_BAN:"REG_ERR_BAN",
		RegResultCode.ERR_CODE_DOESNT_EXIST:"REG_ERR_CODE_DOESNT_EXIST",
		RegResultCode.ERR_CODE_EXPIRED:"REG_ERR_CODE_EXPIRED",
		RegResultCode.ERR_DEVICE_DOESNT_EXIST:"REG_ERR_DEVICE_DOESNT_EXIST",
		RegResultCode.ERR_WRONG_PIN:"REG_ERR_WRONG_PIN",
		RegResultCode.ERR_DEVICE_TAKEN:"REG_ERR_DEVICE_TAKEN",
		RegResultCode.ERR_DEVICE_BAN:"REG_ERR_DEVICE_BAN",
		RegResultCode.ERR_DEVICE_NOT_TAKEN:"REG_ERR_DEVICE_NOT_TAKEN",
		RegResultCode.ERR_TOKEN_NOT_FOUND:"REG_ERR_TOKEN_NOT_FOUND",
		RegResultCode.ERR_WRONG_ANSWERS:"REG_ERR_WRONG_ANSWERS",
		RegResultCode.ERR_LOGIN_MISMATCH:"REG_ERR_LOGIN_MISMATCH",
		RegResultCode.ERR_TOKEN_EXPIRED:"REG_ERR_TOKEN_EXPIRED",
		RegResultCode.ERR_TOKEN_BAN:"REG_ERR_TOKEN_BAN",
		RegResultCode.ERR_NOT_BINDED_TO_SITE:"REG_ERR_NOT_BINDED_TO_SITE",
		RegResultCode.ERR_BAD_CREDENTIALS:"REG_ERR_BAD_CREDENTIALS",
		RegResultCode.ERR_SOCIAL_MISMATCH:"REG_ERR_SOCIAL_MISMATCH",
		RegResultCode.ERR_SOCIAL_NOT_FOUND:"REG_ERR_SOCIAL_NOT_FOUND",
		RegResultCode.ERR_UNKNOWN:"REG_ERR_UNKNOWN"
	]
	
	private let KEY_ACTION = "action"
	private let KEY_TYPE = "type"
	private let KEY_DATA = "data"
	
	private let KEY_ACTION_REG = "reg"
	private let KEY_ACTION_REG_CODE = "reg_code"
	private let KEY_ACTION_REG_VALIDATE = "reg_validate"
	private let KEY_ACTION_RES = "res"
	private let KEY_ACTION_RES_CODE = "res_code"
	private let KEY_ACTION_RES_VALIDATE = "res_validate"
	
	private let KEY_TYPE_PHONE = "phone"
	private let KEY_TYPE_SN = "sn"
	private let KEY_TYPE_GOOGLE = "google"
	private let KEY_TYPE_VK = "vk"
	
	private let KEY_DATA_NAME = "name"
	private let KEY_DATA_LOGIN = "login"
	private let KEY_DATA_PASSWORD = "pass"
	private let KEY_DATA_EMAIL = "email"
	private let KEY_DATA_CODE = "code"
	private let KEY_DATA_TOKEN = "token"
	private let KEY_DATA_PHONE = "phone"
	private let KEY_DATA_SN = "sn"
	private let KEY_DATA_PIN = "pin"
	private let KEY_DATA_RESPONCE = "response"
	private let KEY_DATA_CERT = "credentials"
	private let KEY_DATA_TYPE = "client_type"
	
	private var pkcsKeys: PKCESecrets?
	private var vkSession: AnyObject?
	
	private let url = XTargetUtils.regServer+"/reg"
	private let testMode = false
//	private let testMode = true
	
	public func requestPhonePinCode (phone: UInt, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG_CODE,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[KEY_DATA_PHONE:phone]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func requestRecoveryPhonePinCode (phone: UInt, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES_CODE,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[KEY_DATA_PHONE:phone]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func validatePhonePinCode (phone: UInt, pinCode: UInt, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG_VALIDATE,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[KEY_DATA_PHONE:phone,KEY_DATA_CODE:pinCode]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func validateRecoveryPhonePinCode (phone: UInt, pinCode: UInt, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES_VALIDATE,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[KEY_DATA_PHONE:phone,KEY_DATA_CODE:pinCode]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func registrationUserPhone (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[
				KEY_DATA_PHONE:user.phone,
				KEY_DATA_CODE:user.code,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash,
				KEY_DATA_NAME:user.name]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func recoveryUserPhone (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES,
			 KEY_TYPE:KEY_TYPE_PHONE,
			 KEY_DATA:[
				KEY_DATA_PHONE:user.phone,
				KEY_DATA_CODE:user.code,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func validateDevicePinCode (serialNumber: UInt, pinCode: String, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG_VALIDATE,
			 KEY_TYPE:KEY_TYPE_SN,
			 KEY_DATA:[KEY_DATA_SN:serialNumber,KEY_DATA_PIN:pinCode]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func registrationUserDevice (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG,
			 KEY_TYPE:KEY_TYPE_SN,
			 KEY_DATA:[
				KEY_DATA_SN:user.sn,
				KEY_DATA_PIN:user.pinHash,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash,
				KEY_DATA_NAME:user.name]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func requestRecoveryTokenDevice (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES_CODE,
			 KEY_TYPE:KEY_TYPE_SN,
			 KEY_DATA:[
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_SN:user.sn,
				KEY_DATA_PIN:user.pinHash]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func validateRecoveryTokenDevice (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES_VALIDATE,
			 KEY_TYPE:KEY_TYPE_SN,
			 KEY_DATA:[
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_TOKEN:user.token,
				KEY_DATA_RESPONCE:[user.name,user.code]]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func recoveryUserDevice (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES,
			 KEY_TYPE:KEY_TYPE_SN,
			 KEY_DATA:[
				KEY_DATA_TOKEN:user.token,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func registrationUserGoogle (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG,
			 KEY_TYPE:KEY_TYPE_GOOGLE,
			 KEY_DATA:[
				KEY_DATA_TYPE:2,
				KEY_DATA_CERT:user.token,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash,
				KEY_DATA_NAME:user.name]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func recoveryUserGoogle (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES,
			 KEY_TYPE:KEY_TYPE_GOOGLE,
			 KEY_DATA:[
				KEY_DATA_TYPE:2,
				KEY_DATA_CERT:user.token,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func requestAccessTokenVK (completionHandler: @escaping (RegResponce)->Void )
	{
		do {
			let generator = PKCESecretsSHA256Generator()
			pkcsKeys = try generator.generateSecrets()
		} catch {
			var result = RegResponce()
			result.result = RegResultCode.ERR_UNKNOWN
			result.localizedMessage = "Error generate PKCE keys"
			completionHandler (result)
			return
		}
		
		if let pkcsKeys = pkcsKeys
		{
			let url = URL (string: "https://id.vk.com/auth?app_id="+REG_VK_APP_ID+"&v=0.0.2&redirect_uri=vk"+REG_VK_APP_ID+"://vk.com/blank.html&response_type=code&state="+pkcsKeys.state+"&code_challenge="+pkcsKeys.codeChallenge+"&code_challenge_method="+pkcsKeys.codeChallengeMethod.rawValue+"&scheme=bright_light&lang_id=0")!
			
			let callbackUrlScheme = "vk"+REG_VK_APP_ID
			
			if #available(iOS 12.0, *) {
				let session = ASWebAuthenticationSession (url: url, callbackURLScheme:callbackUrlScheme
				) { [weak self] callbackUrl, error in
					guard let self = self else { return }
					
					let session = self.vkSession as! ASWebAuthenticationSession
					session.cancel()
					self.vkSession = nil
					
					var result = self.handleUrlResponce (url: callbackUrl, error: error)
					
					if self.testMode
					{
						result.result = .SUCCESS
						completionHandler (result)
						return
					}

					if result.result != .SUCCESS {
						if let error = error as? NSError {
							if error.code == ASWebAuthenticationSessionError.canceledLogin.rawValue { result.result = .CANCEL } }
						completionHandler (result) }
					else
					{
						DispatchQueue.main.async {
							self.getAccessTokenVK (authCode: result.token, completionHandler: completionHandler) }
					}
				}
				if #available(iOS 13.0, *) { session.presentationContextProvider = self }
				self.vkSession = session
				session.start()
			// iOS 11
			} else {
				let session = SFAuthenticationSession (url: url, callbackURLScheme:callbackUrlScheme, completionHandler: { (callBack:URL?, error:Error? ) in
						guard error == nil, let callbackUrl = callBack else { return }
					
						let session = self.vkSession as! SFAuthenticationSession
						session.cancel()
						self.vkSession = nil

						var result = self.handleUrlResponce (url: callbackUrl, error: error)
						
						if result.result != .SUCCESS {
							if let error = error as? NSError {
								if error.code == SFAuthenticationError.canceledLogin.rawValue { result.result = .CANCEL } }
							completionHandler (result) }
						else
						{
							DispatchQueue.main.async {
								self.getAccessTokenVK (authCode: result.token, completionHandler: completionHandler) }
						}
					})

				vkSession = session
				session.start()
			}
		}
	}
	
	@available(iOS 12.0, *)
	func presentationAnchor (for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
		return UIApplication.shared.keyWindow!
	}
	
	private func handleUrlResponce (url: URL?, error: Error?) -> RegResponce
	{
		var result = RegResponce()
		
		if let error = error {
			result.result = RegResultCode.ERR_NET
			result.localizedMessage = error.localizedDescription
			
			return result
		}
		
		let parsingError = "VK "+"REG_ERR".localized()
		
		if let url = url, let pkcsKeys = pkcsKeys
		{
			let components = URLComponents(url: url, resolvingAgainstBaseURL: true)
			if let data = components?.queryItems?.first?.value?.data(using: .utf8)
			{
				do {
					if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
						if let oauth = json["oauth"] as? [String:Any] {
							if let authCode = oauth["code"] as? String { result.token = authCode }
							if let state = oauth["state"] as? String { result.message = state }
						}
						if result.token.count == 0 || result.message.count == 0 ||
								result.message != pkcsKeys.state
						{
							result.result = RegResultCode.ERR_NET
							result.localizedMessage = parsingError
						}
					}
				} catch {
					result.result = RegResultCode.ERR_JSON
					result.localizedMessage = parsingError
				}
			} else {
				result.result = RegResultCode.ERR_NET
				result.localizedMessage = parsingError
			}
		} else {
			result.result = RegResultCode.ERR_NET
			result.localizedMessage = parsingError
		}
		
		return result
	}
	
	private func getAccessTokenVK (authCode: String, completionHandler: @escaping (RegResponce)->Void )
	{
		if let pkcsKeys = pkcsKeys
		{
			let url = "https://oauth.vk.com/access_token?code="+authCode+"&code_verifier="+pkcsKeys.codeVerifier+"&client_id="+REG_VK_APP_ID+"&client_secret="+REG_VK_APP_SEC_KEY+"&grantType=code&redirect_uri=vk"+REG_VK_APP_ID+"://vk.com/blank.html"
			
			_ = HTTP.GET (url, parameters: nil, requestSerializer: JSONParameterSerializer())
			{ response in
				let result = self.handleResponce(responce: response)
				DispatchQueue.main.async {
					completionHandler (result) }
			}
		}
	}
	
	public func registrationUserVK (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_REG,
			 KEY_TYPE:KEY_TYPE_VK,
			 KEY_DATA:[
				"v":"5.199",
				KEY_DATA_TYPE:2,
				KEY_DATA_CERT:user.token,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash,
				KEY_DATA_NAME:user.name]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	public func recoveryUserVK (user: RegUserData, completionHandler: @escaping (RegResponce)->Void )
	{
		let parameters: [String : Any] =
			[KEY_ACTION:KEY_ACTION_RES,
			 KEY_TYPE:KEY_TYPE_VK,
			 KEY_DATA:[
				"v":"5.199",
				KEY_DATA_TYPE:2,
				KEY_DATA_CERT:user.token,
				KEY_DATA_LOGIN:user.login,
				KEY_DATA_PASSWORD:user.passwordHash]]
		
		return query(parameters: parameters, completionHandler: completionHandler)
	}
	
	private func query (parameters: [String:Any], completionHandler: @escaping (RegResponce)->Void )
	{
		let task = HTTP.POST (self.url, parameters: parameters, requestSerializer: JSONParameterSerializer())
		{ response in
			var result = self.handleResponce(responce: response)
			DispatchQueue.main.async {
				if self.testMode { result.result = RegResultCode.SUCCESS }
				completionHandler (result)
			}
		}
		
		var attempted = false
		task?.auth = { challenge in
			if !attempted
			{
				attempted = true
				if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust &&
						challenge.protectionSpace.host == "test.security-hub.ru"
				{
					return URLCredential (trust: challenge.protectionSpace.serverTrust!)
				}
			}
			return nil
		}
	}
	
	private func handleResponce (responce: Response) -> RegResponce
	{
		var result = RegResponce()
		
		result.result = RegResultCode.ERR_NET
		result.localizedMessage = "D3_ERR_GENERIC".localized()
		
		if let statusCode = responce.statusCode
		{
			if ( statusCode != 200 )
			{
				result.localizedMessage += " (\(responce.error?.localizedDescription ?? ("ERR_NET_STATUS: "+String(statusCode))))"
				return result
			}
			
			do
			{
				let json = try JSONSerialization.jsonObject(with: responce.data) as? [String: Any]
				if let code = json?["result"] as? Int
				{
//					print(responce.text ?? "")
					result.errorCode = code
					result.result = RegResultCode (rawValue: code) ?? RegResultCode.ERR_UNKNOWN
					
					if let value = json?["msg"] as? String { result.message = value }
					if let value = json?["token"] as? String { result.token = value }
					if let value = json?["timeout"] as? Int { result.timeout = value }
				}
				else
				{
					if let token = json?["access_token"] as? String
					{
						result.result = .SUCCESS
						result.token = token
						return result
					}
					
					result.localizedMessage += " (Invalid VK responce)"
					return result
				}
			} catch {
				result.localizedMessage += " (Invalid JSON format)"
				return result
			}
		}
		else
		{
			result.localizedMessage += " (\(responce.error?.localizedDescription ?? "Unknown status code"))"
			return result
		}
		
		if result.result != RegResultCode.SUCCESS {
			result.localizedMessage = errorStrings [result.result]?.localized() ?? ""
			if result.result == RegResultCode.ERR_UNKNOWN {
				result.localizedMessage += " \(String(result.errorCode))" }
			if result.timeout > 0 {
				result.localizedMessage += " "+String(result.timeout)
			}
		}
		
		return result
	}
	
	/*
	 let str2 = String(decoding: response.data, as: UTF8.self)
	 let str = str2.replacingOccurrences(of: "\n", with: "")
	 let hexes = response.data.map { String(format: "%02X", $0) }
	 print(hexes.joined(separator: ""))
	}*/
}
