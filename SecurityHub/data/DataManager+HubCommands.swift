//
//  DataManager+HubCommands.swift
//  SecurityHub
//
//  Created by Timerlan on 17/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension DataManager {
   
    func setSound(class_id: Int64, name: String) -> Single<DCommandResult> {
        let key =   class_id == 0 ? "alarm" :
                    class_id == 1 ? "sabotage" :
                    class_id == 2 ? "malfunction" :
                    class_id == 3 ? "attention" : "arm"
        sounds[key] = name
        let D: [String : Any] = [key : name]
        return hubHelper.setCommandWithResult(.FIREBASE_SET_SOUND, D: D)
    }
    
    func arm(device: Int64, section: Int64 = 0) -> Single<DCommandResult> {
        let D = "{\"device\": \(device), \"command\": {\"ARM\": {\"section\": \(section) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func disarm(device: Int64, section: Int64 = 0) -> Single<DCommandResult> {
        let D = "{\"device\": \(device), \"command\": {\"DISARM\": {\"section\": \(section) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func switchRelay(device: Int64, section: Int64, zone: Int64, state: Int64) -> Single<DCommandResult>  {
        let D = "{\"device\": \(device), \"command\": {\"SWITCH\": {\"section\": \(section), \"zone\": \(zone), \"state\": \(state) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func reset(device: Int64, section: Int64, zone: Int64) -> Single<DCommandResult>{
        let D = "{\"device\": \(device), \"command\": {\"RESET\": {\"section\": \(section), \"zone\": \(zone) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func rename(site_id: Int64, name: String) -> Single<DCommandResult> {
        let D: [String : Any] = ["id": site_id, "name": name]
        return hubHelper.setCommandWithResult(.SITE_SET, D: D)
    }
    
    func addSection(device_id: Int64, name: String? = nil,  index: Int64? = nil, type: Int64? = nil) -> Single<DCommandResult> {
        var text = ""
        if let name = name     { text = text + " \"name\": \"\(name)\", " }
        if let index = index   { text = text + " \"index\": \(index), " }
        if let type = type     { text = text + " \"type\": \(type), " }
        if text.count > 0 { text.removeLast(2) }
        let D = "{\"device\": \(device_id), \"command\": {\"SECTION_SET\": { \(text) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func addZone(device_id: Int64, uid: String, section: Int64?, name: String, delay: Int? = nil , detector: Int? = nil) -> Single<DCommandResult> {
        let delay = delay == nil ? "" : ", \"delay\": \(delay!)"
        let detector = detector == nil ? "" : ", \"detector\":\(detector!)"
        let section = section == nil ? "" : " , \"section\": \(section!)"
        let text = "\"uid\": \"\(uid)\" \(section), \"name\": \"\(name)\" \(delay) \(detector) "
        let D = "{\"device\": \(device_id), \"command\": {\"ZONE_SET\": { \(text) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func getLibraryScripts(config_version: Int64) -> Single<(result: DCommandResult, value: HubLibraryScripts?)> {
        if let value = scriptsLibrary[config_version] { return Single.just((result: DCommandResult.SUCCESS, value: value)) }
        let D = [ "config_version" : config_version ]
        return hubHelper.get(.LIBRARY_SCRIPTS, D: D)
            .do(onSuccess: { (r) in if let value = r.value { self.scriptsLibrary.updateValue(value, forKey: config_version) }})
    }
    
    func interactiveMode(device_id: Int64) -> Observable<DInteractiveModeResult> {
        let isStarted = dbHelper.get(_class: 5, reason: 23, active: 1, device_id: device_id)?.affect == 1
        let observableDeviceInInteractiveMode = Observable<DInteractiveModeResult>.create { (observer) -> Disposable in
            let timer_start = !isStarted ? Single<Int64>.timer(.seconds(5), scheduler: SerialDispatchQueueScheduler.init(qos: .utility))
                .subscribe { _ in observer.onNext(DInteractiveModeResult.NOT_START);observer.onCompleted() } : nil
            var timer_end = isStarted ? Single<Int64>.timer(.seconds(60), scheduler: SerialDispatchQueueScheduler.init(qos: .utility))
                .subscribe { _ in observer.onNext(DInteractiveModeResult.FINISH);observer.onCompleted() } : nil
            let o1 = self.nCenter.addObserver(forName: HubNotification.INTERACTIVE_MODE_START(device_id), object: nil, queue: OperationQueue.main){ _ in
                timer_start?.dispose()
                timer_end = Single<Int64>.timer(.seconds(60), scheduler: SerialDispatchQueueScheduler.init(qos: .utility))
                    .subscribe { _ in observer.onNext(DInteractiveModeResult.FINISH);observer.onCompleted() }
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.INTERACTIVE_MODE_END(device_id), object: nil, queue: OperationQueue.main){ n in
                guard let e = n.object as? Events else { return }
                if let uid_type = e.jdata.toJson()["uid_type"] as? Int64, let uid = e.jdata.toJson()["uid"] as? String {  observer.onNext(DInteractiveModeResult.FINISH(uid_type: uid_type, uid: uid)) } else { observer.onNext(DInteractiveModeResult.FINISH) }
                observer.onCompleted()
            }
            return Disposables.create { self.nCenter.removeObserver(o1);self.nCenter.removeObserver(o2);timer_start?.dispose();timer_end?.dispose() }
        }
        let D = "{\"device\": \(device_id), \"command\": {\"INTERACTIVE\": 1}}"
        var singleCommand = isAvailableDevice(device_id: device_id)
            .asObservable()
            .concatMap{ (result) -> Single<DCommandResult> in
                if result.success { return self.hubHelper.setCommandWithResult(D: D) }
                return Single.just(result)
            }.map { (result) -> DInteractiveModeResult in
                if result.success { return DInteractiveModeResult.START }
                return DInteractiveModeResult.ERROR(message: result.message)
            }
        if isStarted { singleCommand = Observable.just(DInteractiveModeResult.START) }
        return Observable.merge(observableDeviceInInteractiveMode, singleCommand)
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func addHozOrgan(device_id: Int64, uid: String? = nil, password: String? = nil, sections: [Int64], name: String) -> Single<DCommandResult> {
        var text = ""
        if let uid = uid { text += "\"uid\": \"\(uid)\", " }
        if let password = password { text += "\"password\": \"\(password)\", " }
        text += " \"sections\": \(sections), \"name\": \"\(name)\""
        let D = "{\"device\": \(device_id), \"command\": {\"USER_SET\": { \(text) }}}"
        return hubHelper.setCommandWithResult(D: D)
    }
    
    func addScript(device_id: Int64, bind: Int64, uid: String, params: [[String:Any]], program: String, name: String, extra: String) -> Single<DCommandResult> {
        let D: [String : Any] =
        [
            "device_id": device_id,
            "bind": bind,
            "program": program,
            "name": name,
            "params": params,
            "uid": uid,
            "extra": extra
        ]
        return setCommandWithResultToAvailableDevice(.SCRIPT_SET, D: D, device_id: device_id)
    }
    
    func deleteScript(device_id: Int64, bind: Int64) -> Single<DCommandResult> {
        let D: [String : Any] =
        [
            "device_id": device_id,
            "bind": bind,
        ]
        return setCommandWithResultToAvailableDevice(.SCRIPT_DEL, D: D, device_id: device_id)
    }
    
    func deviceUpdate(device_id: Int64) -> Observable<Bool>  {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device_id, "command" : ["UPDATE"]])
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }

    func zoneDelay(device_id: Int64, section_id: Int64, zone_id: Int64, delay: Int64) -> Observable<Bool>  {
        let delay = delay > 0 ? 0 : 1
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device_id, "command" : ["ZONE_SET": [ "section": section_id, "index": zone_id, "delay": delay ]]])
        .subscribeOn(ThreadUtil.shared.backScheduler)
    }

    func getIvideonToken() -> Observable<String?> {
        guard XTargetUtils.ivideon == nil else {
            return .just(nil)
        }

        return hubHelper.setCommandWhithResult(.IV_GET_TOKEN, D: ["domain": getUser().domainId ])
            .map{ (resp) -> String? in return resp.d["access_token"] as? String }
            .do(onNext: { s in if let s = s { DataManager.settingsHelper.ivideonToken = s }})
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func setCommandWithResultToAvailableDevice(_ command: HubCommandWithId, D: [String: Any], device_id: Int64) -> Single<DCommandResult> {
        return isAvailableDevice(device_id: device_id)
            .flatMap { (res) -> Single<DCommandResult> in
                guard res.success else { return Single.just(res) }
                return self.hubHelper.setCommandWithResult(command, D: D)
            }
    }
    
    @available(*, deprecated)
    func setFCM() {
        guard let token = pushHelper?.getFcmToken(), XServerChanger.fcm != nil else { return }
        let pushMask = DataManager.settingsHelper.pushMask
        var lang = 0
        switch DataManager.defaultHelper.language
        {
            case "en": lang = 37
            case "de": lang = 33
            case "es": lang = 148
            case "ru": lang = 136
            default: lang = 136
        }
        sounds = [:]
        hubHelper.setCommandLang(.FIREBASE_ID_SET, D: ["id": token, "class_mask": pushMask, "client_type" : 0, "lang": lang])
    }
    @available(*, deprecated)
    func deleteFCM() {
        guard let token = pushHelper?.getFcmToken(), XServerChanger.fcm != nil else { return }
        sounds = [:]
        hubHelper.setCommand(.FIREBASE_ID_DEL)
    }
}
