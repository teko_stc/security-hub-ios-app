//
//  HubHelperProtocols.swift
//  SecurityHub
//
//  Created by Timerlan on 23/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

protocol XHubInProtocol {
    func pushMask() -> Int
    func fcmToken() -> String
    func lastEventTime() -> Int64
    func getReferenceName(_ command_id: Int64) -> String?
    func language() -> Int
}

protocol XHubOutProtocol {
    func addReference(command_id: Int64, name: String)
}
