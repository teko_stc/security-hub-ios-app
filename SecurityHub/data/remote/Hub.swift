//
//  HubCommand.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import UIKit

enum HubCommand: String {
    case EVENT             = "EVENT"
    case EVENTS            = "EVENTS"
    case AFFECT_RMV        = "AFFECT_RMV"
    case SITES             = "SITES"
    case SITE_UPD          = "SITE_UPD"
    case SITE_RMV          = "SITE_RMV"
    case SECTION_ZONE_UPD  = "SECTION_ZONE_UPD"
    case SECTION_ZONE_RMV  = "SECTION_ZONE_RMV"
    case OPERATORS         = "OPERATORS"
    case OPERATOR_UPD      = "OPERATOR_UPD"
    case OPERATOR_RMV      = "OPERATOR_RMV"
    case ROLES_GET         = "ROLES_GET"
    case ROLES_UPD         = "ROLES_UPD"
    case CONNECTION_DROP   = "CONNECTION_DROP"
    case IV_GET_RELATIONS  = "IV_GET_RELATIONS"
    case COMMAND_RESULT_UPD = "COMMAND_RESULT_UPD"
    case CLUSTERS          = "CLUSTERS"
    case DEVICE_SITES      = "DEVICE_SITES"
    case DEVICE_SITE_UPD   = "DEVICE_SITE_UPD"
    case DEVICE_USER_RMV   = "DEVICE_USER_RMV"
    case DEVICE_USER_UPD   = "DEVICE_USER_UPD"
    case DEVICE_SITE_RMV   = "DEVICE_SITE_RMV"
    case COMMAND_SET       = "COMMAND_SET"
    case FIREBASE_ID_SET = "FIREBASE_ID_SET"
    case FIREBASE_ID_DEL = "FIREBASE_ID_DEL"
    case FIREBASE_GET_SOUNDS = "FIREBASE_GET_SOUNDS"
    case REFERENCES        = "REFERENCES"
    case SCRIPTS           = "SCRIPTS"
    case DEVICE_SCRIPT_UPD = "DEVICE_SCRIPT_UPD"
    case DEVICE_SCRIPT_RMV = "DEVICE_SCRIPT_RMV"
    case ALARM_ADD         = "ALARM_ADD"
    case ALARM_UPD         = "ALARM_UPD"
    case ALARM_RMV         = "ALARM_RMV"
    case OPTIONS           = "OPTIONS"
}

enum HubCommandWithId:String {
    case DEVICE_ASSIGN_DOMAIN = "DEVICE_ASSIGN_DOMAIN"
    case DEVICE_ASSIGN_SITE   = "DEVICE_ASSIGN_SITE"
    case DEVICE_REVOKE_DOMAIN = "DEVICE_REVOKE_DOMAIN"
    case DELEGATE_REQ      = "DELEGATE_REQ"
    case DELEGATE_REVOKE   = "DELEGATE_REVOKE"
    case ZONE_SET          = "ZONE_SET"
    case ZONE_NAME         = "ZONE_NAME"
    case ZONE_DEL          = "ZONE_DEL"
    case SITE_SET          = "SITE_SET"
    case SITE_DEL          = "SITE_DEL"
    case OPERATOR_SET      = "OPERATOR_SET"
    case OPERATOR_DEL      = "OPERATOR_DEL"
    case COMMAND_SET       = "COMMAND_SET"
    case IV_BIND_CAM       = "IV_BIND_CAM"
    case IV_DEL_RELATION   = "IV_DEL_RELATION"
    case IV_DEL_USER       = "IV_DEL_USER"
    case SET_LOCALE        = "SET_LOCALE"
    case FIREBASE_SET_SOUND = "FIREBASE_SET_SOUND"
    case SCRIPT_SET        = "SCRIPT_SET"
    case SCRIPT_DEL        = "SCRIPT_DEL"
    case ALARM_CLOSE       = "ALARM_CLOSE"
    case ALARM_OPEN        = "ALARM_OPEN"
    case FIREBASE_ID_DEL = "FIREBASE_ID_DEL"
    case EVENTS          = "EVENTS"
    case SOS             = "SEND_PANIC"
}

enum HubCommandWithResult:String {
    case DELEGATE_NEW           = "DELEGATE_NEW"
    case IV_GET_TOKEN           = "IV_GET_TOKEN"
    case OPERATOR_SAFE_SESSION  = "OPERATOR_SAFE_SESSION"
    case SITE_DELEGATES         = "SITE_DELEGATES"
    case LOCALES                = "LOCALES"
    case DEVICES                = "DEVICES"
    case FIREBASE_GET_SOUNDS    = "FIREBASE_GET_SOUNDS"
    case LIBRARY_SCRIPTS        = "LIBRARY_SCRIPTS"
    case IV_GET_RELATIONS       = "IV_GET_RELATIONS"
    case ALARMS                 = "ALARMS"
    case ALARM_EVENTS           = "ALARM_EVENTS"
    case EVENTS                 = "EVENTS"
		case AUTOARM_GET     				= "AUTOARM_GET"
		case AUTOARM_SET     				= "AUTOARM_SET"
		case AUTOARM_DEL     				= "AUTOARM_DEL"
}

class HubResponseCode{
    static let NO_SIGNAL_CODE: Int64            = -777;
    static let NO_RESPONSE_CODE: Int64          = -778;
    static let RECONNECT_CODE: Int64            = -888;
    static let DEVICE_ARMED_CODE: Int64         = -889;
    static let NO_SIGNAL_DEVICE_CODE: Int64     = -890;
    
    static func getError (error : Int64) -> String {
        switch error {
        case 0:     return "D3_SUCCEED".localized()
        case -1:    return "D3_ERR_GENERIC".localized()
        case -2:    return "D3_ERR_USER_IS_INACTIVE".localized()
        case -3:    return "D3_ERR_USER_DOES_NOT_EXIST".localized()
        case -4:    return "D3_ERR_USER_ALREADY_LOGGED_IN".localized()
        case -5:    return "D3_ERR_USER_NO_RIGHTS".localized()
        case -6:    return "D3_ERR_USER_INVALID_PASSWORD".localized()
        case -7:    return "D3_ERR_API_DEVICE_NOT_FOUND".localized()
        case -8:    return "PARAM_ERROR".localized()
        case -9:    return "GET_PARSING_ERROR".localized()
        case -10:   return "API_PARAMETERS_FORMAT_ERROR".localized()
        case -11:   return "JSON_COMMAND_NOT_EXIST".localized()
        case -12:   return "D3_ERR_INVALID_REFERENCE_NAME".localized()
        case -13:   return "D3_ERR_REFERENCE_DATA_NOT_AVAILABLE".localized()
        case -14:   return "D3_ERR_PARENT_CONTAIN_INNER_CHILDS".localized()
        case -15:   return "USER_ALREADY_CREATED_ERROR".localized()
        case -18:   return "D3_ERR_OLD_API".localized()
				case -19:   return "D3_ERROR_NOT_FOUND".localized()
        case -20:   return "D3_ERR_ALREADY_DONE".localized()
        case -21:   return "D3_ERR_DEVICE_NOT_FOUND".localized()
        case -22:   return "D3_ERR_DEVICE_SECTION_IN_USE".localized()
        case -23:   return "D3_ERR_DEVICE_SPLIT".localized()
        case -24:   return "D3_ERR_SITE_NOT_FOUND".localized()
        case -25:   return "D3_ERR_SITE_SECTION_IN_USE".localized()
        case -26:   return "D3_ERR_SITE_SPLIT".localized()
        case -27:   return "D3_ERROR_NO_SUCH_DELEGATION_CODE".localized()
        case -28:   return "D3_ERR_NO_DELEGATE_CODE_AVAILABLE".localized()
        case -29:   return "D3_ERR_SITE_NAME_IN_USE".localized()
        case -30:   return "D3_ERR_DEVICE_IS_DELEGATED".localized()
        case -31:   return "D3_ERR_ORGANIZATION_INFO_NOT_FOUND".localized()
        case -32:   return "D3_ERR_PERSON_NOT_FOUND".localized()
        case -33:   return "D3_ERR_DOMAIN_NOT_FOUND".localized()
        case -34:   return "D3_ERROR_DELEGATE_NOT_FOUND".localized()
        case -35:   return "D3_ERR_LOCALE_NOT_FOUND".localized()
        case -36:   return "D3_ERR_SITE_ALREADY_IN_DOMAIN".localized()
        case -37:   return "D3_ERROR_TWO_OR_MORE_BINDINGS".localized()
        case -41:   return "D3_ERR_OPERATOR_NOT_FOUND".localized()
        case -42:   return "D3_ERR_OPERATOR_OFFLINE".localized()
        case -43:   return "D3_ERR_NOT_SUPPORTED".localized()
        case -44:   return "D3_ERR_DEVICE_SCRIPT_NOT_FOUND".localized()
        case -45:   return "D3_ERR_WRONG_INPUT".localized()
        case -46:   return "D3_ERR_SCRIPT_NOT_COMPILED".localized()
        case -47:   return "D3_ERROR_ALREADY_EXIST".localized()
				case -48:   return "D3_ERROR_INVALID_SECTION".localized()
        case -50:   return "D3_ERR_SQL".localized()
        case -61:   return "D3_ERR_NO_REVIEW".localized()
        case -62:   return "D3_ERR_INVALID_REVIEW_STATE".localized()
        case -63:   return "D3_ERR_NO_OPERATOR_TO_HANDLE_REVIEW".localized()
        case -71:   return "D3_ERR_CONTRACTOR_NOT_FOUND".localized()
        case -81:   return "D3_ERR_CONTRACT_NOT_FOUND".localized()
        case -82:   return "D3_ERR_CONTRACT_NAME_IS_NOT_UNIQUE".localized()
        case -100:  return "API_JSON_LOAD_ERROR".localized()
        case -101:  return "API_JSON_UNPACK_ERROR".localized()
        case -102:  return "API_JSON_LOAD_ERROR".localized()
        case -200:  return "API_SURGARD_SITE_NOT_FOUND".localized()
        case -201:  return "API_RETRY_LATER".localized()
        case -202:  return "API_BAD_HTTP_REQUEST".localized()
        case -203:  return "API_EMPTY_HTTP_RESPONSE".localized()
        case -204:  return "API_IVIDEON_ERROR".localized()
        case -205:  return "API_IVIDEON_NEED_AUTHORIZATION".localized()
        case -206:  return "API_IVIDEON_NO_TOKEN".localized()
        case -207:  return "API_NO_SUCH_CAM_RELATION_ERROR".localized()
        default:    return "D3_ERRROR".localized()
        }
    }
}

class HubConst{
    static let CLASS_ALARM: Int64 = 0
    static let CLASS_SABOTAGE: Int64 = 1
    static let CLASS_MALFUNCTION: Int64 = 2
    static let CLASS_ATTENTION: Int64 = 3
    static let CLASS_ARM: Int64 = 4
    static let CLASS_CONTROL: Int64 = 5
    static let CLASS_SUPPLY: Int64 = 6
    static let CLASS_CONNECTIVITY: Int64 = 7
    static let CLASS_INFORMATION: Int64 = 8
    static let CLASS_DEBUG: Int64 = 9
    static let CLASS_TELEMETRY: Int64 = 10
    static let CLASS_GEOLOCATION: Int64 = 11
    static let CLASS_INNER: Int64 = 15
    
    static let REASON_MALF_LOST_CONNECTION: Int64 = 3
    static let REASON_MAFL_BATT_NO: Int64 = 12
    static let REASON_MAFL_BATT_LOW: Int64 = 50

    static let REASON_BATTERY_NORESERVE: Int64 = 1
    static let REASON_BATTERY_SUPPLY: Int64 = 2
    static let REASON_SOCKET_FAULT: Int64 = 3
    static let REASON_BATTERY_MAINSUNDER: Int64 = 4
    static let REASON_BATTERY_MAINOVER: Int64 = 5
    static let REASON_BATTERY_RESERVE: Int64 = 6
    static let REASON_BATTERY_BATTLOW: Int64 = 7
    static let REASON_BATTERY_RESBATTLOW: Int64 = 8
    static let REASON_BATTERY_LEVEL: Int64 = 9
    static let REASON_BATTERY_RESBATTLEVEL: Int64 = 10
    static let REASON_BATTERY_ERROR: Int64 = 11
    static let REASON_BATTERY_RESBATTERROR: Int64 = 12
    
    static let REASON_CONNECTIVITY_UNAVAILABLE: Int64 = 1
    static let REASON_CONNECTIVITY_NOCARRIER: Int64 = 2
    static let REASON_CONNECTIVITY_BUSY: Int64 = 3
    static let REASON_CONNECTIVITY_NOANSWER: Int64 = 4
    static let REASON_CONNECTIVITY_LEVEL: Int64 = 5
    static let REASON_CONNECTIVITY_IMSI: Int64 = 6
    static let REASON_CONNECTIVITY_SMS: Int64 = 7
    static let REASON_CONNECTIVITY_USSD: Int64 = 8
    static let REASON_CONNECTIVITY_BALANCE: Int64 = 9
    static let REASON_CONNECTIVITY_JAM: Int64 = 10
    
    static let REASON_INFORMATION_ARM_DELAY: Int64 = 6
    static let REASON_INFORMATION_UPDATE_READY: Int64 = 7
	
		static let REASON_CONTROL_ARM_BYPASS: Int64 = 27
    
    static let REASON_TEMPERATURE: Int64 = 4
    
    static let DETECTOR_FIRE: Int64 = 1
    static let DETECTOR_KEYPAD: Int64 = 11
    static let DETECTOR_AUTO_RELAY: Int64 = 13
    static let DETECTOR_SIREN: Int64 = 14
    static let DETECTOR_LAMP: Int64 = 15
    static let DETECTOR_MODEM: Int64 = 21
    static let DETECTOR_ETHERNET: Int64 = 22
    static let DETECTOR_MANUAL_RELAY: Int64 = 23
    static let DETECTOR_SZO: Int64 = 25
    static let DETECTOR_BIK: Int64 = 26
    static let DETECTOR_THERMOSTAT: Int64 = 27
    static let DETECTOR_GPRS_1: Int64 = 28
    static let DETECTOR_GRPS_2: Int64 = 29
    
    static let DEVICETYPE_SH: Int64 = 1
    static let DEVICETYPE_SH_1: Int64 = 2
    static let DEVICETYPE_SH_1_1: Int64 = 3
    static let DEVICETYPE_SH_2: Int64 = 4
    static let DEVICETYPE_SH_4G: Int64 = 5
    static let DEVICETYPE_MB: Int64 = 9
    
    static let SENSORTYPE_UNKNOWN: Int64 = 0
    static let SENSORTYPE_WIRED_OUTPUT: Int64 = 55 // сменил, чтобы не путать с РК
    static let SENSORTYPE_WIRED_INPUT: Int64 = 44    // --
    
    static let SECTION_TYPE_SECURITY: Int64 = 1
    static let SECTION_TYPE_ALARM: Int64 = 2
    static let SECTION_TYPE_FIRE: Int64 = 3
    static let SECTION_TYPE_FIRE_DOUBLE: Int64 = 4
    static let SECTION_TYPE_TECHNOLOGIES: Int64 = 5
    static let SECTION_TYPE_TECHNOLOGIES_DISARMED: Int64 = 6
    
    static let ZONETYPE_3731: Int64 = 5
    static let ZONETYPE_8731: Int64 = 8731
    
    //TODO: нет картинки в других таргетах section_no_type
    static func getSectionsTypes() -> [Int64:(name: String, color: UIColor, icon: String)] {
        let sectionTypes: [D3TypeModel] = DataManager.shared.d3Const.getSectionTypes()
        
        return [
            0                                           : (name: R.strings.section_no_type,
                                                           color: UIColor.hubGreenColor,
                                                           icon: XImages.section_no_type_name),
            HubConst.SECTION_TYPE_SECURITY              : (name: R.strings.section_secutiry,
                                                           color: UIColor.hubGreenColor,
                                                           icon: XImages.section_alarm_default_name),
            HubConst.SECTION_TYPE_ALARM                 : (name: R.strings.section_alarm,
                                                           color: UIColor.hubYellowColor,
                                                           icon: XImages.section_alarm_default_name),
            HubConst.SECTION_TYPE_FIRE                  : (name: R.strings.section_fire,
                                                           color: UIColor.hubRedColor,
                                                           icon: XImages.section_fire_default_name),
            HubConst.SECTION_TYPE_FIRE_DOUBLE           : (name: R.strings.section_fire_double,
                                                           color: UIColor.hubRedColor,
                                                           icon: XImages.section_fire_default_name),
            HubConst.SECTION_TYPE_TECHNOLOGIES          : (name: R.strings.section_technologies,
                                                           color: UIColor.gray,
                                                           icon: XImages.section_leak_default_name),
            HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED : (name: R.strings.section_technologies_disrmed,
                                                           color: UIColor.darkGray,
                                                           icon: XImages.section_leak_default_name)
        ]
    }
    
    static func getSectionsHUBTypes() -> [DSectionTypeEntity] {
        let sectionTypes: [D3TypeModel] = DataManager.shared.d3Const.getSectionTypes()
        
        return [
            DSectionTypeEntity(
                id: HubConst.SECTION_TYPE_SECURITY,
                name: sectionTypes.first(where: { $0.id == SECTION_TYPE_SECURITY })?.name ?? "",
                color: UIColor.hubGreenColor,
                icon: #imageLiteral(resourceName: "sec_type_security")
            ),
            
            DSectionTypeEntity(
                id: HubConst.SECTION_TYPE_ALARM,
                name: sectionTypes.first(where: { $0.id == SECTION_TYPE_ALARM })?.name ?? "",
                color: UIColor.hubYellowColor,
                icon: #imageLiteral(resourceName: "sec_type_alarm")
            ),
            
            DSectionTypeEntity(
                id: HubConst.SECTION_TYPE_FIRE,
                name: sectionTypes.first(where: { $0.id == SECTION_TYPE_FIRE })?.name ?? "",
                color: UIColor.hubRedColor,
                icon: #imageLiteral(resourceName: "sec_type_fire")
            ),
            
            DSectionTypeEntity(
                id: HubConst.SECTION_TYPE_TECHNOLOGIES,
                name: sectionTypes.first(where: { $0.id == SECTION_TYPE_TECHNOLOGIES })?.name ?? "",
                color: UIColor.gray,
                icon: #imageLiteral(resourceName: "sec_type_tech")
            )
        ]
    }
    
    static func getСhargingText() -> [Int64:String]{
        return [
            -1 : "BATTERY_DISCHARGED".localized(),
             0 : "BATTERY_NOT_DIASCHARGED".localized(),
             1 : "BATTERY_CHARGED".localized()
        ]
    }
    
    static func getRebootText() -> [Int64:String]{
        return [
            0 : "reboot_reasons[0]".localized(),
            1 : "reboot_reasons[1]".localized(),
            2 : "reboot_reasons[2]".localized(),
            3 : "reboot_reasons[3]".localized(),
            4 : "reboot_reasons[4]".localized()
        ]
    }
    
    static func getPhysicTypes(type: Int64) -> String {
        let physicTypes: [XMLConstRepository.PhysicModel] = DataManager.shared.d3Const.xmlConstRepository.physics
        
        switch type {
        case 1:     return physicTypes.first(where: { $0.id == 1 })?.caption ?? ""
        case 2:     return physicTypes.first(where: { $0.id == 2 })?.caption ?? ""
        case 3:     return physicTypes.first(where: { $0.id == 3 })?.caption ?? ""
        case 4:     return physicTypes.first(where: { $0.id == 4 })?.caption ?? ""
        case 5:     return physicTypes.first(where: { $0.id == 5 })?.caption ?? ""
        case 6:     return physicTypes.first(where: { $0.id == 6 })?.caption ?? ""
        case 7:     return physicTypes.first(where: { $0.id == 7 })?.caption ?? ""
        case 8:     return physicTypes.first(where: { $0.id == 8 })?.caption ?? ""
        case 9:     return physicTypes.first(where: { $0.id == 9 })?.caption ?? ""
        case 10:    return physicTypes.first(where: { $0.id == 10 })?.caption ?? ""
        case 11:    return physicTypes.first(where: { $0.id == 11 })?.caption ?? ""
        case 12:    return physicTypes.first(where: { $0.id == 12 })?.caption ?? ""
        case 13:    return physicTypes.first(where: { $0.id == 13 })?.caption ?? ""
        case 14:    return physicTypes.first(where: { $0.id == 14 })?.caption ?? ""
        case 16:    return physicTypes.first(where: { $0.id == 16 })?.caption ?? ""
        case 17:    return physicTypes.first(where: { $0.id == 17 })?.caption ?? ""
        case 29:    return physicTypes.first(where: { $0.id == 29 })?.caption ?? ""
        case 30:    return physicTypes.first(where: { $0.id == 30 })?.caption ?? ""
        case 31:    return physicTypes.first(where: { $0.id == 31 })?.caption ?? ""
        case 32:    return physicTypes.first(where: { $0.id == 32 })?.caption ?? ""
        case 33:    return physicTypes.first(where: { $0.id == 33 })?.caption ?? ""
        case 34:    return physicTypes.first(where: { $0.id == 34 })?.caption ?? ""
        case 35:    return physicTypes.first(where: { $0.id == 35 })?.caption ?? ""
        case 36:    return physicTypes.first(where: { $0.id == 36 })?.caption ?? ""
        case 37:    return physicTypes.first(where: { $0.id == 37 })?.caption ?? ""
        case 38:    return physicTypes.first(where: { $0.id == 38 })?.caption ?? ""
        case 39:    return physicTypes.first(where: { $0.id == 39 })?.caption ?? ""
        case 40:    return physicTypes.first(where: { $0.id == 40 })?.caption ?? ""
        case 41:    return physicTypes.first(where: { $0.id == 41 })?.caption ?? ""
        case 42:    return physicTypes.first(where: { $0.id == 42 })?.caption ?? ""
        case 43:    return physicTypes.first(where: { $0.id == 43 })?.caption ?? ""
        case 44:    return physicTypes.first(where: { $0.id == 44 })?.caption ?? ""
        case 45:    return physicTypes.first(where: { $0.id == 45 })?.caption ?? ""
        case 46:    return physicTypes.first(where: { $0.id == 46 })?.caption ?? ""
        case 47:    return physicTypes.first(where: { $0.id == 47 })?.caption ?? ""
        case 48:    return physicTypes.first(where: { $0.id == 48 })?.caption ?? ""
        case 49:    return physicTypes.first(where: { $0.id == 49 })?.caption ?? ""
        case 50:    return physicTypes.first(where: { $0.id == 50 })?.caption ?? ""
        case 51:    return physicTypes.first(where: { $0.id == 51 })?.caption ?? ""
        case 52:    return physicTypes.first(where: { $0.id == 52 })?.caption ?? ""
        case 256:   return physicTypes.first(where: { $0.id == 256 })?.caption ?? ""
        default:    return physicTypes.first(where: { $0.id == 0 })?.caption ?? ""
        }
    }
    
    static func getDeviceTypeInfo(_ cV: Int64, cluster: Int64) -> DDeviceTypeInfo {
        let d3Const: D3ConstRepository = DataManager.shared.d3Const
        
        let configVersion = UInt32(cV)
        switch cluster {
        case 1:
            switch (configVersion >> 8) & 255 {
            case 1:
                switch configVersion & 255 {
                case 1: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 2)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                    
                case 2: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 3)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                    
                case 3: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 4)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                    
                case 4: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 5)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                    
                case 5: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 10)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                    
                case 6: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 11)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )

                default: return DDeviceTypeInfo(
                    type: .SecurityHub,
                    name: d3Const.getDeviceType(type: 1)?.name ?? "",
                    icon: XImages.device_astra_sh_name
                )
                }
                
            case 10: return DDeviceTypeInfo(
                type: .Astra,
                name: d3Const.getDeviceType(type: 7)?.name ?? "",
                icon: XImages.device_astra_8945_name
            )
                
            case 11: return DDeviceTypeInfo(
                type: .Astra,
                name: d3Const.getDeviceType(type: 8)?.name ?? "",
                icon: XImages.device_astra_812_name
            )
           
            default: return DDeviceTypeInfo(
                type: .Astra,
                name: d3Const.getDeviceType(type: 6)?.name ?? "",
                icon: XImages.device_astra_name
            )
            }
            
        case 2: return DDeviceTypeInfo(
            type: .Dozor,
            name: d3Const.getDeviceType(type: 6)?.name ?? "",
            icon: XImages.device_astra_dozor_name
        )
            
        case 3: return DDeviceTypeInfo(
            type: .SecurityButton,
            name: d3Const.getDeviceType(type: 9)?.name ?? "",
            icon: XImages.device_mobile_button_name
        )
            
        default: return DDeviceTypeInfo(
            type: .Other,
            name: d3Const.getDeviceType(type: 0)?.name ?? "",
            icon: XImages.device_astra_name
        )
        }
    }
    
    static func getDeviceType(_ cV: Int64, cluster: Int64) -> Int64 {
        let configVersion = UInt32(cV)
        switch cluster {
        case 1:
            switch (configVersion >> 8) & 0xff {
            case 1:
                switch configVersion & 0xff {
                case 1, 2, 3, 4:    return Int64(configVersion & 0xff) + 1
                case 5:             return 10
                case 6:             return 11;
                default:            return Int64((configVersion >> 8) & 0xff)
                }
            case 2:     return 6
            case 10:    return 7
            case 11:    return 8
            case 12:    return 12
            default:    return 0
            }
        case 2:     return 6
        case 3:     return 9
        default:    return 0
        }
    }
    
    static func isHub(_ cV: Int64, cluster: Int64) -> Bool {
        let configVersion = UInt32(cV)
        let _c = (configVersion >> 8) & 255
        return cluster == 1 && _c == 1
    }
    
    static func isSettingNeeded(_ cV: Int64, cluster: Int64) -> Bool {
        let configVersion = UInt32(cV)
        return cluster == 1 && ((configVersion >> 8) & 255) == 1 && (configVersion & 255) < 7
    }

    static func getHubVersion(_ cV: Int64) -> String? {
        let configVersion = UInt32(cV)
        let first = (configVersion >> 24) & 255, second  = (configVersion >> 16) & 255
        return "\(first).\(second)"
    }

    static func isRelay(_ detector: Int64) -> Bool { return detector == 23 || detector == 13 }
    static func isExit(_ detector: Int64) -> Bool  { return detector == 14 || detector == 15 }

		static func isDeviceSupportBypass(_ cV: Int64, cluster: Int64) -> Bool {
				let configVersion = UInt32(cV)
				return cluster == 1 && ((configVersion >> 8) & 255) == 1 && (configVersion & 255) == 6
		}
    
    static func getCommandResults(code: Int64) -> String {
        switch code {
        case 1:     return "command_results[0]".localized()
        case 2:     return "command_results[1]".localized()
        case 3:     return "command_results[2]".localized()
        case 4:     return "command_results[3]".localized()
        case 5:     return "command_results[4]".localized()
        case 6:     return "command_results[5]".localized()
        case 7:     return "command_results[6]".localized()
        case 8:     return "command_results[7]".localized()
        case 9:     return "command_results[8]".localized()
        case 10:    return "command_results[9]".localized()
        case 11:    return "command_results[10]".localized()

        case 64: return "command_results[63]".localized()
        case 65: return "command_results[64]".localized()
        case 66: return "command_results[65]".localized()
        case 67: return "command_results[66]".localized()
        case 68: return "command_results[67]".localized()
        case 69: return "command_results[68]".localized()
        case 70: return "command_results[69]".localized()
        case 71: return "command_results[70]".localized()
        case 72: return "command_results[71]".localized()
        case 73: return "command_results[72]".localized()
        case 74: return "command_results[73]".localized()
        case 75: return "command_results[74]".localized()
        case 76: return "command_results[75]".localized()
        case 77: return "command_results[76]".localized()
        case 78: return "command_results[77]".localized()
        case 79: return "command_results[78]".localized()
        case 80: return "command_results[79]".localized()
        default: return "UNKNOWN_COMAND_FAIL_REASON".localized()
        }
    }
    
    static func getCommandResultsNegative(code: Int64) -> String {
        switch code {
        case  -5:     return "command_results_negative[4]".localized()
        case -10:     return "command_results_negative[9]".localized()
        case -20:     return "command_results_negative[19]".localized()
        default:      return "UNKNOWN_COMAND_FAIL_REASON".localized()
        }
    }
    
    static func getDetectors(_ d: Int64) -> String {
        let detectors: [XMLConstRepository.DetectorTypeModel] = DataManager.shared.d3Const.xmlConstRepository.detectorTypes
        
        switch d {
        case 1:     return detectors.first(where: { $0.id == 1 })?.caption ?? ""
        case 2:     return detectors.first(where: { $0.id == 2 })?.caption ?? ""
        case 3:     return detectors.first(where: { $0.id == 3 })?.caption ?? ""
        case 4:     return detectors.first(where: { $0.id == 4 })?.caption ?? ""
        case 5:     return detectors.first(where: { $0.id == 5 })?.caption ?? ""
        case 6:     return detectors.first(where: { $0.id == 6 })?.caption ?? ""
        case 7:     return detectors.first(where: { $0.id == 7 })?.caption ?? ""
        case 8:     return detectors.first(where: { $0.id == 8 })?.caption ?? ""
        case 9:     return detectors.first(where: { $0.id == 9 })?.caption ?? ""
        case 10:    return detectors.first(where: { $0.id == 10 })?.caption ?? ""
        case 11:    return detectors.first(where: { $0.id == 11 })?.caption ?? ""
        case 12:    return detectors.first(where: { $0.id == 12 })?.caption ?? ""
        case 13:    return detectors.first(where: { $0.id == 13 })?.caption ?? ""
        case 14:    return detectors.first(where: { $0.id == 14 })?.caption ?? ""
        case 15:    return detectors.first(where: { $0.id == 15 })?.caption ?? ""
        case 16:    return detectors.first(where: { $0.id == 16 })?.caption ?? ""
        case 17:    return detectors.first(where: { $0.id == 17 })?.caption ?? ""
        case 18:    return detectors.first(where: { $0.id == 18 })?.caption ?? ""
        case 19:    return detectors.first(where: { $0.id == 19 })?.caption ?? ""
        case 20:    return detectors.first(where: { $0.id == 20 })?.caption ?? ""
        case 21:    return detectors.first(where: { $0.id == 21 })?.caption ?? ""
        case 22:    return detectors.first(where: { $0.id == 22 })?.caption ?? ""
        case 23:    return detectors.first(where: { $0.id == 23 })?.caption ?? ""
        case 24:    return detectors.first(where: { $0.id == 24 })?.caption ?? ""
        case 25:    return detectors.first(where: { $0.id == 25 })?.caption ?? ""
        case 26:    return detectors.first(where: { $0.id == 26 })?.caption ?? ""
        case 27:    return detectors.first(where: { $0.id == 27 })?.caption ?? ""
        case 28:    return detectors.first(where: { $0.id == 28 })?.caption ?? ""
        case 29:    return detectors.first(where: { $0.id == 29 })?.caption ?? ""
        case 40:    return detectors.first(where: { $0.id == 25 })?.caption ?? ""
        case 41:    return detectors.first(where: { $0.id == 25 })?.caption ?? ""
        case 42:    return detectors.first(where: { $0.id == 25 })?.caption ?? ""
        default:    return detectors.first(where: { $0.id == 0 })?.caption ?? ""
        }
    }
    
    static func getDefaultSectionType(_ sectionMask: Int) -> Int64? {
        for i in 0...5 {
            let value = Int(pow(Double(2), Double(i)))
            if (sectionMask & value) == value { return Int64(i + 1) }
        }
        
        
        if (sectionMask & 1) == 1 { return HubConst.SECTION_TYPE_SECURITY }
        if (sectionMask & 2) == 2 { return HubConst.SECTION_TYPE_ALARM }
        if (sectionMask & 4) == 4 { return HubConst.SECTION_TYPE_FIRE }
        if (sectionMask & 8) == 8 { return SECTION_TYPE_FIRE_DOUBLE }
        if (sectionMask & 16) == 16 { return HubConst.SECTION_TYPE_TECHNOLOGIES }
        if (sectionMask & 32) == 16 { return HubConst.SECTION_TYPE_TECHNOLOGIES }
        return nil
    }
}

