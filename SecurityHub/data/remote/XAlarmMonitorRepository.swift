//
//  XHubAlarmMonitorRepository.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 02.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import RxSwift

class XAlarmMonitorRepository {
    
    public var needAlarmSubject: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    public var alarmedDevicesSubject: BehaviorSubject<[Int64]> = BehaviorSubject(value: [])
    
    private var disposable: Disposable!
    private var alarmCallbackContext: Int!
    private var hubHelper: HubHelper
    
    private var alarms: [HubAlarm] = [] {
        didSet {
            needAlarmSubject.onNext(alarms.count != 0)
            alarmedDevicesSubject.onNext(Array(Set(alarms.map({ $0.device_id }))))
        }
    }
    
    init(hubHelper: HubHelper) {
        self.hubHelper = hubHelper
        self.disposable = getActualAlarms().subscribe()
        self.alarmCallbackContext = hubHelper.addAlarmCallback(alarmCallback)
    }
    
    deinit {
        self.hubHelper.removeAlarmCallback(context: alarmCallbackContext)
        self.disposable.dispose()
    }
    
    public func getAlarm(deviceId: Int64) -> HubAlarm? { alarms.first(where: { $0.device_id == deviceId }) }
    
    private func getActualAlarms() -> Single<(result: DCommandResult, value: HubAlarms?)> {
        guard Roles.alarms == true else {
            return Single.just( (result: DCommandResult.SUCCESS, value: nil) )
        }
        
        return DataManager.shared.hubHelper.get(HubCommandWithResult.ALARMS)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onSuccess: { [weak self] (result: DCommandResult, value: HubAlarms?) in if let value = value { self?.alarms = value.items } })
    }
    
    private func alarmCallback(_ alarm: HubAlarm, _ type: HubHelper.AlarmCallbackType) {
        switch type {
        case .add:
            self.alarms.append(alarm)
        case .upd:
            if let index = self.alarms.firstIndex(where: { $0.alarm_id == alarm.alarm_id }) {
                self.alarms[index].state = alarm.state
                self.alarms[index].type = alarm.type
                self.alarms[index].managed_user = alarm.managed_user
                self.alarms[index].extra = alarm.extra
            }
        case .rmv:
            if let index = self.alarms.firstIndex(where: { $0.alarm_id == alarm.alarm_id }) {
                self.alarms.remove(at: index)
            }
        }
    }
    
    public func alarmEvents(alarmId: Int64) -> Single<(result: DCommandResult, value: HubEvents?)> {
        DataManager.shared.hubHelper.get(HubCommandWithResult.ALARM_EVENTS, D: [ "alarm_id" : alarmId])
            .observeOn(ThreadUtil.shared.backScheduler)
    }
}
