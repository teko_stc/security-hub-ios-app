//
//  HubHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import Starscream
import RxSwift
import RxCocoa
import Gloss

class HubHelper{
    static      let AFFECT_COMMAND_ID: Int64    = 777
    private     var commadId                    = HubHelper.AFFECT_COMMAND_ID + 1
    
    fileprivate var socket: WebSocket?
    fileprivate var state:  HubSocketState = .disconnected
    fileprivate var login:      String!
    fileprivate var pass:       String!
    fileprivate var xInProtocol:    XHubInProtocol!
    fileprivate var xOutProtocol:   XHubOutProtocol!
    fileprivate var reconnectDisponsable: Disposable?
    
    fileprivate let nCenter             = NotificationCenter.default
    fileprivate let concurentDispatch   = DispatchQueue(label:"con", attributes:.concurrent)
    fileprivate let backgroundDispatch  = DispatchQueue.global(qos: .utility)
    private     var commandResultUpdates: [Int64 : Int64] = [ : ]
    private     var  referencesCommandIds: Set<Int64> = []
    private     var  referencesTimer: Disposable?
    private     var tmin: Int64 = Int64(NSDate().timeIntervalSince1970 - 86400)
    
    enum AlarmCallbackType { case add, upd, rmv }
    typealias AlarmCallback = (_ alarm: HubAlarm, _ type: AlarmCallbackType) -> Void
    private var alarmCallbacks: [AlarmCallback] = []
    
    func connect(login: String, pass: String, _ inProtocol: XHubInProtocol, _ outProtocol: XHubOutProtocol) {
        self.login = login
        self.pass = pass
        self.xInProtocol = inProtocol
        self.xOutProtocol = outProtocol
        socket = WebSocket(url: URL(string: "\(XTargetUtils.server)?user=\(login)&pass=\(pass)")!)
				socket?.onText = { text in
					DispatchQueue.main.async {
						self.hubResponse(text: text) } }
        socket?.onConnect = { self.getActual() }
        socket?.advancedDelegate = self
        socket?.callbackQueue = backgroundDispatch
        state = .connecting
        
//        let time = xInProtocol.lastEventTime()
//        tmin = time > tmin ? time : tmin

        socket?.connect()
    }
    
    func disconnect() {
        reconnectDisponsable?.dispose()
        state = .disconnected
        socket?.onText = nil
        socket?.advancedDelegate = nil
        socket?.disconnect()
        socket = nil
        
        referencesTimer?.dispose()
        referencesTimer = nil
    }
    
    func isAlive() -> Bool { return socket?.isConnected ?? false}
    
    private func async(_ void: @escaping (()->Void)) { backgroundDispatch.async { self.concurentDispatch.async { void() } } }
    
    private func getActual() {
        setCommand(.OPTIONS)
        setCommand(.ROLES_GET)
        setCommand(.CLUSTERS)
        setCommand(.SITES)
        setCommand(.OPERATORS)
        setCommand(.EVENTS, D: ["affect" : 1], id: HubHelper.AFFECT_COMMAND_ID)
        if XServerChanger.ivideon != nil  { setCommand(.IV_GET_RELATIONS, D: ["device": 0, "section": 0, "zone": 0]) }
        setCommand(.EVENTS, D: ["tmin": self.tmin, "affect" : 0])
    }
    
    private func hubResponse(text : String){
				hubLog(text)
        guard let res = HubResponse.getFrom(text: text) else { return }
        if let com = HubCommand(rawValue: res.q){
            switch com{
            case .SITES:
                guard let obj = HubSites(json: res.d) else { break; }
                Set(obj.items.flatMap { $0.devices }.map { $0.id }).forEach { (device_id) in setCommand(.SCRIPTS, D: ["device_id" : device_id]) }
                DataManager.shared.SITES(obj, time: res.t)
            case .SITE_UPD:
                guard let obj = HubSite(json: res.d) else { break; }
                DataManager.shared.SITE_UPD(obj: obj, time: res.t)
            case .SITE_RMV:
                guard let obj = res.d["site_id"] as? Int64 else { break; }
                DataManager.shared.SITE_RMV(siteId: obj)
            case .EVENTS:
                guard let events = HubEvents(json: res.d) else { break; }
                if res.id == HubHelper.AFFECT_COMMAND_ID {
									DataManager.shared.GET_ALL_AFFECTS(events, time: res.t) }
                else { DataManager.shared.EVENTS(events, time: res.t) }
                if res.r == 2, let event = events.items.last { setCommand(HubCommand.EVENTS, D: ["affect" : 0, "start" : event.id ]) }
            case .REFERENCES:
                guard let arr = res.d["data"] as? [[String:Any]] else { break; }
                for data in arr {
                    if let json = data["data"] as? String, let name = json.toJson().keys.first, let command_id = data["id"] as? Int64 {
                        nCenter.post(name: HubNotification.REFERENCES(command_id), object: name)
                    } else if let name = data["data"] as? String, let id = data["id"] as? Int64 {
                        nCenter.post(name: HubNotification.REFERENCES(id), object: name)
                    }
                }
            case .COMMAND_RESULT_UPD:
                guard let id = res.d["id"] as? Int64, let result = res.d["result"] as? Int64 else { break; }
                commandResultUpdates.updateValue(result, forKey: id)
                NotificationCenter.default.post(name: HubNotification.COMMAND_RESULT_UPD(id), object: result)
                
                _ = Single<Int64>.timer(.seconds(1), scheduler: ThreadUtil.shared.backScheduler)
                    .observeOn(ThreadUtil.shared.backScheduler)
                    .subscribe(onSuccess: { _ in NotificationCenter.default.post(name: HubNotification.commandWhithId(id), object: result) })
            case .SCRIPTS:
                guard let obj = HubScripts(json: res.d) else { break; }
                DataManager.shared.SCRIPTS(obj, device_id: res.r, time: res.t)
                
            case .DEVICE_SCRIPT_UPD:
                guard let obj = HubScript(json: res.d) else { break; }
                DataManager.shared.SCRIPT(obj, time: res.t)
            case .DEVICE_SCRIPT_RMV:
                guard let device_id = res.d["device_id"] as? Int64, let bind = res.d["bind"] as? Int64, let id = res.d["id"] as? Int64 else { break; }
                DataManager.shared.SCRIPT_RMV(device_id: device_id, bind: bind, id: id)
                

                
            case .ROLES_GET:            DataManager.shared.ROLES_GET(obj: HubUserRole(json: res.d)!)
            case .OPERATORS:            DataManager.shared.OPERATORS(obj: HubOperators(json: res.d)!, time: res.t)
            case .EVENT:                DataManager.shared.EVENT(HubEvent(json: res.d)!)
            case .AFFECT_RMV:           DataManager.shared.AFFECT_RMV(HubAffecrRmv(json: res.d)!)
            case .SECTION_ZONE_UPD:			DataManager.shared.SECTION_ZONE_UPD(obj: HubSectionZone(json: res.d)!, time: res.t)
            case .SECTION_ZONE_RMV:     DataManager.shared.SECTION_ZONE_RMV(obj: HubSectionZone(json: res.d)!, time: res.t)
            case .OPERATOR_UPD:         DataManager.shared.OPERATOR_UPD(obj: HubOperator(json: res.d)!, time: res.t)
            case .OPERATOR_RMV:         DataManager.shared.OPERATOR_RMV(operatorId: res.d["op_id"] as! Int64)
            case .ROLES_UPD:            DataManager.shared.ROLES_UPD(role: res.d["roles"] as! Int64)
            case .CONNECTION_DROP:      DataManager.shared.CONNECTION_DROP()
            case .IV_GET_RELATIONS:     DataManager.shared.IV_GET_RELATIONS(obj: HubRelations(json: res.d)!)
            case .CLUSTERS:             DataManager.shared.CLUSTERS( HubClusters(json: res.d)!)
            case .DEVICE_SITES:         DataManager.shared.DEVICE_SITES( HubDeviceSites(json: res.d)! )
            case .DEVICE_SITE_UPD:      DataManager.shared.DEVICE_SITE_UPD( HubDeviceSite(json: res.d)! )
            case .DEVICE_USER_UPD:      DataManager.shared.DEVICE_USER_UPD( HubUser(json: res.d)!, time: res.t)
            case .DEVICE_USER_RMV:      DataManager.shared.DEVICE_USER_RMV( HubUser(json: res.d)! )
            case .DEVICE_SITE_RMV:      DataManager.shared.DEVICE_SITE_RMV( )
            case .FIREBASE_GET_SOUNDS:  DataManager.shared.sounds = (res.d["items"] as? [[String:Any]])?.first ?? [:]
            case .ALARM_ADD:            alarmCallbacks.forEach({ if let alarm = HubAlarm(json: res.d) { $0(alarm, .add) } })
            case .ALARM_UPD:            alarmCallbacks.forEach({ if let alarm = HubAlarm(json: res.d) { $0(alarm, .upd) } })
            case .ALARM_RMV:            alarmCallbacks.forEach({ if let alarm = HubAlarm(json: res.d) { $0(alarm, .rmv) } })
            case .OPTIONS:              DataManager.shared.OPTIONS(obj: HubOptions(json: res.d)!)
            default:          break
            }
        }
        if let _ = HubCommandWithId(rawValue: res.q) {
            NotificationCenter.default.post(name: HubNotification.COMMAND_RESULT(res.id), object: NCommandResult(result: res.r))
            NotificationCenter.default.post(name: HubNotification.commandWhithId(res.id), object: res.r)
        }
        if let _ = HubCommandWithResult(rawValue: res.q) {
            NotificationCenter.default.post(name: HubNotification.COMMAND_RESULT(res.id), object: NCommandResult(result: res.r, value: res.d))
            NotificationCenter.default.post(name: HubNotification.commandWhithResultId(res.id), object: res)
        }
    }
    
    func setCommand(_ command: HubCommand, D: [String : Any] = [:], id: Int64? = nil) {
        commadId += 1
        let _id = id ?? commadId, resp = HubResponse(Q: command.rawValue, D: D, id: _id).toData()
				hubLog(resp.toString())
        async { self.socket?.write(data: resp) }
    }
    
    func setCommandLang(_ command: HubCommand, D: [String : Any] = [:], id: Int64? = nil) {
        commadId += 1
        let _id = id ?? commadId, resp = HubResponseLang(Q: command.rawValue, D: D, id: _id).toData()
				hubLog(resp.toString())
        async { self.socket?.write(data: resp) }
    }
    
    func setCommandWithResult(_ command: HubCommandWithId, D: [String: Any]) -> Single<DCommandResult> {
        guard let socket = self.socket, self.state == .connected else { return Single.just(DCommandResult.NO_SIGNAL) }
        commadId += 1
        let id = commadId, resp = HubResponse(Q: command.rawValue, D: D, id: id).toData()
				hubLog(resp.toString())
        let single = Single<DCommandResult>.create { event -> Disposable in
            socket.write(data: resp)
            let o = self.nCenter.addObserver(forName: HubNotification.COMMAND_RESULT(id), object: nil, queue: OperationQueue.main) { n in
                if let nCR = n.object as? NCommandResult, nCR.result >= 0 { event(SingleEvent.success(DCommandResult.SUCCESS(result: nCR.result))) }
                else if let nCR = n.object as? NCommandResult { event(SingleEvent.success(DCommandResult.ERROR(result: nCR.result))) }
                else { event(SingleEvent.success(DCommandResult.ERROR)) }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func setCommandWithResult(D: String) -> Single<DCommandResult> {
        guard let socket = self.socket, self.state == .connected else { return Single.just(DCommandResult.NO_SIGNAL) }
        commadId += 1
        let id = commadId
        let query = """
        {
            \"ID\": \(id),
            \"Q\": \"COMMAND_SET\",
            \"D\": \(D)
        }
        """
        guard let resp = query.data(using: String.Encoding.utf8) else { return Single.just(DCommandResult.ERROR) }
				hubLog(resp.toString())
        let single = Single<DCommandResult>.create { event -> Disposable in
            socket.write(data: resp)
            var uo: Any?
            let o = self.nCenter.addObserver(forName: HubNotification.COMMAND_RESULT(id), object: nil, queue: OperationQueue.main) { n in
                if let nCR = n.object as? NCommandResult, nCR.result >= 0 { uo = self.commandResultUpdate(id: nCR.result, event: event) }
                else if let nCR = n.object as? NCommandResult { event(SingleEvent.success(DCommandResult.ERROR(result: nCR.result))) }
                else { event(SingleEvent.success(DCommandResult.ERROR)) }
            }
            return Disposables.create{
                self.nCenter.removeObserver(o)
                if let uo = uo { self.nCenter.removeObserver(uo) }
            }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    private func commandResultUpdate(id: Int64, event: @escaping (SingleEvent<DCommandResult>) -> Void) -> Any? {
        if let result = commandResultUpdates[id] { commandResultUpdate(result: result, event: event); return nil }
        let o = self.nCenter.addObserver(forName: HubNotification.COMMAND_RESULT_UPD(id), object: nil, queue: OperationQueue.main) { n in
            guard let result = n.object as? Int64 else { event(SingleEvent.success(DCommandResult.ERROR)); return }
            self.commandResultUpdate(result: result, event: event)
        }
        return o
    }
    private func commandResultUpdate(result: Int64, event: (SingleEvent<DCommandResult>) -> Void) {
        if result == 1 || result == 6 { event(SingleEvent.success(DCommandResult.SUCCESS(command_update_result: result))) }
        else { event(SingleEvent.success(DCommandResult.ERROR(command_update_result: result))) }
    }
    
    private enum ReferencesError: Error { case error }
    let referencesLock = NSLock()
    func getReference(_ command_id: Int64) -> Single<String> {
        if let command_name = self.xInProtocol.getReferenceName(command_id) { return Single.just(command_name) }
        if self.socket == nil && self.state != .connected { return Single.just("COMMAND_UNKNOWN_TYPE".localized()) }
        
        referencesLock.lock()
        referencesCommandIds.insert(command_id)
        referencesLock.unlock()
         
        if referencesCommandIds.count == 30 {
            referencesTimer?.dispose()
            referencesTimer = nil
            setCommandReferences()
        } else if referencesTimer == nil {
            referencesTimer = Single<Int64>.timer(.seconds(3), scheduler: ThreadUtil.shared.backScheduler)
                .observeOn(ThreadUtil.shared.backScheduler)
                .subscribe(onSuccess: {  [weak self] _ in
                    self?.referencesTimer = nil
                    self?.setCommandReferences()
                })
        }

        let single = Single<String>.create { event -> Disposable in
            let o = self.nCenter.addObserver(forName: HubNotification.REFERENCES(command_id), object: nil, queue: OperationQueue.main) { n in
                let name = n.object as? String ?? "COMMAND_UNKNOWN_TYPE".localized()
                self.xOutProtocol.addReference(command_id: command_id, name: name)
                event(SingleEvent.success(name))
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return single.subscribeOn(ThreadUtil.shared.backScheduler)
    }

    @objc private func setCommandReferences() {
        self.commadId += 1
        
        let D: [String : Any] = ["name" : "command", "id" : Array(referencesCommandIds)]

        let resp = HubResponse(Q: HubCommand.REFERENCES.rawValue, D: D, id: commadId).toData()
				hubLog(resp.toString())
        socket?.write(data: resp)

        referencesLock.lock()
        referencesCommandIds.removeAll()
        referencesLock.unlock()
    }
    
    private var  referencesNameIds: Set<Int64> = []
    private var  referencesNameTimer: Disposable?
    private let referencesNameLock = NSLock()
    func getReferenceName(id: Int64) -> Single<String> {
        if self.socket == nil && self.state != .connected { return Single.just("COMMAND_UNKNOWN_TYPE".localized()) }
        
        referencesNameIds.insert(id)
         
        if referencesNameIds.count == 30 {
            referencesNameTimer?.dispose()
            referencesNameTimer = nil
            setNameReferences()
        } else if referencesNameTimer == nil {
            referencesNameTimer = Single<Int64>.timer(.seconds(2), scheduler: ThreadUtil.shared.backScheduler)
                .observeOn(ThreadUtil.shared.backScheduler)
                .subscribe(onSuccess: {  [weak self] _ in
                    self?.referencesNameTimer = nil
                    self?.setNameReferences()
                })
        }

        let single = Single<String>.create { event -> Disposable in
            let o = self.nCenter.addObserver(forName: HubNotification.REFERENCES(id), object: nil, queue: OperationQueue.main) { n in
                let name = n.object as? String ?? "COMMAND_UNKNOWN_TYPE".localized()
                event(SingleEvent.success(name))
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return single.subscribeOn(ThreadUtil.shared.backScheduler)
    }

    @objc private func setNameReferences() {
        self.commadId += 1
        let D: [String : Any] = ["name" : "name", "id" : Array(referencesNameIds)]

        let resp = HubResponse(Q: HubCommand.REFERENCES.rawValue, D: D, id: self.commadId).toData()
				hubLog(resp.toString())
        self.socket?.write(data: resp)

        referencesNameLock.lock()
        referencesNameIds.removeAll()
        referencesNameLock.unlock()
    }

    func get<T:Glossy>(_ command: HubCommandWithResult, D: [String : Any] = [:]) -> Single<(result: DCommandResult, value: T?)> {
        if XServerChanger.ivideon == nil, command == .IV_GET_TOKEN { return .just((result: DCommandResult.ERROR, value: nil)) }
        guard let socket = self.socket, self.state == .connected else { return Single.just((result: DCommandResult.NO_SIGNAL, value: nil)) }
        commadId += 1
        let id = commadId, resp = HubResponse(Q: command.rawValue, D: D, id: id).toData()
				hubLog(resp.toString())
        let single = Single<(result: DCommandResult, value: T?)>.create { event -> Disposable in
            socket.write(data: resp)
            let o = self.nCenter.addObserver(forName: HubNotification.COMMAND_RESULT(id), object: nil, queue: OperationQueue.main) { n in
                guard let nCR = n.object as? NCommandResult, let value = T(json: nCR.value) else { return event(SingleEvent.success((result: DCommandResult.ERROR, value: nil)))  }
                if nCR.result >= 0 { event(SingleEvent.success((result: DCommandResult.SUCCESS, value: value))) }
                else if let nCR = n.object as? NCommandResult { event(SingleEvent.success((result: DCommandResult.ERROR(result: nCR.result), value: nil))) }
                else { event(SingleEvent.success((result: DCommandResult.ERROR, value: nil))) }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return single.subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @available(*, deprecated)
    func setCommandWhithId(_ command: HubCommandWithId, D: [String:Any], whithError: Bool = true) -> Observable<Bool> {
        commadId += 1
        let id = commadId, resp = HubResponse(Q: command.rawValue, D: D, id: id).toData()
				hubLog(resp.toString())
        var comNotifis: Any? = nil
        return Observable.create{ observer in
//            DispatchQueue.main.async { RappleActivityIndicatorView.startAnimating() }
            self.socket?.write(data: resp)
            comNotifis = NotificationCenter.default.addObserver(forName: HubNotification.commandWhithId(id), object: nil, queue: OperationQueue.main){ notification in
//                DispatchQueue.main.async {  RappleActivityIndicatorView.stopAnimation() }
                NotificationCenter.default.removeObserver(comNotifis!)
                let code = notification.object as! Int64
                if code >= 0 {
                    if command != HubCommandWithId.COMMAND_SET {
                        observer.onNext(true);observer.onCompleted()
                    }else{
                        comNotifis = NotificationCenter.default.addObserver(forName: HubNotification.commandWhithId(code), object: nil, queue: OperationQueue.main){ notification in
                            NotificationCenter.default.removeObserver(comNotifis!)
                            let result = notification.object as! Int64
                            if result == 1 || result == 6{
                                if whithError{
                                    AlertUtil.successAlert("\("ERROR_COMMAND".localized()) \(HubConst.getCommandResults(code: result))")
                                }
                                observer.onNext(true)
                            }else if result > 0 {
                                if whithError{
                                    AlertUtil.errorAlert("\("ERROR_COMMAND".localized()) \(HubConst.getCommandResults(code: result))")
                                }
                                observer.onError(MyError.error)
                            }else{
                                if whithError{
                                    AlertUtil.errorAlert("\("ERROR_COMMAND".localized()) \(HubConst.getCommandResultsNegative(code: result))")
                                }
                                observer.onNext(true)
                            }
                            observer.onCompleted()
                        }
                    }
                }else{
                    if whithError{ AlertUtil.errorAlert(HubResponseCode.getError(error: code)) }
                    observer.onError(MyError.error)
                    observer.onCompleted()
                }
            }
            let disp = Observable<Int>.timer(.seconds(20), scheduler: ThreadUtil.shared.backScheduler)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: {t in
    //                AlertUtil.errorAlert(HubResponseCode.getError(error: HubResponseCode.NO_RESPONS_CODE))
                    observer.onError(MyError.error)
                    observer.onCompleted()
                })
            return Disposables.create { disp.dispose();
//                DispatchQueue.main.async {  RappleActivityIndicatorView.stopAnimation() }
                
            }
        }
    }
    
    @available(*, deprecated)
    func setCommandWhithResult(_ command: HubCommandWithResult, D: [String:Any]) -> Observable<HubResponse> {
        if XServerChanger.ivideon == nil, command == .IV_GET_TOKEN { return .just(HubResponse(r:HubResponseCode.NO_RESPONSE_CODE)) }
        commadId += 1
        let id = commadId, resp = HubResponse(Q: command.rawValue, D: D, id: id).toData()
				hubLog(resp.toString())
        var comNotifis: Any? = nil
        return Observable.create{ observer in
            self.socket?.write(data: resp)
            comNotifis = NotificationCenter.default.addObserver(forName: HubNotification.commandWhithResultId(id), object: nil, queue: OperationQueue.main){ notification in
                NotificationCenter.default.removeObserver(comNotifis!)
                observer.onNext(notification.object as! HubResponse); observer.onCompleted()
            }
            let _ = Observable<Int>.timer(.seconds(20), scheduler: ThreadUtil.shared.backScheduler).subscribe(onNext: {t in observer.onNext(HubResponse(r:HubResponseCode.NO_RESPONSE_CODE));observer.onCompleted() })
            return Disposables.create {}
        }
    }
    
    func addAlarmCallback(_ callback: @escaping AlarmCallback) -> Int {
        self.alarmCallbacks.append(callback)
        return self.alarmCallbacks.count - 1
    }
    
    func removeAlarmCallback(context: Int) {
        self.alarmCallbacks.remove(at: context)
    }
    
}

enum HubSocketState {
    case connected
    case connecting
    case disconnected
}

extension HubHelper: WebSocketAdvancedDelegate{
    
    func websocketHttpUpgrade(socket: WebSocket, response: String) {
        reconnectDisponsable?.dispose()
        if let x_index = response.range(of: "X-Result: ")?.upperBound {
//            X-Protocol:
            let after_x = String(response.suffix(from: x_index))
            var result = 0
            if let x_end_index = after_x.range(of: "\r\n")?.lowerBound {
                result = Int(String(after_x[..<x_end_index])) ?? 0
            }
            if result < 1 {
                disconnect()
                NotificationCenter.default.post(name: HubNotification.connectionStateChange, object: result)
            } else{
                state = .connected
                NotificationCenter.default.post(name: HubNotification.connectionStateChange, object: result)
            }
        }
    }

    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
        if state != .disconnected && DataManager.shared.getUser().login.count != 0 {
            NotificationCenter.default.post(name: HubNotification.connectionStateChange, object: HubResponseCode.RECONNECT_CODE) 
            reconnectDisponsable?.dispose()
            reconnectDisponsable = Observable<Int>.timer(.seconds(1), scheduler: SerialDispatchQueueScheduler.init(qos: .utility))
                .subscribe(onNext:{ t in
                    NotificationCenter.default.post(name: HubNotification.connectionStateChange, object: HubResponseCode.RECONNECT_CODE)
                    self.connect(login: self.login, pass: self.pass, self.xInProtocol, self.xOutProtocol)
                })
        }else{
            NotificationCenter.default.post(name: HubNotification.connectionStateChange, object: HubResponseCode.NO_SIGNAL_CODE)
        }
    }
    
    func websocketDidConnect(socket: WebSocket) { }
    func websocketHttpUpgrade(socket: WebSocket, request: String) {}
    func websocketDidReceiveData(socket: WebSocket, data: Data, response: WebSocket.WSResponse) { }
    func websocketDidReceiveMessage(socket: WebSocket, text: String, response: WebSocket.WSResponse) { }
	
		private func hubLog(_ text : String){
//			print(text)
		}
}

enum MyError: Error {
    case error, haveZones
}
