//
//  PushHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import UIKit
import RxSwift

class PushHelper{
    
    init() {
        DispatchQueue.main.async {
            self.initFCM()
        }
    }
    
    func getDeviceToken() -> Data? {
        return Messaging.messaging().apnsToken
    }
    
    func getFcmToken() -> String? {
        return Messaging.messaging().fcmToken
    }
    
    private func initFCM() {
        Messaging.messaging().delegate = UIApplication.shared.delegate as? AppDelegate
        UIApplication.shared.registerForRemoteNotifications()

        FirebaseConfiguration().setLoggerLevel(FirebaseLoggerLevel.min)
        UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as? AppDelegate
        
        if let fcm = XServerChanger.fcm {
            let options = FirebaseOptions(googleAppID: fcm.application_id, gcmSenderID: fcm.gcm_sender_id)
            options.apiKey = fcm.api_key
            FirebaseApp.app()?.delete({ [weak self] _ in self?.configure(options: options) }) ?? configure(options: options)
        }
    }
    
    private func configure(options: FirebaseOptions) {
        FirebaseApp.configure(options: options)
        
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { [weak self] (settings) in
            switch settings.authorizationStatus {
            case .authorized:
                self?.fcm()
            default: 
                self?.requestAuthorization()
            }
        })
    }
    
    private func requestAuthorization() {
        DispatchQueue.main.async {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { [weak self]  result, _ in
                result ? self?.fcm() : ()
            })
        }
    }
    
    private func fcm() {
        guard XServerChanger.fcm != nil else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
            DataManager.shared.setFCM()
            DataManager.shared.hubHelper.setCommand(.FIREBASE_GET_SOUNDS)
        }
    }
    
}


