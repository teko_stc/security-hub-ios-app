//
//  DataManager+Observers.swift
//  SecurityHub
//
//  Created by Timerlan on 05/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension DataManager {
    func getSitesObserver() -> Observable<DSiteEntity> {
        let observable = Observable<DSiteEntity>.create { observer -> Disposable in
            self.dbHelper.getSites().forEach{ (site) in
                observer.onNext(DataEntityMapper.dSite(site, .insert, self.dbHelper.getAffects(site_id: site.id), self.dbHelper.getArmStatus(site_id: site.id)))
            }
            let o = self.nCenter.addObserver(forName: HubNotification.siteUpdate, object: nil, queue: OperationQueue.main) { n in
                if let nSite = n.object as? NSiteEntity {
                    observer.onNext(DataEntityMapper.dSite(nSite, self.dbHelper.getAffects(site_id: nSite.id), self.dbHelper.getArmStatus(site_id: nSite.id)))
                }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }
    
    func getDevicesObserver(site_id: Int64?)->Observable<DDeviceEntity>{
        let observable = Observable<DDeviceEntity>.create{ observer -> Disposable in
            self.dbHelper.getDevices().forEach{ (di) in
                observer.onNext(DataEntityMapper.dDevice(di, .insert, self.dbHelper.getAffects(site_id: site_id, device_id: di.device.id), self.dbHelper.getArmStatus(site_id: site_id, device_id: di.device.id)))
            }
            let o = self.nCenter.addObserver(forName: HubNotification.deviceUpdate, object: nil, queue: OperationQueue.main) { n in
                if let nDevice = n.object as? NDeviceEntity {
                    observer.onNext(DataEntityMapper.dDevice(nDevice, self.dbHelper.getAffects(site_id: site_id, device_id: nDevice.id), self.dbHelper.getArmStatus(site_id: site_id, device_id: nDevice.id)))
                }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }
    
    func getSectionsObserver(searchText: String? = nil)->Observable<DSectionEntity>{
        let observable = Observable<DSectionEntity>.create{ observer -> Disposable in
            self.dbHelper.getSections(searchText: searchText).forEach({ (si) in
                observer.onNext(DataEntityMapper.dSection(si, .insert, self.dbHelper.getAffects(device_id: si.section.device, section_id: si.section.section)))
            })
            let o = self.nCenter.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.current) { n in
                if let nSection = n.object as? NSectionEntity {
                    observer.onNext(DataEntityMapper.dSection(nSection, self.dbHelper.getAffects(device_id: nSection.deviceId, section_id: nSection.sectionId)))
                }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }
    
    func getSectionsObserver(priority: Int, searchText: String? = nil) -> Observable<[DSectionEntity]> {
        let observable = Observable<[DSectionEntity]>.create{ observer -> Disposable in
            var sites = self.dbHelper.getSites()
            let site = sites.first(where: { s in s.id == priority }) ?? sites.first
            sites.removeAll(where: { s in s.id == site?.id })
            if let site = site { sites.insert(site, at: 0) }
            
            for site in sites {
                let results = self.dbHelper.getSections(priority: site.id.int, searchText: searchText).map { (si) -> DSectionEntity in
                    return DataEntityMapper.dSection(si, .insert, si.affects ?? self.dbHelper.getAffects(device_id: si.section.device, section_id: si.section.section))
                }
                observer.onNext(results)
            }
            
            let o = self.nCenter.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.current) { n in
                if let nSection = n.object as? NSectionEntity {
                    observer.onNext([DataEntityMapper.dSection(nSection, self.dbHelper.getAffects(device_id: nSection.deviceId, section_id: nSection.sectionId))])
                }
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }

    func getSectionObserver(device_id: Int64, section_id: Int64) -> Observable<DSectionEntity>{
        let observable = Observable<DSectionEntity>.create{  [device_id, section_id] observer -> Disposable in
            if let si = self.dbHelper.getSection(device_id: device_id, section_id: section_id) {
                observer.onNext(DataEntityMapper.dSection(si, .insert, self.dbHelper.getAffects(device_id: si.section.device, section_id: si.section.section)))
            }
            let o = self.nCenter.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.current) { n in
                guard let nSection = n.object as? NSectionEntity,
                      nSection.section.device == device_id, nSection.section.section == section_id else {
                    return
                }

                observer.onNext(DataEntityMapper.dSection(nSection, self.dbHelper.getAffects(device_id: nSection.deviceId, section_id: nSection.sectionId)))
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }
    
    func getSectionsObserver(device_id: Int64) -> Observable<DSectionEntity>{
        let observable = Observable<DSectionEntity>.create{  [device_id] observer -> Disposable in
            self.dbHelper.getSectionss(device_id: device_id).forEach({ (si) in
                observer.onNext(
                    DataEntityMapper.dSection(
                        si,
                        .insert,
                        si.affects ?? []
                    )
                )
            })
            let o = self.nCenter.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.current) { n in
                guard let nSection = n.object as? NSectionEntity,
                      nSection.section.device == device_id else {
                    return
                }

                observer.onNext(
                    DataEntityMapper.dSection(
                        nSection,
                        self.dbHelper.getAffects(device_id: nSection.deviceId, section_id: nSection.sectionId)
                    )
                )
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
    }
    
    func getZonesObserver()->Observable<DZoneEntity> {
        let observable = Observable<DZoneEntity>.create{ observer -> Disposable in
            self.dbHelper.getZones().forEach{ (zi) in
                let dZone = HubConst.isRelay(zi.zone.detector) || HubConst.isExit(zi.zone.detector) ?
                DataEntityMapper.dRelay(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone)) :
                    DataEntityMapper.dZone(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.main) { n in
                if let nZone = n.object as? NZoneEntity {
                    let dZone = HubConst.isRelay(nZone.zoneDetector) || HubConst.isExit(nZone.zoneDetector) ?
                        DataEntityMapper.dRelay(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getRelayStatus(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getScript(device_id: nZone.deviceId, zone_id: nZone.zoneId)) :
                        DataEntityMapper.dZone(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId))
                    observer.onNext(dZone)
                }
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue()) { n in
                guard   let nScript = n.object as? NScriptEntity,
                        let zi = self.dbHelper.getZone(device_id: nScript.deviceId, section_id: 0, zone_id: nScript.bind) else { return }
                let dZone = DataEntityMapper.dRelay(zi, .update, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            return Disposables.create{ self.nCenter.removeObserver(o); self.nCenter.removeObserver(o2) }
        }
        return observable
    }
    
    func getZonesObserver(priority: Int, searchText: String? = nil) -> Observable<[DZoneEntity]> {
        let observable = Observable<[DZoneEntity]>.create{ observer -> Disposable in
            var sites = self.dbHelper.getSites()
            let site = sites.first(where: { s in s.id == priority }) ?? sites.first
            sites.removeAll(where: { s in s.id == site?.id })
            if let site = site { sites.insert(site, at: 0) }
            
            for site in sites {
                let results = self.dbHelper.getZones(priority: site.id.int, searchText: searchText).map { (zi) -> DZoneEntity in
                    let dZone = HubConst.isRelay(zi.zone.detector) || HubConst.isExit(zi.zone.detector)
                    ?
                    DataEntityMapper.dRelay(zi, .insert,
                                            zi.affects ?? self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone),
                                            self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone),
                                            self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                    :
                    DataEntityMapper.dZone(zi, .insert,
																						zi.affects ?? self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone),
                                           	self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone))
                    return dZone
                }
                observer.onNext(results)
            }

            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.main) { n in
                if let nZone = n.object as? NZoneEntity {
                    let dZone = HubConst.isRelay(nZone.zoneDetector) || HubConst.isExit(nZone.zoneDetector) ?
                        DataEntityMapper.dRelay(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getRelayStatus(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getScript(device_id: nZone.deviceId, zone_id: nZone.zoneId)) :
                        DataEntityMapper.dZone(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId))
                    observer.onNext([dZone])
                }
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue()) { n in
                guard   let nScript = n.object as? NScriptEntity,
                        let zi = self.dbHelper.getZone(device_id: nScript.deviceId, section_id: 0, zone_id: nScript.bind) else { return }
                let dZone = DataEntityMapper.dRelay(zi, .update, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                observer.onNext([dZone])
            }
            return Disposables.create{ self.nCenter.removeObserver(o); self.nCenter.removeObserver(o2) }
        }
        return observable
    }
		
    func getZoneObserver(device_id: Int64, section_id: Int64, zone_id: Int64)->Observable<DZoneEntity> {
        let observable = Observable<DZoneEntity>.create{ [device_id, section_id, zone_id] observer -> Disposable in
            if let zi = self.dbHelper.getZone(device_id: device_id, section_id: section_id, zone_id: zone_id) {
                let dZone = HubConst.isRelay(zi.zone.detector) || HubConst.isExit(zi.zone.detector) ?
                DataEntityMapper.dRelay(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone)) :
                    DataEntityMapper.dZone(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.current) { n in
                guard let nZone = n.object as? NZoneEntity,
                      nZone.zone.device == device_id, nZone.zone.section == section_id, nZone.zone.zone == zone_id else {
                    return
                }
                
                let dZone = HubConst.isRelay(nZone.zoneDetector) || HubConst.isExit(nZone.zoneDetector) ?
                    DataEntityMapper.dRelay(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getRelayStatus(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getScript(device_id: nZone.deviceId, zone_id: nZone.zoneId)) :
                    DataEntityMapper.dZone(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId))
                observer.onNext(dZone)
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue.current) { n in
                guard   let nScript = n.object as? NScriptEntity,
                        nScript.deviceId == device_id, section_id == 0, nScript.bind == zone_id else {
                    return
                }
                guard   let zi = self.dbHelper.getZone(device_id: nScript.deviceId, section_id: 0, zone_id: nScript.bind) else {
                    return
                }

                let dZone = DataEntityMapper.dRelay(zi, .update, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            return Disposables.create{ self.nCenter.removeObserver(o); self.nCenter.removeObserver(o2) }
        }
        return observable
    }
    
    func getZonesObserver(device_id: Int64, section_id: Int64)->Observable<DZoneEntity> {
        let observable = Observable<DZoneEntity>.create{ [device_id, section_id] observer -> Disposable in
            self.dbHelper.getZoness(device_id: device_id, section_id: section_id).forEach{ (zi) in
                let dZone = HubConst.isRelay(zi.zone.detector) || HubConst.isExit(zi.zone.detector) ?
                DataEntityMapper.dRelay(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone)) :
                    DataEntityMapper.dZone(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            
            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.current) { n in
                guard let nZone = n.object as? NZoneEntity,
                      nZone.zone.device == device_id, nZone.zone.section == section_id else {
                    return
                }
                
                let dZone = HubConst.isRelay(nZone.zoneDetector) || HubConst.isExit(nZone.zoneDetector) ?
                    DataEntityMapper.dRelay(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getRelayStatus(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getScript(device_id: nZone.deviceId, zone_id: nZone.zoneId)) :
                    DataEntityMapper.dZone(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId))
                observer.onNext(dZone)
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue.current) { n in
                guard   let nScript = n.object as? NScriptEntity,
                        nScript.deviceId == device_id, section_id == 0 else {
                    return
                }
                guard   let zi = self.dbHelper.getZone(device_id: nScript.deviceId, section_id: 0, zone_id: nScript.bind) else {
                    return
                }

                let dZone = DataEntityMapper.dRelay(zi, .update, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            return Disposables.create{ self.nCenter.removeObserver(o); self.nCenter.removeObserver(o2) }
        }
        return observable
    }
    
    func getZonesObserver(device_id: Int64)->Observable<DZoneEntity> {
        let observable = Observable<DZoneEntity>.create{ [device_id] observer -> Disposable in
            self.dbHelper.getZoness(device_id: device_id).forEach{ (zi) in
                let dZone = HubConst.isRelay(zi.zone.detector) || HubConst.isExit(zi.zone.detector) ?
                DataEntityMapper.dRelay(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone)) :
                    DataEntityMapper.dZone(zi, .insert, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            
            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.current) { n in
                guard let nZone = n.object as? NZoneEntity,
                      nZone.zone.device == device_id else {
                    return
                }
                
                let dZone = HubConst.isRelay(nZone.zoneDetector) || HubConst.isExit(nZone.zoneDetector) ?
                    DataEntityMapper.dRelay(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getRelayStatus(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getScript(device_id: nZone.deviceId, zone_id: nZone.zoneId)) :
                    DataEntityMapper.dZone(nZone, self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId), self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId))
                observer.onNext(dZone)
            }
            let o2 = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue.current) { n in
                guard   let nScript = n.object as? NScriptEntity,
                        nScript.deviceId == device_id else {
                    return
                }
                guard   let zi = self.dbHelper.getZone(device_id: nScript.deviceId, section_id: 0, zone_id: nScript.bind) else {
                    return
                }

                let dZone = DataEntityMapper.dRelay(zi, .update, self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getRelayStatus(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone), self.dbHelper.getScript(device_id: zi.zone.device, zone_id: zi.zone.zone))
                observer.onNext(dZone)
            }
            return Disposables.create{ self.nCenter.removeObserver(o); self.nCenter.removeObserver(o2) }
        }
        return observable
    }
    
    func getDeviceZonesObserver(device_id: Int64)->Observable<DZoneEntity> {
        let observable = Observable<DZoneEntity>.create{ [device_id] observer -> Disposable in
            self.dbHelper.getDeviceZoness(device_id: device_id).forEach{ (zi) in
                let dZone = DataEntityMapper.dZone(
                    zi, .insert,
                    self.dbHelper.getAffects(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone),
                    self.dbHelper.getLastAlarm(device_id: zi.zone.device, section_id: zi.zone.section, zone_id: zi.zone.zone)
                )
                observer.onNext(dZone)
            }
            
            let o = self.nCenter.addObserver(forName: HubNotification.zonesUpdate, object: nil, queue: OperationQueue.current) { n in
                guard
                    let nZone = n.object as? NZoneEntity,
                    nZone.zone.device == device_id,
                    ( nZone.zone.section == 0 && ( nZone.zone.zone == 0 || nZone.zone.zone == 100 ) )
                else {
                    return
                }
                
                let dZone = DataEntityMapper.dZone(
                    nZone,
                    self.dbHelper.getAffects(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId),
                    self.dbHelper.getLastAlarm(device_id: nZone.deviceId, section_id: nZone.sectionId, zone_id: nZone.zoneId)
                )
                observer.onNext(dZone)
            }

            return Disposables.create{
                self.nCenter.removeObserver(o)
            }
        }
        return observable
    }
    
    func getScripts(device_ids: [Int64]?) -> Observable<DScriptEntity> {
        struct Script{ var script: DScriptInfo; var type: NUpdateType }
        let observable = Observable<Script>.create{ observer -> Disposable in
            self.dbHelper.getScripts(device_ids: device_ids).forEach { (s) in observer.onNext(Script(script: s, type: .insert)) }
            let o = self.nCenter.addObserver(forName: HubNotification.scriptUpdate, object: nil, queue: OperationQueue.main) { n in
                guard let nScript = n.object as? NScriptEntity else { return }
                let s = DScriptInfo(deviceId: nScript.deviceId, configVersion: nScript.configVersion, id: nScript.id, uid: nScript.uid, bind: nScript.bind, zoneName: nScript.zoneName, enabled: nScript.enabled, params: nScript.params, extra: nScript.extra, name: nScript.name)
                observer.onNext(Script(script: s, type: nScript.type))
            }
            return Disposables.create{ self.nCenter.removeObserver(o) }
        }
        return observable
            .concatMap { (s) -> Observable<(Script, (result: DCommandResult, value: HubLibraryScripts?))> in return Observable.zip(Observable.just(s), self.getLibraryScripts(config_version: s.script.configVersion).subscribeOn(ThreadUtil.shared.backScheduler).asObservable()) }
            .do(onNext: { (r) in if r.1.value == nil { throw XError(desc: "addasd") } })
            .map { (r) -> DScriptEntity in DataEntityMapper.dScript(r.0.script, library: r.1.value!, type: r.0.type) }
    }
    
    func getScript(device_id: Int64, zone_id: Int64) -> Observable<(script: HubLibraryScript.Data?, params: [[String: Any]])> {
        let observable = Observable<DScriptInfo>.create{ observer -> Disposable in
            let script = self.dbHelper.getScript(device_id: device_id, zone_id: zone_id)
            if let s = script { observer.onNext(s) }
            return Disposables.create{  }
        }
        return observable
            .concatMap { (s) -> Observable<(DScriptInfo, (result: DCommandResult, value: HubLibraryScripts?))> in return Observable.zip(Observable.just(s), self.getLibraryScripts(config_version: s.configVersion).subscribeOn(ThreadUtil.shared.backScheduler).asObservable()) }
            .do(onNext: { (r) in if r.1.value == nil { throw XError(desc: "addasd") } })
            .map { (r) -> (script: HubLibraryScript.Data?, params: [[String: Any]]) in (script: r.1.value?.items.first(where: { $0.uid == r.0.uid })?.data, params: r.0.params) }
    }
}
