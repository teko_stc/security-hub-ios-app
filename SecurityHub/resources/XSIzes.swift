//
//  XMargins.swift
//  SecurityHub
//
//  Created by Timerlan on 26.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XSizes {
    static let MARGIN_SMALL: CGFloat = 8
    static let MARGIN_STANDART: CGFloat = 16
    static let MARGIN_BIGGER: CGFloat = 32
    
    static let ICON_SMALL: CGFloat = 20
}
