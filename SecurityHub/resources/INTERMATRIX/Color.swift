//
//  ExtensionUtil.swift
//  SecurityHub
//
//  Created by Dan on 29.04.2020.
//  Copyright © 2020 Tattelecom. All rights reserved.
//
import UIKit

extension UIColor {
    static func colorFromHex(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0,
            green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0,
            blue: ((CGFloat)(rgbValue & 0xFF))/255.0,
            alpha: 1
        )
    }
    
    class var hubBackgroundColor: UIColor { return colorFromHex(0xefeff4) }
    
    class var hubMainColor: UIColor { return colorFromHex(0x3b5e63) }
    static var hubMainColorUINT: UInt = 0x3b5e63
    
    class var hubLightMainColor: UIColor { return colorFromHex(0x58c6e8) }
    
    class var hubDarkMainColor: UIColor { return colorFromHex(0x006786) }
    
    class var hubRipleBackgroundColor: UIColor { return colorFromHex(0x006c85) }
    
    class var hubRipleColor: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.5) }
    
    class var hubTransparent: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.0) }
    
    class var hubGreenColor: UIColor { return colorFromHex(0x4CAF50) }
    class var hubGreenDarkColor: UIColor { return colorFromHex(0x1B5E20) }
    
    class var hubRedColor: UIColor { return colorFromHex(0xf44336) }
    class var hubRedDarkColor: UIColor { return colorFromHex(0xb71c1c) }

    class var hubYellowColor: UIColor { return colorFromHex(0xF9A825) }
    class var hubYellowDarkColor: UIColor { return colorFromHex(0xF57F17) }
}
