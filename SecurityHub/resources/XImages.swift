//
//  XImages.swift
//  SecurityHub
//
//  Created by Timerlan on 25.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XImages {
    static let menu_main_teleplus    = UIImage(named: "menu_main_teleplus")?.withColor(color: DEFAULT_UNSELECTED)
    static let menu_main_teleplus_colored    = UIImage(named: "menu_main_teleplus")
    static let menu_main    = UIImage(named: "menu_main")?.withColor(color: DEFAULT_UNSELECTED)
    static let menu_script  = UIImage(named: "menu_scripts")?.withColor(color: DEFAULT_UNSELECTED)
    static let menu_history = UIImage(named: "menu_history")?.withColor(color: DEFAULT_UNSELECTED)
    static let menu_cameras = UIImage(named: "menu_cameras")?.withColor(color: DEFAULT_UNSELECTED)
    static let menu_menu    = UIImage(named: "menu_menu")?.withColor(color: DEFAULT_UNSELECTED)
    
    static let view_arrow_bottom    = UIImage(named: "view_arrow_bottom")
    static let view_arrow_top       = UIImage(named: "view_arrow_top")
    static let view_check           = UIImage(named: "view_check")
    static let view_plus           = UIImage(named: "plus")
    
    static let site_default_name_teleplus        = "menu_main_teleplus"
    static let site_default_teleplus    = UIImage(named: site_default_name_teleplus)
    static let site_default_name        = "menu_main"
    static let site_default             = UIImage(named: site_default_name)
    static let site_armed_big_name      = "ic_security_checked_green_500_big_48"
    static let site_armed_big           = UIImage(named: site_armed_big_name)
    static let site_armed_small_name    = "ic_security_checked_green_500_small_24"
    static let site_armed_small         = UIImage(named: site_armed_small_name)
    static let site_disarmed_big_name   = "ic_restriction_shield_grey_500_big"
    static let site_disarmed_big        = UIImage(named: site_disarmed_big_name)
    static let site_disarmed_small_name = "ic_restriction_shield_grey_500_small_24"
    static let site_disarmed_small      = UIImage(named: site_disarmed_small_name)
    static let site_c_armed_big_name    = "ic_partly_big"
    static let site_c_armed_big         = UIImage(named: site_c_armed_big_name)
    static let site_c_armed_small_name  = "ic_partly_small"
    static let site_c_armed_small       = UIImage(named: site_c_armed_small_name)
    static let site_alarm_big_name      = "ic_alert_red_big"
    static let site_alarm_big           = UIImage(named: site_alarm_big_name)
    static let site_alarm_small_name    = "ic_alert_red"
    static let site_alarm_small         = UIImage(named: site_alarm_small_name)
    
    
    static let section_armed_big_name       = "ic_security_checked_green_500_big_48"
    static let section_armed_big            = UIImage(named: section_armed_big_name)
    static let section_disarmed_big_name    = "ic_restriction_shield_grey_500_big"
    static let section_disarmed_big         = UIImage(named: section_disarmed_big_name)
    static let section_alarm_big_name       = "ic_alert_red_big"
    static let section_alarm_big            = UIImage(named: section_alarm_big_name)

    static let section_alarm_default_name   = "ic_section_alarm_72"
    static let section_alarm_default        = UIImage(named: section_alarm_default_name)
    static let section_fire_default_name    = "ic_section_fire_72"
    static let section_fire_default         = UIImage(named: section_fire_default_name)
    static let section_leak_default_name    = "ic_section_leak_72"
    static let section_leak_default         = UIImage(named: section_leak_default_name)
    static let section_no_type_name         = "ic_securityhub_72_brand" //TODO No image
    static let section_no_type              = UIImage(named: section_no_type_name) //TODO No image
    
    static let zone_default_name       = "ic_sensor_brand_color"
    static let zone_default            = UIImage(named: zone_default_name)
    static let zone_alarm_name         = "ic_alert_red_big"
    static let zone_alarm              = UIImage(named: zone_alarm_name)
    static let zone_armed_big_name     = "ic_security_checked_green_500_big_48"
    static let zone_armed_big          = UIImage(named: zone_armed_big_name)
    static let zone_disarmed_big_name  = "ic_restriction_shield_grey_500_big"
    static let zone_disarmed_big       = UIImage(named: zone_disarmed_big_name)


    static let relay_on_big_name      = "ic_lightbulb_on_outline_big"
    static let relay_on_big           = UIImage(named: relay_on_big_name)
    static let relay_on_small_name    = "ic_lightbulb_on_outline_small"
    static let relay_on_small         = UIImage(named: relay_on_small_name)
    static let relay_off_name         = "ic_lightbulb_off_outline_big"
    static let relay_off              = UIImage(named: relay_off_name)
    
    
    
    static let device_astra_sh          = UIImage(named: device_astra_sh_name)
    static let device_astra             = UIImage(named: device_astra_name)
    static let device_astra_dozor       = UIImage(named: device_astra_dozor_name)
    static let device_astra_8945        = UIImage(named: device_astra_8945_name)
    static let device_astra_812         = UIImage(named: device_astra_812_name)
    static let device_mobile_button     = UIImage(named: device_mobile_button_name)
    
    static let device_astra_sh_name         = "ic_astra_sh_pict"
    static let device_astra_name            = "ic_astra"
    static let device_astra_dozor_name      = "ic_astra_dozor_pict"
    static let device_astra_8945_name       = "ic_astra_8945"
    static let device_astra_812_name        = "ic_astra_812"
    static let device_mobile_button_name    = "ic_mobile_button"
    
    static let button_manage            = UIImage(named: "button_manage")
    
    static let astra_2331   = UIImage(named: "astra_2331")
    static let astra_8231   = UIImage(named: "astra_8231")
    static let astra_8731   = UIImage(named: "astra_8731")
    
    static let astra_8          = UIImage(named: "astra_8")
    static let astra_361        = UIImage(named: "astra_361")
    static let astra_ri_rpdr    = UIImage(named: "astra_ri_rpdr")
    static let astra_3731       = UIImage(named: "astra_3731")
    static let astra_3221       = UIImage(named: "astra_3221")
    static let astra_rpdk       = UIImage(named: "astra_rpdk")
    static let astra_3531       = UIImage(named: "astra_3531")
    static let astra_6131       = UIImage(named: "astra_6131")
    static let astra_3321       = UIImage(named: "astra_3321")
    static let astra_5131       = UIImage(named: "astra_5131")
    static let astra_421        = UIImage(named: "astra_421")
    static let profile_icon     = UIImage(named: "profile_icon_view")
    
    static let auto_relay_name          = "ic_auto_relay"
    static let auto_relay               = UIImage(named: auto_relay_name)
    static let script_relay_name        = "ic_relay_script"
    static let script_relay             = UIImage(named: script_relay_name)

}

extension UIImage {
    
    func withColor(color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    var name: String {
        get{
            let asset = self.value(forKey: "_imageAsset") as! UIImageAsset
            return asset.value(forKey: "_assetName") as! String
        }
    }
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
