//
//  Constants.swift
//  SecurityHub
//
//  Created by Timerlan on 17.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class Constants{
    public static let bigFont = UIFont.systemFont(ofSize: 16.0)
    public static let middleFont = UIFont.systemFont(ofSize: 14.0)
    public static let smallFont = UIFont.systemFont(ofSize: 12.0)
    public static let bigColor = UIColor.black
    public static let middleColor = UIColor.gray
    public static let smallColor = UIColor.lightGray
    public static let margin = 16
    public static let marginText = 4
}
