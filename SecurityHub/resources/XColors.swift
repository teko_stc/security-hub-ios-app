//
//  XColors.swift
//  SecurityHub
//
//  Created by Timerlan on 25.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

//Brand
var DEFAULT_COLOR_MAIN_UINT: UInt {
    get {
        switch XTargetUtils.target {
        case .tattelecom:           return 0x2196F3
        case .omicron:              return 0xfbb03c
        case .myuvo:                return 0x6c2124
        case .krasnodar:            return 0x204474
        case .saratov:              return 0x0171b4
        case .dev:                  return 0x0095B6
        case .test:                 return 0x0095B6
        case .security_hub:         return 0x0095B6
        case .samara:               return 0x0000FF
        case .kuzet:                return 0x01a6c4
        case .favorit:              return 0x1a47db
        case .ksb:                  return 0x2991EC
        case .rostov:               return 0x333333
        case .legion:               return 0xff4500
        case .teleplus:             return 0x0095B6
        case .secmatrix:            return 0x3b5e63
        case .akm:                  return 0x0081c5
        }
    }
}
let DEFAULT_COLOR_MAIN = UIColor.colorFromHex(DEFAULT_COLOR_MAIN_UINT)

let DEFAULT_SELECTED = DEFAULT_COLOR_MAIN //UIColor.colorFromHex(0xffffff)
let DEFAULT_UNSELECTED_UINT: UInt = 0x9a9a9a
let DEFAULT_UNSELECTED = UIColor.colorFromHex(DEFAULT_UNSELECTED_UINT)

let DEFAULT_COLOR_GRAY_UINT: UInt   = 0x808080
let DEFAULT_COLOR_GRAY              = UIColor.colorFromHex(DEFAULT_COLOR_GRAY_UINT)
let DEFAULT_COLOR_RED_UINT: UInt    = 0xf44336
let DEFAULT_COLOR_RED               = UIColor.colorFromHex(DEFAULT_COLOR_RED_UINT)
let DEFAULT_COLOR_GREEN_UINT: UInt  = 0x4CAF50
let DEFAULT_COLOR_GREEN             = UIColor.colorFromHex(DEFAULT_COLOR_GREEN_UINT)
let DEFAULT_COLOR_YELLOW_UINT: UInt = 0xF9A825
let DEFAULT_COLOR_YELLOW            = UIColor.colorFromHex(DEFAULT_COLOR_YELLOW_UINT)

let DEFAULT_BACKGROUND          = UIColor(red: 0.9373, green: 0.9373, blue: 0.9569, alpha: 1.0)

let DEFAULT_COLOR_WINDOW_BACKGROUND = UIColor.colorFromHex(0xfcfcfc)
//let DEFAULT_COLOR_WINDOW_BACKGROUND = UIColor.colorFromHex(0x171717)
let DEFAULT_COLOR_WINDOW_ALT_BACKGROUND = UIColor.colorFromHex(0xdfdfdf)


let DEFAULT_COLOR_TEXT_LIGHT = DEFAULT_COLOR_MAIN//UIColor.colorFromHex(0xffffff)
let DEFAULT_COLOR_TEXT_DARK = UIColor.colorFromHex(0x222222)
let DEFAULT_COLOR_SELECT = UIColor.colorFromHex(0x444444)
let DEFAULT_COLOR_LINE = UIColor.colorFromHex(0x444444)


extension UIColor {
    class var inLightGray: UIColor { return colorFromHex(0xc1c1c1) }
}
