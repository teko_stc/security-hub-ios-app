//
//  Color.swift
//  Samara
//
//  Created by Timerlan on 03/09/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

extension UIColor {
    static func colorFromHex(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0,
            green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0,
            blue: ((CGFloat)(rgbValue & 0xFF))/255.0,
            alpha: 1
        )
    }
    
    class var hubBackgroundColor: UIColor { return colorFromHex(0xefeff4) }
    
    class var hubMainColor: UIColor { return colorFromHex(0x0000ff) }
    static var hubMainColorUINT: UInt = 0xb5d3fb
    
    class var hubLightMainColor: UIColor { return colorFromHex(0x3333ff) }
    
    class var hubDarkMainColor: UIColor { return colorFromHex(0x0000cc) }
    
    class var hubRipleBackgroundColor: UIColor { return colorFromHex(0x000099) }
    
    class var hubRipleColor: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.5) }
    
    class var hubTransparent: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.0) }
    
    class var hubGreenColor: UIColor { return colorFromHex(0x4CAF50) }
    class var hubGreenDarkColor: UIColor { return colorFromHex(0x1B5E20) }
    
    class var hubRedColor: UIColor { return colorFromHex(0xf44336) }
    class var hubRedDarkColor: UIColor { return colorFromHex(0xb71c1c) }
    
    class var hubYellowColor: UIColor { return colorFromHex(0xF9A825) }
    class var hubYellowDarkColor: UIColor { return colorFromHex(0xF57F17) }
}
