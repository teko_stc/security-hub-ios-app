//
//  NavigationHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import UIKit
//import RxSwift
import UserNotifications

class NavigationHelper {
    static let shared = NavigationHelper()
    var window = UIWindow()
    var rootViewController : UINavigationController?

    func show(_ controller: UIViewController){
        if #available(iOS 13.0, *) {
            window.overrideUserInterfaceStyle = .light
        }
        window.backgroundColor = DEFAULT_COLOR_WINDOW_BACKGROUND
        rootViewController = UINavigationController(rootViewController : controller)
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
    
    private func initStatusBar() {
        let layer = CALayer()
        layer.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: UIApplication.shared.statusBarFrame.width, height: 0.7)
        layer.backgroundColor = UIColor.gray.withAlphaComponent(0.7).cgColor
        let statusBarView = UIView()
        statusBarView.tag = 453
        statusBarView.layer.addSublayer(layer)
        statusBarView.backgroundColor = UIColor.white
        statusBarView.frame = UIApplication.shared.statusBarFrame
        statusBarView.layer.isHidden = true
        UIApplication.shared.keyWindow?.addSubview(statusBarView)
    }
    
    func statusBar(isHidden: Bool) {
        if let view = UIApplication.shared.keyWindow?.viewWithTag(453) {
            view.layer.isHidden = isHidden
        } else {
            initStatusBar()
            statusBar(isHidden: isHidden)
        }
    }
    
    func showMain(){
//        self.window.rootViewController = MenuController()
        self.window.rootViewController = UINavigationController(rootViewController: XTabBarController())
        self.window.makeKeyAndVisible()
    }
    
    func showNew(_ controller: UIViewController){
//        self.window.rootViewController = MenuController()
        self.window.rootViewController = UINavigationController(rootViewController: controller)
        self.window.makeKeyAndVisible()
    }
}
