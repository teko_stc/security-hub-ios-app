//
//  EventCameraView.swift
//  SecurityHub
//
//  Created by Timerlan on 20.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class EventCameraView: UIView {
    
    lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var locationView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }()
    
    lazy var locationTView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }()
    
    lazy var descView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        return view
    }()
    
    lazy var timeView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        return view
    }()
    
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconView)
        addSubview(locationView)
        addSubview(locationTView)
        addSubview(descView)
        addSubview(timeView)
        addSubview(line)
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10)
            make.height.width.equalTo(70)
        }
        locationView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(-10)
            make.height.equalTo(20)
        }
        locationTView.snp.remakeConstraints{ make in
            make.top.equalTo(locationView.snp.bottom).offset(5)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(0)
            make.height.equalTo(15)
        }
        descView.snp.remakeConstraints{ make in
            make.top.equalTo(locationTView.snp.bottom).offset(5)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(0)
            make.height.equalTo(15)
        }
        timeView.snp.remakeConstraints{ make in
            make.top.equalTo(descView.snp.bottom).offset(5)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(0)
            make.bottom.equalTo(line.snp.top).offset(-10)
            make.height.equalTo(15)
        }
        line.snp.remakeConstraints{make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}

