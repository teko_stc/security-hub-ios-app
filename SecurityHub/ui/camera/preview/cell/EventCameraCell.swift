//
//  EventCameraCell.swift
//  SecurityHub
//
//  Created by Timerlan on 20.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class EventCameraCell: BaseCell<EventCameraView>{
    static let cellId = "EventCameraCell"
    private var event: (event: Events, iv: IvVideo)!
    
    func setContent(_ obj: (event: Events, iv: IvVideo)) {
        self.event = obj
        mainView.iconView.image = UIImage(named: event.event.iconBig)
        mainView.descView.text = event.event.affect_desc
        mainView.timeView.text = event.event.time == 0 ? "--:--:--" : event.event.time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)
        _ = DataManager.shared.getName(device: event.event.device, section: event.event.section , zone: event.event.zone)
        .subscribe(onNext: { name in self.mainView.locationTView.text = name })
        mainView.locationView.text = DataManager.shared.getNameSites(event.event.device)
    }
    
    @objc func camClick(){
        
    }
}

