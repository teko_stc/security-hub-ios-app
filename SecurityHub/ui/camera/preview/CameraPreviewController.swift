//
//  CameraPreviewController.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import RxSwift
import MobilePlayer

class CameraPreviewController: BaseController<CameraPreviewView>, UITableViewDelegate, UITableViewDataSource {
    private var camera: Camera
    private var events: [(event: Events, iv: IvVideo)] = []
    
    init(_ camera: Camera) {
        self.camera = camera
        super.init(nibName: nil, bundle: nil)
        mainView.tableView.register(EventCameraCell.self, forCellReuseIdentifier: EventCameraCell.cellId)
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.all)
        title = camera.name
        loadPreview()
        
        self.mainView.player.player?.play()
    }
    
    func loadPreview() {
        self.mainView.player.player = AVPlayer(url: URL(string: IvideonApi.getLiveUrl(accessToken: DataManager.settingsHelper.ivideonToken, id: self.camera.id))!)
        _ = DataManager.shared.getCamSet(self.camera.id)
            .subscribe(onNext: { set in
                self.mainView.player.player?.isMuted = !set.audio
                self.mainView.player.player?.play()
            })
        _ = DataManager.shared.getEvents(camId: camera.id)
            .map({ (r) -> [(event: Events, iv: IvVideo)] in
                var _r = r
                _r.sort { (r1, r2) -> Bool in return r1.event.time > r2.event.time }
                return _r
            })
            .subscribe(onNext: { res in
                self.events = res
                self.mainView.tableView.reloadData()
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.mainView.player.player?.pause()
    }
    
    func endLoad() {
        //hideWaiting()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return events.count }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EventCameraCell = (tableView.dequeueReusableCell(withIdentifier: EventCameraCell.cellId, for: indexPath) as? EventCameraCell)!
        cell.setContent(events[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if events[indexPath.row].iv.isError { return AlertUtil.errorAlert(events[indexPath.row].iv.result) }
        guard let url = URL(string: events[indexPath.row].iv.result), let nV = navigationController else { return AlertUtil.errorAlert("ERROR".localized()) }
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewControllerRotatable()
        playerController.player = player
        nV.present(playerController, animated: true) {
            player.play()
        }
    }
}

