//
//  SelectZoneController.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import UIKit

class SelectZoneController: XBaseController<EmptyListView>, UITableViewDataSource, UITableViewDelegate {
    let list: [SelectElement]
    let obj: Any?
    
    init(list: [SelectElement], title: String, obj: Any?) {
        self.list = list
        self.obj = obj
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
  
    override func loadView() {
        super.loadView()
        xView.table.register(SelectZoneCell.self, forCellReuseIdentifier: SelectZoneCell.cellId)
        xView.table.delegate = self
        xView.table.dataSource = self
        xView.table.backgroundView = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DataManager.shared.updateSites()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return list.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SelectZoneCell = tableView.dequeueReusableCell(withIdentifier: SelectZoneCell.cellId, for: indexPath) as! SelectZoneCell
        cell.setContent(element: list[indexPath.row], obj: obj)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (list[indexPath.row].selected == nil){
            if list[indexPath.row].list.count != 0 {
                navigationController?.pushViewController(SelectZoneController(list: list[indexPath.row].list, title: list[indexPath.row].name, obj: self.obj), animated: false)
            }
        }
    }
}

class SelectElement{
    var name = ""
    var selected: Bool?
    var list: [SelectElement] = []
    var obj: Any?
    
    init(name: String, list: [SelectElement]) {
        self.name = name
        self.list = list
    }
    
    init(name: String, selected: Bool, obj: Any) {
        self.name = name
        self.selected = selected
        self.obj = obj
    }
}
