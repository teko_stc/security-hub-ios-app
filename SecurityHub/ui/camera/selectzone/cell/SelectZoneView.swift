//
//  SelectZoneView.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SelectZoneView: UIView {
    
    var margin = 10
    
    lazy var name: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.numberOfLines = 2
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    lazy var menu: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_right_arrow")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var swi: UISwitch = {
        let view = UISwitch()
        view.onTintColor = UIColor.hubMainColor
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(name)
        addSubview(swi)
        addSubview(menu)
    }
    
    override func updateConstraints() {
        name.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(margin)
            make.trailing.equalTo(menu.snp.leading).offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
        menu.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(margin)
            make.trailing.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
            make.width.equalTo(30)
        }
        swi.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(margin)
            make.trailing.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
        super.updateConstraints()
    }
    
    func showSwi() {
        name.textColor = UIColor.black
        swi.isHidden = false
        menu.isHidden = true
    }
    
    func showArrow() {
        name.textColor = UIColor.black
        swi.isHidden = true
        menu.isHidden = false
    }
    
    func hide() {
        name.textColor = UIColor.lightGray
        swi.isHidden = true
        menu.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
}

