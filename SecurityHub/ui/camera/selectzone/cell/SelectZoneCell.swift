//
//  SelectZoneCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import UIKit

class SelectZoneCell: BaseCell<SelectZoneView> {
    public static let cellId = "SelectZoneCell"
    var element: SelectElement!
    var obj: Any?
    
    func setContent(element: SelectElement, obj: Any?){
        self.element = element
        self.obj = obj
        mainView.name.text = element.name
        if (element.selected != nil){
            mainView.showSwi()
            mainView.swi.isOn = element.selected!
        }else{
            if (element.list.count == 0){
                mainView.hide();
                mainView.name.text = element.name + "\n" + "zone_no_zones".localized()
            }else{
                mainView.showArrow()
            }
        }
        mainView.swi.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func switchChanged(){
        if let zon = element.obj as? Zones{
            if let cam = obj as? Camera{
            if !element.selected!{
                _ = DataManager.shared.bindCam(zone: zon, cam: cam.id)
                .subscribe(onNext: { (r) in
                    DataManager.shared.getIviRelations()
                    self.element.selected = true
                }, onError: { (e) in
                    self.mainView.swi.isOn = false
                })
            }else{
                _ = DataManager.shared.delRelation(zone: zon)
                .subscribe(onNext: { res in
                    self.element.selected = false
                    DataManager.shared.getIviRelations()
                }, onError: { (e) in
                    self.mainView.swi.isOn = true
                })
                }
            }
        }else if let res = element.obj as? (Sections,[Int64]){
            let user = obj as! DeviceUsers
            var sections = res.1
            if element.selected!{ if let index = sections.firstIndex(of: res.0.section) { sections.remove(at: index) }
            }else{ sections.append(res.0.section) }
            _ = DataManager.shared.setHozOrgan(device: user.device, name: user.name, index: user.userIndex, sections: sections)
            .do(onNext: {r in
                self.element.selected = !self.element.selected!
                self.element.obj = (res.0,sections)
            },onError: { (e) in self.mainView.swi.isOn = false })
            .subscribe()
        }
    }
}
