//
//  CameraListController.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//


import UIKit
import RxSwift

class CameraListController : XBaseController<EmptyListView>, UITableViewDataSource, UITableViewDelegate {
    private var cameras: [Camera] = []
    
    override func loadView() {
        super.loadView()
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "drawer_item_cameras".localized()
        xView.table.register(CameraCell.self, forCellReuseIdentifier: CameraCell.cellId)
        xView.table.dataSource = self
        xView.table.delegate = self
        xView.table.reloadData()
        xView.refreshControl.addTarget(self, action: #selector(update), for: UIControl.Event.valueChanged)
        if (DataManager.shared.getUser().login != "ios" || DataManager.shared.getUser().login != "test_ios") {
            rightButton(#imageLiteral(resourceName: "menu_exit"))
        }
        initNeedAuto()
        load()
        NotificationCenter.default.addObserver(forName: HubNotification.iviAuth, object: nil, queue: OperationQueue.main) { notification in
            self.load()
        }
    }
    
    private let label = UILabel()
    private let button = ZFRippleButton()
    private func initNeedAuto() {
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        label.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.medium)
        label.text = "IV_NOT_LOGGED_IN".localized()
        label.isHidden = true
        xView.addSubview(label)
        label.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview().offset(-30)
            make.centerX.equalToSuperview()
        }
        button.setTitle("IV_LOGIN".localized(), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        button.backgroundColor = DEFAULT_COLOR_MAIN
        button.rippleBackgroundColor = DEFAULT_COLOR_MAIN
        button.rippleColor = DEFAULT_SELECTED.withAlphaComponent(0.4)
        button.shadowRippleEnable = false
        button.ripplePercent = 1
        button.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        button.layer.shadowOpacity = 0.7 // 2
        button.layer.shadowRadius = 3 // 3
        button.layer.shadowColor = UIColor.lightGray.cgColor // 4
        button.layer.cornerRadius = 15
        button.isHidden = true
        button.addTarget(self, action: #selector(click), for: .touchUpInside)
        xView.addSubview(button)
        button.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview().offset(30)
            make.height.equalTo(50)
            make.width.equalTo(label.snp.width).offset(-40)
            make.centerX.equalToSuperview()
        }
    }
    @objc private func click(){
        navigationController?.pushViewController(IvideonAuthController(), animated: false)
        needAuto(false)
    }

    private func load(){
        showLoader()
        _ = DataManager.shared.getCameraList().subscribe(onNext: { (cameras) in
            self.hideLoader()
            self.xView.refreshControl.endRefreshing()
            if cameras == nil {
                self.needAuto(true)
                self.cameras = []
                self.xView.table.reloadData()
            } else{
                self.needAuto(false)
                self.cameras = cameras!
                self.xView.table.reloadData()
            }
        },onError: { error in
            self.needAuto(false)
            self.cameras = []
            self.xView.table.reloadData()
            self.hideLoader()
            AlertUtil.errorAlert(error.localizedDescription)
        })
    }
    
    private func needAuto(_ isHidden: Bool){
        label.isHidden = !isHidden
        button.isHidden = !isHidden
        xView.table.isHidden = isHidden
        rightButton.isHidden = isHidden
    }
    
    @objc func update(){ load() }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return cameras.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CameraCell = tableView.dequeueReusableCell(withIdentifier: CameraCell.cellId, for: indexPath) as! CameraCell
        cell.setContent(cameras[indexPath.row])
        cell.setNV(navigationController)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(CameraPreviewController(cameras[indexPath.row]), animated: false)
    }
    
    override func rightClick() {
        let alert = AlertUtil.exitAlert( {
            _ = DataManager.shared.exitIvi().subscribe(onNext: {res in
                self.needAuto(true)
                self.cameras = []
                self.xView.table.reloadData()
            })
        }, mes: "CAMERA_EXIT_IV_DIALOG_MES".localized())
        navigationController?.present(alert, animated: false)
    }
}

