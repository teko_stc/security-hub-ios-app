//
//  CameraCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import UIKit
import RxSwift

class CameraCell: BaseCell<CameraView>{
    static let cellId = "CameraCell"
    private var camera: Camera!
    private var set: CamSet!
    
    func setContent(_ obj: Camera) {
        selectionStyle = .none
        self.camera = obj
        _ = camera.getPreview()
            .observeOn(MainScheduler.init())
            .subscribe(onNext: { img in self.mainView.img.image = img })
        mainView.name.text = camera.name
        mainView.status.text = camera.online ? "CAMERA_STATUS_ONLINE".localized() : "CAMERA_STATUS_OFFLINE".localized()
        mainView.status.textColor = camera.online ? UIColor.hubMainColor : UIColor.red
        //mainView.time.text = Int(camera.lastOnline).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)
        mainView.menu.addTarget(self, action: #selector(menu_pressed), for: .touchUpInside)
    }
    
    @objc func menu_pressed(){
        let _ = DataManager.shared.getCamSet(camera.id).subscribe(onNext: { set in
            self.set = set
            let al = AlertUtil.myXAlert("\("CAMERA_SETTING_CAT_SETTINGS".localized()): \(self.camera.name ?? "")", messages: [
                "CAMERA_SETTINGS_RENAME".localized(),
                "CAMERA_BINDING_TITLE".localized(),
                set.audio ? "menu_camera_audio_on".localized() : "menu_camera_audio_off".localized(),
                "\("CAMERA_SETTINGS_VIDEO_Q".localized()): \(set.q == 0 ? "stream_qualities_status_list[1]".localized() : set.q == 1 ? "stream_qualities_status_list[2]".localized() : "stream_qualities_status_list[3]".localized())"
                ], voids: [self.rename,self.relation, self.audio, self.q])
            self.nV?.present(al, animated: false)
        })
    }
    
    private func rename(){
        AlertUtil.renameXAlert(text: camera.name) { (text) in
            _ = IvideonApi.updateCameraName(self.camera, accessToken: DataManager.settingsHelper.ivideonToken, value: text)
                .subscribe(onNext: { res in
                    if res { self.mainView.name.text = text }
                    else{ AlertUtil.errorAlert("error_rename".localized()) }
                })
        }
    }
    
    private func relation(){
        _ = DataManager.shared.getZoneRelation(cam: camera.id)
            .subscribe(onNext: { res in
                self.nV?.pushViewController(SelectZoneController(list: res, title: "CAMERA_TITLE_CHOOSE_ZONE".localized(), obj: self.camera), animated: false)
            })
    }
    
    private func audio(){
        set.audio = !set.audio
        DataManager.shared.updCamSet(set)
    }
    
    private func q(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        alert.addAction(UIAlertAction(title: "stream_qualities_status_list[1]".localized(), style: .default, handler: { a in
            self.set.q = 0;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: "stream_qualities_status_list[2]".localized(), style: .default, handler: { a in
            self.set.q = 1;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: "stream_qualities_status_list[3]".localized(), style: .default, handler: { a in
            self.set.q = 2;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        self.nV?.present(alert, animated: false)
    }
}
