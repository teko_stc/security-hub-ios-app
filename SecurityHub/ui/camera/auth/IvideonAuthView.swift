//
//  AuthView.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import WebKit

class IvideonAuthView: BaseView {
    lazy var webView: WKWebView = {
        let view = WKWebView()
        view.isOpaque = false
        view.backgroundColor = UIColor.colorFromHex(0x1a96e5)
        return view
    }()
    
    override func setContent() {
        super.setContent()
        contentView.alwaysBounceVertical = false
        contentView.alwaysBounceHorizontal = false
        contentView.addSubview(webView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        webView.snp.remakeConstraints { make in
            make.top.left.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    private lazy var backTopView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.colorFromHex(0x1a96e5)
        return view
    }()
    func addBackTopView(_ view: UIView) {
        view.addSubview(backTopView)
        let backHeight = UIApplication.shared.statusBarFrame.height
        backTopView.snp.remakeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalTo(backHeight)
        }
    }
}
