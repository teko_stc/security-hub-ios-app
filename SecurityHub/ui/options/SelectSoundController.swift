//
//  SelectSoundController.swift
//  SecurityHub
//
//  Created by Timerlan on 18/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import AVFoundation

class SelectSoundController: XBaseViewController<XSelectSoundView> {
    override var tabBarIsHidden: Bool { true }

    private let path: String?
    private let sounds: [ String : String]
    private let void: (_ name: String?)->Void    
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left),
        rightViews: [XNavigationViewRightViewBuilder(title: "button_save".localized(), font: UIFont(name: "Open Sans", size: 19), color: UIColor.colorFromHex(0x414042), viewTapped: self.rightClick)]
    )
    
    init(path: String?, sounds: [String:String]? = nil, void: @escaping ((_ name: String?)->Void)) {
        self.path = path
        self.void = void
        self.sounds = sounds ?? [  "default" : "sounds_default".localized(),
                                   "coin.wav" : "sounds_coin".localized(),
                                   "sirena.wav": "sounds_sirena".localized(),
                                   "smeh.wav" : "sounds_haha".localized(),
                                   "sms.wav" : "sounds_sms".localized()]
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        //TODO
        title = "NOTIF_PREF_OREO_SOUND_TITLE".localized()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.selected = path
        mainView.setMusic(Array(sounds))
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            if let player = mainView.player { player.stop() }
            void(mainView.selected)
        }
    }
    
    func rightClick() {
        navigationController?.popViewController(animated: true)
    }
}

struct SoundInfo {
    let path: String
    let name: String
}

class XSelectSoundView: XBaseView, UITableViewDelegate, UITableViewDataSource {
    private var musics: [(key: String, value: String)] = []
    var player: AVAudioPlayer?
    var selected: String?
    private lazy var tableView : UITableView = {
        let view = UITableView()
        view.backgroundColor = DEFAULT_COLOR_WINDOW_BACKGROUND
        view.separatorStyle = .none
        view.estimatedRowHeight = 130
        view.register(XSelectSoundCell.self, forCellReuseIdentifier: XSelectSoundCellId)
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    override func setContent() {
        xView.addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return musics.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: XSelectSoundCellId, for: indexPath) as? XSelectSoundCell else {
            return tableView.dequeueReusableCell(withIdentifier: XSelectSoundCellId, for: indexPath)
        }
        cell.setContent(musics[indexPath.row].value, check: musics[indexPath.row].key == selected)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        let info = musics[indexPath.row]
        let arr = info.key.split(separator: ".")
        if let player = player { player.stop() }
        selected = info.key
        guard arr.count > 1, let audioFilePath = Bundle.main.path(forResource: String(arr[0]), ofType: String(arr[1])) else { return tableView.reloadData() }
        let audioFileUrl = NSURL.fileURL(withPath: audioFilePath)
        if let audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl, fileTypeHint: nil) {
            player = audioPlayer
            audioPlayer.play()
        }
        tableView.reloadData()
    }
    
    func setMusic(_ musics: [(key: String, value: String)]) {
        self.musics = musics
        tableView.reloadData()
    }
}

let XSelectSoundCellId = "XSelectSoundCellId"
class XSelectSoundCell: UITableViewCell {
    private var titleView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        return view
    }()
    private var selectView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIView.ContentMode.scaleAspectFit
        view.image = XImages.view_check
        return view
    }()
    private var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(titleView)
        addSubview(selectView)
        addSubview(lineView)
        updateConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        titleView.snp.remakeConstraints { (make) in
            make.top.left.equalTo(16)
            make.bottom.equalTo(-16)
        }
        selectView.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(titleView.snp.right)
            make.right.equalTo(-16)
            make.height.width.equalTo(30)
        }
        lineView.snp.remakeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(1)
        }
    }
    
    func setContent(_ name: String, check: Bool) {
        titleView.text = name
        selectView.isHidden = !check
    }
}
