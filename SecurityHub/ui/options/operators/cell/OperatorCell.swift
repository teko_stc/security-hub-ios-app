//
//  OperatorCell.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import SCLAlertView

class OperatorCell : BaseCell<OperatorView>{
    static let cellId = "OperatorCell"
    private var operators: Operators!
    
    override func setContent(_ obj: SQLCommand, isEnd: Bool = false) {
        operators = obj as? Operators
        mainView.titleView.text = operators.name
        mainView.loginView.text = "\("operator_cell_login".localized()): \(operators.login)"
        mainView.pravView.text = "operator_cell_permission".localized() + (
            operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? "OPERATOR_ADMIN".localized() :
                operators.roles == 40960 ? "OPERATOR_VIEW_AND_CONTROL".localized() : "OPERATOR_VIEW_ONLY".localized());
        mainView.statusView.text = "PLA_USER_STATUS".localized() + " " + (operators.active == 1 ? "PLA_ACTIVE".localized() : "PLA_INACTIVE".localized())
        mainView.menuBtn.addTarget(self, action: #selector(menu), for: .touchUpInside)
    }
    
    @objc func menu(){
        let a = AlertUtil.myXAlert(operators.name ,messages: [
            "menu_operator_about".localized(),
            "PLA_CHANGE_NAME".localized(),
            "PLA_CHANGE_LOGIN".localized(),
            "PLA_CHANGE_PASS".localized(),
            (operators.active == 1 ? "PLA_DISABLE_USER".localized() : "PLA_ENABLE_USER".localized()),
            "CHANGE_USER_ROLES".localized(),
            "PLA_DELETE_USER".localized()
            ], voids: [ user,name,login,pas,active,prav,del ])
        nV?.present(a, animated: false)
    }
    
    private func user(){
        let mes = "\("OP_NAME".localized()) \(operators.name)\n\n" +
        "\("operator_login".localized()): \(operators.login)\n\n" +
        "operator_cell_permission".localized() + (
            operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? "OPERATOR_ADMIN".localized() :
                operators.roles == 40960 ? "OPERATOR_VIEW_AND_CONTROL".localized() : "OPERATOR_VIEW_ONLY".localized()) + "\n\n" +
        "OBJECT_COUNT_DOTS".localized() + String(DataManager.shared.getSitesCount()) + "\n";
        AlertUtil.infoAlert(title: "", mes)
    }
    private func name(){
       AlertUtil.renameXAlert(text: operators.name) { (name) in
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: name, name: "name").subscribe()
        }
    }
    private func pas(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("operator_input_password".localized())
        alert.addButton("PLA_CHANGE_PASS".localized()) {
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: txt.text?.md5 ?? "", name: "password").subscribe()
        }
        alert.showEdit("PLA_CHANGE_PASS".localized(), subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized(), colorStyle: UIColor.hubMainColorUINT)
    }
    private func login(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("operator_input_login".localized())
        txt.keyboardType = .asciiCapable
        alert.addButton("PLA_CHANGE_LOGIN".localized()) {
            if (txt.text ?? "").contains(where: {$0 == "@"}) { AlertUtil.errorAlert("error_login_hororgan_no_dog".localized()) }else{
                _ = DataManager.shared.changeOperator(id: self.operators.id, value: txt.text ?? "", name: "login").subscribe()
            }
        }
        alert.showEdit("PLA_CHANGE_LOGIN".localized(), subTitle: "", closeButtonTitle: "N_BUTTON_CANCEL".localized(), colorStyle: UIColor.hubMainColorUINT)
    }
    private func active(){
        _ = DataManager.shared.changeOperator(id: self.operators.id, value: (operators.active == 0), name: "active").subscribe()
    }
    private func prav(){
        let a = AlertUtil.myXAlert(messages: ["OPERATOR_VIEW_AND_CONTROL".localized(), "OPERATOR_VIEW_ONLY".localized()], voids: [{
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: 40960, name: "roles").subscribe()
            },{
                _ = DataManager.shared.changeOperator(id: self.operators.id, value: 8192, name: "roles").subscribe()
            }])
        nV?.present(a, animated: false)
    }
    private func del(){
        AlertUtil.deleteAlert({
            _ = DataManager.shared.delOperator(id: self.operators.id).subscribe()
        }, text: "\("OP_DEL".localized()) \(self.operators.name)?")
    }
}
