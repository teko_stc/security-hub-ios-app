//
//  OperatorView.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class OperatorView: UIView {
    static let cellHeight: CGFloat = 117.53
    
    lazy var titleView: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 18)
        view.numberOfLines = 1
        return view
    }()
    
    lazy var loginView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "sa"
        return view
    }()
    
    lazy var pravView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "--"
        return view
    }()
    
    lazy var statusView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "--"
        return view
    }()
    
    lazy var menuBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "points") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.lightGray
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(titleView)
        addSubview(loginView)
        addSubview(statusView)
        addSubview(pravView)
        addSubview(menuBtn)
    }
   
    override func updateConstraints() {
        super.updateConstraints()
        titleView.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(15)
            make.height.equalTo(21)
        }
        loginView.snp.remakeConstraints{ make in
            make.top.equalTo(titleView.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        pravView.snp.remakeConstraints{ make in
            make.top.equalTo(loginView.snp.bottom)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        statusView.snp.remakeConstraints{ make in
            make.top.equalTo(pravView.snp.bottom)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview().offset(-15)
        }
        menuBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(15)
            make.leading.equalTo(titleView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.width.height.equalTo(35)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
}

