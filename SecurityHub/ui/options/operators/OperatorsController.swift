//
//  OperatorsController.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class OperatorsController: BaseController<BaseListView<OperatorView, OperatorCell>>{
   
    override func loadView() {
        super.loadView()
        title = "PUA_TITLE".localized()
        mainView.setNV(navigationController)
        mainView.initTable(cellId: OperatorCell.cellId, cellHeight: OperatorView.cellHeight, refresh: update)
        load()
        rightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func load(){
        _ = DataManager.shared.getOperators()
        .subscribe(onNext: { (operators,type) in
            self.mainView.updateList(operators, type: type)
        })
    }
    
    private func update(){ DataManager.shared.updateOperators() }
    
    override func rightClick() {
        guard let nV = navigationController else { return }
        
        nV.pushViewController(AddOperatorController(), animated: false)
    }
}
