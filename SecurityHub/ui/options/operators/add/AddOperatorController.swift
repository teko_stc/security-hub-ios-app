//
//  AddOperatorController.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class AddOperatorController: XBaseViewController<AddOperatorView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25))
    )
    
    override func loadView() {
        super.loadView()
        title = "title_add_operator".localized()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.listener = add
    }
    
    @objc func add(){
        if (mainView.loginField.text ?? "").contains(where: {$0 == "@"}) {
            AlertUtil.errorAlert("error_login_hororgan_no_dog".localized())
        }else{
            _ = DataManager.shared.addOperator(
                name: mainView.nameField.text!,
                login: mainView.loginField.text!,
                password: mainView.pasField.text!,
                role: mainView.segmentCont.selectedSegmentIndex == 0 ? 8192 : 40960
            )
            .subscribe(onNext: { res in
                guard let nV = self.navigationController else { return }
                nV.popViewController(animated: false)
            })
        }
    }
}
