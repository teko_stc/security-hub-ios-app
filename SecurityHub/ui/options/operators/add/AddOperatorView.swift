//
//  AddOperatorView.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import BarcodeScanner

class AddOperatorView: BaseView, UITextFieldDelegate{
    
    private lazy var lineH: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineC: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineF: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    
    private lazy var nameLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "OP_NAME".localized()
        return view
    }()
    
    lazy var nameField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "operator_input_name".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
//        view.keyboardType = .asciiCapable
        view.returnKeyType = UIReturnKeyType.next
        return view
    }()
    
    private lazy var loginLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "OP_LOGIN".localized()
        return view
    }()
    
    lazy var loginField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "operator_input_login".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
        view.keyboardType = .asciiCapable
        view.returnKeyType = UIReturnKeyType.next
        return view
    }()
    
    private lazy var pasLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "OP_PASS".localized()
        return view
    }()
    
    lazy var pasField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "operator_input_password".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
//        view.keyboardType = .asciiCapable
        view.returnKeyType = UIReturnKeyType.go
        return view
    }()
    
    lazy var segmentCont: UISegmentedControl = {
        let view = UISegmentedControl(items: ["PUA_ONLY_READ".localized(), "PUS_READ_AND_WRITE".localized()])
        view.selectedSegmentIndex = 0
        view.layer.cornerRadius = 5.0
        view.backgroundColor = UIColor.clear
        view.tintColor = UIColor.hubMainColor
        return view
    }()
    
    lazy var addBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubRipleBackgroundColor
        view.ripplePercent = 1
        view.setTitle("N_BUTTON_ADD".localized(), for: .normal)
        return view
    }()
    
    override func setContent() {
        setBottomBackgroundColor(UIColor.hubBackgroundColor)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        nameField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        nameField.delegate = self
        loginField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        loginField.delegate = self
        pasField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        pasField.delegate = self

        addSubview(nameLable)
        addSubview(nameField)
        addSubview(lineH)
        addSubview(loginLable)
        addSubview(loginField)
        addSubview(lineC)
        addSubview(pasLable)
        addSubview(pasField)
        addSubview(lineF)
        addSubview(segmentCont)
        addSubview(addBtn)
        addBtn.addTarget(self, action: #selector(add), for: .touchUpInside)
        textFieldDidChange(textField: nameField)
    }
    
    @objc func add(){ listener?() }
    
    public var listener: (() -> Void)?
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if nameField == textField { loginField.becomeFirstResponder() }
        else if loginField == textField {  pasField.becomeFirstResponder() }
        else if nameField.text?.count ?? 0 > 0 && loginField.text?.count ?? 0 > 0 && pasField.text?.count ?? 0 > 0 {
            listener?()
        }else{
            AlertUtil.errorAlert("error_add_hozorgan".localized())
        }
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        if nameField.text?.count ?? 0 > 0 && loginField.text?.count ?? 0 > 0 && pasField.text?.count ?? 0 > 0 {
            addBtn.isEnabled = true
            addBtn.setTitleColor(UIColor.white, for: .normal)
        }else{
            addBtn.isEnabled = false
            addBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        nameLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.centerY.equalTo(nameField.snp.centerY)
            make.width.equalTo(80)
        }
        nameField.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.leading.equalTo(nameLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(45)
        }
        lineH.snp.remakeConstraints{ make in
            make.top.equalTo(nameField.snp.bottom)
            make.leading.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        loginLable.snp.remakeConstraints{ make in
            make.centerY.equalTo(loginField.snp.centerY)
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(80)
        }
        loginField.snp.remakeConstraints{ make in
            make.top.equalTo(lineH.snp.bottom)
            make.leading.equalTo(loginLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(45)
        }
        lineC.snp.remakeConstraints{ make in
            make.top.equalTo(loginField.snp.bottom)
            make.leading.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        pasLable.snp.remakeConstraints{ make in
            make.centerY.equalTo(pasField.snp.centerY)
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(80)
        }
        pasField.snp.remakeConstraints{ make in
            make.top.equalTo(lineC.snp.bottom)
            make.leading.equalTo(pasLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(45)
        }
        lineF.snp.remakeConstraints{ make in
            make.top.equalTo(pasField.snp.bottom)
            make.leading.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        segmentCont.snp.remakeConstraints{ make in
            make.top.equalTo(lineF.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().offset(-30)
            make.height.equalTo(45)
        }
        addBtn.snp.remakeConstraints{ make in
            make.top.equalTo(segmentCont.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            make.height.equalTo(45)
            make.width.equalToSuperview().offset(-30)
        }
    }
}

