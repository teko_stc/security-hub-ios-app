//
//  ProfileController.swift
//  SecurityHub
//
//  Created by Dan on 13.02.2020.
//  Copyright © 2020 TEKO. All rights reserved.
//

import UIKit

class ProfileController: BaseController<BaseListView<OperatorView, OperatorCell>>{
   
    override func loadView() {
        super.loadView()
        title = "PROFILE_DOMAIN_USERS".localized()
        mainView.setNV(navigationController)
        mainView.initTable(cellId: OperatorCell.cellId, cellHeight: OperatorView.cellHeight, refresh: update)
        load()
        rightButton()
    }
    
    private func load(){
        _ = DataManager.shared.getOperators()
        .subscribe(onNext: { (operators,type) in
            self.mainView.updateList(operators, type: type)
        })
    }
    
    private func update(){ DataManager.shared.updateOperators() }
    
    override func rightClick() {
        guard let nV = navigationController else { return }
        
        nV.pushViewController(AddOperatorController(), animated: false)
    }
}

