//
//  OptionsCell.swift
//  SecurityHub
//
//  Created by Timerlan on 21.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
//import RxSwift

class OptionsCell: BaseCell<OptionsView>{
    static let cellId = "OptionsCell"
    private var option: Option!
    
    func setContent(_ obj: Option) {
        option = obj
        mainView.title.text = option.title
        mainView.subTitle.text = option.subtitle
        mainView.switchView.isOn = option.enable ?? false
        mainView.line.isHidden = option.subtitle == nil
        mainView.subTitle.isHidden = option.subtitle == nil
        mainView.switchView.isHidden = option.enable == nil
        mainView.title.textColor = option.isT ? UIColor.hubMainColor : UIColor.black
        mainView.switchView.addTarget(self, action: #selector(swi), for: .valueChanged)
        
        if obj.isT { selectionStyle = .none }
    }
    
    @objc func swi(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: OptionsCell.cellId), object: option.title)
    }
}

class Option {
    var title: String
    var subtitle: String?
    var enable: Bool?
    var isT: Bool
    
    init(title: String, subtitle: String? = nil, enable: Bool? = nil, isT: Bool = false){
        self.title = title
        self.subtitle = subtitle
        self.enable = enable
        self.isT = isT
    }
}
