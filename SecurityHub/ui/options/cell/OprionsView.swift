//
//  OprionsView.swift
//  SecurityHub
//
//  Created by Timerlan on 21.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class OptionsView: UIView {
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 16.0)
        return view
    }()
    
    lazy var subTitle: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textColor = UIColor.gray
        view.font = UIFont.systemFont(ofSize: 14.0)
        return view
    }()
    
    lazy var switchView: UISwitch = {
        let view = UISwitch()
        view.onTintColor = UIColor.hubMainColor
        return view
    }()
    
    lazy var line: UIView = {
        let view = UILabel()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.hubBackgroundColor
        addSubview(title)
        addSubview(subTitle)
        addSubview(switchView)
        addSubview(line)
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        title.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(10)
            make.trailing.equalTo(switchView.snp.leading).offset(-10)
        }
        subTitle.snp.remakeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(10)
        }
        switchView.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(subTitle.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(-10)
            make.width.equalTo(50)
        }
        line.snp.remakeConstraints{ make in
            make.top.equalTo(subTitle.snp.bottom).offset(10)
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}
