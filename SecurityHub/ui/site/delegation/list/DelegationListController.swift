//
//  DelegationListController.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class DelegationListController: XBaseController<EmptyListView> , UITableViewDataSource {
    private let site_id: Int64
    private var delegations: [HubSiteDelegate] = []
    private var obs: Any?
    
    init(site_id: Int64, name: String) {
        self.site_id = site_id
        super.init(nibName: nil, bundle: nil)
        title = "TITLE_DELEGATED".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        xView.table.register(DelegationCell.self, forCellReuseIdentifier: DelegationCell.cellId)
        xView.table.dataSource = self
        xView.table.reloadData()
        xView.refreshControl.addTarget(self, action: #selector(load), for: UIControl.Event.valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(load), name: HubNotification.delegationUpdate(site_id), object: nil)
        load()
    }
    
    @objc private func load(){
        showLoader()
        _ = DataManager.shared.delegate_sites(site_id: site_id)
        .subscribe(onNext: { delegations in
            self.delegations = delegations
            self.hideLoader()
            self.xView.refreshControl.endRefreshing()
            self.xView.table.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return delegations.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DelegationCell = tableView.dequeueReusableCell(withIdentifier: DelegationCell.cellId, for: indexPath) as! DelegationCell
        cell.setContent(delegations[indexPath.row], site: site_id)
        cell.nV = navigationController
        return cell
    }
}
