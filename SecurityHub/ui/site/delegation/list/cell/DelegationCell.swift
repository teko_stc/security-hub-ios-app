//
//  DelegationCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class DelegationCell: BaseCell<DelegationViewCell> {
    static let cellId = "DelegationCell"
    private var siteDel: HubSiteDelegate!
    private var site: Int64!
    
    func setContent(_ obj: HubSiteDelegate, site: Int64) {
        siteDel = obj
        self.site = site
        mainView.name.text = siteDel.delegated_domain_name
        mainView.domen.text = "\("DELEGATE_DOMAIN_NUMBER_TEXT".localized()) \(String(siteDel.delegated_domain))"
        mainView.menuBtn.addTarget(self, action: #selector(menuClick), for: .touchUpInside)
    }
    
    @objc func menuClick(){
        let al = AlertUtil.myXAlert(messages: ["MENU_REVOKE_DELEGATION".localized()], voids: [delete])
        nV?.present(al, animated: false)
    }
    
    private func delete(){
        AlertUtil.deleteAlert({
            _ = DataManager.shared.delegate_delete(site: self.site, domain_id: self.siteDel.delegated_domain)
            .subscribe(onNext:{ r in NotificationCenter.default.post(name: HubNotification.delegationUpdate(self.site), object: nil)})
        }, text: "\("MESSAGE_REVOKE_DELEGATION".localized()) \(siteDel.delegated_domain)?")
    }
}
