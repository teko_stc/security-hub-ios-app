//
//  DelegationController.swift
//  SecurityHub test
//
//  Created by Timerlan on 08.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class DelegationController: BaseController<DelegationView> {
    private let site_id: Int64
    private let name: String
    
    init(site_id: Int64, name: String) {
        self.site_id = site_id
        self.name = name
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        title = "DELEGATE_TITLE".localized()
        mainView.title.text = name
        mainView.getCode.addTarget(self, action: #selector(getcode), for: .touchUpInside)
        mainView.copy.addTarget(self, action: #selector(copycode), for: .touchUpInside)
        mainView.share.addTarget(self, action: #selector(share), for: .touchUpInside)
    }
    
    @objc func getcode(){
        showLoader()
        _ = DataManager.shared.delegate_new(site_id: site_id).subscribe(onNext: {result in
            self.hideLoader()
            if result.result > 0 {
                self.mainView.getCode.isHidden = true
                self.mainView.code.isHidden = false
                self.mainView.code.text = result.user_code
            }else{
                AlertUtil.errorAlert(HubResponseCode.getError(error: result.result))
            }
        })
    }
    
    @objc func copycode(){
        if mainView.code.isHidden == false {
            UIPasteboard.general.string = mainView.code.text!
            AlertUtil.successAlert("COPIED_IN_BUFFER".localized())
        }else{
            AlertUtil.warAlert("DELEGATE_GET_CODE_AT_FIRST".localized())
        }
    }
    
    @objc func share(){
        if mainView.code.isHidden == false {
            let textToShare = [ mainView.code.text! ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            AlertUtil.warAlert("DELEGATE_GET_CODE_AT_FIRST".localized())
        }
    }
}
