//
//  DelegationView.swift
//  SecurityHub test
//
//  Created by Timerlan on 08.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class DelegationView: BaseView {

    private lazy var text: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.text = "DELEG_MESS".localized()
        view.numberOfLines = 0
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 30.0)
        view.textAlignment = .center
        view.textColor = UIColor.hubMainColor
        return view
    }()
    
    private lazy var controller: UIImageView = {
        var view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.image = #imageLiteral(resourceName: "cont")
        return view
    }()
    
    lazy var copy: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_copy"), for: UIControl.State.normal)
        return view
    }()
    
    lazy var share: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_share"), for: UIControl.State.normal)
        return view
    }()
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.text = "DELEG_CODE".localized()
        view.numberOfLines = 1
        return view
    }()
    
    lazy var code: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.numberOfLines = 0
        return view
    }()
    
    lazy var getCode : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubDarkMainColor
        view.setTitleColor(UIColor.white, for: UIControl.State.normal)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setTitle("DELEG_GET_CODE".localized(), for: .normal)
        return view
    }()
    
    override func setContent() {
        super.setContent()
        contentView.addSubview(controller)
        contentView.addSubview(text)
        contentView.addSubview(title)
        contentView.addSubview(copy)
        contentView.addSubview(share)
        contentView.addSubview(label)
        contentView.addSubview(code)
        contentView.addSubview(getCode)
        code.isHidden = true
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        controller.snp.updateConstraints{ make in make.centerX.top.width.height.equalToSuperview() }
        text.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.width.equalToSuperview().offset(-32)
        }
        title.snp.remakeConstraints{ make in
            make.width.equalToSuperview().offset(-32)
            make.centerX.centerY.equalToSuperview()
        }
        copy.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview().offset(-40)
            make.bottom.equalTo(controller.snp.bottom).offset(-40)
            make.height.width.equalTo(30)
        }
        share.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview().offset(40)
            make.bottom.equalTo(controller.snp.bottom).offset(-40)
            make.height.width.equalTo(30)
        }
        getCode.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(share.snp.top).offset(-16)
            make.width.equalTo(200)
        }
        code.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(getCode.snp.centerY)
        }
        label.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(getCode.snp.top).offset(-16)
        }
    }
}
