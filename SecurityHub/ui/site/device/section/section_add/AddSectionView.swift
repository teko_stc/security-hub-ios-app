//
//  AddSectionView.swift
//  SecurityHub
//
//  Created by Timerlan on 18.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class AddSectionView: BaseView  {
    
    private lazy var imageView: UIImageView = {
        var view = UIImageView();
        view.backgroundColor = UIColor.hubMainColor
        return view
    }()
    
    lazy var form: UIView = {
        var view = UIView();
        view.backgroundColor = UIColor.white;
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        return view;
    }()
    
    private lazy var lineC: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    
    private lazy var nameLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "section_add_name".localized()
        return view
    }()
    
    lazy var nameField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "section_add_input_name".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
        view.returnKeyType = UIReturnKeyType.next
        return view
    }()
    
    lazy var typeField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.titleLabel?.numberOfLines = 0
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var typeLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "section_add_type".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var typeIc: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        return view
    }()
    
    lazy var nextBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubRipleBackgroundColor
        view.ripplePercent = 1
        view.setTitle("button_add_section".localized(), for: .normal)
        return view
    }()
    
    override func setContent() {
        setBottomBackgroundColor(UIColor.hubBackgroundColor)
        contentView.addSubview(imageView)
        contentView.addSubview(form)
        form.addSubview(nameField)
        form.addSubview(nameLable)
        form.addSubview(lineC)
        form.addSubview(typeLable)
        form.addSubview(typeField)
        form.addSubview(typeIc)
        contentView.addSubview(nextBtn)
        nameField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        typeIc.addTarget(self, action: #selector(selectT(_:)), for: .touchUpInside)
        typeField.addTarget(self, action: #selector(selectT(_:)), for: .touchUpInside)
        let type = HubConst.getSectionsHUBTypes().first!;
        self.typeField.setTitle(type.name, for: .normal)
        self.typeField.tag = Int(type.id)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        if nameField.text?.count ?? 0 > 0 {
            nextBtn.isEnabled = true
            nextBtn.setTitleColor(UIColor.white, for: .normal)
        }else{
            nextBtn.isEnabled = false
            nextBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    @objc func selectT(_ sender: Any?) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for type in HubConst.getSectionsHUBTypes(){
            alert.addAction(UIAlertAction(title: type.name, style: .default, handler: { a in
                self.typeField.setTitle(type.name, for: .normal)
                self.typeField.tag = Int(type.id)
            }))
        }
        alert.addAction(UIAlertAction(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        nV?.present(alert, animated: false)
    }
    
    func moveViewToUp() {
        imageView.snp.updateConstraints{ make in make.height.equalTo(0) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
    
    func moveViewToBottom() {
        imageView.snp.updateConstraints{ make in make.height.equalTo(200) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        imageView.snp.remakeConstraints{make in
            make.top.leading.trailing.width.equalToSuperview()
            make.height.equalTo(200)
        }
        form.snp.remakeConstraints{make in
            make.top.equalTo(imageView.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        nameLable.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
        }
        nameField.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.leading.equalTo(nameLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalTo(lineC.snp.top)
        }
        lineC.snp.remakeConstraints{ make in
            make.top.equalTo(nameLable.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        typeLable.snp.remakeConstraints{ make in
            make.top.equalTo(lineC.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.bottom.equalToSuperview().offset(-15)
        }
        typeField.snp.remakeConstraints{ make in
            make.top.equalTo(lineC.snp.bottom)
            make.height.equalTo(45)
            make.leading.equalTo(typeLable.snp.trailing)
        }
        typeIc.snp.remakeConstraints{ make in
            make.leading.equalTo(typeField.snp.trailing)
            make.centerY.equalTo(typeField.snp.centerY)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
        }
        nextBtn.snp.remakeConstraints{ make in
            make.top.equalTo(form.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
    }
}
