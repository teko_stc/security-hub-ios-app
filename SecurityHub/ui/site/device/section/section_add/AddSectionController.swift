//
//  AddSectionController.swift
//  SecurityHub
//
//  Created by Timerlan on 19.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class AddSectionController: BaseController<AddSectionView> {
    private let device: Int64
    private let backViewController: UIViewController?
    
    init(device: Int64, backViewController: UIViewController? = nil) {
        self.device = device
        self.backViewController = backViewController
        super.init(nibName: nil, bundle: nil)
        title = "SETA_ADD_SECTION_TITLE".localized()
    }
    
    override func loadView() {
        super.loadView()
        mainView.nV = navigationController
        mainView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
        mainView.nextBtn.addTarget(self, action: #selector(create), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func create(){
        if mainView.nameField.text?.count ?? 0 == 0 {
            AlertUtil.errorAlert("error_input_name".localized())
        }else{
            _ = DataManager.shared.addSection(device: device, name: mainView.nameField.text!, type: Int64(mainView.typeField.tag) )
                .subscribe(onNext: { result in
                    if let nV = self.navigationController, let backViewController = self.backViewController {
                        nV.popToViewController(backViewController, animated: true)
                        return
                    }
                    guard let nV = self.navigationController, let last = nV.viewControllers.filter({ (vc) -> Bool in return vc is XMainController }).last else { return }
                    nV.popToViewController(last, animated: true)
                })
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) { DispatchQueue.main.async { self.mainView.moveViewToUp()} }
    
    @objc func keyboardWillDissmiss(notification: Notification) { DispatchQueue.main.async { self.mainView.moveViewToBottom() } }
    
    @objc func viewTapped() {
        if mainView.nameField.isFirstResponder == true { mainView.nameField.resignFirstResponder()}
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
