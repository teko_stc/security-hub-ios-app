//
//  SectionSelectController.swift
//  SecurityHub
//
//  Created by Timerlan on 17.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SectionSelectController: BaseController<BaseListView<SectionSelectView,SectionSelectCell>>, UITableViewDelegate {
    private let device_ids: [Int64]
    private let site: Int64!
    private let isDevice: Bool
    private var select: ((_ section: Sections) -> Void)
    
    init(site: Int64! = nil, device_ids : [Int64], title: String = "WIZ_CHOOSE_SECTION".localized(), isDevice: Bool = false, select: @escaping ((_ section: Sections) -> Void)) {
        self.device_ids = device_ids
        self.site = site
        self.isDevice = isDevice
        self.select = select
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        if (Roles.sendCommands && Roles.manageSectionZones && !isDevice) { rightButton() }
        mainView.initTable(cellId: SectionSelectCell.cellId, cellHeight: SectionSelectView.cellHeight, select: select, refresh: refresh)
        _ = DataManager.shared.getSectionsObserver()
            .filter({ (ds) -> Bool in
                if let site = self.site { return ds.siteIds.contains(site) }
                return true
            })
            .filter({ (ds) -> Bool in return self.device_ids.contains(ds.deviceId) })
            .filter({ (ds) -> Bool in return !self.isDevice || ds.section.section == 0 })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ds in self.mainView.updateList( [ds.section], type: ds.type) })
        //TODO Надо
    }
    
    private func refresh(){ DataManager.shared.updateSites()  }
    
    private func select(_ obj: SQLCommand) { self.select(obj as! Sections) }
    
    override func rightClick() {
        if (device_ids.count > 1) {
            let vc = SectionSelectController(site: site, device_ids: device_ids, title: "DTA_CHOOSE_DEVICE".localized(), isDevice: true) { (sec) in
                let vc = AddSectionController(device: sec.device, backViewController: self)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            navigationController?.pushViewController(vc, animated: false)
            return
        }
        let vc = AddSectionController(device: device_ids[0], backViewController: self)
        navigationController?.pushViewController(vc, animated: false)
    }
}
