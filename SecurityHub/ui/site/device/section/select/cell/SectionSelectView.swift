//
//  SectionSelectView.swift
//  SecurityHub
//
//  Created by Timerlan on 17.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SectionSelectView: UIView {
    static let cellHeight : CGFloat = 60 + CGFloat(Constants.margin) * 2;
    
    private lazy var iconCont: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var nameView: UILabel = {
        let view = UILabel()
        view.font = Constants.bigFont
        view.numberOfLines = 0
        view.textColor = Constants.bigColor
        return view
    }()
    
    private lazy var deviceView: UILabel = {
        let view = UILabel()
        view.font = Constants.middleFont
        view.textColor = Constants.middleColor
        return view
    }()
    
    private lazy var numCont: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.cornerRadius = 10
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 10 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor //
        return view
    }()
    
    private lazy var numView: UILabel = {
        let view = UILabel()
        view.font = Constants.smallFont
        view.textColor = Constants.bigColor
        return view
    }()
    
    private lazy var nextIconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = #imageLiteral(resourceName: "arrow_right")
        return view
    }()
    
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconCont)
        iconCont.addSubview(iconView)
        addSubview(nameView)
        addSubview(deviceView)
        addSubview(numCont)
        addSubview(nextIconView)
        numCont.addSubview(numView)
        addSubview(line)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        iconCont.snp.remakeConstraints{ make in
            make.top.left.equalToSuperview().offset(Constants.margin)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(60)
            make.bottom.equalToSuperview().offset(-Constants.margin)
        }
        iconView.snp.remakeConstraints{ make in
            make.center.equalToSuperview()
            make.width.height.equalTo(40)
        }
        nameView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(Constants.margin)
            make.left.equalTo(iconCont.snp.right).offset(Constants.margin)
            make.right.equalTo(nextIconView.snp.left).offset(-Constants.margin)
        }
        deviceView.snp.remakeConstraints{ make in
            make.left.equalTo(iconCont.snp.right).offset(Constants.margin)
            make.right.equalTo(nextIconView.snp.left).offset(-Constants.margin)
            make.bottom.equalTo(line.snp.top).offset(-Constants.margin)
        }
        numCont.snp.remakeConstraints{ make in
            make.bottom.equalTo(iconCont.snp.bottom).offset(Constants.margin/2)
            make.right.equalTo(iconCont.snp.right).offset(Constants.margin/2)
            make.width.height.equalTo(30)
        }
        numView.snp.remakeConstraints{ make in make.center.equalToSuperview() }
        nextIconView.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-Constants.margin/2)
            make.height.width.equalTo(30)
        }
        line.snp.remakeConstraints{ make in
            make.height.equalTo(0.5)
            make.left.equalTo(nameView.snp.left)
            make.right.bottom.equalToSuperview()
        }
    }
    
//    func setSection(_ name: String, section: Int64, type: Int64, hideLine: Bool) {
//        nameView.text = name
//        numView.text = "№\(section)"
//        if let sType = HubConst.getSectionsTypes().first(where: { $0.key == type }) {
//            iconView.image = UIImage(named: sType.value.icon)
//            iconCont.backgroundColor = sType.value.color
//        }
//        line.isHidden = hideLine
//    }
    
    func setDevice(_ device: Devices, section: Int64) {
        nameView.text = device.name
        if section == 0 {
            iconView.image = UIImage(named: HubConst.getDeviceTypeInfo(device.configVersion, cluster: device.cluster_id).icon)
            iconCont.backgroundColor = UIColor.lightGray
            numCont.isHidden = true
        } else{ numCont.isHidden = false }
    }
    
    func setSite(siteNames: String) {
        deviceView.text = siteNames
    }
}
