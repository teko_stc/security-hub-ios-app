//
//  SectionSelectCell.swift
//  SecurityHub
//
//  Created by Timerlan on 17.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SectionSelectCell: BaseCell<SectionSelectView> {
    static let cellId = "SectionSelectCell"
    private var section: Sections!
    
    override func setContent(_ obj: SQLCommand, isEnd: Bool) {
        guard let s = obj as? Sections else { return }
        section = s
        let names = DataManager.shared.getNameSites(section.device)
        mainView.setSite(siteNames: names)
        _ = DataManager.shared.getDevice(device: section.device)
        .filter({ (device) -> Bool in return device != nil })
        .subscribe(onNext: { device in self.mainView.setDevice(device!, section: self.section.section) })
    }
}
