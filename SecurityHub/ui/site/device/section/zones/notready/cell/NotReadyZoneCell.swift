//
//  NotReadyZoneCell.swift
//  SecurityHub
//
//  Created by Timerlan on 08.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class NotReadyZoneCell: BaseCell<NotReadyZoneView>{
    static let cellId = "NotReadyZoneCell"
    
    func setContent(_ nNotReadyZone: NNotReadyZone) {
        selectionStyle = .none
        let zi = nNotReadyZone.zi
        mainView.sectionView.text = "\(zi.siteNames) → \(zi.deviceName) → \(zi.sectionName) → \(zi.zone.name)"
        mainView.zoneView.text = nNotReadyZone.affect_decs
    }
}
