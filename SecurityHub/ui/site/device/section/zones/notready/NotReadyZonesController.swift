//
//  NotReadyZonesController.swift
//  SecurityHub
//
//  Created by Timerlan on 08.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import UIKit

class NotReadyZonesController: BaseController<NotReadyZonesView>, UITableViewDataSource {
    private var zones: [NNotReadyZone]
		
    init(_ nNotReadyZone: NNotReadyZone) {
        self.zones = [nNotReadyZone]
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "INFORMATION".localized()
        mainView.tableView.register(NotReadyZoneCell.self, forCellReuseIdentifier: NotReadyZoneCell.cellId)
        mainView.tableView.dataSource = self
        mainView.tableView.reloadData()
    }
    
    private var o: Any?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         o = NotificationCenter.default.addObserver(forName: HubNotification.notReadyZone, object: nil, queue: OperationQueue.main){ notification in
            guard let nNotReadyZone = notification.object as? NNotReadyZone else { return }
            if let pos = self.zones.firstIndex(where: { (o) -> Bool in return o.zi.zone.id == nNotReadyZone.zi.zone.id }) {
                self.zones[pos] = nNotReadyZone
                self.mainView.tableView.reloadRows(at: [IndexPath(row: pos, section: 0)], with: .fade)
            } else {
                self.zones.append(nNotReadyZone)
                self.mainView.tableView.insertRows(at: [IndexPath(row: self.zones.count-1, section: 0)], with: .right)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if o != nil {
            NotificationCenter.default.removeObserver(o!)
            o = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return zones.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotReadyZoneCell = (tableView.dequeueReusableCell(withIdentifier: NotReadyZoneCell.cellId, for: indexPath) as? NotReadyZoneCell)!
        cell.setContent(zones[indexPath.row])
        return cell
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
