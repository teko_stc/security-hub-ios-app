//
//  NotReadyZonesView.swift
//  SecurityHub
//
//  Created by Timerlan on 08.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import UIKit

class NotReadyZonesView: BaseView {
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.backgroundColor = UIColor.hubBackgroundColor
        view.estimatedRowHeight = 50
        return view
    }()
    
    lazy var titleView: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.text = "MESS_NOT_READY".localized()
        view.numberOfLines = 0
        return view
    }()
    
    override func setContent() {
        super.setContent()
        contentView.alwaysBounceVertical = false
        contentView.addSubview(titleView)
        contentView.addSubview(tableView)
    }
    
    override func updateConstraints() {
        titleView.snp.remakeConstraints { make in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().offset(-30)
            make.height.equalTo(70)
        }
        tableView.snp.remakeConstraints{ make in
            make.top.equalTo(titleView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().offset(-70)
        }
        super.updateConstraints()
    }
}

