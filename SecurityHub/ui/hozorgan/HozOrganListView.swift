//
//  HozOrganListView.swift
//  SecurityHub
//
//  Created by Timerlan on 25.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class HozOrganListView: BaseView {
    lazy var table : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        view.backgroundColor = UIColor.hubBackgroundColor
        return view
    }()
    
    override func setContent() {
        contentView.alwaysBounceVertical = false
        contentView.addSubview(table)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        table.snp.remakeConstraints{make in
            make.top.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
}
