//
//  HozOrganListController.swift
//  SecurityHub
//
//  Created by Timerlan on 25.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class HozOrganListController:  BaseController<BaseListView<HozOrganView, HozOrganCell>> {
    private var dm = DataManager.shared!
    private var users: [DeviceUsers] = []
    private var device_ids: [Int64]
    
    init(device_ids: [Int64]) {
        self.device_ids = device_ids
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        title = "BUTTON_USERS".localized()
        mainView.initTable(cellId: HozOrganCell.cellId, cellHeight: HozOrganView.cellHeight, refresh: update)
        _ = dm.getDeviceUsers(device_ids: device_ids)
            .subscribe(onNext: { (deviceUserd, type) in self.mainView.updateList(deviceUserd, type: type) })
        //TODO Shkrv
//        if (Roles.sendCommands && Roles.manageSectionZones) { rightButton() }
        if (XTargetUtils.target == .myuvo || XTargetUtils.target == .dev)
        {
            if (Roles.sendCommands ) { rightButton() }
        }
        else  if (Roles.sendCommands && Roles.manageSectionZones) { rightButton() }

    }
    
    private func update(){ dm.updateSites() }
    
    override func rightClick() {
        switch device_ids.count {
        case 0:
            AlertUtil.warAlert("add_zone_no_device_error".localized())
        case 1:
            guard dm.isHub(device_id: device_ids[0]) else { return AlertUtil.warAlert("add_section_not_hub_error".localized()) }
            showOnAvailable(device_id: device_ids[0]) { self.addUserAlert(device_ids: self.device_ids) }
        default:
            addUserAlert(device_ids: device_ids)
        }
    }
    private func addUserAlert(device_ids: [Int64]) {
        guard let nV = navigationController else { return }
        let wireless_void = { nV.pushViewController(XFindWirelessController(device_ids: device_ids, section_id: nil, type: .user), animated: true) }
        let wireline_void = { nV.pushViewController(XAddWirelineUserController(device_ids: device_ids), animated: true) }
        var actions = [AlertUtil.XAlertAction(message: "WIZ_USER_SET_HW".localized(), void: wireless_void),
                       AlertUtil.XAlertAction(message: "WIZ_USER_SET_CODE".localized(), void: wireline_void),
        ]
        if (Roles.sendCommands && XTargetUtils.target == .myuvo || XTargetUtils.target == .dev)
        {
            actions = [
                AlertUtil.XAlertAction(message: "WIZ_USER_SET_CODE".localized(), void: wireline_void),
            ]
        }
        else
        {
            actions = [
                AlertUtil.XAlertAction(message: "WIZ_USER_SET_HW".localized(), void: wireless_void),
                AlertUtil.XAlertAction(message: "WIZ_USER_SET_CODE".localized(), void: wireline_void),
            ]
        }
        
        
        nV.present(AlertUtil.myXAlert(actions: actions), animated: false)
    }
    private func showOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in if !result.success { AlertUtil.errorAlert(result.message) } })
            .subscribe()
    }
}
