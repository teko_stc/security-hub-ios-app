//
//  HozOrganCell.swift
//  SecurityHub
//
//  Created by Timerlan on 25.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class HozOrganCell: BaseCell<HozOrganView> {
    static let cellId = "HozOrganCellHozOrganCell"
    private var user: DeviceUsers!
    
    override func setContent(_ obj: SQLCommand, isEnd: Bool = false) {
        guard let u = obj as? DeviceUsers else { return }
        user = u
        mainView.nameView.text = user.name
        mainView.indexView.text = "\("hozorgan_cell_index".localized()): \(user.userIndex)"
        _ = DataManager.shared.getUserSection(user: user)
            .observeOn(MainScheduler())
            .subscribe(onNext: { sections in
                var sec = "\(sections)"
                sec.removeFirst()
                sec.removeLast()
                self.mainView.sectionView.text = "\("SECTIONS".localized()): \(sec)"
            })
        _ = DataManager.shared.getDevice(device: user.device)
        .subscribe(onNext: { device in self.mainView.deviceView.text = device!.name })
        mainView.menuBtn.addTarget(self, action: #selector(menu_click), for: .touchUpInside)
    }
    
    @objc private func menu_click(){
        doOnAvailable(device_id: user.device) {
            let al = AlertUtil.myXAlert(messages: ["N_LOCAL_USER_NAME".localized(), "N_LOCAL_USER_SETTINGS_PARTITIONS".localized(), "N_LOCAL_USER_SETTINGS_DELETE"], voids: [self.rename,self.permission,self.delete])
            self.nV?.present(al, animated: false)
        }
    }
    
    private var disp: Disposable?
    private func permission(){
        disp = DataManager.shared.getSections(device: user.device)
        .concatMap({ (res) -> Observable<Sections> in return Observable.from(res) })
        .filter({ (sec) -> Bool in return sec.detector == 1 })
        .concatMap({ (sec) -> Observable<(Sections,[Int64])> in return Observable.zip(Observable.just(sec),DataManager.shared.getUserSection(user: self.user)) })
        .map({ (sec, sections) -> SelectElement in return SelectElement(name: sec.name, selected: sections.contains(sec.section), obj: (sec, sections)) })
        .toArray()
        .asObservable()
        .do(onNext: { list in
            self.nV?.pushViewController(SelectZoneController(list: list, title: "WIZ_USER_CHOOSE_SECTIONS".localized(), obj: self.user), animated: false)
        })
        .subscribe()
    }
    
    private func rename(){
        AlertUtil.renameXAlert(text: user.name) { (text) in
            _ = DataManager.shared.setHozOrgan(device: self.user.device, name: text, index: self.user.userIndex, sections: nil).subscribe()
        }
    }
    
    private func delete(){
        AlertUtil.deleteAlert({
            _ = DataManager.shared.delHozOrgan(device: self.user.device, index: self.user.userIndex).subscribe()
        }, text: "\("N_DELETE_USER_TITLE".localized()) \(self.user.name)?")
    }
    private func doOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        DataManager.shared.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in if !result.success { AlertUtil.errorAlert(result.message) } })
            .subscribe()
    }
}
