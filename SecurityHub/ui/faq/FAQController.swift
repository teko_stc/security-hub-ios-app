//
//  FAQController.swift
//  SecurityHub
//
//  Created by Timerlan on 02/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation
import WebKit
import Localize_Swift

class WikiController: BaseController<IvideonAuthView>, WKNavigationDelegate {
    private var url: URL?
    
    init(url: URL?) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "drawer_item_help".localized()
        mainView.webView.isOpaque = true
        mainView.webView.backgroundColor = DEFAULT_COLOR_MAIN
        mainView.webView.contentScaleFactor = 3
        mainView.webView.navigationDelegate = self
        if let url = url {
            mainView.webView.load(URLRequest(url: url))
            return
        }
        var bundle = Bundle.main;
        if let path = Bundle.main.path(forResource: Localize.currentLanguage(), ofType: "lproj"), let bun = Bundle(path: path) { bundle = bun }
        guard  let path = bundle.path(forResource: "faq", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()),
            let json = jsonObject as? [String : Any] else { return }
        
        var html = "<html><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <style>* { font-family: Roboto, Arial;} html, body { width: 98%; max-width: 98%; } </style><body>"
        guard let chapters = json["chapters"] as? [[String : Any]] else { return }
        for chapter in chapters {
            guard let chapter_caption = chapter["_caption"] as? String else { break; }
            html += "<h2 style=\"color: #5e9ca0;\">\(chapter_caption)</h2>"
            
            var questions: [[String : Any]] = []
            if let _questions = (chapter["questions"] as? [String : Any])?["question"] as? [[String : Any]] { questions = _questions }
            if let _questions = (chapter["questions"] as? [String : Any])?["question"] as? [String : Any] { questions = [_questions] }
            for question in questions {
                guard let question_caption = question["_caption"] as? String else { break; }
                html += "<h3 style=\"color: #2e6c80;\">\(question_caption)</h3>"
                var answers: [[String : Any]] = []
                if let _answers = (question["answers"] as? [String : Any])?["answer"] as? [[String : Any]] { answers = _answers }
                if let _answers = (question["answers"] as? [String : Any])?["answer"] as? [String : Any] { answers = [_answers] }
                for answer in answers {
                    guard let answer_caption = answer["_caption"] as? String else { break; }
                    let _link = answer["_link"] as? String
                    if _link == nil { html += "<h4>\(answer_caption)</h4>" }
                    else {
                        html.removeLast(5)
                        html += "<a style=\"background-color: #0095B6; color: #fff; display: inline-block; padding: 5px 10px; font-weight: bold; border-radius: 5px;\" href=\"\(_link!)\">\(answer_caption)</a></h4>" }
                }
            }
        }
        html += "</body></html>"
        mainView.webView.loadHTMLString(html, baseURL: nil)
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let serverTrust = challenge.protectionSpace.serverTrust {
            let credential = URLCredential(trust: serverTrust)
            completionHandler(.useCredential, credential)
        }else{
            completionHandler(.useCredential, nil)
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void) {
        guard let _url = self.url else {
            guard let url = navigationAction.request.url, url.absoluteString != "about:blank" else { decisionHandler(.allow); return }
            decisionHandler(.cancel)
            navigationController?.pushViewController(WikiController(url: url), animated: true)
            return
        }
        guard let url = navigationAction.request.url, url.absoluteString != "about:blank" else { decisionHandler(.allow); return }
        if url.absoluteString.contains(_url.absoluteString)  {
            decisionHandler(.allow)
        } else {
            decisionHandler(.cancel)
            navigationController?.pushViewController(WikiController(url: url), animated: true)
        }
    }
}
