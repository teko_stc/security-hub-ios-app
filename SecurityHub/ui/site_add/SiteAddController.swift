//
//  SiteAddController.swift
//  SecurityHub test
//
//  Created by Timerlan on 07.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class SiteAddController: XBaseViewController<SiteAddView> {
    private var stage = 1;
    private var site_name: String?
    private var site_id: Int64?
    private var disp: Disposable?
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.hubBackgroundColor,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    init(_ stage: Int = 1, site_name: String? = nil, site_id: Int64? = nil) {
        self.stage = stage;
        self.site_name = site_name
        self.site_id = site_id
        super.init(nibName: nil, bundle: nil)
        setNavigationViewBuilder(navigationViewBuilder)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        mainView.nV = navigationController
//        AppUtility.lockOrientation(.portrait)
        title = "title_add_site".localized()
        switch stage {
        case 2:
            mainView.imgs = [#imageLiteral(resourceName: "contrl_2")]
            mainView.header.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 150)
            mainView.header.reloadData()
            mainView.textView.text = "WIZ_CONTR_ENTER_NAME_MESS".localized()
            if site_id != nil { mainView.needFormWhithout() } else { mainView.needForm() }
        case 3:
            mainView.imgs = [#imageLiteral(resourceName: "contrl_3")]
            mainView.header.reloadData()
            mainView.textView.text = "WIZ_CONTR_SUCCESS".localized()
        case 4:
            mainView.imgs = [#imageLiteral(resourceName: "contr_image_1"),#imageLiteral(resourceName: "contr_image_2"),#imageLiteral(resourceName: "contr_image_3"),#imageLiteral(resourceName: "contr_image_4")]
            mainView.header.isInfinite = false
            mainView.h = 270
            mainView.nextBtn.isHidden = true
            mainView.header.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 270)
            mainView.header.reloadData()
            if site_id != nil { mainView.nextBtn.isHidden = true }
            mainView.nextBtn.setTitle("WIZ_CONTR_ADD_DEV".localized(), for: .normal)
            mainView.endBtn.isHidden = false
            mainView.textView.text = "WIZ_CONTR_STEPS".localized()
        default: break;
        }
        mainView.nextBtn.addTarget(self, action: #selector(next(button:)), for: .touchUpInside)
        mainView.endBtn.addTarget(self, action: #selector(end(button:)), for: .touchUpInside)
        mainView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) { if stage == 2 { DispatchQueue.main.async { self.mainView.moveViewToUp()} } }
    
    @objc func keyboardWillDissmiss(notification: Notification) { if stage == 2 {  DispatchQueue.main.async { self.mainView.moveViewToBottom() } } }
    
    @objc func viewTapped() {
        if mainView.nameField.isFirstResponder == true { mainView.nameField.resignFirstResponder()}
        if mainView.numField.isFirstResponder == true { mainView.numField.resignFirstResponder()}
    }
    
    @objc func next(button: UIButton?){
        if stage == 2, let site = site_id {
            _ = DataManager.shared.addDevice(site: site, sn: mainView.numField.text!, cluster: mainView.typeField.tag)
            .subscribe(onNext:{ result in self.navigationController?.pushViewController(SiteAddController(self.stage + 1, site_name: nil, site_id: nil ), animated: true) })
        }else if stage == 2 {
            _ = DataManager.shared.addSite(name: mainView.nameField.text!, sn: mainView.numField.text!, cluster: mainView.typeField.tag).subscribe(onNext:{ result in
                DataManager.shared.updateSites()
                DataManager.shared.updateAffects()
                self.navigationController?.pushViewController(SiteAddController(self.stage + 1, site_name: self.mainView.nameField.text!, site_id: nil ), animated: true)
            })
        }else if stage != 4 {
            navigationController?.pushViewController(SiteAddController(stage + 1, site_name: site_name, site_id: site_id), animated: true)
        }
    }
    
    @objc func end(button: UIButton?){
        navigationController?.popToRootViewController(animated: true)
    }
}
