//
//  SiteAddView.swift
//  SecurityHub test
//
//  Created by Timerlan on 07.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BarcodeScanner
import FSPagerView
import Localize_Swift

class SiteAddView: BaseView, UITextFieldDelegate, BarcodeScannerCodeDelegate, FSPagerViewDataSource {
    var imgs: [UIImage] = [#imageLiteral(resourceName: "contrl_1")]
    
    lazy var header: FSPagerView = {
        var view = FSPagerView()
        view.contentMode = UIView.ContentMode.scaleToFill
        view.automaticSlidingInterval = 3.0
        view.isInfinite = false
        view.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 270)
        view.interitemSpacing = 10
        view.dataSource = self
        view.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        return view
    }()
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imgs.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        cell.imageView?.image = imgs[index]
        return cell
    }
    
    lazy var form: UIView = {
        var view = UIView();
        view.backgroundColor = UIColor.white;
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        return view;
    }()
    
    private lazy var lineH: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineC: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineF: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    
    lazy var textView: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.gray
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 5
        view.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.regular)
        view.text = "WIZ_CONTR_OPEN_MESS".localized()
        return view
    }()
    
    private lazy var nameLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "WIZ_CONTR_ENTER_NAME".localized()
        return view
    }()
    
    lazy var nameField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
//        view.keyboardType = .asciiCapable
        view.returnKeyType = UIReturnKeyType.next
        return view
    }()
    
    lazy var typeField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var typeLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "WIZ_CONTR_ENTER_TYPE".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var typeIc: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        return view
    }()
    
    private lazy var numLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "WIZ_CONTR_ENTER_SERIAL".localized()
        return view
    }()
    
    lazy var numField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        view.autocapitalizationType = .none
        view.keyboardType = .numberPad
        view.returnKeyType = UIReturnKeyType.go
        return view
    }()
    
    lazy var numIc: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.gray
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_qr"), for: UIControl.State.normal)
        return view
    }()
    
    lazy var nextBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubRipleBackgroundColor
        view.ripplePercent = 1
        view.setTitle("WIZ_NEXT".localized(), for: .normal)
        return view
    }()
    
    lazy var endBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubBackgroundColor
        view.rippleBackgroundColor = UIColor.hubBackgroundColor
        view.rippleColor = UIColor.hubRipleColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setTitle("WIZ_CONTR_END_SETUP".localized(), for: .normal)
        return view
    }()
    
    override func setContent() {
        setBottomBackgroundColor(UIColor.hubBackgroundColor)
        contentView.addSubview(header)
        contentView.addSubview(lineH)
        contentView.addSubview(textView)
        contentView.addSubview(form)
        
        form.addSubview(nameField)
        form.addSubview(nameLable)
        form.addSubview(lineC)
        form.addSubview(typeLable)
        form.addSubview(typeField)
        form.addSubview(typeIc)
        form.addSubview(lineF)
        form.addSubview(numLable)
        form.addSubview(numField)
        form.addSubview(numIc)
        
        contentView.addSubview(nextBtn)
        contentView.addSubview(endBtn)
        
        form.isHidden = true
        endBtn.isHidden = true
        lineH.isHidden = true
        
        nameField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        nameField.delegate = self
        numField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        typeIc.addTarget(self, action: #selector(selectT(_:)), for: .touchUpInside)
        typeField.addTarget(self, action: #selector(selectT(_:)), for: .touchUpInside)
        numIc.addTarget(self, action: #selector(code(_:)), for: .touchUpInside)
        if let clusters = DataManager.shared.clusters {
            typeField.setTitle(clusters.items.first!.name, for: .normal)
            typeField.tag = Int(clusters.items.first!.id)
        }else{ DataManager.shared.updateClusters() }
    }

   @objc func code(_ sender: Any?) {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        nV?.pushViewController(viewController, animated: true)
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        numField.text = String(code[code.index(after: code.startIndex)..<code.index(before: code.endIndex)])
        textFieldDidChange(textField: nameField)
        _ = Observable<Int>.timer(.milliseconds(1500), scheduler: MainScheduler.init()).subscribe(onNext:{t in
            self.nV?.popViewController(animated: true)
        })
    }
    
    @objc func selectT(_ sender: Any?) {
        let alert = UIAlertController(title: "WIZ_CONTR_ENTER_TYPE".localized(), message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        if let clusters = DataManager.shared.clusters {
            for cluster in clusters.items{
                alert.addAction(UIAlertAction(title: cluster.name, style: .default, handler: { a in
                    self.typeField.setTitle(cluster.name, for: .normal)
                    self.typeField.tag = Int(cluster.id)
                }))
            }
        }else{ DataManager.shared.updateClusters() }
        alert.addAction(UIAlertAction(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
        nV?.present(alert, animated: false)
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        numField.becomeFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        if ( nameField.text?.count ?? 0 > 0 || nameLable.isHidden ) && numField.text?.count ?? 0 > 4 {
            nextBtn.isEnabled = true
            nextBtn.setTitleColor(UIColor.white, for: .normal)
        }else{
            nextBtn.isEnabled = false
            nextBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    func moveViewToUp() {
        header.snp.updateConstraints{ make in make.height.equalTo(0) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
    
    func moveViewToBottom() {
        header.snp.updateConstraints{ make in make.height.equalTo(150) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
    
    var h = 270
    private var h2 = 0;
    func needForm() {
        form.isHidden = false
        textFieldDidChange(textField: nameField)
        h = 140
        if Localize.currentLanguage() == "ru" { h2 = 150 }
        else { h2 = 100 }
    }
    
    func needFormWhithout() {
        form.isHidden = false
        textFieldDidChange(textField: nameField)
        nameLable.isHidden = true
        nameField.isHidden = true
        lineC.isHidden = true
        h = 140
        if Localize.currentLanguage() == "ru" { h2 = 100 }
        else { h2 = 50 }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        header.snp.remakeConstraints{make in
            make.top.equalToSuperview()
            make.centerX.width.equalToSuperview()
            make.height.equalTo(h)
        }
        lineH.snp.remakeConstraints{make in
            make.top.equalTo(header.snp.bottom)
            make.leading.trailing.width.equalToSuperview()
            make.height.equalTo(1)
        }
        textView.snp.remakeConstraints{ make in
            make.top.equalTo(lineH.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        form.snp.remakeConstraints{make in
            make.top.equalTo(textView.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(h2)
        }
        if h2 > 50 {
            nameLable.snp.remakeConstraints{ make in
                make.top.leading.equalToSuperview().offset(15)
                make.width.equalTo(80)
            }
            nameField.snp.remakeConstraints{ make in
                make.top.equalToSuperview()
                make.leading.equalTo(nameLable.snp.trailing)
                make.trailing.equalToSuperview().offset(-15)
                make.bottom.equalTo(lineC.snp.top)
            }
        }
        lineC.snp.remakeConstraints{ make in
            if h2 > 50 {  make.top.equalTo(nameLable.snp.bottom).offset(15) }
            else { make.top.equalToSuperview() }
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        if Localize.currentLanguage() == "ru" {
            typeLable.snp.remakeConstraints{ make in
                if nameField.isHidden { make.top.equalToSuperview().offset(20) }
                else { make.top.equalTo(lineC.snp.bottom).offset(15) }
                make.leading.equalToSuperview().offset(15)
                make.width.equalTo(80)
            }
            typeField.snp.remakeConstraints{ make in
                if nameField.isHidden { make.top.equalToSuperview().offset(5) }
                else { make.top.equalTo(lineC.snp.bottom) }
                make.height.equalTo(45)
                make.leading.equalTo(numLable.snp.trailing)
                make.bottom.equalTo(lineF.snp.top)
            }
            typeIc.snp.remakeConstraints{ make in
                if nameField.isHidden { make.top.equalToSuperview().offset(10) }
                else { make.top.equalTo(lineC.snp.bottom).offset(5) }
                make.leading.equalTo(typeField.snp.trailing)
                make.trailing.equalToSuperview().offset(-5)
                make.width.height.equalTo(35)
            }
            lineF.snp.remakeConstraints{ make in
                make.top.equalTo(typeLable.snp.bottom).offset(15)
                make.leading.equalToSuperview().offset(15)
                make.trailing.equalToSuperview()
                make.height.equalTo(1)
            }
        } else {
            typeLable.isHidden = true
            typeField.isHidden = true
            typeIc.isHidden = true
            lineF.isHidden = true
        }
        numLable.snp.remakeConstraints{ make in
            if Localize.currentLanguage() == "ru" { make.top.equalTo(lineF.snp.bottom).offset(15) }
            else {  make.top.equalTo(lineC.snp.bottom).offset(15) }
            make.leading.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(-15)
            make.width.equalTo(80)
        }
        numField.snp.remakeConstraints{ make in
            if Localize.currentLanguage() == "ru" { make.top.equalTo(lineF.snp.bottom) }
            else { make.top.equalTo(lineC.snp.bottom) }
            make.leading.equalTo(numLable.snp.trailing)
            make.bottom.equalToSuperview()
        }
        numIc.snp.remakeConstraints{ make in
            if Localize.currentLanguage() == "ru" { make.top.equalTo(lineF.snp.bottom).offset(10) }
            else { make.top.equalTo(lineC.snp.bottom).offset(10) }
            make.leading.equalTo(numField.snp.trailing)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
        }
        nextBtn.snp.remakeConstraints{ make in
            make.top.equalTo(form.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        endBtn.snp.remakeConstraints{ make in
            make.top.equalTo(nextBtn.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.lessThanOrEqualToSuperview().offset(-20)
        }
    }
}
