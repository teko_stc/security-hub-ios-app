//
//  EventBigCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 03.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
//import RxSwift
import AVKit
import MapKit

class EventBigCell: BaseCell<EventBigView>{
    static let cellId = "EventBigCell"
    private var event: Events!
    private var url = "";
    private var isError = false
    private var obs: Any?
    
    func setContent(_ obj: Events) {
        self.event = obj
        mainView.camBtn.isHidden = true
        mainView.camBtn.addTarget(self, action: #selector(camClick), for: .touchUpInside)
        mainView.iconView.image = UIImage(named: event.iconBig)
        mainView.descView.text = event.affect_desc
        mainView.timeView.text = event.time == 0 ? "--:--:--" : event.time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)
        mainView.siteView.text = DataManager.shared.getNameSites(event.device)
        mainView.deviceView.text = DataManager.shared.getDeviceName(device: event.device)
        if let ids = obj.jdata.toJson()["sections"] as? [Int64], ids.count > 0 {
            if ids.count == 1, let name = DataManager.shared.getSection(device: obj.device, section: ids.first!)?.name {
                mainView.sectionZoneView.text = "\(name)(\(ids.first!))"
            } else {
                var new_value = "\("SECTIONS".localized()): "
                ids.forEach { (id) in
                    if let name = DataManager.shared.getSection(device: obj.device, section: id)?.name { new_value += "\(name)(\(id)), " }
                }
                new_value.removeLast(2)
                mainView.sectionZoneView.text = new_value
            }
        } else {
            if let section = DataManager.shared.getSection(device: obj.device, section: obj.section) {
                if let zone = DataManager.shared.getZone(device: obj.device, section: obj.section, zone: obj.zone) {
                    mainView.sectionZoneView.text = "\(section.name)(\(section.section)), \(zone.name)(\(zone.zone))"
                } else {
                    mainView.sectionZoneView.text = "\(section.name)(\(section.section))"
                }
            }
        }
        if let _ = obj.jdata.toJson()["lat"] as? Double, let _ = obj.jdata.toJson()["lon"] as? Double{
            self.mainView.camBtn.setImage( #imageLiteral(resourceName: "map") , for: .normal)
            self.mainView.camBtn.isHidden = false
        }
        let _ = DataManager.shared.findVideo(self.event.id)
        .subscribe(onNext: { video in
            if video != nil {
                self.mainView.camBtn.isHidden = false
                if video!.isError { self.mainView.camBtn.setImage( #imageLiteral(resourceName: "no_camera") , for: .normal)}
                self.isError = video!.isError
                self.url = video!.result
            }
        })
        if obs != nil { NotificationCenter.default.removeObserver(obs!) }
        obs = NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(event.id), object: nil, queue: OperationQueue.main){ notification in
            let video = notification.object as! IvVideo
            self.mainView.camBtn.isHidden = false
            if video.isError { self.mainView.camBtn.setImage( #imageLiteral(resourceName: "no_camera") , for: .normal)}
            self.isError = video.isError
            self.url = video.result
        }
    }
    
    @objc func camClick(){
        if let lat = event.jdata.toJson()["lat"] as? Double, let lon = event.jdata.toJson()["lon"] as? Double{
            return openMapForPlace(lat: lat, lon: lon, name: self.mainView.siteView.text ?? "")
        }
        if isError { return AlertUtil.errorAlert(url) }
        guard let url = URL(string: url), let nV = nV else { return AlertUtil.errorAlert("ERROR".localized()) }
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewControllerRotatable()
        playerController.player = player
        nV.present(playerController, animated: true) {
            player.play()
        }
    }
    
    func openMapForPlace(lat: Double, lon: Double, name: String) {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = lon
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
}


class AVPlayerViewControllerRotatable: AVPlayerViewController {
    override var shouldAutorotate: Bool {
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
