//
//  EventCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 24.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
//import RxSwift

class EventCell: BaseCell<EventView>{
    static let cellId = "EventCell"
    
    func setContent(_ event: XObjectEntity.Affect) {
        selectionStyle = .none
        mainView.set(img: event.icon, device: event.device, sectionZone: event.sectionZone, desc: event.desc)
    }
}

class EventMiniCell: UICollectionViewCell{
    static let cellId = "EventMiniCell"
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(iconView)
        iconView.snp.makeConstraints { (make) in make.top.leading.trailing.bottom.equalToSuperview() }
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    @available(*, deprecated, message: "use setContent(img: UIImage)")
    func setContent(_ imgName: String) { iconView.image = UIImage(named: imgName)}
    
    func setContent(img: String) { iconView.image = UIImage(named: img) }
}

