//
//  EventView.swift
//  SecurityHub test
//
//  Created by Timerlan on 24.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class EventView: UIView {
    static let height: CGFloat = 55
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var deviceView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.medium)
        view.numberOfLines = 1
        return view
    }()
    private lazy var sectionZoneView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.medium)
        view.numberOfLines = 1
        return view
    }()
    
    private lazy var descView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10)
        view.numberOfLines = 2
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconView)
        addSubview(deviceView)
        addSubview(sectionZoneView)
        addSubview(descView)
        updateConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(7.5)
            make.left.equalToSuperview().offset(10)
            make.height.width.equalTo(40)
            make.bottom.equalToSuperview().offset(-7.5)
        }
        deviceView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(2)
            make.left.equalTo(iconView.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        sectionZoneView.snp.remakeConstraints { (make) in
            make.top.equalTo(deviceView.snp.bottom)
            make.left.equalTo(iconView.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        descView.snp.remakeConstraints{ make in
            make.top.equalTo(sectionZoneView.snp.bottom).offset(4)
            make.left.equalTo(iconView.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
    }
    
    func set(img: String, device: String, sectionZone: String?, desc: String){
        descView.text = desc
        deviceView.text = device
        sectionZoneView.text = sectionZone
        iconView.image = UIImage(named: img)
    }
}
