//
//  PinController.swift
//  SecurityHub test
//
//  Created by Timerlan on 04.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

@available(*, deprecated, message: "use XPinCodeController")
class PinController: BaseController<PinView> {
    private var type: PinControllerType
    private var code: String?
    private var cancelTitle: String?
    private var cancel: (() -> Void)?
    private var complete: () -> Void
    
    init(type: PinControllerType = .inputPin, code: String? = nil, cancelTitle: String? = nil, cancel: (() -> Void)? = nil, complete: @escaping () -> Void) {
        self.type = type
        self.code = code
        self.cancelTitle = cancelTitle
        self.cancel = cancel
        self.complete = complete
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
    
    override func loadView() {
        super.loadView()
//        AppUtility.lockOrientation(.portrait)
        navigationController?.setNavigationBarHidden(true, animated: false)
        mainView.exitBtn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        mainView.touchBtn.addTarget(self, action: #selector(touch), for: .touchUpInside)
        mainView.setOnPinCodeInputed(onPinCodeInputed)
        if let cancelTitle = cancelTitle {
            mainView.exitBtn.setAttributedTitle(NSAttributedString(string: cancelTitle, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        }
        switch type {
        case .newPin:
            mainView.title.text = "WIZ_PIN_ADD_NEW_PIN".localized()
        case .repeatPin:
            mainView.title.text = "PN_ENTER_CONFIRM_PN".localized()
            mainView.exitBtn.setAttributedTitle(NSAttributedString(string: "SCRIPT_SET_BACK".localized(), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        case .inputPin:
            mainView.title.text = "PN_ENTER_ENTER_PN".localized()
            if DataManager.settingsHelper.touchId && SimpleTouch.isTouchIDEnabled == .success && !faceIDAvailable() {
                mainView.touchBtn.isHidden = false
                touch()
            }
        }
    }
    
    @objc func touch(){
        SimpleTouch.presentTouchID("FINGERPRINT_AUTH_TITLE".localized()) { resp in
            guard resp == .success else { return }
            self.mainView.code = DataManager.defaultHelper.pin
            self.onPinCodeInputed()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainView.code = ""
    }
    
    @objc func exit(){
        guard let cancel = cancel else { return exitAll() }
        cancel()
    }
    
    func exitAll() {
        self.navigationController?.present(AlertUtil.exitAlert({
            self.showLoader()
            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
                DataManager.disconnectDisposable?.dispose()
                self.hideLoader()
                NavigationHelper.shared.show(LoginController())
            })
        }), animated: true)
    }
    
    func onPinCodeInputed() {
        switch type {
        case .newPin:
            let vc = PinController(type: .repeatPin, code: mainView.code, cancel: {
                self.navigationController?.popViewController(animated: true)
            }, complete: complete)
            navigationController?.pushViewController(vc, animated: true)
        case .repeatPin:
            if mainView.code == code {
                DataManager.defaultHelper.pin = mainView.code
                complete()
            } else {
                AlertUtil.errorAlert("PN_ADD_INCORRECT_PIN".localized())
                mainView.code = ""
            }
        case .inputPin:
            if mainView.code == DataManager.shared.getUser().pin {
                DispatchQueue.main.async { self.complete() }
            }else{
                mainView.errorAnimation()
                vibrate()
            }
        }
    }
}

enum PinControllerType {
    case newPin
    case repeatPin
    case inputPin
}
