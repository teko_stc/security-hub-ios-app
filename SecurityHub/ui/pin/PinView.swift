//
//  PinView.swift
//  SecurityHub test
//
//  Created by Timerlan on 04.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PinView: BaseView {
    
    private lazy var backgroundImg: UIView = {
        let view = UIImageView()
        view.contentMode = UIView.ContentMode.scaleAspectFill
        if XTargetUtils.isHubTarget {
            view.image = UIImage(named: "background_photo")
        } else {
            view.image = #imageLiteral(resourceName: "logo_blue")
        }
        view.backgroundColor = UIColor.hubBackgroundColor
        return view
    }()
    
    private lazy var blur: UIVisualEffectView = { return UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark)) }()
    
    private lazy var cont: UIView = {return UIView()}()
    
    private lazy var oneBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 1
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "1", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var twoBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 2
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "2", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var threeBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 3
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "3", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var fourBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 4
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "4", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var fiveBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 5
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "5", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var sixBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 6
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "6", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var sevenBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 7
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "7", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var eightBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 8
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "8", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var nineBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 9
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "9", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    lazy var touchBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.white
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setImage(#imageLiteral(resourceName: "ic_fingerprint_48"),for: .normal)
        view.isHidden = true
        return view
    }()
    
    private lazy var zeroBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 38
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.tag = 0
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "0", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 26.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var cancelBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "N_BUTTON_CANCEL".localized(), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    lazy var exitBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubTransparent
        view.rippleBackgroundColor = UIColor.hubTransparent
        view.rippleColor = UIColor.hubMainColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setAttributedTitle(NSAttributedString(string: "button_exit".localized(), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), for: .normal)
        return view
    }()
    
    private lazy var fc: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        return view
    }()
    
    private lazy var tc: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        return view
    }()
    
    private lazy var thc: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        return view
    }()
    
    private lazy var foc: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        return view
    }()
    
    lazy var title: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16.0)
        view.textColor = UIColor.white
        return view
    }()
    
    override func setContent() {
        setBottomBackgroundColor(UIColor.black)
        contentView.isScrollEnabled = false
        contentView.addSubview(backgroundImg)
        contentView.addSubview(blur)
        contentView.addSubview(cont)
        cont.addSubview(title)
        cont.addSubview(fc)
        cont.addSubview(tc)
        cont.addSubview(thc)
        cont.addSubview(foc)
        cont.addSubview(oneBtn)
        cont.addSubview(twoBtn)
        cont.addSubview(threeBtn)
        cont.addSubview(fourBtn)
        cont.addSubview(fiveBtn)
        cont.addSubview(sixBtn)
        cont.addSubview(sevenBtn)
        cont.addSubview(eightBtn)
        cont.addSubview(nineBtn)
        cont.addSubview(zeroBtn)
        cont.addSubview(exitBtn)
        cont.addSubview(cancelBtn)
        cont.addSubview(touchBtn)
        oneBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        twoBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        threeBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        fourBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        fiveBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        sixBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        sevenBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        eightBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        nineBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        zeroBtn.addTarget(self, action: #selector(click), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
    }
    
    var code:String = "" {
        didSet {
             UIView.animate(withDuration: 0.2) {
                if self.code.count == 0 {
                    self.fc.backgroundColor = UIColor.hubTransparent;
                    self.tc.backgroundColor = UIColor.hubTransparent;
                    self.thc.backgroundColor = UIColor.hubTransparent;
                    self.foc.backgroundColor = UIColor.hubTransparent;
                } else
                if self.code.count == 1 {
                    self.fc.backgroundColor = UIColor.hubMainColor;
                    self.tc.backgroundColor = UIColor.hubTransparent;
                    self.thc.backgroundColor = UIColor.hubTransparent;
                    self.foc.backgroundColor = UIColor.hubTransparent;
                }else
                if self.code.count == 2 {
                    self.fc.backgroundColor = UIColor.hubMainColor;
                    self.tc.backgroundColor = UIColor.hubMainColor;
                    self.thc.backgroundColor = UIColor.hubTransparent;
                    self.foc.backgroundColor = UIColor.hubTransparent;
                }else
                if self.code.count == 3 {
                    self.fc.backgroundColor = UIColor.hubMainColor;
                    self.tc.backgroundColor = UIColor.hubMainColor;
                    self.thc.backgroundColor = UIColor.hubMainColor;
                    self.foc.backgroundColor = UIColor.hubTransparent;
                }else
                if self.code.count == 4 {
                    self.fc.backgroundColor = UIColor.hubMainColor;
                    self.tc.backgroundColor = UIColor.hubMainColor;
                    self.thc.backgroundColor = UIColor.hubMainColor;
                    self.foc.backgroundColor = UIColor.hubMainColor;
                }
            }
        }
    }
    
    private var void: (() -> Void)?
    func setOnPinCodeInputed(_ void: (()->Void)?){
        self.void = void
    }
    
    @objc func click(sender : UIButton) {
        if code.count < 4{
            code += String(sender.tag)
        }
        if code.count == 4{ void?() }
    }
    
    @objc func cancel(sender : UIButton) { code = "" }
    
    private var butHW = 77;
    private var butMar = 20;
    override func updateConstraints() {
        super.updateConstraints()
        backgroundImg.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(-50)
            make.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview().offset(50)
        }
        blur.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(-50)
            make.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview().offset(50)
        }
        cont.snp.remakeConstraints{ make in
            make.centerX.centerY.equalToSuperview()
            make.width.equalTo(butMar*4+butHW*3)
        }
        title.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        fc.snp.remakeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(20)
            make.centerX.equalToSuperview().offset(-22 * 1.5 - 21)
            make.height.width.equalTo(14)
        }
        tc.snp.remakeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(20)
            make.leading.equalTo(fc.snp.trailing).offset(22)
            make.height.width.equalTo(14)
        }
        thc.snp.remakeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(20)
            make.leading.equalTo(tc.snp.trailing).offset(22)
            make.height.width.equalTo(14)
        }
        foc.snp.remakeConstraints{ make in
            make.top.equalTo(title.snp.bottom).offset(20)
            make.leading.equalTo(thc.snp.trailing).offset(22)
            make.height.width.equalTo(14)
        }
        oneBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fc.snp.bottom).offset(40)
            make.leading.equalToSuperview().offset(butMar)
            make.height.width.equalTo(butHW)
        }
        twoBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fc.snp.bottom).offset(40)
            make.leading.equalTo(oneBtn.snp.trailing).offset(butMar)
            make.height.width.equalTo(butHW)
        }
        threeBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fc.snp.bottom).offset(40)
            make.leading.equalTo(twoBtn.snp.trailing).offset(butMar)
            make.trailing.equalToSuperview().offset(-butMar)
            make.height.width.equalTo(butHW)
        }
        fourBtn.snp.remakeConstraints{ make in
            make.top.equalTo(oneBtn.snp.bottom).offset(butMar)
            make.leading.equalToSuperview().offset(butMar)
            make.height.width.equalTo(butHW)
        }
        fiveBtn.snp.remakeConstraints{ make in
           make.top.equalTo(oneBtn.snp.bottom).offset(butMar)
            make.leading.equalTo(oneBtn.snp.trailing).offset(butMar)
            make.height.width.equalTo(butHW)
        }
        sixBtn.snp.remakeConstraints{ make in
           make.top.equalTo(oneBtn.snp.bottom).offset(butMar)
            make.leading.equalTo(twoBtn.snp.trailing).offset(butMar)
            make.trailing.equalToSuperview().offset(-butMar)
            make.height.width.equalTo(butHW)
        }
        sevenBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fourBtn.snp.bottom).offset(butMar)
            make.leading.equalToSuperview().offset(butMar)
            make.height.width.equalTo(butHW)
        }
        eightBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fourBtn.snp.bottom).offset(butMar)
            make.leading.equalTo(oneBtn.snp.trailing).offset(butMar)
            make.height.width.equalTo(butHW)
        }
        nineBtn.snp.remakeConstraints{ make in
            make.top.equalTo(fourBtn.snp.bottom).offset(butMar)
            make.leading.equalTo(twoBtn.snp.trailing).offset(butMar)
            make.trailing.equalToSuperview().offset(-butMar)
            make.height.width.equalTo(butHW)
        }
        touchBtn.snp.remakeConstraints{ make in
            make.top.equalTo(sevenBtn.snp.bottom).offset(butMar)
            make.leading.equalToSuperview().offset(butMar)
            make.height.width.equalTo(butHW)
        }
        zeroBtn.snp.remakeConstraints{ make in
            make.top.equalTo(eightBtn.snp.bottom).offset(butMar)
            make.leading.equalTo(oneBtn.snp.trailing).offset(butMar)
            make.height.width.equalTo(butHW)
        }
        exitBtn.snp.remakeConstraints{ make in
            make.top.equalTo(zeroBtn.snp.bottom)
            make.leading.equalToSuperview().offset(20)
            make.bottom.equalToSuperview()
            make.height.equalTo(40)
        }
        cancelBtn.snp.remakeConstraints{ make in
            make.top.equalTo(zeroBtn.snp.bottom)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
            make.height.equalTo(40)
        }
    }
    
    private var k = 0
    private var animDisposable : Disposable?
    func errorAnimation() {
        k+=1;
        code = ""
        fc.snp.updateConstraints{ make in make.centerX.equalToSuperview().offset(-22 * 1.5 - 61) }
        UIView.animate(withDuration: 0.05) { self.layoutIfNeeded() }
        animDisposable = Observable<Int>.timer(.milliseconds(50), scheduler: MainScheduler.instance).subscribe{ t in
            self.animDisposable?.dispose();
            self.fc.snp.updateConstraints{ make in make.centerX.equalToSuperview().offset(-22 * 1.5 + 19) }
            UIView.animate(withDuration: 0.1) { self.layoutIfNeeded() }
            self.animDisposable = Observable<Int>.timer(.milliseconds(50), scheduler: MainScheduler.instance).subscribe{ t in
                self.animDisposable?.dispose();
                self.fc.snp.updateConstraints{ make in make.centerX.equalToSuperview().offset(-22 * 1.5 - 21) }
                UIView.animate(withDuration: 0.05) { self.layoutIfNeeded() }
                self.animDisposable = Observable<Int>.timer(.milliseconds(50), scheduler: MainScheduler.instance).subscribe{ t in
                    self.animDisposable?.dispose();
                    if self.k == 2 { self.k = 0 } else{ self.errorAnimation() }
                }
            }
        }
    }
    
}
