//
//  AllEventsView.swift
//  SecurityHub test
//
//  Created by Timerlan on 17.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//


import UIKit
import RxSwift

class AllEventsView: BaseView{
    
    lazy var form: UIView = { var view = UIView(); view.backgroundColor = UIColor.white; view.layer.borderWidth = 0.5;view.layer.borderColor = UIColor.lightGray.cgColor; return view; }()
    
    lazy var objLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "N_EVENTS_SITE".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var objField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.setTitle("N_EVENTS_ALL_SITES".localized(), for: .normal)
        view.tag = -1
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var objIc: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        return view
    }()
    
    lazy var formType: UIView = { var view = UIView(); view.backgroundColor = UIColor.white; view.layer.borderWidth = 0.5;view.layer.borderColor = UIColor.lightGray.cgColor; return view; }()
    
    lazy var typeLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "N_EVENTS_CLASS".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var typeField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.setTitle("N_EVENTS_ALL_CLASSES".localized(), for: .normal)
        view.tag = -1
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var typeIc: ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        return view
    }()
    
    lazy var formDate: UIView = { var view = UIView(); view.backgroundColor = UIColor.white; view.layer.borderWidth = 0.5;view.layer.borderColor = UIColor.lightGray.cgColor; return view; }()
    
    lazy var fromLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "N_EVENTS_FROM".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var fromField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var toLable: UILabel = {
        var view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = "N_EVENTS_TO".localized()
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var toField: ZFRippleButton = {
        var view = ZFRippleButton()
        view.setTitleColor(UIColor.black, for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.contentHorizontalAlignment = .left
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var table : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        view.backgroundColor = UIColor.hubBackgroundColor
        return view
    }()
    
    var listener: (( _ obj: Int64, _ _class: Int64, _ from: Int64, _ to: Int64)->Void)?
    
    override func setContent() {
        contentView.alwaysBounceVertical = false
        contentView.addSubview(table)
        contentView.addSubview(form)
        form.addSubview(objLable)
        form.addSubview(objField)
        form.addSubview(objIc)
        contentView.addSubview(formType)
        formType.addSubview(typeLable)
        formType.addSubview(typeField)
        formType.addSubview(typeIc)
        contentView.addSubview(formDate)
        formDate.addSubview(fromLable)
        formDate.addSubview(fromField)
        formDate.addSubview(toLable)
        formDate.addSubview(toField)
        
        var time = Int64(NSDate().timeIntervalSince1970)
        time = time / 86400
        time = time * 86400 + 86399 - Int64(TimeZone.current.secondsFromGMT())
        toField.setTitle(time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
        toField.tag = Int(time)
        fromField.setTitle((time  - 86400 * 3).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
        fromField.tag = Int(time  - 86400 * 3)
        
        objField.addTarget(self, action: #selector(obj), for: .touchUpInside)
        objIc.addTarget(self, action: #selector(obj), for: .touchUpInside)
        typeField.addTarget(self, action: #selector(type), for: .touchUpInside)
        typeIc.addTarget(self, action: #selector(type), for: .touchUpInside)
        fromField.addTarget(self, action: #selector(from), for: .touchUpInside)
        toField.addTarget(self, action: #selector(to), for: .touchUpInside)
    }
    
    private var objDisp: Disposable?
    @objc func obj(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        alert.addAction(UIAlertAction(title: "N_EVENTS_ALL_SITES".localized(), style: .default, handler: { a in
            self.objField.setTitle("N_EVENTS_ALL_SITES".localized(), for: .normal)
            self.objField.tag = -1
            self.listener?(-1,Int64(self.typeField.tag),Int64(self.fromField.tag),Int64(self.toField.tag))
        }))
        objDisp = DataManager.shared.getSites(completable: true)
            .toArray()
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: { sites in
                for site in sites{
                    alert.addAction(UIAlertAction(title: site.site.name, style: .default, handler: { a in
                        self.objField.setTitle(site.site.name, for: .normal)
                        self.objField.tag = Int(site.site.id)
                        self.listener?(site.site.id,Int64(self.typeField.tag),Int64(self.fromField.tag),Int64(self.toField.tag))
                    }))
                }
                alert.addAction(UIAlertAction(title:  "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
                self.nV?.present(alert, animated: false)
            })
    }
    
    @objc func type(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        alert.addAction(UIAlertAction(title: "N_EVENTS_ALL_CLASSES".localized(), style: .default, handler: { a in
            self.typeField.setTitle("N_EVENTS_ALL_CLASSES".localized(), for: .normal)
            self.typeField.tag = -1
            self.listener?(Int64(self.objField.tag),-1,Int64(self.fromField.tag),Int64(self.toField.tag))
        }))
        _ = DataManager.shared.getAllClasses().subscribe(onNext: { classes in
            for cl in classes{
                alert.addAction(UIAlertAction(title: cl.name, style: .default, handler: { a in
                    self.typeField.setTitle(cl.name, for: .normal)
                    self.typeField.tag = Int(cl.id)
                    self.listener?(Int64(self.objField.tag),cl.id,Int64(self.fromField.tag),Int64(self.toField.tag))
                }))
            }
            alert.addAction(UIAlertAction(title: "N_BUTTON_CANCEL".localized(), style: .cancel, handler: nil))
            self.nV?.present(alert, animated: false)
        })
    }
    
    @objc func from(){
        let start_time = Int64(NSDate().timeIntervalSince1970) - (86400 * 180)
        let a = AlertUtil.alertDatePicker({ (date) in
            let year = Calendar(identifier: .gregorian).component(Calendar.Component.year, from: date)
            let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: date)
            let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: date)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let _date = formatter.date(from: "\(year)/\(month)/\(day)")!
            
            let time = _date.timeIntervalSince1970
            self.fromField.setTitle(Int64(time).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
            self.fromField.tag = Int(time)
            self.listener?(Int64(self.objField.tag), Int64(self.typeField.tag), Int64(time), Int64(self.toField.tag))
        }, max: Int64(toField.tag) + 1, min: start_time)
        nV?.present(a, animated: false)
    }
    
    @objc func to(){
        let end_time = Int64(NSDate().timeIntervalSince1970)
        let a = AlertUtil.alertDatePicker({ (date) in
            let next_day = Date(timeInterval: 86400, since: date)
            
            let year = Calendar(identifier: .gregorian).component(Calendar.Component.year, from: next_day)
            let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: next_day)
            let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: next_day)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let _date = formatter.date(from: "\(year)/\(month)/\(day)")!
        
            let time = _date.timeIntervalSince1970
            self.toField.setTitle(Int64(date.timeIntervalSince1970).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
            self.toField.tag = Int(time)
            self.listener?(Int64(self.objField.tag), Int64(self.typeField.tag), Int64(self.fromField.tag), Int64(time))
        }, max: end_time, min: Int64(fromField.tag))
        nV?.present(a, animated: false)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        form.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        formType.snp.remakeConstraints{ make in
            make.top.equalTo(form.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        formDate.snp.remakeConstraints{ make in
            make.top.equalTo(formType.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        table.snp.remakeConstraints{make in
            make.top.equalTo(formDate.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalToSuperview().offset(45 * -3)
        }
        objLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.centerY.equalTo(objField.snp.centerY)
        }
        objField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(objLable.snp.trailing)
        }
        objIc.snp.remakeConstraints{ make in
            make.leading.equalTo(objField.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
            make.centerY.equalTo(objField.snp.centerY)
        }
        typeLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.centerY.equalTo(typeField.snp.centerY)
        }
        typeField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(typeLable.snp.trailing)
        }
        typeIc.snp.remakeConstraints{ make in
            make.leading.equalTo(typeField.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
            make.centerY.equalTo(typeField.snp.centerY)
        }
        fromLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(50)
            make.centerY.equalTo(fromField.snp.centerY)
        }
        fromField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(fromLable.snp.trailing)
            make.width.equalTo(toField.snp.width)
        }
        toLable.snp.remakeConstraints{ make in
            make.leading.equalTo(fromField.snp.trailing)
            make.width.equalTo(40)
            make.centerY.equalTo(toField.snp.centerY)
        }
        toField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(toLable.snp.trailing)
            make.trailing.equalToSuperview().offset(5)
        }
    }
}
