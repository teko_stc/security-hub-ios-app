//
//  LoginView.swift
//  SecurityHub
//
//  Created by Timerlan on 29.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import UIKit

class LoginView: BaseView, UITextFieldDelegate  {
    private var header_height = 200
    private var image_top_margin = 0
    
    private lazy var logoView: UIImageView = {
        var view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.image = #imageLiteral(resourceName: "logo_white_name")
        return view
    }()
    
    private lazy var header: UIImageView = {
        var view = UIImageView()
        view.contentMode = UIView.ContentMode.scaleAspectFill
        view.image = #imageLiteral(resourceName: "background_photo")
        view.backgroundColor = UIColor.hubMainColor
        return view
    }()
    
    private lazy var back: UIView = {var view = UIView();view.backgroundColor = UIColor.hubBackgroundColor;return view;}()
    private lazy var form: UIView = {
        var view = UIView();
        view.backgroundColor = UIColor.white;
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        return view;
    }()
    
    private lazy var lineH: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineF: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    private lazy var lineSaveMe: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    lazy var line: UIView = {var view = UIView();view.backgroundColor = UIColor.lightGray;return view;}()
    
    private lazy var loginLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = ""
        return view
    }()
    
    lazy var loginField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "DOMAIN_USER_ENTER_LOGIN_EDIT".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        view.autocapitalizationType = .none
        view.autocorrectionType = .no
        view.keyboardType = .emailAddress
        view.textContentType = .username
        view.returnKeyType = UIReturnKeyType.next
        return view
    }()
    
    private lazy var passLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = ""
        return view
    }()
    
    lazy var passField: UITextField = {
        var view = UITextField()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.attributedPlaceholder = NSAttributedString(string: "DOMAIN_USER_ENTER_PASS_EDIT".localized(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        view.autocapitalizationType = .none
        view.isSecureTextEntry = true
        view.textContentType = .password
        view.returnKeyType = UIReturnKeyType.go
        return view
    }()
    
    private lazy var saveMeLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = "LA_REMEMBER_ME".localized()
        return view
    }()
    
    lazy var saveMeView: UISwitch = {
        var view = UISwitch()
        view.onTintColor = UIColor.hubMainColor
        view.addTarget(self, action: #selector(swi), for: .valueChanged)
        view.isOn = !UserDefaults.standard.bool(forKey: "saveMeView")
        return view
    }()
    @objc func swi(){
        UserDefaults.standard.set(!saveMeView.isOn, forKey: "saveMeView")
    }
    
    lazy var loginBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubRipleBackgroundColor
        view.ripplePercent = 1
        view.setTitle("WIZ_LOGIN_GO_ENTER".localized(), for: .normal)
        return view
    }()
    
    lazy var losePassBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubBackgroundColor
        view.rippleBackgroundColor = UIColor.hubBackgroundColor
        view.rippleColor = UIColor.hubRipleColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setTitle("N_WIZ_PIN_LOST".localized(), for: .normal)
        return view
    }()
    
    lazy var orLable: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.lightGray
        view.backgroundColor = UIColor.hubBackgroundColor
        view.textAlignment = NSTextAlignment.center
        view.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.regular)
        view.text = ""
        return view
    }()
    
    lazy var registrationBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubBackgroundColor
        view.rippleBackgroundColor = UIColor.hubBackgroundColor
        view.rippleColor = UIColor.hubRipleColor
        view.setTitleColor(UIColor.hubMainColor, for: UIControl.State.normal)
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.hubMainColor.cgColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setTitle("WIZ_WELC_REGISTRATION".localized(), for: .normal)
        return view
    }()

    override func setContent() {
        setBottomBackgroundColor(UIColor.hubBackgroundColor)
        contentView.addSubview(header)
        contentView.addSubview(lineH)
        contentView.addSubview(logoView)
        contentView.addSubview(back)
        contentView.addSubview(form)
        
        form.addSubview(loginLable)
        form.addSubview(loginField)
        form.addSubview(lineF)
        form.addSubview(passLable)
        form.addSubview(passField)
        form.addSubview(lineSaveMe)
        form.addSubview(saveMeView)
        form.addSubview(saveMeLable)
        
        contentView.addSubview(loginBtn)
        
        //if (XTargetUtils.target != .legion)
        //{
            contentView.addSubview(losePassBtn)
            contentView.addSubview(line)
            contentView.addSubview(orLable)
            contentView.addSubview(registrationBtn)
        //}
        
        
        loginField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        loginField.delegate = self
        passField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        textFieldDidChange(textField: loginField)
        header_height = UIDevice().type == .iPhone5 || UIDevice().type == .iPhone5S || UIDevice().type == .iPhone5C || UIDevice().type == .iPhoneSE ? 200 : 250
        image_top_margin = UIDevice().type == .iPhone5 || UIDevice().type == .iPhone5S || UIDevice().type == .iPhone5C || UIDevice().type == .iPhoneSE ? 0 : 20
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        passField.becomeFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        if loginField.text?.count ?? 0 > 0 && passField.text?.count ?? 0 > 0 {
            loginBtn.isEnabled = true
            loginBtn.setTitleColor(UIColor.white, for: .normal)
        }else{
            loginBtn.isEnabled = false
            loginBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        header.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(-50)
            make.centerX.width.equalToSuperview()
            make.height.equalTo(header_height)
        }
        lineH.snp.remakeConstraints{make in
            make.top.equalTo(header.snp.bottom)
            make.leading.trailing.width.equalToSuperview()
            make.height.equalTo(1)
        }
        logoView.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(image_top_margin)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(160)
        }
        back.snp.remakeConstraints{make in
            make.top.equalTo(lineH.snp.bottom)
            make.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview().offset(-300)
        }
        form.snp.remakeConstraints{make in
            make.top.equalTo(lineH.snp.bottom).offset(30)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        loginLable.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(15)
            make.width.equalTo(80)
        }
        loginField.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.leading.equalTo(loginLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalTo(lineF.snp.top)
        }
        lineF.snp.remakeConstraints{ make in
            make.top.equalTo(loginLable.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        passLable.snp.remakeConstraints{ make in
            make.top.equalTo(lineF.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.bottom.equalTo(lineSaveMe.snp.top).offset(-15)
            make.width.equalTo(80)
        }
        passField.snp.remakeConstraints{ make in
            make.top.equalTo(lineF.snp.bottom)
            make.leading.equalTo(passLable.snp.trailing)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalTo(lineSaveMe.snp.top)
        }
        lineSaveMe.snp.remakeConstraints{ make in
            make.top.equalTo(passField.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        saveMeLable.snp.remakeConstraints { (make) in
            make.centerY.equalTo(saveMeView.snp.centerY)
            make.leading.equalToSuperview().offset(15)
//            make.bottom.equalToSuperview().offset(-15)
        }
        saveMeView.snp.remakeConstraints { (make) in
            make.top.equalTo(lineSaveMe.snp.bottom).offset(15)
            make.right.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview().offset(-15)
        }
        loginBtn.snp.remakeConstraints{ make in
            make.top.equalTo(form.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        losePassBtn.snp.remakeConstraints{ make in
            make.top.equalTo(loginBtn.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        line.snp.remakeConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.height.equalTo(1)
        }
        orLable.snp.remakeConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(50)
        }
        registrationBtn.snp.remakeConstraints{ make in
            make.top.equalTo(line.snp.bottom).offset(30)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
    }
    
    func moveViewToUp() {
        header.snp.updateConstraints{ make in make.height.equalTo(114) }
        logoView.snp.updateConstraints{make in
            if !XTargetUtils.isHubTarget {
                make.height.width.equalTo(60)
            } else {
                make.height.width.equalTo(92)
            }
            make.top.equalToSuperview()
        }
        form.snp.updateConstraints{ make in make.top.equalTo(lineH.snp.bottom).offset(15) }
        line.snp.updateConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(0)
            make.height.equalTo(0)
        }
        orLable.snp.updateConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(0)
            make.height.equalTo(0)
        }
        registrationBtn.snp.updateConstraints{ make in make.top.equalTo(line.snp.bottom).offset(15) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
    
    func moveViewToBottom() {
        header.snp.updateConstraints{ make in make.height.equalTo(header_height) }
        logoView.snp.updateConstraints{ make in
            make.height.width.equalTo(160)
            make.top.equalToSuperview().offset(image_top_margin)
        }
        form.snp.updateConstraints{ make in make.top.equalTo(lineH.snp.bottom).offset(30) }
        line.snp.updateConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(20)
            make.height.equalTo(1)
        }
        orLable.snp.updateConstraints{ make in
            make.top.equalTo(losePassBtn.snp.bottom).offset(10)
            make.height.equalTo(20)
        }
        registrationBtn.snp.updateConstraints{ make in make.top.equalTo(line.snp.bottom).offset(30) }
        UIView.animate(withDuration: 1) { self.layoutIfNeeded() }
    }
}

