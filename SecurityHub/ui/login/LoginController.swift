//
//  LogoController.swift
//  SecurityHub
//
//  Created by Timerlan on 29.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import UIKit
import RxSwift
import Localize_Swift

@available(*, deprecated, message: "use XLoginController")
class LoginController: BaseController<LoginView>, UITextFieldDelegate {
    
    override func loadView() {
        super.loadView()
//        AppUtility.lockOrientation(.portrait)
        navigationController?.setNavigationBarHidden(true, animated: false)
        mainView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        mainView.passField.delegate = self
        mainView.loginBtn.addTarget(self, action: #selector(login), for: .touchUpInside)
        mainView.losePassBtn.addTarget(self, action: #selector(losePass), for: .touchUpInside)
        mainView.registrationBtn.addTarget(self, action: #selector(registration), for: .touchUpInside)
        
//        if (XTargetUtils.site == nil) {
//            mainView.registrationBtn.isHidden = true
//            mainView.line.isHidden = true
//            mainView.orLable.isHidden = true
//            mainView.losePassBtn.isHidden = true
//        }
        
        if !UserDefaults.standard.bool(forKey: "saveMeView"), let login = UserDefaults.standard.string(forKey: "saveMeViewLogin") {
            mainView.loginField.text = login
            mainView.passField.text  = "***tim***"
            mainView.textFieldDidChange(textField: mainView.loginField)
        }
    }

    @objc func login(){
        mainView.removeConnactionStateChange()
        guard   let login = mainView.loginField.text,
                var pass = mainView.passField.text,
                login.count > 0 && pass.count > 0 else { return }
        if pass == "***tim***" { pass = UserDefaults.standard.string(forKey: "saveMeViewPass") ?? "" } else { pass = pass.md5 }
        showLoader()
        UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
        UserDefaults.standard.set(nil, forKey: "saveMeViewPass")
        DataManager.connectDisposable = DataManager.shared.connect(login: login, password: pass)
            .subscribe(onNext:{ result in
                DataManager.connectDisposable?.dispose()
                self.hideLoader()
                if result.success {
                    self.navigationController?.pushViewController(PinController(type: .newPin, code: nil, cancelTitle: nil, cancel: nil, complete: {
                        NavigationHelper.shared.showMain()
                    }), animated: true)
                    if !UserDefaults.standard.bool(forKey: "saveMeView") {
                        UserDefaults.standard.set(login, forKey: "saveMeViewLogin")
                        UserDefaults.standard.set(pass, forKey: "saveMeViewPass")
                    }
                }else{
                    AlertUtil.errorAlert(result.errorMes!)
                }
            })
    }
    
    @objc func losePass(){
        guard var url = XTargetUtils.regLink else { return }
        if(XTargetUtils.target == .security_hub){
            url = "\(url)?p=101&l=\(Localize.currentLanguage())"
        }
        AlertUtil.urlAlert("N_WIZ_PIN_LOST".localized(), message: "LA_PASS_REC_DIALOG_MESS_1".localized() + " SECURITY HUB " + "LA_PASS_REC_DIALOG_MESS_2".localized(), url: url, nV: navigationController)
    }
    
    @objc func registration(){
        guard var url = XTargetUtils.regLink else { return }
        if(XTargetUtils.target == .security_hub){
            url = "\(url)?p=100&l=\(Localize.currentLanguage())"
        }
        AlertUtil.urlAlert("WIZ_WELC_REGISTRATION".localized(), message: "LA_SINGUP_DIALOG_MESS_1".localized() + " SECURITY HUB " + "LA_SINGUP_DIALOG_MESS_2".localized(), url: url, nV: navigationController)
    }
    
    @objc func keyboardWillShow(notification: Notification) { DispatchQueue.main.async { self.mainView.moveViewToUp()} }
    
    @objc func keyboardWillDissmiss(notification: Notification) { DispatchQueue.main.async { self.mainView.moveViewToBottom() } }
    
    @objc func viewTapped() {
        if mainView.loginField.isFirstResponder == true { mainView.loginField.resignFirstResponder()}
        if mainView.passField.isFirstResponder == true { mainView.passField.resignFirstResponder()}
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {login(); return true;}
}
