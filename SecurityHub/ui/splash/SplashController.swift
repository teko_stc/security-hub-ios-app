//
//  SplashController.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
//import RxSwift

@available(*, deprecated, message: "use XSplashController")
class SplashController: BaseController<SplashView> {
    
    override func loadView() {
        super.loadView()
//        AppUtility.lockOrientation(.portrait)
        navigationController?.setNavigationBarHidden(true, animated: false)
        mainView.removeConnactionStateChange()
    }
    
    override func viewDidLoad() {
        checkUser()
    }

    private func checkUser(){
        DataManager.connectDisposable = DataManager.shared.connect().subscribe(onNext: { success in
            DataManager.connectDisposable?.dispose()
            if !success { NavigationHelper.shared.show(LoginController()) }else{ self.needPin() }
        })
    }
    
    private func needPin(){
        if DataManager.settingsHelper.needPin {
            NavigationHelper.shared.show(PinController(complete: {
                NavigationHelper.shared.showMain()
            }))
        }else{
            NavigationHelper.shared.showMain()
        }
    }
}
