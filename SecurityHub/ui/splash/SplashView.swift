//
//  SplashView.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SplashView : BaseView {
    
    private lazy var logo: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "logo_blue_name")
        view.contentMode = .scaleAspectFill
        return view;
    }()
    
    private lazy var loader: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        view.center = view.center
        view.hidesWhenStopped = false
        return view
    }()
    
    override func setContent() {
        setBottomBackgroundColor(UIColor.hubBackgroundColor)
        contentView.alwaysBounceVertical = false
        contentView.addSubview(logo)
        contentView.addSubview(loader)
        loader.startAnimating()
        if !XTargetUtils.isHubTarget {
            loader.isHidden = true
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        logo.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview().offset(-UIApplication.shared.statusBarFrame.height)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(175)
        }
        loader.snp.remakeConstraints{make in
            make.centerX.equalToSuperview().offset(16)
            make.centerY.equalToSuperview().offset(-UIApplication.shared.statusBarFrame.height - 35)
        }
//        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436{
//            logo.snp.updateConstraints{ make in make.centerY.equalToSuperview().offset(12)}
//            loader.snp.updateConstraints{make in make.centerY.equalToSuperview().offset(-22) }
//        }
    }
    
}
