//
//  XAlertResetPinCodeViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XAlertResetPinCodeViewStrings {
    /// Reset PIN
    var title: String { "PA_PN_CHANGE_TITLE".localized() }
    
    /// Reset PIN TEXT
    var text: String { "PA_PIN_CHANGE_MESS".localized() }
    
    /// Enter password
    var passwordPlaceholder: String { "WIZ_REG_ENTERPASS".localized() }
    
    /// Reset
    var positive: String { "N_BUTTON_RESET".localized() }
    
    /// Cancel
    var negative: String { "CANCEL_SMALL".localized() }
    
    /// Внимание
    var titleResetFromSetttings: String { "FINGERHANDLER_ATTENTION".localized() }
    
    /// Будьте внимательны? после отключения PIN-кода доступ в ваше приложение не будет защищенным. Для отключения PIN-кода необходимо ввести пароль от вашей учетной записи:
    var textResetFromSetttings: String { "PIN_DISABLE_DIALOG_MESS".localized() }
    
    /// Продолжить
    var positiveResetFromSetttings: String { "DOMAIN_USER_ENTER_NAME_CONT".localized() }
    
    /// Отмена
    var negativeResetFromSetttings: String { "DOMAIN_USER_ENTER_NAME_CANCEL".localized() }
}
