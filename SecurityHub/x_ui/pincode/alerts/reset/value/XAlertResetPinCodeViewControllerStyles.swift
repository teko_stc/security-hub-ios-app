//
//  XAlertResetPinCodeViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAlertResetPinCodeView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: XAlertResetPinCodeView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: XAlertResetPinCodeView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1)
            ),
            field: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                errorColor: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
            ),
            negative: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
