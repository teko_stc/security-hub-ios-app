//
//  XAlertResetPinCodeViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XAlertResetPinCodeViewStyle {
    struct Lable {
        let font: UIFont?
        let color: UIColor
        var alignment: NSTextAlignment = .center
    }
    
    let backgroundColor: UIColor
    let title: Lable
    let text: Lable
    let field: XTextField.Style
    let positive: XZoomButton.Style
    let negative: XZoomButton.Style
}
extension XAlertResetPinCodeView {
    typealias Style = XAlertResetPinCodeViewStyle
}

