//
//  XAlertResetPinCodeView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertResetPinCodeView: UIView {
    public var delegate: XAlertResetPinCodeViewDelegate?
    private let titleView = UILabel()
    private let textView = UILabel()
    private var passwordView: XPasswordView!
    private var positiveView, negativeView: XZoomButton!
    private let strings: XAlertResetPinCodeViewStrings = XAlertResetPinCodeViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func passwordNotCorrect() {
        passwordView.showValidationError()
    }
    
    public func setResetFromSettingsStyle() {
        titleView.text = strings.titleResetFromSetttings
        textView.text = strings.textResetFromSetttings
        positiveView.text = strings.positiveResetFromSetttings
        negativeView.text = strings.negativeResetFromSetttings
    }
    
    private func initViews(style: Style) {
        backgroundColor = style.backgroundColor
        addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))

        titleView.numberOfLines = 0
        titleView.text = strings.title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        addSubview(titleView)
        
        textView.numberOfLines = 0
        textView.text = strings.text
        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.textAlignment = style.text.alignment
        addSubview(textView)
        
        passwordView = XPasswordView(style: style.field)
        passwordView.placeholder = strings.passwordPlaceholder
        passwordView.returnKeyType = .done
        passwordView.xDelegate = self
        addSubview(passwordView)
        
        positiveView = XZoomButton(style: style.positive)
        positiveView.text = strings.positive
        positiveView.addTarget(self, action: #selector(positiveViewTapped), for: .touchUpInside)
        addSubview(positiveView)

        negativeView = XZoomButton(style: style.negative)
        negativeView.text = strings.negative
        negativeView.addTarget(self, action: #selector(negativeViewTapped), for: .touchUpInside)
        addSubview(negativeView)
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        passwordView.translatesAutoresizingMaskIntoConstraints = false
        positiveView.translatesAutoresizingMaskIntoConstraints = false
        negativeView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            titleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            titleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),

            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 10),
            textView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            textView.centerXAnchor.constraint(equalTo: centerXAnchor),
            textView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            textView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            passwordView.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 20),
            passwordView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            passwordView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            passwordView.heightAnchor.constraint(equalToConstant: 52),
            passwordView.widthAnchor.constraint(equalTo: widthAnchor),
            
            positiveView.topAnchor.constraint(equalTo: passwordView.bottomAnchor, constant: 20),
            positiveView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            positiveView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            negativeView.heightAnchor.constraint(equalToConstant: 40),
            positiveView.widthAnchor.constraint(equalTo: widthAnchor),
            
            negativeView.topAnchor.constraint(equalTo: positiveView.bottomAnchor, constant: 6),
            negativeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            negativeView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            negativeView.heightAnchor.constraint(equalToConstant: 40),
            negativeView.widthAnchor.constraint(equalTo: widthAnchor),
            negativeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
        ])
    }
    
    @objc private func positiveViewTapped() {
        if (passwordView.text ?? "").isEmpty { passwordNotCorrect()}
        else { delegate?.xAlertResetPinCodeViewReset(password: passwordView.text!) }
    }
    
    @objc private func negativeViewTapped() { delegate?.xAlertResetPinCodeViewCancel() }
    
    @objc private func viewTapped() {
        if passwordView.isFirstResponder { passwordView.resignFirstResponder()}
    }
}

extension XAlertResetPinCodeView: XTextFieldDelegete {
    func xTextFieldShouldReturn(_ textField: UITextField) {
        positiveViewTapped()
    }
    
    func xTextFieldDidChangeSelection(_ textField: UITextField) {
        _ = passwordView.textFieldShouldBeginEditing(passwordView)
    }
}
