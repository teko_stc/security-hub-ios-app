//
//  XAlertResetPinCodeViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XAlertResetPinCodeViewDelegate {
    func xAlertResetPinCodeViewReset(password: String)
    func xAlertResetPinCodeViewCancel()
}
