//
//  XAlertResetPinCodeController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertResetPinCodeController: XBaseAlertController<XAlertResetPinCodeView> {
    override var widthAnchor: CGFloat { 0.9 }
    
    private let confirmVoid: (_: XAlertResetPinCodeController, _: String) -> ()
    private let cancelVoid: ((_: XAlertResetPinCodeController)->())?
    
    init(confirm: @escaping (_: XAlertResetPinCodeController, _: String) -> (), cancel: ((_: XAlertResetPinCodeController)->())? = nil, isResetFromSettings: Bool = false) {
        self.confirmVoid = confirm
        self.cancelVoid = cancel
        super.init()
        mainView.setResetFromSettingsStyle()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func passwordNotCorrect() { mainView.passwordNotCorrect() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) { verticalAlignment = .top }
    
    @objc func keyboardWillDissmiss(notification: Notification) { verticalAlignment = .center }
}

extension XAlertResetPinCodeController: XAlertResetPinCodeViewDelegate {
    func xAlertResetPinCodeViewReset(password: String) {
        self.confirmVoid(self, password)
    }
    
    func xAlertResetPinCodeViewCancel() {
        dismiss(animated: true) {
            self.cancelVoid?(self)
        }
    }
}

