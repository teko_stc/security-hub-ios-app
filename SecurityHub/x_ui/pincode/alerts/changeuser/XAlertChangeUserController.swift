//
//  XChangeUserAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertChangeUserController: XBaseAlertController<XAlertChangeUserView> {
    override var widthAnchor: CGFloat { 0.8 }

    private let confirmVoid: (_: XAlertChangeUserController, _: Bool) -> ()
    private let cancelVoid: ((_: XAlertChangeUserController)->())?
    
    init(confirm: @escaping (_: XAlertChangeUserController, _: Bool) -> (), cancel: ((_: XAlertChangeUserController)->())? = nil) {
        self.confirmVoid = confirm
        self.cancelVoid = cancel
        super.init()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
    }
}

extension XAlertChangeUserController: XAlertChangeUserViewDelegate {
    func xAlertChangeUserViewNotTapped() {
        dismiss(animated: true) {
            self.cancelVoid?(self)
        }
    }
    
    func xAlertChangeUserViewYepTapped(isSaveConfig: Bool) {
        dismiss(animated: true) {
            self.confirmVoid(self, isSaveConfig)
        }
    }
}
