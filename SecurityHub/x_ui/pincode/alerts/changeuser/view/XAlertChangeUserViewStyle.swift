//
//  XChangeUserAlertStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XAlertChangeUserViewStyle {
    struct Lable {
        let color: UIColor
        let font: UIFont?
    }
    
    let backgroungColor: UIColor
    let title: Lable
    let checkBox: XCheckBoxView.Style
    let close: XZoomButton.Style
    let next: XZoomButton.Style
    
}
extension XAlertChangeUserView {
    typealias Style = XAlertChangeUserViewStyle
}

