//
//  XChangeUserAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertChangeUserView: UIView {
    public var delegate: XAlertChangeUserViewDelegate?
    private var titleView = UILabel()
    private var checkBoxView: XCheckBoxView!
    private var closeView, nextView: XZoomButton!
    private let strings: XAlertChangeUserViewControllerStrings = XAlertChangeUserViewControllerStrings()

    init() {
        super.init(frame: .zero)
        initViews(style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroungColor
        
        titleView.text = strings.title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = .center
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        checkBoxView = XCheckBoxView(style.checkBox)
        checkBoxView.text = strings.checkBox
        checkBoxView.isSelected = true
        addSubview(checkBoxView)
        
        closeView = XZoomButton(style: style.close)
        closeView.text = strings.close
        closeView.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        addSubview(closeView)

        nextView = XZoomButton(style: style.next)
        nextView.text = strings.next
        nextView.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
        addSubview(nextView)
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        checkBoxView.translatesAutoresizingMaskIntoConstraints = false
        closeView.translatesAutoresizingMaskIntoConstraints = false
        nextView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor),
    
            checkBoxView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 10),
            checkBoxView.leadingAnchor.constraint(equalTo: leadingAnchor),
            checkBoxView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            checkBoxView.heightAnchor.constraint(equalToConstant: 40),
            
            closeView.topAnchor.constraint(equalTo: checkBoxView.bottomAnchor, constant: 20),
            closeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            closeView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            closeView.heightAnchor.constraint(equalToConstant: 40),
            closeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            nextView.topAnchor.constraint(equalTo: closeView.topAnchor),
            nextView.leadingAnchor.constraint(equalTo: closeView.trailingAnchor),
            nextView.bottomAnchor.constraint(equalTo: closeView.bottomAnchor),
            nextView.widthAnchor.constraint(equalTo: closeView.widthAnchor),
            nextView.heightAnchor.constraint(equalTo: closeView.heightAnchor),
        ])
    }
        
    @objc private func closeTapped() { delegate?.xAlertChangeUserViewNotTapped() }
    
    @objc private func nextTapped() { delegate?.xAlertChangeUserViewYepTapped(isSaveConfig: checkBoxView.isSelected) }
}

