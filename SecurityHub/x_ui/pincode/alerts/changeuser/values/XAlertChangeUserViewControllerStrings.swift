//
//  XAlertChangeUserViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XAlertChangeUserViewControllerStrings {
    /// Do you really want to log out of your account?\nAll your app user data will be deleted.
    var title: String { "PA_EXIT_DIALOG_TITLE_1".localized() }
    
    /// Save my configuration
    var checkBox: String { "N_LOGOUT_SAVE_APP_DATA".localized() }
    
    /// Not
    var close: String { "N_BUTTON_NO".localized() }
    
    /// Yes
    var next: String { "N_BUTTON_YES".localized() }
}
