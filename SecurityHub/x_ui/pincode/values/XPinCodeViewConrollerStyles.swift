//
//  XPinCodeViewConrollerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XPinCodeView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            pagination: Style.Pagination(
                select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                unselect: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                error: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1)
            ),
            numpad: XPinCodeViewButton.Style(
                title: XPinCodeViewButton.Style.Lable(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 30)
                ),
                subTitle: XPinCodeViewButton.Style.Lable(
                    color: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 12)
                )
            ),
            touch: XPinCodeViewButton.Style(
                icon: XPinCodeViewButton.Style.Icon(
                    name: "pin",
                    color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                    topAnchor: -25
                )
            ),
            back: XPinCodeViewButton.Style(
                icon: XPinCodeViewButton.Style.Icon(
                    name: "ic_delete-3",
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    topAnchor: -27
                )
            ),
            next: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont.systemFont(ofSize: 15.5),
                color: UIColor.white,
                disable: UIColor.white,
                disableBackground: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1)
            ),
            losePinCode: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont.systemFont(ofSize: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            changeUser: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont.systemFont(ofSize: 15.5),
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
            )
        )
    }
}
