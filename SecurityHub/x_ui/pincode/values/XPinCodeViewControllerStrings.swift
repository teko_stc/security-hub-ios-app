//
//  XPinCodeViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XPinCodeVCStrings: XPinCodeControllerStringsProtocol {
    var resertPinAlert: XAlertViewStrings {
        XAlertViewStrings(
            /// Внимание
            title: "WARN_BIG".localized(),
            /// PIN-код не будет сохранен. Вы всегда можете установить его позднее через настройки приложения или при следующем входе в приложение.
            text: "SPSA_PIN_ENTER_DIALOG_MESS".localized(),
            /// Продолжить
            positive: "DOMAIN_USER_ENTER_NAME_CONT".localized(),
            /// Отмена
            negative: "DOMAIN_USER_ENTER_NAME_CANCEL".localized()
        )
    }
    
    /// Придумайте PIN-код
    var title_1 : String { "WIZ_PIN_ADD_NEW_PIN".localized() }
    
    /// Подтвердите PIN-код
    var title_2 : String { "PN_ENTER_CONFIRM_PN".localized() }
    
    /// Введите PIN-код
    var title_3 : String { "WIZ_REG_ENTER_PIN".localized() }
    
    /// Дaлее
    var next: String { "WIZ_GONEXT".localized() }
    
    /// Сменить пользователя
    var changeUser: String { "PNAA_CHANGE_USER".localized() }
    
    /// Забыли PIN-код?
    var losePinCode:String { "A_PN_A_FORGOT_PN".localized() }

    var abc = ["+", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "xyz"]
}
