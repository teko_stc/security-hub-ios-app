//
//  XPinCodeController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import LocalAuthentication

protocol XPinCodeControllerStringsProtocol {
    var resertPinAlert: XAlertViewStrings { get }
}

class XPinCodeController: XBaseViewController<XPinCodeView> {
    override var tabBarIsHidden: Bool { true }
    
    typealias State = XPinCodeViewState
        
    private let defaults = DataManager.defaultHelper
    public let state: State
    private let pin: String
    
    public lazy var strings: XPinCodeControllerStringsProtocol = XPinCodeVCStrings()
    
    init(state: State) {
        self.state = state
        self.pin = defaults.pin
        super.init(nibName: nil, bundle: nil)
    }
    init(pin: String, state: State) {
        self.pin = pin
        self.state = state
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        mainView.setState(state)
        
        checkBiometricAuth()
    }
    
    private func checkBiometricAuth() {
        let context = LAContext()
        if state == .check && DataManager.settingsHelper.touchId
            && context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                         error: nil) {
            mainView.touchView.isHidden = false
        }
    }
    
    func reset(alert: XAlertResetPinCodeController, password: String) {
        if (password.md5 != DataManager.defaultHelper.password) {
            //TODO: Toast
            alert.passwordNotCorrect()
        } else {
            alert.dismiss(animated: true)
            showCreateNewPinCodeController()
        }
    }
    
    func exit(alert: XAlertChangeUserController, isSaveConfig: Bool) {
        if isSaveConfig == false {
            UserDefaults.standard.removePersistentDomain(forName: DataManager.defaultHelper.login + "v.1")
        }
        
        doExit()
    }

    private func doExit() {
        DataManager.disconnectDisposable = DataManager.shared.disconnect()
            .subscribe(onNext: { _ in
                DataManager.disconnectDisposable?.dispose()
                self.showSpashController()
            })
    }
    
    func checkCorrectPinCode(_ pin: String) -> Bool {
        if self.pin != pin { mainView.pinError() }
        return self.pin == pin
    }
    
    func saveIfCorrectPinCode(_ pin: String) -> Bool {
        if self.pin == pin {
            defaults.pin = pin
            DataManager.settingsHelper.needPin = true
        } else { mainView.pinError() }
        return self.pin == pin
    }
}
