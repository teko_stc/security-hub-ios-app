//
//  XPinCodeController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import LocalAuthentication

extension XPinCodeController: XPinCodeViewDelegate {
    func backViewTapped() {
        if state == .recreate {
            let alert = XAlertController(style: baseAlertStyle(), strings: strings.resertPinAlert, positive: { self.back() })
            navigationController?.present(alert, animated: true)
        } else {
            self.back()
        }
    }
    
    func changeUserViewTapped() { navigationController?.present(XAlertChangeUserController(confirm: exit), animated: true) }
    
    func losePinCodeViewTapped() { navigationController?.present(XAlertResetPinCodeController(confirm: reset), animated: true) }
        
    func nextViewTapped(pin: String) { navigationController?.pushViewController(XPinCodeController(pin: pin, state: state == .create ? .confirm : .reconfirm), animated: true) }
    
    func showSpashController() { NavigationHelper.shared.show(XSplashController()) }

    func showMainController() { NavigationHelper.shared.showMain() }

    func showCreateNewPinCodeController() { NavigationHelper.shared.show(XPinCodeController(state: .create)) }
    
    func pinEndConfirm(pin: String) {
        if saveIfCorrectPinCode(pin) {
            if state == .confirm { showMainController() }
            else if let controller = navigationController?.viewControllers.first(where: { $0 is XSecuritySettingsController }) { navigationController?.popToViewController(controller, animated: true) }
        }
    }
    
    func pinEndCheck(pin: String) {
        if checkCorrectPinCode(pin) {
            showMainController()
        }
    }
    
    func touchTapped() {
        let context = LAContext()
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                               localizedReason: "Touch ID") { success, error in
            if success {
                DispatchQueue.main.async {
                    self.showMainController()
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
}

