//
//  XPinCodeViewButton.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AudioToolbox

class XPinCodeViewButton: UIButton {
    private let style: Style
    private let subTitleView = UILabel()
    
    override var isHighlighted: Bool {
        didSet {
            self.transform = self.isHighlighted ? CGAffineTransform(scaleX: 1.2, y: 1.2) : CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    public var text: String? {
        didSet {
            var attr: [NSAttributedString.Key : Any] = [:]
            if let foregroundColor = style.title?.color { attr[.foregroundColor] = foregroundColor}
            if let font = style.title?.font { attr[.font] = font }
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr), for: state)
        }
    }
    
    public var subTitle: String? {
        didSet {
            var attr: [NSAttributedString.Key : Any] = [:]
            if let foregroundColor = style.subTitle?.color { attr[.foregroundColor] = foregroundColor}
            if let font = style.subTitle?.font { attr[.font] = font }
            subTitleView.attributedText = NSAttributedString(string: subTitle ?? "", attributes: attr)
        }
    }
        
    init(style: Style) {
        self.style = style
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private func initViews() {
        if let pointSize = style.title?.font?.pointSize { titleEdgeInsets = UIEdgeInsets(top: -pointSize, left: 0, bottom: 0, right: 0) }
        if let topAnchor = style.icon?.topAnchor { imageEdgeInsets = UIEdgeInsets(top: topAnchor, left: 0, bottom: 0, right: 0) }
        
        subTitleView.textAlignment = .center
        addSubview(subTitleView)
        
        guard let icon = style.icon else { return }
        setImage(UIImage(named: icon.name)?.withRenderingMode(.alwaysTemplate), for: .normal)
        tintColor = icon.color
        
    }
    
    private func setConstraints() {
        subTitleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            subTitleView.topAnchor.constraint(equalTo: centerYAnchor),
            subTitleView.trailingAnchor.constraint(equalTo: trailingAnchor),
            subTitleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            subTitleView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        return true
    }
}

