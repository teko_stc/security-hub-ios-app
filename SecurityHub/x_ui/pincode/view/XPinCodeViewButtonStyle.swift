//
//  XPinCodeViewButtonStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XPinCodeViewButtonStyle {
    struct Lable {
        let color: UIColor
        let font: UIFont?
    }
    
    struct Icon {
        let name: String
        let color: UIColor
        var topAnchor: CGFloat = 0
    }
    
    var title: Lable? = nil
    var subTitle: Lable? = nil
    var icon: Icon? = nil
}
extension XPinCodeViewButton {
    typealias Style = XPinCodeViewButtonStyle
}
