//
//  XPinCodeViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XPinCodeViewStyle {
    struct Title {
        let color: UIColor
        let font: UIFont?
    }
    
    struct Pagination {
        let select, unselect, error: UIColor
    }
    
    let backgroundColor: UIColor
    let title: Title
    let pagination: Pagination
    let numpad, touch, back: XPinCodeViewButton.Style
    let next, losePinCode, changeUser: XZoomButton.Style
}
extension XPinCodeView {
    typealias Style = XPinCodeViewStyle
}
