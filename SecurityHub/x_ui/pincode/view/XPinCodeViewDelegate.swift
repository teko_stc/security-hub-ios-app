//
//  XPinCodeViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XPinCodeViewDelegate {
    func changeUserViewTapped()
    func losePinCodeViewTapped()
    func nextViewTapped(pin: String)
    func pinEndConfirm(pin: String)
    func pinEndCheck(pin: String)
    func backViewTapped() 
    func touchTapped()
}
