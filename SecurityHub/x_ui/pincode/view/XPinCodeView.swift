//
//  XPinCodeView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AudioToolbox

class XPinCodeView: UIView {
    public var delegate: XPinCodeViewDelegate?
    private var leftView: UIButton = UIButton(type: .infoLight)
    
    public var touchView: XPinCodeViewButton!

    private var titleView: UILabel = UILabel()
    private var paginationView: [UIView] = []
    private var paginationViewCenterXAnchors: [NSLayoutConstraint] = []
    private var buttonViews: [XPinCodeViewButton] = []
    private var nextView, changeUserView: XZoomButton!
    private let strings = XPinCodeVCStrings()
    private var pin: String = ""
    private var state: XPinCodeViewState?
    private var errorK: CGFloat? = nil
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    public func setState(_ state: XPinCodeViewState) {
        self.state = state
        switch state {
        case .create:
            titleView.text = strings.title_1
            nextView.text = strings.next
            nextView.isEnabled = false
            break;
        case .confirm:
            titleView.text = strings.title_2
            nextView.isHidden = true
            break;
        case .check:
            titleView.text = strings.title_3
            nextView.style = style().losePinCode
            nextView.text = strings.losePinCode
            break;
        case .recreate:
            titleView.text = strings.title_1
            nextView.text = strings.next
            nextView.isEnabled = false
            leftView.isHidden = false
            changeUserView.isHidden = true
            break;
        case .reconfirm:
            titleView.text = strings.title_2
            nextView.isHidden = true
            leftView.isHidden = false
            changeUserView.isHidden = true
            break;
        }
    }
    
    public func pinError() {
        errorK = -20
        pinErrorAnimation()
    }
    
    private func initViews() {
        backgroundColor = style().backgroundColor
        
        leftView.setImage(UIImage(named: "ic_stair"), for: .normal)
        leftView.contentMode = .scaleAspectFill
        leftView.isHidden = true
        leftView.tintColor = style().title.color
        leftView.addTarget(self, action: #selector(leftViewTapped), for: .touchUpInside)
        addSubview(leftView)
        
        titleView.textColor = style().title.color
        titleView.font = style().title.font
        addSubview(titleView)

        for _ in 0...3 {
            let _view = UIView()
            _view.backgroundColor = style().pagination.unselect
            _view.layer.cornerRadius = 5
            addSubview(_view)
            paginationView.append(_view)
        }

        for i in 0...9 {
            let _view = XPinCodeViewButton(style: style().numpad)
            _view.text = String(i)
            _view.subTitle = strings.abc[i]
            _view.addTarget(self, action: #selector(numpadTapped), for: .touchUpInside)
            addSubview(_view)
            buttonViews.append(_view)
        }
        
        touchView = XPinCodeViewButton(style: style().touch)
        touchView.addTarget(self, action: #selector(touchTapped), for: .touchUpInside)
        touchView.isHidden = true
        addSubview(touchView)
        buttonViews.append(touchView)
        
        let backView = XPinCodeViewButton(style: style().back)
        backView.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        addSubview(backView)
        buttonViews.append(backView)
        
        nextView = XZoomButton(style: style().next)
        nextView.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
        addSubview(nextView)
        
        changeUserView = XZoomButton(style: style().changeUser)
        changeUserView.text = strings.changeUser
        changeUserView.addTarget(self, action: #selector(changeUserTapped), for: .touchUpInside)
        addSubview(changeUserView)
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        titleView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor).isActive = true
        
        leftView.translatesAutoresizingMaskIntoConstraints = false
        leftView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        leftView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.marginLeft).isActive = true
        leftView.heightAnchor.constraint(equalToConstant: XNavigationView.iconSize).isActive = true
        leftView.widthAnchor.constraint(equalToConstant: XNavigationView.iconSize).isActive = true
        
        for i in 0...(paginationView.count - 1) {
            let k = CGFloat(Float(i) - Float(paginationView.count) / 2 + 0.5)
            paginationView[i].translatesAutoresizingMaskIntoConstraints = false
            paginationView[i].topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 40).isActive = true
            paginationViewCenterXAnchors.append(paginationView[i].centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor, constant: k * 40))
            paginationViewCenterXAnchors[i].isActive = true
            paginationView[i].heightAnchor.constraint(equalToConstant: 10).isActive = true
            paginationView[i].widthAnchor.constraint(equalToConstant: 10).isActive = true
        }
        
        for i in 0...(buttonViews.count - 1) {
            let k = i == 0 ? 11 : i == 11 ? 12 : i
            let x_k = CGFloat((k - 1) % 3 - 1)
            let x_y = CGFloat(Float(Int((k - 1) / 3)) - Float(buttonViews.count) / 3 / 2 + 0.5)
            buttonViews[i].translatesAutoresizingMaskIntoConstraints = false
            buttonViews[i].centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor, constant: x_k * 95).isActive = true
            buttonViews[i].centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor, constant: x_y * 95).isActive = true
            buttonViews[i].heightAnchor.constraint(equalToConstant: 70).isActive = true
            buttonViews[i].widthAnchor.constraint(equalToConstant: 70).isActive = true
        }
        
        changeUserView.translatesAutoresizingMaskIntoConstraints = false
        changeUserView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        changeUserView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        changeUserView.leadingAnchor.constraint(equalTo: buttonViews[1].leadingAnchor).isActive = true
        changeUserView.trailingAnchor.constraint(equalTo: buttonViews[3].trailingAnchor).isActive = true

        nextView.translatesAutoresizingMaskIntoConstraints = false
        nextView.bottomAnchor.constraint(equalTo: changeUserView.topAnchor, constant: -6).isActive = true
        nextView.heightAnchor.constraint(equalTo: changeUserView.heightAnchor).isActive = true
        nextView.leadingAnchor.constraint(equalTo: changeUserView.leadingAnchor).isActive = true
        nextView.trailingAnchor.constraint(equalTo: changeUserView.trailingAnchor).isActive = true
    }
    
    private func updatePaginationState(count: Int) {
        UIView.animate(withDuration: 0.2, animations: {
            if count > 0 { for i in 0...count-1 { self.paginationView[i].backgroundColor = self.style().pagination.select } }
            if (count >= self.paginationView.count) { return }
            for i in count...self.paginationView.count-1 { self.paginationView[i].backgroundColor = self.style().pagination.unselect }
        })
    }
    
    @objc private func leftViewTapped() { delegate?.backViewTapped() }
    
    @objc private func numpadTapped(view: XPinCodeViewButton) {
        if (pin.count < 4) {
            pin += view.text ?? ""
            updatePaginationState(count: pin.count)
        } else if nextView.isEnabled && (state == .create || state == .recreate) {
            nextView.accent()
        }
        if (state == .create || state == .recreate) { nextView.isEnabled = pin.count >= 4 }
        if ((state == .confirm || state == .reconfirm) && pin.count == 4) { delegate?.pinEndConfirm(pin: pin) }
        if (state == .check && pin.count == 4) { delegate?.pinEndCheck(pin: pin) }
    }

    @objc private func backTapped() {
        if (pin.count == 0) { return }
        pin = String(pin.prefix(pin.count - 1))
        updatePaginationState(count: pin.count)
        if (state == .create) { nextView.isEnabled = pin.count >= 4 }
    }
    
    @objc private func changeUserTapped() { delegate?.changeUserViewTapped() }
    
    @objc private func nextTapped() {
        guard let state = state else { return }
        switch state {
        case .create:
            delegate?.nextViewTapped(pin: pin)
            break;
        case .check:
            delegate?.losePinCodeViewTapped()
            break;
        case .confirm:
            break;
        case .recreate:
            delegate?.nextViewTapped(pin: pin)
            break;
        case .reconfirm:
            break;
        }
    }
    
    @objc private func touchTapped() {
        delegate?.touchTapped()
    }
    
    private func pinErrorAnimation() {
        guard let errorK = errorK else { return }
        pin = ""
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        for i in 0...(paginationView.count - 1) {
            let k = CGFloat(Float(i) - Float(paginationView.count) / 2 + 0.5)
            paginationViewCenterXAnchors[i].constant = k * 40 + errorK * 2
        }
        UIView.animate(
            withDuration: 0.1,
            animations: {
                self.paginationView.forEach { view in view.backgroundColor = self.style().pagination.error }
                self.layoutIfNeeded()
            },
            completion: { _ in
                if errorK == -20 { self.errorK = 20 }
                else if errorK == 20 { self.errorK = -10 }
                else if errorK == -10 { self.errorK = 10 }
                else if errorK == 10 { self.errorK = 0 }
                else if errorK == 0 {
                    self.errorK = nil;
                    self.updatePaginationState(count: 0);
                    if (self.state == .create) { self.nextView.isEnabled = false }
                }
                self.pinErrorAnimation()
            })
    }
}
