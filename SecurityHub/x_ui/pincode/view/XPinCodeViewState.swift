//
//  XPinCodeViewState.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

enum XPinCodeViewState {
    case create
    case confirm
    case check
    case recreate
    case reconfirm
}
