//
//  XNotReadyZonesView.swift
//  SecurityHub
//
//  Created by Stefan on 02.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

protocol XNotReadyZonesViewDelegate {
		func closeViewTapped()
}

class XNotReadyZonesView: UIView {
		public var delegate: XNotReadyZonesViewDelegate?
		lazy var tableView: UITableView = {
				let view = UITableView()
				view.rowHeight = 75 //UITableView.automaticDimension
				view.separatorStyle = .none
				view.backgroundColor = style.backrgound
				view.estimatedRowHeight = 75
				return view
		}()
		
		private var titleView: UILabel!
		private var closeButtonView: XZoomButton!
		private lazy var style: Style = styles()
	
		private var containerViewCenterYAnchor: NSLayoutConstraint?
		private var widthAnchorMultiplier: CGFloat { 1 }
		private var canDismiss: Bool = true

		private let containerView: UIView = {
				let view = UIView()
				view.layer.cornerRadius = 40
				view.layer.shadowColor = UIColor.black.cgColor
				view.layer.shadowRadius = 100
				view.layer.shadowOpacity = 0.3
				return view
		}()

		private let visualEffectView: UIVisualEffectView = {
				let blurEffect = UIBlurEffect(style: .dark)
				let view = UIVisualEffectView(effect: blurEffect)
				view.alpha = 0.7
				return view
		}()
	
		private let mainView: UIView = {
				let view = UIView()
				return view
		}()
	
		public var verticalAlignment: VerticalAlignment {
				didSet {
						guard let containerViewCenterYAnchor = containerViewCenterYAnchor else { return }
						var k: CGFloat = 0
						switch verticalAlignment {
						case .center:
								k = 0
								break
						case .top:
								k = -((frame.height / 2) - (frame.height / 3))
								break
						}
						containerViewCenterYAnchor.constant = k
						layoutIfNeeded()
				}
		}
		
		init() {
				self.verticalAlignment = .center
				super.init(frame: .zero)
				initViews()
				setConstraints()
				
				visualEffectView.isUserInteractionEnabled = true
				visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
				visualEffectView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longPressed)))
				visualEffectView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(longPressed)))
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		private func initViews() {
				mainView.backgroundColor = style.backrgound
				containerView.backgroundColor = style.backrgound
			
				addSubview(visualEffectView)
				addSubview(containerView)
				containerView.addSubview(mainView)
				
				titleView = UILabel.create(style.title)
				titleView.numberOfLines = 0
				titleView.text = "MESS_NOT_READY".localized()
				mainView.addSubview(titleView)
			
				mainView.addSubview(tableView)
				
				closeButtonView = XZoomButton(style: style.close)
				closeButtonView.text = "N_BUTTON_CLOSE".localized()
				closeButtonView.addTarget(self, action: #selector(closeViewTapped), for: .touchUpInside)
				mainView.addSubview(closeButtonView)
		}
	
		private func setConstraints() {
				[titleView, tableView, closeButtonView, visualEffectView, containerView, mainView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
				containerViewCenterYAnchor = containerView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor)
				NSLayoutConstraint.activate([
						visualEffectView.topAnchor.constraint(equalTo: topAnchor, constant: -1000),
						visualEffectView.leadingAnchor.constraint(equalTo: leadingAnchor),
						visualEffectView.trailingAnchor.constraint(equalTo: trailingAnchor),
						visualEffectView.bottomAnchor.constraint(equalTo: bottomAnchor),
					
						containerView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
						containerView.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor, constant: -160),
						containerView.topAnchor.constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.topAnchor),
						containerViewCenterYAnchor!,
						containerView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: widthAnchorMultiplier, constant: -60),

						mainView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
						mainView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
						mainView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
						mainView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15),
					
						titleView.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 10),
						titleView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 5),
						titleView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -5),
						
						tableView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20),
						tableView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 5),
						tableView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -5),
						tableView.bottomAnchor.constraint(equalTo: closeButtonView.topAnchor, constant: -20),
						
						closeButtonView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -10),
						closeButtonView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 20),
						closeButtonView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -20),
						closeButtonView.heightAnchor.constraint(equalToConstant: 25),
				])
		}
		
		@objc private func closeViewTapped() {
				delegate?.closeViewTapped()
		}
	
		@objc private func tapGesture(_ sender: UITapGestureRecognizer) {
				if canDismiss {
					delegate?.closeViewTapped()
				}
		}
		
		@objc private func longPressed(_ sender: UIGestureRecognizer) {
				if (sender.state == .ended) {
						if canDismiss {
							delegate?.closeViewTapped()
						}
				}
		}
	
		enum VerticalAlignment {
				case top
				case center
		}
}

struct XNotReadyZonesViewStyle {
		let backrgound: UIColor
		let title: XBaseLableStyle
		let close: XZoomButton.Style
}

extension XNotReadyZonesView {
		typealias Style = XNotReadyZonesViewStyle
	
		func styles() -> Style {
				Style(
						backrgound: UIColor.white,
						title: XBaseLableStyle(
								color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
								font: UIFont(name: "Open Sans", size: 18),
								alignment: .center
						),
						close: XZoomButton.Style(
							backgroundColor: UIColor.white,
								font: UIFont(name: "Open Sans", size: 18),
								color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
						)
				)
		}
}
