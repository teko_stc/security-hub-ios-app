//
//  XNotReadyZoneCell.swift
//  SecurityHub
//
//  Created by Stefan on 02.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XNotReadyZoneCell: UITableViewCell, XSwitchDelegate {
		static var identifier = "XNotReadyZoneCell.identifier"
	
		private let iconView:       UIImageView = UIImageView()
		private let titleView:      UILabel = UILabel()
		private let subTitleView:   UILabel = UILabel()
		public let switchView: 		XSwitch = XSwitch(style: nil, isOn: false)
	
		private var action:         ((_ cell: XNotReadyZoneCell) ->())?

		override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
				super.init(style: style, reuseIdentifier: reuseIdentifier)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		public func setContent(_ nNotReadyZone: NNotReadyZone, action: ((_ cell: XNotReadyZoneCell) -> ())?) {
				selectionStyle = .none
				self.action = action
				layer.cornerRadius = 8
				let zi = nNotReadyZone.zi
				titleView.text = zi.zone.name
			
				let icon = ((zi.zone.uidType == 0 || zi.zone.uidType == 100) && zi.zone.section == 0) ?
						DataManager.shared.d3Const.getDeviceIcons(type: HubConst.getDeviceType(zi.configVersion, cluster: zi.cluster).int) :
					DataManager.shared.d3Const.getSensorIcons(uid: zi.zone.uidType.int, detector: zi.zone.detector.int)
				iconView.image = UIImage(named: icon.list)
			
			if zi.sectionDetector == HubConst.SECTION_TYPE_SECURITY &&
					HubConst.isDeviceSupportBypass (zi.configVersion, cluster: zi.cluster)
					{
						subTitleView.isHidden = false
						switchView.isHidden = false
						switchView.isOn = zi.zone.bypass > 0
					}
		}
		
		private func initViews() {
				iconView.contentMode = .scaleAspectFit
				contentView.addSubview(iconView)
			
				titleView.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
				titleView.numberOfLines = 0
				titleView.lineBreakMode = .byWordWrapping
				titleView.font = UIFont(name: "Open Sans", size: 15)
				contentView.addSubview(titleView)
			
				subTitleView.text = "N_BYPASS_SWITCH_ZONE".localized()
				subTitleView.textColor = UIColor.lightGray
				subTitleView.font = UIFont(name: "Open Sans", size: 12)
				subTitleView.textAlignment = .right
				subTitleView.isHidden = true
				contentView.addSubview(subTitleView)
			
				switchView.isHidden = true
				switchView.delegate = self
				contentView.addSubview(switchView)
		}
		
		private func setConstraints() {
				[iconView, titleView, subTitleView, switchView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
				NSLayoutConstraint.activate([
						iconView.widthAnchor.constraint(equalToConstant: 36),
						iconView.heightAnchor.constraint(equalToConstant: 36),
						iconView.topAnchor.constraint(equalTo: contentView.topAnchor),
						iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

						titleView.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
						titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
						titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5),
						
						subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15),
						subTitleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
						subTitleView.trailingAnchor.constraint(equalTo: switchView.leadingAnchor,constant: -5),
						
						switchView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15),
						switchView.widthAnchor.constraint(equalToConstant: 30),
						switchView.heightAnchor.constraint(equalToConstant: 18),
						switchView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5),
				])
		}
	
		func switchStateChange(switch: XSwitch, isOn: Bool) {
			if let action = action { action (self) }
		}
}
