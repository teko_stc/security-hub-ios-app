//
//  XNotReadyZonesController.swift
//  SecurityHub
//
//  Created by Stefan on 02.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XNotReadyZonesController: XBaseViewController<XNotReadyZonesView>, UITableViewDataSource, UITableViewDelegate, XNotReadyZonesViewDelegate {
		private var zones: [NNotReadyZone]	
		
		init(_ nNotReadyZone: NNotReadyZone) {
				self.zones = [nNotReadyZone]
				super.init(nibName: nil, bundle: nil)
				modalPresentationStyle = .overFullScreen
		}
	
		required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		override func viewDidLoad() {
				super.viewDidLoad()
			
				mainView.tableView.register(XNotReadyZoneCell.self, forCellReuseIdentifier: XNotReadyZoneCell.identifier)
				mainView.tableView.dataSource = self
				mainView.tableView.delegate = self
				mainView.tableView.reloadData()
				mainView.delegate = self
		}
		
		private var o: Any?
		override func viewWillAppear(_ animated: Bool) {
				super.viewWillAppear(animated)
				 o = NotificationCenter.default.addObserver(forName: HubNotification.notReadyZone, object: nil, queue: OperationQueue.main){ notification in
						guard let nNotReadyZone = notification.object as? NNotReadyZone else { return }
						if let pos = self.zones.firstIndex(where: { (o) -> Bool in return o.zi.zone.id == nNotReadyZone.zi.zone.id }) {
								self.zones[pos] = nNotReadyZone
								self.mainView.tableView.reloadRows(at: [IndexPath(row: pos, section: 0)], with: .fade)
						} else {
								self.zones.append(nNotReadyZone)
								self.mainView.tableView.insertRows(at: [IndexPath(row: self.zones.count-1, section: 0)], with: .right)
						}
				}
		}
		
		override func viewWillDisappear(_ animated: Bool) {
				super.viewWillDisappear(animated)
				if o != nil {
						NotificationCenter.default.removeObserver(o!)
						o = nil
				}
		}
	
		func closeViewTapped()
		{
			presentingViewController?.dismiss(animated: true)
		}
		
		func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return zones.count }
		
		func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
			
			let cell = tableView.dequeueReusableCell(withIdentifier: XNotReadyZoneCell.identifier, for: indexPath) as! XNotReadyZoneCell
			
				cell.setContent(zones[indexPath.row], action: bypassChangeViewTapped)
			
				return cell
		}
	
		func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		}
	
		func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
			return true
		}
			
		func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
			let cell = tableView .cellForRow(at: indexPath) as! XNotReadyZoneCell
			cell.backgroundColor = UIColor.colorFromHex(0xf8f8f8)
		}
	
		func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
			let cell = tableView .cellForRow(at: indexPath) as! XNotReadyZoneCell
			cell.backgroundColor = UIColor.clear
			if cell.switchView.isHidden == false {
				cell.switchView.isOn = !cell.switchView.isOn
				bypassChangeViewTapped (cell: cell) }
		}
	
		func bypassChangeViewTapped(cell: XNotReadyZoneCell) {
			let indexPath = mainView.tableView.indexPath(for: cell)
			let de = zones[indexPath!.row].zi

			guard DataManager.shared.dbHelper.isDeviceConnected(device_id: de.zone.device) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
			guard DataManager.shared.dbHelper.getArmStatus(device_id: de.zone.device) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
			
			let value = cell.switchView.isOn ? 1 : 0
			
			_ = doOnAvailable(deviceId: de.zone.device)
					.observeOn(ThreadUtil.shared.mainScheduler)
					.do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
						.flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : de.zone.device, "command" : ["ZONE_SET": [ "section": de.zone.section, "index": de.zone.zone, "bypass": value ]]]) })
					.subscribe(onSuccess: { [weak self] result in
							if (!result.success) { self?.showErrorColtroller(message: result.message) }
					}, onError: { [weak self] error in
							self?.showErrorColtroller(message: error.localizedDescription)
					})
		}
}
