//
//  XDevicePartitionMenuViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 26.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XDevicePartitionMenuControllerStrings {
    /// Устройства
    var devices: String { "N_PAGER_DEVICE".localized() }
    
    /// Разделы
    var sections: String { "N_PAGER_SECTION".localized() }
}
