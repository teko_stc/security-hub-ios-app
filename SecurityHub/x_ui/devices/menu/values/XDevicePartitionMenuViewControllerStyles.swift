//
//  XDevicePartitionMenuViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDevicePartitionMenuView {
    func styles() -> Style {
        Style(
            background: UIColor.white,
            line: UIColor(red: 0x41/255, green: 0x41/255, blue: 0x41/255, alpha: 0.2),
            buttonFont: UIFont(name: "Open Sans", size: 20),
            buttonSelectText: UIColor.white,
            buttonUnselectText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
            buttonSelectBackground: UIColor(red: 0x3a/255, green: 0xce/255, blue: 0xff/255, alpha: 1),
            buttonUnselectBackground: UIColor.white
        )
    }
}
