//
//  XDevicePartitionMenuController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDevicePartitionMenuController: XBaseViewController<XDevicePartitionMenuView> {
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.showAddMenuController),
        rightViews: [
            XNavigationViewRightViewBuilder(imageName: "search", color: UIColor.colorFromHex(0x414042), viewTapped: self.showSearchView)
        ]
    )
    
    private let titleView = UILabel()
    private var sites = [Sites]()
    private var selectedSite: Sites? {
        didSet {
            userDefaults().setValue(selectedSite?.id, forKey: "selectedSiteIdKey")
        }
    }
    
    private lazy var lView = LView()
    private lazy var searchView = UIView()
    private lazy var searchViewTextView = UITextField()
    
    private lazy var pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    private lazy var viewLayer: XDevicePartitionMenuViewLayerProtocol = self.mainView
    
    private lazy var strings: XDevicePartitionMenuControllerStrings = XDevicePartitionMenuControllerStrings()
    
    private var viewControllers: [UIViewController] = [
        XDevicesListViewController(),
        XSectionsListViewController()
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)

        pageViewController.dataSource = self
        pageViewController.delegate = self
        pageViewController.setViewControllers([viewControllers.first!], direction: .forward, animated: false)
        embed(pageViewController, inParent: self, inView: viewLayer.containerView)
        viewLayer.initPageHeaderViews(titles: [strings.devices, strings.sections])
        
        mainView.delegate = self
        
        setupHeaderSpinnerView()

        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.loadSites), name: HubNotification.siteUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.siteSelected_DashboardViewController),
                                               name: Notification.Name("siteSelected_DashboardViewController"), object: nil)
        loadSites()
        
        setupSearchView()

        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.keyboardWillShowNotification), name: UIWindow.keyboardWillShowNotification, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.keyboardWillHideNotification), name: UIWindow.keyboardWillHideNotification, object: nil)

        view.addSubview(lView)
        lView.isHidden = true
        lView.frame = view.frame
    }
    
    private func embed(_ viewController: UIViewController, inParent controller:UIViewController, inView view:UIView){
       viewController.willMove(toParent: controller)
       viewController.view.frame = view.bounds
       controller.addChild(viewController)
       view.addSubview(viewController.view)
       viewController.didMove(toParent: controller)
    }
    
    private func showAddMenuController() {
        let controller = XAddMenuController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func setupHeaderSpinnerView() {
        let localView = UIView()
        let imageView = UIImageView(image: UIImage(named: "ic_arrow_down"))
        titleView.setStyle(XBaseLableStyle(color: .black, font: UIFont(name: "Open Sans", size: 25), alignment: .center))
        titleView.numberOfLines = 1
        localView.addSubview(titleView)
        localView.addSubview(imageView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(sitesPressed))
        localView.addGestureRecognizer(tap)

        view.addSubview(localView)
        [localView, titleView,  imageView].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(greaterThanOrEqualTo: localView.topAnchor),
            titleView.centerYAnchor.constraint(equalTo: localView.centerYAnchor),
            titleView.bottomAnchor.constraint(lessThanOrEqualTo: localView.bottomAnchor),
            titleView.leadingAnchor.constraint(greaterThanOrEqualTo: localView.leadingAnchor),
            titleView.centerXAnchor.constraint(equalTo: localView.centerXAnchor),
            titleView.trailingAnchor.constraint(lessThanOrEqualTo: localView.trailingAnchor),
            
            imageView.heightAnchor.constraint(equalToConstant: 6.0),
            imageView.widthAnchor.constraint(equalToConstant: 9.0),
            imageView.centerYAnchor.constraint(equalTo: localView.centerYAnchor),
            imageView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 8.0),
            imageView.trailingAnchor.constraint(lessThanOrEqualTo: localView.trailingAnchor),
            
            localView.heightAnchor.constraint(equalToConstant: 36),
            localView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 48.0),
            localView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -48.0),
            localView.bottomAnchor.constraint(greaterThanOrEqualTo: mainView.topAnchor),
        ])
    }
    
    private func setupSearchView() {
        searchView.isHidden = true
        searchView.backgroundColor = .white
        searchView.layer.borderWidth = 1.0
        searchView.layer.borderColor = mainView.styles().buttonSelectBackground.cgColor
        searchView.layer.cornerRadius = 10.0
        view.addSubview(searchView)
        
        let closeButton = UIButton(type: .infoLight)
        closeButton.setImage(UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = mainView.styles().buttonUnselectText
        closeButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        searchView.addSubview(closeButton)

        searchViewTextView.font = UIFont(name: "Open Sans", size: 18.0)
        searchViewTextView.delegate = self
        searchView.addSubview(searchViewTextView)
        
        [searchView, searchViewTextView, closeButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            searchView.heightAnchor.constraint(equalToConstant: 52.0),
            searchView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 48.0 + 16.0),
            searchView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0),
            searchView.bottomAnchor.constraint(greaterThanOrEqualTo: mainView.topAnchor, constant: 8.0),
            
            closeButton.widthAnchor.constraint(equalToConstant: 34.0),
            closeButton.heightAnchor.constraint(equalToConstant: 34.0),
            closeButton.trailingAnchor.constraint(equalTo: searchView.trailingAnchor, constant: -8.0),
            closeButton.centerYAnchor.constraint(equalTo: searchView.centerYAnchor),
            
            searchViewTextView.topAnchor.constraint(greaterThanOrEqualTo: searchView.topAnchor),
            searchViewTextView.centerYAnchor.constraint(equalTo: searchView.centerYAnchor),
            searchViewTextView.leadingAnchor.constraint(equalTo: searchView.leadingAnchor, constant: 16.0),
            searchViewTextView.trailingAnchor.constraint(equalTo: closeButton.leadingAnchor, constant: 4.0),
            searchViewTextView.bottomAnchor.constraint(lessThanOrEqualTo: searchView.bottomAnchor),
        ])
    }
    
    @objc private func sitesPressed(sender: UIGestureRecognizer) {
        guard let selectedIndex = sites.firstIndex(where: { $0.id == selectedSite?.id }) else { return }
        let sitesVC = SitesViewController(sites: sites, selectedIndex: selectedIndex)
        sitesVC.delegate = self
        sitesVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(sitesVC, animated: true, completion: nil)
    }
    
    @objc private func siteSelected_DashboardViewController(n: Notification) {
        guard let site = n.object as? Sites else {
            return
        }

        self.selectedSite = site
        self.updateSiteLabel()
    }
    
    @objc private func loadSites() {
        DataManager.shared.getDBSites { [weak self] sites in
            guard let self = self else {
                return
            }
            
            self.sites = sites
            let selectedSiteId = userDefaults().integer(forKey: "selectedSiteIdKey")
            if let selected = self.sites.first(where: { $0.id == selectedSiteId }) {
                self.selectedSite = selected
                userDefaults().setValue(selected.id, forKey: "DashboardPrioritySiteId")
                DataManager.shared.nCenter.post(name: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil)
            } else {
                self.selectedSite = self.sites.sorted { $0.id < $1.id }.first
            }
            self.updateSiteLabel()
        }
    }

    private func updateSiteLabel() {
        titleView.superview?.isHidden = sites.count == 0
        titleView.text = selectedSite?.name
    }
    
    @objc private func showSearchView() {
        searchViewTextView.text = nil
        searchView.isHidden = !searchView.isHidden
        if !searchView.isHidden {
            searchViewTextView.becomeFirstResponder()
        } else {
            view.endEditing(true)
            textFieldDidChangeSelection(searchViewTextView)
        }
    }
    
    @objc func keyboardWillShowNotification(_ notification: NSNotification) {
        lView.isHidden = false
        searchView.layer.borderColor = mainView.styles().buttonSelectBackground.cgColor
    }

    @objc func keyboardWillHideNotification(_ notification: NSNotification) {
        lView.isHidden = true
        searchView.layer.borderColor = mainView.styles().buttonUnselectText.cgColor
    }
}

extension XDevicePartitionMenuController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = viewControllers.firstIndex(of: viewController), index > 0 else { return nil }
        return viewControllers[index - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = viewControllers.firstIndex(of: viewController), index < viewControllers.count - 1 else { return nil }
        return viewControllers[index + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingViewControllers.forEach { vc in if let index = viewControllers.firstIndex(of: vc) { viewLayer.pageHeaderItemViewSelect(index: index) } }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed { return }
        previousViewControllers.forEach { vc in if let index = viewControllers.firstIndex(of: vc) { viewLayer.pageHeaderItemViewSelect(index: index) } }
    }
}

extension XDevicePartitionMenuController: XDevicePartitionMenuViewDelegate {
    func onPageHeaderItemViewTapped(index: Int, direction: UIPageViewController.NavigationDirection) {
        pageViewController.setViewControllers([viewControllers[index]], direction: direction, animated: true)
    }
}

extension XDevicePartitionMenuController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        DataManager.shared.nCenter.post(name: NSNotification.Name("XDevicePartitionMenuController_SearchViewChange"), object: textField.text)
        userDefaults().set(textField.text, forKey: "XDevicePartitionMenuController_SearchViewChange")
    }
}

extension XDevicePartitionMenuController: SitesViewControllerDelegate {
    func siteSelected(site: Sites) {
        userDefaults().setValue(site.id, forKey: "DashboardPrioritySiteId")
        DataManager.shared.nCenter.post(name: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil)
        DataManager.shared.nCenter.post(name: NSNotification.Name("siteSelected_XDevicePartitionMenuController"), object: site)
        self.selectedSite = site
        self.updateSiteLabel()
    }
}

private class LView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        superview?.endEditing(true)
        return superview?.point(inside: point, with: event) ?? super.point(inside: point, with: event)
    }
}
