//
//  XDevicePartitionMenuView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XDevicePartitionMenuViewLayerProtocol {
    var containerView: UIView { get }
    func initPageHeaderViews(titles: [String])
    func pageHeaderItemViewSelect(index: Int)
}

class XDevicePartitionMenuView: UIView {
    public var delegate: XDevicePartitionMenuViewDelegate?
    
    private var pageView: UIView = UIView()
    private var pageLineHeaderView: UIView = UIView()
    private var pageHeaderView: UIScrollView = UIScrollView()
    private var pageHeaderStackView: UIStackView  = UIStackView()
    private var pageHeaderButtonViews: [UIButton] = []
    private var pageHeaderSelectedIndex: Int = -1
    
    private lazy var style: Style = styles()
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.background
        addSubview(pageView)
        
        let _layer = CALayer()
        _layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0.5)
        _layer.backgroundColor = style.line.cgColor
        pageLineHeaderView.layer.addSublayer(_layer)
        addSubview(pageLineHeaderView)
        
        pageHeaderView.backgroundColor = style.background
        addSubview(pageHeaderView)
        
        pageHeaderStackView.axis = .horizontal
        pageHeaderStackView.distribution = .equalCentering
        pageHeaderStackView.alignment = .center
        pageHeaderStackView.spacing = 0.0
        pageHeaderView.addSubview(pageHeaderStackView)
    }
    
    private func setConstraints() {
        [pageView, pageLineHeaderView, pageHeaderView, pageHeaderStackView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            pageHeaderView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
            pageHeaderView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            pageHeaderView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            pageHeaderView.heightAnchor.constraint(equalToConstant: 40),
            
            pageHeaderStackView.topAnchor.constraint(equalTo: pageHeaderView.topAnchor),
            pageHeaderStackView.leadingAnchor.constraint(equalTo: pageHeaderView.leadingAnchor),
            
            pageLineHeaderView.topAnchor.constraint(equalTo: pageHeaderView.bottomAnchor),
            pageLineHeaderView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            pageLineHeaderView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            pageLineHeaderView.heightAnchor.constraint(equalToConstant: 1),
            
            pageView.topAnchor.constraint(equalTo: pageLineHeaderView.bottomAnchor),
            pageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            pageView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            pageView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func createPageHeaderButton(title: String, width: CGFloat, height: CGFloat) -> UIButton {
        let btn = UIButton()
        var att1: [NSAttributedString.Key : Any] = [.foregroundColor : style.buttonUnselectText]
        var att2: [NSAttributedString.Key : Any] = [.foregroundColor : style.buttonSelectText]
        if let font = style.buttonFont { att1[.font] = font; att2[.font] = font; }
        btn.setAttributedTitle(NSAttributedString(string: title, attributes: att1), for: .normal)
        btn.setAttributedTitle(NSAttributedString(string: title, attributes: att2), for: .selected)
        btn.setAttributedTitle(NSAttributedString(string: title, attributes: att2), for: .highlighted)
        btn.widthAnchor.constraint(equalToConstant: width).isActive = true
        btn.heightAnchor.constraint(equalToConstant: height).isActive = true
        btn.addTarget(self, action: #selector(btnViewTapped), for: .touchUpInside)

        let layer = CAShapeLayer()
        layer.fillColor = style.buttonUnselectBackground.cgColor
        layer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: width, height: height), byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 20.0, height: 0.0)).cgPath
        btn.layer.addSublayer(layer)
        return btn
    }
    
    @objc private func btnViewTapped(btn: UIButton) {
        guard let index = pageHeaderButtonViews.firstIndex(of: btn), index != pageHeaderSelectedIndex else { return }
        delegate?.onPageHeaderItemViewTapped(index: index, direction: index > pageHeaderSelectedIndex ? .forward : .reverse)
        pageHeaderItemViewSelect(index: index)
    }
}

extension XDevicePartitionMenuView: XDevicePartitionMenuViewLayerProtocol {
    var containerView: UIView { return pageView }
    
    func initPageHeaderViews(titles: [String]) {
        let k = titles.count > 4 ? 3.2 : CGFloat(titles.count)
        titles.forEach { title in
            pageHeaderButtonViews.append(createPageHeaderButton(title: title, width: UIScreen.main.bounds.width/k, height: 40))
            pageHeaderStackView.addArrangedSubview(pageHeaderButtonViews.last!)
        }
        pageHeaderView.contentSize = CGSize(width: UIScreen.main.bounds.width/k * CGFloat(titles.count), height: 40)
        pageHeaderItemViewSelect(index: 0)
    }
    
    func pageHeaderItemViewSelect(index: Int) {
        if pageHeaderSelectedIndex != -1 {
            pageHeaderButtonViews[pageHeaderSelectedIndex].isSelected = false
            if let layer = pageHeaderButtonViews[pageHeaderSelectedIndex].layer.sublayers?.first(where: { $0 is CAShapeLayer }) as? CAShapeLayer {
                layer.fillColor = style.buttonUnselectBackground.cgColor
            }
        }
        pageHeaderSelectedIndex = index
        pageHeaderButtonViews[index].isSelected = true
        if let layer = pageHeaderButtonViews[index].layer.sublayers?.first(where: { $0 is CAShapeLayer }) as? CAShapeLayer {
            layer.fillColor = style.buttonSelectBackground.cgColor
        }
    }
}
