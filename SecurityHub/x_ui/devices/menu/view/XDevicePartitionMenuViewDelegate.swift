//
//  XDevicePartitionMenuViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XDevicePartitionMenuViewDelegate {
    func onPageHeaderItemViewTapped(index: Int, direction: UIPageViewController.NavigationDirection)
}
