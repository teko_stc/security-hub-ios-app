//
//  XDevicePartitionMenuViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicePartitionMenuViewStyle {
    let background: UIColor
    let line: UIColor
    let buttonFont: UIFont?
    let buttonSelectText: UIColor
    let buttonUnselectText: UIColor
    let buttonSelectBackground: UIColor
    let buttonUnselectBackground: UIColor
}
extension XDevicePartitionMenuView {
    typealias Style = XDevicePartitionMenuViewStyle
}
