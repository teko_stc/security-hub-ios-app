//
//  XDeviceTopViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XDeviceTopViewLayer {
    var ACTION_STATE_ARMED: Int { get }
    var ACTION_STATE_DISARMED: Int { get }
    var ACTION_STATE_PARTED: Int { get }

    func setTitle(title: String)
    func setIsFavorite(isFavorite: Bool)
    func setIcon(name: String, isRenderTemplate: Bool)
    func setDeviceState(actionState: Int)
    func setDeviceState(isHaveConnection: Bool)
    func setDeviceState(isUpdateHidden: Bool)
    func setDeviceState(isDelayHidden: Bool)
    func setDeviceState(isHaveSocket: Bool)
    func setDeviceState(isHaveLan: Bool)
    func setDeviceState(level: Int?, isHaveBattery: Bool, isBatteryError: Bool)
    func setDeviceState(isDualSim: Bool, isHaveSim: Bool, isSimNoSignal: Bool, level: Int?)
    func setDeviceState(isHidden: Bool, isHaveSim2: Bool, isSim2NoSignal: Bool, level: Int?)

    func setDeviceStatesList(items: [Int:String])

    func setAlarm()
    func setAttention()
    func setOffline()
    func setDefault()
}

extension XDeviceView: XDeviceTopViewLayer {
    var ACTION_STATE_ARMED: Int { 1 }
    var ACTION_STATE_DISARMED: Int { 0 }
    var ACTION_STATE_PARTED: Int { 2 }
    
    func setTitle(title: String) { topView.setNavigationViewTitle(title: title) }
    
    func setIsFavorite(isFavorite: Bool) { topView.setNavigationViewIsFavoriteDefault(isFavoritDevice: isFavorite) }
    
    func setIcon(name: String, isRenderTemplate: Bool) { topView.setIconViewImage(name: name, renderMode: isRenderTemplate ? .alwaysTemplate : .alwaysOriginal) }
 
    func setDeviceState(actionState: Int) {
        topView.setActionType(.security)
        if actionState == ACTION_STATE_ARMED { topView.setState(state: .security_armed) }
        if actionState == ACTION_STATE_DISARMED { topView.setState(state: .security_disarmed) }
        if actionState == ACTION_STATE_PARTED { topView.setState(state: .security_part_armed) }
    }
    
    func setDeviceStatesList(items: [Int:String]) { topView.setStatesListItems(items: items.map({ XDeviceTopView.Cell.Model(eventId: $0.key, description: $0.value) })) }
  
    func setDeviceState(isHaveConnection: Bool) { topView.setState(isHaveConnection: isHaveConnection) }
    
    func setDeviceState(isUpdateHidden: Bool) { topView.setState(isUpdateHidden: isUpdateHidden) }
    
    func setDeviceState(isDelayHidden: Bool) { topView.setState(isDelayHidden: isDelayHidden, isDelayArm: true) }
    
    func setDeviceState(isHaveSocket: Bool) { topView.setState(isHaveSocket: isHaveSocket) }
    
    func setDeviceState(isHaveLan: Bool) { topView.setState(isHaveLan: isHaveLan) }
    
    func setDeviceState(level: Int?, isHaveBattery: Bool, isBatteryError: Bool) { topView.setState(isHidden: false, level: level, isHaveBattery: isHaveBattery, isBatteryError: isBatteryError) }

    public func setDeviceState(isDualSim: Bool, isHaveSim: Bool, isSimNoSignal: Bool, level: Int?) {
        topView.setState(isHidden: false, isDualSim: isDualSim, isHaveSim: isHaveSim, isSimNoSignal: isSimNoSignal, level: level)
    }
    
    public func setDeviceState(isHidden: Bool, isHaveSim2: Bool, isSim2NoSignal: Bool, level: Int?) {
        topView.setState(isHidden: isHidden, isHaveSim2: isHaveSim2, isSim2NoSignal: isSim2NoSignal, level: level)
    }
    
    func setAlarm() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.alarm.cardBackground
        topView.cardTextColor = style.alarm.cardText
        topView.cardTintColor = style.alarm.cardTint
        
        topView.actionTintColor = style.alarm.cardTint
        topView.actionCardColor = style.alarm.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setAttention() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.warning.cardBackground
        topView.cardTextColor = style.warning.cardText
        topView.cardTintColor = style.warning.cardTint
        
        topView.actionTintColor = style.warning.cardTint
        topView.actionCardColor = style.warning.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setOffline() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.connectionLost.cardBackground
        topView.cardTextColor = style.connectionLost.cardText
        topView.cardTintColor = style.connectionLost.cardTint
        
        topView.actionTintColor = style.connectionLost.cardTint
        topView.actionCardColor = style.connectionLost.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setDefault() {
        topView.setIconStateRenderMode(mode: .alwaysOriginal)
        topView.cardColor = style._default.cardBackground
        topView.cardTextColor = style._default.cardText
        topView.cardTintColor = style._default.cardTint

        topView.actionTintColor = style._default.actionTintColor
        topView.actionCardColor = style._default.actionCardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
}
