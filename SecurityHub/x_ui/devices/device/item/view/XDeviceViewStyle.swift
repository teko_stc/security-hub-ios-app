//
//  XDeviceViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit

struct XDeviceViewStyle {
    struct State {
        let cardBackground, cardTint, cardText, actionCardBackground, actionTintColor: UIColor
    }
    
    let backgroundColor: UIColor
    let topViewFont: UIFont?
    let alarm, warning, connectionLost, _default: State
    let cell: XDeviceBottomListView.Cell.Style
    let header: XDeviceBottomListView.Header.Style
}
extension XDeviceView {
    typealias Style = XDeviceViewStyle
}
