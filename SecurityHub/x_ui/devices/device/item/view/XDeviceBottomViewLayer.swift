//
//  XDeviceBottomViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit

enum XDeviceBottomViewLayerHeaderUpdateType { case insert, update, delete }

protocol XDeviceBottomViewLayer {
    func updHeader(id: Int, icon: String, name: String, action: String, type: XDeviceBottomViewLayerHeaderUpdateType)
    func updItem(id: Int, headerId: Int, icon: String, name: String, affect: String, action: String, script: String, celsius: Int?, type: XDeviceBottomViewLayerHeaderUpdateType)
}

extension XDeviceView: XDeviceBottomViewLayer {
    func updHeader(id: Int, icon: String, name: String, action: String, type: XDeviceBottomViewLayerHeaderUpdateType) {
        let headerModel = XDeviceBottomListView.Header.Model(id: id, icon: icon, name: name, action: action)
        let type: XDeviceBottomListView.UpdateType = type == .insert ? .insert : type == .update ? .update : .delete
        bottomView.updHeader(model: headerModel, type: type)
    }
    
    func updItem(id: Int, headerId: Int, icon: String, name: String, affect: String, action: String, script: String, celsius: Int?, type: XDeviceBottomViewLayerHeaderUpdateType) {
        let itemModel = XDeviceBottomListView.Cell.Model(id: id, headerId: headerId, title: name, iconName: icon, leftTopState: affect, rightTopState: script, rightBottomState: action, tempState: celsius?.string)
        let type: XDeviceBottomListView.UpdateType = type == .insert ? .insert : type == .update ? .update : .delete
        bottomView.updItem(model: itemModel, type: type)
    }
}
