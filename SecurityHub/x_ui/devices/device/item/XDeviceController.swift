//
//  XDeviceController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDeviceController: XBaseViewController<XDeviceView> {
    override var tabBarIsHidden: Bool { true }
    
    private let strings: XDeviceControllerStrings = XDeviceControllerStrings()
    
    private lazy var topViewLayer: XDeviceTopViewLayer = self.mainView
    private lazy var bottomViewLayer: XDeviceBottomViewLayer = self.mainView
    
    private var deviceId: Int
		private var deviceDisposable: Disposable?, sectionsDisposable: Disposable?, zonesDisposable: Disposable?, zonesDisposableCs: Disposable?
    
    init(deviceId: Int) {
        self.deviceId = deviceId
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
//        mainView.topView.isHiddenSettings = !DataManager.shared.dbHelper.isHub(device_id: deviceId.int64)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.delegate = self
        deviceDisposable = DataManager.shared.getDeviceZonesObserver(device_id: deviceId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .subscribe(onNext: { [weak self] de in
                guard let self = self else { return }

                let deviceType = HubConst.getDeviceType(de.configVersion, cluster: de.cluster)
                let icon = DataManager.shared.d3Const.getDeviceIcons(type: deviceType.int)
                
                DispatchQueue.main.async {
                    self.topViewLayer.setTitle(title: de.deviceName)
                    self.topViewLayer.setIsFavorite(isFavorite: false)
                    self.topViewLayer.setIcon(name: icon.big, isRenderTemplate: false)
                    self.topViewLayer.setDeviceState(actionState: de.armStatus == .armed ? self.topViewLayer.ACTION_STATE_ARMED : de.armStatus == .notFullArmed ? self.topViewLayer.ACTION_STATE_PARTED : self.topViewLayer.ACTION_STATE_DISARMED)
                }
                
                let sid = DataManager.settingsHelper.needSharedMain ? -777 : de.siteIds[0]
                let key = "device", count = DataManager.settingsHelper.needSharedMain ? 40 : 5
                for item in 0...count {
                    let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
                    let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
                    if userDefaults().string(forKey: objectTypeKeyName) == key && de.deviceId == userDefaults().integer(forKey: objectIdKeyName) {
                        DispatchQueue.main.async {
                            self.topViewLayer.setIsFavorite(isFavorite: true)
                        }
                    }
                }
                
                var isDualSim = false, isSimNoSignal = false, isHaveSim = false, simLevel: Int?, isSim2NoSignal = false, isHaveSim2 = false, sim2Level: Int?, isLanNo = false, isSocketNo = false
                var isAlarm = false, isAttention = false, isOffline = false, isBatteryNo = false, isBatteryError = false, batteryLevel: Int?, signalLevel: Int?, celsius: Int?, isArmDelay = false, isNeedUpdate = false
                var affects: [Int: String] = [:]
                for affect in de.affects {
                    switch affect._class {
                    case HubConst.CLASS_ALARM:
                        isAlarm = true
                        affects[affect.id.int] = affect.desc
                        let affect_icon = DataManager.shared.d3Const.getEventIcons(_class: affect._class.int, detector: affect.detector.int, reason: affect.reason.int, statment: affect.active.int)
                        DispatchQueue.main.async {
                            self.topViewLayer.setIcon(name: affect_icon.big, isRenderTemplate: true)
                        }
                        break;
                    case HubConst.CLASS_SABOTAGE, HubConst.CLASS_ATTENTION:
                        isAttention = true
                        affects[affect.id.int] = affect.desc
                        break
                    case HubConst.CLASS_MALFUNCTION:
                        if affect.reason == HubConst.REASON_MALF_LOST_CONNECTION { isOffline = true }
                        else if affect.reason == HubConst.REASON_MAFL_BATT_NO {
                            isBatteryNo = true;
                            isAttention = true;
                            affects[affect.id.int] = affect.desc
                        } else {
                            isAttention = true;
                            affects[affect.id.int] = affect.desc
                        }
                        break;
                    case HubConst.CLASS_SUPPLY:
                        if affect.reason == HubConst.REASON_SOCKET_FAULT { isSocketNo = true }
                        else if affect.reason == HubConst.REASON_BATTERY_LEVEL, let _level = affect.jdata["%"] as? Int { batteryLevel = _level }
                        else if affect.reason == HubConst.REASON_BATTERY_ERROR { isBatteryError = true }
                        break;
                    case HubConst.CLASS_CONNECTIVITY:
                        if affect.reason == HubConst.REASON_CONNECTIVITY_UNAVAILABLE {
                            if(affect.detector == HubConst.DETECTOR_MODEM){ isSimNoSignal = true }
                            else if (affect.detector == HubConst.DETECTOR_GPRS_1) { isDualSim = true; isSimNoSignal = true }
                            else if (affect.detector == HubConst.DETECTOR_GRPS_2){ isDualSim = true; isSim2NoSignal = true }
                            else if (affect.detector == HubConst.DETECTOR_ETHERNET){ isLanNo = true }
                        } else if affect.reason == HubConst.REASON_CONNECTIVITY_LEVEL {
                            if affect.detector == HubConst.DETECTOR_MODEM, let bearer = affect.jdata["bearer"] as? Int {
                                if bearer == 2, let _level = affect.jdata["%"] as? Int { sim2Level = _level }
                                else if let _level = affect.jdata["%"] as? Int { simLevel = _level }
                            }
                            else if affect.detector == HubConst.DETECTOR_GPRS_1, let _level = affect.jdata["%"] as? Int { simLevel = _level }
                            else if affect.detector == HubConst.DETECTOR_GRPS_2, let _level = affect.jdata["%"] as? Int { sim2Level = _level }
                            else if let _level = affect.jdata["%"] as? Int { signalLevel = _level }
                        } else if affect.reason == HubConst.REASON_CONNECTIVITY_IMSI {
                            if affect.detector == HubConst.DETECTOR_MODEM, let bearer = affect.jdata["bearer"] as? Int {
                                if bearer == 2 { isHaveSim2 = true } else { isHaveSim = true }
                            }
                            else if affect.detector == HubConst.DETECTOR_GRPS_2 { isHaveSim2 = true }
                            else if affect.detector == HubConst.DETECTOR_GPRS_1 { isHaveSim = true }
                        }
                        break;
                    case HubConst.CLASS_INFORMATION:
                        if affect.reason == HubConst.REASON_INFORMATION_UPDATE_READY { isNeedUpdate = true }
                        else if affect.reason == HubConst.REASON_INFORMATION_ARM_DELAY { isArmDelay = true }
                        break;
                    case HubConst.CLASS_TELEMETRY:
                        if affect.reason == HubConst.REASON_TEMPERATURE, let _celsius = affect.jdata["celsius"] as? Int { celsius = _celsius }
                        //TODO Zone id > 100
                        break;
                    default:
                        break;
                    }
                }
                
                DispatchQueue.main.async {
                    if isAlarm { self.topViewLayer.setAlarm() }
                    else if isAttention { self.topViewLayer.setAttention() }
                    else if isOffline { self.topViewLayer.setOffline() }
                    else { self.topViewLayer.setDefault() }
                    
                    if deviceType == 4 || deviceType == 10 || deviceType == 11 { isDualSim = true }
                    
                    if (deviceType != HubConst.DEVICETYPE_MB) {
                        self.topViewLayer.setDeviceState(isHaveConnection: !isOffline)
                        self.topViewLayer.setDeviceState(isDelayHidden: !isArmDelay) //TODO Не отображает delay
                        self.topViewLayer.setDeviceState(isUpdateHidden: !isNeedUpdate)
                        self.topViewLayer.setDeviceState(isHaveSocket: !isSocketNo)
                        self.topViewLayer.setDeviceState(isHaveLan: !isLanNo)
                        self.topViewLayer.setDeviceState(level: batteryLevel, isHaveBattery: !isBatteryNo, isBatteryError: isBatteryError)
                        self.topViewLayer.setDeviceState(isDualSim: isDualSim, isHaveSim: isHaveSim, isSimNoSignal: isSimNoSignal, level: simLevel)
                        self.topViewLayer.setDeviceState(isHidden: !isDualSim, isHaveSim2: isHaveSim2, isSim2NoSignal: isSim2NoSignal, level: sim2Level)
                    }
                    
                    self.topViewLayer.setDeviceStatesList(items: affects)
                }

                _ = signalLevel == nil && celsius == nil
                
                self.ifReadySection()
            })
    }
    
    var isReadySection = true
    func ifReadySection() {
        guard isReadySection else {
            return
        }

        isReadySection = false
        sectionsDisposable = DataManager.shared.getSectionsObserver(device_id: deviceId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .subscribe(onNext: { [weak self] se in
                guard let self = self else { return }

                let icons = DataManager.shared.d3Const.getSectionIcons(detector: se.section.detector.int)
                let name = se.section.name
                let action_icon = { () -> String in
                    switch (se.section.detector) {
                    case HubConst.SECTION_TYPE_SECURITY, 0:             return se.armStatus == .armed ? "ic_arm_list"       : "ic_disarm_list"
                    case HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED:   return se.armStatus == .armed ? "ic_control_list"   : "ic_no_control_list"
                    default:                                            return "ic_nil"
                    }
                } ()
                let type: XDeviceBottomViewLayerHeaderUpdateType = se.type == .insert ? .insert : se.type == .update ? .update : .delete
                DispatchQueue.main.async {
                    self.bottomViewLayer.updHeader(id: se.section.section.int, icon: icons.list, name: name, action: action_icon, type: type)
                }

                self.ifReadyZones()
            })
    }
    
    var isReadyZones = true
    func ifReadyZones() {
        guard isReadyZones else {
            return
        }

        isReadyZones = false
        zonesDisposable = DataManager.shared.getZonesObserver(device_id: deviceId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .filter({ de in de.deviceId == self.deviceId && !(de.sectionId == 0 && (de.zoneId == 0 || de.zoneId == 100)) })
            .observeOn(ThreadUtil.shared.backScheduler)
            .subscribe(onNext: { [weak self] de in
                guard let self = self else { return }

                var icon = DataManager.shared.d3Const.getSensorIcons(uid: de.zoneUid.int, detector: de.zoneDetector.int)
                let name = de.zone.name
                let affect_icon = { () -> String in
                    de.affects.contains(where: { $0._class == HubConst.CLASS_ALARM }) ? "ic_nil" :
                    de.affects.contains(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) ? "ic_overlay_offline" :
                    de.affects.contains(where: { $0._class == HubConst.CLASS_SABOTAGE || $0._class == HubConst.CLASS_ATTENTION || ( $0._class == HubConst.CLASS_MALFUNCTION && $0.reason != HubConst.REASON_MALF_LOST_CONNECTION ) }) ? "ic_attention_overlay" : "ic_nil"
                }()

                var action_icon = "ic_nil", script_relay = "ic_nil", celsius: Int? = nil
                for affect in de.affects {
                    if affect._class == HubConst.CLASS_ALARM {
                        icon = DataManager.shared.d3Const.getEventIcons(_class: affect._class.int, detector: affect.detector.int, reason: affect.reason.int, statment: affect.active.int)
                    } else if affect._class == HubConst.CLASS_TELEMETRY && affect.reason == HubConst.REASON_TEMPERATURE, let _celsius = affect.jdata["celsius"] as? Int {
                        celsius = _celsius
                    }
                }

                let model = DataManager.shared.d3Const.getSensorType(uid: de.zoneUid.int)

                if de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY && de.script == nil {
                    action_icon = "ic_nil"
                } else if model == HubConst.SENSORTYPE_WIRED_INPUT || model == HubConst.SENSORTYPE_WIRED_OUTPUT && de.script != nil {
                    action_icon = "ic_nil"
                } else if (de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY || de.zoneDetector & 0xff == HubConst.DETECTOR_MANUAL_RELAY){
                    action_icon = de.armStatus == .armed ? "ic_on_overlay" : "ic_off_overlay"
                }

                if (de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY) {
                    script_relay = de.script == nil ? "ic_auto_overlay" : "ic_overlay_scripted"
                }
							
								if (de.zone.bypass > 0 && de.sectionDetector == HubConst.SECTION_TYPE_SECURITY &&
										de.affects.contains(where: { $0._class == HubConst.CLASS_CONTROL && $0.reason == HubConst.REASON_CONTROL_ARM_BYPASS })) { action_icon = "ic_bypass_overlay" }

                let type: XDeviceBottomViewLayerHeaderUpdateType = de.type == .insert ? .insert : de.type == .update ? .update : .delete
                DispatchQueue.main.async {
                    self.bottomViewLayer.updItem(id: de.zone.zone.int, headerId: de.zone.section.int, icon: icon.main, name: name, affect: affect_icon, action: action_icon, script: script_relay, celsius: celsius, type: type)
                }
            })
    }
    
    override func viewDidDestroy() {
        super.viewDidDestroy()
        deviceDisposable?.dispose()
        sectionsDisposable?.dispose()
        zonesDisposable?.dispose()
				zonesDisposableCs?.dispose()
    }
    
    private func armCompletable(deviceId: Int, sectionId: Int = 0) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.arm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func disarmCompletable(deviceId: Int, sectionId: Int = 0) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: {  [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.disarm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }

    private func update() {
        _ = doOnAvailable(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: {  [weak self] _ in self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : $0, "command" : ["UPDATE_APPLY" : [] ]]) })
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable().subscribe()
    }
}


extension XDeviceController: XDeviceViewDelegate {
    func xScriptButtonViewTapped(value: Int) {
        
    }
    
    func xStateItemTapped(type: StateType) {
        if type == .connectionState {
					zonesDisposableCs = DataManager.shared.getZonesObserver(device_id: deviceId.int64)
                .subscribeOn(ThreadUtil.shared.backScheduler)
                .filter {$0.deviceId == self.deviceId && ($0.zoneId == 0 || $0.zoneId == 100)}
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { device in
                    var isOffline = false, affectTime = ""
                    
                    if let affect = device.affects.first(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) {
                        isOffline = true
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm dd.MM"
                        affectTime = dateFormatter.string(from: Date(timeIntervalSince1970: Double(affect.time)))
                    }

                    let alert = XAlertController(
                        style: self.messageAlert(isOffline),
                        strings: XAlertViewStrings(
                            title: isOffline ? self.strings.offlineAlertTitle : self.strings.onlineAlertTitle,
                            text: isOffline ? affectTime : nil,
                            positive: self.strings.connectionAlertPositive
                        )
                    )
										self.navigationController?.present(alert, animated: true) {
										 		self.zonesDisposableCs?.dispose()
										}
                })
                
        } else if [StateType.battery, StateType.connection, StateType.connectionLevel, StateType.temperature].contains(type) {
            let controller = XDevicesStateController(deviceId: deviceId.int64, zoneId: nil, stateType: type)
            navigationController?.pushViewController(controller, animated: true)
        } else if type == .update {
            guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId.int64) == .disarmed else {
                return showErrorColtroller(message: "error_device_armed".localized())
            }
            let alert = XAlertController(
                style: self.messageAlert2(),
                strings: XAlertViewStrings(
                    title: self.strings.updateTitle,
                    text: nil,
                    positive: self.strings.yes,
                    negative: self.strings.no
                ),
                positive: update
            )
            self.navigationController?.present(alert, animated: true)
        }
    }
    
    func zoneViewTapped(section: Int, zone: Int) {
        let controller = XZoneController(deviceId: deviceId, sectionId: section, zoneId: zone)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func sectionViewTapped(section: Int) {
        let controller = XSectionController(deviceId: deviceId, sectionId: section)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func sectionActionViewTapped(section: Int) {
        //TODO
        guard let section = DataManager.shared.dbHelper.getSection(device_id: deviceId.int64, section_id: section.int64) else { return }
        if section.section.detector != HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED && section.section.detector != HubConst.SECTION_TYPE_SECURITY && section.section.detector != 0 { return }
        let type: XArmAlertControllerType = section.section.detector == HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED ? .tech : .security
        let alert = XArmAlertController(type: type, deviceName: section.section.name, siteName: section.siteNames)
        alert.onArmed = { _ = self.armCompletable(deviceId: self.deviceId, sectionId: section.section.section.int).subscribe() }
        alert.onDisarmed = { _ = self.disarmCompletable(deviceId: self.deviceId, sectionId: section.section.section.int).subscribe() }
        alert.onInfoTapped = { self.sectionViewTapped(section: section.section.section.int) }
        navigationController?.present(alert, animated: true)
    }
    
    func xDeviceTopViewStateListViewItemTapped(model: XDeviceTopView.Cell.Model) {
        guard let event = DataManager.shared.dbHelper.get(event_id: model.eventId.int64),
              let ze = DataManager.shared.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone) else { return }
        let z = DataManager.shared.dbHelper.getCameraEvent(eventId: model.eventId.int64)
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: event.id, icon: event.icon, description: event.affect_desc, record: z?.record, time: event.time, names: XCameraEventViewLayerModel.Names(site: ze.siteNames, device: ze.deviceName, section: ze.sectionName, zone: ze.zone.name),location: nil))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func xLeftActionViewTapped(type: XDeviceTopView.ActionType?) {
        _ = self.armCompletable(deviceId: deviceId).subscribe()
    }
    
    func xRightActionViewTapped(type: XDeviceTopView.ActionType?) {
        _ = self.disarmCompletable(deviceId: deviceId).subscribe()
    }
    
    func xDeviceNavigationBarBackViewTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    func xDeviceNavigationBarFavoritViewTapped(isSelected: Bool) {
        guard let de = DataManager.shared.dbHelper.getDevice(device_id: deviceId.int64) else { return }
        let sid = DataManager.settingsHelper.needSharedMain ? -777 : de.siteIds[0]
        let count = DataManager.settingsHelper.needSharedMain ? 40 : 5
        let key = "device"
        for item in 0...count {
            let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
            let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
            if isSelected && userDefaults().string(forKey: objectTypeKeyName) == nil {
                userDefaults().setValue(key, forKey: objectTypeKeyName)
                userDefaults().setValue(de.device.id, forKey: objectIdKeyName)
                if sid < 0 {
                    let count = userDefaults().integer(forKey: "SharedDashboardCount")
                    userDefaults().set(count + 1, forKey: "SharedDashboardCount")
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            } else if !isSelected && userDefaults().string(forKey: objectTypeKeyName) == key && userDefaults().integer(forKey: objectIdKeyName) == de.device.id {
                if sid < 0 {
                    deleteSharedDashboard(item: item)
                } else {
                    userDefaults().setValue(nil, forKey: objectTypeKeyName)
                    userDefaults().setValue(0, forKey: objectIdKeyName)
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            }
        }
        self.topViewLayer.setIsFavorite(isFavorite: false)
        showErrorColtroller(message: strings.noEmptyCell)
    }
    
    func xDeviceNavigationBarSettingsViewTapped() {
        let controller = XDeviceSettingsController(deviceId: deviceId.int64)
        navigationController?.pushViewController(controller, animated: true)
    }
}
