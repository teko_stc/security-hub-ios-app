//
//  XDeviceControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 27.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XDeviceControllerStrings: XCommandStringsProtocol {
    /// Потеряна связь с контроллером
    var offlineAlertTitle = "N_CONTROLLER_OFFLINE".localized()
    
    /// Элемент системы на связи
    var onlineAlertTitle = "N_SYSTEM_PART_ONLINE".localized()
    
    /// OK
    var connectionAlertPositive = "N_BUTTON_OK".localized()
    
    /// На главном экране больше нет свободных ячеек
    var noEmptyCell: String = "N_ERROR_NO_SPACE_ON_MAIN".localized()
    
    /// Команда отправлена
    var commadSend: String { "COMMAND_BEEN_SEND".localized() }
    
    var yes = "YES".localized()
    
    var no = "NO".localized()

    var updateTitle = "N_DEVICE_UPDATE_READY_QUESTION".localized() 
}
