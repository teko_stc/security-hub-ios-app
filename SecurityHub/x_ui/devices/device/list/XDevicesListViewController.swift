//
//  XDevicesListViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XCommandStringsProtocol {
    var commadSend: String { get }
}

class XDevicesListViewController: XBaseViewController<XDevicesListView> {
    
    private lazy var viewLayer: XDevicesListViewLayer = self.mainView
    private lazy var strings: XCommandStringsProtocol = XDevicesListViewControllerStrings()
    
    private var zonesDisposable: Disposable? = nil
    private var searchText: String?
    private var o2, o3: NSObjectProtocol?
		private var priority = 0
    
    private var onboarbingView = OnboardingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        
        load()
        
        o2 = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil, queue: OperationQueue.main) { n in
					
					let cp = userDefaults().integer(forKey: "DashboardPrioritySiteId")
					
					if cp != self.priority {
							self.viewLayer.clear()
							self.load() }
        }
        o3 = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name("XDevicePartitionMenuController_SearchViewChange"), object: nil, queue: OperationQueue.main) { n in

            var temp = n.object as? String
            if temp?.isEmpty == true { temp = nil }

            guard self.searchText != temp else {
                return
            }

            self.searchText = temp
            self.viewLayer.clear()
            self.load()
        }
        setupOnboarbingView()
        setupNotifications()
    }
    
    private func load() {
        priority = userDefaults().integer(forKey: "DashboardPrioritySiteId")

        zonesDisposable?.dispose()
			
				zonesDisposable = DataManager.shared.getZonesObserver(priority: priority, searchText: searchText)
					.subscribeOn(ThreadUtil.shared.backScheduler)
					.map({ (des: [DZoneEntity]) -> (models: [XDevicesListViewCellModel], type: XRxModelState) in
						var type: XRxModelState = .delete
						var models: [XDevicesListViewCellModel] = []
						
						des.forEach { de in
							type = de.type == .insert ? .insert : de.type == .update ? .update : .delete
							models.append(self.map(de: de)) }

						return (models: models, type: type)
					})
					.observeOn(ThreadUtil.shared.mainScheduler)
					.subscribe(onNext: { result in
						self.viewLayer.updateItems(items: result.models, type: result.type, priority: self.priority)
					}, onError: { error in
						print(error)
					})
    }
    
    private func setupNotifications() {
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateOnboardingView), name: HubNotification.siteUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateOnboardingView), name: HubNotification.deviceUpdate, object: nil)
    }
    
    func setupOnboarbingView() {
        onboarbingView.isHidden = true
        mainView.addSubview(onboarbingView)
        onboarbingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            onboarbingView.topAnchor.constraint(equalTo: view.topAnchor),
            onboarbingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            onboarbingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            onboarbingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    @objc func updateOnboardingView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }
            let isHaveSite = DataManager.shared.dbHelper.getSites().count > 0
            let isHaveDevice = DataManager.shared.dbHelper.getDevices().count > 0
        
            switch (isHaveSite, isHaveDevice){
            case (false, _):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_MAIN_EMPTY".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let vc = SiteAddController(site_id: nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                })
            case (true, false):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_DEVICES_MESSAGE_NO_DEVICES".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let items = DataManager.shared.dbHelper.getSites().map({ XBottomSheetViewItem(title: $0.name, text: nil, any: $0.id) })
                    let vc = SiteAddController(site_id: items.first != nil ? (items.first!.any as! Int64) : nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                })
            default:
                self.onboarbingView.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateOnboardingView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if let o2 = o2 { DataManager.shared.nCenter.removeObserver(o2) }
//        zonesDisposable?.dispose()
    }
    
    private func map(de: DZoneEntity) -> XDevicesListViewCellModel {
        let icon = ((de.zoneId == 0 || de.zoneId == 100) && de.sectionId == 0) ?
            DataManager.shared.d3Const.getDeviceIcons(type: HubConst.getDeviceType(de.configVersion, cluster: de.cluster).int) :
            DataManager.shared.d3Const.getSensorIcons(uid: de.zoneUid.int, detector: de.zoneDetector.int)
        let name = ((de.zoneId == 0 || de.zoneId == 100) && de.sectionId == 0) ? de.deviceName : de.zone.name
        let affect_icon = { () -> String in
            de.affects.contains(where: { $0._class == HubConst.CLASS_ALARM }) ? "ic_alarm_overlay" :
            de.affects.contains(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) ? "ic_overlay_offline" :
            de.affects.contains(where: { $0._class == HubConst.CLASS_SABOTAGE || $0._class == HubConst.CLASS_ATTENTION || ( $0._class == HubConst.CLASS_MALFUNCTION && $0.reason != HubConst.REASON_MALF_LOST_CONNECTION ) }) ? "ic_attention_overlay" : "ic_nil"
        }()

        var action_icon = "ic_nil"
        var action_type: XDevicesListView.Cell.Model.DType? = nil
        if ((de.zoneId == 0 || de.zoneId == 100) && de.sectionId == 0) {
            action_type = .security
            switch de.armStatus {
            case .notFullArmed: action_icon = "ic_partly_arm_list"
            case .armed:        action_icon = "ic_arm_list"
            default:            action_icon = "ic_disarm_list"
            }
        } else if (de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY) {
            action_icon = de.script == nil ? "ic_relay_type_auto_black" : "ic_relay_scripted_list_offline"
        } else if (de.zoneDetector & 0xff == HubConst.DETECTOR_MANUAL_RELAY) {
            action_type = .relay
            action_icon = de.armStatus == .armed ? "ic_turn_on_list" : "ic_turn_off_list"
        } else if (de.sectionDetector == HubConst.SECTION_TYPE_SECURITY || de.sectionDetector == 0) {
					if de.zone.bypass > 0 && de.sectionDetector == HubConst.SECTION_TYPE_SECURITY &&
							de.affects.contains(where: { $0._class == HubConst.CLASS_CONTROL && $0.reason == HubConst.REASON_CONTROL_ARM_BYPASS }) { action_icon = "ic_bypass" }
					else { action_icon = de.armStatus == .armed ? "ic_arm_list" : "ic_disarm_list" }
        } else if (de.sectionDetector == HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED) {
            action_icon = de.armStatus == .armed ? "ic_control_list" : "ic_no_control_list"
				}

        return XDevicesListViewCellModel(siteIds: de.siteIds,
                                         deviceId: de.deviceId.int,
                                         sectionId: de.sectionId.int,
                                         zoneId: de.zoneId.int,
                                         deviceIconName: icon.list,
                                         deviceName: name,
                                         siteName: de.siteNames,
                                         deviceStatusIconName: affect_icon,
                                         deviceArmStatusIconName: action_icon,
                                         actionType: action_type)
    }
    
    private func armCompletable(deviceId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.arm(device: $0, section: 0) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func disarmCompletable(deviceId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.disarm(device: $0, section: 0) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func onCompletable(deviceId: Int, sectionId: Int, zoneId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.switchRelay(device: $0, section: sectionId.int64, zone: zoneId.int64, state: 10) })
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func offCompletable(deviceId: Int, sectionId: Int, zoneId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.switchRelay(device: $0, section: sectionId.int64, zone: zoneId.int64, state: 0) })
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable()
    }
}

extension XDevicesListViewController: XDevicesListViewDelegate {
    func xDevicesListViewDidSelectItem(model: XDevicesListView.Cell.Model) {
        if ((model.zoneId == 0 || model.zoneId == 100) && model.sectionId == 0) {
            let controller = XDeviceController(deviceId: model.deviceId)
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = XZoneController(deviceId: model.deviceId, sectionId: model.sectionId, zoneId: model.zoneId)
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func xDevicesListViewActionViewTapped(model: XDevicesListView.Cell.Model) {
        if (model.actionType == .security) {
            let alert = XArmAlertController(type: .security, deviceName: model.deviceName, siteName: model.siteName)
            alert.onArmed = { _ = self.armCompletable(deviceId: model.deviceId).subscribe() }
            alert.onDisarmed = { _ = self.disarmCompletable(deviceId: model.deviceId).subscribe() }
            alert.onInfoTapped = { self.xDevicesListViewDidSelectItem(model: model) }
            navigationController?.present(alert, animated: true)
        } else if (model.actionType == .relay) {
            let alert = XArmAlertController(type: .relay, deviceName: model.deviceName, siteName: model.siteName)
            alert.onArmed = { _ = self.onCompletable(deviceId: model.deviceId, sectionId: model.sectionId, zoneId: model.zoneId).subscribe() }
            alert.onDisarmed = { _ = self.offCompletable(deviceId: model.deviceId, sectionId: model.sectionId, zoneId: model.zoneId).subscribe() }
            alert.onInfoTapped = { self.xDevicesListViewDidSelectItem(model: model) }
            navigationController?.present(alert, animated: true)
        }
    }
}
