//
//  XDevicesListViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XDevicesListViewDelegate {
    func xDevicesListViewDidSelectItem(model: XDevicesListView.Cell.Model)
    func xDevicesListViewActionViewTapped(model: XDevicesListView.Cell.Model)
}
