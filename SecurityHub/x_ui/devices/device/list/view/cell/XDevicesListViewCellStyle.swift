//
//  XDevicesListViewCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicesListViewCellStyle {
    let backgroundColor, selectedBackgroundColor: UIColor
    let deviceIconColor: UIColor
    let title, subTitle: XBaseLableStyle
}
extension XDevicesListViewCell {
    typealias Style = XDevicesListViewCellStyle
}

