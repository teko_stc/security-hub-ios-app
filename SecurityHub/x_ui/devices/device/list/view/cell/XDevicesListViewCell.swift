//
//  XDevicesListViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDevicesListViewCell: UITableViewCell {
    static var identifier = "XCameraBindListViewCell.identifier"

    private let iconView:       UIImageView = UIImageView()
    private let statusView:     UIImageView = UIImageView()
    private let armStatusView:  UIImageView = UIImageView()
    private let titleView:      UILabel = UILabel()
    private let subTitleView:   UILabel = UILabel()
    private let backView:       UIView = UIView()
    
    private var model:          Model?
    private var action:         ((_ model: Model) ->())?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: Model, action: ((_ model: Model) -> ())? = nil) {
        self.model = model
        self.action = action
        
        iconView.image = UIImage(named: model.deviceIconName)?.withRenderingMode(.alwaysTemplate)
        titleView.text = model.deviceName
        subTitleView.text = model.siteName
        statusView.image = UIImage(named: model.deviceStatusIconName)
        armStatusView.image = UIImage(named: model.deviceArmStatusIconName)
    }
    
    public func setStyle(_ style: Style) {
        backgroundColor = style.backgroundColor
        backView.backgroundColor = style.selectedBackgroundColor
        
        iconView.tintColor = style.deviceIconColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        titleView.numberOfLines = 0
        
        subTitleView.font = style.subTitle.font
        subTitleView.textColor = style.subTitle.color
        subTitleView.textAlignment = style.subTitle.alignment
        subTitleView.numberOfLines = 0
        
        setConstraints()
    }
    
    private func initViews() {
        selectedBackgroundView = backView
        backView.layer.cornerRadius = 5
        iconView.contentMode = .scaleAspectFit
        contentView.addSubview(iconView)
        contentView.addSubview(titleView)
        contentView.addSubview(subTitleView)
        statusView.contentMode = .scaleAspectFit
        contentView.addSubview(statusView)
        armStatusView.isUserInteractionEnabled = true
        armStatusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(armStatusViewTapped)))
        armStatusView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(armStatusViewLongPressed)))
        armStatusView.contentMode = .scaleAspectFit
        contentView.addSubview(armStatusView)
    }
    
    private func setConstraints() {
        [iconView, titleView, subTitleView, statusView, armStatusView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: 36),
            iconView.heightAnchor.constraint(equalToConstant: 36),
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),

            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 4),
            subTitleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
            subTitleView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor),
            subTitleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            
            statusView.widthAnchor.constraint(equalToConstant: 30),
            statusView.heightAnchor.constraint(equalToConstant: 30),
            statusView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            statusView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 10),
            
            armStatusView.widthAnchor.constraint(equalToConstant: 30),
            armStatusView.heightAnchor.constraint(equalToConstant: 30),
            armStatusView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            armStatusView.leadingAnchor.constraint(equalTo: statusView.trailingAnchor, constant: 10),
            armStatusView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
        ])
    }
    
    @objc private func armStatusViewTapped(_ sender: UIGestureRecognizer) {
        guard let model = model, let _ = model.actionType else { return }
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.armStatusView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.armStatusView.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.action?(model) }
        }
    }
    
    @objc private func armStatusViewLongPressed(_ sender: UIGestureRecognizer) {
        guard let model = model, let _ = model.actionType else { return }
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.armStatusView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.armStatusView.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.action?(model) }
            })
        }
    }
}
extension XDevicesListView {
    typealias Cell = XDevicesListViewCell
}
