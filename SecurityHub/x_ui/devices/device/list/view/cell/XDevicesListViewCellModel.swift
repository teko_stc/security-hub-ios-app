//
//  XDevicesListViewCellModel.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

struct XDevicesListViewCellModel {
    enum DType {
        case security
        case relay
        case technologies
    }
    
    let siteIds: [Int64]
    let deviceId, sectionId, zoneId: Int
    let deviceIconName, deviceName, siteName: String
    let deviceStatusIconName, deviceArmStatusIconName: String
    let actionType: DType?
}
extension XDevicesListViewCell {
    typealias Model = XDevicesListViewCellModel
}
