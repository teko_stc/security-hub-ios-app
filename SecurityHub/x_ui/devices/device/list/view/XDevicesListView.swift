//
//  XDevicesListView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XDevicesListViewLayer {
    func updateItems(items: [XDevicesListView.Cell.Model], type: XRxModelState, priority: Int)
    func clear()
}

class XDevicesListView: UIView {
    public var delegate: XDevicesListViewDelegate?
    
    private var tableView: UITableView = UITableView()
    private var items: [Cell.Model] = []
    
		//private let lock = XDevicesListView.self
    
    private lazy var style: Style = styles()
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.background
        
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.backgroundColor = style.background
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
    }
    
    private func setConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}

extension XDevicesListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setContent(model: items[indexPath.row], action: actionViewTapped)
        cell.setStyle(style.cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { return UIView() }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { return 20 }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { UIView() }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 70 }
}

extension XDevicesListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        let model = items[indexPath.row]
        delegate?.xDevicesListViewDidSelectItem(model: model)
    }
    
    func actionViewTapped(model: XDevicesListView.Cell.Model) {
        delegate?.xDevicesListViewActionViewTapped(model: model)
    }
}

extension XDevicesListView: XDevicesListViewLayer {
    func clear() {
        self.items = []
        self.tableView.reloadData()
    }
  
    func updateItems(items: [Cell.Model], type: XRxModelState, priority: Int) {
//        objc_sync_enter(lock)
        UIView.performWithoutAnimation {
            self._updateItems(items: items, type: type, priority: priority)
        }
//        objc_sync_exit(lock)
    }
    
    func _updateItems(items: [Cell.Model], type: XRxModelState, priority: Int) {
        if items.count > 1, type == .insert {
            self.items.append(contentsOf: items)
            self.tableView.reloadData()

            return
        }

        if items.count == 0 { return }
        switch type {
        case .insert:
            self.tableView.beginUpdates()
            for i in 0...items.count-1 {
                if (self.items.count == 0) {
                    self.items.append(items[i])
                    self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                } else {
                    let index: Int
                    if items[i].siteIds.contains(priority.int64), items[i].zoneId == 100 {
                        index = self.items.firstIndex { de in de.siteIds.contains(priority.int64) && de.deviceId == items[i].deviceId } ?? 0
                    } else if items[i].siteIds.contains(priority.int64) {
                        index = (self.items.lastIndex { de in de.siteIds.contains(priority.int64) && de.deviceId == items[i].deviceId } ?? -1) + 1
                    } else {
                        index = self.items.count
                    }
                    self.items.insert(items[i], at: index)
                    self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .none)
                }
            }
            self.tableView.endUpdates()
//            self.tableView.reloadData()
        case .update:
            self.tableView.beginUpdates()
            items.forEach { item in
                if let index = self.items.firstIndex(where: { _item in item.deviceId == _item.deviceId && item.sectionId == _item.sectionId && item.zoneId == _item.zoneId }) {
                    self.items[index] = item
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                }
            }
            self.tableView.endUpdates()
        case .delete:
            self.tableView.beginUpdates()
            var indexes: [IndexPath] = []
            items.forEach { item in
                if let index = self.items.firstIndex(where: { _item in item.deviceId == _item.deviceId && item.sectionId == _item.sectionId && item.zoneId == _item.zoneId }) {
                    indexes.append(IndexPath(row: index, section: 0))
                }
            }
            indexes.sort(by: { i1, i2 in i1.row > i2.row })
            indexes.forEach { index in self.items.remove(at: index.row) }
            self.tableView.deleteRows(at: indexes, with: .none)
            self.tableView.endUpdates()
        }
    }
}
