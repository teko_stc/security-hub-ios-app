//
//  XDevicesListViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicesListViewStyle {
    let background: UIColor
    let cell: XDevicesListView.Cell.Style
}
extension XDevicesListView {
    typealias Style = XDevicesListViewStyle
}
