//
//  XDevicesListViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XDevicesListViewControllerStrings: XCommandStringsProtocol {
    /// Команда отправлена
    var commadSend: String { "COMMAND_BEEN_SEND".localized() }
}
