//
//  XDevicesListViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 18.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDevicesListView {
    func styles() -> Style {
        Style(
            background: UIColor.white,
            cell: Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.1),
                deviceIconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    alignment: .left
                ),
                subTitle: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .left
                )
            )
        )
    }
}
