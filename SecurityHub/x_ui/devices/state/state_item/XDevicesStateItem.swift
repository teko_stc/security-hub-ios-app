//
//  XDevicesStateItem.swift
//  SecurityHub
//
//  Created by Daniil on 26.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDevicesStateItem: UIView {
    
    private var imageView: UIImageView = UIImageView()
    private var labelsStackView: UIStackView = UIStackView()
    private var titleLabel: UILabel = UILabel()
    private var subtitleLabel: UILabel = UILabel()
    
    private var image: String
    private var title: String
    private var subtitle: String?
    
    init(image: String, title: String, subtitle: String?) {
        self.image = image
        self.title = title
        self.subtitle = subtitle
        
        super.init(frame: .zero)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        imageView.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = style.iconTint
        addSubview(imageView)
        
        labelsStackView.axis = .vertical
        labelsStackView.distribution = .equalSpacing
        labelsStackView.spacing = 2
        addSubview(labelsStackView)
        
        titleLabel.text = title.trimmingCharacters(in: .whitespacesAndNewlines)
        titleLabel.font = style.titleLabel.font
        titleLabel.textColor = style.titleLabel.color
        titleLabel.numberOfLines = 0
        labelsStackView.addArrangedSubview(titleLabel)
        
        if let subtitle = subtitle {
            subtitleLabel.text = subtitle
            subtitleLabel.font = style.subtitleLabel.font
            subtitleLabel.textColor = style.subtitleLabel.color
            labelsStackView.addArrangedSubview(subtitleLabel)
        }
    }
    
    private func setConstraints() {
        [imageView, labelsStackView].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 56),
            
            imageView.widthAnchor.constraint(equalToConstant: 36),
            imageView.heightAnchor.constraint(equalToConstant: 36),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            
            labelsStackView.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
            labelsStackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 16),
            labelsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        ])
    }
}
