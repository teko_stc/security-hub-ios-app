//
//  XDevicesStateItemStyles.swift
//  SecurityHub
//
//  Created by Daniil on 26.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicesStateItemStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    let iconTint: UIColor
    
    let titleLabel: Label
    let subtitleLabel: Label
}

extension XDevicesStateItem {
    typealias Style = XDevicesStateItemStyle
    
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            iconTint: UIColor(red: 0x3A/255, green: 0xBE/255, blue: 0xFF/255, alpha: 1),
            titleLabel: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            subtitleLabel: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0xBC/255, green: 0xBC/255, blue: 0xBC/255, alpha: 1)
            )
        )
    }
}
