//
//  XDevicesStateController.swift
//  SecurityHub
//
//  Created by Daniil on 22.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDevicesStateController: XBaseViewController<XDevicesStateView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        ),
        titleView: XBaseLableStyle(
            color: UIColor.colorFromHex(0x414042),
            font: UIFont(name: "Open Sans", size: 25)
        )
    )
    
    private let strings: XDevicesStateControllerStrings = XDevicesStateControllerStrings()
    
    private var statesDisposable: Disposable?
    
    private var deviceId: Int64?
    private var zoneId: Int64?
    private var stateType: StateType
    
    init(deviceId: Int64?, zoneId: Int64?, stateType: StateType) {
        self.deviceId = deviceId
        self.zoneId = zoneId
        self.stateType = stateType
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getEvents()
        getStateInfo()
        setTitle()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        statesDisposable?.dispose()
    }
    
    private func setTitle() {
        switch stateType {
        case .battery:
            title = strings.batteryTitle
        
        case .connection, .connectionLevel:
            title = strings.connectionTitle
        
        case .temperature:
            title = strings.temperatureTitle
        
        default:
            break
        }
    }
    
    private func getEvents() {
        let _class: Int64?
        switch self.stateType {
        case .connection, .connectionLevel: _class = 7
        case .temperature: _class = 10
        default: _class = nil
        }
        
        DataManager.shared.getDBEvents(deviceId: deviceId ?? 0, zoneId: zoneId ?? 0, _class: _class) { [weak self] dEvents in
            guard let self = self else { return }
            let models = dEvents.filter{  (event) -> Bool in
                switch self.stateType {
                case .battery: return event._class == 6 || (event._class == 2 && event.reason == 50)
                case .temperature: return event._class == 10 && event.reason == 4
                default: return true
                }
            }
            .map({ (de: DEventEntity) -> XEventsViewCellModel in
                XEventsViewCellModel(
                    id: de.id,
                    icon: de.icon,
                    description: de.affect_desc,
                    record: de.videoRecord,
                    time: de.time,
                    names: XEventsViewCellModel.Names(site: de.siteName, device: de.deviceName, section: de.sectionName ?? "", zone: de.zoneName ?? ""),
                    isDevice: de.section == 0 && (de.zone == 0 || de.zone == 100),
										location: LocationManager.getLocation(jdata: de.jdata)
                )
            })
        
            self.mainView.setEvents(models)
        }
    }
    
    private func getStateInfo() {
        statesDisposable?.dispose()
        statesDisposable = DataManager.shared.getZonesObserver(device_id: deviceId ?? 0)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .filter({
                if let _ = self.zoneId {
                    return $0.zoneId == self.zoneId
                } else {
                    return ($0.zoneId == 0 || $0.zoneId == 100)
                }
            })
            .subscribe(onNext: { device in
                var batteryError = false, batteryNo = false, batteryTitle = "", batterySubtitle = "", batteryLevel = -1
                var rBatteryError = false, rBatteryNo = false, rBatteryTitle = "", rBatterySubtitle = "", rBatteryLevel = -1
                var socketError = false, socketNo = false, socketTitle = "", socketSubtitle = ""
                var isDualSim = false, simNoSignal = false, simExist = false, simTitle = "", simSubtitle = "", sim2NoSignal = false, sim2Exist = false, sim2Title = "", sim2Subtitle = ""
                var lanNo = false, lanTitle = "", lanSubtitle = ""
                var connectionTitle = "", connectionSubtitle = "", connectionLevel = -1
                var temperatureTitle = "", temperatureSubtitle = "", temperatureOutTitle = "", temperatureOutSubtitle = ""
                
                for affect in device.affects {
                    switch affect._class {
                    case HubConst.CLASS_MALFUNCTION:
                        if affect.reason == HubConst.REASON_MAFL_BATT_LOW {
                            batteryError = true
                            
                            batteryTitle = affect.desc
                            batterySubtitle = self.getTimeFromMillis(millis: affect.time)
                        } else if affect.reason == HubConst.REASON_MAFL_BATT_NO {
                            batteryNo = true
                            
                            batteryTitle = affect.desc
                            batterySubtitle = self.getTimeFromMillis(millis: affect.time)
                        }
                        
                    case HubConst.CLASS_SUPPLY:
                        switch affect.reason {
                        case HubConst.REASON_BATTERY_NORESERVE:
                            rBatteryNo = true
                            
                            rBatteryTitle = affect.desc
                            rBatterySubtitle = self.getTimeFromMillis(millis: affect.time)
                            
                        case HubConst.REASON_SOCKET_FAULT:
                            socketNo = false
                            
                            socketTitle = affect.desc
                            socketSubtitle = self.getTimeFromMillis(millis: affect.time)
                            
                        case HubConst.REASON_BATTERY_MAINSUNDER, HubConst.REASON_BATTERY_MAINOVER, HubConst.REASON_BATTERY_RESERVE:
                            socketError = true
                            
                            socketTitle = affect.desc
                            socketSubtitle = self.getTimeFromMillis(millis: affect.time)
                            
                        case HubConst.REASON_BATTERY_LEVEL:
                            batteryLevel = self.getLevel(affect)
                            
                            batteryTitle = affect.desc
                            batterySubtitle = self.getTimeFromMillis(millis: affect.time)
                        
                        case HubConst.REASON_BATTERY_BATTLOW, HubConst.REASON_BATTERY_ERROR:
                            batteryError = true
                            
                            batteryTitle = affect.desc
                            batterySubtitle = self.getTimeFromMillis(millis: affect.time)
                            
                        case HubConst.REASON_BATTERY_RESBATTLEVEL:
                            rBatteryLevel = self.getLevel(affect)
                            
                            rBatteryTitle = affect.desc
                            rBatterySubtitle = self.getTimeFromMillis(millis: affect.time)
                        
                        case HubConst.REASON_BATTERY_RESBATTLOW, HubConst.REASON_BATTERY_RESBATTERROR:
                            rBatteryError = true
                            
                            rBatteryTitle = affect.desc
                            rBatterySubtitle = self.getTimeFromMillis(millis: affect.time)
                            
                        default:
                            break
                        }
                        
                    case HubConst.CLASS_CONNECTIVITY:
                        switch affect.reason {
                        case HubConst.REASON_CONNECTIVITY_UNAVAILABLE:
                            switch affect.detector {
                            case HubConst.DETECTOR_MODEM:
                                simNoSignal = true
                                
                                simTitle = affect.desc
                                
                            case HubConst.DETECTOR_GPRS_1:
                                isDualSim = true
                                simNoSignal = true
                                
                                simTitle = affect.desc
                                
                            case HubConst.DETECTOR_GRPS_2:
                                isDualSim = true
                                sim2NoSignal = true
                                
                                sim2Title = affect.desc
                                
                            case HubConst.DETECTOR_ETHERNET:
                                lanNo = true
                                
                                lanTitle = affect.desc
                                lanSubtitle = self.getTimeFromMillis(millis: affect.time)
                                
                            default:
                                break
                            }
                        
                        case HubConst.REASON_CONNECTIVITY_LEVEL:
                            switch affect.detector {
                            case HubConst.DETECTOR_MODEM:
                                if affect.jdata["bearer"] as? Int == 1 {
                                    simTitle = affect.desc
                                } else {
                                    sim2Title = affect.desc
                                }
                                
                            case HubConst.DETECTOR_GPRS_1:
                                simTitle = affect.desc
                                
                            case HubConst.DETECTOR_GRPS_2:
                                sim2Title = affect.desc
                                
                            default:
                                connectionLevel = self.getLevel(affect)
                                
                                connectionTitle = affect.desc
                                connectionSubtitle = self.getTimeFromMillis(millis: affect.time)
                            }
                        
                        case HubConst.REASON_CONNECTIVITY_IMSI:
                            switch affect.detector {
                            case HubConst.DETECTOR_MODEM:
                                if affect.jdata["bearer"] as? Int == 1 {
                                    simExist = true
                                    
                                    simSubtitle = "\(affect.desc)\n\(self.getTimeFromMillis(millis: affect.time))"
                                } else {
                                    sim2Exist = true
                                    
                                    sim2Subtitle = "\(affect.desc)\n\(self.getTimeFromMillis(millis: affect.time))"
                                }
                            
                            case HubConst.DETECTOR_GPRS_1:
                                simExist = true
                                
                                simSubtitle = "\(affect.desc)\n\(self.getTimeFromMillis(millis: affect.time))"
                                
                            case HubConst.DETECTOR_GRPS_2:
                                sim2Exist = true
                                
                                sim2Subtitle = "\(affect.desc)\n\(self.getTimeFromMillis(millis: affect.time))"
                                
                            default:
                                break
                            }
                            
                        default:
                            break
                        }
                        
                    case HubConst.CLASS_TELEMETRY:
                        if Int64(affect.sectionZone ?? "-1") == self.zoneId {
                            temperatureTitle = affect.desc
                            temperatureSubtitle = self.getTimeFromMillis(millis: affect.time)
                        } else {
                            temperatureOutTitle = affect.desc
                            temperatureOutSubtitle = self.getTimeFromMillis(millis: affect.time)
                        }
                        
                    default:
                        break
                    }
                }
                
                let deviceType = HubConst.getDeviceType(device.configVersion, cluster: device.cluster)
                if deviceType == 4 || deviceType == 10 || deviceType == 11 { isDualSim = true }
                
                var items: [XDevicesStateItem] = []
                
                switch self.stateType {
                case .battery:
                    if self.zoneId == nil {
                        items.append(
                            XDevicesStateItem(
                                image: socketNo ? "ic_socket_no" : socketError ? "ic_socket_error" : "ic_socket",
                                title: !socketTitle.isEmpty ? socketTitle : self.strings.socketOk,
                                subtitle: !socketSubtitle.isEmpty ? socketSubtitle : nil
                            )
                        )
                    }
                    
                    items.append(
                        XDevicesStateItem(
                            image: batteryLevel != -1 ? self.getBatteryImage(batteryLevel) : batteryNo ? "ic_battery_no" : batteryError ? "ic_battery_error" : "ic_battery",
                            title: !batteryTitle.isEmpty ? batteryTitle : self.strings.batteryOk,
                            subtitle: !batterySubtitle.isEmpty ? batterySubtitle : nil
                        )
                    )
                    
                    if !rBatteryTitle.isEmpty && !rBatterySubtitle.isEmpty {
                        items.append(
                            XDevicesStateItem(
                                image: rBatteryLevel != -1 ? self.getBatteryImage(batteryLevel) : rBatteryNo ? "ic_battery_no" : rBatteryError ? "ic_battery_error" : "ic_battery",
                                title: rBatteryTitle,
                                subtitle: rBatterySubtitle
                            )
                        )
                    }
                    
                case .connection:
                    items.append(
                        XDevicesStateItem(
                            image: lanNo ? "ic_lan_no" : "ic_lan",
                            title: !lanTitle.isEmpty ? lanTitle : self.strings.lanOk,
                            subtitle: !lanSubtitle.isEmpty ? lanSubtitle : nil
                        )
                    )
                    
                    if !isDualSim {
                        items.append(
                            XDevicesStateItem(
                                image: !simExist ? "ic_sim_no" : simNoSignal ? "ic_sim_no_signal" : "ic_sim",
                                title: !simTitle.isEmpty ? simTitle : self.strings.simOk,
                                subtitle: !simSubtitle.isEmpty ? simSubtitle : nil
                            )
                        )
                    } else {
                        items.append(
                            XDevicesStateItem(
                                image: !simExist ? "ic_sim_1_no" : simNoSignal ? "ic_sim_1_no_signal" : "ic_sim_1",
                                title: !simTitle.isEmpty ? simTitle : self.strings.simOk,
                                subtitle: !simSubtitle.isEmpty ? simSubtitle : nil
                            )
                        )
                        
                        items.append(
                            XDevicesStateItem(
                                image: !sim2Exist ? "ic_sim_2_no" : sim2NoSignal ? "ic_sim_2_no_signal" : "ic_sim_2",
                                title: !sim2Title.isEmpty ? sim2Title : self.strings.simOk,
                                subtitle: !sim2Subtitle.isEmpty ? sim2Subtitle : nil
                            )
                        )
                    }
                    
                case .connectionLevel:
                    items.append(
                        XDevicesStateItem(
                            image: self.getConnectionImage(connectionLevel),
                            title: !connectionTitle.isEmpty ? connectionTitle : self.strings.connectionLevelUnknown,
                            subtitle: !connectionSubtitle.isEmpty ? connectionSubtitle : nil
                        )
                    )
                
                case .temperature:
                    if !temperatureTitle.isEmpty {
                        items.append(
                            XDevicesStateItem(
                                image: "ic_temp",
                                title: temperatureTitle,
                                subtitle: !temperatureSubtitle.isEmpty ? temperatureSubtitle : nil
                            )
                        )
                    } else if !temperatureOutTitle.isEmpty {
                        items.append(
                            XDevicesStateItem(
                                image: "ic_temp_2",
                                title: temperatureOutTitle,
                                subtitle: !temperatureOutSubtitle.isEmpty ? temperatureOutSubtitle : nil
                            )
                        )
                    } else {
                        items.append(
                            XDevicesStateItem(
                                image: "ic_temp",
                                title: self.strings.temperatureUnknown,
                                subtitle: nil
                            )
                        )
                    }
                    
                default:
                    break
                }
                
                self.mainView.setStateItems(items)
            })
    }
    
    private func getTimeFromMillis(millis: Int64) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH:mm dd.MM"
        
        return dateFormatter.string(from: Date(timeIntervalSince1970: Double(millis)))
    }
    
    private func getLevel(_ affect: DAffectEntity) -> Int {
        let jdata = affect.jdata
        
        if !jdata.filter({ $0.key == "%" }).isEmpty {
            return jdata["%"] as! Int
        } else if !jdata.filter({ $0.key == "db" }).isEmpty {
            return jdata["db"] as! Int
        }
        
        return -1
    }
    
    private func getBatteryImage(_ batteryLevel: Int) -> String {
        return batteryLevel >= 90 ? "ic_battery_high" : batteryLevel >= 50 || batteryLevel == -1 ? "ic_battery_mid" : batteryLevel >= 10 ? "ic_battery_low" : "ic_battery_empty"
    }
    
    private func getConnectionImage(_ connectionLevel: Int) -> String {
        return connectionLevel >= 90 || connectionLevel == -1 ? "ic_level_top" : connectionLevel >= 50 ? "ic_level_high" : connectionLevel >= 10 ? "ic_level_mid" : "ic_level_low"
    }
}

extension XDevicesStateController: XDevicesStateViewDelegate {
    func eventTapped(_ model: XEventsViewCell.Model) {
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: nil, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: model.location))
        
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func onLocationTapped(_ model: XEventsViewCell.Model) {
			let elc = XEventsLocationController()
			elc.location = model.location
			elc.eventTitle = "\(model.description), \(model.names.site), \(model.names.device)"
			navigationController?.present(elc, animated: true)
		}
}
