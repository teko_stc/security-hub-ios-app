//
//  XDevicesStateCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 28.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XDevicesStateCellModel {
    struct Names {
        let site, device, section, zone: String
    }
    
    let id: Int64
    let icon: String
    let description: String
    let record: String?
    let time: Int64
    let names: Names
}

extension XDevicesStateCell {
    typealias Model = XDevicesStateCellModel
}
