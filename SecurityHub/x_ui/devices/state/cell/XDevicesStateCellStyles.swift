//
//  XDevicesStateCellStyles.swift
//  SecurityHub
//
//  Created by Daniil on 28.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicesStateCellStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    
    let iconTint: UIColor
    
    let title: Label
    let subtitle: Label
    let dateTime: Label
}

extension XDevicesStateCell {
    typealias Style = XDevicesStateCellStyle
    
    func cellStyle() -> Style {
        return Style(
            backgroundColor: UIColor.clear,
            iconTint: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            title: Style.Label(
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor.colorFromHex(0x414042)
            ),
            subtitle: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x414042)
            ),
            dateTime: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x414042)
            )
        )
    }
}
