//
//  XDevicesStateCell.swift
//  SecurityHub
//
//  Created by Daniil on 28.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDevicesStateCell: UITableViewCell {
    
    static var identifier = "XDevicesStateCell.identifier"
    
    private var model: Model?
    private var onViewTapped: ((Model) -> ())?
    
    private var iconImage: UIImageView = UIImageView()
    private var titleLabel: UILabel = UILabel()
    private var subtitleLabel: UILabel = UILabel()
    private var dateTimeLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews(cellStyle())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = style.backgroundColor
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longGesture)))
        
        iconImage.tintColor = style.iconTint
        addSubview(iconImage)
        
        titleLabel.font = style.title.font
        titleLabel.textColor = style.title.color
        titleLabel.numberOfLines = 0
        addSubview(titleLabel)
        
        subtitleLabel.font = style.subtitle.font
        subtitleLabel.textColor = style.subtitle.color
        subtitleLabel.numberOfLines = 0
        addSubview(subtitleLabel)
        
        dateTimeLabel.font = style.dateTime.font
        dateTimeLabel.textColor = style.dateTime.color
        dateTimeLabel.numberOfLines = 0
        dateTimeLabel.textAlignment = .right
        addSubview(dateTimeLabel)
    }
    
    private func setConstraints() {
        [iconImage, titleLabel, subtitleLabel, dateTimeLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            iconImage.widthAnchor.constraint(equalToConstant: 36),
            iconImage.heightAnchor.constraint(equalToConstant: 36),
            iconImage.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            iconImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            
            dateTimeLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            dateTimeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: dateTimeLabel.leadingAnchor, constant: -16),
            
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
            bottomAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 16)
        ])
    }
    
    public func setContent(model: Model) {
        self.model = model
        
        iconImage.image = UIImage(named: model.icon)
        titleLabel.text = model.description
    
        subtitleLabel.text = !model.names.zone.isEmpty && !model.names.section.isEmpty ? "\(model.names.site)\n\(model.names.zone) · \(model.names.section)" : "\(model.names.site)\n\(model.names.device)"
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM\nHH:mm"
        dateTimeLabel.text = dateFormatterGet.string(from: Date(timeIntervalSince1970: Double(model.time)))
    }
    
    public func setDelegate(onViewTapped: @escaping (Model) -> ()) {
        self.onViewTapped = onViewTapped
    }
    
    @objc private func tapGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            
            if let model = self.model { self.onViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                
                if let model = self.model { self.onViewTapped?(model) }
            })
        }
    }
}

extension XDevicesStateView {
    typealias Cell = XDevicesStateCell
}
