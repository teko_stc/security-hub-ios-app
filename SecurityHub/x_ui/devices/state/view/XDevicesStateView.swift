//
//  XDevicesStateView.swift
//  SecurityHub
//
//  Created by Daniil on 22.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDevicesStateView: UIView {
    
    private let strings: XDevicesStateViewsStrings = XDevicesStateViewsStrings()
    
    public var delegate: XDevicesStateViewDelegate?
    
    private var events: [XEventsViewCell.Model] = []
    
    private var cardView: UIView = UIView()
    private var statesStackView: UIStackView = UIStackView()
    private var historyLabel: UILabel = UILabel()
    private var historyTable: UITableView = UITableView()
    private var historyEmptyLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        cardView.backgroundColor = style.cardBackground
        cardView.layer.cornerRadius = 40
        addSubview(cardView)
        
        statesStackView.axis = .vertical
        statesStackView.distribution = .equalSpacing
        statesStackView.spacing = 20
        addSubview(statesStackView)
        
        historyLabel.text = strings.history
        historyLabel.font = style.history.font
        historyLabel.textColor = style.history.color
        addSubview(historyLabel)
        
        historyTable.backgroundColor = style.backgroundColor
        historyTable.separatorStyle = .none
        historyTable.delegate = self
        historyTable.dataSource = self
        historyTable.register(XEventsViewCell.self, forCellReuseIdentifier: XEventsViewCell.identifier)
        addSubview(historyTable)
        
        historyEmptyLabel.text = strings.empty
        historyEmptyLabel.font = style.empty.font
        historyEmptyLabel.textColor = style.empty.color
        addSubview(historyEmptyLabel)
    }
    
    private func setConstraints() {
        [cardView, statesStackView, historyLabel, historyTable, historyEmptyLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            statesStackView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 16),
            statesStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            statesStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            cardView.bottomAnchor.constraint(equalTo: statesStackView.bottomAnchor, constant: 36),
            
            historyLabel.topAnchor.constraint(equalTo: cardView.bottomAnchor, constant: 16),
            historyLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            
            historyTable.topAnchor.constraint(equalTo: historyLabel.bottomAnchor, constant: 16),
            historyTable.leadingAnchor.constraint(equalTo: leadingAnchor),
            historyTable.trailingAnchor.constraint(equalTo: trailingAnchor),
            historyTable.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            
            historyEmptyLabel.centerXAnchor.constraint(equalTo: historyTable.centerXAnchor),
            historyEmptyLabel.centerYAnchor.constraint(equalTo: historyTable.centerYAnchor)
        ])
    }
    
    public func setStateItems(_ items: [XDevicesStateItem]) {
        statesStackView.subviews.forEach { $0.removeFromSuperview() }
        
        items.forEach { statesStackView.addArrangedSubview($0) }
    }
    
    private var t: Double = 0
    public func addEvent(event: XEventsViewCell.Model) {
        if !historyEmptyLabel.isHidden {
            historyEmptyLabel.isHidden = true
        }
        
        events.append(event)
        t = Date().timeIntervalSince1970
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            sleep(1)
            if Date().timeIntervalSince1970 < self.t + 1 {
                DispatchQueue.main.async { self.historyTable.reloadData() }
            }
        }
//        historyTable.insertRows(at: [IndexPath(row: events.count - 1, section: 0)], with: .none)
    }
    
    func setEvents(_ models: [XEventsViewCell.Model]) {
        if !historyEmptyLabel.isHidden && models.count > 0 {
            historyEmptyLabel.isHidden = true
        }
        events = models
        historyTable.reloadData()
    }
}

extension XDevicesStateView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XEventsViewCell.identifier, for: indexPath) as! XEventsViewCell
    
        cell.setStyle(style().cell)
        cell.setContent(model: events[indexPath.row])
				cell.setDelegate(onViewTapped: { [self] n in self.delegate?.eventTapped(n) }, onRecordTapped: { n in }, onLocationTapped: { n in self.delegate?.onLocationTapped(n) })
        
        return cell
    }
}
