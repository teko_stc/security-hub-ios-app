//
//  XDevicesStateViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 28.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XDevicesStateViewDelegate {
    func eventTapped(_ model: XEventsViewCell.Model)
		func onLocationTapped(_ model: XEventsViewCell.Model)
}
