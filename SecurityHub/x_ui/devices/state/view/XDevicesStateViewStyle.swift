//
//  XDevicesStateViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 22.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDevicesStateViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    let cardBackground: UIColor
    
    let history: Label
    let empty: Label
    
    let cell: XEventsViewCell.Style
}

extension XDevicesStateView {
    typealias Style = XDevicesStateViewStyle
}
