//
//  XDevicesStateControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 22.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDevicesStateView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
            cardBackground: UIColor.white,
            history: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            empty: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            cell: XEventsView.Cell.Style(
                background: .clear,
                iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    alignment: .left
                ),
                text: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .left
                ),
                time: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .center
                )
            )
        )
    }
}
