//
//  XDevicesStateControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 22.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XDevicesStateControllerStrings {
    /// Текущее состояние питания
    var batteryTitle = "N_EVENT_HISTORY_CURRENT_SUPPLY".localized()
    
    /// Текущее состояние каналов связи
    var connectionTitle = "N_EVENT_HISTORY_CURRENT_ETH".localized()
    
    /// Текущие данные о температуре
    var temperatureTitle = "N_EVENT_HISTORY_CURRENT_TEMP".localized()
    
    /// Питание от сети в норме
    var socketOk = "N_SOCKET_OK".localized()
    
    /// Уровень заряда батареи неизвестен
    var batteryOk = "N_BATT_OK".localized()
    
    /// Соединение Ethernet активно
    var lanOk = "N_LAN_OK".localized()
    
    /// SIM-карта не установлена
    var simOk = "N_SIM_NO".localized()
    
    /// Уровень сигнала неизвестен
    var connectionLevelUnknown = "N_LEVEL_UNKNOWN".localized()
    
    /// Нет данных о температуре
    var temperatureUnknown = "N_TEMP_NO_DATA".localized()
}

class XDevicesStateViewsStrings {
    /// История
    var history = "SUBTITLE_EVENTS".localized()
    
    /// Нет данных
    var empty = "NO_DATA".localized()
}
