//
//  XSectionsListViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 26.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XSectionsListViewController: XBaseViewController<XDevicesListView> {
    
    private lazy var viewLayer: XDevicesListViewLayer = self.mainView
    private lazy var strings: XCommandStringsProtocol = XDevicesListViewControllerStrings()

    private var sectionsDisposable: Disposable? = nil
    private var o2, o3: NSObjectProtocol?
    private var searchText: String?
		private var priority = 0
    
    private var onboarbingView = OnboardingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        
        searchText = userDefaults().string(forKey: "XDevicePartitionMenuController_SearchViewChange")
        load()

        o2 = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil, queue: OperationQueue.main) { n in

					let cp = userDefaults().integer(forKey: "DashboardPrioritySiteId")
					
					if cp != self.priority {
							self.viewLayer.clear()
							self.load() }
        }
        o3 = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name("XDevicePartitionMenuController_SearchViewChange"), object: nil, queue: OperationQueue.main) { n in
            var temp = n.object as? String
            if temp?.isEmpty == true { temp = nil }

            guard self.searchText != temp else {
                return
            }

            self.searchText = temp
            self.viewLayer.clear()
            self.load()
        }

        setupOnboarbingView()
        setupNotifications()
    }
    
    private func load() {
        priority = userDefaults().integer(forKey: "DashboardPrioritySiteId")

        sectionsDisposable?.dispose()
        sectionsDisposable = DataManager.shared.getSectionsObserver(priority: priority, searchText: searchText)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (des: [DSectionEntity]) -> (models: [XDevicesListViewCellModel], type: XRxModelState) in
                var type: XRxModelState = .delete
                var models: [XDevicesListViewCellModel] = []
                des.forEach { se in
                    type = se.type == .insert ? .insert : se.type == .update ? .update : .delete
                    models.append(self.map(se: se))
                }
                return (models: models, type: type)
            })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { result in
							self.viewLayer.updateItems(items: result.models, type: result.type, priority: self.priority)
            }, onError: { error in
                print(error)
            })
    }
    
    private func map(se: DSectionEntity) -> XDevicesListViewCellModel {
        let icons = DataManager.shared.d3Const.getSectionIcons(detector: se.section.detector.int)
        let affect_icon = { () -> String in
            se.affects.contains(where: { $0._class == HubConst.CLASS_ALARM }) ? "ic_alarm_overlay" :
            se.affects.contains(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) ? "ic_overlay_offline" :
            se.affects.contains(where: { $0._class == HubConst.CLASS_SABOTAGE || $0._class == HubConst.CLASS_ATTENTION || ( $0._class == HubConst.CLASS_MALFUNCTION && $0.reason != HubConst.REASON_MALF_LOST_CONNECTION ) }) ? "ic_attention_overlay" : "ic_nil"
        }()
        let action_icon = { () -> String in
            switch (se.section.detector) {
            case HubConst.SECTION_TYPE_SECURITY, 0:             return se.armStatus == .armed ? "ic_arm_list"       : "ic_disarm_list"
            case HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED:   return se.armStatus == .armed ? "ic_control_list"   : "ic_no_control_list"
            default:                                            return "ic_nil"
            }
        } ()
        let action_type = { () -> XDevicesListView.Cell.Model.DType? in
            switch (se.section.detector) {
            case HubConst.SECTION_TYPE_SECURITY, 0:             return .security
            case HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED:   return .technologies
            default:                                            return nil
            }
        }()
        
        return XDevicesListViewCellModel(siteIds: se.siteIds, deviceId: se.deviceId.int, sectionId: se.section.section.int, zoneId: 0, deviceIconName: icons.list, deviceName: se.section.name, siteName: se.siteNames, deviceStatusIconName: affect_icon, deviceArmStatusIconName: action_icon, actionType: action_type)
    }
    
    private func setupNotifications() {
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateOnboardingView), name: HubNotification.siteUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateOnboardingView), name: HubNotification.deviceUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateOnboardingView), name: HubNotification.sectionsUpdate, object: nil)
    }
    
    func setupOnboarbingView() {
        onboarbingView.isHidden = true
        mainView.addSubview(onboarbingView)
        onboarbingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            onboarbingView.topAnchor.constraint(equalTo: view.topAnchor),
            onboarbingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            onboarbingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            onboarbingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    @objc func updateOnboardingView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }

            let isHaveSite = DataManager.shared.dbHelper.getSites().count > 0
            let isHaveDevice = DataManager.shared.dbHelper.getDevices().count > 0
            let isHaveSection = DataManager.shared.dbHelper.getSections().filter({ $0.section != 0 }).count > 0

            switch (isHaveSite, isHaveDevice, isHaveSection){
            case (false, _, _):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_MAIN_EMPTY".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let vc = SiteAddController(site_id: nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                })
            case (true, false, _):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_DEVICES_MESSAGE_NO_DEVICES".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let items = DataManager.shared.dbHelper.getSites().map({ XBottomSheetViewItem(title: $0.name, text: nil, any: $0.id) })
                    let vc = SiteAddController(site_id: items.first != nil ? (items.first!.any as! Int64) : nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                })
            case (true, true, false):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_SECTIONS_MESSAGE_NO_SECTIONS".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let deviceIds = DataManager.shared.dbHelper.getDeviceIds()
                    switch deviceIds.count {
                    case 1:
                        let vc = XAddSectionController(device_id: deviceIds[0])
                        self.navigationController?.pushViewController(vc, animated: false)
                        break
                    default:
                        let void: ((_ sec: Sections) -> Void) = { sec in
                            let vc = XAddSectionController(device_id: sec.device)
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                        let controller = SectionSelectController(site: nil, device_ids: deviceIds, title: "DTA_CHOOSE_DEVICE".localized(), isDevice: true, select: void)
                        self.navigationController?.pushViewController(controller, animated: false)
                    }
                })
            default:
                self.onboarbingView.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateOnboardingView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if let o2 = o2 { DataManager.shared.nCenter.removeObserver(o2) }
//        sectionsDisposable?.dispose()
    }
    
    private func armCompletable(deviceId: Int, sectionId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.arm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func disarmCompletable(deviceId: Int, sectionId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.disarm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
}

extension XSectionsListViewController: XDevicesListViewDelegate {
    func xDevicesListViewDidSelectItem(model: XDevicesListView.Cell.Model) {
        let controller = XSectionController(deviceId: model.deviceId, sectionId: model.sectionId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func xDevicesListViewActionViewTapped(model: XDevicesListView.Cell.Model) {
        let alert_type: XArmAlertControllerType = model.actionType == .security ? .security : .tech
        let alert = XArmAlertController(type: alert_type, deviceName: model.deviceName, siteName: model.siteName)
        alert.onArmed = { _ = self.armCompletable(deviceId: model.deviceId, sectionId: model.sectionId).subscribe() }
        alert.onDisarmed = { _ = self.disarmCompletable(deviceId: model.deviceId, sectionId: model.sectionId).subscribe() }
        alert.onInfoTapped = { self.xDevicesListViewDidSelectItem(model: model) }
        navigationController?.present(alert, animated: true)
    }
}
