//
//  XSectionViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 17.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XSectionView {
    func styles() -> Style {
        Style(
            backgroundColor: .white,
            topViewFont: UIFont(name: "Open Sans", size: 23),
            alarm: Style.State(
                cardBackground: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1),
                cardTint: UIColor.white,
                cardText: UIColor.white,
                actionCardBackground: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1),
                actionTintColor: UIColor.white
            ),
            warning: Style.State(
                cardBackground: UIColor(red: 0xff/255, green: 0xdf/255, blue: 0x57/255, alpha: 1),
                cardTint: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor(red: 0xff/255, green: 0xdf/255, blue: 0x57/255, alpha: 1),
                actionTintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            connectionLost: Style.State(
                cardBackground: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
                cardTint: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
                actionTintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            _default: Style.State(
                cardBackground: UIColor.white,
                cardTint: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                actionTintColor: UIColor.white
            ),
            cell: XDeviceBottomListView.Cell.Style(
                backgroundColor: .white,
                tintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                titleFont: UIFont(name: "Open Sans", size: 15.5),
                tempFont: UIFont(name: "Open Sans", size: 18)
            ),
            header: XDeviceBottomListView.Header.Style(
                background: .white,
                selected: UIColor(red: 0xcc/255, green: 0xcc/255, blue: 0xcc/255, alpha: 1),
                tint: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 18)
            )
        )
    }
}
