//
//  XSectionView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 17.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSectionView: UIView {
    private var scrollView: UIScrollView = UIScrollView()
    private var contentView = UIView()
    var topView: XDeviceTopView!
    var bottomView: XDeviceBottomListView!
    
    lazy var style: Style = styles()
    
    public var delegate: XSectionViewDelegate? {
        didSet {
            topView.delegate = delegate
            bottomView.delegate = delegate
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.contentSize = contentView.frame.size
    }
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.backgroundColor
        
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        bottomView = XDeviceBottomListView(cellStyle: style.cell, headerStyle: style.header)
        contentView.addSubview(bottomView)
        
        topView = XDeviceTopView()
        topView.leftStatesMargin = 0
        topView.font = style.topViewFont
        contentView.addSubview(topView)
    }
    
    private func setConstraints() {
        [scrollView, contentView, topView, bottomView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            topView.topAnchor.constraint(equalTo: contentView.topAnchor),
            topView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            topView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: -60),
            bottomView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            bottomView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            bottomView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30)
        ])
    }
}
