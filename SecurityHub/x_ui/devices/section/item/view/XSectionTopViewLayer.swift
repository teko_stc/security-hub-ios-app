//
//  XDeviceTopViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


enum XSectionTopViewLayerSectionType {
    case security, technical, fire, alarm
}

protocol XSectionTopViewLayer {
    var ACTION_STATE_ARMED: Int { get }
    var ACTION_STATE_DISARMED: Int { get }

    func setTitle(title: String)
    func setIsFavorite(isFavorite: Bool)
    func setIcon(name: String, isRenderTemplate: Bool)
    func setSectionState(type: XSectionTopViewLayerSectionType?, isActionActive: Bool?)
    func setSectionStatesList(items: [Int:String])
    
    func setAlarm()
    func setAttention()
    func setOffline()
    func setDefault()
}

extension XSectionView: XSectionTopViewLayer {
    var ACTION_STATE_ARMED: Int { 1 }
    var ACTION_STATE_DISARMED: Int { 0 }
    
    func setTitle(title: String) { topView.setNavigationViewTitle(title: title) }
    
    func setIsFavorite(isFavorite: Bool) { topView.setNavigationViewIsFavoriteDefault(isFavoritDevice: isFavorite) }
    
    func setIcon(name: String, isRenderTemplate: Bool) { topView.setIconViewImage(name: name, renderMode: isRenderTemplate ? .alwaysTemplate : .alwaysOriginal) }
 
    func setSectionState(type: XSectionTopViewLayerSectionType?, isActionActive: Bool?) {
        switch type {
        case .security:
            topView.setActionType(.security)
            if isActionActive == true { topView.setState(state: .security_armed) }
            else { topView.setState(state: .security_disarmed) }
            break;
        case .technical:
            topView.setActionType(.technical)
            if isActionActive == true { topView.setState(state: .technical_armed) }
            else { topView.setState(state: .technical_disarmed) }
            break;
        case .fire, .alarm:
            if isActionActive == true { topView.setActionType(.reset) }
            else { topView.setActionType(nil) }
            topView.setState(state: nil)
            break;
        case .none:
            topView.setActionType(nil)
            topView.setState(state: nil)
            break;
        }
    }
        
    func setSectionStatesList(items: [Int:String]) { topView.setStatesListItems(items: items.map({ XDeviceTopView.Cell.Model(eventId: $0.key, description: $0.value) })) }
    
    func setAlarm() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.alarm.cardBackground
        topView.cardTextColor = style.alarm.cardText
        topView.cardTintColor = style.alarm.cardTint
        
        topView.actionTintColor = style.alarm.cardTint
        topView.actionCardColor = style.alarm.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setAttention() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.warning.cardBackground
        topView.cardTextColor = style.warning.cardText
        topView.cardTintColor = style.warning.cardTint
        
        topView.actionTintColor = style.warning.cardTint
        topView.actionCardColor = style.warning.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setOffline() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.connectionLost.cardBackground
        topView.cardTextColor = style.connectionLost.cardText
        topView.cardTintColor = style.connectionLost.cardTint
        
        topView.actionTintColor = style.connectionLost.cardTint
        topView.actionCardColor = style.connectionLost.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setDefault() {
        topView.setIconStateRenderMode(mode: .alwaysOriginal)
        topView.cardColor = style._default.cardBackground
        topView.cardTextColor = style._default.cardText
        topView.cardTintColor = style._default.cardTint

        topView.actionTintColor = style._default.actionTintColor
        topView.actionCardColor = style._default.actionCardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
}
