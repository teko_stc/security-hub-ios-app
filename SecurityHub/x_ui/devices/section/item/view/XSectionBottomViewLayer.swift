//
//  XSectionBottomViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 17.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit

enum XSectionBottomViewLayerHeaderUpdateType { case insert, update, delete }

protocol XSectionBottomViewLayer {
    func initHeader(id: Int)
    func updItem(id: Int, headerId: Int, icon: String, name: String, affect: String, action: String, script: String, celsius: Int?, type: XSectionBottomViewLayerHeaderUpdateType)
}

extension XSectionView: XSectionBottomViewLayer {
    func initHeader(id: Int) {
        let headerModel = XDeviceBottomListView.Header.Model(id: id, icon: "ic_nil", name: "", action: "ic_nil")
        let type: XDeviceBottomListView.UpdateType = .insert
        bottomView.updHeader(model: headerModel, type: type)
    }
    
    func updItem(id: Int, headerId: Int, icon: String, name: String, affect: String, action: String, script: String, celsius: Int?, type: XSectionBottomViewLayerHeaderUpdateType) {
        let itemModel = XDeviceBottomListView.Cell.Model(id: id, headerId: headerId, title: name, iconName: icon, leftTopState: affect, rightTopState: script, rightBottomState: action, tempState: celsius?.string)
        let type: XDeviceBottomListView.UpdateType = type == .insert ? .insert : type == .update ? .update : .delete
        bottomView.updItem(model: itemModel, type: type)
    }
}
