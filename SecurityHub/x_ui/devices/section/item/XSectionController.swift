//
//  XSectionController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 17.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit
import RxSwift

class XSectionController: XBaseViewController<XSectionView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var topViewLayer: XSectionTopViewLayer = self.mainView
    private lazy var bottomViewLayer: XSectionBottomViewLayer = self.mainView

    private var deviceId, sectionId: Int
    private var sectionDisposable: Disposable?, zonesDisposable: Disposable?
    private var observerEvents, observerResetZone: Any?
    
    private let strings: XZoneControllerStrings = XZoneControllerStrings()

    init(deviceId: Int, sectionId: Int) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
//        mainView.topView.isHiddenSettings = !DataManager.shared.dbHelper.isHub(device_id: deviceId.int64)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        self.bottomViewLayer.initHeader(id: sectionId)
        
        sectionDisposable = DataManager.shared.getSectionObserver(device_id: deviceId.int64, section_id: sectionId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] se in
                guard let self = self else { return }
                
                let icons = DataManager.shared.d3Const.getSectionIcons(detector: se.section.detector.int)
                let name = se.section.name
                
                self.topViewLayer.setTitle(title: name)
                self.topViewLayer.setIsFavorite(isFavorite: false)
                self.topViewLayer.setIcon(name: icons.big, isRenderTemplate: false)
                
                if (se.section.detector == HubConst.SECTION_TYPE_SECURITY || se.section.detector == 0) {
                    self.topViewLayer.setSectionState(type: .security, isActionActive: se.armStatus == .armed)
                } else if (se.section.detector == HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED) {
                    self.topViewLayer.setSectionState(type: .technical, isActionActive: se.armStatus == .armed)
                } else {
                    self.topViewLayer.setSectionState(type: .alarm, isActionActive: DataManager.shared.dbHelper.isHaveAlarmTenMinAgo(deviceId: self.deviceId.int64, sectionId: self.sectionId.int64))
                    if se.type == .insert {
                        self.observerEvents = DataManager.shared.nCenter.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main, using: self.updateResetState)
                        self.observerResetZone = DataManager.shared.nCenter.addObserver(forName: NSNotification.Name.init("resetZone"), object: nil, queue: OperationQueue.main, using: self.updateResetState)
                    }
                }
                
                let sid = DataManager.settingsHelper.needSharedMain ? -777 : se.siteIds[0]
                let key = "section", count = DataManager.settingsHelper.needSharedMain ? 40 : 5
                for item in 0...count {
                    let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
                    let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
                    if userDefaults().string(forKey: objectTypeKeyName) == key && se.section.id == userDefaults().integer(forKey: objectIdKeyName) {
                        self.topViewLayer.setIsFavorite(isFavorite: true)
                    }
                }
                
                var isAlarm = false, isAttention = false, isOffline = false
//                var affects: [Int: String] = [:]
                for affect in se.affects {
                    switch affect._class {
                    case HubConst.CLASS_ALARM:
                        isAlarm = true
//                        affects[affect.id.int] = affect.desc
                        let affect_icon = DataManager.shared.d3Const.getEventIcons(_class: affect._class.int, detector: affect.detector.int, reason: affect.reason.int, statment: affect.active.int)
                        self.topViewLayer.setIcon(name: affect_icon.big, isRenderTemplate: true)
                        break;
                    case HubConst.CLASS_SABOTAGE, HubConst.CLASS_ATTENTION:
                        isAttention = true
//                        affects[affect.id.int] = affect.desc
                        break
                    case HubConst.CLASS_MALFUNCTION:
                        if affect.reason == HubConst.REASON_MALF_LOST_CONNECTION { isOffline = true }
                        if affect.reason != HubConst.REASON_MAFL_BATT_NO { isAttention = true }
//                        affects[affect.id.int] = affect.desc
                        break;
                    default:
                        break;
                    }
                }
                
                if isAlarm { self.topViewLayer.setAlarm() }
                else if isAttention { self.topViewLayer.setAttention() }
                else if isOffline { self.topViewLayer.setOffline() }
                else { self.topViewLayer.setDefault() }
                
//                self.topViewLayer.setSectionStatesList(items: affects)
            })
        
        zonesDisposable = DataManager.shared.getZonesObserver(device_id: deviceId.int64, section_id: sectionId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { de in
                var icon = DataManager.shared.d3Const.getSensorIcons(uid: de.zoneUid.int, detector: de.zoneDetector.int)
                let name = de.zone.name
                let affect_icon = { () -> String in
                    de.affects.contains(where: { $0._class == HubConst.CLASS_ALARM }) ? "ic_nil" :
                    de.affects.contains(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) ? "ic_overlay_offline" :
                    de.affects.contains(where: { $0._class == HubConst.CLASS_SABOTAGE || $0._class == HubConst.CLASS_ATTENTION || ( $0._class == HubConst.CLASS_MALFUNCTION && $0.reason != HubConst.REASON_MALF_LOST_CONNECTION ) }) ? "ic_attention_overlay" : "ic_nil"
                }()
                
                var action_icon = "ic_nil", script_relay = "ic_nil", celsius: Int? = nil
                for affect in de.affects {
                    if affect._class == HubConst.CLASS_ALARM {
                        icon = DataManager.shared.d3Const.getEventIcons(_class: affect._class.int, detector: affect.detector.int, reason: affect.reason.int, statment: affect.active.int)
                    } else if affect._class == HubConst.CLASS_TELEMETRY && affect.reason == HubConst.REASON_TEMPERATURE, let _celsius = affect.jdata["celsius"] as? Int {
                        celsius = _celsius
                    }
                }
                
                if (de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY) {
                    script_relay = de.script == nil ? "ic_auto_overlay" : "ic_overlay_scripted"
                    if de.script != nil { action_icon = de.armStatus == .armed ? "ic_on_overlay" : "ic_off_overlay" }
                }
                else if (de.zoneDetector & 0xff == HubConst.DETECTOR_MANUAL_RELAY) { action_icon = de.armStatus == .armed ? "ic_on_overlay" : "ic_off_overlay" }
							
								if (de.zone.bypass > 0 && de.sectionDetector == HubConst.SECTION_TYPE_SECURITY &&
										de.affects.contains(where: { $0._class == HubConst.CLASS_CONTROL && $0.reason == HubConst.REASON_CONTROL_ARM_BYPASS })) { action_icon = "ic_bypass_overlay" }
                
                let type: XSectionBottomViewLayerHeaderUpdateType = de.type == .insert ? .insert : de.type == .update ? .update : .delete
                self.bottomViewLayer.updItem(id: de.zone.zone.int, headerId: de.zone.section.int, icon: icon.main, name: name, affect: affect_icon, action: action_icon, script: script_relay, celsius: celsius, type: type)
            })
    }
    
    @objc private func updateResetState(notification: Notification) {
        guard let event = notification.object as? (event: Events, type: NUpdateType) else {
            return self.topViewLayer.setSectionState(type: .alarm, isActionActive: DataManager.shared.dbHelper.isHaveAlarmTenMinAgo(deviceId: self.deviceId.int64, sectionId: self.sectionId.int64))
        }
        guard event.event.device == deviceId && event.event.section == sectionId && event.event._class == HubConst.CLASS_ALARM && event.event.active == 1 else { return }
        self.topViewLayer.setSectionState(type: .alarm, isActionActive: true)
    }
    
    private func armCompletable(deviceId: Int, sectionId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.arm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result, type: .security) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func disarmCompletable(deviceId: Int, sectionId: Int) -> Completable {
        return doOnConnect(deviceId: deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in
							self?.waitNotReadyZone = true
							self?.showToast(message: self?.strings.commadSend) })
            .flatMap({ DataManager.shared.disarm(device: $0, section: sectionId.int64) })
            .do(onSuccess: { [weak self] result in
							if result.success { self?.waitNotReadyZone = false }
							self?.resultProcessing(result, type: .security) })
            .do(onError: { [weak self] error in
							self?.waitNotReadyZone = false
							self?.errorProcessing(error) })
            .asCompletable()
    }
    
    //TODO
    private func resetCompletable(deviceId: Int, sectionId: Int) {
        showToast(message: strings.commadSend)
        return DataManager.shared.dbHelper.getZones(device_id: deviceId.int64, section_id: sectionId.int64)
            .forEach { zone in
                _ = doOnConnect(deviceId: deviceId.int64)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in self?.showToast(message: self?.strings.commadSend) })
                    .flatMap({ DataManager.shared.reset(device: $0, section: sectionId.int64, zone: zone.zone) })
                    .do(onSuccess: { res in if (res.success) { DataManager.shared.dbHelper.dropLastAlarm(device_id: self.deviceId.int64, section_id: self.sectionId.int64, zone_id: zone.zone) }})
                    .do(onSuccess: { [weak self] result in self?.resultProcessing(result, type: .reset) })
                    .do(onError: { [weak self] error in self?.errorProcessing(error) })
                    .asCompletable().subscribe()
        }
    }
    
    private func resultProcessing(_ result: DCommandResult, type: XDeviceTopView.ActionType?) {
        if result.success, type == .reset { return self.topViewLayer.setSectionState(type: .alarm, isActionActive: false) }
        if result.success { return }
        showToast(message: result.message)
    }
}

extension XSectionController: XSectionViewDelegate {
    func xStateItemTapped(type: StateType) {
        
    }
    
    func xScriptButtonViewTapped(value: Int) {
        
    }
    
    func zoneViewTapped(section: Int, zone: Int) {
        let controller = XZoneController(deviceId: deviceId, sectionId: sectionId, zoneId: zone)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func sectionViewTapped(section: Int) { }
    
    func sectionActionViewTapped(section: Int) { }
    
    func xDeviceTopViewStateListViewItemTapped(model: XDeviceTopView.Cell.Model) {
        guard let event = DataManager.shared.dbHelper.get(event_id: model.eventId.int64),
              let ze = DataManager.shared.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone) else { return }
        let z = DataManager.shared.dbHelper.getCameraEvent(eventId: model.eventId.int64)
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: event.id, icon: event.icon, description: event.affect_desc, record: z?.record, time: event.time, names: XCameraEventViewLayerModel.Names(site: ze.siteNames, device: ze.deviceName, section: ze.sectionName, zone: ze.zone.name),location: nil))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func xLeftActionViewTapped(type: XDeviceTopView.ActionType?) {
        if (type == .reset) {
            resetCompletable(deviceId: deviceId, sectionId: sectionId)
        } else if (type == .security || type == .technical) {
            _ = self.armCompletable(deviceId: deviceId, sectionId: sectionId).subscribe()
        }
    }
    
    func xRightActionViewTapped(type: XDeviceTopView.ActionType?) {
        if (type == .security || type == .technical) {
            _ = self.disarmCompletable(deviceId: deviceId, sectionId: sectionId).subscribe()
        }
    }
    
    func xDeviceNavigationBarBackViewTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    func xDeviceNavigationBarFavoritViewTapped(isSelected: Bool) {
        guard let se = DataManager.shared.dbHelper.getSection(device_id: deviceId.int64, section_id: sectionId.int64) else { return }
        let sid = DataManager.settingsHelper.needSharedMain ? -777 : se.siteIds[0]
        let count = DataManager.settingsHelper.needSharedMain ? 40 : 5
        let key = "section"
        for item in 0...count {
            let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
            let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
            if isSelected && userDefaults().string(forKey: objectTypeKeyName) == nil {
                userDefaults().setValue(key, forKey: objectTypeKeyName)
                userDefaults().setValue(se.section.id, forKey: objectIdKeyName)
                if sid < 0 {
                    let count = userDefaults().integer(forKey: "SharedDashboardCount")
                    userDefaults().set(count + 1, forKey: "SharedDashboardCount")
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            } else if !isSelected && userDefaults().string(forKey: objectTypeKeyName) == key && userDefaults().integer(forKey: objectIdKeyName) == se.section.id {
                if sid < 0 {
                    deleteSharedDashboard(item: item)
                } else {
                    userDefaults().setValue(nil, forKey: objectTypeKeyName)
                    userDefaults().setValue(0, forKey: objectIdKeyName)
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            }
        }
        self.topViewLayer.setIsFavorite(isFavorite: false)
        showErrorColtroller(message: strings.noEmptyCell)
    }
    
    func xDeviceNavigationBarSettingsViewTapped() {
        let controller = XSectionSettingsController(deviceId: deviceId.int64, sectionId: sectionId.int64)
        navigationController?.pushViewController(controller, animated: true)
    }
}

