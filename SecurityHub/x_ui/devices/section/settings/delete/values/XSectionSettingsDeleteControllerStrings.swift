//
//  XSectionSettingsDeleteControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XSectionSettingsDeleteViewStrings {
    
    /// Удалить раздел
    func title(sectionName: String) -> String { "N_DELETE_SECTION_TITLE_1".localized() + " \"\(sectionName)\"" }
    
    /// Раздел будет удален из памяти контроллера.\nРаздел нельзя удалить, если к нему привязаны датчики.
    var text: String { "N_DELETE_SECTION_MESSAGE".localized() }
    
    /// Удалить
    var confirm: String { "DELETE".localized() }
}

class XSectionSettingsDeleteControllerStrings {
    var errorAlertText = XAlertView.Strings(
        /// Команда не может быть выполнена.\nНомер зоны или раздела уже используется.
        title: "D3_SUCCEED".localized(),
        text: nil,
        positive: "OK".localized(),
        negative: nil
    )
}
