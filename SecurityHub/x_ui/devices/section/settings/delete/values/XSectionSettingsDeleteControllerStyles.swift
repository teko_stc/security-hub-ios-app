//
//  XSectionSettingsDeleteControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XSectionSettingsDeleteView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: Style.Label(
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            confirm: XZoomButton.Style(
                backgroundColor: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor.white
            )
        )
    }
}

extension XSectionSettingsDeleteController {
    func errorAlertStyle() -> XAlertViewStyle {
        return XAlertViewStyle(
            backgroundColor: UIColor.white,
            title: XAlertViewStyle.Lable(
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            positive: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
