//
//  XSectionSettingsDeleteViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSectionSettingsDeleteViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    
    let title: Label
    let text: Label
    
    let confirm: XZoomButtonStyle
}

extension XSectionSettingsDeleteView {
    typealias Style = XSectionSettingsDeleteViewStyle
}
