//
//  XSectionSettingsDeleteViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XSectionSettingsDeleteViewDelegate {
    func confirmTapped()
}
