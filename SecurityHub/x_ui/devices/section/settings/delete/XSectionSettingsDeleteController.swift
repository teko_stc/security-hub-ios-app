//
//  XSectionSettingsDeleteController.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XSectionSettingsDeleteController: XBaseViewController<XSectionSettingsDeleteView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private let strings: XSectionSettingsDeleteControllerStrings = XSectionSettingsDeleteControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceId, sectionId: Int64
    
    init(deviceId: Int64, sectionId: Int64) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getSectionInfo()
    }
    
    private func getSectionInfo() {
        _ = DataManager.shared.getSection(device: deviceId, section: sectionId)
            .subscribe(onNext: { section in
                self.mainView.setContent(name: section?.name ?? "")
            })
    }
}

extension XSectionSettingsDeleteController: XSectionSettingsDeleteViewDelegate {
    func confirmTapped() {
				DataManager.shared.removeAutoArm(device: deviceId, section: sectionId)
        guard DataManager.shared.dbHelper.getZones(device_id: deviceId, section_id: sectionId).count == 0 else { return showErrorColtroller(message: "error_need_delete_zone".localized()) }
        _ = doOnAvailable(deviceId: deviceId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["SECTION_DEL" : ["index" : self.sectionId]] ]) })
            .asObservable()
            .concatMap({ (result) -> Observable<Int64> in return Observable.timer(.seconds(1), scheduler: ConcurrentMainScheduler.instance) })
            .concatMap({ (result) -> Single<DCommandResult> in return DataManager.shared.hubHelper.setCommandWithResult(.ZONE_DEL, D: ["device_id" : self.deviceId, "device_section": self.sectionId, "zone": 0 ]) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] result in
                if (result.success) {
                    DataManager.shared.updateSites()
                    DataManager.shared.updateAffects()
                    
                    self?.navigationController?.popToRootViewController(animated: true)
                } else {
                    self?.showErrorColtroller(message: result.message)
                }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })
    }
    
    func showErrorDialog() {
        let alert = XAlertController(style: self.errorAlertStyle(), strings: self.strings.errorAlertText)
        self.navigationController?.present(alert, animated: true)
    }
}
