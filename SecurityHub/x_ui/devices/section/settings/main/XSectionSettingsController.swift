//
//  XSectionSettingsController.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSectionSettingsControllerStringsProtocol {
    var changeNameTitle: String { get }
    var noRight: String { get }
    var selectTypeTitle: String { get }
}

class XSectionSettingsController: XBaseViewController<XSectionSettingsView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private var strings: XSectionSettingsControllerStringsProtocol = XSectionSettingsControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceId, sectionId: Int64, isAdmin: Bool
    private var isSettingNeeded = false
		private var autoArm: DAutoArmEntity?
		private var autoArmNeedUpdate = false
    
    init(deviceId: Int64, sectionId: Int64) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        self.isAdmin = DataManager.shared.getUser().roles & Roles.ORG_ADMIN != 0 || DataManager.shared.getUser().roles & Roles.DOMEN_ADMIN != 0
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
		
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getSectionInfo()
    }
	
		override func viewWillAppear(_ animated: Bool) {
			super.viewWillAppear(animated)
			
			if autoArmNeedUpdate {
				autoArm = DataManager.shared.getAutoArm(device: deviceId, section: sectionId)
				mainView.setContent (autoarm: autoArm!)
				autoArmNeedUpdate = false
			}
		}
    
    private func getSectionInfo() {
        _ = DataManager.shared.getSectionsObserver()
            .filter({ $0.section.section == self.sectionId && $0.section.device == self.deviceId })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] se in
                guard let self = self else {
                    return
                }

								self.isSettingNeeded = HubConst.isSettingNeeded(se.configVersion, cluster: se.cluster_id)

                let type = DataManager.shared.d3Const.getSectionType(detector: se.section.detector.int)?.name ?? "No Type"
								self.mainView.setContent(name: se.section.name, type: type)

								if se.section.detector == HubConst.SECTION_TYPE_SECURITY && self.autoArmNeedUpdate == false {
									self.mainView.setLoading()
									self.getAutoArm (device: se.section.device, section: se.section.section, autoArmId: se.section.autoarmId) }
            })
    }
	
		private func getAutoArm (device: Int64, section: Int64, autoArmId: Int64) {
			_ = DataManager.shared.hubHelper.get(.AUTOARM_GET, D: ["device_id": device,"section": section])
					.observeOn(ThreadUtil.shared.mainScheduler)
					.subscribe(onSuccess: { [weak self] (result: DCommandResult, value: HubAutoArm?) in
							guard let self = self else { return }
							if result.success, let value = value {
								if ( autoArmId < 1 && value.runAt == 0 && value.active == 0 ) ||
										( autoArmId > 0 && (value.runAt + value.active) > 0 ) {
									self.autoArm = DAutoArmEntity (id: autoArmId, site: 0, device: device, section: section, runAt: value.runAt, active: value.active, daysOfWeek: value.daysOfWeek, hour: value.hour, minute: value.minute, timezone: value.timezone, lastExecuted: value.lastExecuted)
									self.mainView.setContent(autoarm: self.autoArm!)
									DataManager.shared.updateAutoArm(autoArm: self.autoArm!) }
							} else {
								self.showErrorColtroller(message: result.message)
							}
					}, onError: { [weak self] error in
							self?.showErrorColtroller(message: error.localizedDescription)
					})
		}
    
    private func change(value: Any, attr: String = "name") {
        _ = doOnAvailable(deviceId: self.deviceId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["SECTION_SET": [ "index": self.sectionId, attr: value ]]]) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onSuccess: { [weak self] result in
                if (!result.success) { self?.showErrorColtroller(message: result.message) }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })
    }
}

extension XSectionSettingsController: XSectionSettingsViewDelegate {
    func nameViewTapped(name: String) {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XTextEditController(title: strings.changeNameTitle, value: name, onCompleted: { vc, value in self.change(value: value); self.back(); })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func typeViewTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        guard let se = DataManager.shared.dbHelper.getSection(device_id: deviceId, section_id: sectionId) else { return }
        let style = XBottomSheetView.Style(backgroundColor: .white, title: XBottomSheetView.Style.Title(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5), aligment: .left), cell: XBottomSheetView.Cell.Style(backgroundColor: .white, selectedBackgroundColor: .white, title: XBottomSheetView.Cell.Style.Label(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20), select: UIColor.colorFromHex(0x3ab3ff))))
        let items = DataManager.shared.d3Const.getSectionTypes().filter({ $0.id != 4 && $0.id != 6 }).map({ XBottomSheetViewItem(title: $0.name, text: nil, any: $0) })
        let index = items.firstIndex(where: { ($0.any as! D3TypeModel).id == se.section.detector }) ?? -1
        let sheet = XBottomSheetController(style: style, title: strings.selectTypeTitle, selectedIndex: index, items: items)
        sheet.onSelectedItem = { vc, index, item in self.change(value: (item.any as! D3TypeModel).id, attr: "type"); vc.dismiss(animated: true) }
        navigationController?.present(sheet, animated: true)
    }
    
    func deleteTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XSectionSettingsDeleteController(deviceId: deviceId, sectionId: sectionId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func temperatureThresholdsViewTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = TemperatureThresholdsViewController(deviceId: deviceId.int, sectionId: sectionId.int, zoneId: nil)
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func autoarmViewTapped() {
			if autoArm == nil { return }
				
			if !isAdmin {
				showErrorColtroller(message: strings.noRight)
				return
			}
			
			guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
			
			let aac = XAutoArmController()
			aac.autoArm = autoArm
			navigationController?.pushViewController(aac, animated: true)
			autoArmNeedUpdate = true
		}
}
