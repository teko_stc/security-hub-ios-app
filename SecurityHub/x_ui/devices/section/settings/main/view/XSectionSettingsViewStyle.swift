//
//  XSectionSettingsStyle.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSectionSettingsViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    struct Button {
        let font: UIFont?
        let color: UIColor
        let selectedColor: UIColor
    }
    
    let backgroundColor: UIColor
    let cardBackground: UIColor
    
    let title: Label
    let name: Label
    let type: Label
    
    let delete: Button
}

extension XSectionSettingsView {
    typealias Style = XSectionSettingsViewStyle
}
