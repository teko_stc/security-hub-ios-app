//
//  XSectionSettingsView.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSectionSettingsView: UIView {
    
    private let strings: XSectionSettingsViewStrings = XSectionSettingsViewStrings()
    
    public var delegate: XSectionSettingsViewDelegate?
    
    private var cardView: UIView = UIView()
    private var nameView, temperatureThresholdsView, typeView, autoarmView: XEditButton!
    private var deleteButton: UIButton = UIButton()
		private var deleteButtonTopAnchor: NSLayoutConstraint!
	
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        cardView.backgroundColor = style.cardBackground
        cardView.layer.cornerRadius = 40
        addSubview(cardView)
        
        nameView = XEditButton(style: XEditButton.Style(header: XBaseLableStyle(color: style.title.color, font: style.title.font), title: XBaseLableStyle(color: style.name.color, font: style.name.font), _switch: nil))
        nameView.headerTitle = strings.name
        nameView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(nameView)
        
        typeView = XEditButton(style: XEditButton.Style(header: XBaseLableStyle(color: style.title.color, font: style.title.font), title: XBaseLableStyle(color: style.name.color, font: style.name.font), _switch: nil))
        typeView.headerTitle = strings.type
        typeView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(typeView)
        
        temperatureThresholdsView = XEditButton(
            style: XEditButtonStyle(
                header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20)),
                title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
            )
        )
        temperatureThresholdsView.title = "N_SECTION_TEMP_LIMITS_SET".localized()
        temperatureThresholdsView.headerTitle = "N_WIZ_SECTION_TEMP_LIMITS".localized()
        temperatureThresholdsView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(temperatureThresholdsView)
			
				autoarmView = XEditButton(style: XEditButton.Style(header: XBaseLableStyle(color: style.name.color, font: style.name.font), title: XBaseLableStyle(color: style.title.color, font: style.title.font), subTitle: XBaseLableStyle(color: style.title.color, font: style.title.font), _switch: nil))
				autoarmView.headerTitle = strings.autoarmTitle
				autoarmView.isMultiLine = true
				autoarmView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
				autoarmView.isHidden = true
				addSubview(autoarmView)
   
        deleteButton.setTitle(strings.delete, for: .normal)
        deleteButton.setTitleColor(style.delete.color, for: .normal)
        deleteButton.setTitleColor(style.delete.selectedColor, for: .highlighted)
        deleteButton.titleLabel?.font = style.delete.font
        deleteButton.contentHorizontalAlignment = .left
        deleteButton.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(deleteButton)
    }
    
    private func setConstraints() {
        [cardView, nameView, typeView, temperatureThresholdsView, autoarmView, deleteButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
			
				deleteButtonTopAnchor = deleteButton.topAnchor.constraint(equalTo: temperatureThresholdsView.bottomAnchor, constant: 20)
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            nameView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            nameView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            nameView.heightAnchor.constraint(equalToConstant: 54),

            typeView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 20),
            typeView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            typeView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            typeView.heightAnchor.constraint(equalToConstant: 54),
            
            temperatureThresholdsView.topAnchor.constraint(equalTo: typeView.bottomAnchor, constant: 20),
            temperatureThresholdsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            temperatureThresholdsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            temperatureThresholdsView.heightAnchor.constraint(equalToConstant: 54),
						
						autoarmView.topAnchor.constraint(equalTo: temperatureThresholdsView.bottomAnchor, constant: 20),
						autoarmView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
						autoarmView.heightAnchor.constraint(equalToConstant: 60),
						autoarmView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),

						deleteButtonTopAnchor,
            deleteButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            
            cardView.bottomAnchor.constraint(equalTo: deleteButton.bottomAnchor, constant: 36)
        ])
    }
    
    public func setContent(name: String, type: String) {
        nameView.title = name
        typeView.title = type
    }
	
		public func setLoading() {
			if autoarmView.isHidden {
				removeConstraint(deleteButtonTopAnchor)
				deleteButtonTopAnchor = deleteButton.topAnchor.constraint(equalTo: autoarmView.bottomAnchor, constant: 20)
				addConstraint(deleteButtonTopAnchor)
				autoarmView.isHidden = false }
			
			autoarmView.subTitle = strings.autoarmLoading
		}
	
		public func setContent(autoarm: DAutoArmEntity) {
			if autoarm.id < 1 {
				autoarmView.subTitle = strings.autoarmDisable
				return
			}
			
			if autoarm.active == 0
			{
				let date = Date(timeIntervalSince1970: Double(autoarm.runAt))
				let dateFormatter = DateFormatter()
				dateFormatter.dateFormat = "dd.MM.yy HH:mm"
				dateFormatter.timeZone = .current
				autoarmView.subTitle = dateFormatter.string(from: date)
				return
			}
			
			var schedule = strings.autoarmSchedule
			var bitMask: Int64 = 1
			var calendar = Calendar(identifier: .gregorian)
			calendar.locale = Locale (identifier: DataManager.defaultHelper.language)
			var daysOfWeek = calendar.shortWeekdaySymbols
			daysOfWeek.append(daysOfWeek[0])
			daysOfWeek.remove(at: 0)
			for item in daysOfWeek {
				if (autoarm.daysOfWeek & bitMask) != 0 {
					schedule.append(" \(item.capitalized),")
				}
				bitMask <<= 1
			}
			schedule.removeLast()
			schedule.append(String(format:" %02d:%02d",autoarm.hour,autoarm.minute))
			autoarmView.subTitle = schedule
		}
    
    @objc private func viewTapped(view: UIView) {
        switch view {
        case nameView: delegate?.nameViewTapped(name: nameView.title ?? "")
        case typeView: delegate?.typeViewTapped()
        case deleteButton: delegate?.deleteTapped()
        case temperatureThresholdsView: delegate?.temperatureThresholdsViewTapped()
				case autoarmView: delegate?.autoarmViewTapped()
        default: return
        }
    }
}
