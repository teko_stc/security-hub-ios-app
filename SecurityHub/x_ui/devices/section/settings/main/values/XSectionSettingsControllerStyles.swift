//
//  XSectionSettingsControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XSectionSettingsView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
            cardBackground: UIColor.white,
            title: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            name: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            type: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            delete: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1),
                selectedColor: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 0.5)
            )
        )
    }
}
