//
//  XSectionSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XSectionSettingsViewStrings {
    /// Имя
    var name: String { "N_SECTION_SETTINGS_NAME".localized() }
    
    /// Тип
    var type: String { "N_SECTION_SETTINGS_TYPE".localized() }
    
    /// Удалить
    var delete: String { "N_SECTION_SETTINGS_DELETE".localized() }
	
		/// Автовзятие
		var autoarmTitle: String { "SITE_AUTOARM_TITLE".localized() }
		var autoarmDisable: String { "SITE_AUTOARM_NO_SET".localized() }
		var autoarmSchedule: String { "SITE_AUTOARM_ON_SCHEDULE".localized() }
		var autoarmLoading: String { "SITE_AUTOARM_LOADING".localized() }
}

class XSectionSettingsControllerStrings: XSectionSettingsControllerStringsProtocol {
    /// Название раздела
    var changeNameTitle: String { "N_TITLE_SECTION_NAME".localized() }
    
    /// Недостаточно полномочий
    var noRight: String { "N_NO_PERMISSION".localized() }
    
    /// Выберите тип
    var selectTypeTitle: String { "N_WIZ_SECTION_SELECT_TYPE".localized() }
}
