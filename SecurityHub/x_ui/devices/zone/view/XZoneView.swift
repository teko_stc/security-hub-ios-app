//
//  XZoneView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit



class XZoneView: UIView {
    private var scrollView: UIScrollView = UIScrollView()
    private var contentView = UIView()
    var topView: XDeviceTopView!
    var bottomView: XDeviceBottomView!
    
    lazy var strings: XZoneViewStrings = XZoneViewStrings()
    lazy var style: Style = styles()
    
    public var delegate: XZoneViewDelegate? {
        didSet {
            topView.delegate = delegate
        }
    }
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.contentSize = contentView.frame.size
    }
    
    private func initViews() {
        backgroundColor = style.backgroundColor
        
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        topView = XDeviceTopView(scriptButtonStyle: style.scriptButton)
        topView.font = style.topViewFont
        contentView.addSubview(topView)
        
        bottomView = XDeviceBottomView(cellStyle: style.bottom)
        contentView.addSubview(bottomView)
    }
    
    private func setConstraints() {
        [scrollView, contentView, topView, bottomView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),

            topView.topAnchor.constraint(equalTo: contentView.topAnchor),
            topView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            topView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 20),
            bottomView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            bottomView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            bottomView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30)
        ])
    }
}
