//
//  XZoneBottomViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XZoneViewBottomViewLayer {
    func setModelType(name: String)
    func setModel(name: String, icon: String)
    func setAlarmType(alarmName: String, detectorName: String)
    func setZone(io: Int)
    func setZone(id: Int)
    func setSection(name: String)
    func setDevice(name: String)
    func setSite(name: String)
    func build()
}

extension XZoneView: XZoneViewBottomViewLayer {
    func setModelType(name: String) { bottomView.setModelType(name: name) }
    
    func setModel(name: String, icon: String) { bottomView.setModel(name: name, icon: icon) }
    
    func setAlarmType(alarmName: String, detectorName: String) { bottomView.setAlarmType(alarmName: alarmName, detectorName: detectorName) }
    
    func setZone(io: Int) { bottomView.setZone(io: io) }
    
    func setZone(id: Int) { bottomView.setZone(id: id) }
    
    func setSection(name: String) { bottomView.setSection(name: name) }
    
    func setDevice(name: String) { bottomView.setDevice(name: name) }
    
    func setSite(name: String) { bottomView.setSite(name: name) }
    
    func build() { bottomView.build() }
}
