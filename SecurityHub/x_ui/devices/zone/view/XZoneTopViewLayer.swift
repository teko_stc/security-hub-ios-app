//
//  XZoneTopViewLayer.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

enum XZoneViewLayerDeviceType {
    case security, technical, fire, alarm, manual_relay, auto_relay
}

protocol XZoneTopViewLayer {
    func setTitle(title: String)
    func setIsFavorite(isFavorite: Bool)
    func setIcon(name: String, isRenderTemplate: Bool)
    func setDeviceState(type: XZoneViewLayerDeviceType?, isActionActive: Bool?, extraActions: [String:Int64]?)
    func setDeviceState(isHaveScript: Bool, scriptName: String?)
    func setDeviceState(isHaveConnection: Bool)
    func setDeviceStatesList(items: [Int:String])
    func setDeviceState(isHidden: Bool, level: Int?, isHaveBattery: Bool, isBatteryError: Bool)
    func setDeviceState(isSignalLevelHidden: Bool, level: Int?)
    func setDeviceState(isTemperatureHidden: Bool, celsius: Int?)
    func setDeviceState(isTemperature2Hidden: Bool, celsius: Int?)
    func setDeviceState(isUpdateHidden: Bool)
    func setDeviceState(isDelayHidden: Bool, isDelayArm: Bool)
		func setDeviceState(isBypassHidden: Bool, isBypassArm: Bool)
    
    func setAlarm()
    func setAttention()
    func setOffline()
    func setDefault()
}

extension XZoneView: XZoneTopViewLayer {
    func setTitle(title: String) { topView.setNavigationViewTitle(title: title) }
    
    func setIsFavorite(isFavorite: Bool) { topView.setNavigationViewIsFavoriteDefault(isFavoritDevice: isFavorite) }
    
    func setIcon(name: String, isRenderTemplate: Bool) { topView.setIconViewImage(name: name, renderMode: isRenderTemplate ? .alwaysTemplate : .alwaysOriginal) }
 
    func setDeviceState(type: XZoneViewLayerDeviceType?, isActionActive: Bool?, extraActions: [String:Int64]?) {
        switch type {
        case .security:
            topView.setActionType(nil)
            if isActionActive == true { topView.setState(state: .security_armed) }
            else { topView.setState(state: .security_disarmed) }
            break;
        case .technical:
            topView.setActionType(nil)
            if isActionActive == true { topView.setState(state: .technical_armed) }
            else { topView.setState(state: .technical_disarmed) }
            break;
        case .fire, .alarm:
            if isActionActive == true { topView.setActionType(.reset) }
            else { topView.setActionType(nil) }
            topView.setState(state: nil)
            break;
        case .manual_relay:
            topView.setActionType(.relay)
            if isActionActive == true { topView.setState(state: .relay_on) }
            else { topView.setState(state: .relay_off) }
            topView.setStateManualRelay()
            break;
        case .auto_relay:
            topView.setActionType(.script, extraActions: extraActions)
//            if isActionActive == true { topView.setState(state: .relay_on) }
//            else { topView.setState(state: .relay_off) }
            topView.setState(state: nil)
            topView.setStateAutoRelay()
            break;
        case .none:
            topView.setActionType(nil)
            topView.setState(state: nil)
            break;
        }
    }
        
    func setDeviceState(isHaveScript: Bool, scriptName: String?) {
        topView.setState(isHaveScript: isHaveScript)
        if isHaveScript { topView.setScriptActionDefaults(lable: strings.scriptInfoText(name: scriptName), button: strings.scriptInfoTextButton) }
        else { topView.setScriptActionDefaults(lable: strings.scriptAddText, button: strings.scriptAddTextButton) }
    }
    
    func setDeviceStatesList(items: [Int:String]) { topView.setStatesListItems(items: items.map({ XDeviceTopView.Cell.Model(eventId: $0.key, description: $0.value) })) }
    
    func setDeviceState(isHaveConnection: Bool) { topView.setState(isHaveConnection: isHaveConnection) }
    
    func setDeviceState(isHidden: Bool, level: Int?, isHaveBattery: Bool, isBatteryError: Bool) { topView.setState(isHidden: isHidden, level: level, isHaveBattery: isHaveBattery, isBatteryError: isBatteryError) }
    
    public func setDeviceState(isSignalLevelHidden: Bool, level: Int?) { topView.setState(isSignalLevelHidden: isSignalLevelHidden, level: level) }
    
    public func setDeviceState(isTemperatureHidden: Bool, celsius: Int?) { topView.setState(isTemperatureHidden: isTemperatureHidden, celsius: celsius) }

    public func setDeviceState(isTemperature2Hidden: Bool, celsius: Int?) { topView.setState(isTemperature2Hidden: isTemperature2Hidden, celsius: celsius) }
    
    public func setDeviceState(isUpdateHidden: Bool) {  topView.setState(isUpdateHidden: isUpdateHidden) }
    
    public func setDeviceState(isDelayHidden: Bool, isDelayArm: Bool) {  topView.setState(isDelayHidden: isDelayHidden, isDelayArm: isDelayArm) }
	
		public func setDeviceState(isBypassHidden: Bool, isBypassArm: Bool) {
			topView.setState(isBypassHidden: isBypassHidden)
			if isBypassArm { topView.setState(state: .security_bypass) }
		}


    func setAlarm() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.alarm.cardBackground
        topView.cardTextColor = style.alarm.cardText
        topView.cardTintColor = style.alarm.cardTint
        
        topView.actionTintColor = style.alarm.cardTint
        topView.actionCardColor = style.alarm.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setAttention() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.warning.cardBackground
        topView.cardTextColor = style.warning.cardText
        topView.cardTintColor = style.warning.cardTint
        
        topView.actionTintColor = style.warning.cardTint
        topView.actionCardColor = style.warning.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setOffline() {
        topView.setIconStateRenderMode(mode: .alwaysTemplate)
        topView.cardColor = style.connectionLost.cardBackground
        topView.cardTextColor = style.connectionLost.cardText
        topView.cardTintColor = style.connectionLost.cardTint
        
        topView.actionTintColor = style.connectionLost.cardTint
        topView.actionCardColor = style.connectionLost.cardBackground
        topView.actionImageRenderMode = .alwaysTemplate
    }
    
    func setDefault() {
        topView.setIconStateRenderMode(mode: .alwaysOriginal)
        topView.cardColor = style._default.cardBackground
        topView.cardTextColor = style._default.cardText
        topView.cardTintColor = style._default.cardTint

        topView.actionTintColor = style._default.actionTintColor
        topView.actionCardColor = style._default.actionCardBackground
        topView.actionImageRenderMode = .alwaysOriginal
    }
}
