//
//  XZoneViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 02.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XZoneViewStyle {
    struct State {
        let cardBackground, cardTint, cardText, actionCardBackground, actionTintColor: UIColor
    }
    
    let backgroundColor: UIColor
    let topViewFont: UIFont?
    let alarm, warning, connectionLost, _default: State
    let scriptButton: XZoomButton.Style
    
    let bottom: XDeviceBottomView.Cell.Style
}
extension XZoneView {
    typealias Style = XZoneViewStyle
}
