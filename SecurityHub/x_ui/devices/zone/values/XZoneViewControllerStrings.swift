//
//  XZoneViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 02.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XZoneControllerStrings : XCommandStringsProtocol {
    /// Потеряна связь с контроллером
    var offlineAlertTitle = "N_CONTROLLER_OFFLINE".localized()
    
    /// Элемент системы на связи
    var onlineAlertTitle = "N_SYSTEM_PART_ONLINE".localized()
		
    var connectionAlertPositive = "N_BUTTON_OK".localized()
    
    /// На главном экране больше нет свободных ячеек
    var noEmptyCell: String = "N_ERROR_NO_SPACE_ON_MAIN".localized()
    
    /// На элементе системы установлена задержка на вход/выход
    var delayTitle: String { "N_STATE_DELAY_MESSAGE".localized() }
	
		/// Данная зона может быть взята с обходом
		var bypassTitle: String { "N_STATE_BYPASS_MESSAGE".localized() }
    
    /// Элимент системы находится в состоянии задержки на выход (общая продолжительность 60 секунд)
    var delayActiveTitle: String { "N_STATE_DELAY_ARM_MESSAGE_ZONE".localized() }
    
    /// Команда отправлена
    var commadSend: String { "COMMAND_BEEN_SEND".localized() }
}

class XZoneViewStrings {
    /// Сценарий не найден
    var scriptAddText: String { "D3_ERR_DEVICE_SCRIPT_NOT_FOUND".localized() }
    
    /// Добавить
    var scriptAddTextButton: String { "N_BUTTON_ADD".localized() }
    
    /// Подробнее
    var scriptInfoTextButton: String { "N_TOP_INFO_SCRIPT_INFO".localized() }
    
    /// Сценарий
    func scriptInfoText(name: String?) -> String { "N_WIZARD_ALL_SCRIPT".localized() + " \"\(name ?? "")\"" }
}
