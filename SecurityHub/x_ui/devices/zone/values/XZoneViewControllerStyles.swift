//
//  XZoneViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 02.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XZoneView {
    func styles() -> Style {
        Style(
            backgroundColor: UIColor(red: 0xee/255, green: 0xee/255, blue: 0xee/255, alpha: 1),
            topViewFont: UIFont(name: "Open Sans", size: 23),
            alarm: Style.State(
                cardBackground: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1),
                cardTint: UIColor.white,
                cardText: UIColor.white,
                actionCardBackground: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1),
                actionTintColor: UIColor.white
            ),
            warning: Style.State(
                cardBackground: UIColor(red: 0xff/255, green: 0xdf/255, blue: 0x57/255, alpha: 1),
                cardTint: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor(red: 0xff/255, green: 0xdf/255, blue: 0x57/255, alpha: 1),
                actionTintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            connectionLost: Style.State(
                cardBackground: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
                cardTint: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
                actionTintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            _default: Style.State(
                cardBackground: UIColor.white,
                cardTint: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                cardText: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                actionCardBackground: UIColor.white,
                actionTintColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            scriptButton: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.white
            ),
            bottom: XDeviceBottomView.Cell.Style(
                backgroundColor: UIColor(red: 0xee/255, green: 0xee/255, blue: 0xee/255, alpha: 1),
                tintColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 12.5),
                    alignment: .left
                ),
                text: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    alignment: .left
                )
            )
        )
    }
}

extension XZoneController {
    func messageAlert(_ isOffline: Bool) -> XAlertViewStyle {
        return XAlertViewStyle(
            backgroundColor: UIColor.white,
            title: XAlertViewStyle.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: isOffline ? XAlertViewStyle.Lable(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ) : nil,
            positive: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1)
            )
        )
    }
}

