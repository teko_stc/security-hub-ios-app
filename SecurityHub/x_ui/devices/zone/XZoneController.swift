//
//  XZoneController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XZoneController: XBaseViewController<XZoneView> {
    override var tabBarIsHidden: Bool { true }
    
    private let strings: XZoneControllerStrings = XZoneControllerStrings()
    
    private lazy var viewLayer: XZoneTopViewLayer = self.mainView
    private lazy var viewBottomLayer: XZoneViewBottomViewLayer = self.mainView
    
    private var deviceId, sectionId, zoneId: Int
    private var deviceDisposable: Disposable?
    private var observerEvents: Any?
    
    init(deviceId: Int, sectionId: Int, zoneId: Int) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        self.zoneId = zoneId
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        mainView.delegate = self
//        mainView.topView.isHiddenSettings = !DataManager.shared.dbHelper.isHub(device_id: deviceId.int64)

        deviceDisposable = DataManager.shared.getZoneObserver(device_id: deviceId.int64, section_id: sectionId.int64, zone_id: zoneId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] de in
                UIView.performWithoutAnimation { [de] in
                    self?.setZone(de: de)
                }
            })
    }
    
    private func setZone(de: DZoneEntity) {
        let icon = DataManager.shared.d3Const.getSensorIcons(uid: de.zoneUid.int, detector: de.zoneDetector.int)

        self.viewLayer.setTitle(title: de.zone.name)
        self.viewLayer.setIsFavorite(isFavorite: false)
        self.viewLayer.setIcon(name: icon.big, isRenderTemplate: false)
        
        var isBatteryHidden = true, isSignalLevelHidden = true, isTemperatureHidden = true, isTemperature2Hidden = true
        let model = DataManager.shared.d3Const.getSensorType(uid: de.zoneUid.int)
        if (model == HubConst.ZONETYPE_8731) { isSignalLevelHidden = false }
        else if (model != HubConst.SENSORTYPE_WIRED_OUTPUT && model != HubConst.SENSORTYPE_WIRED_INPUT) { isBatteryHidden = false; isSignalLevelHidden = false; isTemperatureHidden = false }
        if (model == HubConst.ZONETYPE_3731) { isTemperature2Hidden = false }
        
        if (de.sectionDetector == HubConst.SECTION_TYPE_SECURITY || de.sectionDetector == 0) {
            self.viewLayer.setDeviceState(type: .security, isActionActive: de.armStatus == .armed, extraActions: nil)
        } else if (de.sectionDetector == HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED) {
            self.viewLayer.setDeviceState(type: .technical, isActionActive: de.armStatus == .armed, extraActions: nil)
        } else {
            self.viewLayer.setDeviceState(type: .alarm, isActionActive: DataManager.shared.dbHelper.isHaveAlarmTenMinAgo(deviceId: self.deviceId.int64, sectionId: self.sectionId.int64, zoneId: self.zoneId.int64), extraActions: nil)
            if de.type == .insert {
                self.observerEvents = DataManager.shared.nCenter.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main, using: self.updateResetState)
            }
        }
       
        if (de.zoneDetector & 0xff == HubConst.DETECTOR_AUTO_RELAY) {
            self.viewLayer.setDeviceState(type: .auto_relay, isActionActive:de.armStatus == .armed, extraActions: de.script?.getCommands())
            self.viewLayer.setDeviceState(isHaveScript: de.script != nil, scriptName: de.script?.name)
            isTemperatureHidden = true
        }
        if (de.zoneDetector & 0xff == HubConst.DETECTOR_MANUAL_RELAY) {
            self.viewLayer.setDeviceState(type: .manual_relay, isActionActive: de.armStatus == .armed, extraActions: nil)
            isTemperatureHidden = true
        }
        
        let sid = DataManager.settingsHelper.needSharedMain ? -777 : de.siteIds[0]
        let key = (de.zone.detector & 0xff == HubConst.DETECTOR_AUTO_RELAY) || (de.zone.detector & 0xff == HubConst.DETECTOR_MANUAL_RELAY) ? "relay" : "zone"
        let count = DataManager.settingsHelper.needSharedMain ? 40 : 5
        for item in 0...count {
            let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
            let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
            if userDefaults().string(forKey: objectTypeKeyName) == key && de.zone.id == userDefaults().string(forKey: objectIdKeyName) {
                self.viewLayer.setIsFavorite(isFavorite: true)
            }
        }

        var isDualSim = false, isSimNoSignal = false, isHaveSim = false, simLevel: Int?, isSim2NoSignal = false, isHaveSim2 = false, sim2Level: Int?, isLanNo = false, isSocketNo = false
        var isAlarm = false, isAttention = false, isOffline = false, isBatteryNo = false, isBatteryError = false, batteryLevel: Int?, signalLevel: Int?, celsius: Int?, isArmDelay = false, isArmBypass = false, isNeedUpdate = false
        var affects: [Int: String] = [:]
        for affect in de.affects {
            switch affect._class {
            case HubConst.CLASS_ALARM:
                isAlarm = true
                affects[affect.id.int] = affect.desc
                let affect_icon = DataManager.shared.d3Const.getEventIcons(_class: affect._class.int, detector: affect.detector.int, reason: affect.reason.int, statment: affect.active.int)
                self.viewLayer.setIcon(name: affect_icon.big, isRenderTemplate: true)
                break;
            case HubConst.CLASS_SABOTAGE, HubConst.CLASS_ATTENTION:
                isAttention = true
                affects[affect.id.int] = affect.desc
                break
            case HubConst.CLASS_MALFUNCTION:
                if affect.reason == HubConst.REASON_MALF_LOST_CONNECTION { isOffline = true }
                else if affect.reason == HubConst.REASON_MAFL_BATT_NO { isBatteryNo = true; isAttention = true; affects[affect.id.int] = affect.desc }
                else { isAttention = true; affects[affect.id.int] = affect.desc }
                break;
            case HubConst.CLASS_SUPPLY:
                if affect.reason == HubConst.REASON_SOCKET_FAULT { isSocketNo = true }
                else if affect.reason == HubConst.REASON_BATTERY_LEVEL, let _level = affect.jdata["%"] as? Int { batteryLevel = _level }
                else if affect.reason == HubConst.REASON_BATTERY_ERROR { isBatteryError = true }
                break;
            case HubConst.CLASS_CONNECTIVITY:
                if affect.reason == HubConst.REASON_CONNECTIVITY_UNAVAILABLE {
                    if(affect.detector == HubConst.DETECTOR_MODEM){ isSimNoSignal = true }
                    else if (affect.detector == HubConst.DETECTOR_GPRS_1) { isDualSim = true; isSimNoSignal = true }
                    else if (affect.detector == HubConst.DETECTOR_GRPS_2){ isDualSim = true; isSim2NoSignal = true }
                    else if (affect.detector == HubConst.DETECTOR_ETHERNET){ isLanNo = true }
                } else if affect.reason == HubConst.REASON_CONNECTIVITY_LEVEL {
                    if affect.detector == HubConst.DETECTOR_MODEM, let bearer = affect.jdata["bearer"] as? Int {
                        if bearer == 2, let _level = affect.jdata["%"] as? Int { sim2Level = _level }
                        else if let _level = affect.jdata["%"] as? Int { simLevel = _level }
                    }
                    else if affect.detector == HubConst.DETECTOR_GPRS_1, let _level = affect.jdata["%"] as? Int { simLevel = _level }
                    else if affect.detector == HubConst.DETECTOR_GRPS_2, let _level = affect.jdata["%"] as? Int { sim2Level = _level }
                    else if let _level = affect.jdata["%"] as? Int { signalLevel = _level }
                } else if affect.reason == HubConst.REASON_CONNECTIVITY_IMSI {
                    if affect.detector == HubConst.DETECTOR_MODEM, let bearer = affect.jdata["bearer"] as? Int {
                        if bearer == 2 { isHaveSim2 = true } else { isHaveSim = true }
                    }
                    else if affect.detector == HubConst.DETECTOR_GRPS_2 { isHaveSim2 = true }
                    else if affect.detector == HubConst.DETECTOR_GPRS_1 { isHaveSim = true }
                }
                break;
            case HubConst.CLASS_INFORMATION:
                if affect.reason == HubConst.REASON_INFORMATION_UPDATE_READY { isNeedUpdate = true }
                else if affect.reason == HubConst.REASON_INFORMATION_ARM_DELAY { isArmDelay = true }
                break;
            case HubConst.CLASS_TELEMETRY:
                if affect.reason == HubConst.REASON_TEMPERATURE, let _celsius = affect.jdata["celsius"] as? Int { celsius = _celsius }
                //TODO Zone id > 100
                break;
						case HubConst.CLASS_CONTROL:
								if de.sectionDetector == HubConst.SECTION_TYPE_SECURITY &&
										affect.reason == HubConst.REASON_CONTROL_ARM_BYPASS { isArmBypass = true }
								break;
            default:
                break;
            }
        }
        
        if isAlarm { self.viewLayer.setAlarm() }
        else if isAttention { self.viewLayer.setAttention() }
        else if isOffline { self.viewLayer.setOffline() }
        else { self.viewLayer.setDefault() }
				
        self.viewLayer.setDeviceState(isHaveConnection: !isOffline)
        self.viewLayer.setDeviceState(isDelayHidden: !(de.zone.delay > 0 || isArmDelay), isDelayArm: isArmDelay)
				self.viewLayer.setDeviceState(isBypassHidden: !(de.zone.bypass > 0 || isArmBypass), isBypassArm: isArmBypass)
        self.viewLayer.setDeviceState(isUpdateHidden: !isNeedUpdate)
        self.viewLayer.setDeviceState(isHidden: isBatteryHidden, level: batteryLevel, isHaveBattery: !isBatteryNo, isBatteryError: isBatteryError)
        self.viewLayer.setDeviceState(isSignalLevelHidden: isSignalLevelHidden, level: signalLevel)
        self.viewLayer.setDeviceState(isTemperatureHidden: isTemperatureHidden, celsius: celsius)
        self.viewLayer.setDeviceState(isTemperature2Hidden: isTemperature2Hidden, celsius: nil) //TODO
                        
        self.viewLayer.setDeviceStatesList(items: affects)
        
        if let sensorInfo = DataManager.shared.d3Const.getSencorInfo(uid: de.zoneUid.int, detector: de.zoneDetector.int) {
            self.viewBottomLayer.setModel(name: sensorInfo.model.name, icon: sensorInfo.model.icons.list)
            if let sensorTypeName = sensorInfo.typeDescription {
                self.viewBottomLayer.setModelType(name: sensorTypeName)
            }
            if let alartDescription = sensorInfo.alartDescription, let detectorDescription = sensorInfo.detectorDescription { self.viewBottomLayer.setAlarmType(alarmName: detectorDescription, detectorName: alartDescription)
            }
        }
        
        if model == HubConst.SENSORTYPE_WIRED_INPUT || model == HubConst.SENSORTYPE_WIRED_OUTPUT {
            self.viewBottomLayer.setZone(io: de.zoneUid.int >> 8 & 0xff)
        }
        
        self.viewBottomLayer.setZone(id: de.zoneId.int)
        if de.sectionId != 0 { self.viewBottomLayer.setSection(name: de.sectionName) }
        self.viewBottomLayer.setDevice(name: de.deviceName)
        self.viewBottomLayer.setSite(name: de.siteNames )
        self.viewBottomLayer.build()
        
        _ = isDualSim && isSimNoSignal && isHaveSim && simLevel == nil && isSim2NoSignal && isHaveSim2 && sim2Level == nil && isLanNo && isSocketNo
    }
    
    @objc private func updateResetState(notification: Notification) {
        guard let event = notification.object as? (event: Events, type: NUpdateType) else { return }
        guard event.event.device == deviceId && event.event.section == sectionId && event.event._class == HubConst.CLASS_ALARM && event.event.active == 1 else { return }
        viewLayer.setDeviceState(type: .alarm, isActionActive: true, extraActions: nil)
    }
    
    override func viewDidDestroy() {
        super.viewDidDestroy()
        deviceDisposable?.dispose()
    }
    
    private func onCompletable(deviceId: Int, sectionId: Int, zoneId: Int) -> Completable {
        showToast(message: strings.commadSend)
        return DataManager.shared.switchRelay(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64, state: 1)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result, type: .relay) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func offCompletable(deviceId: Int, sectionId: Int, zoneId: Int) -> Completable {
        showToast(message: strings.commadSend)
        return DataManager.shared.switchRelay(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64, state: 0)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result, type: .relay) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func resetCompletable(deviceId: Int, sectionId: Int, zoneId: Int) -> Completable {
        showToast(message: strings.commadSend)
        return DataManager.shared.reset(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onSuccess: { res in if (res.success) { DataManager.shared.dbHelper.dropLastAlarm(device_id: self.deviceId.int64, section_id: self.sectionId.int64, zone_id: self.zoneId.int64) }})
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result, type: .reset) })
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .asCompletable()
    }
    
    private func resultProcessing(_ result: DCommandResult, type: XDeviceTopView.ActionType?) {
        if result.success, type == .reset {
            DataManager.shared.nCenter.post(name: NSNotification.Name.init("resetZone"), object: nil)
            return viewLayer.setDeviceState(type: .none, isActionActive: false, extraActions: nil)
        }
        if result.success { return }
        showToast(message: result.message)
    }
}

extension XZoneController: XZoneViewDelegate {
    func xScriptButtonViewTapped(value: Int) {
        showToast(message: strings.commadSend)
        _ = DataManager.shared.switchRelay(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64, state: value.int64)
            .subscribeOn(MainScheduler())
            .do(onSuccess: { [weak self] result in self?.resultProcessing(result, type: nil)})
            .do(onError: { [weak self] error in self?.errorProcessing(error) })
            .subscribe()
    }
    
    func xStateItemTapped(type: StateType) {
        if type == .connectionState {
            _ = DataManager.shared.getZoneObserver(device_id: deviceId.int64, section_id: sectionId.int64, zone_id: zoneId.int64)
                .subscribeOn(ThreadUtil.shared.backScheduler)
                .filter {$0.deviceId == self.deviceId && $0.zoneId == self.zoneId}
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { device in
                    var isOffline = false, affectTime = ""
                    
                    if let affect = device.affects.first(where: { $0._class == HubConst.CLASS_MALFUNCTION && $0.reason == HubConst.REASON_MALF_LOST_CONNECTION }) {
                        isOffline = true
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm dd.MM"
                        affectTime = dateFormatter.string(from: Date(timeIntervalSince1970: Double(affect.time)))
                    }
                    
                    let alert = XAlertController(
                        style: self.messageAlert(isOffline),
                        strings: XAlertViewStrings(
                            title: isOffline ? self.strings.offlineAlertTitle : self.strings.onlineAlertTitle,
                            text: isOffline ? affectTime : nil,
                            positive: self.strings.connectionAlertPositive
                        )
                    )
                    self.navigationController?.present(alert, animated: true)
                })
        } else if [StateType.battery, StateType.connection, StateType.connectionLevel, StateType.temperature].contains(type) {
            let controller = XDevicesStateController(deviceId: deviceId.int64, zoneId: zoneId.int64, stateType: type)
            navigationController?.pushViewController(controller, animated: true)
        } else if type == .delay {
            showErrorColtroller(message: strings.delayTitle)
        } else if type == .delayActive {
            showErrorColtroller(message: strings.delayActiveTitle)
        } else if type == .bypass {
					showErrorColtroller(message: strings.bypassTitle)
 				}
    }
    
    func xDeviceTopViewStateListViewItemTapped(model: XDeviceTopView.Cell.Model) {
        guard let event = DataManager.shared.dbHelper.get(event_id: model.eventId.int64),
              let ze = DataManager.shared.dbHelper.getZone(device_id: event.device, section_id: event.section, zone_id: event.zone) else { return }
        let z = DataManager.shared.dbHelper.getCameraEvent(eventId: model.eventId.int64)
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: event.id, icon: event.icon, description: event.affect_desc, record: z?.record, time: event.time, names: XCameraEventViewLayerModel.Names(site: ze.siteNames, device: ze.deviceName, section: ze.sectionName, zone: ze.zone.name),location: nil))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func xLeftActionViewTapped(type: XDeviceTopView.ActionType?) {
        if (type == .reset) {
            _ = resetCompletable(deviceId: deviceId, sectionId: sectionId, zoneId: zoneId).subscribe()
        } else if (type == .relay) {
            _ = onCompletable(deviceId: deviceId, sectionId: sectionId, zoneId: zoneId).subscribe()
        } else if (type == .script) {
            guard let script = DataManager.shared.dbHelper.getScript(device_id: deviceId.int64, zone_id: zoneId.int64) else {
                let vc = XSelectScriptTypeController(device_ids: [self.deviceId.int64])
                navigationController?.pushViewController(vc, animated: true)
                return
            }
            guard let scriptsLibrary = DataManager.shared.scriptsLibrary[script.configVersion], let library = scriptsLibrary.items.first(where: { $0.uid == script.uid })?.data else {
                _ = DataManager.shared.getLibraryScripts(config_version: script.configVersion).subscribe()
                return
            }
            let vc = XAddScriptController(device_id: script.deviceId, script: library, params: script.params, name: script.name, bind: script.bind)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func xRightActionViewTapped(type: XDeviceTopView.ActionType?) {
        if (type == .relay) {
            _ = offCompletable(deviceId: deviceId, sectionId: sectionId, zoneId: zoneId).subscribe()
        }
    }
    
    func xDeviceNavigationBarBackViewTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    func xDeviceNavigationBarFavoritViewTapped(isSelected: Bool) {
        guard let ze = DataManager.shared.dbHelper.getZone(device_id: deviceId.int64, section_id: sectionId.int64, zone_id: zoneId.int64) else { return }
        let sid = DataManager.settingsHelper.needSharedMain ? -777 : ze.siteIds[0]
        let count = DataManager.settingsHelper.needSharedMain ? 40 : 5
        let key = (ze.zone.detector & 0xff == HubConst.DETECTOR_AUTO_RELAY) || (ze.zone.detector & 0xff == HubConst.DETECTOR_MANUAL_RELAY) ? "relay" : "zone"
        for item in 0...count {
            let objectTypeKeyName = "DashboardItemType_\(sid)_\(item)"
            let objectIdKeyName = "DashboardItemId_\(sid)_\(item)"
            if isSelected && userDefaults().string(forKey: objectTypeKeyName) == nil {
                userDefaults().setValue(key, forKey: objectTypeKeyName)
                userDefaults().setValue(ze.zone.id, forKey: objectIdKeyName)
                if sid < 0 {
                    let count = userDefaults().integer(forKey: "SharedDashboardCount")
                    userDefaults().set(count + 1, forKey: "SharedDashboardCount")
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            } else if !isSelected && userDefaults().string(forKey: objectTypeKeyName) == key && userDefaults().string(forKey: objectIdKeyName) == ze.zone.id {
                if sid < 0 {
                    deleteSharedDashboard(item: item)
                } else {
                    userDefaults().setValue(nil, forKey: objectTypeKeyName)
                    userDefaults().setValue(0, forKey: objectIdKeyName)
                }
                return DataManager.shared.nCenter.post(name: NSNotification.Name("updateDashboard"), object: nil)
            }
        }
        self.viewLayer.setIsFavorite(isFavorite: false)
        showErrorColtroller(message: strings.noEmptyCell)
    }
    
    func xDeviceNavigationBarSettingsViewTapped() {
        let controller = XZoneSettingsController(deviceId: self.deviceId, sectionId: self.sectionId, zoneId: self.zoneId)
        navigationController?.pushViewController(controller, animated: true)
    }
}
