//
//  XRaitingViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XRaitingViewStyle {
    let height: CGFloat
    let backgroundColor: UIColor
    let selectColor: UIColor
    let unselectColor: UIColor
    
    let iconName: String
    let iconInsets: UIEdgeInsets
    let iconSize: CGSize
}

extension XRaitingView {
    typealias Style = XRaitingViewStyle
}
