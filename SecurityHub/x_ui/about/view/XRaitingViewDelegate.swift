//
//  XRaitingViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XRaitingViewDelegate {
    func raitingSelect(startCount: Int)
}
