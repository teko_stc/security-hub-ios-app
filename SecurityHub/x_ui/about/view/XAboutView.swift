//
//  XAboutView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAboutView: UIView {
    public var delegate: XAboutViewDelegate? {
        didSet {
            raitingView.delegate = delegate
        }
    }
    
    private var containerView: UIView = UIView()
    private var raitingView: XRaitingView!
    private var logoView: UIImageView!
    private var versionView, copyrightView: UILabel!
    private var tekobizView, securityhubView, supportmailView, supportPhoneView, tlgrmView: UIButton!
    public let strings = XAboutViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews(xAboutViewStyle())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setVersionInfo(_ ver: String) {
        versionView.text = String(format: "%@ %@\n\n%@\n\n%@", strings.version, ver, strings.createdInfo, strings.webSite)
    }
    
    public func hideRaitingView(withAnimation: Bool = true) {
        UIView.animate(
            withDuration: withAnimation ? 0.3 : 0,
            animations: {
                self.raitingView.alpha = 0
                self.layoutIfNeeded()
            },
            completion: { _ in
                self.raitingView.isHidden = true
            }
        )
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        addSubview(containerView)
        
        raitingView = XRaitingView(self.xRaitingViewStyle())
        
        logoView = UIImageView(image: UIImage(named: style.logoName))
        logoView.contentMode = .scaleAspectFit
        
        versionView = UILabel()
        versionView.textAlignment = .center
        versionView.font = style.mainFont
        versionView.textColor = style.mainFontColor
        versionView.numberOfLines = 0
        
        let attr: [NSAttributedString.Key : Any] = [ .font : style.mainFont!, .foregroundColor : style.actionFontColor ]
        
        tekobizView = UIButton()
        tekobizView.setAttributedTitle(NSAttributedString(string: strings.tekobiz, attributes: attr), for: .normal)
        tekobizView.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        securityhubView = UIButton()
        securityhubView.setAttributedTitle(NSAttributedString(string: strings.securityhub, attributes: attr), for: .normal)
        securityhubView.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        tlgrmView = UIButton()
        tlgrmView.setAttributedTitle(NSAttributedString(string: strings.tlgrm, attributes: attr), for: .normal)
        tlgrmView.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        let attr2: [NSAttributedString.Key : Any] = [ .font : style.mainFont!, .foregroundColor : style.mainFontColor ]
        supportmailView = UIButton()
        let _s = String(format: "%@:\n%@", strings.support, strings.supportmail)
        supportmailView.setAttributedTitle(NSAttributedString(string: _s, attributes: attr2), for: .normal)
        supportmailView.titleLabel?.numberOfLines = 2
        supportmailView.titleLabel?.textAlignment = .center
        supportmailView.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        supportPhoneView = UIButton()
        supportPhoneView.setAttributedTitle(NSAttributedString(string: strings.supportPhone, attributes: attr), for: .normal)
        supportPhoneView.addTarget(self, action: #selector(click), for: .touchUpInside)

        copyrightView = UILabel()
        copyrightView.font = style.copyrightFont
        copyrightView.textColor = style.copyrightFontColor
        copyrightView.text = strings.copyright
        
        addSubview(raitingView)
        containerView.addSubview(logoView)
        containerView.addSubview(versionView)
        containerView.addSubview(tekobizView)
        containerView.addSubview(securityhubView)
        containerView.addSubview(tlgrmView)
        containerView.addSubview(supportmailView)
        containerView.addSubview(supportPhoneView)
        addSubview(copyrightView)
    }
    
    private func setConstraints() {
        raitingView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        logoView.translatesAutoresizingMaskIntoConstraints = false
        versionView.translatesAutoresizingMaskIntoConstraints = false
        tekobizView.translatesAutoresizingMaskIntoConstraints = false
        securityhubView.translatesAutoresizingMaskIntoConstraints = false
        supportmailView.translatesAutoresizingMaskIntoConstraints = false
        supportPhoneView.translatesAutoresizingMaskIntoConstraints = false
        copyrightView.translatesAutoresizingMaskIntoConstraints = false
        tlgrmView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            raitingView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            raitingView.leadingAnchor.constraint(equalTo: leadingAnchor),
            raitingView.heightAnchor.constraint(equalToConstant: xRaitingViewStyle().height),
            raitingView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            
            containerView.topAnchor.constraint(equalTo: raitingView.bottomAnchor, constant: 12),
            containerView.bottomAnchor.constraint(equalTo: copyrightView.topAnchor, constant: -12),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            logoView.widthAnchor.constraint(equalToConstant: 110 * 1.5),
            logoView.topAnchor.constraint(equalTo: containerView.topAnchor),
            logoView.bottomAnchor.constraint(equalTo: versionView.topAnchor, constant: -20),
            logoView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            
            versionView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            versionView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            versionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            tekobizView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            tekobizView.heightAnchor.constraint(equalToConstant: 20),
            tekobizView.topAnchor.constraint(equalTo: versionView.bottomAnchor),
            tekobizView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            securityhubView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            securityhubView.heightAnchor.constraint(equalToConstant: 20),
            securityhubView.topAnchor.constraint(equalTo: tekobizView.bottomAnchor),
            securityhubView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            tlgrmView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            tlgrmView.heightAnchor.constraint(equalToConstant: 20),
            tlgrmView.topAnchor.constraint(equalTo: securityhubView.bottomAnchor),
            tlgrmView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            supportmailView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            supportmailView.topAnchor.constraint(equalTo: tlgrmView.bottomAnchor, constant: 12),
            supportmailView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            supportPhoneView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1),
            supportPhoneView.heightAnchor.constraint(equalToConstant: 30),
            supportPhoneView.topAnchor.constraint(equalTo: supportmailView.bottomAnchor),
            supportPhoneView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            copyrightView.centerXAnchor.constraint(equalTo: centerXAnchor),
            copyrightView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30),
            copyrightView.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    @objc private func click(_ v: UIButton) {
        guard let text = v.titleLabel?.text else { return }
        
        delegate?.linkClick(strings.getLink(text))
    }
}
