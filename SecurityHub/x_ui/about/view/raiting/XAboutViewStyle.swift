//
//  XAboutViewStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XAboutViewStyle {
    let backgroundColor: UIColor
    let logoName: String
    let mainFont: UIFont?
    let copyrightFont: UIFont?
    let mainFontColor, actionFontColor, copyrightFontColor: UIColor
}

extension XAboutView {
    typealias Style = XAboutViewStyle
}
