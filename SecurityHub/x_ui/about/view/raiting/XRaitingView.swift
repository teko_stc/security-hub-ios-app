//
//  XRaitingView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XRaitingView: UIView {
    public var delegate: XRaitingViewDelegate?
    
    private var starViews: [UIImageView] = []
    private let style: Style

    init(_ style: Style) {
        self.style = style
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        raitingEvents(touches, with: event, type: -1)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        raitingEvents(touches, with: event, type: 0)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        raitingEvents(touches, with: event, type: 1)
    }
    
    private func initViews() {
        let _path = UIBezierPath(
            roundedRect: CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 200 + style.height),
            byRoundingCorners: [.bottomLeft, .bottomRight],
            cornerRadii: CGSize(width: 30.0, height: 0.0)
        )
        let _layer = CAShapeLayer()
        _layer.path = _path.cgPath
        _layer.fillColor = style.backgroundColor.cgColor
        layer.addSublayer(_layer)

        for _ in 0...4 {
            let starView = UIImageView()
            starView.image = UIImage(named: style.iconName)?
                .withRenderingMode(.alwaysTemplate)
                .withAlignmentRectInsets(style.iconInsets)
            starView.tintColor = style.unselectColor
            starView.contentMode = .scaleAspectFit
            addSubview(starView)
            starViews.append(starView)
        }
    }
    
    private func setConstraints() {
        var consts: [NSLayoutConstraint] = []
        for i in 0...4 {
            starViews[i].translatesAutoresizingMaskIntoConstraints = false
            consts.append(contentsOf: [
                starViews[i].heightAnchor.constraint(equalToConstant: style.iconSize.height),
                starViews[i].widthAnchor.constraint(equalToConstant: style.iconSize.width),
                starViews[i].bottomAnchor.constraint(equalTo: bottomAnchor, constant: style.iconInsets.bottom + style.iconInsets.top),
                starViews[i].centerXAnchor.constraint(equalTo: centerXAnchor, constant: (CGFloat(i) - 2) * style.iconSize.width)
            ])
        }
        NSLayoutConstraint.activate(consts)
    }
    
    private func raitingEvents(_ touches: Set<UITouch>, with event: UIEvent?, type: Int) {
        for touch in touches {
            let p = touch.location(in: starViews.last)
            let _x = (p.x - style.iconSize.width)
            if (p.y > 0 && p.y < style.iconSize.height && _x > -style.iconSize.width*5 && _x < 0) {
                let k = 4 + Int(_x / style.iconSize.width)
                for i in 0...k { starViews[i].tintColor = style.selectColor }
                if (k+1 <= 4) { for i in (k+1)...4 { starViews[i].tintColor = style.unselectColor } }
                if (type == 1) { delegate?.raitingSelect(startCount: k + 1) }
            }
        }
    }
}

