//
//  XAboutViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XAboutViewDelegate: XRaitingViewDelegate {
    func linkClick(_ link: String)
}
