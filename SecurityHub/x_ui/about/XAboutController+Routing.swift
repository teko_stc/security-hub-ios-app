//
//  XAboutController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import StoreKit

extension XAboutController: XAboutViewDelegate {
    func linkClick(_ link: String) {
        guard let url = URL(string: link) else { return }
        UIApplication.shared.open(url)
    }
    
    func raitingSelect(startCount: Int) {
        if (startCount > 3) {
            SKStoreReviewController.requestReview()
            settingsHelper.needRatingInXAboutController = false
        } else {
            //TODO: Отправить информацию о негативной оценке куда либо
        }
        checkNeedRaiting(needRaiting: false, withAnimation: true)
    }
}
