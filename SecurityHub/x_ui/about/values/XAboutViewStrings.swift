//
//  XAboutViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XAboutViewStrings {
    /// Оцените
    var titleWithRaiting: String { "N_RATE_APP_TITLE".localized() }
    
    /// About
    var title: String { "AA_TITLE".localized() }
    
    /// App version
    var version: String { "ABOUT_VERSION".localized() }
    
    /// Created by TEKO-TD
    var createdInfo: String { "ABOUT_DEVELOPER".localized() }
    
    /// Web-site
    var webSite: String { "WEB_SITE".localized() }
    
    var tekobiz: String { "teko.biz" }
    var tlgrm: String { "t.me/teko_astra_chat" }
    var securityhub: String { "security-hub.ru" }
    var helpmail: String { "help@security-hub.ru" }
    
    /// Support
    var support: String { "SUPPORT".localized() }
    
    var supportmail: String { "support@teko.biz" }
    var supportPhone: String { "+7 (843) 528 - 22 - 56" }
    
    var copyright: String  { "© 2017-2021 Security Hub" }
    
    func getLink(_ string: String) -> String {
        if string == supportPhone {
            return "tel://+78435282256"
        }
        
        switch string {
        case tekobiz: return "https://teko.biz"
        case securityhub: return "https://security-hub.ru"
        case tlgrm: return "https://t.me/teko_astra_chat"
        default: return "mailto:support@teko.biz"
        }
    }
}
