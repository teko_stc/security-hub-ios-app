//
//  XAboutViewStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAboutView {
    func xRaitingViewStyle() -> XRaitingView.Style {
        return XRaitingView.Style(
            height: 90,
            backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            selectColor: UIColor(red: 0xff/255, green: 0xdf/255, blue: 0x67/255, alpha: 1),
            unselectColor:UIColor.white,
            iconName: "ic_main",
            iconInsets: UIEdgeInsets(top: -11, left: -11, bottom: -11, right: -11),
            iconSize: CGSize(width: 58, height: 58)
        )
    }
    
    func xAboutViewStyle() -> XAboutView.Style {
        return XAboutView.Style(
            backgroundColor: UIColor.white,
            logoName: "new_logo",
            mainFont: UIFont(name: "Open Sans", size: 15.5),
            copyrightFont: UIFont(name: "Open Sans", size: 11.3),
            mainFontColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
            actionFontColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            copyrightFontColor: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1)
        )
    }
}
