//
//  XAboutController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAboutController: XBaseViewController<XAboutView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilderWithRaiting = XNavigationViewBuilder(
        backgroundColor: UIColor.colorFromHex(0x3abeff),
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: .white, viewTapped: self.back),
        titleView: XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 25), alignment: .center)
    )
    
    private lazy var navigationViewBuilderWithOutRaiting = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
        
    public let settingsHelper = DataManager.settingsHelper
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        checkNeedRaiting(needRaiting: settingsHelper.needRatingInXAboutController)
        setVersionInfo()
    }
    
    func checkNeedRaiting(needRaiting: Bool, withAnimation: Bool = false) {
        if (needRaiting) {
            title = mainView.strings.titleWithRaiting
            setNavigationViewBuilder(navigationViewBuilderWithRaiting)
        } else {
            title = mainView.strings.title
            setNavigationViewBuilder(navigationViewBuilderWithOutRaiting)
            mainView.hideRaitingView(withAnimation: false)
        }
    }
        
    private func setVersionInfo() {
        let version : String! = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        mainView.setVersionInfo(version)
    }
}
