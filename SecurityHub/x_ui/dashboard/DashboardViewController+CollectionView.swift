//
//  DashboardViewController+CollectionView.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension DashboardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return columnCount * rowCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func setupCollectionView() {
        for index in 0...((columnCount * rowCount) - 1) {
            guard let siteId = selectedSite?.id,
                  let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(index)") else {
                self.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                continue
            }
            switch objectType {
            case "site":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getSiteEntity(site_id: objectId.int64) { [weak self] si in
                    if objectId > 0, let si = si, let site = si.site  {
                        self?.items[index] = (f: site, s: si.armStatus, t: si.affects)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }
            case "device":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getDeviceEntity(device_id: objectId.int64) { [weak self] de in
                    if objectId > 0, let de = de, let device = de.device {
                        self?.items[index] = (f: device, s: de.armStatus, t: de.affects)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }
                
            case "section":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getSectionEntity(l_section_id: objectId.int64) { [weak self] se in
                    if objectId > 0, let se = se {
                        self?.items[index] = (f: se.section, s: se.affects, t: 0)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }
                
            case "section_group":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
               
                DataManager.shared.getSectionGroupEntity(id: objectId.int64) { [weak self] sg in
                    if objectId > 0, let sg = sg {
                        self?.items[index] = (f: sg.sectionGroup, s: sg.armStatus, t: sg.affects)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }
                
            case "relay":
                let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(index)") ?? "0"
                DataManager.shared.getDashboardRelay(id: objectId) { [weak self] re in
                    if let re = re {
                        self?.items[index] = (f: re.relay, s: re.hasScripts, t: re.affects)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }
                
                
            case "zone":
                let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(index)") ?? "0"
                DataManager.shared.getDahsboardZone(id: objectId) { [weak self] ze in
                    if let ze = ze {
                        self?.items[index] = (f: ze.zone, s: ze.section, t: ze.affects)
                    } else {
                        self?.items[index] = (f: 0, s: 0, t: 0)
                        userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(index)")
                        userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(index)")
                    }
                    
                    self?.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                }

            case "sos":
                items[index] = (f: "SOS", s: 0, t: 0)
                collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])

            default:
                self.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let emptyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardEmptyCellIdentifier", for: indexPath) as! DashboardEmptyCell

        guard let siteId = selectedSite?.id,
              let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)") else {
            return emptyCell
        }
        switch objectType {
        case "site":
            guard let site = items[indexPath.item]?.f as? Sites,
                  let armStatus = items[indexPath.item]?.s as? DSiteEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(site: site, armStatus: armStatus, affects: affects)
            return cell
        case "device":
            guard let device = items[indexPath.item]?.f as? Devices,
                  let armStatus = items[indexPath.item]?.s as? DDeviceEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(device: device, armStatus: armStatus, affects: affects)
            return cell
        case "section":
            guard let section = items[indexPath.item]?.f as? Sections,
                  let affects = items[indexPath.item]?.s as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(section: section, affects: affects)
            return cell
        case "section_group":
            guard let sectionGroup = items[indexPath.item]?.f as? SectionGroups,
                  let armStatus = items[indexPath.item]?.s as? DSiteEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(sectionGroup: sectionGroup, armStatus: armStatus, affects: affects)
            return cell
        case "relay":
            guard let relay = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo,
                  let hasScripts = items[indexPath.item]?.s as? Bool,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }
            
            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(relay: relay.zone, hasScripts: hasScripts, affects: affects)
            return cell
       case "zone":
            guard let zone = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo,
                  let section = items[indexPath.item]?.s as? Sections,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }
            
            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(zone: zone.zone, section: section, affects: affects)
            return cell
        case "camera":
            let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
            guard let cameraId = objectId else {
                userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)")
                userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
                return emptyCell
            }
            let cell = dequeueReusableCell(indexPath: indexPath)
            if let camera = cameras.first(where: { $0.id == cameraId }) {
                cell.setup(camera: camera)
            } else {
                cell.setupEmptyCamera()
            }
            return cell
    
        case "sos":
            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setupSOS()
            return cell
    
        default:
            return emptyCell
        }
    }
    
    private func dequeueReusableCell(indexPath: IndexPath) -> DashboardCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCellIdentifier", for: indexPath) as! DashboardCell
        cell.tag = indexPath.item
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(sender:)))
        cell.addGestureRecognizer(longPress)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapPressed(sender:)))
        cell.addGestureRecognizer(tap)
        return cell
    }
    
    @objc func tapPressed(sender: UITapGestureRecognizer) {
        guard let item = sender.view?.tag else { return }
        collectionView(collectionView, didSelectItemAt: IndexPath(row: item, section: 0))
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == .began, let cellTag = sender.view?.tag {
            openDeleteAlert(item: cellTag)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: horizontalInsets, bottom: 0, right: horizontalInsets)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let siteId = selectedSite?.id,
              let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)") else {
            openAddMenu(item: indexPath.row)
            return
        }
        switch objectType {
        case "site":
            guard let site = items[indexPath.item]?.f as? Sites else {
                return openAddMenu(item: indexPath.row)
            }
            openSiteAlert(site: site)
        case "device":
            guard let device = items[indexPath.item]?.f as? Devices else {
                return openAddMenu(item: indexPath.row)
            }
            openDeviceAlert(device: device)
        case "section":
            guard let section = items[indexPath.item]?.f as? Sections else {
                return openAddMenu(item: indexPath.row)
            }

            if section.detector == 0 || section.detector == 1 {
                openSectionAlert(section: section)
            } else {
                let controller = XSectionController(deviceId: section.device.int, sectionId: section.section.int)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case "section_group":
            guard let sectionGroup = items[indexPath.item]?.f as? SectionGroups else {
                return openAddMenu(item: indexPath.row)
            }

            DataManager.shared.getSectionGroupSections(id: sectionGroup.id) { [weak self] sections in
                self?.openSectionGroupAlert(name: sectionGroup.name, sections: sections)
            }
        case "relay":
            guard let re = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo else {
                return openAddMenu(item: indexPath.row)
            }

            let relay = re.zone
            if relay.detector & 0xff == HubConst.DETECTOR_MANUAL_RELAY {
                openRelayAlert(relay: relay)
            } else {
                let controller = XZoneController(deviceId: relay.device.int, sectionId: relay.section.int, zoneId: relay.zone.int)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        case "zone":
            guard let ze = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo else {
                return openAddMenu(item: indexPath.row)
            }

            let zone = ze.zone
            let controller = XZoneController(deviceId: zone.device.int, sectionId: zone.section.int, zoneId: zone.zone.int)
            self.navigationController?.pushViewController(controller, animated: true)
        case "camera":
            let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
            guard let cameraId = objectId,
                  let camera = cameras.first(where: { $0.id == cameraId }) else {
                openAddMenu(item: indexPath.row)
                return
            }
            _ = DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
                .subscribeOn(ThreadUtil.shared.backScheduler)
                .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onSuccess: { [weak self] token in
                    let model = XCameraViewLayerModel(type: .ivideon, id: camera.id, name: camera.name, url: camera.getPreviewUrl(accessToken: token ?? ""))
                    let controller = XCameraPreviewController(model: model)
                    self?.navigationController?.pushViewController(controller, animated: true)
                })
        case "sos":
            openSOS()
        default:
            openAddMenu(item: indexPath.row)
        }
    }
}
