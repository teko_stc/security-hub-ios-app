//
//  DashboardViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class DashboardViewController: UIViewController {
		var waitNotReadyZone = false
    var sites = [Sites]()
    var selectedSite: Sites? {
        didSet {
            userDefaults().setValue(selectedSite?.id, forKey: "selectedSiteIdKey")
        }
    }
    var cameras: [Camera] = []
    
    private var navigationObserver: Any?
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var downIcon: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var toastLabel: UILabel?
    var toastTimer: Timer?
    private var onboarbingView = OnboardingView()
    
    // constatnts
    let rowCount = 3
    let columnCount = 2
    let horizontalInsets: CGFloat  = 20
    
    var items: [Int: (f: Any, s: Any, t: Any)] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupNotifications()
        loadSites()
        loadCameras()
        setupOnboarbingView()
        updateOnboardingView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        (tabBarController as? XBaseTabBarController)?.setBaseTabBarHidden(false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationObserver = NotificationCenter.default.addObserver(forName: HubNotification.navigation, object: nil, queue: OperationQueue.main) { notification in
            guard let nV = self.navigationController else { return }
					if (nV.viewControllers.last is XNotReadyZonesController) == false, let nNotReadyZone = notification.object as? NNotReadyZone, self.waitNotReadyZone {
								nV.viewControllers.last!.present(XNotReadyZonesController(nNotReadyZone), animated: false)
							self.waitNotReadyZone = false
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let _ = navigationObserver { NotificationCenter.default.removeObserver(navigationObserver!) }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupCollectionViewCellSize()
    }

    private func setup() {
        self.titleLabel.text = ""
        self.titleLabel.widthAnchor.constraint(lessThanOrEqualTo: self.view.widthAnchor, multiplier: 0.6).isActive = true
        self.titleLabel.isUserInteractionEnabled = true
        self.titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.sitesPressed)))
        self.downIcon.isUserInteractionEnabled = true
        self.downIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.sitesPressed)))
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "DashboardEmptyCell", bundle: nil), forCellWithReuseIdentifier: "DashboardEmptyCellIdentifier")
        self.collectionView.register(UINib(nibName: "DashboardCell", bundle: nil), forCellWithReuseIdentifier: "DashboardCellIdentifier")
        self.collectionView.isScrollEnabled = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    private func setupNotifications() {
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: NSNotification.Name("updateDashboard"), object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.loadSites), name: HubNotification.siteUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.deviceUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.sectionsUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.zonesUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.siteSelected_XDevicePartitionMenuController),
                                               name: Notification.Name("siteSelected_XDevicePartitionMenuController"), object: nil)
    }
    
    private var updateDashboardCounter: Int64 = 1
    private var updateDashboardTimer: Timer?
    private let updateDashboardLock = NSLock()
    @objc func updateDashboard() {
        updateDashboardLock.lock()
        updateDashboardCounter += 1
        updateDashboardTimer?.invalidate()
        if updateDashboardCounter % 100 == 0 {
            DispatchQueue.main.async {
                self.setupCollectionView()
                self.updateOnboardingView()
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.updateDashboardTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                    self?.setupCollectionView()
                    self?.updateOnboardingView()
                }
            }
        }
        updateDashboardLock.unlock()
    }
    
    func updateOnboardingView() {
        DataManager.shared.getDBSites { [weak self] sites in
            guard let self = self else {
                return
            }
            
            let isHaveSite = sites.count > 0
            switch (isHaveSite){
            case (false):
                self.onboarbingView.isHidden = false
                self.onboarbingView.set(title: "N_MAIN_EMPTY".localized(), actionTitle: "ADB_ADD".localized(), action: {
                    let vc = SiteAddController(site_id: nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                })
            default:
                self.onboarbingView.isHidden = true
            }
        }
    }
    
    private func updateSiteLabel() {
        titleLabel.superview?.isHidden = sites.count == 0
        titleLabel.text = selectedSite?.name
    }
    
    @objc private func loadSites() {
        DataManager.shared.getDBSites { [weak self] sites in
            guard let self = self else {
                return
            }
            
            self.sites = sites
            let selectedSiteId = userDefaults().integer(forKey: "selectedSiteIdKey")
            if let selected = self.sites.first(where: { $0.id == selectedSiteId }) {
                self.selectedSite = selected
                userDefaults().setValue(selected.id, forKey: "DashboardPrioritySiteId")
                DataManager.shared.nCenter.post(name: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil)
            } else {
                self.selectedSite = self.sites.sorted { $0.id < $1.id }.first
            }
            self.updateSiteLabel()
            self.updateDashboard()
        }
    }
    
    @objc private func siteSelected_XDevicePartitionMenuController(n: Notification) {
        guard let site = n.object as? Sites else {
            return
        }

        self.selectedSite = site
        self.updateSiteLabel()
        self.setupCollectionView()
    }
    
    private func loadCameras(){
        _ = DataManager.shared.getCameraList().subscribe(onNext: { [weak self] (cameras) in
            guard let cameras = cameras else {
                self?.cameras = []
                return
            }
            self?.cameras = cameras
            self?.updateDashboard()
        },onError: { [weak self] error in
            self?.cameras = []
        })
    }
    
    private func setupCollectionViewCellSize() {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = (collectionView.frame.width - 2 * horizontalInsets) / CGFloat(columnCount)
            let itemHeight = (collectionView.frame.height - 50) / CGFloat(rowCount)
            flowLayout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        }
    }
    
    @objc func sitesPressed() {
        openSiteMenu()
    }

    @IBAction func abbButtonPressed(_ sender: Any) {
        let controller = XAddMenuController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        guard let siteId = self.selectedSite?.id else { return }
        let controller = XSiteSettingsController(siteId: siteId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func openSiteMenu() {
        guard let selectedIndex = sites.firstIndex(where: { $0.id == selectedSite?.id }) else { return }
        let sitesVC = SitesViewController(sites: sites, selectedIndex: selectedIndex)
        sitesVC.delegate = self
        sitesVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(sitesVC, animated: true, completion: nil)
    }
    
    func openAddMenu(item: Int) {
        guard let siteId = self.selectedSite?.id else { return }
        let addVC = DashboardAddMainViewController(item: item, siteId: siteId, delegate: self)
        self.navigationController?.pushViewController(addVC, animated: true)
    }
    
    func setupOnboarbingView() {
        onboarbingView.isHidden = true
        view.addSubview(onboarbingView)
        onboarbingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            onboarbingView.topAnchor.constraint(equalTo: view.topAnchor),
            onboarbingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            onboarbingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            onboarbingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

extension DashboardViewController: SitesViewControllerDelegate {
    func siteSelected(site: Sites) {
        userDefaults().setValue(site.id, forKey: "DashboardPrioritySiteId")
        DataManager.shared.nCenter.post(name: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil)
        DataManager.shared.nCenter.post(name: NSNotification.Name("siteSelected_DashboardViewController"), object: site)
        self.selectedSite = site
        self.updateSiteLabel()
        self.setupCollectionView()
    }
}
