//
//  DashboardViewController+Alerts.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 30.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

extension DashboardViewController {
    func openDeleteAlert(item: Int) {
        guard let siteId = selectedSite?.id,
              let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(item)") else {
            return
        }
        let alertStrings = XAlertView.Strings(title: "N_MAIN_DELETE_MESSAGE".localized(), text: nil, positive: "DELETE".localized(), negative: "ADB_CANCEL".localized())
        let alertVC = XAlertController(style: baseAlertStyle(), strings: alertStrings) { [weak self] in
            userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(item)")
            userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(item)")
            self?.setupCollectionView()
        } negative: {}
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func baseAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 12),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func openSiteAlert(site: Sites) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: "N_MAIN_TITLE_FULL_SITE".localized(), siteName: "", hideInfoButton: true)
        alert.onArmed = { [weak self] in
						var waitNotReadyHappen = false
            for device in DataManager.shared.getDevices(site_id: site.id) {
                _ = self?.doOnConnect(deviceId: device.id)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.arm(device: $0) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        alert.onDisarmed = { [weak self] in
						var waitNotReadyHappen = false
            for device in DataManager.shared.getDevices(site_id: site.id) {
                _ = self?.doOnConnect(deviceId: device.id)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.disarm(device: $0) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        self.present(alert, animated: true)
    }
    
    func openDeviceAlert(device: Devices) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: device.name, siteName: "controller".localized())
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: device.id)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.arm(device: $0) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: device.id)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.disarm(device: $0) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XDeviceController(deviceId: device.id.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }
    
    func openSectionGroupAlert(name: String, sections: [Sections]) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: name, siteName: "BAR_ARM_GROUP".localized(), hideInfoButton: true)
        alert.onArmed = { [weak self] in
						var waitNotReadyHappen = false
            for section in sections {
                _ = self?.doOnConnect(deviceId: section.device)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.arm(device: $0, section: section.section) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        alert.onDisarmed = { [weak self] in
						var waitNotReadyHappen = false
            for section in sections {
                _ = self?.doOnConnect(deviceId: section.device)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.disarm(device: $0, section: section.section) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        self.present(alert, animated: true)
    }
    
    func openSectionAlert(section: Sections) {
        var alert_type: XArmAlertControllerType = .security
        if section.detector == 6 { alert_type = .tech }
        let alert = XArmAlertController(type: alert_type, deviceName: section.name, siteName: "PARTITION".localized())
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: section.device)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.arm(device: $0, section: section.section) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: section.device)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.disarm(device: $0, section: section.section) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XSectionController(deviceId: section.device.int, sectionId: section.section.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }
    
    func openRelayAlert(relay: Zones) {
        let alert_type: XArmAlertControllerType = .relay
        let alert = XArmAlertController(type: alert_type, deviceName: relay.name, siteName: "CONTROLLER".localized())
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: relay.device)
               .observeOn(ThreadUtil.shared.mainScheduler)
               .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.switchRelay(device: $0, section: relay.section, zone: relay.zone, state: 1) })
                .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                .do(onError: { [weak self] error in self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: relay.device)
               .observeOn(ThreadUtil.shared.mainScheduler)
               .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.switchRelay(device: $0, section: relay.section, zone: relay.zone, state: 0) })
                .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                .do(onError: { [weak self] error in self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XZoneController(deviceId: relay.device.int, sectionId: relay.section.int, zoneId: relay.zone.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }

    func openSOS() {
        let alert_type: XArmAlertControllerType = .sos
        let alert = XArmAlertController(type: alert_type, deviceName: "BAR_PANIC_MAIN_TITLE".localized(), siteName: "BAR_PANIC_DIALOG_MESSAGE".localized())
        alert.onSos = { [weak self] location in
            guard let device_id = DataManager.shared.getHubDeviceIds(site_id: self?.selectedSite?.id).min() else {
                return
            }
					
						var dParam: [String: Any] = ["device_id":device_id]
						if let location = location {
							dParam ["lat"] = location.latitude
							dParam ["lon"] = location.longitude
						}
            _ = DataManager.shared.hubHelper.setCommandWithResult(.SOS, D: dParam)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] result in self?.showErrorColtroller(message: result.success ? "BAR_PANIC_SUCCESS_MESSAGE".localized() : result.message) })
                .do(onError: self?.errorProcessing)
                .asCompletable().subscribe()
        }
				self.present(alert, animated: true)
    }

    func showErrorColtroller(message: String, cancel: @escaping (() -> ()) = {}) {
        toastLabel?.removeFromSuperview()
        toastLabel = nil
        let alert = XAlertController(
            style: XAlertView.Style(
                backgroundColor: .white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xf9543a))
            ),
            strings: XAlertView.Strings(title: message, text: nil, positive: "N_BUTTON_OK".localized(), negative: nil),
            positive: cancel
        )
        navigationController?.present(alert, animated: true)
    }
    
    func showToast(message : String, font: UIFont? = UIFont(name: "Open Sans", size: 20)) {
        if toastLabel != nil {
            toastTimer?.invalidate()
            toastLabel?.alpha = 0.0
            toastLabel?.removeFromSuperview()
        }
        toastLabel = UILabel()
        toastLabel!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel!.textColor = UIColor.white
        toastLabel!.font = font
        toastLabel!.textAlignment = .center;
        toastLabel!.text = message
        toastLabel!.alpha = 1.0
        toastLabel!.layer.cornerRadius = 10;
        toastLabel!.clipsToBounds  =  true
        toastLabel!.numberOfLines = 0
        self.view.addSubview(toastLabel!)
        toastLabel!.translatesAutoresizingMaskIntoConstraints = false
        toastLabel!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        toastLabel!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        toastLabel!.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        toastLabel!.setContentHuggingPriority(.defaultLow, for: .horizontal)
        toastLabel!.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        toastTimer = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { [weak self] timer in
            self?.toastLabel?.alpha = 0.0
            self?.toastLabel?.removeFromSuperview()
            self?.toastLabel = nil
            self?.toastTimer = nil
        }
    }
    
    public func resultProcessing(_ result: DCommandResult) {
        if result.success { return }
        showToast(message: result.message)
    }

    public func errorProcessing(_ error: Error) {
        guard let xError = error as? XError else {
            return showToast(message: error.localizedDescription)
        }

        return showErrorColtroller(message: xError.desc)
    }

    func doOnConnect(deviceId: Int64) -> Single<Int64> {
        DataManager.shared.isAvailableDevice(device_id: deviceId)
            .flatMap({ result -> Single<Int64> in
                switch result.code {
                case HubResponseCode.NO_SIGNAL_DEVICE_CODE:
                    return Single.error(XError(desc: result.message))
                default:
                    return Single.just(deviceId)
                }
            })
    }

    func doOnAvailable(deviceId: Int64) -> Single<Int64> {
        DataManager.shared.isAvailableDevice(device_id: deviceId)
            .flatMap({ result -> Single<Int64> in
                switch result.success {
                case false:
                    return Single.error(XError(desc: result.message))
                case true:
                    return Single.just(deviceId)
                }
            })
    }
}
