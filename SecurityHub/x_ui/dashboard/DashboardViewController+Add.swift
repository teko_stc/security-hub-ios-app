//
//  DashboardViewController+Add.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension DashboardViewController: DashboardAddViewControllerDelegate {
    func siteSelected(item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("site", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(siteId, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func deviceSelected(device: Devices, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("device", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(device.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func sectionSelected(section: Sections, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("section", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(section.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func sectionGroupSelected(sectionGroup: SectionGroups, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("section_group", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(sectionGroup.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func relaySelected(relay: Zones, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("relay", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(relay.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func zoneSelected(zone: Zones, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("zone", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(zone.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
    
    func cameraSelected(camera: Camera, item: Int) {
        guard let siteId = selectedSite?.id else { return }
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("camera", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(camera.id, forKey: objectIdKeyName)
        self.setupCollectionView()
    }

    func sosSelected(siteId: Int64, item: Int) {
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("sos", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(siteId, forKey: objectIdKeyName)
        self.setupCollectionView()
    }
}
