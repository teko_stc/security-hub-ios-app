//
//  DashboardEmptyCell.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 18.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardEmptyCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.heightAnchor.constraint(equalToConstant: 180),
            contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/2-1),
        ])
    }

    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let self = self else { return }
                    self.transform = self.transform.scaledBy(x: 0.75, y: 0.75)
                }
            } else {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let self = self else { return }
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                }
            }
        }
    }
}
