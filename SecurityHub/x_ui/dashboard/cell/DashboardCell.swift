//
//  DashboardCell.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 05.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var leftTopImageView: UIImageView!
    @IBOutlet weak var rightTopImageView: UIImageView!
    @IBOutlet weak var rightBottomView: UIImageView!
    var tempView = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tempView.backgroundColor = UIColor.white
        tempView.font = UIFont(name: "Open Sans", size: 18)
        contentView.addSubview(tempView)
        tempView.translatesAutoresizingMaskIntoConstraints = false
        tempView.centerYAnchor.constraint(equalTo: rightBottomView.centerYAnchor).isActive = true
        tempView.centerXAnchor.constraint(equalTo: rightBottomView.centerXAnchor).isActive = true
        
        [contentView, subtitleLabel].forEach{ $0.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
//            contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: 140),
            contentView.heightAnchor.constraint(lessThanOrEqualToConstant: 180),
            contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/2 - 1),
            subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let self = self else { return }
                    self.transform = self.transform.scaledBy(x: 0.75, y: 0.75)
                }
            } else {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let self = self else { return }
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                }
            }
        }
    }

    func setup(site: Sites, armStatus: DSiteEntity.ArmStatus, affects: [DAffectEntity], isShared: Bool = false) {
        self.iconImageView.image = UIImage(named: "ic_main_arm")
        self.titleLabel.text = isShared ? site.name : "N_MAIN_TITLE_FULL_SITE".localized()
        
        switch armStatus {
        case .armed:
            self.subtitleLabel.text = "arm_statement_titles[1]".localized()
            self.rightBottomView.image = UIImage(named: "ic_armed_overlay")
        case .notFullArmed:
            self.subtitleLabel.text = "arm_statement_titles[2]".localized()
            self.rightBottomView.image = UIImage(named: "ic_partly_armed_overlay")
        case .disarmed:
            self.subtitleLabel.text = "arm_statement_titles[0]".localized()
            self.rightBottomView.image = UIImage(named: "ic_disarmed_overlay")
        }
        
        if affects.contains(where: { $0._class == 8 && $0.reason == 6 }) {
            self.rightTopImageView.image = UIImage(named: "ic_delay_off")
        } else {
            self.rightTopImageView.image = nil
        }
        
        var isAlarm = false, isAttention = false
        for affect in affects {
            switch affect._class {
            case HubConst.CLASS_ALARM:
                isAlarm = true
            case HubConst.CLASS_SABOTAGE, HubConst.CLASS_ATTENTION:
                isAttention = true
            case HubConst.CLASS_MALFUNCTION:
                if affect.reason != HubConst.REASON_MALF_LOST_CONNECTION { isAttention = true }
            default: break;
            }
        }
        self.leftTopImageView.image = isAlarm ? UIImage(named: "ic_alarm_overlay") : isAttention ? UIImage(named: "ic_attention_overlay") : nil
        self.tempView.text = nil
    }
    
    func setup(device: Devices, armStatus: DDeviceEntity.ArmStatus, affects: [DAffectEntity], siteName: String? = nil) {
        let deviceType = HubConst.getDeviceType(device.configVersion, cluster: device.cluster_id)
        let icon = DataManager.shared.d3Const.getDeviceIcons(type: deviceType.int)

        self.iconImageView.image = UIImage(named: icon.main)
        self.titleLabel.text = device.name
        var sname = ""
        if let siteName = siteName {
            sname = "\(siteName)\n"
            self.subtitleLabel.numberOfLines = 0
            self.titleLabel.numberOfLines = 1
        } else {
            self.subtitleLabel.numberOfLines = 1
            self.titleLabel.numberOfLines = 2
        }
        switch armStatus {
        case .armed:
            self.subtitleLabel.text = sname + "arm_statement_titles[1]".localized()
            self.rightBottomView.image = UIImage(named: "ic_armed_overlay")
        case .notFullArmed:
            self.subtitleLabel.text = sname + "arm_statement_titles[2]".localized()
            self.rightBottomView.image = UIImage(named: "ic_partly_armed_overlay")
        case .disarmed:
            self.subtitleLabel.text = sname + "arm_statement_titles[0]".localized()
            self.rightBottomView.image = UIImage(named: "ic_disarmed_overlay")
        }
        var isAlarm = false, isAttention = false, isOffline = false
        for affect in affects {
            switch affect._class {
            case HubConst.CLASS_ALARM:
                isAlarm = true
            case HubConst.CLASS_SABOTAGE, HubConst.CLASS_ATTENTION:
                isAttention = true
            case HubConst.CLASS_MALFUNCTION:
                if affect.reason == HubConst.REASON_MALF_LOST_CONNECTION { isOffline = true }
                else { isAttention = true }
            default: break;
            }
        }
        self.leftTopImageView.image = isAlarm ? UIImage(named: "ic_alarm_overlay") : isAttention ? UIImage(named: "ic_attention_overlay") : isOffline ? UIImage(named: "ic_overlay_offline") : nil
        
        self.rightTopImageView.image = nil
        self.tempView.text = nil
    }
    
    func setup(section: Sections, affects: [DAffectEntity], siteName: String? = nil) {
        var imageName: String = "ic_section_common_main"
        var selectorName: String? = nil
        if let sectionType =  R.sectionTypes.first(where: { $0.id == section.detector })  {
            selectorName = sectionType.icons[1]
        }
        if let name = selectorName, let selector = DataManager.shared.getSelector(name: name) {
            if let lastItem = selector.items.last {
                imageName = (lastItem.drawable as NSString).lastPathComponent
            }
        }
        self.iconImageView.image = UIImage(named: imageName)
        self.titleLabel.text = section.name
        
        var sname = ""
        if let siteName = siteName {
            sname = "\(siteName)\n"
            self.subtitleLabel.numberOfLines = 0
            self.titleLabel.numberOfLines = 1
        } else {
           self.subtitleLabel.numberOfLines = 1
           self.titleLabel.numberOfLines = 2
       }
        switch section.detector {
        case 0, 1:
            if section.arm > 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[1]".localized()
                self.rightBottomView.image = UIImage(named: "ic_armed_overlay")
            } else if section.arm == 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[0]".localized()
                self.rightBottomView.image = UIImage(named: "ic_disarmed_overlay")
            }
        case 6:
            if section.arm > 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[1]".localized()
                self.rightBottomView.image = nil
            } else if section.arm == 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[0]".localized()
                self.rightBottomView.image = UIImage(named: "ic_no_section_control_overlay")
            }
        default:
            self.subtitleLabel.text = sname + "dashboard_dayaround".localized()
            self.rightBottomView.image = nil
        }
        
        if affects.contains(where: { $0._class == 8 && $0.reason == 6 }) {
            self.rightTopImageView.image = UIImage(named: "ic_delay_off")
        } else {
            self.rightTopImageView.image = nil
        }
        
        if affects.contains(where: { $0._class == 0 }) {
            self.leftTopImageView.image = UIImage(named: "ic_alarm_overlay")
        } else if affects.contains(where: { $0._class > 0 && $0._class < 4 }) {
            self.leftTopImageView.image = UIImage(named: "ic_attention_overlay")
        } else {
            self.leftTopImageView.image = nil
        }
        self.tempView.text = nil
    }
    
    func setup(sectionGroup: SectionGroups, armStatus: DSiteEntity.ArmStatus, affects: [DAffectEntity]) {
        self.iconImageView.image = UIImage(named: "ic_section_group_main")
        self.titleLabel.text = sectionGroup.name
        
        var sname = ""
        if !sectionGroup.siteName.isEmpty {
            sname = "\(sectionGroup.siteName)\n"
            self.subtitleLabel.numberOfLines = 0
            self.titleLabel.numberOfLines = 1
        } else {
           self.subtitleLabel.numberOfLines = 1
           self.titleLabel.numberOfLines = 2
       }
        
        switch armStatus {
        case .armed:
            self.subtitleLabel.text = sname + "arm_statement_titles[1]".localized()
            self.rightBottomView.image = UIImage(named: "ic_armed_overlay")
        case .notFullArmed:
            self.subtitleLabel.text = sname + "arm_statement_titles[2]".localized()
            self.rightBottomView.image = UIImage(named: "ic_partly_armed_overlay")
        case .disarmed:
            self.subtitleLabel.text = sname + "arm_statement_titles[0]".localized()
            self.rightBottomView.image = UIImage(named: "ic_disarmed_overlay")
        }
        
        if affects.contains(where: { $0._class == 8 && $0.reason == 6 }) {
            self.rightTopImageView.image = UIImage(named: "ic_delay_off")
        } else {
            self.rightTopImageView.image = nil
        }
        
        if affects.contains(where: { $0._class == 0 }) {
            self.leftTopImageView.image = UIImage(named: "ic_alarm_overlay")
        } else if affects.contains(where: { $0._class > 0 && $0._class < 4 }) {
            self.leftTopImageView.image = UIImage(named: "ic_attention_overlay")
        } else {
            self.leftTopImageView.image = nil
        }
        self.tempView.text = nil
    }
    
    func setup(relay: Zones, hasScripts: Bool, affects: [DAffectEntity], siteName: String? = nil) {
        let selectorName = DataManager.shared.d3Const.getSensorIcons(uid: Int(relay.uidType), detector: Int(relay.detector)).main
        self.iconImageView.image = UIImage(named: selectorName)
        self.titleLabel.text = relay.name
        let isOn = affects.contains(where: { $0._class == 5 && $0.reason == 6 }) && (relay.getType() == 5 && (relay.detector == 23 || (relay.detector == 13 && hasScripts)) || relay.getType() == 2)
        
        var sname = ""
        if let siteName = siteName {
            sname = "\(siteName)\n"
            self.subtitleLabel.numberOfLines = 0
            self.titleLabel.numberOfLines = 1
        } else {
           self.subtitleLabel.numberOfLines = 1
           self.titleLabel.numberOfLines = 2
       }

        let model = DataManager.shared.d3Const.getSensorType(uid: relay.uidType.int)
        if relay.detector & 0xff == HubConst.DETECTOR_AUTO_RELAY && !hasScripts {
            self.subtitleLabel.text = sname
            self.rightBottomView.image = nil
        } else if model == HubConst.SENSORTYPE_WIRED_INPUT || model == HubConst.SENSORTYPE_WIRED_OUTPUT && hasScripts {
            self.subtitleLabel.text = sname
            self.rightBottomView.image = nil
        } else if isOn {
            self.subtitleLabel.text = sname + "N_BUTTON_ON".localized()
            self.rightBottomView.image = UIImage(named: "ic_on_overlay")
        } else {
            self.subtitleLabel.text = sname + "N_BUTTON_OFF".localized()
            self.rightBottomView.image = UIImage(named: "ic_off_overlay")
        }

        if relay.detector & 0xff == HubConst.DETECTOR_AUTO_RELAY && hasScripts {
            self.rightTopImageView.image = UIImage(named: "ic_overlay_scripted")
        } else if relay.detector & 0xff == HubConst.DETECTOR_AUTO_RELAY{
            self.rightTopImageView.image = UIImage(named: "ic_auto_overlay")
        } else {
            self.rightTopImageView.image = nil
        }
        
        if affects.contains(where: { $0._class == 2 && $0.reason == 3 }) {
            self.leftTopImageView.image = UIImage(named: "ic_overlay_offline")
        } else if affects.contains(where: { $0._class > 0 && $0._class < 4 && !($0._class == 2 && $0.reason == 3) }) {
            self.leftTopImageView.image = UIImage(named: "ic_attention_overlay")
        } else {
            self.leftTopImageView.image = nil
        }
        self.tempView.text = nil
    }
    
    func setup(zone: Zones, section: Sections, affects: [DAffectEntity], siteName: String? = nil) {
        var selectorName = DataManager.shared.d3Const.getSensorIcons(uid: Int(zone.uidType), detector: Int(zone.detector)).main
        if let alarm = affects.filter({ $0._class == 0 }).first {
            selectorName = DataManager.shared.d3Const.getEventIcons(_class: alarm._class.int, detector: alarm.detector.int, reason: alarm.reason.int, statment: alarm.active.int).main
        }
        self.iconImageView.image = UIImage(named: selectorName)
        self.titleLabel.text = zone.name

        var sname = ""
        if let siteName = siteName {
            sname = "\(siteName)\n"
            self.subtitleLabel.numberOfLines = 0
            self.titleLabel.numberOfLines = 1
        } else {
           self.subtitleLabel.numberOfLines = 1
           self.titleLabel.numberOfLines = 2
       }
        
        var rightBottomViewStateImage: String?
        switch section.detector {
        case 0, 1:
            if section.arm > 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[1]".localized()
                rightBottomViewStateImage = "ic_armed_overlay"
            } else if section.arm == 0 {
                self.subtitleLabel.text = sname + "arm_statement_titles[0]".localized()
                rightBottomViewStateImage = "ic_disarmed_overlay"
            }
        default:
            self.subtitleLabel.text = sname
            rightBottomViewStateImage = nil
        }
        
        if affects.contains(where: { $0._class == 8 && $0.reason == 6 }) {
            self.rightTopImageView.image = UIImage(named: "ic_delay_off")
        } else {
            self.rightTopImageView.image = nil
        }
        
        if affects.contains(where: { $0._class == 2 && $0.reason == 3 }) {
            self.leftTopImageView.image = UIImage(named: "ic_overlay_offline")
        } else if affects.contains(where: { $0._class > 0 && $0._class < 4 && !($0._class == 2 && $0.reason == 3) }) {
            self.leftTopImageView.image = UIImage(named: "ic_attention_overlay")
        } else {
            self.leftTopImageView.image = nil
        }
        
        var celsius: Int?
        for affect in affects {
            switch (affect._class, affect.reason) {
            case (HubConst.CLASS_TELEMETRY, HubConst.REASON_TEMPERATURE):
                if let _celsius = affect.jdata["celsius"] as? Int { celsius = _celsius }
                //TODO Zone id > 100
            default:
                break;
            }
        }
			
				if zone.bypass > 0 && section.detector == HubConst.SECTION_TYPE_SECURITY &&
						affects.contains(where: { $0._class == HubConst.CLASS_CONTROL && $0.reason == HubConst.REASON_CONTROL_ARM_BYPASS }) {
					self.rightBottomView.image = UIImage(named: "ic_bypass_overlay");
				} else {
					if let celsius = celsius { self.tempView.text = "⠀\(celsius)°"; self.rightBottomView.image = UIImage(named: "ic_nil") }
					else { self.rightBottomView.image = UIImage(named: rightBottomViewStateImage ?? "ic_nil"); self.tempView.text = nil  }
				}
    }
    
    func setup(camera: Camera) {
        self.iconImageView.image = UIImage(named: "ic_main_camera")
        self.titleLabel.text = camera.name
        self.subtitleLabel.text = ""
        self.leftTopImageView.image = nil
        self.rightTopImageView.image = nil
        self.rightBottomView.image = nil
        self.tempView.text = nil
        self.subtitleLabel.numberOfLines = 1
        self.titleLabel.numberOfLines = 2
    }

    func setupSOS(siteName: String? = nil) {
        self.iconImageView.image = UIImage(named: "ic_sos_b")
        self.titleLabel.text = siteName?.isEmpty == false ? "BAR_PANIC_MAIN_TITLE".localized() : ""
        self.subtitleLabel.text = siteName ?? ""
        self.leftTopImageView.image = nil
        self.rightTopImageView.image = nil
        self.rightBottomView.image = nil
        self.tempView.text = nil
        self.subtitleLabel.numberOfLines = 1
        self.titleLabel.numberOfLines = 2
    }
    
    func setupEmptyCamera() {
        self.iconImageView.image = UIImage(named: "ic_main_camera")
        self.titleLabel.text = "BAR_CAMERA".localized()
        self.subtitleLabel.text = ""
        self.leftTopImageView.image = nil
        self.rightTopImageView.image = nil
        self.rightBottomView.image = nil
        self.tempView.text = nil
        self.subtitleLabel.numberOfLines = 1
        self.titleLabel.numberOfLines = 2
    }
}
