//
//  SharedDasboardView.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 26.10.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation
import UIKit

protocol SharedDasboardViewDelegate {
    func openAddMenu(item: Int)
    func openSiteAlert(site: Sites)
    func openDeviceAlert(device: DDeviceEntity)
    func openSectionAlert(section: DSectionEntity)
    func openSection(section: DSectionEntity)
    func openSectionGroupAlert(name: String, sections: [Sections], siteName: String)
    func openRelayAlert(relay: DZoneWithSitesDeviceSectionInfo)
    func openRelay(relay: DZoneWithSitesDeviceSectionInfo)
    func openZone(zone: DZoneWithSitesDeviceSectionInfo)
    func openCamera(camera: Camera)
    func openDeleteAlert(item: Int)
    func openSOS(siteId: Int64)
}

class SharedDasboardView: UIView {

    var collectionView: UICollectionView!
    var delegate: SharedDasboardViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    var cameras: [Camera] = []
    var items: [Int: (f: Any, s: Any, t: Any)] = [:]
            
    private func initViews() {
        let flowLayout = XDeviceBottomListViewFlowLayout()
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width/2 - 10, height: 180)
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .white
        collectionView.register(UINib(nibName: "DashboardEmptyCell", bundle: nil), forCellWithReuseIdentifier: "DashboardEmptyCellIdentifier")
        collectionView.register(UINib(nibName: "DashboardCell", bundle: nil), forCellWithReuseIdentifier: "DashboardCellIdentifier")
        collectionView.alwaysBounceVertical = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 00, right: 0)
        addSubview(collectionView)
    }
    
    private func setConstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

extension SharedDasboardView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let emptyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardEmptyCellIdentifier", for: indexPath) as! DashboardEmptyCell

        let siteId = -777
        guard let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)") else {
            return emptyCell
        }

        switch objectType {
        case "site":
            guard let site = items[indexPath.item]?.f as? Sites,
                  let armStatus = items[indexPath.item]?.s as? DSiteEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(site: site, armStatus: armStatus, affects: affects, isShared: true)
            return cell
        case "device":
            guard let device = items[indexPath.item]?.f as? DDeviceEntity,
                  let armStatus = items[indexPath.item]?.s as? DDeviceEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(device: device.device!, armStatus: armStatus, affects: affects, siteName: device.siteNames)
            return cell
        case "section":
            guard let section = items[indexPath.item]?.f as? DSectionEntity,
                  let affects = items[indexPath.item]?.s as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(section: section.section, affects: affects, siteName: section.siteNames)
            return cell
        case "section_group":
            guard let sectionGroup = items[indexPath.item]?.f as? SectionGroups,
                  let armStatus = items[indexPath.item]?.s as? DSiteEntity.ArmStatus,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(sectionGroup: sectionGroup, armStatus: armStatus, affects: affects)
            return cell
        case "relay":
            guard let relay = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo,
                  let hasScripts = items[indexPath.item]?.s as? Bool,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }
            
            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(relay: relay.zone, hasScripts: hasScripts, affects: affects, siteName: relay.siteNames)
            return cell
       case "zone":
            guard let zone = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo,
                  let section = items[indexPath.item]?.s as? Sections,
                  let affects = items[indexPath.item]?.t as? [DAffectEntity] else {
                return emptyCell
            }
            
            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setup(zone: zone.zone, section: section, affects: affects, siteName: zone.siteNames)
            return cell
        case "camera":
            let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
            guard let cameraId = objectId else {
                userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)")
                userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
                return emptyCell
            }
            let cell = dequeueReusableCell(indexPath: indexPath)
            if let camera = cameras.first(where: { $0.id == cameraId }) {
                cell.setup(camera: camera)
            } else {
                cell.setupEmptyCamera()
            }
            return cell
        case "sos":
            guard let siteName = items[indexPath.item]?.s as? String else {
                return emptyCell
            }

            let cell = dequeueReusableCell(indexPath: indexPath)
            cell.setupSOS(siteName: siteName)
            return cell
        default:
            return emptyCell
        }
    }
    
    private func dequeueReusableCell(indexPath: IndexPath) -> DashboardCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCellIdentifier", for: indexPath) as! DashboardCell
        cell.tag = indexPath.item
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(sender:)))
        cell.addGestureRecognizer(longPress)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapPressed(sender:)))
        cell.addGestureRecognizer(tap)
        return cell
    }
    
    @objc func tapPressed(sender: UITapGestureRecognizer) {
        guard let item = sender.view?.tag else { return }
        collectionView(collectionView, didSelectItemAt: IndexPath(row: item, section: 0))
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == .began, let cellTag = sender.view?.tag {
            delegate?.openDeleteAlert(item: cellTag)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let siteId = -777
        guard let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(indexPath.item)") else {
            delegate?.openAddMenu(item: indexPath.row)
            return
        }
        switch objectType {
        case "site":
            guard let site = items[indexPath.item]?.f as? Sites else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }
            delegate?.openSiteAlert(site: site)
        case "device":
            guard let device = items[indexPath.item]?.f as? DDeviceEntity else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }
            delegate?.openDeviceAlert(device: device)
        case "section":
            guard let section = items[indexPath.item]?.f as? DSectionEntity else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }

            if section.section.detector == 0 || section.section.detector == 1 {
                delegate?.openSectionAlert(section: section)
            } else {
                delegate?.openSection(section: section)
            }
        case "section_group":
            guard let sectionGroup = items[indexPath.item]?.f as? SectionGroups else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }

            DataManager.shared.getSectionGroupSections(id: sectionGroup.id) { [weak self] sections in
                self?.delegate?.openSectionGroupAlert(name: sectionGroup.name, sections: sections, siteName: sectionGroup.siteName)
            }
        case "relay":
            guard let relay = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }

            if relay.zone.detector & 0xff == HubConst.DETECTOR_MANUAL_RELAY {
                delegate?.openRelayAlert(relay: relay)
            } else {
                delegate?.openRelay(relay: relay)
            }
        case "zone":
            guard let zone = items[indexPath.item]?.f as? DZoneWithSitesDeviceSectionInfo else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }
            
            delegate?.openZone(zone: zone)
        case "camera":
            let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
            guard let cameraId = objectId,
                  let camera = cameras.first(where: { $0.id == cameraId }) else {
                delegate?.openAddMenu(item: indexPath.row)
                return
            }
            delegate?.openCamera(camera: camera)
        case "sos":
            let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(indexPath.item)")
            delegate?.openSOS(siteId: objectId.int64)
        default:
            delegate?.openAddMenu(item: indexPath.row)
        }
    }
}
