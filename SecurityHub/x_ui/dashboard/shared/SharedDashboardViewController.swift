//
//  SharedDashboardViewController.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 25.10.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class SharedDashboardViewController: XBaseViewController<SharedDasboardView> {
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.showAddMenuController)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        
        self.mainView.items = [:]
        self.mainView.collectionView?.reloadData()
        
        setupCollectionView()
        loadCameras()
        
        setupNotifications()
    }
    
    private func setupNotifications() {
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: NSNotification.Name("updateDashboard"), object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.siteUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.deviceUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.sectionsUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateDashboard), name: HubNotification.zonesUpdate, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupCollectionView()
    }
    
    private var updateDashboardCounter: Int64 = 1
    private var updateDashboardTimer: Timer?
    private let updateDashboardLock = NSLock()
    @objc func updateDashboard() {
        updateDashboardLock.lock()
        updateDashboardCounter += 1
        updateDashboardTimer?.invalidate()
        if updateDashboardCounter % 100 == 0 {
            DispatchQueue.main.async {
                self.setupCollectionView()
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.updateDashboardTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                    self?.setupCollectionView()
                }
            }
        }
        updateDashboardLock.unlock()
    }

    private func resetItems() {
        let count = userDefaults().integer(forKey: "SharedDashboardCount")

        mainView.items = [:]

        if count > 0 {
            for index in 0...(count - 1) {
                mainView.items[index] = (f: 0, s: 0, t: 0)
            }
        }

        UIView.performWithoutAnimation {
            self.mainView.collectionView?.reloadData()
        }
    }

    var setupCollectionViewCounter = 0
    private func setupCollectionView() {
        setupCollectionViewCounter += 1
        let count = userDefaults().integer(forKey: "SharedDashboardCount")
        guard count > 0 else {
            self.mainView.items = [:]
            self.mainView.collectionView?.reloadData()

            return
        }
        
        if self.mainView.items.count > count {
            for index in count...(self.mainView.items.count - 1) {
                self.mainView.items[index] = nil
                self.mainView.collectionView?.deleteItems(at: [IndexPath(item: index, section: 0)])
            }
        }

        let siteId = -777
        for index in 0...(count - 1) {
            guard let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(index)") else {
                continue
            }
            switch objectType {
            case "site":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getSiteEntity(site_id: objectId.int64) { [weak self, setupCollectionViewCounter] si in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if objectId > 0, let si = si, let site = si.site  {
                        self?.mainView.items[index] = (f: site, s: si.armStatus, t: si.affects)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "device":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getDeviceEntity(device_id: objectId.int64) { [weak self, setupCollectionViewCounter] de in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if objectId > 0, let de = de, de.device != nil {
                        self?.mainView.items[index] = (f: de, s: de.armStatus, t: de.affects)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "section":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                
                DataManager.shared.getSectionEntity(l_section_id: objectId.int64) { [weak self, setupCollectionViewCounter] se in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if objectId > 0, let se = se {
                        self?.mainView.items[index] = (f: se, s: se.affects, t: 0)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "section_group":
                let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
               
                DataManager.shared.getSectionGroupEntity(id: objectId.int64) { [weak self, setupCollectionViewCounter] sg in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if objectId > 0, let sg = sg {
                        self?.mainView.items[index] = (f: sg.sectionGroup, s: sg.armStatus, t: sg.affects)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "relay":
                let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(index)") ?? "0"
                DataManager.shared.getDashboardRelay(id: objectId) { [weak self, setupCollectionViewCounter] re in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if let re = re {
                        self?.mainView.items[index] = (f: re.relay, s: re.hasScripts, t: re.affects)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "zone":
                let objectId = userDefaults().string(forKey: "DashboardItemId_\(siteId)_\(index)") ?? "0"
                DataManager.shared.getDahsboardZone(id: objectId) { [weak self, setupCollectionViewCounter] ze in
                    guard setupCollectionViewCounter == self?.setupCollectionViewCounter else {
                        return
                    }

                    if let ze = ze {
                        self?.mainView.items[index] = (f: ze.zone, s: ze.section, t: ze.affects)
                        self?.mainView.collectionView?.reloadItems(at: [IndexPath(item: index, section: 0)])
                    } else {
                        deleteSharedDashboard(item: index)
                        self?.setupCollectionView()
                    }
                }
            case "sos":
                DispatchQueue.global().async {
                    let objectId = userDefaults().integer(forKey: "DashboardItemId_\(siteId)_\(index)")
                    let siteName = DataManager.shared.dbHelper.getSite(site_id: objectId.int64)?.name ?? ""
                    DispatchQueue.main.async { [weak self] in
                        self?.mainView.items[index] = (f: "SOS", s: siteName, t: 0)
                        self?.mainView.collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                    }
                }
            default:
                continue
            }
            mainView.items[index] == nil ? mainView.items[index] = (f: 0, s: 0, t: 0) : ()
        }
        mainView.collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [weak self] in
            self?.mainView.collectionView.reloadData()
        })
    }
    
    private func loadCameras(){
        _ = DataManager.shared.getCameraList().subscribe(onNext: { [weak self] (cameras) in
            guard let cameras = cameras else {
                self?.mainView.cameras = []
                return
            }
            self?.mainView.cameras = cameras
            self?.updateDashboard()
        },onError: { [weak self] error in
            self?.mainView.cameras = []
        })
    }
    
    // MARK: - Routing
    
    private func showAddMenuController() {
        let controller = XAddMenuController()
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension SharedDashboardViewController: SharedDasboardViewDelegate {
    
    func openSection(section: DSectionEntity) {
        openSection(section: section.section)
    }
    
    func openRelay(relay: DZoneWithSitesDeviceSectionInfo) {
        openRelay(relay: relay.zone)
    }
    
    func openZone(zone: DZoneWithSitesDeviceSectionInfo) {
        openZone(zone: zone.zone)
    }
    
    func openDeleteAlert(item: Int) {
        let siteId = -777
        guard let _ = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(item)") else {
            return
        }
        let alertStrings = XAlertView.Strings(title: "N_MAIN_DELETE_MESSAGE".localized(), text: nil, positive: "DELETE".localized(), negative: "ADB_CANCEL".localized())
        let alertVC = XAlertController(style: baseAlertStyle(), strings: alertStrings) { [weak self] in
            deleteSharedDashboard(item: item)

            self?.setupCollectionView()
        } negative: {}
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func openSiteAlert(site: Sites) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: site.name, siteName: "N_MAIN_TITLE_FULL_SITE".localized(), hideInfoButton: true)
        alert.onArmed = { [weak self] in
						var waitNotReadyHappen = false
            for device in DataManager.shared.getDevices(site_id: site.id) {
                _ = self?.doOnConnect(deviceId: device.id)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.arm(device: $0) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        alert.onDisarmed = { [weak self] in
            for device in DataManager.shared.getDevices(site_id: site.id) {
								var waitNotReadyHappen = false
                _ = self?.doOnConnect(deviceId: device.id)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.disarm(device: $0) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        self.present(alert, animated: true)
    }
    
    func openDeviceAlert(device: DDeviceEntity) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: device.device!.name,
                                        siteName: "\(device.siteNames)\n\( "controller".localized() )")
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: device.id)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.arm(device: $0) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: device.id)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.disarm(device: $0) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XDeviceController(deviceId: device.id.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }
    
    func openSectionAlert(section: DSectionEntity) {
        var alert_type: XArmAlertControllerType = .security
        if section.section.detector == 6 { alert_type = .tech }
        let alert = XArmAlertController(type: alert_type, deviceName: section.section.name,
                                        siteName: "\(section.siteNames)\n\( "PARTITION".localized() )")
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: section.deviceId)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.arm(device: $0, section: section.section.section) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: section.deviceId)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in
									self?.waitNotReadyZone = true
									self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.disarm(device: $0, section: section.section.section) })
                .do(onSuccess: { [weak self] result in
									if result.success { self?.waitNotReadyZone = false }
									self?.resultProcessing(result) })
                .do(onError: { [weak self] error in
									self?.waitNotReadyZone = false
									self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XSectionController(deviceId: section.section.device.int, sectionId: section.section.section.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }
    
    func openSection(section: Sections) {
        let controller = XSectionController(deviceId: section.device.int, sectionId: section.section.int)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openSectionGroupAlert(name: String, sections: [Sections], siteName: String) {
        let alert_type: XArmAlertControllerType = .security
        let alert = XArmAlertController(type: alert_type, deviceName: name,
                                        siteName: siteName + "\n" + "BAR_ARM_GROUP".localized(), hideInfoButton: true)
        alert.onArmed = { [weak self] in
						var waitNotReadyHappen = false
            for section in sections {
                _ = self?.doOnConnect(deviceId: section.device)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.arm(device: $0, section: section.section) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        alert.onDisarmed = { [weak self] in
						var waitNotReadyHappen = false
            for section in sections {
                _ = self?.doOnConnect(deviceId: section.device)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in
											if !waitNotReadyHappen {
												self?.waitNotReadyZone = true
												waitNotReadyHappen = true }
											self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ DataManager.shared.disarm(device: $0, section: section.section) })
                    .do(onSuccess: { [weak self] result in
											if result.success { self?.waitNotReadyZone = false }
											self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in
											self?.waitNotReadyZone = false
											self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        }
        self.present(alert, animated: true)
    }
    
    func openRelayAlert(relay: DZoneWithSitesDeviceSectionInfo) {
        let alert_type: XArmAlertControllerType = .relay
        let alert = XArmAlertController(type: alert_type, deviceName: relay.zone.name,
                                        siteName: relay.siteNames + "\n" + "CONTROLLER".localized())
        alert.onArmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: relay.zone.device)
               .observeOn(ThreadUtil.shared.mainScheduler)
               .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.switchRelay(device: $0, section: relay.zone.section, zone: relay.zone.zone, state: 1) })
                .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                .do(onError: { [weak self] error in self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onDisarmed = { [weak self] in
            _ = self?.doOnConnect(deviceId: relay.zone.device)
               .observeOn(ThreadUtil.shared.mainScheduler)
               .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ DataManager.shared.switchRelay(device: $0, section: relay.zone.section, zone: relay.zone.zone, state: 0) })
                .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                .do(onError: { [weak self] error in self?.errorProcessing(error) })
                .asCompletable().subscribe()
        }
        alert.onInfoTapped = {
            let controller = XZoneController(deviceId: relay.zone.device.int, sectionId: relay.zone.section.int, zoneId: relay.zone.zone.int)
            self.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(alert, animated: true)
    }
    
    func openRelay(relay: Zones) {
        let controller = XZoneController(deviceId: relay.device.int, sectionId: relay.section.int, zoneId: relay.zone.int)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openZone(zone: Zones) {
        let controller = XZoneController(deviceId: zone.device.int, sectionId: zone.section.int, zoneId: zone.zone.int)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openCamera(camera: Camera) {
        _ = DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onSuccess: { [weak self] token in
                let model = XCameraViewLayerModel(type: .ivideon, id: camera.id, name: camera.name, url: camera.getPreviewUrl(accessToken: token ?? ""))
                let controller = XCameraPreviewController(model: model)
                self?.navigationController?.pushViewController(controller, animated: true)
            })
    }

    func openSOS(siteId: Int64) {
        let alert_type: XArmAlertControllerType = .sos
        var siteName = DataManager.shared.dbHelper.getSite(site_id: siteId)?.name ?? ""
        if siteName.count > 0 {
            siteName += "\n\("BAR_PANIC_DIALOG_MESSAGE".localized())"
        }
        let alert = XArmAlertController(type: alert_type, deviceName: "BAR_PANIC_MAIN_TITLE".localized(), siteName: siteName)
        alert.onSos = { [weak self] location in
            guard let device_id = DataManager.shared.getHubDeviceIds(site_id: siteId).min() else {
                return
            }
					
						var dParam: [String: Any] = ["device_id":device_id]
						if let location = location {
							dParam ["lat"] = location.latitude
							dParam ["lon"] = location.longitude
						}
            _ = DataManager.shared.hubHelper.setCommandWithResult(.SOS, D: dParam)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] result in self?.showErrorColtroller(message: result.success ? "BAR_PANIC_SUCCESS_MESSAGE".localized() : result.message) })
                .do(onError: self?.errorProcessing)
                .asCompletable().subscribe()
        }
				self.present(alert, animated: true)
    }
    
    func openAddMenu(item: Int) {
        let siteId = -777
        let addVC = DashboardAddMainViewController(item: item, siteId: siteId.int64, delegate: self)
        self.navigationController?.pushViewController(addVC, animated: true)
    }
}

extension SharedDashboardViewController: DashboardAddViewControllerDelegate {
    private func addCount(item: Int) {
        let count = userDefaults().integer(forKey: "SharedDashboardCount")
        if item == count {
            userDefaults().set(count + 1, forKey: "SharedDashboardCount")
        }
    }
    
    func siteSelected(site: Sites, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("site", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(site.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func siteSelected(item: Int) { }
    
    func deviceSelected(device: Devices, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("device", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(device.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func sectionSelected(section: Sections, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("section", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(section.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func sectionGroupSelected(sectionGroup: SectionGroups, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("section_group", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(sectionGroup.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func relaySelected(relay: Zones, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("relay", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(relay.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func zoneSelected(zone: Zones, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("zone", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(zone.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
    
    func cameraSelected(camera: Camera, item: Int) {
        addCount(item: item)

        let siteId = -777
        let objectTypeKeyName = "DashboardItemType_\(siteId)_\(item)"
        userDefaults().setValue("camera", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(siteId)_\(item)"
        userDefaults().setValue(camera.id, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }

    func sosSelected(siteId: Int64, item: Int) {
        addCount(item: item)

        let objectTypeKeyName = "DashboardItemType_\(-777)_\(item)"
        userDefaults().setValue("sos", forKey: objectTypeKeyName)
        let objectIdKeyName = "DashboardItemId_\(-777)_\(item)"
        userDefaults().setValue(siteId, forKey: objectIdKeyName)
        self.resetItems()
        self.setupCollectionView()
    }
}

func deleteSharedDashboard(item: Int) {
    let siteId = -777
    let count = userDefaults().integer(forKey: "SharedDashboardCount")

    userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(item)")
    userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(item)")
    
    var array: [(Any, String)] = []
    for i in 0...count-1 {
        guard
            let objectId = userDefaults().value(forKey: "DashboardItemId_\(siteId)_\(i)"),
            let objectType = userDefaults().string(forKey: "DashboardItemType_\(siteId)_\(i)")
        else {
            continue
        }
        array.append((objectId, objectType))
    }

    var i = 0
    for a in array {
        userDefaults().setValue(a.0, forKey: "DashboardItemId_\(siteId)_\(i)")
        userDefaults().setValue(a.1, forKey: "DashboardItemType_\(siteId)_\(i)")
        i+=1
    }
    
    if i < count {
        for j in i...(count-1) {
            userDefaults().removeObject(forKey: "DashboardItemType_\(siteId)_\(j)")
            userDefaults().removeObject(forKey: "DashboardItemId_\(siteId)_\(j)")
        }
    }

    userDefaults().set(i, forKey: "SharedDashboardCount")
}
