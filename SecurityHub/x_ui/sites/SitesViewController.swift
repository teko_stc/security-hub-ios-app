//
//  SitesViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 20.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol SitesViewControllerDelegate: AnyObject {
    func siteSelected(site: Sites)
}

class SitesViewController: UIViewController {
    
    private let sites: [Sites]
    private let selectedIndex: Int
    
    weak var delegate: SitesViewControllerDelegate?

    @IBOutlet weak var tableView: UITableView!
    
    init(sites: [Sites], selectedIndex: Int) {
        self.sites = sites
        self.selectedIndex = selectedIndex
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    private func setup() {
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "SiteCell", bundle: nil), forCellReuseIdentifier: "SiteCellIdentifier")
        
        tableView.selectRow(at: IndexPath(row: selectedIndex, section: 0), animated: false, scrollPosition: .top)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension SitesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SiteCellIdentifier") as? SiteCell {
            cell.nameLabel.text = sites[indexPath.row].name
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.siteSelected(site: sites[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}
