//
//  SiteCell.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class SiteCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectionView: UIView!
    
    private let grayColor = UIColor.colorFromHex(0x414042)
    private let whiteColor = UIColor.white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        nameLabel.textColor = selected ? whiteColor : grayColor
        selectionView.isHidden = !selected
    }
    
}
