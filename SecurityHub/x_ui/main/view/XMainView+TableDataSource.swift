//
//  XMainView+TableDataSource.swift
//  SecurityHub
//
//  Created by Timerlan on 07.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import SwipeCellKit

protocol XMainTableProtocol {
    func setSearchBarHidden(_  hidden: Bool)
    func initOTable()
    func initETable()
    func addObject(_ object: XObjectEntity)
    func deleteObject(id: Int64)
    func addEvent(_ event: Events, type: NUpdateType, isFromBD: Bool)
}

extension XMainView: XMainTableProtocol, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == oTableView ? olist.count : elist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView == oTableView ? tableView.dequeueReusableCell(withIdentifier: XObjectCellId, for: indexPath) :
                                         tableView.dequeueReusableCell(withIdentifier: EventBigCell.cellId, for: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == oTableView ? CGFloat(olist[indexPath.row].foldingInfo.height) : EventBigView.height
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? XObjectCell {
            cell.delegate = self
            cell.setContent(self.olist[indexPath.row])
            cell.setOpen(olist[indexPath.row].foldingInfo.isOpen)
        }else if let cell = cell as? EventBigCell {
            cell.setContent(elist[indexPath.row])
            cell.setNV(xN)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == oTableView {
            guard olist[indexPath.row].isFolding else { return }
            scrollEnable = false
            UIView.performWithoutAnimation {
                olist[indexPath.row].foldingInfo.isOpen = !olist[indexPath.row].foldingInfo.isOpen
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            scrollEnable = true
        } else {
            //MARK: //TODO:
            tableView.cellForRow(at: indexPath)?.isSelected = false
            if let cell = tableView.cellForRow(at: indexPath) as? EventBigCell,
                let site = cell.mainView.siteView.text,
                let device = cell.mainView.deviceView.text,
                let section = cell.mainView.sectionZoneView.text {
                let event = elist[indexPath.row]
                let time = (event.time == 0 ? "--:--:--" : event.time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short))
                let text = "\(site)\n\(device)\n\(section)\n\n\(event.affect_desc)\n\n\(time)\n"
                AlertUtil.infoAlert(title: "INFORMATION".localized(), text)
            }
        }
    }
    
    //MARK: - свайпы туда-сюда
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let oe = olist[indexPath.row]
        if oe.menu.count == 0 { return nil }
        
        let manageAction = SwipeAction(style: .default, title: "BAR_CONTROL".localized()) { action, indexPath in
            var messages: [String] = []
            var voids: [(()->Void)] = []
            oe.menu.forEach({ (menu) in
                messages.append(menu.title)
                voids.append { menu.void(menu.obj) }
            })
            let mAlert = AlertUtil.myXAlert(oe.title, messages: messages, voids: voids)
            self.xN.present(mAlert, animated: false)
        }
        
        manageAction.backgroundColor = DEFAULT_COLOR_WINDOW_ALT_BACKGROUND
        manageAction.textColor = DEFAULT_COLOR_GRAY
    
        manageAction.image = XImages.button_manage //.resizeImage(targetSize: CGSize(width: 30, height: 30))
        return [manageAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        return options
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if !scrollEnable { return }
//        let tableView = segmentControl.selectedSegmentIndex == 0 ? oTableView : eTableView
//        let y = segmentControl.selectedSegmentIndex == 0 ? oY : eY
//
//        let ty = tableView.contentOffset.y, srh = searchHeight, seh = segmentHeight
//        let sy: CGFloat = ty + seh, sgHeight: CGFloat = sy < 0 && sy > -srh ? -sy : sy >= 0 ? 0 : srh
//        let sait = seh + (sy < 0 && sy > -srh ? -sy : sy >= 0 ? 0 : srh)
//        xC.additionalSafeAreaInsets.top = sait
//        searchBar.setHeight(sgHeight)
//
//        var tfh = tableView.frame.height, tch: CGFloat = 0
//        if segmentControl.selectedSegmentIndex == 0 {
//            olist.forEach { (o) in tch += CGFloat(o.foldingInfo.height) }
//        } else {
//            tch = CGFloat(elist.count) * EventBigView.height
//        }
//        let _y = tableView.contentOffset.y + sait
//        if (tch + 250 < tfh) {
//            setToolbarHidden(false, y: 0)
//        } else if (_y >= 0 && _y < tch - tfh){
//            if (y > _y + 250) { setToolbarHidden(false, y: _y) }
//            else if (y < _y - 250){ setToolbarHidden(true, y: _y) }
//        }

//        NEED TO SEARCHVIEW
//        scrollViewEndScrollDetecting()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollViewEndScrollDetecting()
    }

    private func scrollViewEndScrollDetecting(){
        timerDisp?.dispose()
        timerDisp = Observable<Int64>.timer(.milliseconds(300), scheduler: ConcurrentDispatchQueueScheduler.init(qos: .background))
            .observeOn(MainScheduler.init())
            .subscribe(onNext: { t in
                if (!self.oTableView.isDragging) { self.scrollViewEndScroll() }
                else { self.scrollViewEndScrollDetecting() }
            }, onError: { e in
                if (!self.oTableView.isDragging) { self.scrollViewEndScroll() }
                else { self.scrollViewEndScrollDetecting() }
            })
    }
    
    private func scrollViewEndScroll(){
        if !(searchBar.frame.height != 0 && searchBar.frame.height != searchHeight) { return }
        setSearchBarHidden(searchBar.frame.height < searchHeight / 2)
    }
    
    func setSearchBarHidden(_ hidden: Bool){
        let scrollY = -(hidden ? segmentHeight : segmentHeight + searchHeight)
        oTableView.setContentOffset(CGPoint(x: 0, y: scrollY), animated: true)
        eTableView.setContentOffset(CGPoint(x: 0, y: scrollY), animated: true)
    }
    
    func initOTable(){
        setToolbarHidden(false, y: 0)
        olist = []
        oTableView.dataSource = self
        oTableView.delegate = self
        oTableView.register(XObjectCell.self, forCellReuseIdentifier: XObjectCellId)
        oTableView.reloadData()
    }
    
    func initETable(){
        elist = []
        eTableView.dataSource = self
        eTableView.delegate = self
        eTableView.register(EventBigCell.self, forCellReuseIdentifier: EventBigCell.cellId)
        eTableView.reloadData()
    }

    
    func addEvent(_ event: Events, type: NUpdateType, isFromBD: Bool) {
        switch type {
        case .insert:
            if let max = elist.map({ (e) -> Int64 in e.time }).max(), max <= event.time, !isFromBD, eTUDisp == nil {
                elist.insert(event, at: 0)
                eTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }else {
                elist.append(event)
                eTableUpdateEndDetecting()
            }
        case .update:
            guard let pos = elist.firstIndex(where: {$0.id == event.id}) else{ break }
            elist[pos] = event
            eTableUpdateEndDetecting()
        case .delete: break;
        }
    }
    
    private func eTableUpdateEndDetecting(){
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.elist = self.elist.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.eTableView.reloadData()
                self.eTUDisp = nil
            }, onError: { e in
                self.eTUDisp = nil
            })
    }

    func addObject(_ object: XObjectEntity) {
        scrollEnable = false
        let pos = olist.firstIndex(where: {$0.id == object.id})
        if let pos = pos, object.updateType == .delete {
            olist.remove(at: pos)
            oTableView.beginUpdates()
            oTableView.deleteRows(at: [IndexPath(row: pos, section: 0)], with: .left)
            oTableView.endUpdates()
        } else if object.updateType == .insert && pos == nil {
            olist.append(object)
            oTableView.beginUpdates()
            oTableView.insertRows(at: [IndexPath(row: olist.count - 1, section: 0)], with: .right);
            oTableView.endUpdates()
        } else if let pos = pos {
            let isOpen = olist[pos].foldingInfo.isOpen
            olist[pos] = object
            olist[pos].foldingInfo.isOpen = isOpen
            UIView.performWithoutAnimation {
                oTableView.reloadRows(at: [IndexPath(row: pos, section: 0)], with: .none)
            }
        }
        scrollEnable = true
    }
    
    func deleteObject(id: Int64) { addObject(XMainCellBaseEntityBuilder.create(id: id)) }
}
