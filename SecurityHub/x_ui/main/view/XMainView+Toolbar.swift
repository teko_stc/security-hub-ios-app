//
//  XMainView+Toolbar.swift
//  SecurityHub
//
//  Created by Timerlan on 07.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController

protocol XMainToolbarProtocol {
    var  segmentHeight: CGFloat { get }
    var  searchHeight: CGFloat { get }
    func setToolbar(controller: XBaseController<XMainView>,
                    items: [XSpinerItem],
                    onSpin: @escaping ((_ item: XSpinerItem)->Void),
                    onEventSegmentClick: @escaping (()->Void),
                    onObjectSegmentClick: @escaping (()->Void))
    func setToolbarHidden(_ hidden: Bool, y: CGFloat?)
    func setToolbarHidden(_ hidden: Bool)
    func clearToolbar()
}

extension XMainView: XMainToolbarProtocol, UISearchBarDelegate {
    var segmentHeight: CGFloat { return 40 }
    var searchHeight: CGFloat { return 0 } //48
    
    private func initSegment(onEventSegmentClick: @escaping (()->Void), onObjectSegmentClick: @escaping (()->Void)){
        self.onEventSegmentClick = onEventSegmentClick
        self.onObjectSegmentClick = onObjectSegmentClick
        xC.additionalSafeAreaInsets.top = segmentHeight
        xC.view.addSubview(segmentBlock)
        segmentBlock.snp.remakeConstraints{ (make) in
            make.width.equalToSuperview()
            make.top.equalTo(0)
            make.height.equalTo(segmentHeight)
        }
        segmentControl.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(segmentHeight - XSizes.MARGIN_SMALL)
            make.width.equalToSuperview().offset(-XSizes.MARGIN_SMALL * 2)
            make.bottom.equalTo(-XSizes.MARGIN_SMALL)
        }
        segmentControl.addTarget(self, action: #selector(segmentValueChanged), for: .valueChanged)
    }
    @objc private func segmentValueChanged(segment: UISegmentedControl){
        tabTapped()
        oTableView.isHidden = segment.selectedSegmentIndex == 1
        eTableView.isHidden = segment.selectedSegmentIndex == 0
        if segment.selectedSegmentIndex == 1 {
            onEventSegmentClick?()
        } else {
            onObjectSegmentClick?()
        }
    }
    
    private func initSpinner(items: [XSpinerItem], onSpin: @escaping ((_ item: XSpinerItem)->Void)){
        let tBar = xN.tabBarController!.tabBar
        spinerView = XSpinerView(rootView: xC.view)
        spinerView.tag = XMainView.TOP_TAG
        segmentControl.setTitle(items.first?.subTitle ?? "", forSegmentAt: 0)
        spinerView.onSpin = { item in
            self.segmentControl.setTitle(item.subTitle, forSegmentAt: 0)
            self.segmentControl.selectedSegmentIndex = 0
            self.oTableView.isHidden = false
            self.eTableView.isHidden = true
            onSpin(item)
        }
        spinerView.items = items
        xN.navigationBar.addSubview(spinerView)
        spinerView!.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalToSuperview()
            make.width.equalTo(200)
        }
        addGestureRecognizer(oTableView.superview!, void: #selector(tableTapped))
        addGestureRecognizer(tBar, void: #selector(tabTapped))
    }
    private func addGestureRecognizer(_ view: UIView, void: Selector){
        let dismissTap = UITapGestureRecognizer(target: self, action: void)
        dismissTap.cancelsTouchesInView = false
        view.addGestureRecognizer(dismissTap)
    }
    @objc private func tableTapped(){ xC.view.endEditing(true) }
    @objc func tabTapped(){
        spinerView?.isOpen = false
        xC.view.endEditing(true)
    }
    
    private func initSearch(){
        xC.view.addSubview(searchBar)
        searchBar.snp.remakeConstraints { (make) in
            make.top.equalTo(segmentHeight)
            make.left.right.width.equalToSuperview()
            make.height.equalTo(0)
        }
        searchBar.textField.snp.remakeConstraints{ (make) in
            make.height.equalTo(0)
            make.width.equalToSuperview().offset(-2 * XSizes.MARGIN_SMALL)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-0.5 * XSizes.MARGIN_SMALL)
        }
        searchBar.delegate = self
    }
    
    func setToolbar(controller: XBaseController<XMainView>,
                    items: [XSpinerItem],
                    onSpin: @escaping ((_ item: XSpinerItem)->Void),
                    onEventSegmentClick: @escaping (()->Void),
                    onObjectSegmentClick: @escaping (()->Void)){
        xC = controller
        xN.navigationBar.shadowImage = UIImage()
        initSegment(onEventSegmentClick: onEventSegmentClick, onObjectSegmentClick: onObjectSegmentClick)
        initSearch()
        initSpinner(items: items, onSpin: onSpin)
        onSpin(items.first!)
    }
    
    func clearToolbar() {
        xN.navigationBar.subviews.filter { view in return view.tag == XMainView.TOP_TAG }.forEach { view in view.removeFromSuperview() }
        xC.view.subviews.filter { view in return view.tag == XMainView.TOP_TAG }.forEach { view in view.removeFromSuperview() }
    }
    
    func setToolbarHidden(_ hidden: Bool) { setToolbarHidden(hidden, y: nil) }
    
    func setToolbarHidden(_ hidden: Bool, y: CGFloat?) {
        let y = y ?? (segmentControl.selectedSegmentIndex == 0 ? oTableView.contentOffset.y : eTableView.contentOffset.y)
        if segmentControl.selectedSegmentIndex == 0 { oY = y }else { eY = y }
        segmentBlock.layer.isHidden = hidden
        if isToolbarHidden == hidden { return }
        if let animatedTabBar = xN.tabBarController as? MenuController {
            animatedTabBar.setTabBarHidden(hidden)
        }
        NavigationHelper.shared.statusBar(isHidden: !hidden)
        xN.setNavigationBarHidden(hidden, animated: true)
        isToolbarHidden = hidden
        segmentControl.isHidden = hidden
        xC.additionalSafeAreaInsets.top = hidden ? 0 : (segmentHeight + searchHeight)
        segmentBlock.snp.updateConstraints{ (make) in
            make.height.equalTo(hidden ? UIApplication.shared.statusBarFrame.height : segmentHeight)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchBar.update()
    }
}
