//
//  XMainView.swift
//  SecurityHub
//
//  Created by Timerlan on 06.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XMainViewProtocol {
    func viewWillAppear()
    func viewWillDisappear()
    func tabItemClick()
}

class XMainView: XBaseView, XMainViewProtocol {
    static let TOP_TAG = 12445
    var olist: [XObjectEntity] = []
    var elist: [Events] = []
    var isToolbarHidden: Bool = false
    var oY: CGFloat = 0
    var eY: CGFloat = 0
    var scrollEnable = true
    var timerDisp: Disposable?
    var eTUDisp: Disposable?
    var xC: XBaseController<XMainView>! { didSet{ xN = xC.navigationController } }
    var xN: UINavigationController!
    var spinerView: XSpinerView!
    var onEventSegmentClick: (()->Void)?
    var onObjectSegmentClick: (()->Void)?
    lazy var segmentBlock: UIView = {
        let view = UIView()
        view.tag = XMainView.TOP_TAG
        let layerBottom = CALayer()
        layerBottom.frame = CGRect(x: 0, y: segmentHeight, width: UIScreen.main.bounds.width, height: 0.7)
        layerBottom.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        view.backgroundColor = UIColor.white
        view.addSubview(segmentControl)
        return view
    }()
    lazy var segmentControl: UISegmentedControl = {
        let view = UISegmentedControl(items: ["SUNTITLE_OBJECTS".localized(), "SUBTITLE_EVENTS".localized()])
        view.tintColor = DEFAULT_COLOR_TEXT_LIGHT
        view.selectedSegmentIndex = 0
        return view
    }()
    lazy var oTableView : UITableView = {
        let view = UITableView()
//        view.rowHeight = UITableView.automaticDimension
        view.backgroundColor = DEFAULT_BACKGROUND
        view.separatorStyle = .none
        view.estimatedRowHeight = 130
        return view
    }()
    lazy var eTableView : UITableView = {
        let view = UITableView()
        //        view.rowHeight = UITableView.automaticDimension
        view.backgroundColor = DEFAULT_BACKGROUND
        view.separatorStyle = .none
        view.isHidden = true
        view.estimatedRowHeight = 130
        return view
    }()
    lazy var searchBar: XSearchBar = {
        let view = XSearchBar()
        view.tag = XMainView.TOP_TAG
        view.Height = searchHeight
        view.searchBarStyle = UISearchBar.Style.prominent
        view.placeholder = "searchBar_placeholder".localized()
        view.barTintColor = DEFAULT_COLOR_MAIN
        view.isTranslucent = false
        view.backgroundImage = UIImage()
        return view
    }()
  
    override func setContent() {
        xView.addSubview(oTableView)
        xView.addSubview(eTableView)
    }
    
    override func setConstraints() {
        oTableView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
        eTableView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    func viewWillAppear() {
        NavigationHelper.shared.statusBar(isHidden: true)
        xN.setNavigationBarHidden(isToolbarHidden, animated: false)
        segmentControl.isHidden = isToolbarHidden
        if let animatedTabBar = xN.tabBarController as? MenuController {
            animatedTabBar.setTabBarHidden(isToolbarHidden)
        }
        spinerView.isHidden = false
    }
    
    func viewWillDisappear() {
//        clearToolbar()
        NavigationHelper.shared.statusBar(isHidden: true)
        spinerView.isOpen = false
        spinerView.isHidden = true
        isToolbarHidden = false
    }

    func tabItemClick() {
        if spinerView.isOpen {
            spinerView.isOpen = false
        }else {
            setSearchBarHidden(false)
        }
    }
}

