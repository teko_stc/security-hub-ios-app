//
//  XMainController+Navigation.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension XMainController {
    private func showOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in if !result.success { AlertUtil.errorAlert(result.message) } })
            .subscribe()
    }
    private func showOnAvailable(device_id: Int64, controller: UIViewController) {
        guard let nV = navigationController else { return }
        dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in
                if result.success, let controller = controller as? UIAlertController { nV.present(controller, animated: false) }
                else if result.success { nV.pushViewController(controller, animated: false) }
            })
            .do(onSuccess: { (result) in if !result.success { AlertUtil.errorAlert(result.message) } })
            .subscribe()
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Navigation
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func showDevices(_ obj: Any, _ value: Any) {
        guard let nV = navigationController, let site = obj as? Sites else { return }
        let vc = XMainController(site_id: site.id, type: .device, name: site.name)
        nV.pushViewController(vc, animated: true)
    }
    func showSections(_ obj: Any, _ value: Any) {
        guard let nV = navigationController else { return }
        var vc: XMainController?
        if let site_id = self.site_id, let device = obj as? Devices {
            vc = XMainController(site_id: site_id, device_id: device.id, type: .section, name: device.name)
        }else if let device = obj as? Devices {
            vc = XMainController(device_id: device.id, type: .section, name: device.name)
        }else if let site = obj as? Sites {
            vc = XMainController(site_id: site.id, type: .section, name: site.name)
        }
        if let vc = vc { nV.pushViewController(vc, animated: true) }
    }
    func showZones(_ obj: Any, _ value: Any) {
        guard let nV = navigationController else { return }
        var vc: XMainController?
        if let site_id = self.site_id, let section = obj as? Sections {
            vc = XMainController(site_id: site_id, device_id: section.device, section_id: section.section, type: .zone, name: section.name)
        }else if let section = obj as? Sections {
            vc = XMainController(device_id: section.device, section_id: section.section, type: .zone, name: section.name)
        }else if let device = obj as? Devices {
            vc = XMainController(device_id: device.id, type: .zone, name: device.name)
        }else if let site = obj as? Sites {
            vc = XMainController(site_id: site.id, type: .zone, name: site.name)
        }
        if let vc = vc { nV.pushViewController(vc, animated: true) }
    }
    func showReles(_ obj: Any, _ value: Any) {
        guard let nV = navigationController else { return }
        var vc: XMainController?
        if let site_id = self.site_id, let device = obj as? Devices {
            vc = XMainController(site_id: site_id, device_id: device.id, type: .relay, name: device.name)
        }else if let device = obj as? Devices {
            vc = XMainController(device_id: device.id, type: .relay, name: device.name)
        }else if let site = obj as? Sites {
            vc = XMainController(site_id: site.id, type: .relay, name: site.name)
        }
        if let vc = vc { nV.pushViewController(vc, animated: true) }
    }
    func showHozorgan(_ obj: Any, _ value: Any){
        guard let nV = navigationController else { return }
        var device_ids: [Int64] = []
        if let site = obj as? Sites { device_ids = dm.getDeviceIds(site_id: site.id) }
        else if let device = obj as? Devices { device_ids = [device.id] }
        let vc = HozOrganListController(device_ids: device_ids)
        nV.pushViewController(vc, animated: true)
    }
    func showEvent(_ event_id: Int64) {
        guard let event = dm.getEvent(id: event_id) else { return }
        let site = dm.getNameSites(event.device)
        let device = dm.getDeviceName(device: event.device)
        var section = ""
        if let ids = event.jdata.toJson()["sections"] as? [Int64], ids.count > 0 {
            if ids.count == 1, let name = dm.getSection(device: event.device, section: ids.first!)?.name {
                section = "\(name)(\(ids.first!))"
            } else {
                var new_value = "\("SECTIONS".localized()): "
                ids.forEach { (id) in
                    if let name = DataManager.shared.getSection(device: event.device, section: id)?.name { new_value += "\(name)(\(id)), " }
                }
                new_value.removeLast(2)
                section = new_value
            }
        } else {
            if let _sec = DataManager.shared.getSection(device: event.device, section: event.section) {
                if let zone = DataManager.shared.getZone(device: event.device, section: event.section, zone: event.zone) {
                    section = "\(_sec.name)(\(_sec.section)), \(zone.name)(\(zone.zone))"
                } else {
                    section = "\(_sec.name)(\(_sec.section))"
                }
            }
        }
        let time = (event.time == 0 ? "--:--:--" : event.time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short))
        let text = "\(site)\n\(device)\n\(section)\n\n\(event.affect_desc)\n\n\(time)\n"
        AlertUtil.infoAlert(title: "INFORMATION".localized(), text)
    }
    func scripts(_ obj: Any){
        guard let nV = navigationController else { return }
        var vc: XScriptsController?
        if let device = obj as? Devices {
            vc = XScriptsController(device_ids: [device.id])
        }else if let site = obj as? Sites {
            let device_ids = dm.getDeviceIds(site_id: site.id)
            vc = XScriptsController(device_ids: device_ids)
        }
        if let vc = vc { nV.pushViewController(vc, animated: true) }
    }
    func script(_ obj: Any) {
        guard let nV = navigationController else { return }
        guard let zone = obj as? Zones else { return }
        _ = dm.getScript(device_id: zone.device, zone_id: zone.zone)
            .observeOn(MainScheduler.init())
            .subscribe(onNext: { (r) in
                guard let s = r.script else { return AlertUtil.errorAlert("ERROR".localized())}
                let vc = XAddScriptController(device_id: zone.device, script: s, params: r.params, name: HubLibraryScript.Data.LocalazedString.getName(s.name), bind: zone.zone)
                nV.pushViewController(vc, animated: true)
            })
    }
    func script_add(_ obj: Any) {
        guard let nV = navigationController else { return }
        guard let zone = obj as? Zones else { return }
        let vc = XSelectScriptTypeController(device_ids: [zone.device], bind: zone.zone)
        nV.pushViewController(vc, animated: true)
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Add Button Click
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func addSiteClick() {
        guard let nV = navigationController else { return }
        let actions = [
            AlertUtil.XAlertAction(message: "MAIN_NEW_OBJECT".localized(), void: { self.addSite(nV) }),
            AlertUtil.XAlertAction(message: "MAIN_DELEG_OBJECT".localized(), void: delegate)
        ]
        let vc = AlertUtil.myXAlert(actions: actions)
        nV.present(vc, animated: false)
    }
    private func addSite(_ nV: UINavigationController) {
        let vc = SiteAddController(site_id: nil)
        nV.pushViewController(vc, animated: false)
    }
    private func delegate() { AlertUtil.delegateAlert() }
    
    func addDeviceClick() {
        guard let nV = navigationController, let site_id = self.site_id else { return }
        let vc = SiteAddController(site_id: site_id)
        nV.pushViewController(vc, animated: false)
    }
    
    func addSectionClick() {
        guard let nV = navigationController else { return }
        if let device_id = self.device_id {
            addSection(nV, device_id: device_id)
        } else if let site_id = self.site_id {
            let deviceIds = dm.getDeviceIds(site_id: site_id)
            switch deviceIds.count {
            case 0: AlertUtil.warAlert("N_WA_NO_DEVICES_TO_SET_ELEMENTS".localized())
            case 1: addSection(nV, device_id: deviceIds[0])
            default:
                let void: ((_ sec: Sections) -> Void) = { sec in self.addSection(nV, device_id: sec.device) }
                let controller = SectionSelectController(site: site_id, device_ids: deviceIds, title: "DTA_CHOOSE_DEVICE".localized(), isDevice: true, select: void)
                nV.pushViewController(controller, animated: false)
            }
        }
    }
    private func addSection(_ nV: UINavigationController, device_id: Int64) {
        guard dm.isHub(device_id: device_id) else { return AlertUtil.warAlert("add_section_not_hub_error".localized()) }
        let vc = AddSectionController(device: device_id)
        showOnAvailable(device_id: device_id, controller: vc)
    }
    
    func addZoneClick() {
        let device_ids = device_id != nil ? [device_id!] : site_id != nil ? dm.getDeviceIds(site_id: site_id!) : []
        switch device_ids.count {
        case 0:
            AlertUtil.warAlert("add_zone_no_device_error".localized())
        case 1:
            guard dm.isHub(device_id: device_ids[0]) else { return AlertUtil.warAlert("add_section_not_hub_error".localized()) }
            showOnAvailable(device_id: device_ids[0]) { self.addZoneAlert(device_ids: device_ids) }
        default:
            addZoneAlert(device_ids: device_ids)
        }
    }
    private func addZoneAlert(device_ids: [Int64]) {
        guard let nV = navigationController else { return }
        let wireless_void = { nV.pushViewController(XFindWirelessController(device_ids: device_ids, section_id: self.section_id, type: .zone), animated: true) }
        let wireline_void = { nV.pushViewController(XAddWirelineZoneController(device_ids: device_ids, section_id: self.section_id), animated: true) }
        let actions = [
            AlertUtil.XAlertAction(message: "WIZ_WIRELESS_ZONE".localized(), void: wireless_void),
            AlertUtil.XAlertAction(message: "WIZ_WIRED_ZONE".localized(), void: wireline_void)
        ]
        nV.present(AlertUtil.myXAlert(actions: actions), animated: false)
    }

    
    func addRelayClick() {
        let device_ids = device_id != nil ? [device_id!] : site_id != nil ? dm.getDeviceIds(site_id: site_id!) : []
        switch device_ids.count {
        case 0:
            AlertUtil.warAlert("add_zone_no_device_error".localized())
        case 1:
            guard dm.isHub(device_id: device_ids[0]) else { return AlertUtil.warAlert("add_section_not_hub_error".localized()) }
            showOnAvailable(device_id: device_ids[0]) { self.addRelayAlert(device_ids: device_ids) }
        default:
            addRelayAlert(device_ids: device_ids)
        }
    }
    private func addRelayAlert(device_ids: [Int64]) {
        guard let nV = navigationController else { return }
        let wireless_void = { nV.pushViewController(XFindWirelessController(device_ids: device_ids, section_id: self.section_id, type: .relay), animated: true) }
        let wireline_void = { nV.pushViewController(XAddWirelineRelayController(device_ids: device_ids), animated: true) }
        let wireline_exit_void = { nV.pushViewController(XAddWirelineExitController(device_ids: device_ids), animated: true) }
        let actions = [
            AlertUtil.XAlertAction(message: "WIZ_RELAY_WIRELESS_BUTTON".localized(), void: wireless_void),
            AlertUtil.XAlertAction(message: "WIZ_RELAY_WIRED_BUTTON".localized(), void: wireline_void),
            AlertUtil.XAlertAction(message: "FAB_SET_EXIT".localized(), void: wireline_exit_void)
        ]
        nV.present(AlertUtil.myXAlert(actions: actions), animated: false)
    }
}
