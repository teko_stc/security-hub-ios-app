//
//  XMainController+Observers.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension XMainController {
    func eventObserver() -> Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>{
        let mask = DataManager.settingsHelper.storyMask
        return dm.getEvents()
            .filter{ (e) -> Bool in return (mask & (1 << e.event._class)) != 0 }
            .filter{ (e) -> Bool in
                if self.objectType == .device, let site_id = self.site_id {
                    return e.siteIds.contains(site_id)
                }
                return true
            }
            .filter({ (e) -> Bool in
                if self.objectType == .section, let site_id = self.site_id, let device_id = self.device_id {
                    return e.siteIds.contains(site_id) && e.event.device == device_id
                } else if self.objectType == .section, let device_id = self.device_id {
                    return e.event.device == device_id
                } else if self.objectType == .section, let site_id = self.site_id {
                    return e.siteIds.contains(site_id)
                }
                return true
            })
            .filter({ (e) -> Bool in
                if self.objectType == .zone, let site_id = self.site_id, let device_id = self.device_id, let section_id = self.section_id {
                    return e.siteIds.contains(site_id) && e.event.device == device_id && e.event.section == section_id
                } else if self.objectType == .zone, let device_id = self.device_id, let section_id = self.section_id {
                    return e.event.device == device_id && e.event.section == section_id
                } else if self.objectType == .zone, let device_id = self.device_id {
                    return e.event.device == device_id
                } else if self.objectType == .zone, let site_id = self.site_id {
                    return e.siteIds.contains(site_id)
                }
                return true
            })
            .filter({ (e) -> Bool in
                return self.objectType == .relay ? (e.event.detector == 23 || e.event.detector == 13) : true
            })
            .filter({ (e) -> Bool in
                if self.objectType == .relay, let site_id = self.site_id, let device_id = self.device_id {
                    return e.siteIds.contains(site_id) && e.event.device == device_id
                } else if self.objectType == .relay, let device_id = self.device_id {
                    return e.event.device == device_id
                } else if self.objectType == .relay, let site_id = self.site_id {
                    return e.siteIds.contains(site_id)
                }
                return true
            })
            .observeOn(MainScheduler.init())
    }
    
    func siteObserver() -> Observable<XObjectEntity> {
        return dm.getSitesObserver()
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .map { (o) -> XObjectEntity in return self.toXObjectEntity(o) }
            .observeOn(MainScheduler.init())
            .do(onNext: { (o) in self.tableProtocol.addObject(o) })
    }
    
    func deviceObsever() -> Observable<XObjectEntity> {
        return dm.getDevicesObserver(site_id: site_id)
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .filter({ (de) -> Bool in
                if let site_id = self.site_id { return de.siteIds.contains(site_id) }
                return true
            })
            .map { (o) -> XObjectEntity in return self.toXObjectEntity(o) }
            .observeOn(MainScheduler.init())
            .do(onNext: { (o) in self.tableProtocol.addObject(o) })
    }
    
    func sectionObserver() -> Observable<XObjectEntity> {
        return dm.getSectionsObserver()
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .filter({ (ds) -> Bool in
                if let site_id = self.site_id, let device_id = self.device_id { return ds.siteIds.contains(site_id) && ds.deviceId == device_id}
                else if let site_id = self.site_id { return ds.siteIds.contains(site_id) }
                else if let device_id = self.device_id { return ds.deviceId == device_id }
                return true
            })
            .map { (o) -> XObjectEntity in return self.toXObjectEntity(o) }
            .observeOn(MainScheduler.init())
            .do(onNext: { (o) in self.tableProtocol.addObject(o) })
    }
}
