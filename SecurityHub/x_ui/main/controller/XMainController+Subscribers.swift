//
//  XMainController+Subscribers.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension XMainController {
    func eventSubscribe(_ e: (event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)){
        tableProtocol.addEvent(e.event, type: e.type, isFromBD: e.isFromBD)
    }

    func toXObjectEntity(_ ds: DSiteEntity) -> XObjectEntity {
        guard let site = ds.site else { return XMainCellBaseEntityBuilder.create(id: ds.id) }
        let ce = XMainCellSiteEntityBuilder.create(site: site)
            .updateType(ds.type)
            .status(affects: ds.affects, status: ds.armStatus)
            .buttonCommands(arm: arm, disarm: disarm, need: Roles.sendCommands)
            .buttonDevice(showDevices, need: DataManager.settingsHelper.needDevices)
            .buttonRele(showReles, need: !DataManager.settingsHelper.needDevices)
            .buttonListHozorgan(showHozorgan, need: !DataManager.settingsHelper.needDevices)
            .buttonSection(showSections, need: !DataManager.settingsHelper.needDevices && DataManager.settingsHelper.needSections)
            .buttonZone(showZones, need: !DataManager.settingsHelper.needDevices && !DataManager.settingsHelper.needSections)
            .menuAbout(about)
        if Roles.manageSites && site.delegated == 0 {
            _ = ce.menuRename(rename)
                .menuScrips(scripts)
                .menuDelegate(delegate)
                .menuDelete(delete)
        } else if Roles.manageSites && site.delegated == 2 {
            _ = ce.menuRename(rename)
                .menuScrips(scripts)
                .menuDelegate(delegate)
                .menuListDelegate(list_delegate)
                .menuDelete(delete)
        } else if Roles.manageSites && site.delegated == 1 {
            _ = ce.menuScrips(scripts)
                .menuDeleteDelegate(delete_delegate)
        }
        return ce.build(affectClick: showEvent)
    }
    
    func toXObjectEntity(_ dd: DDeviceEntity) -> XObjectEntity {
        guard let device = dd.device else { return XMainCellBaseEntityBuilder.create(id: dd.id) }
        let dTI = HubConst.getDeviceTypeInfo(device.configVersion, cluster: device.cluster_id)
        let ce = XMainCellDeviceEntityBuilder.create(device: device)
            .updateType(dd.type)
            .affects(affects: dd.affects, image: dTI.icon)
            .siteName(objectType == .site ? dd.siteNames : nil)

        switch dTI.type {
        case .SecurityHub:      return securityHubDevice(ce: ce, dd: dd)
        case .SecurityButton:   return securityButtonDevice(ce: ce)
        default:                return otherDevice(ce: ce, dd: dd)
        }
    }
    private func securityHubDevice(ce: XMainCellDeviceEntityBuilder, dd: DDeviceEntity) -> XObjectEntity {
        var ce = ce.status(dd.armStatus)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.buttonRele(showReles).buttonListHozorgan(showHozorgan)
        if DataManager.settingsHelper.needSections { ce = ce.buttonSection(showSections) } else { ce = ce.buttonZone(showZones) }
        ce = ce.menuAbout(about)//.menuScrips(scripts)
        if Roles.sendCommands && Roles.manageSectionZones { ce = ce.menu(ussd: ussd, delete: delete, reboot: reboot, update: update) }
        return ce.build(affectClick: showEvent)
    }
    private func securityButtonDevice(ce: XMainCellDeviceEntityBuilder) -> XObjectEntity {
        return ce.build(affectClick: showEvent)
    }
    private func otherDevice(ce: XMainCellDeviceEntityBuilder, dd: DDeviceEntity) -> XObjectEntity {
        var ce = ce.status(dd.armStatus)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.buttonRele(showReles).buttonListHozorgan(showHozorgan)
        if DataManager.settingsHelper.needSections { ce = ce.buttonSection(showSections) } else { ce = ce.buttonZone(showZones) }
        ce = ce.menuAbout(about)
        if Roles.sendCommands && Roles.manageSectionZones { ce = ce.menu(ussd: ussd, delete: delete, reboot: nil, update: nil) }
        return ce.build(affectClick: showEvent)
    }

    func toXObjectEntity(_ ds: DSectionEntity) -> XObjectEntity {
        let section = ds.section
        let dTI = HubConst.getDeviceTypeInfo(ds.configVersion, cluster: ds.cluster_id)
        var ce = XMainCellSectionEntityBuilder.create(section: section).updateType(ds.type)
        if objectType == .site || objectType == .device { ce = ce.siteName(ds.siteNames).deviceName(ds.deviceName) }

        if section.section == 0 { return controller(ce: ce, ds: ds, dTI: dTI) }
        if section.detector == HubConst.SECTION_TYPE_SECURITY { return securitySection(ce: ce, ds: ds, dTI: dTI) }
        return otherSection(ce: ce, ds: ds, dTI: dTI)
    }
    private func controller(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce
        ce = ce.isDevice(affects: ds.affects, image: dTI.icon)
        if dTI.type != .SecurityButton { ce = ce.zones(name: "STA_BUTTON_COMPONENTS".localized(), showZones: showZones) }
        if (dTI.type == .SecurityHub || dTI.type == .Astra) && Roles.sendCommands { ce = ce.menuUssd(ussd) }
        if dTI.type == .SecurityHub && Roles.manageSectionZones { ce = ce.menuRenameСontroller(rename) }
        return ce.build(affectClick: showEvent)
    }
    private func securitySection(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce, section = ds.section
        ce = ce.isSecurity(affects: ds.affects, status: ds.armStatus, index: section.section)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.zones(name: "DEVICES_BIG".localized(), showZones: showZones)
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if Roles.sendCommands { ce = ce.menuUpdateAndDelete(update: update, delete: delete) }
        }
        return ce.build(affectClick: showEvent)
    }
    private func otherSection(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce, section = ds.section
        let sec_type = HubConst.getSectionsTypes().first(where: { $0.key == section.detector}) ?? HubConst.getSectionsTypes().first!
        ce = ce.isNotSecutiry(affects: ds.affects, name: sec_type.value.name, image: sec_type.value.icon, index: section.section)
            .zones(name: "DEVICES_BIG".localized(), showZones: showZones)
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if Roles.sendCommands { ce = ce.menuUpdateAndDelete(update: update, delete: delete) }
        }
        return ce.build(affectClick: showEvent)
    }
}

