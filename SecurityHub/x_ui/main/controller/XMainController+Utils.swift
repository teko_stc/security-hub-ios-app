//
//  XMainController+Utils.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

extension XMainController {
    func getSpinItems() -> [XSpinerItem] {
        var items: [XSpinerItem] = []
        let needDevices  = DataManager.settingsHelper.needDevices
        let needSections = DataManager.settingsHelper.needSections
        switch objectType {
        case .site:
            items.append(XSpinerItem(image: nil, title: name ?? "SUNTITLE_OBJECTS".localized(), subTitle: "SUNTITLE_OBJECTS".localized()))
            if needDevices {
                items.append(XSpinerItem(image: nil, title: "DEVICES_TITLE".localized(), subTitle: "DEVICES_TITLE".localized()))
            }
            if needSections{
                items.append(XSpinerItem(image: nil, title: "SECTIONS".localized(), subTitle: "SECTIONS".localized()))
            }
            items.append(XSpinerItem(image: nil, title: "DEVICES_BIG".localized(), subTitle: "DEVICES_BIG".localized()))
            items.append(XSpinerItem(image: nil, title: "RELAY".localized(), subTitle: "RELAY".localized()))
        case .device:
            items.append(XSpinerItem(image: nil, title: name ?? "DEVICES_TITLE".localized(), subTitle: "DEVICES_TITLE".localized()))
            if needSections {
                items.append(XSpinerItem(image: nil, title: "SECTIONS".localized(), subTitle: "SECTIONS".localized()))
            }
            items.append(XSpinerItem(image: nil, title: "DEVICES_BIG".localized(), subTitle: "DEVICES_BIG".localized()))
            items.append(XSpinerItem(image: nil, title: "RELAY".localized(), subTitle: "RELAY".localized()))
        case .section:
            items.append(XSpinerItem(image: nil, title: name ?? "SECTIONS".localized(), subTitle: "SECTIONS".localized()))
            items.append(XSpinerItem(image: nil, title: "DEVICES_BIG".localized(), subTitle: "DEVICES_BIG".localized()))
            items.append(XSpinerItem(image: nil, title: "RELAY".localized(), subTitle: "RELAY".localized()))
        case .zone:
            items.append(XSpinerItem(image: nil, title: name ?? "DEVICES_BIG".localized(), subTitle: "DEVICES_BIG".localized()))
        case .relay:
            items.append(XSpinerItem(image: nil, title: name ?? "RELAY".localized(), subTitle: "RELAY".localized()))
        }
        return items
    }
    func needRightButton() -> Bool {
        return (objectType == .site && Roles.manageSites) || (objectType == .device && Roles.manageDevices) || ((objectType == .section || objectType == .zone || objectType == .relay) && Roles.sendCommands && Roles.manageSectionZones)
    }
    func siteInfo(site: Sites, dSI: DSiteInfo) -> String {
        var text =  "\n\("OBJCET_NAME_DOTS".localized()) \(site.name)" +
                    "\n\("DOMAIN_NUMBER".localized()) \(site.domain)"
        if site.delegated == 1 { text += "\n\("DOMAIN_DELEGATED_FROM".localized())" }
        text += "\n\("USERS_COUNT_DOTS".localized()) \(dSI.operatorCount)"
        if dSI.deviceInfos.count == 0 { text += "\n\n\("SITE_ABOUT_NO_DEVICES".localized())"; return text }
        text += "\n\n\("DEVICES_DOTS".localized()) "
        for dDI in dSI.deviceInfos { text += deviceInfo(dDI: dDI) }
        text += "\n"
        return text
    }
    private func deviceInfo(dDI: DDeviceInfo) -> String {
        var text = "\n\(dDI.name)" +
        "\n   \("SECTIONS_COUNT_DOTS".localized()) \(dDI.sectionCount)" +
        "\n   \("DEVICES_COUNT_DOTS".localized()) \(dDI.zoneCount)" +
        "\n   \("CONNECTION_DOTS".localized()) \(dDI.isConnected ? "EXIST".localized() : "NO_EXIST".localized())"
        if let time = dDI.lastEventTime {
            text += "\n   \("LAST_CONNECTION_TIME".localized())" +
                    "\n   \(time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short))"
        }
        return text
    }
    func deviceInfo(device: Devices) -> String {
        var text =  "\n\(device.name)"
        if HubConst.isHub(device.configVersion, cluster: device.cluster_id), let version = HubConst.getHubVersion(device.configVersion) {
            text += "\n\("DTA_DEVICE_VER_DOTS".localized()) \(version)"
        }
        text += "\n"
        return text
    }
}
