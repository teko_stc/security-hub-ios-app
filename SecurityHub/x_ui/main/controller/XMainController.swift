//
//  XMainController.swift
//  SecurityHub
//
//  Created by Timerlan on 25.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RxSwift

enum XMainObjectType { case site, device, section, zone, relay }

class XMainController: XBaseController<XMainView> {
    lazy var mVProtocol: XMainViewProtocol = xView
    lazy var toolbarProtocol: XMainToolbarProtocol = xView
    lazy var tableProtocol: XMainTableProtocol = xView
    lazy var spinItems: [XSpinerItem] = getSpinItems()

    let dm = DataManager.shared!
    let nc = DataManager.shared!.nCenter
    
    private var oDisposable: Disposable?
    private var eDisposable: Disposable?
    private var oListStructObserver: Any?
    private var eHistoryObserver: Any?
    let objectType: XMainObjectType
    let site_id: Int64?
    let device_id: Int64?
    let section_id: Int64?
    let name: String?
    
    init(site_id: Int64? = nil, device_id: Int64? = nil, section_id: Int64? = nil, type: XMainObjectType, name: String?) {
        self.site_id = site_id
        self.device_id = device_id
        self.section_id = section_id
        self.objectType = type
        self.name = name
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
   
    override func loadView() {
        super.loadView()
        if needRightButton() { rightButton() }
        toolbarProtocol.setToolbar(controller: self, items: spinItems, onSpin: onSpin,
                                   onEventSegmentClick: loadEvents, onObjectSegmentClick: loadObject)
        eHistoryObserver = nc.addObserver(forName: .eventHistoryUpdate, object: nil, queue: .main) { n in self.reloadEvents() }
        oListStructObserver = nc.addObserver(forName: .objectListStructUpdate, object: nil, queue: .main) { n in
            self.spinItems = self.getSpinItems()
            self.xView.spinerView.items = self.spinItems
            self.reloadObjects(self.spinItems.first)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mVProtocol.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mVProtocol.viewWillDisappear()
    }
    
    override func viewDidDestroy() {
        oDisposable?.dispose()
        eDisposable?.dispose()
        if let _ = oListStructObserver { nc.removeObserver(oListStructObserver!) }
        if let _ = eHistoryObserver { nc.removeObserver(eHistoryObserver!) }
    }
    
    override func rightClick() {
        switch objectType {
        case .site:     addSiteClick()
        case .device:   addDeviceClick()
        case .section:  addSectionClick()
        case .zone:     addZoneClick()
        case .relay:    addRelayClick()
        }
    }
    
    public func tabItemClick(){
        mVProtocol.tabItemClick()
    }
    
    private func onSpin(_ item: XSpinerItem) {
        rightButton.isHidden = !(item.title == spinItems.first?.title ?? "" )
        rightButton.tag = rightButton.isHidden ? 0 : 1
        reloadObjects(item)
    }
    
    private func loadEvents(){
        rightButton.isHidden = true
        if eDisposable != nil { return }
        reloadEvents()
    }
    
    private func loadObject(){
        rightButton.isHidden = rightButton.tag == 0
    }
    
    private func reloadEvents() {
        eDisposable?.dispose()
        eDisposable = nil
        tableProtocol.initETable()
        eDisposable = eventObserver().subscribe(onNext: { es in self.eventSubscribe(es) })
    }
    
    private func reloadObjects(_ item: XSpinerItem?) {
        guard let item = item else { return }
        oDisposable?.dispose()
        oDisposable = nil
        tableProtocol.initOTable()
        if item.subTitle == "SUNTITLE_OBJECTS".localized() {
            oDisposable = siteObserver().subscribe()
        }else if item.subTitle == "DEVICES_TITLE".localized() {
            oDisposable = deviceObsever().subscribe()
        }else if item.subTitle == "SECTIONS".localized() {
            oDisposable = sectionObserver().subscribe()
        }
    }
}
