//
//  XMainController+Commands.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension XMainController {
    private func doOnConnect(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.code != HubResponseCode.NO_SIGNAL_DEVICE_CODE { void() } })
            .do(onSuccess: { (result) in if result.code == HubResponseCode.NO_SIGNAL_DEVICE_CODE { self.resultAlert(device_id: device_id, result: result) } })
            .subscribe()
    }
    private func doOnDisarmed(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.code != HubResponseCode.DEVICE_ARMED_CODE { void() } })
            .do(onSuccess: { (result) in if result.code == HubResponseCode.DEVICE_ARMED_CODE { self.resultAlert(device_id: device_id, result: result) } })
            .subscribe()
    }
    private func doOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in self.resultAlert(device_id: device_id, result: result) })
            .subscribe()
    }
    private func resultAlert(_ commandName: String? = nil, siteName: String? = nil, device_id: Int64? = nil, sectionName: String? = nil, zoneName: String? = nil, relayName: String? = nil, result: DCommandResult) {
        if result.success { return }
        var message = ""
        DispatchQueue.global(qos: .utility).async {
            if let device_id = device_id , let di = self.dm.getDeviceInfo(device_id: device_id) { message = "\("OBJECT".localized()): \(di.siteNames)\n\("DEVICE".localized()): \(di.device.name)\n" }
            else if let siteName = siteName { message = "\("OBJECT".localized()): \(siteName)\n" }
            if let sectionName = sectionName { message += "\("PARTITION".localized()): \(sectionName)\n" }
            if let zoneName = zoneName { message += "\("zone".localized()): \(zoneName)\n" }
            if let relayName = relayName { message += "\("RELAY".localized()): \(relayName)\n" }
            if let commandName = commandName { message += "\("ERROR_COMMAND".localized()): \(commandName)\n" }
            message += "\n\(result.message)"
            DispatchQueue.main.async {
                AlertUtil.errorAlert(message)
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ALL COMMANDS
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func about(_ obj: Any){
        if let site = obj as? Sites             { about(site: site)}
        else if let device = obj as? Devices    { about(device: device) }
    }
    func ussd(_ obj: Any) {
        if let section = obj as? Sections       { ussd(section: section) }
        else if let device = obj as? Devices    { ussd(device: device) }
    }
    func update(_ obj: Any) {
        if let section = obj as? Sections       { update(section: section) }
        else if let device = obj as? Devices    { update(device: device) }
    }
    func reboot(_ obj: Any) {
        if let device = obj as? Devices         { reboot(device: device) }
    }
    func delay(_ obj: Any) {
        if let zone = obj as? Zones             { delay(zone: zone) }
    }
    func arm(_ obj: Any, _ value: Any){
        if let site = obj as? Sites             { arm(site_id: site.id) }
        else if let device = obj as? Devices    { arm(device_id: device.id) }
        else if let section = obj as? Sections  { arm(device_id: section.device, section_id: section.section, name: section.name) }
    }
    func disarm(_ obj: Any, _ value: Any){
        if let site = obj as? Sites             { disarm(site_id: site.id) }
        else if let device = obj as? Devices    { disarm(device_id: device.id) }
        else if let section = obj as? Sections  { disarm(device_id: section.device, section_id: section.section, name: section.name) }
    }
    func rename(_ obj: Any){
        if let site = obj as? Sites             { rename(site: site) }
        else if let section = obj as? Sections  { rename(section: section) }
        else if let zone = obj as? Zones {
            doOnAvailable(device_id: zone.device) {
                AlertUtil.renameXAlert(text: zone.name) { name in
                    let _ = self.dm.updateZoneName(device: zone.device, section: zone.section, zone: zone.zone, name: name).subscribe()
                }
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //SITE
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private func about(site: Sites) {
        dm.getSiteInfoSingle(site_id: site.id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (dSI) in AlertUtil.infoAlert(title: "TITLE_OBJECT_INFO".localized(), self.siteInfo(site: site, dSI: dSI)) })
            .subscribe()
    }
    private func rename(site: Sites) {
        AlertUtil.renameXAlert(text: site.name) { name in
            let _ = self.dm.rename(site_id: site.id, name: name)
                .observeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("site_menu_values[1]".localized(), siteName: site.name, result: result)})
                .subscribe()
        }
    }
    private func arm(site_id: Int64) {
        for device_id in dm.getDeviceIds(site_id: site_id) {
            arm(device_id: device_id)
        }
    }
    private func disarm(site_id: Int64) {
        for device_id in dm.getDeviceIds(site_id: site_id) {
            disarm(device_id: device_id)
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //DEVICE
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private func about(device: Devices) {
        AlertUtil.infoAlert(title: "site_menu_values[0]".localized(), deviceInfo(device: device))
    }
    private func ussd(device: Devices) {
        doOnConnect(device_id: device.id) {
            AlertUtil.ussdAlert(device.id)
        }
    }
    private func delete(device: Devices) {
        doOnDisarmed(device_id: device.id) {
            AlertUtil.deleteXAlert(text: "\("site_menu_values[5]".localized()) \(device.name)?", canceled: nil) {
                let _ = self.dm.removeDevice(device_id: device.id).subscribe()
            }
        }
    }
    private func reboot(device: Devices) {
        let _ = self.dm.reboot(device: device.id).subscribe()
    }
    private func update(device: Devices) {
        doOnAvailable(device_id: device.id) {
            let _ = self.dm.deviceUpdate(device_id: device.id).subscribe()
        }
    }
    private func arm(device_id: Int64) {
        arm(device_id: device_id, section_id: 0)
    }
    private func disarm(device_id: Int64) {
        disarm(device_id: device_id, section_id: 0)
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //SECTION
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private func ussd(section: Sections) {
        doOnConnect(device_id: section.device) {
            AlertUtil.ussdAlert(section.device)
        }
    }
    private func rename(section: Sections) {
        doOnAvailable(device_id: section.device) {
            AlertUtil.renameXAlert(text: section.name) { name in
                let _ = DataManager.shared.addSection(device: section.device, name: name, index: section.section).subscribe()
            }
        }
    }
    private func update(section: Sections) {
        doOnAvailable(device_id: section.device) {
            let messages: [String]  = HubConst.getSectionsHUBTypes().map { type -> String in return type.name }
            let voids: [(()->Void)] = HubConst.getSectionsHUBTypes().map { type -> (()->Void) in return { self.updateType(section, type) } }
            let alert = AlertUtil.myXAlert("\("STA_MENU_CHANGE_SECTION_TYPE".localized()) - \(section.name)", messages: messages, voids: voids)
            self.navigationController?.present(alert, animated: false)
        }
    }
    private func updateType(_ section: Sections, _ type: DSectionTypeEntity) {
        let _ = dm.addSection(device: section.device, name: section.name, index: section.section, type: type.id).subscribe()
    }
    private func delete(section: Sections) {
        doOnAvailable(device_id: section.device) {
            AlertUtil.deleteXAlert(text: "\("site_menu_values[5]".localized()) \(section.name)?", canceled: nil) {
                let _ = self.dm.deleteSection(device: section.device, index: section.section)
                    .observeOn(MainScheduler.init())
                    .do(onError: { e in if let m = e as? MyError, m == MyError.haveZones{ AlertUtil.errorAlert("error_need_delete_zone".localized()) }})
                    .concatMap({ (result) -> Observable<Int64> in return Observable.timer(.seconds(1), scheduler: ConcurrentMainScheduler.instance) })
                    .concatMap({ (result) -> Observable<Bool> in DataManager.shared.deleteSectionZone(device: section.device, section: section.section)})
                    .subscribe(onNext: { result in
                        //                        DataManager.shared.updateSites()
                        //TODO DataManager.shared.updateAllEventsList(site: self.section.site, section: self.section.section)
                    })
            }
        }
    }
    private func arm(device_id: Int64, section_id: Int64, name: String? = nil) {
        doOnConnect(device_id: device_id) {
            let _ = self.dm.arm(device: device_id, section: section_id)
                .observeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("EA_ARMING".localized(), device_id: device_id, sectionName: name, result: result)})
                .subscribe()
        }
    }
    private func disarm(device_id: Int64, section_id: Int64, name: String? = nil) {
        doOnConnect(device_id: device_id) {
            let _ = self.dm.disarm(device: device_id, section: section_id)
                .observeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("EA_DISARMING".localized(), device_id: device_id, sectionName: name, result: result)})
                .subscribe()
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ZONE
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private func delay(zone: Zones) {
        doOnAvailable(device_id: zone.device) {
            let _ = self.dm.zoneDelay(device_id: zone.device, section_id: zone.section, zone_id: zone.zone, delay: zone.delay).subscribe()
        }
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //RELAY
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func on(_ obj: Any, _ value: Any){
        guard let zone = obj as? Zones else { return }
        doOnConnect(device_id: zone.device) {
            let _ = self.dm.switchRelay(device: zone.device, section: zone.section, zone: zone.zone, state: 1)
                .subscribeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("TURN_ON".localized(), device_id: zone.device, relayName: zone.name, result: result)})
                .subscribe()
        }
    }
    
    func off(_ obj: Any, _ value: Any){
        guard let zone = obj as? Zones else { return }
        doOnConnect(device_id: zone.device) {
            let _ = self.dm.switchRelay(device: zone.device, section: zone.section, zone: zone.zone, state: 0)
                .subscribeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("TURN_OFF".localized(), device_id: zone.device, relayName: zone.name, result: result)})
                .subscribe()
        }
    }
    
    func     _switch(_ obj: Any, _ value: Any){
        guard let zone = obj as? Zones, let value = value as? Int64 else { return }
        doOnConnect(device_id: zone.device) {
            let _ = self.dm.switchRelay(device: zone.device, section: zone.section, zone: zone.zone, state: value)
                .subscribeOn(MainScheduler())
                .do(onSuccess: { result in self.resultAlert("TURN_OFF".localized(), device_id: zone.device, relayName: zone.name, result: result)})
                .subscribe()
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func fire_reset(_ obj: Any, _ value: Any){
        guard let zone = obj as? Zones else { return }
        doOnConnect(device_id: zone.device) {
            let _ = self.dm.reset(device: zone.device, section: zone.section, zone: zone.zone).subscribe()
        }
    }
    
    func delete(_ obj: Any){
        if let site = obj as? Sites {
            AlertUtil.deleteXAlert(text: "\("site_menu_values[5]".localized()) \(site.name)?", canceled: nil) {
                let _ = Observable.from(self.dm.getDeviceIds(site_id: site.id))
                    .concatMap({ id -> Observable<Bool> in DataManager.shared.removeDevice(device_id: id, site: 0) })
                    .toArray()
                    .asObservable()
                    .concatMap({ r -> Observable<Bool> in DataManager.shared.delete(site_id: site.id)})
                    .concatMap({ r -> Observable<Int> in Observable.timer(.seconds(1), scheduler: ThreadUtil.shared.backScheduler)})
                    .subscribe(onNext: { b in DataManager.shared.updateSites() })
            }
        } else if let device = obj as? Devices {
            delete(device: device)
        } else if let section = obj as? Sections {
            delete(section: section)
        } else if let zone = obj as? Zones {
            doOnAvailable(device_id: zone.device) {
                AlertUtil.deleteXAlert(text: "\("zone_menu_values[2]".localized()) \(zone.name)?", canceled: nil) {
                    let _ = DataManager.shared.deregisterZone(device: zone.device, section: zone.section, zone: zone.zone)
                        .concatMap({ (result) -> Observable<Int64> in return Observable.timer(.seconds(1), scheduler: ConcurrentMainScheduler.instance) })
                        .concatMap({ (time) -> Observable<Bool> in return DataManager.shared.deleteSectionZone(device: zone.device, section: zone.section, zone: zone.zone)})
                        .subscribe(onNext: { b in
                            //TODO
                            //DataManager.shared.updateAllEventsList(site: self.zone.site, section: self.zone.section, zone: self.zone.zone)
                        })
                }
            }
        }
    }
    
    func delegate(_ obj: Any){
        guard let nV = navigationController else { return }
        toolbarProtocol.setToolbarHidden(false)
        if let site = obj as? Sites {
            nV.pushViewController(DelegationController(site_id: site.id, name: site.name), animated: false)
        }
    }
    
    func list_delegate(_ obj: Any){
        guard let nV = navigationController, let site = obj as? Sites else { return }
        toolbarProtocol.setToolbarHidden(false)
        nV.pushViewController(DelegationListController(site_id: site.id, name: site.name), animated: false)
    }
    
    func delete_delegate(_ obj: Any){
        if let site = obj as? Sites {
            let _ = dm.delegate_delete(site: site.id, domain_id: dm.getUser().domainId)
                .subscribe(onNext: { b in
                    //TODO dm.updateAllEventsList(site: self.site.id)
                })
        }
    }
}


