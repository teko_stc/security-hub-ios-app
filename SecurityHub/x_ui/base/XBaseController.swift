
//
//  XBaseControl.swift
//  SecurityHub
//
//  Created by Timerlan on 15.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RappleProgressHUD

class XBaseController<T : XBaseView>: UIViewController {
    var xView: T! { return view as? T }
	
		var waitNotReadyZone = false

    private var connectionStateChange: Any?
    private var navigationObserver: Any?
    
    private lazy var backButton: UIBarButtonItem  = {
        let backButton = UIBarButtonItem()
        backButton.tintColor = DEFAULT_SELECTED
        backButton.title = "SCRIPT_SET_BACK".localized()
        return backButton
    }()
    
    override func loadView() {
        super.loadView()
        view = T()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.navigationBar.barTintColor = DEFAULT_COLOR_MAIN
//        navigationController?.navigationBar.backgroundColor = DEFAULT_COLOR_MAIN
        navigationController?.navigationBar.tintColor = DEFAULT_COLOR_MAIN //DEFAULT_SELECTED
        navigationController?.navigationBar.isTranslucent = false
        //        navigationController?.navigationBar.shadowImage = UIImage() // IOS 11
        navigationController?.navigationBar.barStyle = UIBarStyle.default //UIBarStyle.black
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationObserver = NotificationCenter.default.addObserver(forName: HubNotification.navigation, object: nil, queue: OperationQueue.main) { notification in
            guard let nV = self.navigationController else { return }
					if (nV.viewControllers.last is XNotReadyZonesController) == false, let nNotReadyZone = notification.object as? NNotReadyZone, self.waitNotReadyZone {
							nV.viewControllers.last!.present(XNotReadyZonesController(nNotReadyZone), animated: false)
							self.waitNotReadyZone = false
            }
        }
        connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main) { notification in
            let code = notification.object as? Int ?? 0
            if code == -6 {
                NavigationHelper.shared.show(XSplashController())
                return
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = connectionStateChange { NotificationCenter.default.removeObserver(connectionStateChange!) }
        if let _ = navigationObserver { NotificationCenter.default.removeObserver(navigationObserver!) }
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        guard parent == nil else { return }
        viewDidDestroy()
    }
        
    public func viewDidDestroy() { }
    
    let rightButton = ZFRippleButton()
    func rightButton(_ img: UIImage? = XImages.view_plus?.withColor(color: DEFAULT_SELECTED)) {
        rightButton.rippleBackgroundColor = UIColor.white
        rightButton.rippleColor = DEFAULT_SELECTED.withAlphaComponent(0.2)
        rightButton.ripplePercent = 1
        rightButton.shadowRippleEnable = false
        rightButton.setImage( img, for: UIControl.State.normal)
        rightButton.addTarget(self, action: #selector(rightClick), for: UIControl.Event.touchUpInside)
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 5)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton)
    }
    @objc func rightClick(){}
    
    //MARK: //TODO
    func showLoader(){ RappleActivityIndicatorView.startAnimating() }
    func hideLoader(){ RappleActivityIndicatorView.stopAnimation() }
}
