//
//  XBaseViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XBaseViewController <T : UIView > : UIViewController, UIGestureRecognizerDelegate {
    public var tabBarIsHidden: Bool { false }
	
		var waitNotReadyZone = false
	
    public var mainView : T = T()
    
    private var navigationObserver: Any?
    
    override var title: String? {
        didSet {
            navigationView.title = title
        }
    }
    
    public var subTitle: String? {
        didSet {
            navigationView.subTitle = subTitle
        }
    }
            
    private var navigationView = XNavigationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = mainView.backgroundColor
        [mainView, navigationView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false; view.addSubview($0) })
        NSLayoutConstraint.activate([
            navigationView.topAnchor.constraint(equalTo: view.topAnchor),
            navigationView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navigationView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            mainView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        (tabBarController as? XBaseTabBarController)?.setBaseTabBarHidden(tabBarIsHidden)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationObserver = NotificationCenter.default.addObserver(forName: HubNotification.navigation, object: nil, queue: OperationQueue.main) { notification in
            guard let nV = self.navigationController else { return }
					if (nV.viewControllers.last is XNotReadyZonesController) == false, let nNotReadyZone = notification.object as? NNotReadyZone, self.waitNotReadyZone {
							nV.viewControllers.last!.present(XNotReadyZonesController(nNotReadyZone), animated: false)
							self.waitNotReadyZone = false
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        (tabBarController as? XBaseTabBarController)?.setBaseTabBarHidden(tabBarIsHidden)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let _ = navigationObserver { NotificationCenter.default.removeObserver(navigationObserver!) }
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        guard parent == nil else { return }
        viewDidDestroy()
    }
    
		func back() { navigationController?.popViewController(animated: true) }
    
    func setNavigationViewBuilder(_ builder: XNavigationViewBuilder?) {
        if let navigationViewBuilder = builder { navigationView.buildView(navigationViewBuilder) }
        else { navigationView.clearView() }
    }
    
    func showErrorColtroller(message: String, cancel: @escaping (() -> ()) = {}) {
        toastLabel?.removeFromSuperview()
        toastLabel = nil
        let alert = XAlertController(
            style: XAlertView.Style(
                backgroundColor: .white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xf9543a))
            ),
            strings: XAlertView.Strings(title: message, text: nil, positive: "N_BUTTON_OK".localized(), negative: nil),
            positive: cancel
        )
        navigationController?.present(alert, animated: true)
    }
    
    private var toastLabel: UILabel?
    private var toastTimer: Timer?
    func showToast(message : String?, font: UIFont? = UIFont(name: "Open Sans", size: 20)) {
        guard let _ = message else {
            return
        }

        if toastLabel != nil {
            toastTimer?.invalidate()
            toastLabel?.alpha = 0.0
            toastLabel?.removeFromSuperview()
        }
        toastLabel = UILabel()
        toastLabel!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel!.textColor = UIColor.white
        toastLabel!.font = font
        toastLabel!.textAlignment = .center;
        toastLabel!.text = message
        toastLabel!.alpha = 1.0
        toastLabel!.layer.cornerRadius = 10;
        toastLabel!.clipsToBounds  =  true
        toastLabel!.numberOfLines = 0
        self.view.addSubview(toastLabel!)
        toastLabel!.translatesAutoresizingMaskIntoConstraints = false
        toastLabel!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        toastLabel!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        toastLabel!.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        toastLabel!.setContentHuggingPriority(.defaultLow, for: .horizontal)
        toastLabel!.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        toastTimer = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { [weak self] timer in
            self?.toastLabel?.alpha = 0.0
            self?.toastLabel?.removeFromSuperview()
            self?.toastLabel = nil
            self?.toastTimer = nil
        }
    }
    
    func baseAlertStyle() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: .white,
            title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
            text: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042)),
            positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x3abeff)),
            negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042))
        )
    }
    
    public func viewDidDestroy() { }

    public func resultProcessing(_ result: DCommandResult) {
        if result.success { return }
        showToast(message: result.message)
    }

    public func errorProcessing(_ error: Error) {
        guard let xError = error as? XError else {
            return showToast(message: error.localizedDescription)
        }

        return showErrorColtroller(message: xError.desc)
    }
}

extension XBaseViewController {
    func doOnConnect(deviceId: Int64) -> Single<Int64> {
        DataManager.shared.isAvailableDevice(device_id: deviceId)
            .flatMap({ result -> Single<Int64> in
                switch result.code {
                case HubResponseCode.NO_SIGNAL_DEVICE_CODE:
                    return Single.error(XError(desc: result.message))
                default:
                    return Single.just(deviceId)
                }
            })
    }

    func doOnAvailable(deviceId: Int64) -> Single<Int64> {
        DataManager.shared.isAvailableDevice(device_id: deviceId)
            .flatMap({ result -> Single<Int64> in
                switch result.success {
                case false:
                    return Single.error(XError(desc: result.message))
                case true:
                    return Single.just(deviceId)
                }
            })
    }
}
