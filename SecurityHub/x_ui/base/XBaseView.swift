//
//  XBaseView.swift
//  SecurityHub
//
//  Created by Timerlan on 15.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XBaseView: UIView {
    lazy var xView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xView.backgroundColor = DEFAULT_COLOR_WINDOW_BACKGROUND
        addSubview(xView)
        xView.snp.remakeConstraints{  make in  make.edges.equalToSuperview() }
        setContent()
        setConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func setContent(){}
    
    func setConstraints(){}
    
    override func updateConstraints() {
        super.updateConstraints()
        setConstraints()
    }
}
