//
//  XBaseFormController.swift
//  SecurityHub
//
//  Created by Timerlan on 25/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Eureka

class XBaseFormController: FormViewController {
    private lazy var backButton: UIBarButtonItem  = {
        let backButton = UIBarButtonItem()
        backButton.tintColor = DEFAULT_SELECTED
        backButton.title = "SCRIPT_SET_BACK".localized()
        return backButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
	
		override func viewDidAppear(_ animated: Bool) {
			super.viewDidAppear(animated)
			navigationController?.setNavigationBarHidden(false, animated: false)
		}
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        guard parent == nil else { return }
        viewDidDestroy()
    }
           
    public func viewDidDestroy() { }
}
