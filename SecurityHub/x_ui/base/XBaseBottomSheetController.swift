//
//  XBaseBottomSheetController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XBaseBottomSheetController<T : UIView>: UIViewController, UIGestureRecognizerDelegate {
    public let mainView: T = T()
    
    public var backgroundColor: UIColor? {
        didSet {
            containerView.layer.backgroundColor = backgroundColor?.cgColor
        }
    }
         
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 30
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 100
        view.layer.shadowOpacity = 0.3
        return view
    }()
    
    private let visualEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0.1
        return view
    }()
    
    private var containerViewBottomAnchor: NSLayoutConstraint!
    private var point: CGPoint? = nil
    private let addingHeight: CGFloat = 200
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .coverVertical
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        setConstraints()
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture))
        containerView.addGestureRecognizer(gesture)
        gesture.delegate = self
        
        visualEffectView.isUserInteractionEnabled = true
        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
        visualEffectView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(tapGesture)))
        visualEffectView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(tapGesture)))
    }
    
    public func viewDidDissmised() {}
    
    private func initViews() {
        view.addSubview(visualEffectView)
        view.addSubview(containerView)
        containerView.addSubview(mainView)
    }
    
    private func setConstraints() {
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        mainView.translatesAutoresizingMaskIntoConstraints = false
        containerViewBottomAnchor = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: addingHeight)
        NSLayoutConstraint.activate([
            visualEffectView.topAnchor.constraint(equalTo: view.topAnchor, constant: -1000),
            visualEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            visualEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            visualEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
          
            containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            containerView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor),
            containerView.heightAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.heightAnchor, constant: -100 + addingHeight),
            containerViewBottomAnchor,
            
            mainView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 30),
            mainView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            mainView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            mainView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -30 - addingHeight),
        ])
    }
    
    @objc private func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let _point = recognizer.location(in: view)
        if (recognizer.state == .began) { point = _point }
        if (point!.y - _point.y) < 100 {
            containerViewBottomAnchor.constant = addingHeight - (point!.y - _point.y)
            view.layoutIfNeeded()
        }
        if (recognizer.state == .ended) {
            if (point!.y - _point.y) < -100 {
                dismiss(animated: true)
                viewDidDissmised()
            } else {
                containerViewBottomAnchor.constant = addingHeight
                UIView.animate(withDuration: 0.3, animations: { self.view.layoutIfNeeded() })
            }
            point = nil
        }
    }
    
    @objc private func tapGesture(_ sender: UITapGestureRecognizer) {
        if (sender.state == .ended) {
            dismiss(animated: true)
            viewDidDissmised()
        }
    }
}
