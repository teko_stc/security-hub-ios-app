//
//  XBaseAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XBaseAlertController<T : UIView>: UIViewController, UIGestureRecognizerDelegate {
    
    public let mainView: T = T()
    
    public var widthAnchor: CGFloat { 1 }
    
    public var verticalAlignment: VerticalAlignment {
        didSet {
            guard let containerViewCenterYAnchor = containerViewCenterYAnchor else { return }
            var k: CGFloat = 0
            switch verticalAlignment {
            case .center:
                k = 0
                break
            case .top:
                k = -((view.frame.height / 2) - (view.frame.height / 3))
                break
            }
            containerViewCenterYAnchor.constant = k
            view.layoutIfNeeded()
        }
    }
    
    public var backgroundColor: UIColor? {
        didSet {
            containerView.layer.backgroundColor = backgroundColor?.cgColor
        }
    }
    
    public var canDismiss: Bool = true
     
    private var containerViewCenterYAnchor: NSLayoutConstraint?
    
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 40
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 100
        view.layer.shadowOpacity = 0.3
        return view
    }()
    
    private let visualEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0.1
        return view
    }()
    
    init() {
        self.verticalAlignment = .center
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundColor = mainView.backgroundColor
        initViews()
        setConstraints()
        
        visualEffectView.isUserInteractionEnabled = true
        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
        visualEffectView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longPressed)))
        visualEffectView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(longPressed)))
    }
    
    private func initViews() {
        view.addSubview(visualEffectView)
        view.addSubview(containerView)
        containerView.addSubview(mainView)
    }
    
    private func setConstraints() {
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        mainView.translatesAutoresizingMaskIntoConstraints = false
        containerViewCenterYAnchor = containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
        NSLayoutConstraint.activate([
            visualEffectView.topAnchor.constraint(equalTo: view.topAnchor, constant: -1000),
            visualEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            visualEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            visualEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
          
            containerView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            containerView.heightAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.heightAnchor, constant: -30),
            containerView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor),
            containerViewCenterYAnchor!,
            containerView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: widthAnchor, constant: -30),

            mainView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            mainView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            mainView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            mainView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15),
        ])
    }
        
    @objc private func tapGesture(_ sender: UITapGestureRecognizer) {
        if canDismiss {
            dismiss(animated: true)
        }
    }
    
    @objc private func longPressed(_ sender: UIGestureRecognizer) {
        if (sender.state == .ended) {
            if canDismiss {
                dismiss(animated: true)
            }
        }
    }
    
    enum VerticalAlignment {
        case top
        case center
    }
}

