//
//  XScriptsView.swift
//  SecurityHub
//
//  Created by Timerlan on 13/01/2020.
//  Copyright © 2020 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import SwipeCellKit

class XScriptsView: XBaseView, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    public var needDelete: Bool = false
    public var deleteScript: ((_ device_id: Int64, _ bind: Int64, _ name: String) -> Void)?
    public var updScriptVoid: ((_ script: DScriptEntity) -> Void)?
    private var list: [DScriptEntity] = []
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = DEFAULT_BACKGROUND
        view.separatorStyle = .none
        view.dataSource = self
        view.delegate = self
        view.register(XScriptCell.self, forCellReuseIdentifier: XScriptCell.cellId)
        return view
    }()
    
    override func setContent() { xView.addSubview(tableView) }
    override func setConstraints() { tableView.snp.remakeConstraints { $0.edges.equalToSuperview() } }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return list.count }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return XScriptCell.height }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {  return tableView.dequeueReusableCell(withIdentifier: XScriptCell.cellId, for: indexPath) }
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? XScriptCell else { return }
        if needDelete { cell.delegate = self }
        cell.setContent(list[indexPath.row])
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        updScriptVoid?(list[indexPath.row])
    }
    
    //MARK: - свайпы туда-сюда
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let script = list[indexPath.row]
        let manageAction = SwipeAction(style: .default, title: "SCRIPT_INFO_DELETE".localized()) { _, _ in self.deleteScript?(script.deviceId, script.bind, script.getName() ?? "") }
        manageAction.backgroundColor = UIColor.systemRed
        manageAction.textColor = UIColor.white
        manageAction.image = nil
        return [manageAction]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        return options
    }
    
    func updScript(_ data: DScriptEntity) {
        let pos = list.firstIndex{ $0.id == data.id }
        if (data.type == .insert && pos == nil) {
            list.append(data)
            tableView.insertRows(at: [IndexPath(row: list.count - 1, section: 0)], with: .left)
        } else if let pos = pos, (data.type == .insert || data.type == .update) {
            list[pos] = data
            tableView.reloadRows(at: [IndexPath(row: pos, section: 0)], with: .fade)
        } else if let pos = pos, data.type == .delete {
            list.remove(at: pos)
            tableView.deleteRows(at: [IndexPath(row: pos, section: 0)], with: .left)
        }
    }
}
