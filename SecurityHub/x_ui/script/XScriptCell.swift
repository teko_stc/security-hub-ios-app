//
//  XScriptCell.swift
//  SecurityHub
//
//  Created by Timerlan on 13/01/2020.
//  Copyright © 2020 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import SwipeCellKit

class XScriptCell: SwipeTableViewCell {
    static let cellId = "XScriptCell"
    static let height: CGFloat = 100
    
    private lazy var titleView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        return view
    }()
    private lazy var locationView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        return view
    }()
    private lazy var statusView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        return view
    }()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let _layer = CALayer()
        _layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: XScriptCell.height)
        _layer.borderWidth = 0.5
        _layer.borderColor = DEFAULT_COLOR_LINE.cgColor
        layer.addSublayer(_layer)
        
        addSubview(titleView)
        addSubview(locationView)
        addSubview(statusView)
        
        titleView.snp.remakeConstraints {
            $0.top.left.equalTo(XSizes.MARGIN_STANDART)
            $0.right.equalTo(-XSizes.MARGIN_STANDART)
        }
        locationView.snp.remakeConstraints {
            $0.top.equalTo(titleView.snp.bottom)//.offset(XSizes.MARGIN_STANDART)
            $0.left.equalTo(XSizes.MARGIN_STANDART)
            $0.right.equalTo(-XSizes.MARGIN_STANDART)
        }
        statusView.snp.remakeConstraints {
            $0.top.equalTo(locationView.snp.bottom).offset(XSizes.MARGIN_STANDART)
            $0.left.equalTo(XSizes.MARGIN_STANDART)
            $0.right.equalTo(-XSizes.MARGIN_STANDART)
            $0.bottom.equalTo(-XSizes.MARGIN_STANDART)
        }
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    open func setContent(_ data: DScriptEntity){
        titleView.text = data.getName()
        locationView.text = data.zoneName
        statusView.text = data.enabled ? "SCRIPTS_STATEMENT_ACTIVE".localized() : "SCRIPT_STATEMENT_LOAD_IN_DEVICE".localized()
    }
}
