//
//  XScriptsController.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import SwipeCellKit

class XScriptsController: XBaseController<XScriptsView> {
    private let device_ids: [Int64]?
    private let dm = DataManager.shared!
    private let disposables: CompositeDisposable
    init(device_ids: [Int64]? = nil) {
        self.device_ids = device_ids
        self.disposables = CompositeDisposable()
        super.init(nibName: nil, bundle: nil)
        title = "SCRIPTS_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func loadView() {
        super.loadView()
        navigationController?.setNavigationBarHidden(false, animated: true)
        if (Roles.sendCommands && Roles.manageSectionZones) { rightButton(); self.xView.needDelete = true }
        self.xView.deleteScript = self.deleteScript
        self.xView.updScriptVoid = self.updScript
        self.load();
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            disposables.dispose()
        }
    }
    
    private func load() {
        let disp = dm.getScripts(device_ids: device_ids)
            .observeOn(MainScheduler.init())
            .subscribe(onNext: self.xView.updScript, onError: { _ in self.error(DCommandResult.ERROR) })
        _ = disposables.insert(disp)
    }
    
    private func deleteScript(_ device_id: Int64, _ bind: Int64, _ name: String) {
        AlertUtil.deleteXAlert(text: "\("SCRIPT_INFO_DELETE".localized()) \"\(name)\"?") {
            let disp = self.dm.deleteScript(device_id: device_id, bind: bind)
                  .observeOn(MainScheduler.init())
                  .subscribe(onSuccess: self.finish, onError: { _ in self.error(DCommandResult.ERROR) })
            _ = self.disposables.insert(disp)
        }
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
    }
    private func error(_ result: DCommandResult) {
        AlertUtil.errorAlert(result.message)
    }
    
    override func rightClick(){
        let device_ids = self.device_ids ?? dm.getHubDeviceIds()
        switch device_ids.count {
        case 0:
            AlertUtil.warAlert("add_zone_no_device_error".localized())
        case 1:
            guard dm.isHub(device_id: device_ids[0]) else { return AlertUtil.warAlert("add_section_not_hub_error".localized()) }
            showOnAvailable(device_id: device_ids[0]) { self.add(device_ids: device_ids) }
        default:
            add(device_ids: device_ids)
        }
    }
    
    private func showOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .observeOn(MainScheduler())
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in if !result.success { AlertUtil.errorAlert(result.message) } })
            .subscribe()
    }
    
    private func add(device_ids: [Int64]) {
        guard let nV = navigationController else { return }
        let vc = XSelectScriptTypeController(device_ids: device_ids)
        nV.pushViewController(vc, animated: true)
    }
    
    private func updScript(_ script: DScriptEntity) {
        guard let nV = navigationController else { return }
        guard let library = script.library else { return self.error(DCommandResult.ERROR) }
        let vc = XAddScriptController(device_id: script.deviceId, script: library, params: script.params, name: script.getName(), bind: script.bind)
        nV.pushViewController(vc, animated: true)
    }
}
