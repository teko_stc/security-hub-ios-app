//
//  XAlarmController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XAlarmController: XBaseViewController<XAlarmView> {
    private var alarmMonitorRepository: XAlarmMonitorRepository
    private var needAlarmDisposable, alarmedDevicesDisposable: Disposable?
        
    private lazy var viewLayer: XAlarmViewLayer = self.mainView
    
    private var deviceId: Int64
    
    private lazy var navigationViewAlarmedBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.colorFromHex(0xf9543e),
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair-1", color: .white, viewTapped: self.back)
    )
    
    init(alarmMonitorRepository: XAlarmMonitorRepository, deviceId: Int64) {
        self.deviceId = deviceId
        self.alarmMonitorRepository = alarmMonitorRepository
        super.init(nibName: nil, bundle: nil)
        /// Новая тревога
        self.title = "" 
        self.mainView.delegate = self
        self.setNavigationViewBuilder(navigationViewAlarmedBuilder)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //TODO
//        let createdTime = alarmMonitorRepository.getAlarm(deviceId: deviceId)?.create_time ?? 0
        let d3Const = DataManager.shared.d3Const
        _ = alarmMonitorRepository.alarmEvents(alarmId: alarmMonitorRepository.getAlarm(deviceId: deviceId)?.alarm_id ?? 0)
            .subscribe(onSuccess: { [weak self, d3Const] result in
                guard let events = result.value else { return }
                
                for event in events.items {
                    let ev = d3Const.getEventType(_class: event.classField.int, detector: event.detector.int, reason: event.reason.int, statment: event.active.int)
                    let affect_desc = DataManager.shared.getEventDescription(jdata: event.jdata, class_desc: ev?.description ?? "", device_id: event.device, reason: event.reason, commandName: nil)
                    
                    //TODO No Ivideon
                    let model = XEventsViewCellModel(
                        id: event.id,
                        icon: ev?.icons.list ?? "no",
                        description: affect_desc,
                        record: nil,
                        time: event.time,
                        names: XEventsViewCellModel.Names(
                            site: DataManager.shared.getNameSites(event.device),
                            device: DataManager.shared.getDeviceName(device: event.device),
                            section: DataManager.shared.getSection(device: event.device, section: event.section)?.name ?? "",
                            zone: DataManager.shared.getZone(device: event.device, section: event.section, zone: event.zone)?.name ?? ""
                        ),
                        isDevice: event.section == 0 && (event.zone == 0 || event.zone == 100),
												location: LocationManager.getLocation(jdata: event.jdata)
                    )
                    
                    DispatchQueue.main.async {
                        self?.viewLayer.addEvent(model, type: .insert, isFromBD: true)
                    }
                }
            })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _ = DataManager.shared.getDBDevice(device_id: deviceId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] di in self?.viewLayer.initData(deviceName: di.device.name, siteName: di.siteNames) })
            .do(onError: { error in debugPrint(error.localizedDescription) })
            .subscribe()
    }
}

extension XAlarmController: XAlarmViewDelegate {
    func onActionViewTapped() {
        guard let alarm = alarmMonitorRepository.getAlarm(deviceId: deviceId), alarm.state < 2 else { return showErrorColtroller(message: "Данную тревогу может закрыть только Дежурный офицер организации охраны") }
        if (alarm.state == 0) {
            showToast(message: "COMMAND_BEEN_SEND".localized())
            _ = DataManager.shared.hubHelper.setCommandWithResult(.ALARM_OPEN, D: ["alarm_id" : alarm.alarm_id, "comment" : "", "extra" : "" ])
                .asObservable()
                .concatMap({ result -> Single<DCommandResult> in
                    if (!result.success) { return Single.just(result) }
                    else { return DataManager.shared.hubHelper.setCommandWithResult(.ALARM_CLOSE, D: ["alarm_id" : alarm.alarm_id, "success" : 11, "comment" : "" ]) }
                })
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { [weak self] result in
                    if (!result.success) { self?.showErrorColtroller(message: result.message) }
                    else { self?.showErrorColtroller(message: "SUCCESS_SET".localized(), cancel: { self?.navigationController?.popViewController(animated: true) })  }
                }, onError: { [weak self] error in
                    self?.showErrorColtroller(message: error.localizedDescription)
                })
        } else {
            showToast(message: "COMMAND_BEEN_SEND".localized())
            _ = DataManager.shared.hubHelper.setCommandWithResult(.ALARM_CLOSE, D: ["alarm_id" : alarm.alarm_id, "success" : 11, "comment" : "" ])
                .subscribe(onSuccess: { [weak self] result in
                    if (!result.success) { self?.showErrorColtroller(message: result.message) }
                    else { self?.showErrorColtroller(message: "SUCCESS_SET".localized(), cancel: { self?.navigationController?.popViewController(animated: true) })  }
                }, onError: { [weak self] error in
                    self?.showErrorColtroller(message: error.localizedDescription)
                })
        }
    }
    
    func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        let model = event
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: nil, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: model.location))
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model)
		{
			let elc = XEventsLocationController()
			elc.location = event.location
			elc.eventTitle = "\(event.description), \(event.names.site), \(event.names.device)"
			navigationController?.present(elc, animated: true)
		}
}
