//
//  XAlarmViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlarmViewStyle: XAlarmViewStyleProtocol {
    var backgroundColor: UIColor { .white }
    
    var cardColor: UIColor { UIColor.colorFromHex(0xf9543e) }
    
    var titleView: XBaseLableStyle { XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 20), alignment: .center) }
    
    var subTitleView: XBaseLableStyle { XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 20), alignment: .center)  }
    
    var actionView: XZoomButtonStyle { XZoomButtonStyle(backgroundColor: .white, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)) }
    
    var eventsHeader: XBaseLableStyle {
        XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18.5))
    }
    
    var eventsViewCell: XEventsView.Cell.Style {
        XEventsView.Cell.Style(
            background: .white,
            iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            title: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .left
            ),
            text: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .left
            ),
            time: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            )
        )
    }
}
