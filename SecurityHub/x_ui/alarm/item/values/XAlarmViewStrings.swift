//
//  XAlarmViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XAlarmViewStrings: XAlarmViewStringsProtocol {
    
    /// Просмотрено
    var actionText: String { "N_NOTIF_ALARM_VIEWED".localized() }
    
    /// События тревоги
    var eventsHeader: String { "N_ALARMS_EVENTS_LIST".localized() }
}
