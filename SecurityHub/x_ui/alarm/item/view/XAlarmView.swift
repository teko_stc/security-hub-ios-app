//
//  XAlarmView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XAlarmViewDelegate {
    func onActionViewTapped()
    func onEventViewItemTapped(event: XEventsView.Cell.Model)
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model)
}

protocol XAlarmViewLayer {
    func initData(deviceName: String, siteName: String)
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool)
}

protocol XAlarmViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var cardColor: UIColor { get }
    var titleView: XBaseLableStyle { get }
    var subTitleView: XBaseLableStyle { get }
    var actionView: XZoomButtonStyle { get }
    var eventsHeader: XBaseLableStyle { get }
    var eventsViewCell: XEventsView.Cell.Style { get }
}

protocol XAlarmViewStringsProtocol {
    var actionText: String { get }
    var eventsHeader: String { get }
}

class XAlarmView: UIView {
    public var delegate: XAlarmViewDelegate?
    
    private var titleView: UILabel = UILabel(), subTitleView: UILabel = UILabel(), actionView: XZoomButton!, caLayer: CAShapeLayer = CAShapeLayer()
    private var tableView: UITableView = UITableView()
    
    private var style: XAlarmViewStyleProtocol = XAlarmViewStyle()
    private var strings: XAlarmViewStringsProtocol = XAlarmViewStrings()
    
    private var eventsItems: [XEventsViewCell.Model] = []
    
    private var eTUDisp: Disposable?

    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        caLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(caLayer)
        
        titleView.setStyle(style.titleView)
        addSubview(titleView)
        
        subTitleView.setStyle(style.subTitleView)
        addSubview(subTitleView)
                
        actionView = XZoomButton(style: style.actionView)
        actionView.text = strings.actionText
        actionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        addSubview(actionView)
        
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.alwaysBounceVertical = false
        tableView.backgroundColor = style.backgroundColor
        tableView.register(XEventsView.Cell.self, forCellReuseIdentifier: XEventsView.Cell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
    
        [titleView, subTitleView, actionView, tableView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 6),
            titleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            titleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),

            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            subTitleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            subTitleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            actionView.topAnchor.constraint(equalTo: subTitleView.bottomAnchor, constant: 10),
            actionView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            actionView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            actionView.heightAnchor.constraint(equalToConstant: 50),
            
            tableView.topAnchor.constraint(equalTo: actionView.bottomAnchor, constant: 30),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        caLayer.path = UIBezierPath(
            roundedRect: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: actionView.frame.origin.y + actionView.frame.size.height + 30)),
            byRoundingCorners: [.bottomLeft, .bottomRight],
            cornerRadii: CGSize(width: 20, height: 0)
        ).cgPath
    }
    
    @objc private func actionViewTapped() {
        delegate?.onActionViewTapped()
    }
}

extension XAlarmView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { eventsItems.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XEventsViewCell.identifier, for: indexPath) as! XEventsViewCell
        let item = eventsItems[indexPath.row]
        cell.setStyle(style.eventsViewCell)
        cell.setDelegate(onViewTapped: onEventViewItemTapped, onRecordTapped: onEventViewItemRecordTapped, onLocationTapped: onEventViewItemLocationTapped)
        cell.setContent(model: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 55))),
            lable = UILabel(frame: CGRect(origin: CGPoint(x: 20, y: 20), size: CGSize(width: UIScreen.main.bounds.width - 20, height: 25)))
        view.backgroundColor = style.backgroundColor
        lable.setStyle(style.eventsHeader)
        lable.text = strings.eventsHeader
        view.addSubview(lable)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { 55 }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { UIView() }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 20 }
  
    private func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        delegate?.onEventViewItemTapped(event: event)
    }
    
    private func onEventViewItemRecordTapped(event: XEventsView.Cell.Model) {
        //TODO
    }
	
		private func onEventViewItemLocationTapped(event: XEventsView.Cell.Model) {
				delegate?.onEventViewItemLocationTapped(event: event)
		}
}

extension XAlarmView: XAlarmViewLayer {
    func initData(deviceName: String, siteName: String) {
        titleView.text = siteName
        subTitleView.text = deviceName
    }

    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool) {
        if type == .insert {
            if let max = eventsItems.map({ (e) -> Int64 in e.time }).max(), max <= model.time, !isFromBD, eTUDisp == nil {
                eventsItems.insert(model, at: 0)
                tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            }else {
                eventsItems.append(model)
                eTableUpdateEndDetecting()
            }
        }
    }
    
    private func eTableUpdateEndDetecting(){
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.eventsItems = self.eventsItems.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.tableView.reloadData()
                self.eTUDisp = nil
            }, onError: { e in
                self.eTUDisp = nil
            })
    }
}
