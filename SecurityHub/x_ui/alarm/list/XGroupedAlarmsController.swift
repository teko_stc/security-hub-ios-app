//
//  XGropedAlarmsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XGroupedAlarmsController: XBaseViewController<XGroupedAlarmsView> {
    private var alarmMonitorRepository: XAlarmMonitorRepository
    private var needAlarmDisposable, alarmedDevicesDisposable: Disposable?
    private var isLoader: Bool = false
    private var putAsidedModels: [(XEventsViewCellModel, NUpdateType)] = []
        
    init(alarmMonitorRepository: XAlarmMonitorRepository) {
        self.alarmMonitorRepository = alarmMonitorRepository
        super.init(nibName: nil, bundle: nil)
        self.mainView.delegate = self
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private lazy var navigationViewAlarmedBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.colorFromHex(0xf9543e),
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair-1", color: .white, viewTapped: self.back),
        titleView: XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 22), alignment: .left)
    )
    private lazy var navigationViewDefaultBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair-1", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 22), alignment: .left)
    )
    
    private lazy var viewLayer: XGroupedAlarmsViewLayer = self.mainView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isLoader = true
        DataManager.shared.getDBEvents(startTime: 0, endTime: Int64.max, _class: 3, siteId: nil) { [weak self] dEvents in
            guard let self = self else { return }
            let models = dEvents.filter( { $0.active == 1 } )
                .map({ (de: DEventEntity) -> XEventsViewCellModel in
                XEventsViewCellModel(
                    id: de.id,
                    icon: de.icon,
                    description: de.affect_desc,
                    record: de.videoRecord,
                    time: de.time,
                    names: XEventsViewCellModel.Names(site: de.siteName, device: de.deviceName, section: de.sectionName ?? "", zone: de.zoneName ?? ""),
										isDevice: de.section == 0 && (de.zone == 0 || de.zone == 100),location: LocationManager.getLocation(jdata: de.jdata)
                )
            })
            
            self.isLoader = false
            self.viewLayer.setEvents(models)
            self.putAsidedModels.forEach({ (model, type) in
                self.viewLayer.addEvent(model, type: type, isFromBD: false)
            })
        }
        
        NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.current) { [weak self] notification in
            guard let self = self, let e = notification.object as? (event: Events, type: NUpdateType) else { return }
            guard e.event._class == 3, e.event.affect == 1 else { return }

            if e.type == .insert {
                self.updateEvent(eventId: e.event.id, type: .insert)
            } else {
                self.updateEvent(eventId: e.event.id, type: .delete)
            }
        }
    }
    
    
    private func updateEvent(eventId: Int64, type: NUpdateType) {
        DataManager.shared.getDBEvent(id: eventId) { [weak self] de in
            guard let self = self, let de = de else { return }

            let model = XEventsViewCellModel(
                id: de.id,
                icon: de.icon,
                description: de.affect_desc,
                record: de.videoRecord,
                time: de.time,
                names: XEventsViewCellModel.Names(site: de.siteName, device: de.deviceName, section: de.sectionName ?? "", zone: de.zoneName ?? ""),
                isDevice: de.section == 0 && (de.zone == 0 || de.zone == 100),location: LocationManager.getLocation(jdata: de.jdata)
            )
            
            if self.isLoader {
                self.putAsidedModels.append((model, type))
            } else {
                self.viewLayer.addEvent(model, type: type, isFromBD: false)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        needAlarmDisposable = alarmMonitorRepository.needAlarmSubject
            .observeOn(ThreadUtil.shared.mainScheduler)
            .bind(onNext: { [weak self] needAlarm in
                guard let self = self else { return }
                self.setNavigationViewBuilder(needAlarm ? self.navigationViewAlarmedBuilder : self.navigationViewDefaultBuilder)
                self.title = needAlarm ? "N_NEW_ALARMS".localized() : "N_ALARMS_CRIT_EVENTS_LIST".localized()
            })
        alarmedDevicesDisposable = alarmMonitorRepository.alarmedDevicesSubject
            .map({ deviceIds in deviceIds.map({ id in DataManager.shared.dbHelper.getDevice(device_id: id) }).compactMap({ di in di }).map({ di in XGroupedAlarmsViewLayerModel(deviceId: di.device.id, deviceName: di.device.name, siteName: di.siteNames) }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .bind(onNext: { [weak self] devices in self?.viewLayer.updAlarmedDevices(devices) })
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        alarmedDevicesDisposable?.dispose()
//        needAlarmDisposable?.dispose()
//        needAlarmDisposable = nil
//    }
}

extension XGroupedAlarmsController: XGroupedAlarmsViewDelegate {
    func onDevicesViewItemTapped(deviceId: Int64) {
        let controller = XAlarmController(alarmMonitorRepository: alarmMonitorRepository, deviceId: deviceId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        let model = event
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: nil, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: model.location))
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model) {
			let elc = XEventsLocationController()
			elc.location = event.location
			elc.eventTitle = "\(event.description), \(event.names.site), \(event.names.device)"
			navigationController?.present(elc, animated: true)
		}
}
