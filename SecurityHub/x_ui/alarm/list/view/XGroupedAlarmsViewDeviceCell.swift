//
//  XGroupedAlarmsViewDeviceCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XGroupedAlarmsViewDeviceCellStyleProtocol {
    var backgroundColor: UIColor { get }
    var deviceLable: XBaseLableStyle { get }
    var siteLable: XBaseLableStyle { get }
    var descriptionLable: XBaseLableStyle { get }
}

class XGroupedAlarmsViewDeviceCell: UITableViewCell {
    static var identifier = "XGroupedAlarmsViewDeviceCell.identifier"

    private var baseView: UIView = UIView()
    private var titleView: UILabel = UILabel(), subTitleView:  UILabel = UILabel(), descriptionView: UILabel = UILabel()
    
    private var deviceId: Int64?
    private var onViewTapped: ((Int64) -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()

        baseView.transform = .identity
        baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabGesture)))
        baseView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longGesture)))
        contentView.addSubview(baseView)
        
        baseView.addSubview(titleView)
        baseView.addSubview(subTitleView)
        baseView.addSubview(descriptionView)

        [baseView, titleView, subTitleView, descriptionView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            baseView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            baseView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            baseView.topAnchor.constraint(equalTo: contentView.topAnchor),
            baseView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            titleView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            titleView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 0),
            subTitleView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            subTitleView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),

            descriptionView.topAnchor.constraint(equalTo: subTitleView.bottomAnchor, constant: 0),
            descriptionView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            descriptionView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),
            descriptionView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -10)
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(deviceId: Int64, deviceName: String, siteName: String, description: String) {
        self.deviceId = deviceId
        titleView.text = deviceName
        subTitleView.text = siteName
        descriptionView.text = description
    }
    
    public func setDelegate(onViewTapped: @escaping (Int64) -> ()) {
        self.onViewTapped = onViewTapped
    }
    
    public func setStyle(_ style: XGroupedAlarmsViewDeviceCellStyleProtocol) {
        backgroundColor = style.backgroundColor
        selectedBackgroundView?.backgroundColor = style.backgroundColor
                
        titleView.setStyle(style.deviceLable)
        subTitleView.setStyle(style.siteLable)
        descriptionView.setStyle(style.descriptionLable)
    }

    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let deviceId = self.deviceId { self.onViewTapped?(deviceId) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let deviceId = self.deviceId { self.onViewTapped?(deviceId) }
            })
        }
    }
}

