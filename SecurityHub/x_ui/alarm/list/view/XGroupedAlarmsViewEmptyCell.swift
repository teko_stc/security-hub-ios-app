//
//  XGroupedAlarmsViewEmptyCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 22.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XGroupedAlarmsViewEmptyCell: UITableViewCell {
    static let identifier: String = "XGroupedAlarmsViewEmptyCell.identifier"
    
    private let titleView = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectedBackgroundView = UIView()
        addSubview(titleView)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -6),
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(title: String) {
        titleView.text = title
    }
    
    public func setStyle(style: XBaseLableStyle) {
        titleView.setStyle(style)
        titleView.numberOfLines = 0
    }
}
