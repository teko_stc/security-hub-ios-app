//
//  XGroupedAlarmsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XGroupedAlarmsViewDelegate {
    func onDevicesViewItemTapped(deviceId: Int64)
    func onEventViewItemTapped(event: XEventsView.Cell.Model)
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model)
}

protocol XGroupedAlarmsViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var devicesViewCell: XGroupedAlarmsViewDeviceCellStyleProtocol { get }
    var otherEventHeader: XBaseLableStyle { get }
    var eventsViewCell: XEventsView.Cell.Style { get }
    var emptyCell: XBaseLableStyle { get }
}

protocol XGroupedAlarmsViewStringsProtocol {
    var devicesSelectDescription: String { get }
    var otherEventHeader: String { get }
    var otherEventEmptyText: String { get }
}

protocol XGroupedAlarmsViewLayer {
    func updAlarmedDevices(_ items: [XGroupedAlarmsViewLayerModel])
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool)
    func setEvents(_ models: [XEventsView.Cell.Model])
}

struct XGroupedAlarmsViewLayerModel {
    let deviceId: Int64
    let deviceName, siteName: String
}

class XGroupedAlarmsView: UIView {
    public var delegate: XGroupedAlarmsViewDelegate?
    
    private var tableView: UITableView = UITableView()
    private var otherEventsEmptyView: UILabel = UILabel()
    
    private var style: XGroupedAlarmsViewStyleProtocol = XGroupedAlarmsViewStyle()
    private var strings: XGroupedAlarmsViewStringsProtocol = XGroupedAlarmsViewStrings()
    
    private var devicesItems: [XGroupedAlarmsViewLayerModel] = []
    private var eventsItems: [XEventsViewCell.Model] = []
    
    private var eTUDisp: Disposable?
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
                
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.alwaysBounceVertical = false
        tableView.backgroundColor = style.backgroundColor
        tableView.register(XGroupedAlarmsViewDeviceCell.self, forCellReuseIdentifier: XGroupedAlarmsViewDeviceCell.identifier)
        tableView.register(XEventsView.Cell.self, forCellReuseIdentifier: XEventsView.Cell.identifier)
        tableView.register(XGroupedAlarmsViewEmptyCell.self, forCellReuseIdentifier: XGroupedAlarmsViewEmptyCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension XGroupedAlarmsView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int { 2 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { section == 0 ? devicesItems.count : (eventsItems.count == 0 ? 1 : eventsItems.count) }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: XGroupedAlarmsViewDeviceCell.identifier, for: indexPath) as! XGroupedAlarmsViewDeviceCell
            let item = devicesItems[indexPath.row]
            cell.setStyle(style.devicesViewCell)
            cell.setDelegate(onViewTapped: onDevicesViewItemTapped)
            cell.setContent(deviceId: item.deviceId, deviceName: item.deviceName, siteName: item.siteName, description: strings.devicesSelectDescription)
            return cell
        } else if eventsItems.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: XGroupedAlarmsViewEmptyCell.identifier, for: indexPath) as! XGroupedAlarmsViewEmptyCell
            cell.setContent(title: strings.otherEventEmptyText)
            cell.setStyle(style: style.emptyCell)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: XEventsViewCell.identifier, for: indexPath) as! XEventsViewCell
            let item = eventsItems[indexPath.row]
            cell.setStyle(style.eventsViewCell)
            cell.setDelegate(onViewTapped: onEventViewItemTapped, onRecordTapped: onEventViewItemRecordTapped, onLocationTapped: onEventViewItemLocationTapped)
            cell.setContent(model: item)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        indexPath.section == 1 && eventsItems.count == 0 ? 300 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 1 && devicesItems.count > 0) {
            let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 55))),
                lable = UILabel(frame: CGRect(origin: CGPoint(x: 20, y: 20), size: CGSize(width: UIScreen.main.bounds.width - 20, height: 25)))
            lable.setStyle(style.otherEventHeader)
            lable.text = strings.otherEventHeader
            view.addSubview(lable)
            return view
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { section == 1 && devicesItems.count > 0 ? 55 : 0 }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == 0 && devicesItems.count > 0) {
            let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 20))), caLayer = CAShapeLayer()
            view.backgroundColor = style.backgroundColor
            caLayer.fillColor = style.devicesViewCell.backgroundColor.cgColor
            caLayer.zPosition = -3
            caLayer.path = UIBezierPath(
                roundedRect: CGRect(origin: CGPoint(x: 0, y: -10), size: CGSize(width: UIScreen.main.bounds.width, height: 30)),
                byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 30, height: 0)).cgPath
            view.layer.addSublayer(caLayer)
            return view
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { section == 0 && devicesItems.count > 0 ? 20 : 0 }
  
    
    private func onDevicesViewItemTapped(deviceId: Int64) {
        delegate?.onDevicesViewItemTapped(deviceId: deviceId)
    }
    
    private func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        delegate?.onEventViewItemTapped(event: event)
    }
    
    private func onEventViewItemRecordTapped(event: XEventsView.Cell.Model) {
        //TODO
    }
	
		private func onEventViewItemLocationTapped(event: XEventsView.Cell.Model) {
				delegate?.onEventViewItemLocationTapped(event: event)
		}
}

extension XGroupedAlarmsView: XGroupedAlarmsViewLayer {
    func updAlarmedDevices(_ items: [XGroupedAlarmsViewLayerModel]) {
        self.devicesItems = items
        tableView.reloadData()
    }
    
    func setEvents(_ models: [XEventsView.Cell.Model]) {
        otherEventsEmptyView.isHidden = models.count > 0
        eventsItems = models
        tableView.reloadData()
    }
    
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool) {
        switch type {
        case .insert:
            if let max = eventsItems.map({ (e) -> Int64 in e.time }).max(), max <= model.time, !isFromBD, eTUDisp == nil {
                eventsItems.insert(model, at: 0)
                tableView.insertRows(at: [IndexPath(row: 0, section: 1)], with: .none)
            }else {
                eventsItems.append(model)
                eTableUpdateEndDetecting()
            }
        case .update:
            guard let pos = eventsItems.firstIndex(where: {$0.id == model.id}) else{ break }
            eventsItems[pos] = model
            eTableUpdateEndDetecting()
        case .delete:
            guard let pos = eventsItems.firstIndex(where: {$0.id == model.id}) else{ break }
            eventsItems.remove(at: pos)
            tableView.deleteRows(at: [IndexPath(row: pos, section: 1)], with: .none)
        }
    }
    
    private func eTableUpdateEndDetecting(){
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.eventsItems = self.eventsItems.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.otherEventsEmptyView.isHidden = self.eventsItems.count > 0
                self.tableView.reloadData()
                self.eTUDisp = nil
            }, onError: { e in
                self.otherEventsEmptyView.isHidden = self.eventsItems.count > 0
                self.eTUDisp = nil
            })
    }
}
