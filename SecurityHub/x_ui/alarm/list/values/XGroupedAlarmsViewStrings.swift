//
//  XGroupedAlarmsViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XGroupedAlarmsViewStrings: XGroupedAlarmsViewStringsProtocol {
    
    /// Нажмите, чтобы просмотреть описание
    var devicesSelectDescription: String { "N_CLICK_TO_VIEW".localized() }
    
    /// Другие критические события
    var otherEventHeader: String { "N_ALARMS_CRIT_EVENTS_LIST".localized() }
    
    /// Не обнаружено критических событий
    var otherEventEmptyText: String { "N_ALARM_NO_CRIT_EVENTS".localized() }
}
