//
//  XAlarmAffectsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


class XAlarmAffectsController: XBaseViewController<XAlarmAffectsView> {
    private lazy var viewLayer: XAlarmAffectsViewLayer = self.mainView
    
    private lazy var navigationViewAlarmedBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.mainView.delegate = self
        self.setNavigationViewBuilder(navigationViewAlarmedBuilder)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //TODO
        _ = DataManager.shared.getEventsXAlarmAffectsController()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .map({ (event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool) in
                //TODO No Ivideon
							return (model: XEventsViewCellModel(id: event.id, icon: event.icon, description: event.affect_desc, record: nil, time: event.time, names: XEventsViewCellModel.Names(site: DataManager.shared.getNameSites(event.device), device: DataManager.shared.getDeviceName(device: event.device), section: DataManager.shared.getSection(device: event.device, section: event.section)?.name ?? "", zone: DataManager.shared.getZone(device: event.device, section: event.section, zone: event.zone)?.name ?? ""), isDevice: event.section == 0 && (event.zone == 0 || event.zone == 100),location: LocationManager.getLocation(jdata: event.jdata)), type: type, isFromBD: isFromBD, affect: event.affect)
            })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] (result) in
                if (result.affect == 1 && result.type == .insert) {
                    self?.viewLayer.addEvent(result.model, type: .insert, isFromBD: result.isFromBD)
                } else if ( result.type == .update) {
                    self?.viewLayer.addEvent(result.model, type: .delete, isFromBD: result.isFromBD)
                }
            })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            self?.mainView.loaderView.isHidden = true
        }
    }
}

extension XAlarmAffectsController: XAlarmAffectsViewDelegate {
    func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        let model = event
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: nil, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: model.location))
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model) {
			let elc = XEventsLocationController()
			elc.location = event.location
			elc.eventTitle = "\(event.description), \(event.names.site), \(event.names.device)"
			navigationController?.present(elc, animated: true)
		}
}
