//
//  XAlarmAffectsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XAlarmAffectsViewDelegate {
    func onEventViewItemTapped(event: XEventsView.Cell.Model)
		func onEventViewItemLocationTapped(event: XEventsView.Cell.Model)
}

protocol XAlarmAffectsViewLayer {
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool)
}

protocol XAlarmAffectsViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var eventsViewCell: XEventsView.Cell.Style { get }
}

class XAlarmAffectsView: UIView {
    public var delegate: XAlarmAffectsViewDelegate?
    
    private var tableView: UITableView = UITableView()
    
    public var loaderView: XLoaderView = XLoaderView()
    
    private var style: XAlarmAffectsViewStyleProtocol = XAlarmAffectsViewStyle()
    
    private var eventsItems: [XEventsViewCell.Model] = []
    
    private var eTUDisp: Disposable?
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.alwaysBounceVertical = false
        tableView.backgroundColor = style.backgroundColor
        tableView.register(XEventsView.Cell.self, forCellReuseIdentifier: XEventsView.Cell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        addSubview(tableView)
        
        loaderView.color = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
        loaderView.isHidden = false
        addSubview(loaderView)
    
        [tableView, loaderView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            loaderView.centerYAnchor.constraint(equalTo: centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension XAlarmAffectsView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { eventsItems.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XEventsViewCell.identifier, for: indexPath) as! XEventsViewCell
        let item = eventsItems[indexPath.row]
        cell.setStyle(style.eventsViewCell)
        cell.setDelegate(onViewTapped: onEventViewItemTapped, onRecordTapped: onEventViewItemRecordTapped, onLocationTapped: onEventViewItemLocationTapped)
        cell.setContent(model: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { UIView() }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 20 }
  
    private func onEventViewItemTapped(event: XEventsView.Cell.Model) {
        delegate?.onEventViewItemTapped(event: event)
    }
    
    private func onEventViewItemRecordTapped(event: XEventsView.Cell.Model) {
        //TODO
    }
	
		private func onEventViewItemLocationTapped(event: XEventsView.Cell.Model) {
				delegate?.onEventViewItemLocationTapped(event: event)
		}
}

extension XAlarmAffectsView: XAlarmAffectsViewLayer {
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool) {
        switch type {
        case .insert:
            if let max = eventsItems.map({ (e) -> Int64 in e.time }).max(), max <= model.time, !isFromBD, eTUDisp == nil {
                eventsItems.insert(model, at: 0)
                tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            }else {
                eventsItems.append(model)
                eTableUpdateEndDetecting()
            }
        case .update:
            guard let pos = eventsItems.firstIndex(where: {$0.id == model.id}) else{ break }
            eventsItems[pos] = model
            eTableUpdateEndDetecting()
        case .delete:
            guard let pos = eventsItems.firstIndex(where: {$0.id == model.id}) else{ break }
            eventsItems.remove(at: pos)
            tableView.deleteRows(at: [IndexPath(row: pos, section: 0)], with: .none)
        }
    }
    
    private func eTableUpdateEndDetecting(){
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.eventsItems = self.eventsItems.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.tableView.reloadData()
                self.loaderView.isHidden = true
                self.eTUDisp = nil
            }, onError: { e in
                self.eTUDisp = nil
            })
    }
}
