//
//  XEventsFilterView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XEventsFilterViewDelegate {
    func siteActionViewTapped()
    func eventClassActionViewTapped()
    func startDateActionViewTapped()
    func endDateActionViewTapped()
}

protocol XEventsFilterViewStrings {
    var title: String { get }
    var siteTitle: String { get }
    var siteActionDefaultTitle: String { get }
    var eventClassTitle: String { get }
    var eventClassActionDefaultTitle: String { get }
    var yesterday: String { get }
    var today: String { get }
}

struct XEventsFilterViewStyle {
    let backgroundColor, backgroundColorAlternative, tintColor: UIColor
    let font, largeFont: UIFont?
}
extension XEventsFilterView {
    typealias Style = XEventsFilterViewStyle
    typealias Strings = XEventsFilterViewStrings
    typealias Delegate = XEventsFilterViewDelegate
}

class XEventsFilterView: UIView {
    override class var layerClass: AnyClass { return CAShapeLayer.self }
    
    override var layer: CAShapeLayer { get { return super.layer as! CAShapeLayer } }
    
    public var delegate: Delegate?
    
    private var backgroundView: UIView!
    private var iconView: UIImageView!
    private var titleView: UILabel!
    private var siteActionView: XZoomButton!, eventClassActionView: XZoomButton!, startDateActionView: XZoomButton!, endDateActionView: XZoomButton!
    private var endDateActionViewBottomAnchor, iconViewBottomAnchor: NSLayoutConstraint!
    private let dateFormatter = DateFormatter()
    private var style: Style, strings: Strings
    
    init(style: Style, strings: Strings) {
        self.style = style
        self.strings = strings
        super.init(frame: .zero)
        
        backgroundColor = .clear
        dateFormatter.dateFormat = "EEEE, dd MMM"
        
        layer.fillColor = style.backgroundColor.cgColor

        backgroundView = UIView()
        backgroundView.backgroundColor = style.backgroundColor
        addSubview(backgroundView)
        
        iconView = UIImageView(image: UIImage(named: "ic_stair-1")?.withRenderingMode(.alwaysTemplate))
        iconView.tintColor = style.tintColor
        addSubview(iconView)
        
        titleView = UILabel.create(XBaseLableStyle(color: style.tintColor, font: style.font, alignment: .right))
        titleView.text = strings.title
        addSubview(titleView)
        
        siteActionView = XZoomButton(style: XZoomButton.Style(backgroundColor: .clear, font: style.largeFont, color: style.tintColor, alignment: .center))
        siteActionView.layer.borderColor = UIColor.white.cgColor
        siteActionView.layer.borderWidth = 2.0
        siteActionView.layer.cornerRadius = 10.0
        siteActionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        siteActionView.text = strings.siteActionDefaultTitle
        addSubview(siteActionView)
        
        eventClassActionView = XZoomButton(style: XZoomButton.Style(backgroundColor: .clear, font: style.largeFont, color: style.tintColor, alignment: .center))
        eventClassActionView.layer.borderColor = UIColor.white.cgColor
        eventClassActionView.layer.borderWidth = 2.0
        eventClassActionView.layer.cornerRadius = 10.0
        eventClassActionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        eventClassActionView.text = strings.eventClassActionDefaultTitle
        addSubview(eventClassActionView)

        startDateActionView = XZoomButton(style: XZoomButton.Style(backgroundColor: .clear, font: style.largeFont, color: style.tintColor, alignment: .center))
        startDateActionView.layer.borderColor = UIColor.white.cgColor
        startDateActionView.layer.borderWidth = 2.0
        startDateActionView.layer.cornerRadius = 10.0
        startDateActionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        startDateActionView.text = strings.yesterday
        addSubview(startDateActionView)
        
        endDateActionView = XZoomButton(style: XZoomButton.Style(backgroundColor: .clear, font: style.largeFont, color: style.tintColor, alignment: .center))
        endDateActionView.layer.borderColor = UIColor.white.cgColor
        endDateActionView.layer.borderWidth = 2.0
        endDateActionView.layer.cornerRadius = 10.0
        endDateActionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        endDateActionView.text = strings.today
        addSubview(endDateActionView)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapped)))
        addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(onViewTapped)))
        
        [siteActionView, eventClassActionView, startDateActionView, endDateActionView].forEach({ $0?.isHidden = true })
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 30.0, height: 0.0)).cgPath
    }
    
    public func siteActionView(text: String?) { siteActionView.text = text ?? strings.siteActionDefaultTitle }
    
    public func eventClassActionView(text: String?) { eventClassActionView.text = text ?? strings.siteActionDefaultTitle }
    
    public func startDateActionView(text: String?) { startDateActionView.text = text ?? strings.siteActionDefaultTitle }
    
    public func endDateActionView(text: String?) { endDateActionView.text = text ?? strings.siteActionDefaultTitle }
    
    public func updateBackgroundColor(isAlternative: Bool) {
        layer.fillColor = (isAlternative ? style.backgroundColorAlternative : style.backgroundColor).cgColor
        backgroundView.backgroundColor = isAlternative ? style.backgroundColorAlternative : style.backgroundColor
    }
        
    private func setConstraints() {
        [backgroundView, iconView, titleView, siteActionView, eventClassActionView, startDateActionView, endDateActionView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        iconViewBottomAnchor = iconView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -40)
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: topAnchor, constant: -100),
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: iconView.topAnchor),
            
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: 40),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 23.5),
            iconView.heightAnchor.constraint(equalToConstant: 14),
            iconView.widthAnchor.constraint(equalToConstant: 20),
            iconViewBottomAnchor,

            titleView.topAnchor.constraint(equalTo: iconView.topAnchor),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),

            siteActionView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 30),
            siteActionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            siteActionView.widthAnchor.constraint(equalToConstant: 250),
            siteActionView.heightAnchor.constraint(equalToConstant: 45),
            siteActionView.leadingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -20),
            siteActionView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -20),
            
            eventClassActionView.topAnchor.constraint(equalTo: siteActionView.bottomAnchor, constant: 10),
            eventClassActionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            eventClassActionView.widthAnchor.constraint(equalToConstant: 250),
            eventClassActionView.heightAnchor.constraint(equalToConstant: 45),
            eventClassActionView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 20),
            eventClassActionView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -20),
            
            startDateActionView.topAnchor.constraint(equalTo: eventClassActionView.bottomAnchor, constant: 20),
            startDateActionView.leadingAnchor.constraint(equalTo: siteActionView.leadingAnchor),
            startDateActionView.heightAnchor.constraint(equalToConstant: 45),
            startDateActionView.widthAnchor.constraint(equalTo: endDateActionView.widthAnchor),

            endDateActionView.topAnchor.constraint(equalTo: eventClassActionView.bottomAnchor, constant: 20),
            endDateActionView.heightAnchor.constraint(equalToConstant: 45),
            endDateActionView.leadingAnchor.constraint(equalTo: startDateActionView.trailingAnchor, constant: 10),
            endDateActionView.trailingAnchor.constraint(equalTo: siteActionView.trailingAnchor),
        ])
    }
    
    @objc private func onViewTapped(sender: UIGestureRecognizer) { if sender.state == .ended { changeStateView(isHidden: !siteActionView.isHidden) } }
    
    public func changeStateView(isHidden: Bool) {
        guard siteActionView.isHidden != isHidden else {
            return
        }

        [siteActionView, eventClassActionView, startDateActionView, endDateActionView].forEach({ $0?.isHidden = isHidden })
        iconView.image = UIImage(named: isHidden ? "ic_stair-1" : "ic_stair_up")?.withRenderingMode(.alwaysTemplate)
        iconViewBottomAnchor.constant = !isHidden ? -225 : -40
        layoutIfNeeded()
    }
    
    @objc private func actionViewTapped(_ view: UIView) {
        switch view {
        case siteActionView:
            delegate?.siteActionViewTapped()
            break;
        case eventClassActionView:
            delegate?.eventClassActionViewTapped()
            break;
        case startDateActionView:
            delegate?.startDateActionViewTapped()
            break;
        case endDateActionView:
            delegate?.endDateActionViewTapped()
            break;
        default:
            break;
        }
    }
}
