//
//  XEventsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XEventsViewDelegate: XEventsFilterViewDelegate {
    func eventViewTapped(model: XEventsView.Cell.Model)
    func eventRecordViewTapped(model: XEventsView.Cell.Model)
		func eventLocationViewTapped(model: XEventsView.Cell.Model)
}

protocol XEventsViewStrings: XEventsFilterViewStrings {
    var emptyListTitle: String { get }
}

protocol XEventsViewStyle {
    var backgroundColor: UIColor { get }
    var loaderColor: UIColor { get }
    var filter: XEventsFilterView.Style { get }
    var emptyTitle: XBaseLableStyle { get }
    var cell: XEventsView.Cell.Style { get }
}
extension XEventsView {
    typealias Style = XEventsViewStyle
    typealias Strings = XEventsViewStrings
    typealias Delegate = XEventsViewDelegate
    typealias Cell = XEventsViewCell
}

protocol XEventsViewViewLayer {
    func setSiteSelectedValue(text: String)
    func setEventClassSelectedValue(text: String)
    func setStartTimeSelectedValue(text: String)
    func setEndTimeSelectedValue(text: String)
    func clearEvents()
    func addEvent(_ model: XEventsView.Cell.Model, type: NUpdateType, isFromBD: Bool)
    func setEvents(_ models: [XEventsView.Cell.Model])
    func showLoader()
    func hideLoader()
    func updateBackgroundFilter(isAlternative: Bool)
    func hideFilter()
}

class XEventsView: UIView {
    public var delegate: Delegate? { didSet { filterView.delegate = delegate } }
    
    private var filterView: XEventsFilterView!
    private var emptyTitleView: UILabel!
    private var tableView: UITableView!
    private var loaderView: XLoaderView!
    private var style: Style = XEventsViewControllerStyles()
    private var strings: Strings = XEventsViewControllerStrings()
    private var events: [Cell.Model] = []
    private var eTUDisp: Disposable?
		private var emptyTitleHidden = false
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        filterView = XEventsFilterView(style: style.filter, strings: strings)
        addSubview(filterView)
    
        tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
        
        emptyTitleView = UILabel.create(style.emptyTitle)
        emptyTitleView.text = strings.emptyListTitle
        emptyTitleView.numberOfLines = 0
        addSubview(emptyTitleView)
        
        loaderView = XLoaderView()
        loaderView.color = style.loaderColor
        addSubview(loaderView)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPanTableView))
        panGesture.delegate = self
        tableView.isUserInteractionEnabled = true
        tableView.addGestureRecognizer(panGesture)

        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setConstraints() {
        [filterView, emptyTitleView, tableView, loaderView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            filterView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            filterView.leadingAnchor.constraint(equalTo: leadingAnchor),
            filterView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            tableView.topAnchor.constraint(equalTo: filterView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            emptyTitleView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            emptyTitleView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor, constant: 20),
            
            loaderView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    
}

extension XEventsView: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
				self.updateEmptyEvents()
				return events.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setContent(model: events[indexPath.row])
        cell.setStyle(style.cell)
        cell.setDelegate(onViewTapped: { m in self.delegate?.eventViewTapped(model: m) }, onRecordTapped: { m in self.delegate?.eventRecordViewTapped(model: m) },
					onLocationTapped: { m in self.delegate?.eventLocationViewTapped(model: m) })
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { UIView() }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 70 }

    @objc func didPanTableView(gestureRecognizer: UIPanGestureRecognizer) {
        filterView.changeStateView(isHidden: true)
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension XEventsView: XEventsViewViewLayer {
    func updateBackgroundFilter(isAlternative: Bool) {
        filterView.updateBackgroundColor(isAlternative: isAlternative)
    }
    
    func hideFilter() {
        filterView.changeStateView(isHidden: true)
    }
    
    func setStartTimeSelectedValue(text: String) { filterView.startDateActionView(text: text) }
    
    func setEndTimeSelectedValue(text: String) { filterView.endDateActionView(text: text) }
    
    func setSiteSelectedValue(text: String) { filterView.siteActionView(text: text) }
    
    func setEventClassSelectedValue(text: String) { filterView.eventClassActionView(text: text) }
    
    func clearEvents() {
        events = []
        tableView.reloadData()
    }
    
    func addEvent(_ model: Cell.Model, type: NUpdateType, isFromBD: Bool) {
        switch type {
        case .insert:
            if let max = events.map({ (e) -> Int64 in e.time }).max(), max <= model.time, !isFromBD, eTUDisp == nil {
                events.insert(model, at: 0)
                tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            }else {
                events.append(model)
                eTableUpdateEndDetecting()
            }
						updateEmptyEvents()
        case .update:
            guard let pos = events.firstIndex(where: {$0.id == model.id}) else{ break }
            events[pos] = model
            eTableUpdateEndDetecting()
        case .delete: break;
        }
    }
    
    func showLoader() {
        self.loaderView.isHidden = false
				self.updateEmptyEvents()
    }
    
    func hideLoader() {
        self.loaderView.isHidden = true
				self.updateEmptyEvents()
    }
    
    func setEvents(_ models: [Cell.Model]) {
        events = models
        tableView.reloadData()
    }
	
		private func updateEmptyEvents() {
				var hidden = false
			
				if events.count > 0 || loaderView.isHidden == false { hidden = true }
				if hidden == emptyTitleHidden { return }
				
				emptyTitleHidden = hidden
				emptyTitleView.isHidden = hidden
		}
    
    private func eTableUpdateEndDetecting() {
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.events = self.events.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.loaderView.isHidden = true
                self.tableView.reloadData()
                self.eTUDisp = nil
            }, onError: { e in
								self.updateEmptyEvents()
                self.loaderView.isHidden = true
                self.eTUDisp = nil
            })
    }
}
