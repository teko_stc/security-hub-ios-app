//
//  XEventsViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


struct XEventsViewCellStyle {
    let background, iconColor: UIColor
    let title, text, time: XBaseLableStyle
}

struct XEventsViewCellModel {
    struct Names {
        let site, device, section, zone: String
    }
    
    let id: Int64
    let icon: String
    let description: String
    let record: String?
    let time: Int64
    let names: Names
    let isDevice: Bool
		let location: (latitude: Double,longitude: Double)?
}
extension XEventsViewCell {
    typealias Model = XEventsViewCellModel
    typealias Style = XEventsViewCellStyle
}

class XEventsViewCell: UITableViewCell {
    static var identifier = "XEventsViewCell.identifier"

    private var model: Model?
    private var onViewTapped: ((Model) -> ())? = nil
    private var onRecordTapped: ((Model) -> ())? = nil
		private var onLocationTapped: ((Model) -> ())? = nil
   
    private var baseView: UIView = UIView()
    private var iconView: UIImageView = UIImageView()
    private var titleView: UILabel = UILabel(), textView: UILabel = UILabel(), text2View: UILabel = UILabel(), timeView: UILabel = UILabel()
    private var recordButtonView: UIButton = UIButton()
		private var locationButtonView: UIButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        baseView.transform = CGAffineTransform.identity
        baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabGesture)))
        baseView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longGesture)))
        contentView.addSubview(baseView)
        
        baseView.addSubview(iconView)
        baseView.addSubview(titleView)
        baseView.addSubview(textView)
        baseView.addSubview(text2View)
        baseView.addSubview(timeView)
        
        recordButtonView.imageView?.contentMode = .scaleAspectFit
        recordButtonView.contentVerticalAlignment = .fill
        recordButtonView.contentHorizontalAlignment = .fill
        recordButtonView.addTarget(self, action: #selector(recordButtonViewTapped), for: .touchUpInside)
        baseView.addSubview(recordButtonView)
			
				locationButtonView.imageView?.contentMode = .scaleAspectFit
				locationButtonView.contentVerticalAlignment = .fill
				locationButtonView.contentHorizontalAlignment = .fill
				locationButtonView.addTarget(self, action: #selector(locationButtonViewTapped), for: .touchUpInside)
				baseView.addSubview(locationButtonView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
 
    private func setConstraints() {
        [baseView, iconView, titleView, textView, text2View, timeView, recordButtonView, locationButtonView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            baseView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            baseView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            baseView.topAnchor.constraint(equalTo: contentView.topAnchor),
            baseView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            iconView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 20),
            iconView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            iconView.heightAnchor.constraint(equalToConstant: 40),
            iconView.widthAnchor.constraint(equalToConstant: 40),
            
            titleView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 14),
            
            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            textView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: timeView.leadingAnchor, constant: -10),
            
            text2View.topAnchor.constraint(equalTo: textView.bottomAnchor),
            text2View.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            text2View.trailingAnchor.constraint(equalTo: timeView.leadingAnchor, constant: -10),
            text2View.bottomAnchor.constraint(lessThanOrEqualTo: baseView.bottomAnchor, constant: -10),
            
            timeView.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 4),
            timeView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 20),
            timeView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),
            timeView.widthAnchor.constraint(equalToConstant: 42),
            
            recordButtonView.topAnchor.constraint(equalTo: timeView.bottomAnchor),
            recordButtonView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 22),
            recordButtonView.trailingAnchor.constraint(equalTo: timeView.trailingAnchor),
            recordButtonView.bottomAnchor.constraint(lessThanOrEqualTo: baseView.bottomAnchor, constant: -4),
            recordButtonView.heightAnchor.constraint(equalToConstant: 44),
						
						locationButtonView.topAnchor.constraint(equalTo: timeView.bottomAnchor),
						locationButtonView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 22),
						locationButtonView.trailingAnchor.constraint(equalTo: timeView.trailingAnchor),
						locationButtonView.bottomAnchor.constraint(lessThanOrEqualTo: baseView.bottomAnchor, constant: -4),
						locationButtonView.heightAnchor.constraint(equalToConstant: 44),
        ])
    }
    
    public func setContent(model: Model) {
        self.model = model
        iconView.image = UIImage(named: model.icon)
				titleView.text = model.description
        textView.text = "\(model.names.site)"
        text2View.text = model.isDevice ? "\(model.names.device)" : "\(model.names.zone) · \(model.names.section)"
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM\nHH:mm"
        timeView.text = dateFormatterGet.string(from: Date(timeIntervalSince1970: Double(model.time)))
				
        let ic_camera = model.record == "clip/failed" ? "ic_camera_failed_filled" : "ic_camera_filled"
        recordButtonView.setImage(UIImage(named: ic_camera), for: .normal)
        recordButtonView.isHidden = XTargetUtils.ivideon == nil || model.record == nil

				locationButtonView.setImage(UIImage(named: "ic_location"), for: .normal)
				locationButtonView.isHidden = model.location == nil
    }
    
    public func setDelegate(onViewTapped: @escaping (Model) -> (), onRecordTapped: @escaping (Model) -> (), onLocationTapped: @escaping (Model) -> ()) {
        self.onViewTapped = onViewTapped
        self.onRecordTapped = onRecordTapped
				self.onLocationTapped = onLocationTapped
    }
    
    public func setStyle(_ style: Style) {
        backgroundColor = style.background
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = style.background
        
        iconView.tintColor = style.iconColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        titleView.numberOfLines = 4

        textView.setStyle(style.text)
        textView.numberOfLines = 1
        
        text2View.setStyle(style.text)
        text2View.numberOfLines = 1

        timeView.font = style.time.font
        timeView.textColor = style.time.color
        timeView.textAlignment = style.time.alignment
        timeView.numberOfLines = 0
        
//        recordButtonView.tintColor = style.iconColor
    }

    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.onViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.onViewTapped?(model) }
            })
        }
    }
    
    @objc private func recordButtonViewTapped() {
        if let model = self.model { self.onRecordTapped?(model) }
    }
	
		@objc private func locationButtonViewTapped() {
				if let model = self.model { self.onLocationTapped?(model) }
		}
}
