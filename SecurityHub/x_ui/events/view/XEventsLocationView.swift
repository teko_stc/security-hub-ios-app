//
//  XEventsLocationView.swift
//  SecurityHub
//
//  Created by Stefan on 22.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

protocol XEventsLocationViewDelegate {
		func closeViewTapped()
		func centerViewTapped()
		func openMapsViewTapped()
		func copyViewTapped()
		func shareViewTapped()
}

class XEventsLocationView: GMSMapView {
		public var locationDelegate: XEventsLocationViewDelegate?
		private var closeView, centerView, openMapsView, copyView, shareView: XZoomButton!
		private var strings = XEventsLocationViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		public func setDefault() {
		}
		
		private func initViews() {
			
				closeView = XZoomButton (style: XEventsLocationViewStyles.regularButtonStyle())
				closeView.addTarget(self, action: #selector(closeViewTapped), for: .touchUpInside)
				closeView.setImage (UIImage(named:"ic_location_close"), for: .normal)
				addSubview(closeView)
			
				centerView = XZoomButton (style: XEventsLocationViewStyles.regularButtonStyle())
				centerView.addTarget(self, action: #selector(centerViewTapped), for: .touchUpInside)
				centerView.setImage (UIImage(named:"ic_location_center"), for: .normal)
				addSubview(centerView)
								
				openMapsView = XZoomButton (style: XEventsLocationViewStyles.roundedButtonStyle())
				openMapsView.text = strings.openMapsTitle
				openMapsView.addTarget(self, action: #selector(openMapsViewTapped), for: .touchUpInside)
				addSubview(openMapsView)
			
				let copyIcon = UIImage(named:"ic_location_copy")
				copyView = XZoomButton (style: XEventsLocationViewStyles.roundedButtonStyle())
				copyView.setImage (copyIcon, for: .normal)
				copyView.setImage (copyIcon, for: .highlighted)
				copyView.addTarget(self, action: #selector(copyViewTapped), for: .touchUpInside)
				addSubview(copyView)
			
				let shareIcon = UIImage(named:"ic_location_share")
				shareView = XZoomButton (style: XEventsLocationViewStyles.roundedButtonStyle())
				shareView.setImage (shareIcon, for: .normal)
				shareView.setImage (shareIcon, for: .highlighted)
				shareView.addTarget(self, action: #selector(shareViewTapped), for: .touchUpInside)
				addSubview(shareView)
		}
		
		private func setConstraints() {
				closeView.translatesAutoresizingMaskIntoConstraints = false
				centerView.translatesAutoresizingMaskIntoConstraints = false
				openMapsView.translatesAutoresizingMaskIntoConstraints = false
				copyView.translatesAutoresizingMaskIntoConstraints = false
				shareView.translatesAutoresizingMaskIntoConstraints = false
				NSLayoutConstraint.activate([
					
					closeView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 17),
					closeView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 17),
					closeView.widthAnchor.constraint(equalToConstant: 56),
					closeView.heightAnchor.constraint(equalToConstant: 56),
					
					centerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 17),
					centerView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -17),
					centerView.widthAnchor.constraint(equalToConstant: 56),
					centerView.heightAnchor.constraint(equalToConstant: 56),
					
					copyView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -15),
					copyView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 18),
					copyView.trailingAnchor.constraint(equalTo: centerXAnchor, constant: -9),
					copyView.heightAnchor.constraint(equalToConstant: 40),
					
					shareView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -15),
					shareView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -18),
					shareView.leadingAnchor.constraint(equalTo: centerXAnchor, constant: 9),
					shareView.heightAnchor.constraint(equalToConstant: 40),

					openMapsView.bottomAnchor.constraint(equalTo: copyView.topAnchor, constant: -10),
					openMapsView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 18),
					openMapsView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -18),
					openMapsView.heightAnchor.constraint(equalToConstant: 40)
				])
		}
	
		@objc private func closeViewTapped() {
			locationDelegate?.closeViewTapped()
		}
	
		@objc private func centerViewTapped() {
			locationDelegate?.centerViewTapped()
		}
	
		@objc private func openMapsViewTapped() {
			locationDelegate?.openMapsViewTapped()
		}
		
		@objc private func copyViewTapped() {
			locationDelegate?.copyViewTapped()
		}
		
		@objc private func shareViewTapped() {
			locationDelegate?.shareViewTapped()
		}
}
