//
//  XEventsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import Eureka

protocol XEventsControllerStrings {
    var siteBottomSheetTitle: String { get }
    var eventClassBottomSheetTitle: String { get }
}

protocol XEventsControllerStyle {
    var bottomSheet: XBottomSheetView.Style { get }
}
extension XEventsController {
    typealias Style = XEventsControllerStyle
    typealias Strings = XEventsControllerStrings
}

class XEventsController: XBaseViewController<XEventsView> {    
    private var style = XEventsViewControllerStyles()
    private var strings = XEventsViewControllerStrings()
    
    private lazy var viewLayer: XEventsViewViewLayer = mainView
    
    private var site: Sites?
    private var eventClass: Int?
    private var startTime: Int64 = Int64(Date().dayBefore.noon.timeIntervalSince1970)
    private var endTime: Int64 = Int64(Date().noon2.timeIntervalSince1970)
    private var disposable: Disposable?
    private var isLoader: Bool = false
    private var putAsidedModels: [(XEventsViewCellModel, NUpdateType)] = []
		private var observerEventsLoader: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        
        NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.current) { [weak self] notification in
            guard let self = self, let e = notification.object as? (event: Events, type: NUpdateType) else { return }
            guard e.event.time >= self.startTime &&  e.event.time <= self.endTime else { return }
            guard self.eventClass == nil || self.eventClass ?? -1 == e.event._class else { return }

						if DataManager.shared.isEventsLoading == false {
							self.updateEvent(eventId: e.event.id, type: e.type) }
        }
        NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(), object: nil, queue: OperationQueue.current) { [weak self] notification in
            guard let self = self, let ivVideo = notification.object as? IvVideo else { return }
					
						self.updateEvent(eventId: ivVideo.eventId, type: .update)
        }
        
				if DataManager.shared.isEventsLoading {
					observerEventsLoader = NotificationCenter.default.addObserver(forName: HubNotification.eventsLoadEnd, object: nil, queue: OperationQueue.current) { [weak self] notification in
							guard let self = self else { return }
						
							NotificationCenter.default.removeObserver(self.observerEventsLoader!, name: notification.name, object: notification.object)
							self.load()
					}

					viewLayer.showLoader()
				} else { load() }
    }
    
    private func updateEvent(eventId: Int64, type: NUpdateType) {
        DataManager.shared.getDBEvent(id: eventId) { [weak self] de in
            guard let self = self, let de = de else { return }
            guard [0,1,2,3,4,5,8,10,11].contains(de._class) && (de._class != 10 && de.reason != 4 ) else { return }
            guard de.time >= self.startTime &&  de.time <= self.endTime else { return }
            guard self.eventClass == nil || self.eventClass ?? -1 == de._class else { return }
            guard self.site?.id == nil || self.site?.id ?? 0 == de.site else { return }

            let model = XEventsViewCellModel(
                id: de.id,
                icon: de.icon,
                description: de.affect_desc,
                record: de.videoRecord,
                time: de.time,
                names: XEventsViewCellModel.Names(site: de.siteName, device: de.deviceName, section: de.sectionName ?? "", zone: de.zoneName ?? ""),
                isDevice: de.section == 0 && (de.zone == 0 || de.zone == 100),
								location: LocationManager.getLocation(jdata: de.jdata)
            )
            
            if self.isLoader {
                self.putAsidedModels.append((model, type))
            } else {
                self.viewLayer.addEvent(model, type: type, isFromBD: false)
            }
        }
    }
    
    private func load() {
        isLoader = true
        putAsidedModels = []
        disposable?.dispose()
        viewLayer.clearEvents()
        
        let _class: Int64? = eventClass?.int64, siteId: Int64? = site?.id
        viewLayer.showLoader()
        DataManager.shared.getDBEvents(startTime: startTime, endTime: endTime, _class: _class, siteId: siteId) { [weak self] dEvents in
            guard let self = self else { return }
					let models = dEvents
                .filter({ (de: DEventEntity) -> Bool in
                    return [0,1,2,3,4,5,8,10,11].contains(de._class) && (de._class != 10 && de.reason != 4 )
                })
                .map({ (de: DEventEntity) -> XEventsViewCellModel in
                    XEventsViewCellModel(
                        id: de.id,
                        icon: de.icon,
												description: de.affect_desc,
                        record: de.videoRecord,
                        time: de.time,
                        names: XEventsViewCellModel.Names(site: de.siteName, device: de.deviceName, section: de.sectionName ?? "", zone: de.zoneName ?? ""),
												isDevice: de.section == 0 && (de.zone == 0 || de.zone == 100),
												location: LocationManager.getLocation (jdata: de.jdata)
                    )
                })
            
            if dEvents.filter({ $0.active == 0 && $0._class != 4 }).count == 0 {
                _ = DataManager.shared.hubHelper.get(.EVENTS, D: ["tmin": self.startTime, "tmax": self.endTime, "affect" : 0])
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe(onSuccess: { [weak self] (result: DCommandResult, value: HubEvents?) in
                        guard let self = self else { return }

                        if (result.success && value?.count ?? 0 > 0) {
                            self.load()
                        } else {
                            self.isLoader = false
                            self.viewLayer.hideLoader()
                        }
                    }, onError: { [weak self] error in
                        guard let self = self else { return }
                        self.isLoader = false
                        self.viewLayer.hideLoader()
                    })
            }
            if dEvents.count > 0 {
                self.isLoader = false
                self.viewLayer.hideLoader()
                self.viewLayer.setEvents(models)
                self.putAsidedModels.forEach({ (model, type) in
                    self.viewLayer.addEvent(model, type: type, isFromBD: false)
                })
						}
        }
    }
}

extension XEventsController: XEventsViewDelegate {
    func eventViewTapped(model: XEventsView.Cell.Model) {
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: model.record, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: model.location))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func eventRecordViewTapped(model: XEventsView.Cell.Model) {
        if model.record == "clip/failed" || model.record == nil {
            showErrorColtroller(message: "IV_CLIP_FAILED".localized())
            return
        }

        let event = XCameraEventViewLayerModel(id: model.id,
                                               icon: model.icon,
                                               description: model.description,
                                               record: model.record,
                                               time: model.time,
                                               names: XCameraEventViewLayerModel.Names(site: model.names.site,
                                                                                       device: model.names.device,
                                                                                       section: model.names.section,
																																											 zone: model.names.zone),location: model.location)
        let controller = XCameraEventRecordController(model: event)
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func eventLocationViewTapped(model: XEventsView.Cell.Model) {
			let elc = XEventsLocationController()
			elc.location = model.location
			elc.eventTitle = "\(model.description), \(model.names.site), \(model.names.device)"
			elc.modalPresentationStyle = .fullScreen
			navigationController?.present(elc, animated: true)
		}
    
    func siteActionViewTapped() {
        var items = DataManager.shared.dbHelper.getSites().map { site in XBottomSheetViewItem(title: site.name, text: nil, any: site) }
        items.insert(XBottomSheetViewItem(title: strings.siteActionDefaultTitle, text: nil, any: nil), at: 0)
        let index = items.firstIndex(where: { ($0.any as? Sites)?.id == site?.id }) ?? 0
        let bottomSheet = XBottomSheetController(style: style.bottomSheet, title: strings.siteBottomSheetTitle, selectedIndex: index, items: items)
        bottomSheet.onSelectedItem = { sheet, index, item in
            self.site = item.any as? Sites
            self.viewLayer.setSiteSelectedValue(text: item.title)
            self.load()
            sheet.dismiss(animated: true)
        }
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func eventClassActionViewTapped() {
        var items = DataManager.shared.dbHelper.getAllClasses().filter({ [0,1,2,3,4,5,8,10,11].contains($0.id) }).map { _class in XBottomSheetViewItem(title: _class.name, text: nil, any: _class.id.int) }
        items.insert(XBottomSheetViewItem(title: strings.eventClassActionDefaultTitle, text: nil, any: nil), at: 0)
        let index = items.firstIndex(where: { ($0.any as? Int) == eventClass }) ?? 0
        let bottomSheet = XBottomSheetController(style: style.bottomSheet, title: strings.siteBottomSheetTitle, selectedIndex: index, items: items)
        bottomSheet.onSelectedItem = { sheet, index, item in
            self.eventClass = item.any as? Int
            self.viewLayer.setEventClassSelectedValue(text: item.title)
            self.load()
            sheet.dismiss(animated: true)
        }
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func startDateActionViewTapped() {
        let alert = AlertUtil.alertDatePicker({ date in
						self.setStartTimeValue(date: date)
						if self.endTime < Int64(date.noon.timeIntervalSince1970) {
							if date.dayAfter.noon > Date().noon { self.setEndTimeValue(date: date) }
							else { self.setEndTimeValue(date: date.dayAfter) } }
            self.load()
            self.updateBackgroundFilter()
        }, max: Int64(Date().noon.timeIntervalSince1970), min: Int64(NSDate().timeIntervalSince1970 - (86400 * 365)), _default: self.startTime)
        navigationController?.present(alert, animated: true)
    }
	
		private func setStartTimeValue (date: Date) {
				startTime = Int64(date.noon.timeIntervalSince1970)
				if (startTime == Int64(Date().dayBefore.timeIntervalSince1970)) { viewLayer.setStartTimeSelectedValue(text: strings.yesterday) }
				else if (startTime == Int64(Date().noon.timeIntervalSince1970)) { viewLayer.setStartTimeSelectedValue(text: strings.today) }
				else { viewLayer.setStartTimeSelectedValue(text: startTime.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none)) }
		}
    
    func endDateActionViewTapped() {
        let alert = AlertUtil.alertDatePicker({ date in
						self.setEndTimeValue(date: date)
						if self.startTime >= Int64(date.noon.timeIntervalSince1970) {
							self.setStartTimeValue(date: date.dayBefore) }
            self.load()
            self.updateBackgroundFilter()
        }, max: Int64(Date().noon.timeIntervalSince1970), min: Int64(NSDate().timeIntervalSince1970 - (86400 * 365)), _default: self.endTime)
        navigationController?.present(alert, animated: true)
    }
	
		private func setEndTimeValue (date: Date) {
				endTime = Int64(date.noon2.timeIntervalSince1970)
				if endTime == Int64(Date().dayBefore.noon2.timeIntervalSince1970) {
					viewLayer.setEndTimeSelectedValue(text: strings.yesterday) }
				else if (endTime == Int64(Date().noon2.timeIntervalSince1970)) { viewLayer.setEndTimeSelectedValue(text: strings.today) }
				else { viewLayer.setEndTimeSelectedValue(text: endTime.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none)) }
		}

    func updateBackgroundFilter() {
        let isAlternative = endTime != Int64(Date().noon2.timeIntervalSince1970) || startTime != Int64(Date().dayBefore.noon.timeIntervalSince1970)
        viewLayer.updateBackgroundFilter(isAlternative: isAlternative)
    }
}
