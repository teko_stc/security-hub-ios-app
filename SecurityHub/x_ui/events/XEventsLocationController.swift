//
//  XEventsLocationController.swift
//  SecurityHub
//
//  Created by Stefan on 22.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XEventsLocationController: XBaseViewController<XEventsLocationView>, XEventsLocationViewDelegate {
	var eventTitle: String?
	var location: (latitude: Double,longitude: Double)?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		mainView.locationDelegate = self
		centerViewTapped()
		mainView.isMyLocationEnabled = true
	}
	
	private func getGoogleUrl() -> String
	{
		if let location = location
		{
			return "https://maps.google.com/?q=\(location.latitude),\(location.longitude)"
		}
		
		return ""
	}
	
	func closeViewTapped(){
		presentingViewController?.dismiss(animated: true)
	}
	
	func centerViewTapped() {
		if let location = location
		{
			mainView.camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: 20.0)
			let marker = GMSMarker()
			marker.position = CLLocationCoordinate2D (latitude: location.latitude, longitude: location.longitude)
			marker.title = eventTitle
			marker.map = mainView
		}
	}
	
	func openMapsViewTapped() {
		if let location = location
		{
			let title = (eventTitle?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) ?? ""
			//let appleUrl = "maps://?q=\(title)&ll=\(location.latitude),\(location.longitude)"
			let appleUrl = "http://maps.apple.com/?q=\(title)&ll=\(location.latitude),\(location.longitude)"
			let googleUrl = "comgooglemaps://?label=\(title)&q=\(location.latitude),\(location.longitude)"
			let yandexUrl = "yandexmaps://maps.yandex.ru/?ll=\(location.latitude),\(location.longitude)&z=12&l=map"
			let urls : [String:String] = ["Apple Maps":appleUrl,"Google Maps":googleUrl,"Yandex Maps":yandexUrl]
			
			let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
			urls.forEach { (key: String, value: String) in
//				let url = URL(string: value)
				//if let url = url
				//{
					//if (UIApplication.shared.canOpenURL(url)) {
						alert.addAction(UIAlertAction(title: key, style: .default, handler: { action in
							UIApplication.shared.open(URL(string: urls[key]!)!)
						}))
					//}
				//}
			}
			
			if alert.actions.count == 1 {
				UIApplication.shared.open(URL(string: appleUrl)!)
			} else {
				self.present(alert, animated: true)
			}
		}
	}
	
	func copyViewTapped() {
		UIPasteboard.general.url = URL(string: getGoogleUrl())
		showToast(message: "GMS_COPY_LOCATION".localized())
	}
	
	func shareViewTapped() {
		let items = [URL(string: getGoogleUrl())]
		let shareViewController = UIActivityViewController(activityItems: items as [Any], applicationActivities: nil)
		shareViewController.popoverPresentationController?.sourceView = mainView
		
		self.present(shareViewController, animated: true, completion: nil)
	}
}
