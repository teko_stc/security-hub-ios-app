//
//  XEventsViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XEventsViewControllerStrings: XEventsViewStrings, XEventsControllerStrings {
    /// Выберите объект
    var siteBottomSheetTitle: String { "N_EVENTS_CHOOSE_SITE".localized() }
    
    /// Выберите класс
    var eventClassBottomSheetTitle: String { "N_EVENTS_CHOOSE_CLASS".localized() }
    
    /// Событий нет.\nПроверьте фильтр
    var emptyListTitle: String { "N_EVENTS_NO_EVENTS".localized() }
    
    /// Вчера
    var yesterday: String { "FUNC_YESTERDAY".localized() }
    
    /// Сегодня
    var today: String { "FUNC_TODAY".localized() }
    
    /// Фильтр
    var title: String { "N_EVENTS_FILTER".localized() }
    
    /// Объект:
    var siteTitle: String { "N_EVENTS_SITE".localized() }
    
    /// Все объекты
    var siteActionDefaultTitle: String { "N_EVENTS_ALL_SITES".localized() }
    
    /// Класс:
    var eventClassTitle: String { "N_EVENTS_CLASS".localized() }
    
    /// Все события
    var eventClassActionDefaultTitle: String { "N_EVENTS_ALL_CLASSES".localized() }
}

class XEventsLocationViewStrings {
		var openMapsTitle: String { "GMS_OPEN_MAPS".localized() }
}
