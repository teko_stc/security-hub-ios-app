//
//  XEventsViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XEventsViewControllerStyles: XEventsViewStyle, XEventsControllerStyle {
    var bottomSheet: XBottomSheetView.Style {
        XBottomSheetView.Style(
            backgroundColor: .white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: .white,
                selectedBackgroundColor: .white,
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                ),
                text: nil
            )
        )
    }
    
    var backgroundColor: UIColor { .white }
    
    var loaderColor: UIColor { UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1) }
    
    var filter: XEventsFilterView.Style {
        XEventsFilterView.Style(
            backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            backgroundColorAlternative: UIColor.colorFromHex(0x008000),
            tintColor: .white,
            font: UIFont(name: "Open Sans", size: 15.5),
            largeFont: UIFont(name: "Open Sans", size: 16.5)
        )
    }
    
    var emptyTitle: XBaseLableStyle {
        XBaseLableStyle(
            color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
            font: UIFont(name: "Open Sans", size: 15.5),
            alignment: .left
        )
    }
    
    var cell: XEventsView.Cell.Style {
        XEventsView.Cell.Style(
            background: .white,
            iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            title: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .left
            ),
            text: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .left
            ),
            time: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            )
        )
    }
}

struct XEventsLocationViewStyles
{
	static func backgroundColor () -> UIColor
	{
		return UIColor.white
	}
	
	static func roundedButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.white,
			font: UIFont(name: "Open Sans", size: 20),
			color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
		)
	}
	
	static func regularButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 16),
			color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			alignment: .center
		)
	}
}
