//
//  XRecoveryConfirmRightsView.swift
//  SecurityHub
//
//  Created by Stefan on 10.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryConfirmRightsView: UIView, XCheckBoxViewDelegate {
	
		public var delegate: XRegistrationCompletionDelegate?
		private var titleView = UILabel()
		private var scrollView = UIScrollView()
		private var contentView: UIView = UIView()
		private var objectView: XTextField!
		private var objectSubView = UILabel()
		private var notArmedView: XCheckBoxView!
		private var dateTimeView: XTextField!
		private var dateTimeSubView = UILabel()
		private var nextButtonView: XZoomButton!
		private var strings = XRecoveryConfirmRightsViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
		override func layoutSubviews() {
				super.layoutSubviews()

				scrollView.contentSize = contentView.frame.size
		}
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
			
				titleView.backgroundColor = UIColor.clear
				titleView.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
				titleView.font = UIFont(name: "Open Sans", size: 13)
				titleView.textAlignment = .left
				titleView.lineBreakMode = .byWordWrapping
				titleView.numberOfLines = 0
				titleView.text = strings.title
				addSubview(titleView)
			
				addSubview(scrollView)
			
				scrollView.addSubview(contentView)
			
				objectView = XTextField (style: XRegistrationStyles.textFieldNormalStyle())
				objectView.textContentType = .name
				objectView.keyboardType = .default
				objectView.returnKeyType = .next
				objectView.autocorrectionType = .no
				objectView.autocapitalizationType = .none
				objectView.spellCheckingType = .no
				objectView.xDelegate = self
				objectView.placeholder = strings.objectName
				contentView.addSubview(objectView)
			
				objectSubView.backgroundColor = UIColor.clear
				objectSubView.textColor = UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1)
				objectSubView.font = UIFont(name: "Open Sans", size: 13)
				objectSubView.textAlignment = .center
				objectSubView.lineBreakMode = .byWordWrapping
				objectSubView.numberOfLines = 0
				objectSubView.text = strings.objectNameAny
				contentView.addSubview(objectSubView)
		 
				notArmedView = XCheckBoxView (XRegistrationStyles.checkBoxStyle())
				notArmedView.text = strings.deviceNotArmed
				notArmedView.delegate = self
				contentView.addSubview(notArmedView)
			
				dateTimeView = XTextField (style: XRegistrationStyles.textFieldNormalStyle())
				dateTimeView.textContentType = .name
				dateTimeView.keyboardType = .default
				dateTimeView.returnKeyType = .done
				dateTimeView.autocorrectionType = .no
				dateTimeView.autocapitalizationType = .none
				dateTimeView.spellCheckingType = .no
				dateTimeView.xDelegate = self
				dateTimeView.placeholder = strings.dateTime
				let datePicker: UIDatePicker = UIDatePicker()
				datePicker.frame = self.frame
				datePicker.datePickerMode = .date
				datePicker.timeZone = NSTimeZone.local
				datePicker.locale = Locale(identifier: DataManager.defaultHelper.language)
				datePicker.backgroundColor = UIColor.white
				datePicker.date = Date()
				datePicker.frame = CGRect (x: 0, y:frame.size.height - 216.0, width: frame.size.width, height: 216.0)
				if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels }
				datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
				dateTimeView.inputView = datePicker
				let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
				let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//				let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dateTimeDoneTapped))
				let done = UIBarButtonItem(title: "OK".localized(), style: .plain, target: self, action: #selector(dateTimeDoneTapped))
				let dateTimeSwitch = UIBarButtonItem(title: "REC_USER_TIME".localized(), style: .plain, target: self, action: #selector(dateTimeSwitchTapped))
				toolbar.setItems([dateTimeSwitch,flexSpace,done], animated: false)
				dateTimeView.inputAccessoryView = toolbar
				contentView.addSubview(dateTimeView)
			
				dateTimeSubView.backgroundColor = UIColor.clear
				dateTimeSubView.textColor = UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1)
				dateTimeSubView.font = UIFont(name: "Open Sans", size: 13)
				dateTimeSubView.textAlignment = .center
				dateTimeSubView.lineBreakMode = .byWordWrapping
				dateTimeSubView.numberOfLines = 0
				dateTimeSubView.text = strings.approxTime
				contentView.addSubview(dateTimeSubView)
								
				nextButtonView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				nextButtonView.text = strings.next
				nextButtonView.loadingText = " "
				nextButtonView.addTarget(self, action: #selector(registrationCompleteViewTapped), for: .touchUpInside)
				nextButtonView.style.disableBackground = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 1)
				nextButtonView.style.disable = UIColor.white
				nextButtonView.isEnabled = false
				addSubview(nextButtonView)
		}
		
		private func setConstraints() {
				titleView.translatesAutoresizingMaskIntoConstraints = false
				scrollView.translatesAutoresizingMaskIntoConstraints = false
				contentView.translatesAutoresizingMaskIntoConstraints = false
				objectView.translatesAutoresizingMaskIntoConstraints = false
				objectSubView.translatesAutoresizingMaskIntoConstraints = false
				notArmedView.translatesAutoresizingMaskIntoConstraints = false
				dateTimeView.translatesAutoresizingMaskIntoConstraints = false
				dateTimeSubView.translatesAutoresizingMaskIntoConstraints = false
				nextButtonView.translatesAutoresizingMaskIntoConstraints = false

				NSLayoutConstraint.activate([
					titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 23),
					titleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
					titleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
					
					scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 23),
					scrollView.bottomAnchor.constraint(equalTo: nextButtonView.topAnchor, constant: -23),
					scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
					scrollView.widthAnchor.constraint(equalTo: widthAnchor),
					
					contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
					contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
					contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
					contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 20),
					contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40),
					contentView.heightAnchor.constraint(equalToConstant: 267),
					
					objectView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
					objectView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
					objectView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
					objectView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
					objectView.heightAnchor.constraint(equalToConstant: 52),
					
					objectSubView.topAnchor.constraint(equalTo: objectView.bottomAnchor, constant: 3),
					objectSubView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20),
					objectSubView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
					
					notArmedView.topAnchor.constraint(equalTo: objectSubView.bottomAnchor, constant: 24),
					notArmedView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
					notArmedView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
					notArmedView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
					notArmedView.heightAnchor.constraint(equalToConstant: 20),
					
					dateTimeView.topAnchor.constraint(equalTo: notArmedView.bottomAnchor, constant: 11),
					dateTimeView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
					dateTimeView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
					dateTimeView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
					dateTimeView.heightAnchor.constraint(equalToConstant: 52),
					
					dateTimeSubView.topAnchor.constraint(equalTo: dateTimeView.bottomAnchor, constant: 11),
					dateTimeSubView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
					dateTimeSubView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
					
					nextButtonView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -37),
					nextButtonView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 87),
					nextButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -174),
					nextButtonView.heightAnchor.constraint(equalToConstant: 40),
				])
		}

		private func trimmingString (string: String) -> String
		{
			return string.trimmingCharacters (in: .whitespacesAndNewlines)
		}
	
		private func validateData () -> Bool
		{
			var result = true
			
			if objectView.text?.count == 0 { result = false } else
			if notArmedView.isSelected == false && dateTimeView.text?.count == 0 { result = false }
			
			if result { nextButtonView.isEnabled = true }
			else { nextButtonView.isEnabled = false }
			
			return result
		}
	
		public func startLoading ()
		{
			nextButtonView.startLoading()
		}
		
		public func stopLoading ()
		{
			nextButtonView.endLoading()
		}
	
		func xCheckBoxViewChangeState(isSelected: Bool) {
			if isSelected
			{
				if dateTimeView.isFirstResponder { dateTimeView.resignFirstResponder() }
				scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: false)
				dateTimeView.isHidden = true
				dateTimeSubView.isHidden = true
			}
			else
			{
				dateTimeView.isHidden = false
				dateTimeSubView.isHidden = false
			}
			
			_ = validateData()
		}
	
		@objc func datePickerValueChanged(_ sender: UIDatePicker){
				
				let dateFormatter: DateFormatter = DateFormatter()
				dateFormatter.dateFormat = "HH:mm dd.MM.yy"
				dateTimeView.text = dateFormatter.string(from: sender.date)
		}
	
		@objc private func dateTimeDoneTapped() {
			dateTimeView.resignFirstResponder()
			scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: false)
			_ = validateData()
		}
	
		@objc private func dateTimeSwitchTapped() {
			if let toolbar = dateTimeView.inputAccessoryView as? UIToolbar, let picker = dateTimeView.inputView as? UIDatePicker
			{
				if let item = toolbar.items?.first
				{
					if picker.datePickerMode == .date {
						item.title = "REC_USER_DATE".localized()
						picker.datePickerMode = .time
					} else {
						item.title = "REC_USER_TIME".localized()
						picker.datePickerMode = .date
					}
				}
			}
		}
		
		@objc private func registrationCompleteViewTapped() {
			var userData = RegUserData()
			userData.name = trimmingString(string: objectView.text!)
			if notArmedView.isSelected == true { userData.code = 0 }
			else
			{
				if let dateString = dateTimeView.text
				{
					let dateFormatter: DateFormatter = DateFormatter()
					dateFormatter.dateFormat = "HH:mm dd.MM.yy"
					let date = dateFormatter.date(from: dateString)
					userData.code = UInt (date!.timeIntervalSince1970)
				}
			}
			
			delegate?.registrationCompleteViewTapped(userData: userData)
		}
		
		@objc private func viewTapped() {
				scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
			
				if objectView.isFirstResponder { objectView.resignFirstResponder()} else
				if dateTimeView.isFirstResponder { dateTimeView.resignFirstResponder()}
		}
}

extension XRecoveryConfirmRightsView: XTextFieldDelegete {
	
	func xTextFieldShouldReturn(_ textField: UITextField) {
		if objectView.isFirstResponder {
			objectView.resignFirstResponder()
			if notArmedView.isSelected == false { dateTimeView.becomeFirstResponder() }
		} else
		if dateTimeView.isFirstResponder {
			scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: false)
			dateTimeView.resignFirstResponder()
		}
	}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		if textField.isEqual(dateTimeView) { return false }
		
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
		if textField.isEqual(dateTimeView)
		{
			let point = CGPoint (x: 0, y: dateTimeView.frame.origin.y - 48 )
			scrollView.setContentOffset(point, animated: true)
		}
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
		if textField.isEqual(dateTimeView)
		{
			scrollView.scrollRectToVisible(dateTimeView.frame, animated: true)
		}
		
		_ = validateData()
	}
}
