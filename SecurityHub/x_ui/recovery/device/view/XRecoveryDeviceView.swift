//
//  XRecoveryDeviceView.swift
//  SecurityHub
//
//  Created by Stefan on 10.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryDeviceView: UIView {
		public var delegate: XRecoveryDeviceDelegate?
		private var loginView: XTextField!
		private var deviceView: XTextField!
		private var nextButtonView: XZoomButton!
		private var strings = XRecoveryDeviceViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		public func setDefault(device: String) {
			deviceView.text = device
			
			if loginView.text?.count ?? 0 > 0 && device.count > 4 { nextButtonView.isEnabled = true }
			else { nextButtonView.isEnabled = false }
			
			_ = deviceView.textFieldShouldBeginEditing(deviceView)
			_ = deviceView.textFieldShouldEndEditing(deviceView)
		}
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
			
				loginView = XTextField (style: XRegistrationStyles.textFieldNormalStyle())
				loginView.textContentType = .name
				loginView.keyboardType = .default
				loginView.returnKeyType = .next
				loginView.autocorrectionType = .no
				loginView.autocapitalizationType = .none
				loginView.spellCheckingType = .no
				loginView.xDelegate = self
				loginView.placeholder = strings.login
				addSubview(loginView)

				deviceView = XTextField (style: XRegistrationStyles.textFieldNormalStyle())
				deviceView.textContentType = .telephoneNumber
				deviceView.keyboardType = .numberPad
				deviceView.returnKeyType = .done
				deviceView.autocorrectionType = .no
				deviceView.autocapitalizationType = .none
				deviceView.spellCheckingType = .no
				deviceView.xDelegate = self
				deviceView.placeholder = strings.serialNumber
				let rightView = UIButton()
				rightView.setImage (UIImage(named: "ic_reg_device_serial"), for: .normal)
				rightView.frame = CGRect(x: 0, y: 0, width: 52, height: 52)
				if #available(iOS 13.0, *) {
					rightView.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10) }
				rightView.addTarget(self, action: #selector(regSerialNumberViewTapped), for: .touchUpInside)
				deviceView.rightView = rightView
				addSubview(deviceView)
								
				nextButtonView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				nextButtonView.text = strings.next
				nextButtonView.loadingText = strings.next
				nextButtonView.addTarget(self, action: #selector(nextViewTapped), for: .touchUpInside)
				nextButtonView.isEnabled = false
				nextButtonView.style.disableBackground = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 1)
				nextButtonView.style.disable = UIColor.white
				addSubview(nextButtonView)
		}
		
		private func setConstraints() {
				loginView.translatesAutoresizingMaskIntoConstraints = false
				deviceView.translatesAutoresizingMaskIntoConstraints = false
				nextButtonView.translatesAutoresizingMaskIntoConstraints = false
			
				NSLayoutConstraint.activate([
					loginView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 30),
					loginView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
					loginView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -40),
					loginView.heightAnchor.constraint(equalToConstant: 52),
					
					deviceView.topAnchor.constraint(equalTo: loginView.bottomAnchor, constant: 24),
					deviceView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
					deviceView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -40),
					deviceView.heightAnchor.constraint(equalToConstant: 52),
					
					nextButtonView.topAnchor.constraint(equalTo: deviceView.bottomAnchor, constant: 22),
					nextButtonView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 87),
					nextButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -174),
					nextButtonView.heightAnchor.constraint(equalToConstant: 40),
				])
		}
	
		private func trimmingString (string: String) -> String
		{
			return string.trimmingCharacters (in: .whitespacesAndNewlines)
		}
		
		@objc private func nextViewTapped() {
			viewTapped()
			
			var user = RegUserData()
			if let str = deviceView.text { user.sn = UInt(str)! }
			user.login = loginView.text!
			delegate?.nextViewTapped(user: user)
		}
	
		@objc private func regSerialNumberViewTapped() { delegate?.regSerialNumberViewTapped() }
		
		@objc private func viewTapped() {
			if loginView.isFirstResponder { loginView.resignFirstResponder() }
			if deviceView.isFirstResponder { deviceView.resignFirstResponder() }
		}
}

extension XRecoveryDeviceView: XTextFieldDelegete {
		func xTextFieldShouldReturn(_ textField: UITextField) {
			if loginView.isFirstResponder {
				loginView.resignFirstResponder()
				deviceView.becomeFirstResponder() } else
			if deviceView.isFirstResponder { deviceView.resignFirstResponder() }
		}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		if let text = textField.text,
				let textRange = Range(range, in: text) {
				let updatedText = text.replacingCharacters(in: textRange, with: string)
			
				if updatedText.count > 32 { return false }
			
				var loginCount = 0
				var deviceCount = 0
			
				if loginView.isEqual(textField)
				{
					loginCount = updatedText.count
					deviceCount = deviceView.text?.count ?? 0
				} else
				{
					loginCount = loginView.text?.count ?? 0
					deviceCount = updatedText.count
				}
						
				if loginCount > 0 && deviceCount > 4 { nextButtonView.isEnabled = true }
				else { nextButtonView.isEnabled = false }
		}
		
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
	}
	
	public func startLoading ()
	{
		nextButtonView.startLoading()
	}
	
	public func stopLoading ()
	{
		nextButtonView.endLoading()
	}
}

