//
//  XRecoveryPasswordView.swift
//  SecurityHub
//
//  Created by Stefan on 11.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryPasswordView: UIView {
		public var delegate: XRegistrationCompletionDelegate?
		private var titleView = UILabel()
		private var scrollView = UIScrollView()
		private var contentView: UIView = UIView()
		private var passwordView, passwordConfirmView: XPasswordView!
		private var validPasswordView: UILabel = UILabel()
		private var validConfirmPasswordView: UILabel = UILabel()
		private var completeButtonView: XZoomButton!
		private var strings = XRecoveryPasswordViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
		override func layoutSubviews() {
				super.layoutSubviews()

				scrollView.contentSize = contentView.frame.size
		}
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
			
				titleView.backgroundColor = UIColor.clear
				titleView.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
				titleView.font = UIFont(name: "Open Sans", size: 13)
				titleView.textAlignment = .left
				titleView.lineBreakMode = .byWordWrapping
				titleView.numberOfLines = 0
				titleView.text = strings.title
				addSubview(titleView)
			
				addSubview(scrollView)
			
				scrollView.addSubview(contentView)
			
				passwordView = XPasswordView (style: XRegistrationStyles.textFieldNormalStyle())
				if #available(iOS 12.0, *) {
					passwordView.textContentType = .oneTimeCode
				} else {
					passwordView.textContentType = UITextContentType(rawValue: "")
				}
				passwordView.keyboardType = .default
				passwordView.returnKeyType = .next
				passwordView.autocorrectionType = .no
				passwordView.autocapitalizationType = .none
				passwordView.spellCheckingType = .no
				passwordView.xDelegate = self
				passwordView.placeholder = strings.password
				contentView.addSubview(passwordView)
			
				validPasswordView.font = UIFont(name: "Open Sans", size: 12.5)
				validPasswordView.backgroundColor = UIColor.clear
				validPasswordView.textColor = UIColor.red
				validPasswordView.numberOfLines = 0
				validPasswordView.lineBreakMode = .byWordWrapping
				validPasswordView.textAlignment = .center
				contentView.addSubview(validPasswordView)
			
				passwordConfirmView = XPasswordView (style: XRegistrationStyles.textFieldNormalStyle())
				if #available(iOS 12.0, *) {
					passwordConfirmView.textContentType = .oneTimeCode
				} else {
					passwordConfirmView.textContentType = UITextContentType(rawValue: "")
				}
				passwordConfirmView.keyboardType = .default
				passwordConfirmView.returnKeyType = .done
				passwordConfirmView.autocorrectionType = .no
				passwordConfirmView.autocapitalizationType = .none
				passwordConfirmView.spellCheckingType = .no
				passwordConfirmView.xDelegate = self
				passwordConfirmView.placeholder = strings.passwordConfirm
				contentView.addSubview(passwordConfirmView)
			
				validConfirmPasswordView.font = UIFont(name: "Open Sans", size: 12.5)
				validConfirmPasswordView.backgroundColor = UIColor.clear
				validConfirmPasswordView.textColor = UIColor.red
				validConfirmPasswordView.numberOfLines = 0
				validConfirmPasswordView.lineBreakMode = .byWordWrapping
				validConfirmPasswordView.textAlignment = .center
				contentView.addSubview(validConfirmPasswordView)
								
				completeButtonView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				completeButtonView.text = strings.complete
				completeButtonView.loadingText = " "
				completeButtonView.addTarget(self, action: #selector(registrationCompleteViewTapped), for: .touchUpInside)
				completeButtonView.style.disableBackground = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 1)
				completeButtonView.style.disable = UIColor.white
				completeButtonView.isEnabled = false
				addSubview(completeButtonView)
		}
		
		private func setConstraints() {
				titleView.translatesAutoresizingMaskIntoConstraints = false
				scrollView.translatesAutoresizingMaskIntoConstraints = false
				contentView.translatesAutoresizingMaskIntoConstraints = false
				passwordView.translatesAutoresizingMaskIntoConstraints = false
				passwordConfirmView.translatesAutoresizingMaskIntoConstraints = false
				completeButtonView.translatesAutoresizingMaskIntoConstraints = false
				validPasswordView.translatesAutoresizingMaskIntoConstraints = false
				validConfirmPasswordView.translatesAutoresizingMaskIntoConstraints = false

				NSLayoutConstraint.activate([
					titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 23),
					titleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
					titleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
					
					scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 23),
					scrollView.bottomAnchor.constraint(equalTo: completeButtonView.topAnchor, constant: -23),
					scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
					scrollView.widthAnchor.constraint(equalTo: widthAnchor),
					
					contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
					contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
					contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
					contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 20),
					contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40),
					contentView.heightAnchor.constraint(equalToConstant: 152),
					
					passwordView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
					passwordView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
					passwordView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
					passwordView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
					passwordView.heightAnchor.constraint(equalToConstant: 52),
					
					validPasswordView.topAnchor.constraint(equalTo: passwordView.bottomAnchor, constant: 4),
					validPasswordView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 14),
					validPasswordView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -14),
					
					passwordConfirmView.topAnchor.constraint(equalTo: validPasswordView.bottomAnchor, constant: 20),
					passwordConfirmView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
					passwordConfirmView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
					passwordConfirmView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
					passwordConfirmView.heightAnchor.constraint(equalToConstant: 52),
					
					validConfirmPasswordView.topAnchor.constraint(equalTo: passwordConfirmView.bottomAnchor, constant: 4),
					validConfirmPasswordView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 14),
					validConfirmPasswordView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -14),
					
					completeButtonView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -37),
					completeButtonView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 87),
					completeButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -174),
					completeButtonView.heightAnchor.constraint(equalToConstant: 40),
				])
		}

		private func trimmingString (string: String) -> String
		{
			return string.trimmingCharacters (in: .whitespacesAndNewlines)
		}
	
		// least one uppercase,
		// least one digit
		// least one lowercase
		// least one symbol
		// min 6 characters total
		private func validatePassword (password: String) -> Bool
		{
			let passwordRegx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{6,}$"
			let passwordCheck = NSPredicate (format: "SELF MATCHES %@",passwordRegx)
			
			return passwordCheck.evaluate (with: password)
		}
	
		private func validateData ()
		{
			var password1Result = false
			var password2Result = false
			
			if passwordView.text?.count ?? 0 > 0 {
				let password = trimmingString (string: passwordView.text!)
				password1Result = validatePassword (password: password)
				if !password1Result {
					passwordView.showValidationError(message: "")
					validPasswordView.text = strings.invalidPassword }
			}
			
			if passwordConfirmView.text?.count ?? 0 > 0 {
				let password = trimmingString (string: passwordConfirmView.text!)
				if password1Result && password.count > 0 {
					let password1 = trimmingString (string: passwordView.text!)
					if password1 != password
					{
						password2Result = false
						passwordConfirmView.showValidationError(message: "")
						validConfirmPasswordView.text = strings.mismatchPassword
					} else { password2Result = true }
				}
			}
			
			if password1Result && password2Result {
				if validPasswordView.text?.count != 0 {
					_ = passwordView.textFieldShouldBeginEditing(passwordView)
					_ = passwordView.textFieldShouldEndEditing(passwordView)
				}
				if validConfirmPasswordView.text?.count != 0 {
					_ = passwordConfirmView.textFieldShouldBeginEditing(passwordConfirmView)
					_ = passwordConfirmView.textFieldShouldEndEditing(passwordConfirmView)
				}
				completeButtonView.isEnabled = true }
			else { completeButtonView.isEnabled = false }
		}
	
		public func startLoading ()
		{
			completeButtonView.startLoading()
		}
		
		public func stopLoading ()
		{
			completeButtonView.endLoading()
		}
		
		@objc private func registrationCompleteViewTapped() {
			var userData = RegUserData()
			userData.passwordHash = trimmingString(string: passwordView.text!).md5
			delegate?.registrationCompleteViewTapped(userData: userData)
		}
		
		@objc private func viewTapped() {
				//scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
			
				if passwordView.isFirstResponder { passwordView.resignFirstResponder()} else
				if passwordConfirmView.isFirstResponder { passwordConfirmView.resignFirstResponder()}
				validateData()
		}
}

extension XRecoveryPasswordView: XTextFieldDelegete {
	
	func xTextFieldShouldReturn(_ textField: UITextField) {

		if passwordView.isFirstResponder {
			passwordView.resignFirstResponder()
			if passwordView.text?.count ?? 0 > 0 {
				let password = trimmingString (string: passwordView.text!)
				if validatePassword (password: password) == true
					{ passwordConfirmView.becomeFirstResponder() }
			} else { passwordConfirmView.becomeFirstResponder() }
			validateData()
		} else
		if passwordConfirmView.isFirstResponder {
			//scrollView.setContentOffset(CGPoint (x: 0, y: 0), animated: false)
			passwordConfirmView.resignFirstResponder()
			validateData()
		}
	}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
		if textField.isEqual(passwordView) || textField.isEqual(passwordConfirmView)
		{
			//let point = CGPoint (x: 0, y: passwordView.frame.origin.y - 28 )
//			scrollView.setContentOffset(point, animated: true)
			if textField.isEqual(passwordView) { validPasswordView.text = "" }
			else { validConfirmPasswordView.text = "" }
		}
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
		if textField.isEqual(passwordConfirmView)
		{
			scrollView.scrollRectToVisible(passwordConfirmView.frame, animated: true)
		}
	}
}

