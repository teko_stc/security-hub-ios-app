//
//  XRecoveryDeviceDelegate.swift
//  SecurityHub
//
//  Created by Stefan on 11.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

protocol XRecoveryDeviceDelegate {
		func nextViewTapped(user: RegUserData)
		func regSerialNumberViewTapped()
}
