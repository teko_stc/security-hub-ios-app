//
//  XRecoveryPasswordController.swift
//  SecurityHub
//
//  Created by Stefan on 11.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryPasswordController: XBaseViewController<XRecoveryPasswordView>, XRegistrationCompletionDelegate {
		var userData = RegUserData()
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
		
		override func viewDidLoad() {
				super.viewDidLoad()
				title = strings.enterNewPassword
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func registrationCompletionSuccess(userData: RegUserData) {
			let rsc = XRecoverySuccessController()
			rsc.login = userData.login
			rsc.password_md5 = userData.passwordHash
			let navc = navigationController
			navc?.popViewController(animated: false)
			navc?.popViewController(animated: false)
			navc?.pushViewController(rsc, animated: true)
		}
		
		private func registrationCompletionError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { super.back() } },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		override func back()
		{
			let alert = XAlertController(
				style: XRegistrationStyles.errorAlertStyle(),
				strings: strings.confirmAlert(message: "REC_USER_CANCEL"),
				positive: { super.back() },
				negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func registrationCompleteViewTapped(userData: RegUserData)
		{
			self.userData.passwordHash = userData.passwordHash
			
			mainView.startLoading()
			registrationManager.recoveryUserDevice (user: self.userData) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS )
				{
					if result.result == RegResultCode.ERR_DEVICE_DOESNT_EXIST ||
							result.result == RegResultCode.ERR_DEVICE_NOT_TAKEN ||
							result.result == RegResultCode.ERR_NOT_BINDED_TO_SITE ||
							result.result == RegResultCode.ERR_WRONG_PIN ||
							result.result == RegResultCode.ERR_TOKEN_EXPIRED ||
							result.result == RegResultCode.ERR_TOKEN_NOT_FOUND {
						self.registrationCompletionError (message: self.strings.wrongLogin,withBack: true)
					} else {
						self.registrationCompletionError (message: result.localizedMessage,withBack: false)
					}
				}
				else
				{
					self.registrationCompletionSuccess(userData: self.userData)
				}
			}
		}
}

