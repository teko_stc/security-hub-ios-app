//
//  XRecoveryDeviceController.swift
//  SecurityHub
//
//  Created by Stefan on 10.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit
import BarcodeScanner

class XRecoveryDeviceController: XBaseViewController<XRecoveryDeviceView>, XRecoveryDeviceDelegate, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
	
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
	
		override func viewDidLoad() {
				super.viewDidLoad()

				title = strings.serialNumber
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func registrationError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { self.back()} },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func nextViewTapped(user: RegUserData)
		{
			mainView.startLoading()
			var userc = user
			let snWithPin = String(user.sn)
			userc.pinHash = String(snWithPin.suffix(4)).md5
			let index = snWithPin.index(snWithPin.endIndex, offsetBy: -4)
			userc.sn = UInt(String(snWithPin.prefix(upTo: index)))!
			registrationManager.requestRecoveryTokenDevice(user: userc) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS ) {
					self.registrationError (message: result.localizedMessage, withBack: false) }
				else
				{
					let pcc = XRecoveryConfirmRightsController()
					userc.token = result.token
					pcc.userData = userc
					let navc = self.navigationController
					navc?.pushViewController(pcc, animated: true)
				}
			}
		}
		
		func regSerialNumberViewTapped() {
			let scannerViewController = BarcodeScannerViewController()
			scannerViewController.codeDelegate = self
			scannerViewController.errorDelegate = self
			scannerViewController.dismissalDelegate = self
			present(scannerViewController, animated: true)
		}
	
		func scanner(_ controller: BarcodeScanner.BarcodeScannerViewController, didCaptureCode code: String, type: String)
		{
			let serialNumber = String(code[code.index(after: code.startIndex)..<code.index(before: code.endIndex)])
			mainView.setDefault(device: serialNumber)
			DispatchQueue.main.async { self.dismiss(animated: true) }
		}
	
		func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error)
		{
			DispatchQueue.main.async {
				self.dismiss(animated: true)
				self.registrationError (message: error.localizedDescription, withBack: false) }
		}

		func scannerDidDismiss(_ controller: BarcodeScannerViewController)
		{
			dismiss(animated: true)
		}
}

