//
//  XRecoveryConfirmRightsController.swift
//  SecurityHub
//
//  Created by Stefan on 10.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit
import BarcodeScanner

class XRecoveryConfirmRightsController: XBaseViewController<XRecoveryConfirmRightsView>, XRegistrationCompletionDelegate {
	
		var userData = RegUserData()
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
	
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
	
		override func viewDidLoad() {
				super.viewDidLoad()

				title = strings.confirmRights
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func registrationError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { self.back()} },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func registrationCompleteViewTapped(userData: RegUserData) {
			mainView.startLoading()
			self.userData.name = userData.name
			self.userData.code = userData.code
			registrationManager.validateRecoveryTokenDevice(user: self.userData) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS ) {
					self.registrationError (message: result.localizedMessage, withBack: false)
				}
				else
				{
					let nav = self.navigationController
					let pcc = XRecoveryPasswordController()
					pcc.userData = self.userData
					pcc.userData.token = result.token
					nav?.popViewController(animated: false)
					nav?.popViewController(animated: false)
					nav?.pushViewController(pcc, animated: true)
				}
			}
		}
}

