//
//  XRecoverySuccessView.swift
//  SecurityHub
//
//  Created by Stefan on 08.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoverySuccessView: UIView {
		public var delegate: XRegistrationSuccessDelegate?
		private var titleView = UILabel()
		private var signInView: XZoomButton!
		private var backView: XZoomButton!
		private var strings = XRecoverySuccessViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
			
				titleView.backgroundColor = UIColor.clear
				titleView.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
				titleView.font = UIFont(name: "Open Sans", size: 13)
				titleView.textAlignment = .left
				titleView.lineBreakMode = .byWordWrapping
				titleView.numberOfLines = 0
				titleView.text = strings.successTitle
				addSubview(titleView)
								
				signInView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				signInView.text = strings.signIn
				signInView.loadingText = " "
				signInView.addTarget(self, action: #selector(signInViewTapped), for: .touchUpInside)
				addSubview(signInView)
			
				backView = XZoomButton (style: XRegistrationStyles.regularGreyButtonStyle())
				backView.text = strings.back
				backView.addTarget(self, action: #selector(backViewTapped), for: .touchUpInside)
				backView.titleLabel?.numberOfLines = 0
				backView.titleLabel?.lineBreakMode = .byWordWrapping
				backView.titleLabel?.textAlignment = .center
				addSubview(backView)
		}
		
		private func setConstraints() {
				titleView.translatesAutoresizingMaskIntoConstraints = false
				signInView.translatesAutoresizingMaskIntoConstraints = false
				backView.translatesAutoresizingMaskIntoConstraints = false

				NSLayoutConstraint.activate([
					titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 25),
					titleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 30),
					titleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -30),
					
					signInView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 82),
					signInView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 60),
					signInView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -120),
					signInView.heightAnchor.constraint(equalToConstant: 40),
					
					backView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -34),
					backView.centerXAnchor.constraint(equalTo: centerXAnchor),
					backView.widthAnchor.constraint(equalToConstant:184),
					backView.heightAnchor.constraint(equalToConstant: 50),
				])
		}
	
		public func startLoading ()
		{
			signInView.startLoading()
			backView.isEnabled = false
		}
		
		public func stopLoading ()
		{
			signInView.endLoading()
			backView.isEnabled = true
		}
		
		@objc private func signInViewTapped() {
			delegate?.registrationSignInViewTapped()
		}
		
		@objc private func backViewTapped() {
			delegate?.registrationBackTapped()
		}
}

