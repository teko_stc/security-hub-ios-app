//
//  XRecoveryPhoneView.swift
//  SecurityHub
//
//  Created by Stefan on 08.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryPhoneView: UIView {
		public var delegate: XRegistrationPhoneDelegate?
		private var phoneView: XTextField!
		private var nextButtonView, regDeviceSerialView, regGoogleView, regVKView : XZoomButton!
		private var deviceSerialBottomAnchor: NSLayoutConstraint?
		private var strings = XRecoveryPhoneViewStrings()
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		public func setDefault(phone: String) {
				phoneView.text = phone
		}
	
		public func hideSocial() {
				regGoogleView.isHidden = true
				regVKView.isHidden = true
			
				removeConstraint(deviceSerialBottomAnchor!)
				addConstraint(regDeviceSerialView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -34))
		}
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))

				phoneView = XTextField (style: XRegistrationStyles.textFieldNormalStyle())
				phoneView.textContentType = .telephoneNumber
				phoneView.keyboardType = .numberPad
				phoneView.returnKeyType = .done
				phoneView.autocorrectionType = .no
				phoneView.autocapitalizationType = .none
				phoneView.spellCheckingType = .no
				phoneView.xDelegate = self
				phoneView.placeholder = strings.phonePlaceholder
				addSubview(phoneView)
								
				nextButtonView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				nextButtonView.text = strings.next
				nextButtonView.loadingText = strings.next
				nextButtonView.addTarget(self, action: #selector(nextViewTapped), for: .touchUpInside)
				nextButtonView.isEnabled = false
				nextButtonView.style.disableBackground = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 1)
				nextButtonView.style.disable = UIColor.white
				addSubview(nextButtonView)
			
				let highlightedColor = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 0.68)
			
				regDeviceSerialView = XZoomButton (style: XRegistrationStyles.regularButtonStyle())
				regDeviceSerialView.text = strings.regSerialNumber
				regDeviceSerialView.style.disableBackground = highlightedColor
				regDeviceSerialView.backgroundHighligh = true
				regDeviceSerialView.titleLabel?.numberOfLines = 0
				regDeviceSerialView.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
				regDeviceSerialView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 0)
				regDeviceSerialView.titleLabel?.lineBreakMode = .byWordWrapping
				regDeviceSerialView.addTarget(self, action: #selector(regSerialNumberViewTapped), for: .touchUpInside)
				regDeviceSerialView.setImage (UIImage(named:"ic_reg_device_serial"), for: .normal)
				regDeviceSerialView.setImage (UIImage(named:"ic_reg_device_serial"), for: .highlighted)
				addSubview(regDeviceSerialView)
			
				regGoogleView = XZoomButton (style: XRegistrationStyles.regularButtonStyle())
				regGoogleView.text = strings.regGoogle
				regGoogleView.style.disableBackground = highlightedColor
				regGoogleView.backgroundHighligh = true
				regGoogleView.titleLabel?.numberOfLines = 0
				regGoogleView.titleLabel?.lineBreakMode = .byWordWrapping
				regGoogleView.contentEdgeInsets = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 20)
				regGoogleView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
				regGoogleView.addTarget(self, action: #selector(regGoogleViewTapped), for: .touchUpInside)
				regGoogleView.setImage (UIImage(named:"ic_reg_google"), for: .normal)
				regGoogleView.setImage (UIImage(named:"ic_reg_google"), for: .highlighted)
				addSubview(regGoogleView)
			
				regVKView = XZoomButton (style: XRegistrationStyles.regularButtonStyle())
				regVKView.text = strings.regVK
				regVKView.style.disableBackground = highlightedColor
				regVKView.backgroundHighligh = true
				regVKView.titleLabel?.numberOfLines = 0
				regVKView.titleLabel?.lineBreakMode = .byWordWrapping
				regVKView.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
				regVKView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 0)
				regVKView.addTarget(self, action: #selector(regVKViewTapped), for: .touchUpInside)
				regVKView.setImage (UIImage(named:"ic_reg_vk"), for: .normal)
				regVKView.setImage (UIImage(named:"ic_reg_vk"), for: .highlighted)
				addSubview(regVKView)
		}
		
		private func setConstraints() {
				phoneView.translatesAutoresizingMaskIntoConstraints = false
				nextButtonView.translatesAutoresizingMaskIntoConstraints = false
				regDeviceSerialView.translatesAutoresizingMaskIntoConstraints = false
				regGoogleView.translatesAutoresizingMaskIntoConstraints = false
				regVKView.translatesAutoresizingMaskIntoConstraints = false
				deviceSerialBottomAnchor = regDeviceSerialView.bottomAnchor.constraint(equalTo: regGoogleView.topAnchor, constant: 4)
				NSLayoutConstraint.activate([
					phoneView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 30),
					phoneView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
					phoneView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -40),
					phoneView.heightAnchor.constraint(equalToConstant: 52),
					
					nextButtonView.topAnchor.constraint(equalTo: phoneView.bottomAnchor, constant: 22),
					nextButtonView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 87),
					nextButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -174),
					nextButtonView.heightAnchor.constraint(equalToConstant: 40),
					
					regVKView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -34),
					regVKView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
					regVKView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
					regVKView.heightAnchor.constraint(equalToConstant: 64),

					regGoogleView.bottomAnchor.constraint(equalTo: regVKView.topAnchor, constant: 4),
					regGoogleView.leadingAnchor.constraint(equalTo: regVKView.leadingAnchor),
					regGoogleView.widthAnchor.constraint(equalTo: regVKView.widthAnchor),
					regGoogleView.heightAnchor.constraint(equalTo: regVKView.heightAnchor),

					deviceSerialBottomAnchor!,
					regDeviceSerialView.leadingAnchor.constraint(equalTo: regVKView.leadingAnchor),
					regDeviceSerialView.widthAnchor.constraint(equalTo: regVKView.widthAnchor),
					regDeviceSerialView.heightAnchor.constraint(equalTo: regVKView.heightAnchor),
				])
		}
		
		@objc private func nextViewTapped() {
			viewTapped()
			if let phone = UInt(phoneView.text!)
			{
				delegate?.nextViewTapped(phone: phone)
			}
		}
	
		@objc private func regSerialNumberViewTapped() { delegate?.regSerialNumberViewTapped() }
		@objc private func regGoogleViewTapped() { delegate?.regGoogleViewTapped() }
		@objc private func regVKViewTapped() { delegate?.regVKViewTapped() }
		
		@objc private func viewTapped() {
				if phoneView.isFirstResponder { phoneView.resignFirstResponder()}
		}
}

extension XRecoveryPhoneView: XTextFieldDelegete {
		func xTextFieldShouldReturn(_ textField: UITextField) {
			phoneView.resignFirstResponder()
		}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		if string.count > 0
		{
			let num = UInt(string)
			if num == nil { return false }
			if string[0] == "+" { return false }
		}
		else
		{
			if textField.text?.count == 1 { return false }
		}
		
		if let text = textField.text,
				let textRange = Range(range, in: text) {
				let updatedText = text.replacingCharacters(in: textRange, with: string)
				if updatedText.count > 10 { nextButtonView.isEnabled = true }
				else { nextButtonView.isEnabled = false }
		}
		
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
		if textField.text?.count == 0 { textField.text = "+" }
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
		if textField.text?.count == 1 { textField.text = "" }
	}
	
	public func startLoading ()
	{
		nextButtonView.startLoading()
	}
	
	public func stopLoading ()
	{
		nextButtonView.endLoading()
	}
}

