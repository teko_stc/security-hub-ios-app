//
//  XRecoveryCompletionController.swift
//  SecurityHub
//
//  Created by Stefan on 08.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryCompletionController: XBaseViewController<XRecoveryCompletionView>, XRegistrationCompletionDelegate {
		public var numberPhone: UInt = 0
		public var pinCode: UInt = 0
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
		
		override func viewDidLoad() {
				super.viewDidLoad()
				title = strings.titleNewPassword
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func registrationCompletionSuccess(userData: RegUserData) {
			let rsc = XRecoverySuccessController()
			rsc.login = userData.login
			rsc.password_md5 = userData.passwordHash
			let navc = navigationController
			navc?.popViewController(animated: false)
			navc?.popViewController(animated: false)
			navc?.pushViewController(rsc, animated: true)
		}
		
		private func registrationCompletionError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { super.back() } },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		override func back()
		{
			let alert = XAlertController(
				style: XRegistrationStyles.errorAlertStyle(),
				strings: strings.confirmAlert(message: "REC_USER_CANCEL"),
				positive: { super.back() },
				negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func registrationCompleteViewTapped(userData: RegUserData)
		{
			var user = userData
			user.phone = numberPhone
			user.code = pinCode
			
			mainView.startLoading()
			registrationManager.recoveryUserPhone(user: user) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS )
				{
					if result.result == RegResultCode.ERR_WRONG_LOGIN {
						self.registrationCompletionError (message: self.strings.wrongLogin,withBack: false)
					} else {
						var withBack = false
						if ( result.result == RegResultCode.ERR_CODE_DOESNT_EXIST ||
								 result.result == RegResultCode.ERR_WRONG_CODE ||
								 result.result == RegResultCode.ERR_USER_PHONE_DOESNT_EXIST ||
								 result.result == RegResultCode.ERR_CODE_EXPIRED ) { withBack = true }
						self.registrationCompletionError (message: self.strings.wrongLogin,withBack: withBack)
					}
				}
				else
				{
					self.registrationCompletionSuccess(userData: user)
				}
			}
		}
}
