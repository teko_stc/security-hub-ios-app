//
//  XRecoveryPinCodeController.swift
//  SecurityHub
//
//  Created by Stefan on 08.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRecoveryPinCodeController: XBaseViewController<XRecoveryPinCodeView>, XRegistrationPinCodeDelegate {
		public var numberPhone: UInt = 0
		public var timeoutCode: Int = 0
		let registrationManager = RegistrationManager()
		let strings = XRegistrationControllerStrings()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
		
		override func viewDidLoad() {
				super.viewDidLoad()
				title = strings.titlePinCode
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
				mainView.startRequestTimer(timerValue: timeoutCode)
		}
	
		override func back()
		{
			mainView.stopRequestTimer()
			super.back()
		}
	
		private func pinCodeSuccess(pinCode: String) {
			let rcc = XRecoveryCompletionController()
			rcc.numberPhone = self.numberPhone
			rcc.pinCode = UInt(pinCode)!
			let navc = navigationController
			navc?.popViewController(animated: false)
			navc?.pushViewController(rcc, animated: true)
		}
		
		private func pinCodeError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { self.back() }},
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func nextViewTapped(pinCode: String)
		{
			self.mainView.startLoading()
			
			registrationManager.validateRecoveryPhonePinCode (phone: numberPhone, pinCode: UInt(pinCode)!) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS )
				{
					self.pinCodeError (message: result.localizedMessage,withBack: false)
				}
				else
				{
					self.mainView.stopRequestTimer()
					self.pinCodeSuccess(pinCode: pinCode)
				}
			}
		}
	
		func requestAgainViewTapped()
		{
			mainView.startLoading()
			registrationManager.requestRecoveryPhonePinCode(phone: numberPhone) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS ) { self.pinCodeError (message: result.localizedMessage,withBack: false) }
				else
				{
				}
			}
		}
}
