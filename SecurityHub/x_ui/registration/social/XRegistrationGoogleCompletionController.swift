//
//  XRegistrationGoogleCompletionController.swift
//  SecurityHub
//
//  Created by Stefan on 12.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRegistrationGoogleCompletionController: XBaseViewController<XRegistrationCompletionView>, XRegistrationCompletionDelegate {
		public var token: String = ""
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
		
		override func viewDidLoad() {
				super.viewDidLoad()
				title = strings.titleCompletion
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
				mainView.setGoogleTitle()
		}
		
		private func registrationCompletionSuccess(userData: RegUserData) {
			let rsc = XRegistrationSuccessController()
			rsc.login = userData.login
			rsc.password_md5 = userData.passwordHash
			let navc = navigationController
			navc?.popViewController(animated: false)
			navc?.popViewController(animated: false)
			navc?.pushViewController(rsc, animated: true)
		}
		
		private func registrationCompletionError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { super.back() } },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		override func back()
		{
			let alert = XAlertController(
				style: XRegistrationStyles.errorAlertStyle(),
				strings: strings.confirmAlert(message: "REG_USER_CANCEL"),
				positive: { super.back() },
				negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func registrationCompleteViewTapped(userData: RegUserData)
		{
			var user = userData
			user.token = token
			
			mainView.startLoading()
			registrationManager.registrationUserGoogle(user: user) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS )
				{
					if result.result == RegResultCode.ERR_CODE_EXPIRED ||
							result.result == RegResultCode.ERR_SOCIAL_NOT_FOUND ||
							result.result == RegResultCode.ERR_BAD_CREDENTIALS
					{
						self.registrationCompletionError (message: self.strings.sessionExpired,withBack: true)
					} else {
						self.registrationCompletionError (message: result.localizedMessage,withBack: false)
					}
				}
				else
				{
					self.registrationCompletionSuccess(userData: user)
				}
			}
		}
}

