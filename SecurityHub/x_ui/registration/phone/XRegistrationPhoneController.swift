//
//  XRegistrationPhoneController.swift
//  SecurityHub
//
//  Created by Stefan on 01.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit
import GoogleSignIn

class XRegistrationPhoneController: XBaseViewController<XRegistrationPhoneView>, XRegistrationPhoneDelegate {
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
	
		override func viewDidLoad() {
				super.viewDidLoad()

				title = strings.title
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
				if !(XTargetUtils.target == .test || XTargetUtils.target == .security_hub) ||
						XTargetUtils.regLink != XTargetUtils.regServer
				{
					mainView.hideSocial()
				}
		}
		
		private func registrationError(message: String?) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: {},
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		private func googleSignSuccess (token: String?)
		{
			if let token = token
			{
				let pcc = XRegistrationGoogleCompletionController()
				pcc.token = token
				self.navigationController?.pushViewController(pcc, animated: true)
			}
			else
			{
				self.registrationError(message: "Error get token")
			}
			
			GIDSignIn.sharedInstance.signOut()
		}
	
		func nextViewTapped(phone: UInt)
		{
			mainView.startLoading()
			registrationManager.requestPhonePinCode(phone: phone) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS ) {
					if result.result == .ERR_CODE_EXISTS {
						self.showPinCodeController(phone: phone, timeout: result.timeout) }
					else {
						self.registrationError (message: result.localizedMessage)
					}
				}
				else
				{
					self.showPinCodeController(phone: phone, timeout: 0)
				}
			}
		}
	
		func showPinCodeController(phone: UInt, timeout: Int) {
			let pcc = XRegistrationPinCodeController()
			pcc.numberPhone = phone
			pcc.timeoutCode = timeout
			self.navigationController?.pushViewController(pcc, animated: true)
		}
		
		func regSerialNumberViewTapped() {
			navigationController?.pushViewController(XRegistrationDeviceController(), animated: true)
		}
		
		func regGoogleViewTapped() {
			
			GIDSignIn.sharedInstance.signIn (withPresenting: self) { signInResult, error in
					guard error == nil else {
						if let error = error as? NSError {
							if error.code != -5 { self.registrationError(message: error.localizedDescription) } }
						return
					}
				
					DispatchQueue.main.async {
						self.googleSignSuccess ( token: signInResult?.user.idToken?.tokenString )
					}
				}
		}
		
		func regVKViewTapped() {
			registrationManager.requestAccessTokenVK() { result in
				if result.result != .SUCCESS {
					if result.result != .CANCEL { self.registrationError (message: result.localizedMessage) } }
				else
				{
					let pcc = XRegistrationVKCompletionController()
					pcc.token = result.token
					self.navigationController?.pushViewController(pcc, animated: true)
				}
			}
		}
}

