//
//  XRegistrationResources.swift
//  SecurityHub
//
//  Created by Stefan on 01.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

struct XRegistrationStyles
{
	static func backgroundColor () -> UIColor
	{
		return UIColor.white
	}
	
	static func textFieldNormalStyle () -> XTextFieldStyle
	{
		return XTextFieldStyle (
			backgroundColor: UIColor.white,
		 	unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
			selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			font: UIFont(name: "Open Sans", size: 15.5))
	}
	
	static func textFieldPinCodeStyle () -> XTextFieldStyle
	{
		return XTextFieldStyle (
			backgroundColor: UIColor.white,
			unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
			selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			font: UIFont(name: "Open Sans", size: 20.0))
	}
	
	static func roundedButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			font: UIFont(name: "Open Sans", size: 20),
			color: UIColor.white
		)
	}
	
	static func regularButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 16),
			color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			alignment: .left
		)
	}
	
	static func regularGreyButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 16),
			color: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1),
			alignment: .center
		)
	}
	
	static func highlightButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 13),
			color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			alignment: .center
		)
	}
	
	static func checkBoxStyle () -> XCheckBoxViewStyle
	{
		return XCheckBoxViewStyle (
			icon: XCheckBoxView.Style.Icon(
					select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
					unselect: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1)
			), text: XCheckBoxView.Style.Text(
					select: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1),
					unselect: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1),
					font: UIFont(name: "Open Sans", size: 11.5)
			)
		)
	}
	
	static func errorAlertStyle () -> XAlertViewStyle
	{
		return XAlertViewStyle (
			backgroundColor: UIColor.white,
			title: XAlertView.Style.Lable(
					font: UIFont(name: "Open Sans", size: 20),
					color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
			),
			text: nil,
			buttonOrientation: .vertical,
			positive: XZoomButton.Style (
					backgroundColor: UIColor.clear,
					font: UIFont(name: "Open Sans", size: 15.5),
					color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
			),
			negative: nil
		)
	}
}

struct XRegistrationPhoneViewStrings {
		/// Номер телефона
		var phonePlaceholder: String { "REG_USER_ENTER_NUMBER_PHONE".localized() }
		
		/// Регистрация по серийному номеру
		var regSerialNumber: String { "REG_USER_SERIAL_NUMBER".localized() }
		
		/// Регистрация по учетной записи Google
		var regGoogle: String { "REG_USER_GOOGLE".localized() }
		
		/// Регистрация по учетной записи VK
		var regVK: String { "REG_USER_VK".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRecoveryPhoneViewStrings {
		/// Номер телефона
		var phonePlaceholder: String { "REG_USER_ENTER_NUMBER_PHONE".localized() }
		
		/// Регистрация по серийному номеру
		var regSerialNumber: String { "REC_USER_SERIAL_NUMBER".localized() }
		
		/// Регистрация по учетной записи Google
		var regGoogle: String { "REC_USER_GOOGLE".localized() }
		
		/// Регистрация по учетной записи VK
		var regVK: String { "REC_USER_VK".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRegistrationDeviceViewStrings {
		/// Серийный номер
		var serialNumber: String { "REG_USER_NUMBER_PIN".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRecoveryDeviceViewStrings {
		/// Серийный номер
		var serialNumber: String { "REG_USER_NUMBER_PIN".localized() }
		var login: String { "REG_USER_LOGIN".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRegistrationPinCodeViewStrings {
		/// Запросить повторно
		var requestAgain: String { "REG_USER_REQUEST_AGAIN".localized() }
		var requestTitle: String { "REG_USER_REQUEST_TITLE".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRecoveryPinCodeViewStrings {
		/// Запросить повторно
		var requestAgain: String { "REG_USER_REQUEST_AGAIN".localized() }
		var requestTitle: String { "REG_USER_REQUEST_TITLE".localized() }
		var title: String { "REC_USER_PHONE_CONFIRM_TITLE".localized() }
		
		/// Далее
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRegistrationCompletionViewStrings {
	
		var completionTitle: String { "REG_USER_COMPLETION_TITLE".localized() }
		var completionDeviceTitle: String { "REG_USER_DEVICE_COMPLETE".localized() }
		var completionGoogleTitle: String { "REG_USER_GOOGLE_COMPLETE".localized() }
		var completionVkTitle: String { "REG_USER_VK_COMPLETE".localized() }
		var userName: String { "REG_USER_NAME".localized() }
		var login: String { "REG_USER_LOGIN".localized() }
		var password: String { "REG_USER_PASSWORD".localized() }
		var passwordConfirm: String { "REG_USER_PASSWORD_CONFIRM".localized() }
		var complete: String { "REG_USER_COMPLETE".localized() }
		var mismatchPassword: String { "REG_USER_PASSWORD_MISMATCH".localized() }
		var invalidPassword: String { "REG_ERR_INVALID_PASSWORD".localized() }
}

struct XRecoveryCompletionViewStrings {
	
		var completionTitle: String { "REC_USER_NEW_PASSWORD_TITLE".localized() }
		var recoveryGoogleTitle: String { "REC_USER_GOOGLE_COMPLETE".localized() }
		var recoveryVkTitle: String { "REC_USER_VK_COMPLETE".localized() }
		var login: String { "REG_USER_LOGIN".localized() }
		var password: String { "REG_USER_PASSWORD".localized() }
		var passwordConfirm: String { "REG_USER_PASSWORD_CONFIRM".localized() }
		var complete: String { "REC_USER_SAVE".localized() }
		var mismatchPassword: String { "REG_USER_PASSWORD_MISMATCH".localized() }
		var invalidPassword: String { "REG_ERR_INVALID_PASSWORD".localized() }
}

struct XRecoveryConfirmRightsViewStrings {
	
		var title: String { "REC_USER_CONFIRM_RIGHTS_TITLE".localized() }
		var objectName: String { "REC_USER_OBJECT_NAME".localized() }
		var objectNameAny: String { "REC_USER_OBJECT_NAME_ANY".localized() }
		var deviceNotArmed: String { "REC_USER_DEVICE_NOT_ARMED".localized() }
		var dateTime: String { "REC_USER_DATE_TIME".localized() }
		var approxTime: String { "REC_USER_APPROX_TIME".localized() }
		var invalidPassword: String { "REG_ERR_INVALID_PASSWORD".localized() }
		var next: String { "N_BUTTON_NEXT".localized() }
}

struct XRecoveryPasswordViewStrings {
	
		var title: String { "REC_USER_ENTER_NEW_PASSWORD_TITLE".localized() }
		var password: String { "REG_USER_PASSWORD".localized() }
		var passwordConfirm: String { "REG_USER_PASSWORD_CONFIRM".localized() }
		var complete: String { "REC_USER_SAVE".localized() }
		var mismatchPassword: String { "REG_USER_PASSWORD_MISMATCH".localized() }
		var invalidPassword: String { "REG_ERR_INVALID_PASSWORD".localized() }
}

struct XRegistrationSuccessViewStrings {
	
		var successTitle: String { "REG_USER_SUCCESS_TITLE".localized() }
		var signIn: String { "REG_USER_SIGN_IN".localized() }
		var back: String { "REG_USER_BACK_AUTH".localized() }
}

struct XRecoverySuccessViewStrings {
	
		var successTitle: String { "REC_USER_PASSWORD_UPDATED_TITLE".localized() }
		var signIn: String { "REG_USER_SIGN_IN".localized() }
		var back: String { "REG_USER_BACK_AUTH".localized() }
}

struct XRegistrationControllerStrings {
		
		/// Регистрация
		var title: String { "WIZ_WELC_REGISTRATION".localized() }
		var titlePinCode: String { "REG_USER_CONFIRM_PHONE".localized() }
		var titleCompletion: String { "REG_USER_COMPLETION".localized() }
		var registrationSuccess: String { "REG_USER_SUCCESS".localized() }
		var sessionExpired: String { "REG_USER_SESSION_TIMEOUT".localized() }
		var authError: String { "REG_USER_AUTH_ERROR".localized() }
		var serialNumber: String { "REG_USER_DEVICE_NUMBER".localized() }
	
		/// Восстановление
		var titleRecovery: String { "REC_USER_ACCESS".localized() }
		var titleNewPassword: String { "REC_USER_NEW_PASSWORD".localized() }
		var titlePasswordUpdated: String { "REC_USER_PASSWORD_UPDATED".localized() }
		var wrongLogin: String { "REC_USER_INVALID_LOGIN".localized() }
		var confirmRights: String { "REC_USER_CONFIRM_RIGHTS".localized() }
		var enterNewPassword: String { "REC_USER_ENTER_NEW_PASSWORD".localized() }
		var newPasswordChanged: String { "REC_USER_PASSWORD_CHANGED".localized() }
		
		func confirmAlert(message: String) -> XAlertView.Strings {
				return XAlertView.Strings(
						
						title: message.localized(),
						text: nil,
						/// Yes
						positive: "YES".localized(),
						/// No
						negative: "NO".localized()
				)
		}
		
		func errorAlert(message: String) -> XAlertView.Strings {
				return XAlertView.Strings(
						title: message,
						text: nil,
						positive: "N_BUTTON_OK".localized(),
						negative: nil
				)
		}
}
