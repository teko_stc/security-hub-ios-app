//
//  XRegistrationSuccessController.swift
//  SecurityHub
//
//  Created by Stefan on 07.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XRegistrationSuccessController: XBaseViewController<XRegistrationSuccessView>, XRegistrationSuccessDelegate {
		var login = ""
		var password_md5 = ""
		let strings = XRegistrationControllerStrings()
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: nil,
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
		
		override func viewDidLoad() {
				super.viewDidLoad()
				title = strings.registrationSuccess
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func signInSuccess() {
			UserDefaults.standard.set(login, forKey: "XLoginController.login")
			UserDefaults.standard.set(password_md5, forKey: "XLoginController.password")
			
			navigationController?.popToRootViewController(animated: false)
			
			NavigationHelper.shared.show(XSplashController())
		}
		
		private func signInError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { self.registrationBackTapped() } },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
	func registrationSignInViewTapped()
	{
		DataManager.connectDisposable?.dispose()
		mainView.startLoading()
		DataManager.connectDisposable = DataManager.shared.connect(login: login, password: password_md5)
				.subscribe(onNext:{ result in
						DataManager.connectDisposable?.dispose()
						self.mainView.stopLoading()
					if result.success { self.signInSuccess() }
					else { self.signInError(message: self.strings.authError, withBack: true) }
				})
	}
	
	func registrationBackTapped()
	{
		let navc = navigationController
		navc?.popViewController(animated: true)
//		navc?.popToRootViewController(animated: false)
//		navc?.pushViewController(XLoginController(), animated: false)
	}
}

