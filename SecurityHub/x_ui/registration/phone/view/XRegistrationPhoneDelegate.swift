//
//  XRegistrationPhoneDelegate.swift
//  SecurityHub
//
//  Created by Stefan on 01.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

protocol XRegistrationPhoneDelegate {
		func nextViewTapped(phone: UInt)
		func regSerialNumberViewTapped()
		func regGoogleViewTapped()
		func regVKViewTapped()
}
