//
//  XRegistrationPinCodeView.swift
//  SecurityHub
//
//  Created by Stefan on 02.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit
import SnapKit

class XRegistrationPinCodeView: UIView {
		public var delegate: XRegistrationPinCodeDelegate?
		private var pinCode1View, pinCode2View, pinCode3View, pinCode4View: XTextField!
		private var nextButtonView, requestAgainView: XZoomButton!
		private var requestTitleView, requestTimerView : UILabel!
		private var strings = XRegistrationPinCodeViewStrings()
		private var requestTimer: Timer?
		private var requestTimerCount: Int = 0
		private let requestTimerValue: Int = 60
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
		private func initViews() {
				backgroundColor = XRegistrationStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
			
				pinCode1View = XTextField (style: XRegistrationStyles.textFieldPinCodeStyle())
				pinCode2View = XTextField (style: XRegistrationStyles.textFieldPinCodeStyle())
				pinCode3View = XTextField (style: XRegistrationStyles.textFieldPinCodeStyle())
				pinCode4View = XTextField (style: XRegistrationStyles.textFieldPinCodeStyle())
			
				let pinCodeViewArray = [pinCode1View,pinCode2View,pinCode3View,pinCode4View]

				for (_, item) in pinCodeViewArray.enumerated()
				{
					guard let itemUw = item else { break	}
					
					itemUw.textContentType = .telephoneNumber
					itemUw.keyboardType = .numberPad
					itemUw.returnKeyType = .done
					itemUw.autocorrectionType = .no
					itemUw.autocapitalizationType = .none
					itemUw.spellCheckingType = .no
					itemUw.textAlignment = .center
					itemUw.xDelegate = self
					itemUw.tintColor = UIColor.clear
					addSubview(itemUw)
				}
								
				nextButtonView = XZoomButton (style: XRegistrationStyles.roundedButtonStyle())
				nextButtonView.text = strings.next
				nextButtonView.loadingText = strings.next
				nextButtonView.style.disableBackground = UIColor(red: 0xd8/255, green: 0xd8/255, blue: 0xd8/255, alpha: 1)
				nextButtonView.style.disable = UIColor.white
				nextButtonView.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
				nextButtonView.isEnabled = false
				addSubview(nextButtonView)
				
				requestTitleView = UILabel()
				requestTitleView.backgroundColor = UIColor.clear
				requestTitleView.textColor = UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1)
				requestTitleView.font = UIFont(name: "Open Sans", size: 13)
				requestTitleView.textAlignment = .center
				requestTitleView.lineBreakMode = .byWordWrapping
				requestTitleView.numberOfLines = 0
				requestTitleView.text = strings.requestTitle
				addSubview(requestTitleView)
			
				requestTimerView = UILabel()
				requestTimerView.backgroundColor = UIColor.clear
				requestTimerView.textColor = UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1)
				requestTimerView.font = UIFont(name: "Open Sans", size: 13)
				requestTimerView.textAlignment = .center
				requestTimerView.text = ""
				addSubview(requestTimerView)
			
				requestAgainView = XZoomButton (style: XRegistrationStyles.highlightButtonStyle())
				requestAgainView.text = strings.requestAgain
				requestAgainView.addTarget(self, action: #selector(requestAgainViewTapped), for: .touchUpInside)
				requestAgainView.isHidden = true
				addSubview(requestAgainView)
		}
		
		private func setConstraints() {
				pinCode1View.translatesAutoresizingMaskIntoConstraints = false
				pinCode2View.translatesAutoresizingMaskIntoConstraints = false
				pinCode3View.translatesAutoresizingMaskIntoConstraints = false
				pinCode4View.translatesAutoresizingMaskIntoConstraints = false
				nextButtonView.translatesAutoresizingMaskIntoConstraints = false
				requestAgainView.translatesAutoresizingMaskIntoConstraints = false
				requestTitleView.translatesAutoresizingMaskIntoConstraints = false
				requestTimerView.translatesAutoresizingMaskIntoConstraints = false
			
				NSLayoutConstraint.activate([
					pinCode1View.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 61),
					pinCode1View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -99),
					pinCode1View.widthAnchor.constraint(equalToConstant: 52),
					pinCode1View.heightAnchor.constraint(equalToConstant: 52),
					
					pinCode2View.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 61),
					pinCode2View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -33),
					pinCode2View.widthAnchor.constraint(equalToConstant: 52),
					pinCode2View.heightAnchor.constraint(equalToConstant: 52),
					
					pinCode3View.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 61),
					pinCode3View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 33),
					pinCode3View.widthAnchor.constraint(equalToConstant: 52),
					pinCode3View.heightAnchor.constraint(equalToConstant: 52),
					
					pinCode4View.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 61),
					pinCode4View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 99),
					pinCode4View.widthAnchor.constraint(equalToConstant: 52),
					pinCode4View.heightAnchor.constraint(equalToConstant: 52),
					
					nextButtonView.topAnchor.constraint(equalTo: pinCode4View.bottomAnchor, constant: 22),
					nextButtonView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 87),
					nextButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -174),
					nextButtonView.heightAnchor.constraint(equalToConstant: 40),
					
					requestAgainView.topAnchor.constraint(equalTo: nextButtonView.bottomAnchor, constant: 21),
					requestAgainView.centerXAnchor.constraint(equalTo: centerXAnchor),
					requestAgainView.widthAnchor.constraint(equalToConstant: 140),
					requestAgainView.heightAnchor.constraint(equalToConstant: 18),
					
					requestTitleView.topAnchor.constraint(equalTo: nextButtonView.bottomAnchor, constant: 21),
					requestTitleView.centerXAnchor.constraint(equalTo: centerXAnchor),
					requestTitleView.widthAnchor.constraint(equalToConstant: 160),
					requestTitleView.heightAnchor.constraint(equalToConstant: 36),
					
					requestTimerView.topAnchor.constraint(equalTo: requestTitleView.bottomAnchor, constant: 2),
					requestTimerView.centerXAnchor.constraint(equalTo: centerXAnchor),
					requestTimerView.widthAnchor.constraint(equalToConstant: 60),
					requestTimerView.heightAnchor.constraint(equalToConstant: 18),
				])
		}
	
		public func startRequestTimer(timerValue: Int)
		{
			if requestTimer != nil { requestTimer?.invalidate() }
			
			if timerValue > 0 { requestTimerCount = timerValue }
			else { requestTimerCount = requestTimerValue }
			updateRequestTimerCount()
			requestTimer = Timer.scheduledTimer(timeInterval: 1.0,
																			 target: self,
																			 selector: #selector(updateRequestTimer),
																			 userInfo: nil,
																			 repeats: true)
		}
	
		@objc func updateRequestTimer()
		{
			requestTimerCount -= 1
			
			updateRequestTimerCount()
			
			if ( requestTimerCount < 1 )
			{
				requestTimer?.invalidate()
				requestTimer = nil
				
				requestTitleView.isHidden = true
				requestTimerView.isHidden = true
				requestAgainView.isHidden = false
			}
		}
	
		private func updateRequestTimerCount ()
		{
			let min = requestTimerCount / requestTimerValue
			let sec = requestTimerCount - min * requestTimerValue
			
			requestTimerView.text = String ( format: "%02d : %02d",min, sec)
		}
	
		func stopRequestTimer()
		{
			if ( requestTimer != nil )
			{
				requestTimer?.invalidate()
				requestTimer = nil
			}
		}
	
		@objc private func nextViewTapped(pinCode: String) {
			delegate?.nextViewTapped(pinCode: pinCode)
		}
	
		@objc private func nextButtonTapped() {
			if let pinCode = pinCodeString()
			{
				nextViewTapped(pinCode: pinCode)
			}
		}
	
		@objc private func requestAgainViewTapped()
		{
			delegate?.requestAgainViewTapped()
			
			requestAgainView.isHidden = true
			requestTitleView.isHidden = false
			requestTimerView.isHidden = false
			nextButtonView.isEnabled = false
			startRequestTimer(timerValue: 0)
		}
		
		@objc private func viewTapped() {
				if pinCode1View.isFirstResponder { pinCode1View.resignFirstResponder()} else
				if pinCode2View.isFirstResponder { pinCode2View.resignFirstResponder()} else
				if pinCode3View.isFirstResponder { pinCode3View.resignFirstResponder()} else
				if pinCode4View.isFirstResponder { pinCode4View.resignFirstResponder()}
		}
	
		func highlightTextField ( textField: XTextField )
		{
			if textField.text?.count ?? 0 > 0
			{
				DispatchQueue.main.async {
					_ = textField.textFieldShouldBeginEditing(textField)
				}
			}
		}
	
		private func pinCodeString () -> String?
		{
			if let pin1 = pinCode1View.text, let pin2 = pinCode2View.text, let pin3 = pinCode3View.text, let pin4 = pinCode4View.text
			{
				let pinCode = pin1+pin2+pin3+pin4
				if (pinCode.count == 4) { return pinCode }
			}
			
			return nil
		}
	
		public func startLoading ()
		{
			nextButtonView.startLoading()
		}
		
		public func stopLoading ()
		{
			nextButtonView.endLoading()
			
			pinCode1View.text = ""
			pinCode2View.text = ""
			pinCode3View.text = ""
			pinCode4View.text = ""
			
			_ = pinCode1View.textFieldShouldEndEditing(pinCode1View)
			_ = pinCode2View.textFieldShouldEndEditing(pinCode2View)
			_ = pinCode3View.textFieldShouldEndEditing(pinCode3View)
			_ = pinCode4View.textFieldShouldEndEditing(pinCode4View)
			
			if self.requestTimer != nil { perform(#selector(disableNextButtonView), with: nil, afterDelay: 0.3) }
		}
	
		@objc func disableNextButtonView()
		{
			nextButtonView.isEnabled = false
		}
}

extension XRegistrationPinCodeView: XTextFieldDelegete {
	
		func xTextFieldShouldReturn(_ textField: UITextField) {
			if ( textField.isEqual (pinCode4View) ) { pinCode4View.resignFirstResponder() }
		}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		if let text = textField.text
		{
			if text.count > 0 && string.count > 0 { return false }
		}
		if string.count > 1 { return false }
		
		if string.count == 1
		{
			DispatchQueue.main.async {
				self.xTextFieldShouldEndEditing (textField)
			}
		}
		
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
		if ( textField.isEqual (pinCode1View) )
		{
			pinCode1View.resignFirstResponder()
			pinCode2View.becomeFirstResponder()
			highlightTextField(textField: pinCode1View)
		} else
		
		if ( textField.isEqual (pinCode2View) )
		{
			pinCode2View.resignFirstResponder()
			pinCode3View.becomeFirstResponder()
			highlightTextField(textField: pinCode2View)
		} else
		
		if ( textField.isEqual (pinCode3View) )
		{
			pinCode3View.resignFirstResponder()
			pinCode4View.becomeFirstResponder()
			highlightTextField(textField: pinCode3View)
		} else
		
		if ( textField.isEqual (pinCode4View) )
		{
			pinCode4View.resignFirstResponder()
			highlightTextField(textField: pinCode4View)
		}
		
		if let pinCodeString = pinCodeString()
		{
			nextButtonView.isEnabled = true
			nextViewTapped(pinCode: pinCodeString)
		}
	}
}

