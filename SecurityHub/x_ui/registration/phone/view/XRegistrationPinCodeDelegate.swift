//
//  XRegistrationPinCodeDelegate.swift
//  SecurityHub
//
//  Created by Stefan on 02.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

protocol XRegistrationPinCodeDelegate {
		func nextViewTapped(pinCode: String)
		func requestAgainViewTapped()
}
