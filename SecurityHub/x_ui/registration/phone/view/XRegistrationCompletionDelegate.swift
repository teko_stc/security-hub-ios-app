//
//  XRegistrationCompletionDelegate.swift
//  SecurityHub
//
//  Created by Stefan on 04.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

protocol XRegistrationCompletionDelegate {
	func registrationCompleteViewTapped(userData: RegUserData)
}
