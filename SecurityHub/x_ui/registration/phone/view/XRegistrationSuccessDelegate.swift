//
//  XRegistrationSuccessDelegate.swift
//  SecurityHub
//
//  Created by Stefan on 07.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import Foundation

protocol XRegistrationSuccessDelegate {
	func registrationSignInViewTapped()
	func registrationBackTapped()
}
