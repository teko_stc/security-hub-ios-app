//
//  XRegistrationDeviceController.swift
//  SecurityHub
//
//  Created by Stefan on 09.03.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit
import BarcodeScanner

class XRegistrationDeviceController: XBaseViewController<XRegistrationDeviceView>, XRegistrationPhoneDelegate, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
		let strings = XRegistrationControllerStrings()
		let registrationManager = RegistrationManager()
	
		
		private lazy var navigationViewBuilder = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[]
		)
	
		override func viewDidLoad() {
				super.viewDidLoad()

				title = strings.serialNumber
				setNavigationViewBuilder(navigationViewBuilder)
				mainView.delegate = self
		}
		
		private func registrationError(message: String?, withBack: Bool) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: strings.errorAlert(message: message),
					positive: { if withBack { self.back() } },
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func nextViewTapped(phone: UInt)
		{
			mainView.startLoading()
			let snWithPin = String(phone)
			let pinCode = String(snWithPin.suffix(4)).md5
			let index = snWithPin.index(snWithPin.endIndex, offsetBy: -4)
			let serialNumber = UInt(String(snWithPin.prefix(upTo: index)))
			registrationManager.validateDevicePinCode(serialNumber: serialNumber!, pinCode: pinCode) { result in
				self.mainView.stopLoading()
				if ( result.result != RegResultCode.SUCCESS ) {
					self.registrationError (message: result.localizedMessage, withBack: false) }
				else
				{
					let pcc = XRegistrationDeviceCompletionController()
					pcc.serialNumber = serialNumber!
					pcc.pinCodeMD5 = pinCode
					let navc = self.navigationController
					navc?.popViewController(animated: false)
					navc?.pushViewController(pcc, animated: true)
				}
			}
		}
		
		func regSerialNumberViewTapped() {
			let scannerViewController = BarcodeScannerViewController()
			scannerViewController.codeDelegate = self
			scannerViewController.errorDelegate = self
			scannerViewController.dismissalDelegate = self
			present(scannerViewController, animated: true)
		}
		
		func regGoogleViewTapped() {
		}
		
		func regVKViewTapped() {
		}
	
		func scanner(_ controller: BarcodeScanner.BarcodeScannerViewController, didCaptureCode code: String, type: String)
		{
			let serialNumber = String(code[code.index(after: code.startIndex)..<code.index(before: code.endIndex)])
			mainView.setDefault(device: serialNumber)
			DispatchQueue.main.async { self.dismiss(animated: true) }
		}
	
		func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error)
		{
			DispatchQueue.main.async {
				self.dismiss(animated: true)
				self.registrationError (message: error.localizedDescription,withBack: false) }
		}

		func scannerDidDismiss(_ controller: BarcodeScannerViewController)
		{
			dismiss(animated: true)
		}
}

