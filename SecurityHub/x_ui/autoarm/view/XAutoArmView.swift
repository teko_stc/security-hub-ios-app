//
//  XAutoArmView.swift
//  SecurityHub
//
//  Created by Stefan on 09.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

protocol XAutoArmDelegate {
	func typeViewTapped(alert: UIAlertController)
	func dataDidChange (model: DAutoArmEntity)
}

class XAutoArmView: UIView, XSwitchDelegate {
	
		public var delegate: XAutoArmDelegate?
		private var autoArmView, typeView: XEditButton!
		private var autoArmSwitchView: XSwitch!
		private var dateTimeView: XTextField!
		private var weekView: UIView!
		private var dateTimeSubView = UILabel()
		private var nextButtonView: XZoomButton!
		private var dateTimeTopAnchor: NSLayoutConstraint!
	
		private let strings = XAutoArmViewStrings()
		private lazy var style: XZoneSettingsViewStyleProtocol = XZoneSettingsViewStyle()
		private var dateTime = ""
		private var time = ""
		private var daysOfWeekMask: Int64 = 0b1111111
		private let dateFormat = "dd.MM.yy HH:mm"
		private let timeFormat = "HH:mm"
		
		override init(frame: CGRect) {
				super.init(frame: frame)
				initViews()
				setConstraints()
		}
		required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
		override func layoutSubviews() {
				super.layoutSubviews()
		}
	
		public func setData(model: DAutoArmEntity) {
			if model.id > 0 {
				autoArmSwitchView.isOn = true
				autoArmView.title = strings.autoArmEnable
				dateTimeSubView.isHidden = false
				dateTimeView.isHidden = false
				typeView.isHidden = false
				
				if model.active > 0 {
					typeView.title = strings.typeSchedule
					showWeekDays()
					dateTimeView.placeholder = strings.time
					time = String(format: "%02d:%02d", model.hour, model.minute)
					dateTimeView.text = time
					daysOfWeekMask = model.daysOfWeek
					let date = Date(timeIntervalSince1970: Double(4868899200+model.hour*3600+model.minute*60))
					if let picker = dateTimeView.inputView as? UIDatePicker { picker.date = date }
					weekView.subviews.forEach { view in
						if let button = view as? UIButton {
							let bitMask: Int64 = 1 << button.tag
							if (model.daysOfWeek & bitMask) != 0 { button.isSelected = true }
							else { button.isSelected = false }
						}
					}
				} else {
					typeView.title = strings.typeOnce
					hideWeekDays()
					dateTimeView.placeholder = strings.dateTime
					let dateFormatter: DateFormatter = DateFormatter()
					dateFormatter.locale = .current
					dateFormatter.dateFormat = dateFormat
					let date = Date(timeIntervalSince1970: Double(model.runAt))
					dateTime = dateFormatter.string(from: date)
					dateTimeView.text = dateTime
					if let picker = dateTimeView.inputView as? UIDatePicker { picker.date = date }
				}
			} else {
				autoArmSwitchView.isOn = false
				autoArmView.title = strings.autoArmDisable
				typeView.isHidden = true
				weekView.isHidden = true
				dateTimeView.isHidden = true
				dateTimeSubView.isHidden = true
			}
		}
		
		private func initViews() {
				backgroundColor = XAutoArmStyles.backgroundColor()
				addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
			
				autoArmView = XEditButton(
					style: XEditButtonStyle(
				 header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20)),
				 title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
		 			)
 				)
				autoArmView.headerTitle = strings.autoArmTitle
				autoArmView.title = strings.autoArmDisable
				autoArmView.addTarget(self, action: #selector(autoArmViewTapped), for: .touchUpInside)
				addSubview(autoArmView)
			
				autoArmSwitchView = XSwitch()
				autoArmSwitchView.delegate = self
				autoArmView.addSubview(autoArmSwitchView)
			
				typeView = XEditButton (style: style.baseView)
				typeView.headerTitle = strings.typeTitle
				typeView.title = strings.typeOnce
				typeView.addTarget(self, action: #selector(typeViewTapped), for: .touchUpInside)
				typeView.isHidden = true
				addSubview(typeView)
			
				weekView = UIView()
				weekView.isHidden = true
				addSubview(weekView)
			
				var calendar = Calendar(identifier: .gregorian)
				calendar.locale = Locale (identifier: DataManager.defaultHelper.language)
				var daysOfWeek = calendar.shortWeekdaySymbols
				daysOfWeek.append(daysOfWeek[0])
				daysOfWeek.remove(at: 0)
				for i in 0...6 {
					let button = UIButton(frame: CGRect(x: i*42, y: 0, width: 30, height: 30))
					button.setBackgroundImage(UIImage(named: "ic_week_day"), for: .normal)
					button.setTitleColor(UIColor.colorFromHex(0x3abeff), for: .normal)
					button.setBackgroundImage(UIImage(named: "ic_week_day_selected"), for: .selected)
					button.setTitleColor(UIColor.white, for: .selected)
					button.titleLabel?.textAlignment = .center
					button.titleLabel?.font = UIFont(name: "Open Sans", size: 15.5)
					button.setTitle(daysOfWeek[i].uppercased()[0..<2], for: .normal)
					button.tag = i
					button.addTarget(self, action: #selector(weekDayTapped(sender:)), for: .touchUpInside)
					button.isSelected = true
					weekView.addSubview(button)
				}
			
				dateTimeView = XTextField (style: XAutoArmStyles.textFieldNormalStyle())
				dateTimeView.textContentType = .name
				dateTimeView.keyboardType = .default
				dateTimeView.returnKeyType = .done
				dateTimeView.autocorrectionType = .no
				dateTimeView.autocapitalizationType = .none
				dateTimeView.spellCheckingType = .no
				dateTimeView.xDelegate = self
				dateTimeView.placeholder = strings.dateTime
				let datePicker: UIDatePicker = UIDatePicker()
				datePicker.frame = self.frame
				datePicker.datePickerMode = .date
				datePicker.locale = Locale(identifier: DataManager.defaultHelper.language)
				datePicker.timeZone = NSTimeZone.local
				datePicker.backgroundColor = UIColor.white
				datePicker.date = Date().addingTimeInterval(300)
				datePicker.minimumDate = Date().addingTimeInterval(60)
				datePicker.frame = CGRect (x: 0, y:frame.size.height - 116.0, width: frame.size.width, height: 116.0)
				if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels }
				datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
				dateTimeView.inputView = datePicker
				let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 10, height: 44))
				let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//				let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dateTimeDoneTapped))
				let done = UIBarButtonItem(title: "OK".localized(), style: .plain, target: self, action: #selector(dateTimeDoneTapped))
				let dateTimeSwitch = UIBarButtonItem(title: strings.time, style: .plain, target: self, action: #selector(dateTimeSwitchTapped))
				toolbar.setItems([dateTimeSwitch,flexSpace,done], animated: false)
				dateTimeView.inputAccessoryView = toolbar
				dateTimeView.isHidden = true
				addSubview(dateTimeView)
			
				dateTimeSubView.backgroundColor = UIColor.clear
				dateTimeSubView.textColor = UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1)
				dateTimeSubView.font = UIFont(name: "Open Sans", size: 13)
				dateTimeSubView.textAlignment = .center
				dateTimeSubView.lineBreakMode = .byWordWrapping
				dateTimeSubView.numberOfLines = 0
				dateTimeSubView.text = strings.pressToSet
				dateTimeSubView.isHidden = true
				addSubview(dateTimeSubView)
		}
		
		private func setConstraints() {
				autoArmView.translatesAutoresizingMaskIntoConstraints = false
				autoArmSwitchView.translatesAutoresizingMaskIntoConstraints = false
				typeView.translatesAutoresizingMaskIntoConstraints = false
				weekView.translatesAutoresizingMaskIntoConstraints = false
				dateTimeView.translatesAutoresizingMaskIntoConstraints = false
				dateTimeSubView.translatesAutoresizingMaskIntoConstraints = false
			
				dateTimeTopAnchor = dateTimeView.topAnchor.constraint(equalTo: typeView.bottomAnchor, constant: 23)
				var leadingConstant = 53.0
				let width = UIScreen.main.bounds.width
				if (width - 78) < 282 { leadingConstant = (width - 282.0) / 2 }
			
				NSLayoutConstraint.activate([
					autoArmView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 23),
					autoArmView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
					autoArmView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -25),
					autoArmView.heightAnchor.constraint(equalToConstant: 54),
					
					autoArmSwitchView.centerYAnchor.constraint(equalTo: autoArmView.centerYAnchor),
					autoArmSwitchView.trailingAnchor.constraint(equalTo: autoArmView.trailingAnchor),
					autoArmSwitchView.widthAnchor.constraint(equalToConstant: 30),
					autoArmSwitchView.heightAnchor.constraint(equalToConstant: 18),
					
					typeView.topAnchor.constraint(equalTo: autoArmView.bottomAnchor, constant: 23),
					typeView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
					typeView.trailingAnchor.constraint(equalTo: autoArmView.trailingAnchor),
					typeView.heightAnchor.constraint(equalToConstant: 54),
					
					weekView.topAnchor.constraint(equalTo: typeView.bottomAnchor, constant: 23),
					weekView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
					weekView.widthAnchor.constraint(equalToConstant: 282),
					weekView.heightAnchor.constraint(equalToConstant: 30),
					
					dateTimeTopAnchor,
					dateTimeView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant),
					dateTimeView.trailingAnchor.constraint(equalTo: weekView.trailingAnchor),
					dateTimeView.heightAnchor.constraint(equalToConstant: 52),
					
					dateTimeSubView.topAnchor.constraint(equalTo: dateTimeView.bottomAnchor, constant: 4),
					dateTimeSubView.leadingAnchor.constraint(equalTo: dateTimeView.leadingAnchor),
					dateTimeSubView.trailingAnchor.constraint(equalTo: dateTimeView.trailingAnchor),
				])
		}
	
		private func showWeekDays() {
				removeConstraint(dateTimeTopAnchor)
				dateTimeTopAnchor = dateTimeView.topAnchor.constraint(equalTo: weekView.bottomAnchor, constant: 23)
				addConstraint(dateTimeTopAnchor)
			
				weekView.isHidden = false
		}
	
		private func hideWeekDays() {
				weekView.isHidden = true
			
				removeConstraint(dateTimeTopAnchor)
				dateTimeTopAnchor = dateTimeView.topAnchor.constraint(equalTo: typeView.bottomAnchor, constant: 23)
				addConstraint(dateTimeTopAnchor)
		}
	
		private func dataDidChange ()
		{
			var id: Int64 = 0
			var runAt: Int64 = 0
			var active: Int64 = 0
			var hour: Int64 = 0
			var minute: Int64 = 0
			
			if autoArmSwitchView.isOn {
				id = 1
				if typeView.title == strings.typeOnce {
					let dateFormatter: DateFormatter = DateFormatter()
					dateFormatter.locale = .current
					dateFormatter.dateFormat = dateFormat
					let date = dateFormatter.date(from: dateTimeView.text ?? "")
					if let date = date {
						runAt = Int64(Int (date.timeIntervalSince1970)) }
					else { runAt = -1 }
				} else {
					if dateTimeView.text?.count == 5 && daysOfWeekMask > 0 {
						let timeComponents = dateTimeView.text?.components(separatedBy: ":")
						hour = Int64 (timeComponents![0])!
						minute = Int64 (timeComponents![1])!
						active = 1
					} else { active = -1 }
				}
			}
			
			delegate?.dataDidChange(model: DAutoArmEntity(id: id, site: 0, device: 0, section: 0, runAt: runAt, active: active, daysOfWeek: daysOfWeekMask, hour: hour, minute: minute, timezone: 0, lastExecuted: 0))
			
			return
		}
	
		@objc func datePickerValueChanged(_ sender: UIDatePicker) {
				let dateFormatter: DateFormatter = DateFormatter()
				if typeView.title == strings.typeOnce {
					sender.minimumDate = Date().addingTimeInterval(60)
					dateFormatter.dateFormat = dateFormat
					dateTime = dateFormatter.string(from: sender.date)
					dateTimeView.text = dateTime
				} else {
					dateFormatter.dateFormat = timeFormat
					time = dateFormatter.string(from: sender.date)
					dateTimeView.text = time }
			
				dateTimeSubView.isHidden = false
			
				dataDidChange()
		}
	
		@objc private func dateTimeDoneTapped() {
			dateTimeView.resignFirstResponder()
		}
	
		@objc private func dateTimeSwitchTapped() {
			if let toolbar = dateTimeView.inputAccessoryView as? UIToolbar, let picker = dateTimeView.inputView as? UIDatePicker
			{
				if let item = toolbar.items?.first
				{
					if picker.datePickerMode == .date {
						item.title = strings.date
						picker.datePickerMode = .time
					} else {
						item.title = strings.time
						picker.datePickerMode = .date
					}
				}
			}
		}
	
		func switchStateChange(switch: XSwitch, isOn: Bool) {
			if dateTimeView.isFirstResponder {
				dateTimeView.resignFirstResponder() }
			
			if isOn {
				autoArmView.title = strings.autoArmEnable
				typeView.isHidden = false
				if typeView.title == strings.typeOnce { hideWeekDays() }
				else { showWeekDays() }
				dateTimeView.isHidden = false
				dateTimeSubView.isHidden = dateTimeView.text?.count ?? 0 == 0
			} else {
				autoArmView.title = strings.autoArmDisable
				typeView.isHidden = true
				hideWeekDays()
				dateTimeView.isHidden = true
				dateTimeSubView.isHidden = true
			}
			
			dataDidChange()
		}
		
		@objc private func autoArmViewTapped() {
			let isOn = !autoArmSwitchView.isOn
			autoArmSwitchView.isOn = isOn
			switchStateChange(switch: autoArmSwitchView, isOn: isOn)
		}
	
		@objc private func typeViewTapped() {
			if dateTimeView.isFirstResponder {
				dateTimeView.resignFirstResponder() }
			
			guard let delegate = delegate else { return }
			
			let alert = UIAlertController(title: strings.typeSelect, message: nil, preferredStyle: .actionSheet)
			alert.addAction(UIAlertAction(title: strings.typeOnce, style: .default, handler: { action in
				self.typeView.title = self.strings.typeOnce
				self.hideWeekDays()
				self.dateTimeView.placeholder = self.strings.dateTime
				self.dateTimeView.text = self.dateTime
				if self.dateTime.count > 0 { self.dateTimeSubView.isHidden = false }
				else { self.dateTimeSubView.isHidden = true }
				self.dataDidChange()
			}))
			
			alert.addAction(UIAlertAction(title: strings.typeSchedule, style: .default, handler: { action in
				self.typeView.title = self.strings.typeSchedule
				self.showWeekDays()
				self.dateTimeView.placeholder = self.strings.time
				self.dateTimeView.text = self.time
				if self.time.count > 0 { self.dateTimeSubView.isHidden = false }
				else { self.dateTimeSubView.isHidden = true }
				self.dataDidChange()
			}))
			
			delegate.typeViewTapped(alert: alert)
		}
	
		@objc private func weekDayTapped(sender: UIButton) {
			let selected = !sender.isSelected
			sender.isSelected = selected
			if selected { daysOfWeekMask |= 1 << sender.tag }
			else { daysOfWeekMask ^= 1 << sender.tag }
			dataDidChange()
		}
		
		@objc private func viewTapped() {
				if dateTimeView.isFirstResponder {
					dateTimeView.resignFirstResponder() }
		}
}

extension XAutoArmView: XTextFieldDelegete {
	
	func xTextFieldShouldReturn(_ textField: UITextField) {
		if dateTimeView.isFirstResponder {
			dateTimeView.resignFirstResponder() }
	}
	
	func xTextField (	_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool
	{
		if textField.isEqual(dateTimeView) { return false }
		
		return true
	}
	
	func xTextFieldShouldBeginEditing(_ textField: UITextField)
	{
		if textField.isEqual(dateTimeView)
		{
			if let toolbar = dateTimeView.inputAccessoryView as? UIToolbar, let picker = dateTimeView.inputView as? UIDatePicker
			{
				if let item = toolbar.items?.first
				{
					let positionY = frame.origin.y + weekView.frame.origin.y + 80
					picker.frame = CGRect (x: 0, y:positionY, width: frame.size.width, height: frame.size.height - positionY)
					
					if typeView.title == strings.typeSchedule
					{
						item.title = strings.date
						picker.datePickerMode = .time
						item.isEnabled = false
						if time.count > 0 {
							let dateFormatter = DateFormatter()
							dateFormatter.dateFormat = timeFormat
							picker.minimumDate = nil
							picker.date = dateFormatter.date(from: time) ?? Date()
						}
					} else {
						picker.minimumDate = Date().addingTimeInterval(60)
						item.isEnabled = true
						if textField.text?.count ?? 0 < 1 {
							picker.date = Date().addingTimeInterval(300)
						}
					}
				}
				if textField.text?.count ?? 0 < 1 { datePickerValueChanged(picker) }
			}
		}
	}
	
	func xTextFieldShouldEndEditing(_ textField: UITextField)
	{
	}
}
