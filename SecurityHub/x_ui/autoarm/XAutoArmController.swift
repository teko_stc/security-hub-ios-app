//
//  XAutoArmController.swift
//  SecurityHub
//
//  Created by Stefan on 09.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

class XAutoArmController: XBaseViewController<XAutoArmView>, XAutoArmDelegate {
		override var tabBarIsHidden: Bool { true }
	
	public var autoArm: DAutoArmEntity? {
		didSet { if let autoArm = autoArm { mainView.setData(model: autoArm) } } }
	public var autoArmEdit: DAutoArmEntity?
		
		private lazy var navigationViewBuilderOff = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[XNavigationViewRightViewBuilder(title: "SITE_NAME_CHANGE".localized(), font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xA7A9AC),disabled: true)]
		)
	
		private lazy var navigationViewBuilderOn = XNavigationViewBuilder(
				backgroundColor: .clear,
				leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
				titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
				rightViews:[XNavigationViewRightViewBuilder(title: "SITE_NAME_CHANGE".localized(), font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042), viewTapped: changeViewTapped)]
		)
	
		override func viewDidLoad() {
				super.viewDidLoad()

				title = " "
				setNavigationViewBuilder(navigationViewBuilderOff)
				mainView.delegate = self
		}
		
		private func changeError(message: String?) {
				guard let message = message else { return }
				let alert = XAlertController(
					style: XRegistrationStyles.errorAlertStyle(),
					strings: self.errorAlert(message: message),
					positive: {},
					negative: nil
			)
			navigationController?.present(alert, animated: true)
		}
	
		func errorAlert(message: String) -> XAlertView.Strings {
				return XAlertView.Strings(
						title: message,
						text: nil,
						positive: "N_BUTTON_OK".localized(),
						negative: nil
				)
		}
	
		func changeViewTapped() {
			guard let autoarm = autoArmEdit else { return }
			
			setNavigationViewBuilder(navigationViewBuilderOff)
			
			var dParam: [String:Any] = [:]
			if autoarm.site > 0 { dParam["site_id"] = autoarm.site }
			else { dParam["device_id"] = autoarm.device; dParam["section"] = autoarm.section }
			var command = HubCommandWithResult.AUTOARM_SET
			if autoarm.id > 0 {
				if autoarm.active == 0 {
					dParam["run_at"] = autoarm.runAt
				} else {
					dParam["active"] = autoarm.active
					dParam["days_of_week"] = autoarm.daysOfWeek
					dParam["hour"] = autoarm.hour
					dParam["minute"] = autoarm.minute
					dParam["timezone"] = Int(TimeZone.current.secondsFromGMT() / 3600)
				}
			} else { command = HubCommandWithResult.AUTOARM_DEL }
			
			_ = DataManager.shared.hubHelper.get(command, D: dParam)
					.observeOn(ThreadUtil.shared.mainScheduler)
					.subscribe(onSuccess: { [weak self] (result: DCommandResult, value: HubAutoArm?) in
							guard let self = self else { return }
							if result.success {
								DataManager.shared.updateAutoArm(autoArm: self.autoArmEdit!)
								self.back()
							} else {
								self.showErrorColtroller(message: result.message)
								self.setNavigationViewBuilder(self.navigationViewBuilderOn)
							}
					}, onError: { [weak self] error in
							self?.showErrorColtroller(message: error.localizedDescription)
							self?.setNavigationViewBuilder(self?.navigationViewBuilderOn)
					})
		}
	
		func typeViewTapped(alert: UIAlertController) {
			self.present(alert, animated: true)
		}
	
		func dataDidChange (model: DAutoArmEntity) {
			guard let autoArm = autoArm else { return }
			
			var changeOn = false
			
			while true {
				if model.runAt == -1 || model.active == -1 { break }
				if model.id < 1 {
					if autoArm.id < 1 { break }
				} else
				if model.active == 0 {
					if model.runAt == autoArm.runAt { break }
				} else {
					if model.active == autoArm.active && model.daysOfWeek == autoArm.daysOfWeek &&
							model.hour == autoArm.hour && model.minute == autoArm.minute { break }
				}
				
				autoArmEdit = DAutoArmEntity(id: model.id, site: autoArm.site, device: autoArm.device, section: autoArm.section, runAt: model.runAt, active: model.active, daysOfWeek: model.daysOfWeek, hour: model.hour, minute: model.minute, timezone: 0, lastExecuted: 0)
				
				changeOn = true
				break
			}
			
			if changeOn { setNavigationViewBuilder(navigationViewBuilderOn) }
			else { setNavigationViewBuilder(navigationViewBuilderOff) }
		}
}

