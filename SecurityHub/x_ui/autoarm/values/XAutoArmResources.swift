//
//  XAutoArmResources.swift
//  SecurityHub
//
//  Created by Stefan on 09.04.2024.
//  Copyright © 2024 TEKO. All rights reserved.
//

import UIKit

struct XAutoArmStyles
{
	static func backgroundColor () -> UIColor
	{
		return UIColor.white
	}
	
	static func textFieldNormalStyle () -> XTextFieldStyle
	{
		return XTextFieldStyle (
			backgroundColor: UIColor.white,
			unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
			selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			font: UIFont(name: "Open Sans", size: 15.5))
	}
	
	static func roundedButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			font: UIFont(name: "Open Sans", size: 20),
			color: UIColor.white
		)
	}
	
	static func regularButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 20),
			color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
			alignment: .left
		)
	}
	
	static func regularGreyButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 16),
			color: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1),
			alignment: .center
		)
	}
	
	static func highlightButtonStyle () -> XZoomButtonStyle
	{
		return XZoomButtonStyle (
			backgroundColor: UIColor.clear,
			font: UIFont(name: "Open Sans", size: 13),
			color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
			alignment: .center
		)
	}
}

struct XAutoArmViewStrings {
		var autoArmTitle: String { "SITE_AUTOARM_TITLE".localized() }
		var autoArmDisable: String { "SITE_AUTOARM_DISABLE".localized() }
		var autoArmEnable: String { "SITE_AUTOARM_ENABLE".localized() }
		var typeTitle: String { "site_add_cluster".localized() }
		var typeOnce: String { "SITE_AUTOARM_ONCE".localized() }
		var typeSchedule: String { "SITE_AUTOARM_ON_SCHEDULE".localized() }
		var typeSelect: String { "SITE_AUTOARM_SELECT_TYPE".localized() }
		var dateTime: String { "SITE_AUTOARM_DATE_TIME".localized() }
		var date: String { "REC_USER_DATE".localized() }
		var time: String { "REC_USER_TIME".localized() }
		var pressToSet: String { "WIZ_ZONE_PRESS_TO_SET".localized() }
}


