//
//  DashboardAddSectionGroupViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 16.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddSectionGroupViewController: DashboardAddViewController {
    
    private var sections: [Sections] = []
    private var sections_i: [DSectionWithSitesDevicesInfo] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getSections()
        createItems()
    }
    
    private func setup() {
        self.tableView.allowsMultipleSelection = true
        self.titleLabel.text = "SECTION_GROUP".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_2".localized()
        self.nextButton.setTitle("N_BUTTON_NEXT".localized(), for: .normal)
    }

    private func getSections(){
        if siteId > 0 {
            self.sections = DataManager.shared.getSections(site_id: self.siteId).filter({ $0.section != 0 && $0.detector == 1 })
        } else {
            self.sections_i = DataManager.shared.dbHelper.getSections().filter({ $0.section.section != 0 && $0.section.detector == 1 })
            self.sections = sections_i.map({ $0.section })
        }
    }
    
    private func createItems() {
        for (index, section) in sections.enumerated() {
            var listSelectorName: String = "n_image_main_subtype_sgroup_list_selector"
            if let sectionType =  R.sectionTypes.first(where: { $0.id == section.detector })  {
                listSelectorName = sectionType.icons[0]
            }
            var imageName: String = ""
            var selectedName: String = ""
            if let selector = DataManager.shared.getSelector(name: listSelectorName) {
                if let lastItem = selector.items.last {
                    imageName = (lastItem.drawable as NSString).lastPathComponent
                }
                if let selectedItem = selector.items.first(where: { $0.stateSelected == true }) {
                    selectedName = (selectedItem.drawable as NSString).lastPathComponent
                }
            }
            var description = ""
            if sections_i.count > 0 { description = "\(sections_i[index].siteNames)" }
            let sectionItem = DashboardAddItem(
                image: UIImage(named: imageName) ?? UIImage(),
                selectedImage: UIImage(named: selectedName) ?? UIImage(),
                title: section.name,
                description: description)
            self.items.append(sectionItem)
        }
    }
    
    private var deviceId: Int64?
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardAddCellIdentifier") as? DashboardAddCell {
            cell.setup(item: items[indexPath.row])
            cell.setDeselectable(sections[indexPath.row].device != deviceId && deviceId != nil)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentSection = sections[indexPath.row]
        super.tableView(tableView, didSelectRowAt: indexPath)
        
        if let selectedIndexes = self.tableView.indexPathsForSelectedRows, selectedIndexes.count == 1 {
            deviceId = sections[indexPath.row].device
            if let updIndxs = tableView.indexPathsForVisibleRows?.filter({ indx in return !selectedIndexes.contains(where: { $0.row == indx.row }) }) {
                tableView.reloadRows(at: updIndxs, with: .automatic)
            }
        } else if deviceId != currentSection.device {
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.indexPathsForSelectedRows == nil {
            deviceId = nil
            if let updIndxs = tableView.indexPathsForVisibleRows {
                tableView.reloadRows(at: updIndxs, with: .automatic)
            }
        }
    }

    override func nextButtonPressed(_ sender: Any) {
        guard let selectedIndexes = self.tableView.indexPathsForSelectedRows else { return }
        var selectedSections = [Sections]()
        for index in selectedIndexes {
            let section = sections[index.row]
            selectedSections.append(section)
        }
        guard selectedSections.count > 0 else { return }
        openAddSectionGroupNameScreen(sections: selectedSections)
    }
    
    private func openAddSectionGroupNameScreen(sections: [Sections]) {
        let addSectionGroupNameVC = DashboardAddSectionGroupNameViewController(
            item: self.item,
            siteId: self.siteId,
            sections: sections,
            delegate: self.delegate)
        self.navigationController?.pushViewController(addSectionGroupNameVC, animated: true)
    }
}
