//
//  DashboardAddViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct DashboardAddItem {
    var image: UIImage?
    var selectedImage: UIImage?
    var title: String?
    var description: String
}

protocol DashboardAddViewControllerDelegate: AnyObject {
    func siteSelected(site: Sites, item: Int)
    func siteSelected(item: Int)
    func deviceSelected(device: Devices, item: Int)
    func sectionSelected(section: Sections, item: Int)
    func sectionGroupSelected(sectionGroup: SectionGroups, item: Int)
    func relaySelected(relay: Zones, item: Int)
    func zoneSelected(zone: Zones, item: Int)
    func cameraSelected(camera: Camera, item: Int)
    func sosSelected(siteId: Int64, item: Int)
}

extension DashboardAddViewControllerDelegate {
    func siteSelected(site: Sites, item: Int) { }
}

class DashboardAddViewController: UIViewController, UIGestureRecognizerDelegate {
    
		var waitNotReadyZone = false
    public var tabBarIsHidden: Bool { true }
    private var navigationObserver: Any?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    let item: Int
    let siteId: Int64
    weak var delegate: DashboardAddViewControllerDelegate?
    var items: [DashboardAddItem] = [DashboardAddItem]()
    
    init(item: Int, siteId: Int64, delegate: DashboardAddViewControllerDelegate?) {
        self.item = item
        self.siteId = siteId
        self.delegate = delegate
        super.init(nibName: "DashboardAddViewController", bundle: nil)
        self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
        (tabBarController as? XBaseTabBarController)?.setXBaseTabBarTopViewHidden(tabBarIsHidden)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationObserver = NotificationCenter.default.addObserver(forName: HubNotification.navigation, object: nil, queue: OperationQueue.main) { notification in
            guard let nV = self.navigationController else { return }
					if (nV.viewControllers.last is XNotReadyZonesController) == false, let nNotReadyZone = notification.object as? NNotReadyZone, self.waitNotReadyZone {
								nV.viewControllers.last!.present(XNotReadyZonesController(nNotReadyZone), animated: false)
							self.waitNotReadyZone = false
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        navigationController?.setNavigationBarHidden(true, animated: false)
        (tabBarController as? XBaseTabBarController)?.setXBaseTabBarTopViewHidden(tabBarIsHidden)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let _ = navigationObserver { NotificationCenter.default.removeObserver(navigationObserver!) }
    }
    
    
    private func setup() {
        titleLabel.text = ""
        descriptionLabel.text = ""
        backButton.isHidden = (self.navigationController?.viewControllers.count ?? 1) <= 1
        nextButton.isHidden = true
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "DashboardAddCell", bundle: nil), forCellReuseIdentifier: "DashboardAddCellIdentifier")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {

    }
}

extension DashboardAddViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardAddCellIdentifier") as? DashboardAddCell {
            cell.setup(item: items[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) { }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextButton.isHidden = (tableView.indexPathsForSelectedRows?.count ?? 0) == 0
    }
}

