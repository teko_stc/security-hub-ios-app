//
//  DashboardAddSectionViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 08.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddSectionViewController: DashboardAddViewController {
    
    private var sections: [Sections] = []
    private var sections_i: [DSectionWithSitesDevicesInfo] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getSections()
        createItems()
    }
    
    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_2".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
    }

    private func getSections(){
        if siteId > 0 {
            self.sections = DataManager.shared.getSections(site_id: self.siteId).filter({ $0.section != 0 })
        } else {
            self.sections_i = DataManager.shared.dbHelper.getSections().filter({ $0.section.section != 0 })
            self.sections = sections_i.map({ $0.section })
        }
    }
    
    private func createItems() {
        for (index, section) in sections.enumerated() {
            let sectionType = R.sectionTypes.first(where: { $0.id == section.detector })
            let listSelectorName = sectionType?.icons[0] ?? "n_image_main_subtype_section_list_selector"
            var imageName: String = ""
            var selectedName: String = ""
            if let selector = DataManager.shared.getSelector(name: listSelectorName) {
                if let lastItem = selector.items.last {
                    imageName = (lastItem.drawable as NSString).lastPathComponent
                }
                if let selectedItem = selector.items.first(where: { $0.stateSelected == true }) {
                    selectedName = (selectedItem.drawable as NSString).lastPathComponent
                }
            }
            var description = ""
            if sections_i.count > 0, let st = sectionType?.title {
                description = "\(st)\n\(sections_i[index].siteNames)"
            } else if sections_i.count > 0 {
                description = "\(sections_i[index].siteNames)"
            }
            let sectionItem = DashboardAddItem(
                image: UIImage(named: imageName) ?? UIImage(),
                selectedImage: UIImage(named: selectedName) ?? UIImage(),
                title: section.name,
                description: description)
            self.items.append(sectionItem)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        self.delegate?.sectionSelected(section: sections[selectedRow], item: self.item)
        self.navigationController?.popToRootViewController(animated: true)
    }
}
