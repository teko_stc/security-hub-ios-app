//
//  DashboardAddCameraViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 04.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddCameraViewController: DashboardAddViewController {

    private var cameras: [Camera] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        loadCameras()
    }
    
    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_5".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
    }
    
    private func loadCameras(){
        _ = DataManager.shared.getCameraList().subscribe(onNext: { [weak self] (cameras) in
            guard let cameras = cameras else {
                self?.cameras = []
                return
            }
            self?.cameras = cameras
            for camera in cameras {
                let cameraItem = DashboardAddItem(
                    image: UIImage(named: "ic_main_camera_list") ?? UIImage(),
                    selectedImage: UIImage(named: "ic_main_camera_list_i") ?? UIImage(),
                    title: camera.name,
                    description: "")
                self?.items.append(cameraItem)
            }
            self?.tableView.reloadData()
        },onError: { [weak self] error in
            self?.cameras = []
        })
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        self.delegate?.cameraSelected(camera: cameras[selectedRow], item: self.item)
        self.navigationController?.popToRootViewController(animated: true)
    }
}
