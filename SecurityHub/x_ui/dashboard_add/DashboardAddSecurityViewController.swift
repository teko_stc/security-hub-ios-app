//
//  DashboardAddSecurityViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 08.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddSecurityViewController: DashboardAddViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_2".localized()
        let allSiteItem = DashboardAddItem(
            image: UIImage(named: "ic_main_site_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_main_site_list_i") ?? UIImage(),
            title: "N_MAIN_TITLE_FULL_SITE".localized(),
            description: "N_MAIN_TITLE_FULL_SITE".localized())
        let controllerItem = DashboardAddItem(
            image: UIImage(named: "ic_controller_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_controller_list_i") ?? UIImage(),
            title: "CONTROLLER".localized(),
            description: "CONTROLLER".localized())
        let sectionGroupItem = DashboardAddItem(
            image: UIImage(named: "ic_section_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_section_list_i") ?? UIImage(),
            title: "SECTION_GROUP".localized(),
            description: "SECTION_GROUP".localized())
        let sectionItem = DashboardAddItem(
            image: UIImage(named: "ic_section_common_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_section_common_list_i") ?? UIImage(),
            title: "PARTITION".localized(),
            description: "PARTITION".localized())
        self.items = [allSiteItem, controllerItem, sectionItem, sectionGroupItem]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        if indexPath.row == 0, siteId > 0 {
            self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
        } else {
            self.nextButton.setTitle("N_BUTTON_NEXT".localized(), for: .normal)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        switch selectedRow {
        case 0:
            if siteId > 0 {
                self.delegate?.siteSelected(item: self.item)
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                let addSiteVC = DashboardAddSiteViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
                self.navigationController?.pushViewController(addSiteVC, animated: true)
            }
        case 1:
            openAddDevicesScreen()
        case 3:
            openAddSectionGroupScreen()
        case 2:
            openAddSectionScreen()
        default: break
        }
    }
    
    private func openAddDevicesScreen() {
        let addDeviceVC = DashboardAddDeviceViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addDeviceVC, animated: true)
    }
    
    private func openAddSectionGroupScreen() {
        let addSectionGroupVC = DashboardAddSectionGroupViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addSectionGroupVC, animated: true)
    }
    
    private func openAddSectionScreen() {
        let addSectionVC = DashboardAddSectionViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addSectionVC, animated: true)
    }
}
