//
//  DashboardAddRelayViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 04.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddRelayViewController: DashboardAddViewController {
    
    private var zones: [Zones] = []
    private var zones_i: [DZoneWithSitesDeviceSectionInfo] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getZones()
        createItems()
    }
    
    private func setup() {
        self.titleLabel.text = "BAR_CONTROL".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_3".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
    }
    
    private func getZones(){
        if siteId > 0 {
            self.zones = DataManager.shared.getZones(site_id: self.siteId).filter({ HubConst.isRelay($0.detector) })
        } else {
            self.zones_i = DataManager.shared.dbHelper.getZones().filter({ HubConst.isRelay($0.zone.detector) })
            self.zones = zones_i.map({ $0.zone })
        }
    }
    
    private func createItems() {
        for (index, zone) in zones.enumerated() {
            let listSelectorName = DataManager.shared.d3Const.getSensorIcons(uid: Int(zone.uidType), detector: Int(zone.detector)).list
            var imageName: String = ""
            var selectedName: String = ""
            if let selector = DataManager.shared.getSelector(name: listSelectorName) {
                if let lastItem = selector.items.last {
                    imageName = (lastItem.drawable as NSString).lastPathComponent
                }
                if let selectedItem = selector.items.first(where: { $0.stateSelected == true }) {
                    selectedName = (selectedItem.drawable as NSString).lastPathComponent
                }
            }
            var description = ""
            if zones_i.count > 0 { description = "\(zones_i[index].siteNames)\n\(zones_i[index].sectionName)" }
            let sectionItem = DashboardAddItem(
                image: UIImage(named: imageName) ?? UIImage(),
                selectedImage: UIImage(named: selectedName) ?? UIImage(),
                title: zone.name,
                description: description)
            self.items.append(sectionItem)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        self.delegate?.relaySelected(relay: zones[selectedRow], item: self.item)
        self.navigationController?.popToRootViewController(animated: true)
    }
}
