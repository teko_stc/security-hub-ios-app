//
//  DashboardAddMainViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 08.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddMainViewController: DashboardAddViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.reloadData()
    }

    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_1".localized()
        self.nextButton.setTitle("N_BUTTON_NEXT".localized(), for: .normal)
        let securityItem = DashboardAddItem(
            image: UIImage(named: "ic_arm_list_blue") ?? UIImage(),
            selectedImage: UIImage(named: "ic_arm_white") ?? UIImage(),
            title: "BAR_ARM".localized(),
            description: "BAR_ARM".localized())
        let managementItem = DashboardAddItem(
            image: UIImage(named: "ic_relay_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_relay_list_i") ?? UIImage(),
            title: "BAR_CONTROL".localized(),
            description: "BAR_CONTROL".localized())
        let deviceItem = DashboardAddItem(
            image: UIImage(named: "ic_device_common_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_device_common_list_i") ?? UIImage(),
            title: "BAR_DEVICE".localized(),
            description: "BAR_DEVICE".localized())
        let cameraItem = DashboardAddItem(
            image: UIImage(named: "ic_main_camera_list") ?? UIImage(),
            selectedImage: UIImage(named: "ic_main_camera_list_i") ?? UIImage(),
            title: "BAR_CAMERA".localized(),
            description: "BAR_CAMERA".localized())
        let sosItem = DashboardAddItem(
            image: UIImage(named: "ic_sos") ?? UIImage(),
            selectedImage: UIImage(named: "ic_sos_i") ?? UIImage(),
            title: "BAR_PANIC".localized(),
            description: "")
        self.items = [securityItem, managementItem, deviceItem, sosItem]
        if XTargetUtils.ivideon != nil {
            self.items.insert(cameraItem, at: 3)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        switch selectedRow {
        case 0:
            openAddSecurityScreen()
        case 1:
            openAddRelayScreen()
        case 2:
            openAddZoneScreen()
        case 3:
            if XTargetUtils.ivideon != nil {
                openAddCameraScreen()
            } else {
                if siteId > 0 {
                    delegate?.sosSelected(siteId: siteId, item: item)
                    navigationController?.popToRootViewController(animated: true)
                } else {
                    let addSiteVC = DashboardAddSiteViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
                    addSiteVC.isSos = true
                    self.navigationController?.pushViewController(addSiteVC, animated: true)
                }
            }
        case 4:
            if siteId > 0 {
                delegate?.sosSelected(siteId: siteId, item: item)
                navigationController?.popToRootViewController(animated: true)
            } else {
                let addSiteVC = DashboardAddSiteViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
                addSiteVC.isSos = true
                self.navigationController?.pushViewController(addSiteVC, animated: true)
            }
        default: break
        }
    }
    
    private func openAddSecurityScreen() {
        let addSecurityVC = DashboardAddSecurityViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addSecurityVC, animated: true)
    }
    
    private func openAddRelayScreen() {
        let addRelayVC = DashboardAddRelayViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addRelayVC, animated: true)
    }
    
    private func openAddZoneScreen() {
        let addDeviceVC = DashboardAddZoneViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addDeviceVC, animated: true)
    }
    
    private func openAddCameraScreen() {
        let addCameraVC = DashboardAddCameraViewController(item: self.item, siteId: self.siteId, delegate: self.delegate)
        self.navigationController?.pushViewController(addCameraVC, animated: true)
    }
}
