//
//  DashboardAddSectionGroupNameViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddSectionGroupNameViewController: DashboardAddViewController {
    private let sections: [Sections]
    
    private var nameLabel: UILabel!
    private var nameField: UITextField!
    
    init(item: Int, siteId: Int64, sections: [Sections], delegate: DashboardAddViewControllerDelegate?) {
        self.sections = sections
        super.init(item: item, siteId: siteId, delegate: delegate)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    private func setup() {
        self.tableView.allowsMultipleSelection = true
        self.titleLabel.text = "SECTION_GROUP".localized()
        self.descriptionLabel.text = "N_MAIN_SET_SGROUP_NAME".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
        
        self.tableView.isHidden = true
        
        nameLabel = UILabel()
        nameLabel.font = UIFont(name: "Open Sans", size: 16)
        nameLabel.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
        nameLabel.text = "N_GROUP_NAME".localized()
        
        self.view.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(64)
            maker.right.equalTo(-64)
            maker.top.equalTo(descriptionLabel.snp.bottom).offset(100)
            maker.height.equalTo(20)
        }
        
        nameField = UITextField()
        nameField.font = UIFont(name: "Open Sans", size: 25)
        nameField.textColor = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
        nameField.tintColor = UIColor(red: 58/255, green: 190/255, blue: 1.0, alpha: 1.0)
        nameField.returnKeyType = .done
        nameField.delegate = self
        
        self.view.addSubview(nameField)
        nameField.snp.makeConstraints { (maker) in
            maker.left.equalTo(64)
            maker.right.equalTo(-64)
            maker.top.equalTo(nameLabel.snp.bottom).offset(16)
            maker.height.equalTo(30)
        }
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(red: 58/255, green: 190/255, blue: 1.0, alpha: 1.0)
        
        self.view.addSubview(separatorView)
        separatorView.snp.makeConstraints { (maker) in
            maker.left.equalTo(64)
            maker.right.equalTo(-64)
            maker.top.equalTo(nameField.snp.bottom).offset(2)
            maker.height.equalTo(2)
        }
    }
    
    func createSectionGroup(name: String, completion: @escaping ((SectionGroups) -> Void)) {
        ThreadUtil.shared.backOpetarion.async {
            let groups = DataManager.shared.dbHelper.getSectionGroups()
            let newId : Int64
            if let lastId = groups.last?.id { newId = lastId + 1 } else {
                newId = 0
            }
            let newSectionGroup = SectionGroups(id: newId, name: name)
            DataManager.shared.dbHelper.add(sectionGroup: newSectionGroup)
            
            for section in self.sections {
                let sectionGroupSection = SectionGroupSection(sectionGroupId: newId, sectionId: section.id)
                DataManager.shared.dbHelper.add(sectionGroupSection: sectionGroupSection)
            }
            DispatchQueue.main.async {
                completion(newSectionGroup)
            }
        }
    }
}

extension DashboardAddSectionGroupNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let name = textField.text, name.count > 0 else { return false }
        createSectionGroup(name: name) { sectionGroup in
            self.delegate?.sectionGroupSelected(sectionGroup: sectionGroup, item: self.item)
            self.navigationController?.popToRootViewController(animated: true)
        }
        return false
    }
}
