//
//  DashboardAddDeviceViewController.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 31.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddDeviceViewController: DashboardAddViewController {
    
    private var devices: [Devices] = []
    private var devices_i: [DDeviceWithSitesInfo] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getDevices()
        createItems()
    }
    
    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_2".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
    }

    private func getDevices(){
        if siteId > 0 {
            self.devices = DataManager.shared.getDevices(site_id: self.siteId)
        } else {
            self.devices_i = DataManager.shared.dbHelper.getDevices()
            self.devices = devices_i.map({ $0.device })
        }
    }
    
    private func createItems() {
        for (index, device) in devices.enumerated() {
            let deviceType = HubConst.getDeviceType(device.configVersion, cluster: device.cluster_id)
            let icon = DataManager.shared.d3Const.getDeviceIcons(type: deviceType.int)

            var description = ""
            if devices_i.count > 0 { description = devices_i[index].siteNames }
            let sectionItem = DashboardAddItem(
                image: UIImage(named: icon.list) ?? UIImage(),
                selectedImage: UIImage(named: icon.list)?.withColor(color: .white) ?? UIImage(),
                title: device.name,
                description: description)
            self.items.append(sectionItem)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        self.delegate?.deviceSelected(device: devices[selectedRow], item: self.item)
        self.navigationController?.popToRootViewController(animated: true)
    }
}
