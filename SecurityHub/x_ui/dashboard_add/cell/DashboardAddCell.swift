//
//  DashboardAddCell.swift
//  SecurityHub
//
//  Created by Nail Gabutdinov on 08.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class DashboardAddCell: UITableViewCell {
    
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelBottomConstraint: NSLayoutConstraint!
    
    private var grayColor = UIColor.colorFromHex(0x414042)
    private var whiteColor = UIColor.white
    
    private var defImage: UIImage?
    private var selImage: UIImage?
    private var isDeselectable: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.descriptionLabel.isHidden = false
        self.titleLabelTopConstraint.constant = 7
        
        [titleLabel, descriptionLabel].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        iconImageView.image = selected ? selImage : defImage
        titleLabel.textColor = selected ? whiteColor : isDeselectable ? .inLightGray : grayColor
        descriptionLabel.textColor = selected ? whiteColor : isDeselectable ? .inLightGray : grayColor
        selectionView.isHidden = !selected
    }
    
    func setup(item: DashboardAddItem) {
        self.defImage = item.image
        self.selImage = item.selectedImage
        self.iconImageView.image = defImage
        self.titleLabel.text = item.title
        self.descriptionLabel.text = item.description
        if item.description.isEmpty {
            self.descriptionLabel.isHidden = true
            self.titleLabelTopConstraint.constant = 16
            self.descriptionLabelBottomConstraint.constant = 16
        } else {
            self.descriptionLabel.isHidden = false
            self.titleLabelTopConstraint.constant = 7
            self.descriptionLabelBottomConstraint.constant = 0
        }
    }
    
    func setDeselectable(_ isDeselectable: Bool) {
        self.isDeselectable = isDeselectable
    }
    
    func setForService() {
        self.titleLabelTopConstraint.constant = 20
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
        ])
        whiteColor = UIColor.colorFromHex(0x414042)
        grayColor = UIColor.colorFromHex(0x414042)
        selectionView.backgroundColor = UIColor.inLightGray.withAlphaComponent(0.6)
    }
}
