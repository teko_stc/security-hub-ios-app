//
//  DashboardAddSiteViewController.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 31.10.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class DashboardAddSiteViewController: DashboardAddViewController {
    
    private var sites: [Sites] = []
    internal var isSos = false

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getSites()
        createItems()
    }
    
    private func setup() {
        self.titleLabel.text = "N_MAIN_SET_TITLE".localized()
        self.descriptionLabel.text = "N_MAIN_SET_TITLE_7".localized()
        self.nextButton.setTitle("WIZ_READY".localized(), for: .normal)
    }

    private func getSites(){
        self.sites = DataManager.shared.dbHelper.getSites()
    }
    
    private func createItems() {
        for site in sites {
            let sectionItem = DashboardAddItem(
                image: UIImage(named: "ic_house_small") ?? UIImage(),
                selectedImage: UIImage(named: "ic_house_small_white") ?? UIImage(),
                title: site.name,
                description: "N_MAIN_TITLE_FULL_SITE".localized())
            self.items.append(sectionItem)
        }
    }
    
    override func nextButtonPressed(_ sender: Any) {
        guard let selectedRow = self.tableView.indexPathForSelectedRow?.row else { return }
        if isSos {
            self.delegate?.sosSelected(siteId: sites[selectedRow].id, item: self.item)
        } else {
            self.delegate?.siteSelected(site: sites[selectedRow], item: self.item)
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
}
