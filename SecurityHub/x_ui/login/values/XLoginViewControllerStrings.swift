//
//  XLoginViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XLoginViewStrings {
    /// Введите логин
    var loginPlaceholder: String { "DOMAIN_USER_ENTER_LOGIN_EDIT".localized() }
    
    /// Введите пароль
    var passwordPlaceholder: String { "DOMAIN_USER_ENTER_PASS_EDIT".localized() }
    
    /// Запомнить меня
    var savePassword: String { "LA_REMEMBER_ME".localized() }
    
    /// Забыли пароль?
    var losePassword: String { "N_WIZ_PIN_LOST".localized() }
    
    /// Войти
    var signIn: String { "WIZ_LOGIN_GO_ENTER".localized() }
    
    /// Авторизация
    var signInLoading: String { "" }
    
    /// Регистрация
    var signUp: String { "WIZ_WELC_REGISTRATION".localized() }
}

class XLoginControllerStrings {
    
    /// Вход
    var title: String { "WIZ_WELC_ENTER".localized() }
    
    func signUpAlertStrings() -> XAlertView.Strings {
        return XAlertView.Strings(
            /// You can sign up in the system on the website Secuti HUB.\nDo you want to sign up?
            title: "LA_SINGUP_DIALOG_MESS_1".localized() + " \(XTargetUtils.name) " + "LA_SINGUP_DIALOG_MESS_2".localized(),
            text: nil,
            /// Continue
            positive: "PNEA_NEXT".localized(),
            /// Cancel
            negative: "LA_SIGNUP_DIALOG_CANCEL".localized()
        )
    }
    
    func forgotPasswordAlertStrings() -> XAlertView.Strings {
        return XAlertView.Strings(
            title: "LA_PASS_REC_DIALOG_MESS_1".localized() + " \(XTargetUtils.name) " + "LA_PASS_REC_DIALOG_MESS_2".localized(),
            text: nil,
            /// Continue
            positive: "PNEA_NEXT".localized(),
            /// Cancel
            negative: "LA_SIGNUP_DIALOG_CANCEL".localized()
        )
    }
    
    func errorAlert(message: String) -> XAlertView.Strings {
        return XAlertView.Strings(
            title: message,
            text: nil,
            positive: "N_BUTTON_OK".localized(),
            negative: nil
        )
    }
}
