//
//  XLoginViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XLoginView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            field: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            savePassword: XCheckBoxView.Style(
                icon: XCheckBoxView.Style.Icon(
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                    unselect: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
                ), text: XCheckBoxView.Style.Text(
                    select: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    unselect: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 13)
                )
            ),
            losePassword: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 13),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                alignment: .right
            ),
            signIn: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.white
            ),
            signUp: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}

extension XLoginController {
    func secBaseAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func errorAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
