//
//  XLoginController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import Localize_Swift

extension XLoginController: XLoginViewDelegate {
    func signInViewTapped(login: String, isNeedSave: Bool) { signIn(login: login, isNeedSave: isNeedSave) }
    
    func signInViewTapped(login: String, password: String, isNeedSave: Bool) { signIn(login: login, password_md5: password.md5, isNeedSave: isNeedSave) }
    
    func signUpViewTapped() {
        /*let alert = XAlertController(
            style: secBaseAlertStyle(),
            strings: strings.signUpAlertStrings(),
            positive: xSignUpAlertControllerPositiveButtonTapped,
            negative: nil
        )
        navigationController?.present(alert, animated: true)*/
				navigationController?.pushViewController(XRegistrationPhoneController(), animated: true)
    }
    
    func forgotPasswordViewTapped() {
        /*let alert = XAlertController(
            style: secBaseAlertStyle(),
            strings: strings.forgotPasswordAlertStrings(),
            positive: xForgotPasswordAlertControllerPositiveButtonTapped,
            negative: nil
        )
        navigationController?.present(alert, animated: true)*/
				navigationController?.pushViewController(XRecoveryPhoneController(), animated: true)
    }
    
    func showErrorAlert(message: String) {
        let alert = XAlertController(
            style: errorAlertStyle(),
            strings: strings.errorAlert(message: message),
            positive: {},
            negative: nil
        )
        navigationController?.present(alert, animated: true)
    }
    
    func xSignUpAlertControllerPositiveButtonTapped() {
        let uri = "\(XTargetUtils.regLink!)?p=100&l=\(DataManager.defaultHelper.language)"
        let controller = XWebPageController(uri: uri)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func xForgotPasswordAlertControllerPositiveButtonTapped() {
        let uri = "\(XTargetUtils.regLink!)?p=101&l=\(DataManager.defaultHelper.language)"
        let controller = XWebPageController(uri: uri)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showPinCode() {
        navigationController?.pushViewController(XPinCodeController(state: .create), animated: true)
    }
    
    func showSpashController() {
        NavigationHelper.shared.show(XSplashController())
    }

    func serverChanger() {
        navigationController?.pushViewController(XServerChangerViewController(), animated: true)
    }
}

