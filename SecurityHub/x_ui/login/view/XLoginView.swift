//
//  XLoginView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import UIKit

class XLoginView: UIView {
    public var delegate: XLoginViewDelegate?
    private var loginView: XTextField!
    private var passwordView: XPasswordView!
    private var savePasswordView: XCheckBoxView!
    private var losePasswordView, signInView, signUpView: XZoomButton!
    private var strings: XLoginViewStrings = XLoginViewStrings()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func hideSignUpView() {
        signUpView.isHidden = true
    }
    
    public func setDefault(login: String) {
        loginView.text = login
        passwordView.setDefaultValue()
        savePasswordView.isSelected = true
    }
    
    public func startSignInAnimation() {
        signInView.startLoading()
    }
    
    public func endSignInAnimation() {
        signInView.endLoading()
    }
    
    private func initViews(style: Style) {
        backgroundColor = style.backgroundColor
        addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))

        loginView = XTextField(style: style.field)
        loginView.textContentType = .emailAddress
        loginView.returnKeyType = .next
        loginView.autocorrectionType = .no
        loginView.autocapitalizationType = .none
        loginView.spellCheckingType = .no
        loginView.xDelegate = self
        loginView.placeholder = strings.loginPlaceholder
        addSubview(loginView)
        
        passwordView = XPasswordView(style: style.field)
        passwordView.placeholder = strings.passwordPlaceholder
        passwordView.returnKeyType = .done
        passwordView.xDelegate = self
        addSubview(passwordView)
        
        savePasswordView = XCheckBoxView(style.savePassword)
        savePasswordView.text = strings.savePassword
        addSubview(savePasswordView)
        
        losePasswordView = XZoomButton(style: style.losePassword)
        losePasswordView.text = strings.losePassword
        losePasswordView.addTarget(self, action: #selector(losePasswordViewTapped), for: .touchUpInside)
        addSubview(losePasswordView)
        
        signInView = XZoomButton(style: style.signIn)
        signInView.text = strings.signIn
        signInView.loadingText = strings.signInLoading
        signInView.addTarget(self, action: #selector(signInViewTapped), for: .touchUpInside)
        addSubview(signInView)
        
        signUpView = XZoomButton(style: style.signUp)
        signUpView.text = strings.signUp
        signUpView.addTarget(self, action: #selector(signUpViewTapped), for: .touchUpInside)
        addSubview(signUpView)
    }
    
    private func setConstraints() {
        loginView.translatesAutoresizingMaskIntoConstraints = false
        passwordView.translatesAutoresizingMaskIntoConstraints = false
        savePasswordView.translatesAutoresizingMaskIntoConstraints = false
        losePasswordView.translatesAutoresizingMaskIntoConstraints = false
        signInView.translatesAutoresizingMaskIntoConstraints = false
        signUpView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loginView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 30),
            loginView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            loginView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, constant: -40),
            loginView.heightAnchor.constraint(equalToConstant: 52),
            
            passwordView.topAnchor.constraint(equalTo: loginView.bottomAnchor, constant: 20),
            passwordView.leadingAnchor.constraint(equalTo: loginView.leadingAnchor),
            passwordView.trailingAnchor.constraint(equalTo: loginView.trailingAnchor),
            passwordView.heightAnchor.constraint(equalTo: loginView.heightAnchor),

            savePasswordView.topAnchor.constraint(equalTo: passwordView.bottomAnchor, constant: 20),
            savePasswordView.leadingAnchor.constraint(equalTo: loginView.leadingAnchor),
            savePasswordView.heightAnchor.constraint(equalToConstant: 30),

            losePasswordView.topAnchor.constraint(equalTo: savePasswordView.topAnchor),
            losePasswordView.leadingAnchor.constraint(equalTo: savePasswordView.trailingAnchor),
            losePasswordView.trailingAnchor.constraint(equalTo: loginView.trailingAnchor),
            losePasswordView.widthAnchor.constraint(equalTo: savePasswordView.widthAnchor),
            losePasswordView.heightAnchor.constraint(equalTo: savePasswordView.heightAnchor),

            signInView.topAnchor.constraint(equalTo: savePasswordView.bottomAnchor, constant: 30),
            signInView.leadingAnchor.constraint(equalTo: loginView.leadingAnchor),
            signInView.trailingAnchor.constraint(equalTo: loginView.trailingAnchor),
            signInView.heightAnchor.constraint(equalToConstant: 40),

            signUpView.topAnchor.constraint(equalTo: signInView.bottomAnchor, constant: 6),
            signUpView.leadingAnchor.constraint(equalTo: loginView.leadingAnchor),
            signUpView.trailingAnchor.constraint(equalTo: loginView.trailingAnchor),
            signUpView.heightAnchor.constraint(equalTo: signInView.heightAnchor),
        ])
    }
    
    @objc private func losePasswordViewTapped() { delegate?.forgotPasswordViewTapped() }
    
    @objc private func signUpViewTapped() { delegate?.signUpViewTapped() }
    
    @objc private func signInViewTapped() {
        if (loginView.text ?? "").isEmpty { loginView.becomeFirstResponder() }
        else if (passwordView.text ?? "").isEmpty { passwordView.becomeFirstResponder() }
        else if (passwordView.isDefaulValue()) { delegate?.signInViewTapped(login: loginView.text!, isNeedSave: savePasswordView.isSelected) }
        else { delegate?.signInViewTapped(login: loginView.text!, password: passwordView.text!, isNeedSave: savePasswordView.isSelected) }
    }
    
    @objc private func viewTapped() {
        if loginView.isFirstResponder { loginView.resignFirstResponder()}
        if passwordView.isFirstResponder { passwordView.resignFirstResponder()}
    }
}

extension XLoginView: XTextFieldDelegete {
    func xTextFieldShouldReturn(_ textField: UITextField) {
        if (textField == loginView) { passwordView.becomeFirstResponder() }
        if (textField == passwordView) { viewTapped(); signInViewTapped() }
    }
}

