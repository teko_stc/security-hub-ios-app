//
//  XLoginViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XLoginViewStyle {
    let backgroundColor: UIColor
    let field: XTextField.Style
    let savePassword: XCheckBoxView.Style
    let losePassword, signIn, signUp: XZoomButton.Style
}
extension XLoginView {
    typealias Style = XLoginViewStyle
}

