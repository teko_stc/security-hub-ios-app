//
//  XLoginViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XLoginViewDelegate {
    func signInViewTapped(login: String, isNeedSave: Bool)
    
    func signInViewTapped(login: String, password: String, isNeedSave: Bool)
    
    func signUpViewTapped()
    
    func forgotPasswordViewTapped()
}
