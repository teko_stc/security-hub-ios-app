//
//  XLoginController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XLoginController: XBaseViewController<XLoginView> {
    let strings = XLoginControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .clear,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_settings-1", color: UIColor.colorFromHex(0x414042), viewTapped: self.serverChanger)]
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        if (XTargetUtils.regLink == nil) { mainView.hideSignUpView() }
        checkSavedLoginPasswordInfo()
    }
    
    public func signIn(login: String, isNeedSave: Bool) {
        guard let password = UserDefaults.standard.string(forKey: "XLoginController.password") else { return }
        signIn(login: login, password_md5: password, isNeedSave: isNeedSave)
    }
    
    public func signIn(login: String, password_md5: String, isNeedSave: Bool) {
        if (!isNeedSave) {
            UserDefaults.standard.set(nil, forKey: "XLoginController.login")
            UserDefaults.standard.set(nil, forKey: "XLoginController.password")
        }
        DataManager.connectDisposable?.dispose()
        mainView.startSignInAnimation()
        DataManager.connectDisposable = DataManager.shared.connect(login: login, password: password_md5)
            .subscribe(onNext:{ result in
                DataManager.connectDisposable?.dispose()
                self.mainView.endSignInAnimation()
                if result.success {  self.signInSuccess(login: login, password_md5: password_md5, isNeedSave: isNeedSave) }
                else { self.signInError(message: result.errorMes) }
            })
    }
    
    private func signInSuccess(login: String, password_md5: String, isNeedSave: Bool) {
        if (isNeedSave) {
            UserDefaults.standard.set(login, forKey: "XLoginController.login")
            UserDefaults.standard.set(password_md5, forKey: "XLoginController.password")
        }
        showSpashController()
    }
    
    private func signInError(message: String?) {
        guard let message = message else { return }
        showErrorAlert(message: message)
    }
    
    private func checkSavedLoginPasswordInfo() {
        guard let login = UserDefaults.standard.string(forKey: "XLoginController.login") else { return }
        mainView.setDefault(login: login)
    }
}
