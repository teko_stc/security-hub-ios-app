//
//  XWebPageController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XWebPageController: XBaseViewController<XWebPageView> {
    private let uri: String
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.colorFromHex(0xf6f6f6),
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    
    init(uri: String) {
        self.uri = uri
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.loadUrl(uri)
    }
}
