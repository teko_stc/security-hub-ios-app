//
//  XWebPageView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit
import WebKit

class XWebPageView: UIView {    
    private var webView: WKWebView = WKWebView()
    private var loaderView: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    init(){
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
        
    public func loadUrl(_ url_string: String) {
        guard let url = URL(string: url_string) else { return }
        var request = URLRequest(url: url)
        request.setValue("en", forHTTPHeaderField: "Accept-Language")
        webView.load(request)
    }
    
    private func initViews() {
        webView.isOpaque = false
        webView.navigationDelegate = self
        webView.backgroundColor = UIColor.colorFromHex(0xf6f6f6)
        addSubview(webView)
        
        loaderView.center = center
        loaderView.hidesWhenStopped = true
        loaderView.startAnimating()
        addSubview(loaderView)
    }
    
    private func setConstraints() {
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loaderView.heightAnchor.constraint(equalToConstant: 50),
            loaderView.widthAnchor.constraint(equalToConstant: 50),
            loaderView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),

            webView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension XWebPageView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loaderView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let serverTrust = challenge.protectionSpace.serverTrust else { return completionHandler(.useCredential, nil) }
        let exceptions = SecTrustCopyExceptions(serverTrust)
        SecTrustSetExceptions(serverTrust, exceptions)
        completionHandler(.useCredential, URLCredential(trust: serverTrust))
    }
}
