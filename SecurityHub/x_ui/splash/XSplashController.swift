//
//  XSplashController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSplashController: XBaseViewController<XSplashView> {
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .clear,
        leftView: nil,
        titleView: nil,
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_settings-1", color: UIColor.colorFromHex(0x414042), viewTapped: self.serverChanger)]
    )

    private let defaultHelper = DataManager.defaultHelper
    private let settingsHelper = DataManager.settingsHelper
    private var stateSuccess: Bool? = nil, stateHubSitesEnd = false, stateHubEventsEnd = false, stateHubOptionsEnd = false
        
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.delegate = self
        checkState()
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.doHubSitesEnd), name: NSNotification.Name(rawValue: "HubSitesEnd"), object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.doHubEventsEnd), name: NSNotification.Name(rawValue: "HubEventsEnd"), object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.doHubOptionsEnd), name: NSNotification.Name(rawValue: "HubOptionsEnd"), object: nil)
    }
    
    public func acceptPersonAgreement() {
        defaultHelper.needShowPersonAgreement = false
        mainView.showSignInView()
        setNavigationViewBuilder(navigationViewBuilder)
        if (XTargetUtils.regLink != nil) { mainView.showSignUpView() }
    }
    
    private func checkState() {
        if defaultHelper.needShowPersonAgreement, XTargetUtils.isHubTarget {
            showPersonsAgreement()
        } else {
					perform(#selector(checkUser), with: nil, afterDelay: 0.3)
        }
    }
    
		@objc private func checkUser(){
        DataManager.connectDisposable = DataManager.shared.connect()
            .subscribe(
                onNext: { success in
                    DataManager.connectDisposable?.dispose()
                    if success == false {
                        self.auth(success: success)
                    } else {
                        self.stateSuccess = success
                        if self.stateHubSitesEnd, self.stateHubEventsEnd, self.stateHubOptionsEnd {
                            self.auth(success: success)
                        }
                    }
                }
            )
    }
    
    @objc private func doHubSitesEnd(){
        stateHubSitesEnd = true
        if let success = stateSuccess, stateHubEventsEnd, stateHubOptionsEnd {
            DispatchQueue.main.async {
                self.auth(success: success)
            }
        }
    }
    
    @objc private func doHubEventsEnd(){
        stateHubEventsEnd = true
        if let success = stateSuccess, stateHubSitesEnd, stateHubOptionsEnd {
            DispatchQueue.main.async {
                self.auth(success: success)
            }
        }
    }
    
    @objc private func doHubOptionsEnd(){
        stateHubOptionsEnd = true
        if let success = stateSuccess, stateHubSitesEnd, stateHubEventsEnd {
            DispatchQueue.main.async {
                self.auth(success: success)
            }
        }
    }
    
    var isauth = false
    private func auth(success: Bool) {
        guard isauth == false else {
            return
        }
        
        isauth = true

        if success && DataManager.defaultHelper.pin.isEmpty && self.settingsHelper.needPin {
            let controller = XPinCodeController(state: .create)
            NavigationHelper.shared.showNew(controller)
        } else if success && self.settingsHelper.needPin {
            self.showPinCode()
        } else if success {
            self.showMain()
        } else {
            self.mainView.showSignInView()
            self.setNavigationViewBuilder(navigationViewBuilder)
            if (XTargetUtils.regLink != nil) { self.mainView.showSignUpView() }
        }
    }
}
