//
//  XSplashViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import Localize_Swift

class XSplashViewStrings {
    
    /// Войти
    var signIn: String { "IV_LOGIN".localized() }
    
    /// Регистрация
    var signUp: String { "WIZ_WELC_REGISTRATION".localized() }
}

extension XSplashController {
    func rejectAlertStrings() -> XAlertView.Strings {
        return XAlertView.Strings(
            /// To continue operation, you must accept the license agreement.\nIf it is rejected, the application will be closed automatically.
            title: "MESS_LICENSE".localized(),
            text: nil,
            /// Reject
            positive: "REJECT".localized(),
            ///Cancel
            negative: "LA_SIGNUP_DIALOG_CANCEL".localized()
        )
    }
    
    func signUpAlertStrings() -> XAlertView.Strings {
        return XAlertView.Strings(
            /// You can sign up in the system on the website Secuti HUB.\nDo you want to sign up?
            title: "LA_SINGUP_DIALOG_MESS_1".localized() + " \(XTargetUtils.name) " + "LA_SINGUP_DIALOG_MESS_2".localized(),
            text: nil,
            /// Continue
            positive: "DOMAIN_USER_ENTER_NAME_CONT".localized(),
            /// Cancel
            negative: "LA_SIGNUP_DIALOG_CANCEL".localized()
        )
    }
}
