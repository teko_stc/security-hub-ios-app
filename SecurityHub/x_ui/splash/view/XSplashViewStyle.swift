//
//  XSplashViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSplashViewStyle {
    struct Logo {
        let name: String
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    let logo: Logo
    
    let signInStyle: XZoomButton.Style
    let signUnStyle: XZoomButton.Style
}

extension XSplashView {
    typealias Style = XSplashViewStyle
}
