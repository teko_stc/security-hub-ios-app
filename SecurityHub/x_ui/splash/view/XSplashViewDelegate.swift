//
//  XSplashViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XSplashViewDelegate {
    func signIn()
    func signUp()
}

