//
//  XSplashView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSplashView: UIView {
    public var delegate: XSplashViewDelegate?
    private var logoView: UIImageView!
    private var loaderView: XLoaderView = XLoaderView()
    private var titleView: UILabel = UILabel()
    private var signInView, signUpView: XZoomButton!
    private let strings: XSplashViewStrings = XSplashViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews(styles())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func showSignInView() {
        superview?.backgroundColor = .white
        backgroundColor = .white
        
        switch XTargetUtils.target {
        case .security_hub, .dev, .test:
            logoView.image = UIImage(named: "new_logo")
        default:
            logoView.image = UIImage(named: "hub_colorfull")
        }

        self.loaderView.isHidden = true
        self.titleView.isHidden = true
        self.signInView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: { self.signInView.alpha = 1 }, completion: nil )
    }
    
    public func showSignUpView() {
        self.loaderView.isHidden = true
        self.titleView.isHidden = true
        self.signUpView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: { self.signUpView.alpha = 1 }, completion: nil )
    }

    private func initViews(_ style: Style){
        superview?.backgroundColor = UIColor.colorFromHex(0x19a1dc)
        backgroundColor = UIColor.colorFromHex(0x19a1dc)
        
        var image: UIImage?
        switch XTargetUtils.target {
        case .security_hub, .dev, .test:
            image = UIImage(named: "new_logo")?.withRenderingMode(.alwaysTemplate)
        default:
            image = UIImage(named: "hub_colorfull")
        }
        
        logoView = UIImageView(image: image)
        logoView.contentMode = .scaleAspectFit
        logoView.tintColor = .white
        addSubview(logoView)
        
        signUpView = XZoomButton(style: style.signUnStyle)
        signUpView.text = strings.signUp
        signUpView.alpha = 0
        signUpView.isHidden = true
        signUpView.addTarget(self, action: #selector(signUpClick), for: .touchUpInside)
        addSubview(signUpView)
     
        signInView = XZoomButton(style: style.signInStyle)
        signInView.text = strings.signIn
        signInView.alpha = 0
        signInView.isHidden = true
        signInView.addTarget(self, action: #selector(signInClick), for: .touchUpInside)
        addSubview(signInView)
        
        loaderView.color = .white
        addSubview(loaderView)
        
//        titleView.text = ""
//        titleView.setStyle(XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 15.5)))
//        addSubview(titleView)

    }
    
    private func setConstraints() {
        logoView.translatesAutoresizingMaskIntoConstraints = false
        signUpView.translatesAutoresizingMaskIntoConstraints = false
        signInView.translatesAutoresizingMaskIntoConstraints = false
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            logoView.heightAnchor.constraint(equalToConstant: 150),
            logoView.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoView.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoView.leadingAnchor.constraint(equalTo: leadingAnchor),
            logoView.trailingAnchor.constraint(equalTo: trailingAnchor),

            signUpView.heightAnchor.constraint(equalToConstant: 40),
            signUpView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 18),
            signUpView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -18),
            signUpView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -30),
            
            signInView.heightAnchor.constraint(equalToConstant: 40),
            signInView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 18),
            signInView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -18),
            signInView.bottomAnchor.constraint(equalTo: signUpView.topAnchor, constant: -4),
            
            loaderView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -80),
            loaderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    @objc private func signUpClick() { delegate?.signUp() }

    @objc private func signInClick() { delegate?.signIn() }
}
