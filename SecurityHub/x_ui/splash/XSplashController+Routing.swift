//
//  XSplashController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import UIKit
import Localize_Swift

extension XSplashController: XSplashViewDelegate, XAlertPersonAgreementControllerDelegate {
    func xRejectAlertControllerPositiveButtonTapped() { exit(0) }
    
    func xRejectAlertControllerNegativeButtonTapped() { showPersonsAgreement() }
    
    func acceptPersonAgreementTapped() { acceptPersonAgreement() }

    func rejectedPersonAgreementTapped() {
        let alert = XAlertController(
            style: secBaseAlertStyle(),
            strings: rejectAlertStrings(),
            positive: xRejectAlertControllerPositiveButtonTapped,
            negative: xRejectAlertControllerNegativeButtonTapped
        )
        navigationController?.present(alert, animated: true)
    }
    
    func showPersonsAgreement() {
        let alert = XAlertPersonAgreementController(delegate: self)
        navigationController?.present(alert, animated: true)
    }
    
    func signUp() {
        /*let alert = XAlertController(
            style: secBaseAlertStyle(),
            strings: signUpAlertStrings(),
            positive: xSignUpAlertControllerPositiveButtonTapped,
            negative: nil
        )
        navigationController?.present(alert, animated: true)*/
				navigationController?.pushViewController(XRegistrationPhoneController(), animated: true)
    }
    
    func xSignUpAlertControllerPositiveButtonTapped() {
        let uri = "\(XTargetUtils.regLink!)?p=100&l=\(DataManager.defaultHelper.language)"
        let controller = XWebPageController(uri: uri)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func signIn() {
        navigationController?.pushViewController(XLoginController(), animated: true)
    }
    
    func showPinCode() {
        NavigationHelper.shared.showNew(XPinCodeController(state: .check))
    }
    
    func showMain() {
        NavigationHelper.shared.showMain()
    }
    
    func serverChanger() {
        navigationController?.pushViewController(XServerChangerViewController(), animated: true)
    }
}
