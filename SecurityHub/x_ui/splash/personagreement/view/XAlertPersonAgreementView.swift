//
//  XAlertPersonAgreementView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertPersonAgreementView: UIView, XCheckBoxViewDelegate {
    public var delegate: XAlertPersonAgreementViewDelegate?
    
    private var scrollView = UIScrollView()
    private var titleView = UILabel()
    private var textView = UILabel()
    private var checkBoxView: XCheckBoxView!
    private var closeView, nextView: XZoomButton!
    private let strings: XAlertPersonAgreementViewStrings = XAlertPersonAgreementViewStrings()

    init() {
        super.init(frame: .zero)
        initViews(style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroungColor
        
        titleView.text = strings.title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = .center
        addSubview(titleView)
        
        addSubview(scrollView)
        
        textView.text = strings.text
        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.numberOfLines = 0
        scrollView.addSubview(textView)
        
        checkBoxView = XCheckBoxView(style.checkBox)
        checkBoxView.text = strings.checkBox
        checkBoxView.delegate = self
        addSubview(checkBoxView)
        
        closeView = XZoomButton(style: style.close)
        closeView.text = strings.close
        closeView.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        addSubview(closeView)

        nextView = XZoomButton(style: style.next)
        nextView.text = strings.next
        nextView.isEnabled = false
        nextView.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
        addSubview(nextView)
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        checkBoxView.translatesAutoresizingMaskIntoConstraints = false
        closeView.translatesAutoresizingMaskIntoConstraints = false
        nextView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            scrollView.heightAnchor.constraint(equalTo: safeAreaLayoutGuide.heightAnchor, multiplier: 0.99, constant: -171),
            
            textView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            textView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            textView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1, constant: -10),
            textView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            checkBoxView.topAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 10),
            checkBoxView.leadingAnchor.constraint(equalTo: leadingAnchor),
            checkBoxView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            checkBoxView.heightAnchor.constraint(equalToConstant: 40),
            
            closeView.topAnchor.constraint(equalTo: checkBoxView.bottomAnchor, constant: 20),
            closeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            closeView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            closeView.heightAnchor.constraint(equalToConstant: 40),
            closeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            nextView.topAnchor.constraint(equalTo: closeView.topAnchor),
            nextView.leadingAnchor.constraint(equalTo: closeView.trailingAnchor),
            nextView.bottomAnchor.constraint(equalTo: closeView.bottomAnchor),
            nextView.widthAnchor.constraint(equalTo: closeView.widthAnchor),
            nextView.heightAnchor.constraint(equalTo: closeView.heightAnchor),
        ])
    }
    
    func xCheckBoxViewChangeState(isSelected: Bool) { nextView.isEnabled = isSelected }
    
    @objc private func closeTapped() { delegate?.xAlertPersonAgreementViewCloseTapped() }
    
    @objc private func nextTapped() { delegate?.xAlertPersonAgreementViewNextTapped() }
}
