//
//  XAlertPersonAgreementController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertPersonAgreementController: XBaseAlertController<XAlertPersonAgreementView> {
    private let delegate: XAlertPersonAgreementControllerDelegate
    
    init(delegate: XAlertPersonAgreementControllerDelegate) {
        self.delegate = delegate
        super.init()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
    }
}

extension XAlertPersonAgreementController: XAlertPersonAgreementViewDelegate {
    func xAlertPersonAgreementViewCloseTapped() {
        dismiss(animated: true) {
            self.delegate.rejectedPersonAgreementTapped()
        }
    }
    
    func xAlertPersonAgreementViewNextTapped() {
        dismiss(animated: true) {
            self.delegate.acceptPersonAgreementTapped()
        }
    }
}
