//
//  XAlertPersonAgreementViewStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAlertPersonAgreementView {
    func style() -> Style {
        return Style(
            backgroungColor: UIColor.white,
            title: Style.Lable(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20)
            ),
            text: Style.Lable(
                color: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 12)
            ),
            checkBox: XCheckBoxView.Style(
                icon: XCheckBoxView.Style.Icon(
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                    unselect: UIColor(red: 0xa7/255, green: 0xa9/255, blue: 0xac/255, alpha: 1)
                ),
                text: XCheckBoxView.Style.Text(
                    select: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    unselect: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5)
                )
            ),
            close: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            next: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                disable: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                disableBackground: UIColor.clear
            )
        )
    }
}
