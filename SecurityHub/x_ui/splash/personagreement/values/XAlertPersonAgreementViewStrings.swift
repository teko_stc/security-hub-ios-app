//
//  File.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XAlertPersonAgreementViewStrings {
    
    /// ЛИЦЕНЗИОННОЕ СОГЛАШЕНИЕ
    var title: String { "LICENSE".localized() }
    
    /// ОПИСАНИЕ ЛИЦЕНЗИОННОЕ СОГЛАШЕНИЕ
    var text: String { "LICENSE_TEXT".localized() }
    
    /// I accept the terms of the license agreement
    var checkBox: String { "LIC_AGREE".localized() }
    
    /// Close
    var close: String { "CLOSE_POPUP".localized() }
    
    /// Next
    var next: String { "WIZ_NEXT".localized() }
}
