//
//  XConnectionsViewController.swift
//  SecurityHub
//
//  Created by Рахматуллин Тимерлан on 29.10.2023.
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit
import Localize_Swift

final class XConnectionsViewController: XBaseViewController<XConnectionsView> {
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 22), alignment: .left)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "N_PROFILE_CONNECTION".localized()

        setNavigationViewBuilder(navigationViewBuilder)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabGesture)))
    }
    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        let alertStrings = XAlertView.Strings(title: "ios_server_changer_warning".localized(), text: nil, positive: "Ok".localized())
        let alertVC = XAlertController(style: baseAlertStyle(), strings: alertStrings, positive: { })
        present(alertVC, animated: true, completion: nil)
    }
}

final class XConnectionsView: UIView {
    
    private let label =  UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        label.setStyle(XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18)))
        label.numberOfLines = 0
        addSubview(label)
        label.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(20.0)
        }
        
        NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: .main) { [weak self] notification in
            let code = notification.object as? Int ?? 0
            self?.doit(isAlive: code >= 0)
        }

        doit(isAlive: DataManager.shared.hubHelper.isAlive())
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
            
    private func doit(isAlive: Bool) {
        label.text = "\("ios_server_settings".localized()): \(XServerChanger.item.name)\n\("N_TITLE_SERVER_ADDRESS".localized()): \(XServerChanger.item.url)\n\("N_TITLE_SERVER_PORT".localized()): \(XServerChanger.item.port)\n\("ios_server_status".localized()): \(isAlive ? "ios_server_status_active".localized() : "ios_server_status_not_active".localized())"
    }
}
    
