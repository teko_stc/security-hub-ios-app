//
//  XBaseTabBarColors.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XBaseTabBarColors {
    var defaultBackground: UIColor { get }
    var defaultSelect: UIColor { get }
    var defaultUnselect: UIColor { get }
    
    var alarmBackground: UIColor { get }
    var alarmSelect: UIColor { get }
    var alarmUnselect: UIColor { get }
    
    var warningBackground: UIColor { get }
    var warningSelect: UIColor { get }
    var warningUnselect: UIColor { get }
    
    var tabBarBorder: UIColor { get }
}

