//
//  XTabBarItem.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBaseTabBarItem {
    var viewController: UIViewController
    var imageName: String
}
