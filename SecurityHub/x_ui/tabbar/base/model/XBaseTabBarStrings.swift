//
//  XBaseTabBarStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XBaseTabBarStrings {
    var alarm_state_title: String { get }
    var warning_state_title: String { get }
}
