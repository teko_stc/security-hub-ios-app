//
//  XBaseTabBarController.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 23.06.2021.
//

import Foundation
import UIKit

class XBaseTabBarController: UITabBarController {
    public var xBaseTabBarDataSource: XBaseTabBarDataSource?
    public var xBaseTabBarDelegate: XBaseTabBarDelegate?
    private var xBaseTabBarTopView: XBaseTabBarTopView?
    private var xBaseTabBarState = -1
    private lazy var y = self.tabBar.frame.origin.y
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.isTranslucent = false
        delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
				if xBaseTabBarTopView != nil {
					let _h = xBaseTabBarTopView?.frame.size.height ?? 0
					let _w = tabBar.frame.size.width, _y = tabBar.frame.origin.y - _h
					xBaseTabBarTopView?.frame = CGRect(x: 0, y: _y, width: _w, height: _h)
					xBaseTabBarTopView?.updateConstraints()
					return }
        xBaseTabBarTopView = XBaseTabBarTopView(frame: .zero, font: xBaseTabBarDataSource?.xBaseTabBarFont, borderColor: xBaseTabBarDataSource?.xBaseTabBarColors.tabBarBorder)
        xBaseTabBarTopView!.buttonClick = xBaseTabBarTopViewAlarmButtonClick
        view.addSubview(xBaseTabBarTopView!)
        UIView.performWithoutAnimation {
            switch self.xBaseTabBarState {
            case 1:     return self.xBaseTabBarShowWarningState()
            case 2:     return self.xBaseTabBarShowAlarmState()
            default:    return self.xBaseTabBarShowDefaultState()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initControllers(hardReset: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    public func initControllers(hardReset: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        if (self.viewControllers?.count == xBaseTabBarDataSource?.xBaseTabBarItems.count), !hardReset { return }
        var controllers: [UIViewController] = []
        xBaseTabBarDataSource?.xBaseTabBarItems.forEach { item in
            item.viewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: item.imageName), selectedImage: UIImage(named: item.imageName))
						if #available(iOS 15.0, *) {
							item.viewController.tabBarItem.imageInsets = UIEdgeInsets(top: -8, left: -5, bottom: -2, right: -5) } else {
								item.viewController.tabBarItem.imageInsets = UIEdgeInsets(top: -1, left: -1, bottom: -1, right: -1) }
            controllers.append(item.viewController)
        }
        self.viewControllers = controllers
    }
    
    public func xBaseTabBarShowDefaultState() {
        self.xBaseTabBarState = 0
        guard let colors = xBaseTabBarDataSource?.xBaseTabBarColors, let xBaseTabBarTopView = xBaseTabBarTopView else { return }
        UIView.transition(with: xBaseTabBarTopView, duration: 0.3, options: .curveEaseInOut, animations: {
            self.xBaseTabBarUpdateState(topViewHeight: 5, backgroundColor: colors.defaultBackground, selectColor: colors.defaultSelect, unselectColor: colors.defaultUnselect, steteColor: .clear, stateText: nil)
        })
    }
    
    public func xBaseTabBarShowWarningState() {
        self.xBaseTabBarState = 1
        guard let colors = xBaseTabBarDataSource?.xBaseTabBarColors, let strings = xBaseTabBarDataSource?.xBaseTabBarStrings, let xBaseTabBarTopView = xBaseTabBarTopView else { return }
        UIView.transition(with: xBaseTabBarTopView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.xBaseTabBarUpdateState(topViewHeight: 60, backgroundColor: colors.warningBackground, selectColor: colors.warningSelect, unselectColor: colors.warningUnselect, steteColor: colors.warningSelect, stateText: strings.warning_state_title)
        })
    }
    
    public func xBaseTabBarShowAlarmState() {
        self.xBaseTabBarState = 2
        guard let colors = xBaseTabBarDataSource?.xBaseTabBarColors, let strings = xBaseTabBarDataSource?.xBaseTabBarStrings, let xBaseTabBarTopView = xBaseTabBarTopView else { return }
        UIView.transition(with: xBaseTabBarTopView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.xBaseTabBarUpdateState(topViewHeight: 60, backgroundColor: colors.alarmBackground, selectColor: colors.alarmSelect, unselectColor: colors.alarmUnselect, steteColor: colors.alarmUnselect, stateText: strings.alarm_state_title)
        })
    }
    
    func setBaseTabBarHidden(_ isHidden: Bool) {
        tabBar.isHidden = isHidden
        xBaseTabBarTopView?.isHidden = isHidden
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + (isHidden ? self.tabBar.frame.size.height : 0) )
    }
    
    func setXBaseTabBarTopViewHidden(_ isHidden: Bool) {
        xBaseTabBarTopView?.isHidden = isHidden
    }
    
    private func xBaseTabBarUpdateState(topViewHeight _h: CGFloat, backgroundColor: UIColor, selectColor: UIColor, unselectColor: UIColor, steteColor: UIColor, stateText: String?) {
        if #available(iOS 15.0, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = backgroundColor
            
            updateTabBarItemAppearance(appearance.compactInlineLayoutAppearance, selectColor: selectColor, unselectColor: unselectColor)
            updateTabBarItemAppearance(appearance.inlineLayoutAppearance, selectColor: selectColor, unselectColor: unselectColor)
            updateTabBarItemAppearance(appearance.stackedLayoutAppearance, selectColor: selectColor, unselectColor: unselectColor)
             
            tabBar.standardAppearance = appearance
            tabBar.scrollEdgeAppearance = appearance
        }
        tabBar.barTintColor = backgroundColor
        tabBar.tintColor = selectColor
        tabBar.unselectedItemTintColor = unselectColor
        let attr = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 0.1)]
        UITabBarItem.appearance().setTitleTextAttributes(attr, for: .normal)
        let _w = tabBar.frame.size.width, _y = self.y - _h
        xBaseTabBarTopView?.frame = CGRect(x: 0, y: _y, width: _w, height: _h)
//        viewControllers?.forEach({ item in
//            item.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: UIScreen.main.bounds.height - _y, right: 0)
//            print(String(describing: item.additionalSafeAreaInsets))
//        })
        xBaseTabBarTopView?.updateViewState(stateText: stateText, textColor: steteColor, stateColor: backgroundColor)
    }
    
    @available(iOS 13.0, *)
    private func updateTabBarItemAppearance(_ appearance: UITabBarItemAppearance, selectColor: UIColor, unselectColor: UIColor) {
        appearance.selected.iconColor = selectColor
        appearance.normal.iconColor = unselectColor
    }
    
    private func xBaseTabBarTopViewAlarmButtonClick() {
        switch xBaseTabBarState {
        case 1: xBaseTabBarDelegate?.xBaseTabBarWarningButtonClick?(); break;
        case 2: xBaseTabBarDelegate?.xBaseTabBarAlarmButtonClick?(); break;
        default: break;
        }
    }
}

extension XBaseTabBarController: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let indexOfTab = tabBar.items?.firstIndex(of: item), let viewController = xBaseTabBarDataSource?.xBaseTabBarItems[indexOfTab].viewController else { return }
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        xBaseTabBarDelegate?.xBaseTabBar?(selectedIndex: indexOfTab, shouldSelect: viewController)
    }
}
