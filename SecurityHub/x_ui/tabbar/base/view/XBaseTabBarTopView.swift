//
//  XTabBarTopView.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 24.06.2021.
//

import UIKit

class XBaseTabBarTopView: UIView {
    public var buttonClick: (() -> ())?
    private var defaultView, alarmView: UIView
    private var alarmButton: UIButton
    private var layer1, layer2, layer3: CALayer

    init(frame: CGRect, font: UIFont?, borderColor: UIColor?) {
        let defaulFrame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)

        defaultView = UIView(frame: defaulFrame)
        defaultView.isHidden = true
        layer1 = CALayer()
        layer1.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: 1)
        layer1.backgroundColor = borderColor?.cgColor
        defaultView.layer.addSublayer(layer1)
        
        alarmView = UIView(frame: defaulFrame)
        alarmView.isHidden = true
        layer2 = CALayer()
        layer2.frame = defaulFrame
        layer2.cornerRadius = 30
        layer2.backgroundColor = UIColor.black.cgColor
        alarmView.layer.addSublayer(layer2)
        layer3 = CALayer()
        layer3.frame = CGRect(x: 0, y: frame.size.height/2, width: frame.size.width, height: frame.size.height/2)
        alarmView.layer.addSublayer(layer3)
        
        alarmButton = UIButton(frame: defaulFrame)
        alarmButton.titleLabel?.font = font
        alarmButton.setTitle("defaul", for: .normal)
        alarmButton.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        alarmButton.contentEdgeInsets = UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0)
        alarmButton.isHidden = true
        
        super.init(frame: frame)
        addSubview(defaultView)
        addSubview(alarmView)
        addSubview(alarmButton)
        
        alarmButton.addTarget(self, action: #selector(alarmButtonClick), for: UIControl.Event.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
				let defaulFrame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)

        defaultView.frame = defaulFrame
        layer1.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: 1)

        alarmView.frame = defaulFrame
        layer2.frame = defaulFrame
				layer3.frame = CGRect(x: 0, y: frame.size.height/2, width: frame.size.width, height: frame.size.height/2)
        
        alarmButton.frame = defaulFrame
    }
    
    public func updateViewState(stateText text: String?, textColor: UIColor?, stateColor: UIColor) {
        if let text = text, let textColor = textColor {
            UIView.performWithoutAnimation {
                self.alarmButton.setTitle(text, for: UIControl.State.normal)
                self.alarmButton.setTitleColor(textColor, for: UIControl.State.normal)
            }
        }
        defaultView.layer.backgroundColor = stateColor.cgColor
        layer2.backgroundColor = stateColor.cgColor
        layer3.backgroundColor = stateColor.cgColor
        defaultView.isHidden = text != nil
        alarmView.isHidden = text == nil
        alarmButton.isHidden = text == nil
        updateConstraints()
    }
    
    @objc private func alarmButtonClick() {
        buttonClick?()
    }
}
