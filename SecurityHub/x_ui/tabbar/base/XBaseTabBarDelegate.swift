//
//  XTabBarDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 23.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

@objc protocol XBaseTabBarDelegate {
    @objc optional func xBaseTabBar(selectedIndex index: Int, shouldSelect viewController: UIViewController)
    
    @objc optional func xBaseTabBarAlarmButtonClick()
    
    @objc optional func xBaseTabBarWarningButtonClick()
}
