//
//  XTabBarDataSource.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import UIKit

protocol XBaseTabBarDataSource {
    var xBaseTabBarItems: [XBaseTabBarItem] { get set }
    var xBaseTabBarColors: XBaseTabBarColors { get }
    var xBaseTabBarStrings: XBaseTabBarStrings { get }
    var xBaseTabBarFont: UIFont? { get }
}

