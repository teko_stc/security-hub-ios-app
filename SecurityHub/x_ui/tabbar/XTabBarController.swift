//
//  XTabBarController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XTabBarController: XBaseTabBarController, XBaseTabBarDataSource {
    
    var xBaseTabBarStrings: XBaseTabBarStrings { XTabBarStrings() }
    
    var xBaseTabBarColors: XBaseTabBarColors { XTabBarColors() }

    var xBaseTabBarFont: UIFont? { UIFont(name: "Open Sans", size: 25) }
    
    var xBaseTabBarItems: [XBaseTabBarItem]
    
    lazy var alarmMonitorRepository = XAlarmMonitorRepository(hubHelper: DataManager.shared.hubHelper)
    var disposable: Disposable?
    
    init(){
        let dashboard = DataManager.settingsHelper.needSharedMain ? SharedDashboardViewController() : DashboardViewController()
        xBaseTabBarItems = [
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : dashboard), imageName: "ic_main"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XDevicePartitionMenuController()), imageName: "ic_devices"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XEventsController()), imageName: "ic_events"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : DataManager.defaultHelper.login == "tim" ? XBaseViewController<UIView>() : XCamerasMainController()), imageName: "ic_camera"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XMenuController()), imageName: "ic_menu")
        ]
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        xBaseTabBarDataSource = self
        xBaseTabBarDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updatedNeedAlarmMonitor), name: NSNotification.Name("DataManager.settingsHelper.needAlarmMonitor"), object: nil)
//        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updatedNeedAlarmMonitor), name: NSNotification.Name("DashboardPrioritySiteIdUpdate"), object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updatedNeedAlarmMonitorDevice), name: HubNotification.deviceUpdate, object: nil)
        DataManager.shared.nCenter.addObserver(self, selector: #selector(self.updateTabBar), name: NSNotification.Name(rawValue: "DataManager.settingsHelper.needSharedMain"), object: nil)
        self.updateAlarmMonitorViewContent(isNeedMonitor: DataManager.settingsHelper.needAlarmMonitor)
    }
    
    @objc private func updatedNeedAlarmMonitor(_ n: Notification) {
        if let value = n.object as? Bool { self.updateAlarmMonitorViewContent(isNeedMonitor: value) }
        else { self.updateAlarmMonitorViewContent(isNeedMonitor: DataManager.settingsHelper.needAlarmMonitor) }
    }
    
    @objc private func updatedNeedAlarmMonitorDevice(_ n: Notification) {
        guard let nDevice = n.object as? NDeviceEntity, nDevice.type != .update else {
            return
        }
        
        self.updateAlarmMonitorViewContent(isNeedMonitor: DataManager.settingsHelper.needAlarmMonitor)
    }
    
    @objc private func updateTabBar(_ n: Notification) {
        guard let value = n.object as? Bool else {
            return
        }
        
        let dashboard = value ? SharedDashboardViewController() : DashboardViewController()
        xBaseTabBarItems = [
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : dashboard), imageName: "ic_main"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XDevicePartitionMenuController()), imageName: "ic_devices"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XEventsController()), imageName: "ic_events"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : DataManager.defaultHelper.login == "tim" ? XBaseViewController<UIView>() : XCamerasMainController()), imageName: "ic_camera"),
            XBaseTabBarItem(viewController: UINavigationController(rootViewController : XMenuController()), imageName: "ic_menu")
        ]
        initControllers(hardReset: true)
    }
    
    private func updateAlarmMonitorViewContent(isNeedMonitor: Bool) {
        if isNeedMonitor {
            disposable?.dispose()
            disposable = alarmMonitorRepository.needAlarmSubject
                .observeOn(ThreadUtil.shared.mainScheduler)
                .bind(onNext: { [weak self] needAlarm in
                        if needAlarm { self?.xBaseTabBarShowAlarmState() } else { self?.xBaseTabBarShowDefaultState() }
                })
        } else {
            disposable?.dispose()
            disposable = DataManager.shared.getAlarmAffectsCount()
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { [weak self] count, isAlarm in
                    if isAlarm {
                        self?.xBaseTabBarShowAlarmState()
                    } else if count > 0 {
                        self?.xBaseTabBarShowWarningState()
                    } else {
                        self?.xBaseTabBarShowDefaultState()
                    }
                })
        }
    }
}

extension XTabBarController: XBaseTabBarDelegate {
    func xBaseTabBarAlarmButtonClick() {
        if (DataManager.settingsHelper.needAlarmMonitor) {
            let controller = XGroupedAlarmsController(alarmMonitorRepository: alarmMonitorRepository)
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = XAlarmAffectsController()
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func xBaseTabBarWarningButtonClick() {
        let controller = XAlarmAffectsController()
        navigationController?.pushViewController(controller, animated: true)
    }
}
