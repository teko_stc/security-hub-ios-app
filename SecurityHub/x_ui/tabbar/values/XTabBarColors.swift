//
//  XTabBarColors.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XTabBarColors: XBaseTabBarColors {
    var defaultBackground: UIColor { UIColor.white }
    var defaultSelect: UIColor { UIColor(red: 58/255, green: 190/255, blue: 255/255, alpha: 1) }
    var defaultUnselect: UIColor { UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1) }
    
    var alarmBackground: UIColor { UIColor(red: 249/255, green: 84/255, blue: 62/255, alpha: 1) }
    var alarmSelect: UIColor { UIColor(red: 58/255, green: 190/255, blue: 255/255, alpha: 1) }
    var alarmUnselect: UIColor { UIColor.white }
    
    var warningBackground: UIColor { UIColor(red: 255/255, green: 223/255, blue: 87/255, alpha: 1) }
    var warningSelect: UIColor { UIColor(red: 65/255, green: 64/255, blue: 66/255, alpha: 1) } // { UIColor(red: 58/255, green: 190/255, blue: 255/255, alpha: 1) }
    var warningUnselect: UIColor { .white } //{ UIColor(red: 65/255, green: 64/255, blue: 66/255, alpha: 1) }
    
    var tabBarBorder: UIColor { UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1) }
}
