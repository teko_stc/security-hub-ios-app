//
//  XTabBarStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.06.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import Localize_Swift

class XTabBarStrings: XBaseTabBarStrings {
    /// Тревога
    var alarm_state_title: String { "N_BOTTOM_STATE_ALARM".localized() }
    
    /// Критическое событие
    var warning_state_title: String { "N_BOTTOM_STATE_CRIT".localized() }
}
