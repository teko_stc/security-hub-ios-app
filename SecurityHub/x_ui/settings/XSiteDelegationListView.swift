//
//  XSiteDelegationListView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSiteDelegationListViewLayerModel {
    let id: Int64
    let userLogin: String
    let domain: String
    let delegated_domain: Int64
}

protocol XSiteDelegationListViewDelegate {
    func delegationViewTapped(model: XSiteDelegationListViewLayerModel)
}

protocol XSiteDelegationListViewLayer {
    func siteDelegationUpdate(models: [XSiteDelegationListViewLayerModel])
}

protocol XSiteDelegationListViewStringsProtocol {
    var listEmpty: String { get }
}

protocol XSiteDelegationListViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var emptyView: XBaseLableStyle { get }
    var cell: XSiteDelegationListViewCellStyleProtocol { get }
}

class XSiteDelegationListView: UIView {
    public var delegate: XSiteDelegationListViewDelegate?
    
    private var emptyView: UILabel!
    private var tableView: UITableView!
    
    private lazy var style: XSiteDelegationListViewStyleProtocol = XSiteDelegationListViewStyle()
    private lazy var strings: XSiteDelegationListViewStringsProtocol = XSiteDelegationListViewStrings()
    
    private var items: [XSiteDelegationListViewLayerModel] = []
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.register(XSiteDelegationListViewCell.self, forCellReuseIdentifier: XSiteDelegationListViewCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
        
        emptyView = UILabel.create(style.emptyView)
        emptyView.text = strings.listEmpty
        emptyView.numberOfLines = 0
        addSubview(emptyView)
        
        [emptyView, tableView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            emptyView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            emptyView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension XSiteDelegationListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XSiteDelegationListViewCell.identifier, for: indexPath) as! XSiteDelegationListViewCell
        cell.setContent(model: items[indexPath.row])
        cell.setStyle(style.cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.delegationViewTapped(model: items[indexPath.row])
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        tableView.deselectRow(at: indexPath, animated: true)
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { UIView(frame: CGRect(origin: .zero, size: CGSize(width: 0, height: 0))) }
}

extension XSiteDelegationListView: XSiteDelegationListViewLayer {
    func siteDelegationUpdate(models: [XSiteDelegationListViewLayerModel]) {
        self.items = models
        tableView.reloadData()
        emptyView.isHidden = self.items.count != 0
    }
}
