//
//  XApnResetControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XApnResetView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: XBaseLableStyle(
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20)
            ),
            info: XBaseLableStyle(
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            button: XZoomButtonStyle(
                backgroundColor: UIColor(red: 0xF9/255, green: 0x54/255, blue:  0x3E/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor.white
            )
        )
    }
}
