//
//  XApnResetControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

class XApnResetViewStrings {
    func title(deviceName: String) -> String {
        return "Сбросить \(deviceName) на заводские настройки?"
    }
    
    var info: String { "Контроллер будет сброшен на заводские настройки. Настройки разделов, зон и пользователей устройства буду утеряны безвозвратно. Локальные настройки останутся без изменений." }
    var reset: String { "Сбросить" }
}
