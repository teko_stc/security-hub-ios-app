//
//  XApnResetController.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation
import UIKit

class XApnResetController: XBaseViewController<XApnResetView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceName: String
    private var onResetTapped: () -> Void
    
    init(deviceName: String, onResetTapped: @escaping () -> Void) {
        self.deviceName = deviceName
        self.onResetTapped = onResetTapped
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationViewBuilder(navigationViewBuilder)

        mainView.delegate = self
        mainView.setTitle(deviceName)
    }
}

extension XApnResetController: XApnResetViewDelegate {
    func resetTapped() {
        onResetTapped()
        
        back()
    }
}
