//
//  XApnResetController.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XApnResetView: UIView {
    
    private let strings: XApnResetViewStrings = XApnResetViewStrings()
    
    public var delegate: XApnResetViewDelegate!
    
    private var titleView: UILabel!
    private var infoView: UILabel!
    private var resetButton: XZoomButton!
    
    init() {
        super.init(frame: .zero)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        titleView = UILabel.create(style.title)
        titleView.text = "Сбросить Security Hub на заводские настройки?"
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        infoView = UILabel.create(style.info)
        infoView.text = strings.info
        infoView.numberOfLines = 0
        addSubview(infoView)
        
        resetButton = XZoomButton(style: style.button)
        resetButton.text = strings.reset
        resetButton.addTarget(self, action: #selector(resetTapped), for: .touchUpInside)
        addSubview(resetButton)
    }
    
    private func setConstraints() {
        [titleView, infoView, resetButton].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            
            infoView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 60),
            infoView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            infoView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor),
            
            resetButton.heightAnchor.constraint(equalToConstant: 40),
            resetButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -42),
            resetButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 94),
            resetButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -94)
        ])
    }
    
    @objc private func resetTapped() {
        delegate.resetTapped()
    }
    
    public func setTitle(_ deviceName: String) {
        titleView.text = strings.title(deviceName: deviceName)
    }
}
