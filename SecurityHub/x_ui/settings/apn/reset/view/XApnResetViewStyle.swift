//
//  XApnResetViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XApnResetViewStyle {
    let backgroundColor: UIColor
    
    let title: XBaseLableStyle
    let info: XBaseLableStyle
    let button: XZoomButtonStyle
}

extension XApnResetView {
    typealias Style = XApnResetViewStyle
}
