//
//  XApnResetViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 22.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

protocol XApnResetViewDelegate {
    func resetTapped()
}
