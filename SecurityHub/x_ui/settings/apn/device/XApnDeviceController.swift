//
//  XApnDeviceController.swift
//  SecurityHub
//
//  Created by Daniil on 13.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation
import UIKit

class XApnDeviceController: XBaseViewController<XApnDeviceView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        ),
        titleView: XBaseLableStyle(
            color: UIColor.colorFromHex(0x414042),
            font: UIFont(name: "Open Sans", size: 25)
        ),
        subTitleView: XBaseLableStyle(
            color: UIColor.colorFromHex(0x414042),
            font: UIFont(name: "Open Sans", size: 15.5)
        )
    )
    
    public let strings: XApnDeviceControllerStrings = XApnDeviceControllerStrings()
    private let connection: LocalConnection = LocalConnection.instance
    
    public var config: BaseConfig!
    public var isEditingConfig: Bool = false
    
    public var device: LocalDevice
    
    init(device: LocalDevice) {
        self.device = device
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = device.name
        subTitle = device.serialNumber
        setNavigationViewBuilder(navigationViewBuilder)
        
        configConnection()
        sendConfigRequest()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isEditingConfig {
            isEditingConfig = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if !isEditingConfig {
            sendTestModeRequest(state: .off)
        }
    }
    
    private func configConnection() {
        connection.debugEnabled(false)
        
        connection.setHandlers { data, host in
            DispatchQueue.main.async {
                self.onReceive(data, host: host)
            }
        } errorHandler: { error in
            print("Local connection error: \(error)")
        }
    }
    
    private func onReceive(_ data: Data, host: String) {
        var bytesArray = BytesUtils.uInt8ArrayToInt8([UInt8] (data))
        
        if (bytesArray[0] & 0x7F) == ConfigRequest.command {
            bytesArray.removeFirst()
            
            switch bytesArray.count + 1 {
            case ConfigConstants.hubVersion1:
                config = HubConfig1(config: bytesArray)
            case ConfigConstants.hubVersion2:
                config = HubConfig2(config: bytesArray)
            case ConfigConstants.hubVersion3:
                config = HubConfig3(config: bytesArray)
            case ConfigConstants.hubVersion4:
                config = HubConfig4(config: bytesArray)
            default:
                return
            }
            
            loadDeviceConfig()
        }
    }
    
    private func sendConfigRequest() {
        connection.sendData(
            host: device.ip,
            data: ConfigRequest(pin: device.pin).toData()
        )
    }
    
    private func sendTestModeRequest(state: TestModeRequest.TestModeState) {
        connection.sendData(
            host: device.ip,
            data: TestModeRequest(pin: device.pin, state: state).toData()
        )
    }
    
    public func sendResetRequest() {
        connection.sendData(
            host: device.ip,
            data: ResetRequest(pin: device.pin).toData()
        )
    }
    
    public func sendSetConfigRequest() {
        connection.sendData(
            host: device.ip,
            data: SetConfigRequest(pin: device.pin, config: config.config).toData()
        )
    }
}
