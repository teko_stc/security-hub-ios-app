//
//  XApnDeviceControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 13.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XApnDeviceView {
    func viewStyles() -> Style {
        return Style(backgroundColor: UIColor.white)
    }
    
    func selectorViewCellStyle() -> XSelectorView.Cell.Style {
        return XSelectorView.Cell.Style(
            backgroundColor: UIColor.white,
            selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
            title: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                margins: UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
            ),
            subtitle: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                margins: UIEdgeInsets(top: 0, left: 24, bottom: 12, right: 24)
            ),
            icon: XSelectorViewCell.Style.Icon(
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                size: CGSize(width: 0, height: 0),
                margins: UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
            )
        )
    }
    
    func selectorViewEmptyCellStyle() -> XSelectorView.Cell.Style {
        return XSelectorView.Cell.Style(
            backgroundColor: UIColor.white,
            selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
            title: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                margins: UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
            ),
            subtitle: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xA7/255, green: 0xA9/255, blue:  0xAC/255, alpha: 1),
                margins: UIEdgeInsets(top: 0, left: 24, bottom: 12, right: 24)
            ),
            icon: XSelectorViewCell.Style.Icon(
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                size: CGSize(width: 0, height: 0),
                margins: UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
            )
        )
    }
    
    func selectorViewActionCellStyle() -> XSelectorView.Cell.Style {
        return XSelectorView.Cell.Style(
            backgroundColor: UIColor.white,
            selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
            title: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xF9/255, green: 0x54/255, blue:  0x3E/255, alpha: 1),
                margins: UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
            ),
            icon: XSelectorViewCell.Style.Icon(
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                size: CGSize(width: 0, height: 0),
                margins: UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
            )
        )
    }
    
    func selectorViewHeaderStyle() -> XSelectorView.Header.Style {
        return XSelectorView.Header.Style(
            backgroundColor: UIColor.white,
            title: XSelectorView.Header.Style.Title(
                font: UIFont(name: "Open Sans", size: 11.3),
                color: UIColor(red: 0xA7/255, green: 0xA9/255, blue:  0xAC/255, alpha: 1),
                margins: UIEdgeInsets(top: 16, left: 24, bottom: 4, right: 24)
            )
        )
    }
}
