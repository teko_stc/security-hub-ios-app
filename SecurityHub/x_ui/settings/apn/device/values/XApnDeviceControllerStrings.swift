//
//  XApnDeviceControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 26.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class XApnDeviceControllerStrings {
    var allow: String { "Разрешить" }
    var disallow: String { "Запретить" }
    var dialogPositive: String { "Изменить" }
    var dialogNegative: String { "Отмена" }
    
    var incorrectIP: String { "Неверный IP" }
    var incorrectPin: String { "Неверный PIN-код" }
    var incorrectTemp: String { "Недопустимое значение" }
    var onlyLatinError: String { "Должны быть только латинские символы" }
    
    var apnTitle: String { "Настройка APN" }
    var apn2Title: String { "Настройка APN2" }
    var otherSettingsTitle: String { "Другие настройки" }
    var tempTitle: String { "Температура" }
    var actionsTitle: String { "Действия" }
    
    var apnServer: String { "APN сервер" }
    var apnLogin: String { "APN логин" }
    var apnPassword: String { "APN пароль" }
    
    var apn2Server: String { "APN2 сервер" }
    var apn2Login: String { "APN2 логин" }
    var apn2Password: String { "APN2 пароль" }
    
    var pinCode: String { "PIN-код устройства" }
    var reservePort: String { "Резервный порт сервера" }
    var mainPort: String { "Основной порт сервера" }
    var radioOffset: String { "Сдвиг частот" }
    var serverIP: String { "IP-адрес сервера" }
    var gatewayIp: String { "Основной шлюз" }
    var maskIp: String { "Маска подсети" }
    var staticIP: String { "Статический IP контроллера" }
    var reserveIP: String { "Резервный IP-адрес сервера" }
    var radioLiter: String { "Частотная литера радиомодуля" }
    var waitReviewed: String { "Взятие при необработанной тревоге" }
    var armServer: String { "Взятие при отсутствии связи" }
    var disarmServer: String { "Снятие при отсутствии связи" }
    var roaming: String { "Работа в роуминге" }
    var entryDelay: String { "Задержка на вход" }
    var exitDelay: String { "Задержка на выход" }
    
    var minTemp: String { "Минимальная температура раздела " }
    var maxTemp: String { "Максимальная температура раздела " }
    
    var reset: String { "Сброс на заводские настройки" }
}

class XApnDeviceViewStrings {
    var noData: String { "Не задано" }
}
