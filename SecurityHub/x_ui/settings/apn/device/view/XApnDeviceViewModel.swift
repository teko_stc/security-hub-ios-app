//
//  XApnDeviceViewModel.swift
//  SecurityHub
//
//  Created by Daniil on 26.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

struct XApnDeviceViewModel {
    struct Item {
        let title: String
        var value: String?
        let click: (() -> ())?
    }
    
    let title: String
    let items: [Item]
}

extension XApnDeviceView {
    typealias Model = XApnDeviceViewModel
}
