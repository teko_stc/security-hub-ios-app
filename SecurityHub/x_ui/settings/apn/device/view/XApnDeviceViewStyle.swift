//
//  XApnDeviceViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 13.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XApnDeviceViewStyle {
    let backgroundColor: UIColor
}

extension XApnDeviceView {
    typealias Style = XApnDeviceViewStyle
}
