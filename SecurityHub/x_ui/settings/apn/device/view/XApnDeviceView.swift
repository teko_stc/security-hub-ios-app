//
//  XApnDeviceView.swift
//  SecurityHub
//
//  Created by Daniil on 13.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XApnDeviceView: UIView {
    
    private let strings: XApnDeviceViewStrings = XApnDeviceViewStrings()
    
    private var config: [Model] = []
    
    private var selectorView: XSelectorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(viewStyles())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        selectorView = XSelectorView(
            headerStyle: selectorViewHeaderStyle(),
            cellStyle: selectorViewCellStyle()
        )
        addSubview(selectorView)
    }
    
    private func setConstraints() {
        selectorView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            selectorView.topAnchor.constraint(equalTo: topAnchor),
            selectorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            selectorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            selectorView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -31)
        ])
    }
    
    public func setConfig(_ config: [Model]) {
        self.config = config
        
        updateConfig()
    }
    
    private func updateConfig() {
        var sections: [XSelectorView.Section] = []
        
        for section in self.config {
            var items: [XSelectorView.Section.Item] = []
            
            for item in section.items {
                let value = item.value
                
                items.append(
                    XSelectorView.Section.Item(
                        title: item.title,
                        subtitle: (value == nil) ? (nil) : (value!.isEmpty ? strings.noData : value),
                        imageName: "ic_nil",
                        click: item.click,
                        customStyle: value == nil ? selectorViewActionCellStyle() : value!.isEmpty ? selectorViewEmptyCellStyle() : nil
                    )
                )
            }
            
            sections.append(
                XSelectorView.Section(
                    title: section.title,
                    items: items
                )
            )
        }
        
        selectorView.reloadData(sections)
    }
}
