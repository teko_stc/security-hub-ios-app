//
//  XApnDeviceController+LoadDevices.swift
//  SecurityHub
//
//  Created by Daniil on 20.02.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

extension XApnDeviceController {
    
    public func loadDeviceConfig() {
        var configItems = [
            XApnDeviceView.Model(
                title: strings.apnTitle,
                items: [
                    XApnDeviceView.Model.Item(
                        title: strings.apnServer,
                        value: config.apnServer,
                        click: { [self] in showTextEditController(title: strings.apnServer, type: ConfigConstants.apnServer, value: config.apnServer) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.apnLogin,
                        value: config.apnLogin,
                        click: { [self] in showTextEditController(title: strings.apnLogin, type: ConfigConstants.apnLogin, value: config.apnLogin) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.apnPassword,
                        value: config.apnPassword,
                        click: { [self] in showTextEditController(title: strings.apnPassword, type: ConfigConstants.apnPassword, value: config.apnPassword) }
                    ),
                ]
            ),
            XApnDeviceView.Model(
                title: strings.otherSettingsTitle,
                items: [
                    XApnDeviceView.Model.Item(
                        title: strings.pinCode,
                        value: config.pinCode,
                        click: { [self] in showTextEditController(title: strings.pinCode, type: ConfigConstants.pinCode, value: config.pinCode) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.reservePort,
                        value: config.reservePort,
                        click: { [self] in showTextEditController(title: strings.reservePort, type: ConfigConstants.reservePort, value: config.reservePort) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.mainPort,
                        value: config.mainPort,
                        click: { [self] in showTextEditController(title: strings.mainPort, type: ConfigConstants.mainPort, value: config.mainPort) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.radioOffset,
                        value: config.radioOffset,
                        click: { [self] in showTextEditController(title: strings.radioOffset, type: ConfigConstants.radioOffset, value: config.radioOffset) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.serverIP,
                        value: config.serverIp,
                        click: { [self] in showTextEditController(title: strings.serverIP, type: ConfigConstants.serverIp, value: config.serverIp) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.gatewayIp,
                        value: config.gatewayIp,
                        click: { [self] in showTextEditController(title: strings.gatewayIp, type: ConfigConstants.gatewayIp, value: config.gatewayIp) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.maskIp,
                        value: config.maskIp,
                        click: { [self] in showTextEditController(title: strings.maskIp, type: ConfigConstants.maskIp, value: config.maskIp) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.staticIP,
                        value: config.staticIp,
                        click: { [self] in showTextEditController(title: strings.staticIP, type: ConfigConstants.staticIp, value: config.staticIp) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.reserveIP,
                        value: config.reserveIp,
                        click: { [self] in showTextEditController(title: strings.reserveIP, type: ConfigConstants.reserveIp, value: config.reserveIp) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.radioLiter,
                        value: config.radioLiter ? "1" : "3",
                        click: { [self] in showSelectorAlert(title: strings.radioLiter, type: ConfigConstants.radioLiter, value: config.radioLiter) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.waitReviewed,
                        value: getAllownessString(config.waitReviewed),
                        click: { [self] in showSelectorAlert(title: strings.waitReviewed, type: ConfigConstants.waitReviewed, value: config.waitReviewed) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.armServer,
                        value: getAllownessString(config.armServer),
                        click: { [self] in showSelectorAlert(title: strings.armServer, type: ConfigConstants.armServer, value: config.armServer) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.disarmServer,
                        value: getAllownessString(config.disarmServer),
                        click: { [self] in showSelectorAlert(title: strings.disarmServer, type: ConfigConstants.disarmServer, value: config.disarmServer) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.roaming,
                        value: getAllownessString(config.roaming),
                        click: { [self] in showSelectorAlert(title: strings.roaming, type: ConfigConstants.roaming, value: config.roaming) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.entryDelay,
                        value: config.entryDelay,
                        click: { [self] in showSelectorAlert(title: strings.entryDelay, type: ConfigConstants.entryDelay, value: config.entryDelay) }
                    ),
                    XApnDeviceView.Model.Item(
                        title: strings.exitDelay,
                        value: config.exitDelay,
                        click: { [self] in showSelectorAlert(title: strings.exitDelay, type: ConfigConstants.exitDelay, value: config.exitDelay) }
                    ),
                ]
            ),
            XApnDeviceView.Model(
                title: strings.actionsTitle,
                items: [
                    XApnDeviceView.Model.Item(
                        title: strings.reset,
                        click: showResetController
                    )
                ]
            )
        ]
        
        print(config.apn2List)
        print(config.tempList)
        
        if config.apn2List.allSatisfy({ $0 != nil }) {
            configItems.insert(
                XApnDeviceView.Model(
                    title: strings.apn2Title,
                    items: [
                        XApnDeviceView.Model.Item(
                            title: strings.apn2Server,
                            value: config.apn2Server!,
                            click: { [self] in showTextEditController(title: strings.apn2Server, type: ConfigConstants.apn2Server, value: config.apn2Server!) }
                        ),
                        XApnDeviceView.Model.Item(
                            title: strings.apn2Login,
                            value: config.apn2Login!,
                            click: { [self] in showTextEditController(title: strings.apn2Login, type: ConfigConstants.apn2Login, value: config.apn2Login!) }
                        ),
                        XApnDeviceView.Model.Item(
                            title: strings.apn2Password,
                            value: config.apn2Password!,
                            click: { [self] in showTextEditController(title: strings.apn2Password, type: ConfigConstants.apn2Password, value: config.apn2Password!) }
                        ),
                    ]
                ), at: 1
            )
        }
        
        if config.tempList.allSatisfy({ $0 != nil }) {
            var tempItems: [XApnDeviceView.Model.Item] = []
            var num: Int = 1
            
            for i in 0..<config.tempList.count {
                let title = (i % 2 == 0 ? strings.minTemp : strings.maxTemp) + String(num)
                
                tempItems.append(
                    XApnDeviceView.Model.Item(
                        title: title,
                        value: config.tempList[i],
                        click: { [self] in showTextEditController(title: title, type: ConfigConstants.tempList[i], value: config.tempList[i]!) }
                    )
                )
                
                if i % 2 != 0 {
                    num += 1
                }
            }
            
            configItems.insert(
                XApnDeviceView.Model(
                    title: strings.tempTitle,
                    items: tempItems
                ),
                at: 3
            )
        }
        
        mainView.setConfig(configItems)
    }
    
    private func showTextEditController(title: String, type: String, value: String) {
        isEditingConfig = true
        
        let controller = XTextEditController(title: title, value: value) { [self] controller, value in
            guard checkValue(type: type, value: value) else {
                if let controller = controller as? XTextEditController {
                    if type == ConfigConstants.pinCode {
                        controller.showToast(message: strings.incorrectPin)
                    } else if ConfigConstants.ipList.contains(type) {
                        controller.showToast(message: strings.incorrectIP)
                    } else if ConfigConstants.apnList.contains(type) {
                        controller.showToast(message: strings.onlyLatinError)
                    } else if ConfigConstants.tempList.contains(type) {
                        controller.showToast(message: strings.incorrectTemp)
                    }
                }
                
                return
            }
            
            config.setConfigValue(type, value: value)
            sendSetConfigRequest()
            
            controller.navigationController?.popViewController(animated: true)
            back()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showSelectorAlert(title: String, type: String, value: String) {
        let alert = XSelectorAlertController(
            title: title,
            negativeText: strings.dialogNegative,
            positiveText: strings.dialogPositive,
            items: ConfigConstants.delayList.map {
                XSelectorAlertControllerModel(title: String($0), value: String($0))
            },
            selectedIndex: ConfigConstants.delayList.firstIndex(of: Int8(value) ?? 0) ?? 0,
            buttonsOrientation: .vertical
        )
        
        alert.onPositiveViewTapped = { [self] value in
            alert.dismiss(animated: true)
            
            config.setConfigValue(type, value: value as? String ?? "")
            sendSetConfigRequest()
            
            back()
        }
        
        navigationController?.present(alert, animated: true)
    }
    
    private func showSelectorAlert(title: String, type: String, value: Bool) {
        let items = type == ConfigConstants.radioLiter ? [
            XSelectorAlertControllerModel(title: "3", value: false),
            XSelectorAlertControllerModel(title: "1", value: true)
        ] : [
            XSelectorAlertControllerModel(title: strings.allow, value: false),
            XSelectorAlertControllerModel(title: strings.disallow, value: true)
        ]
        
        let alert = XSelectorAlertController(
            title: title,
            negativeText: strings.dialogNegative,
            positiveText: strings.dialogPositive,
            items: items,
            selectedIndex: value ? 1 : 0,
            buttonsOrientation: .vertical
        )
        
        alert.onPositiveViewTapped = { [self] value in
            alert.dismiss(animated: true)
            
            config.setConfigValue(type, value: value as? Bool ?? true)
            sendSetConfigRequest()
            
            back()
        }
        
        navigationController?.present(alert, animated: true)
    }
    
    private func showResetController() {
        isEditingConfig = true
        
        let controller = XApnResetController(deviceName: "\(device.name) \(device.serialNumber)") { [self] in
            sendResetRequest()
            
            back()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func checkValue(type: String, value: String) -> Bool {
        if type == ConfigConstants.pinCode {
            return value.range(of: #"^[0-9.]+$"#, options: .regularExpression) != nil && value.count == 4
        } else if ConfigConstants.ipList.contains(type) {
            return NetworkUtils.validateIpAddress(ip: value)
        } else if ConfigConstants.apnList.contains(type) {
            return value.range(of: #"^[A-Za-z0-9.]+$"#, options: .regularExpression) != nil
        } else if ConfigConstants.tempList.contains(type) {
            let value = Int(value) ?? 128
            
            return value >= -128 && value <= 127
        }
        
        return true
    }
    
    private func getAllownessString(_ state: Bool) -> String {
        return state ? strings.disallow : strings.allow
    }
}
