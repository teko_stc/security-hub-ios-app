//
//  XApnViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XApnViewDelegate {
    func deviceSelected(device: Int)
    func refreshDevices()
}
