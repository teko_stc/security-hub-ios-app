//
//  XApnViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 09.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XApnViewStyle {
    let background: UIColor
    let loaderColor: UIColor
    
    let info: XBaseLableStyle
    let tableTitle: XBaseLableStyle
    let finding: XBaseLableStyle
    let pullToUpdate: XBaseLableStyle
}

extension XApnView {
    typealias Style = XApnViewStyle
}
