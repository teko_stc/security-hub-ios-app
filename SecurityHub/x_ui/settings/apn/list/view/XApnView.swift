//
//  XApnController.swift
//  SecurityHub
//
//  Created by Daniil on 09.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XApnView: UIView {
    
    private let strings: XApnViewStrings = XApnViewStrings()
    
    public var delegate: XApnViewDelegate!
    
    private var devices: [Cell.Model] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var scrollView: UIScrollView = UIScrollView()
    private var contentView: UIView = UIView()
    private var infoLabel: UILabel = UILabel()
    private var tableTitleLabel: UILabel = UILabel()
    private var tableView: XDynamicSizeTableView = XDynamicSizeTableView()
    private var tableFooterView: UIView!
    private var refreshControl: UIRefreshControl = UIRefreshControl()
    private var loaderView: XLoaderView = XLoaderView()
    private var findingLabel: UILabel = UILabel()
    private var pullToUpdateLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        scrollView.contentSize = contentView.frame.size
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.background
        
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        infoLabel = UILabel.create(style.info)
        infoLabel.numberOfLines = 0
        infoLabel.text = strings.info
        contentView.addSubview(infoLabel)
        
        tableTitleLabel = UILabel.create(style.tableTitle)
        tableTitleLabel.text = strings.tableTitle
        contentView.addSubview(tableTitleLabel)
        
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        contentView.addSubview(tableView)
        
        tableFooterView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 300)))
        tableView.tableFooterView = tableFooterView
        
        tableView.refreshControl = refreshControl
        
        loaderView.color = style.loaderColor
        tableFooterView.addSubview(loaderView)
        
        findingLabel = UILabel.create(style.finding)
        findingLabel.text = strings.finding
        tableFooterView.addSubview(findingLabel)
        
        pullToUpdateLabel = UILabel.create(style.pullToUpdate)
        pullToUpdateLabel.text = strings.pullToUpdate
        pullToUpdateLabel.numberOfLines = 0
        pullToUpdateLabel.isHidden = true
        tableFooterView.addSubview(pullToUpdateLabel)
    }
    
    private func setConstraints() {
        [scrollView, contentView, infoLabel, tableTitleLabel, tableView, findingLabel, loaderView, pullToUpdateLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 14),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -40),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            
            tableTitleLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 14),
            tableTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 22),
            
            tableView.topAnchor.constraint(equalTo: tableTitleLabel.bottomAnchor, constant: 20),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 24),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            loaderView.widthAnchor.constraint(equalToConstant: 40),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.topAnchor.constraint(equalTo: tableFooterView.topAnchor, constant: 32),
            loaderView.centerXAnchor.constraint(equalTo: tableFooterView.centerXAnchor),
            
            findingLabel.topAnchor.constraint(equalTo: loaderView.bottomAnchor, constant: 24),
            findingLabel.leadingAnchor.constraint(equalTo: tableFooterView.leadingAnchor, constant: 26),
            findingLabel.trailingAnchor.constraint(equalTo: tableFooterView.trailingAnchor, constant: -26),
            
            pullToUpdateLabel.topAnchor.constraint(equalTo: tableFooterView.topAnchor, constant: 24),
            pullToUpdateLabel.leadingAnchor.constraint(equalTo: tableFooterView.leadingAnchor, constant: 26),
            pullToUpdateLabel.trailingAnchor.constraint(equalTo: tableFooterView.trailingAnchor, constant: -26)
        ])
    }
    
    public func addDevice(_ model: Cell.Model) {
        devices.append(model)
    }
    
    public func resetDevices() {
        devices.removeAll()
    }
    
    public func changeFindingState(_ finding: Bool) {
        loaderView.isHidden = !finding
        findingLabel.isHidden = !finding
        
        pullToUpdateLabel.isHidden = finding
    }
}

extension XApnView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        
        cell.setContent(devices[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate.deviceSelected(device: indexPath.row)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
            
            delegate.refreshDevices()
        }
    }
}
