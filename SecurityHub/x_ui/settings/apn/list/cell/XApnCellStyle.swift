//
//  XApnCellStyle.swift
//  SecurityHub
//
//  Created by Daniil on 10.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XApnCellStyle {
    struct Selection {
        let color: UIColor
        let cornerRadius: CGFloat
    }
    
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let selection: Selection
    let selectionLabelColor: UIColor
    
    let name: Label
    let serialNumber: Label
}

extension XApnCell {
    typealias Style = XApnCellStyle

    func cellStyle() -> Style {
        return Style(
            selection: Style.Selection(
                color: UIColor(red: 0x3A/255, green: 0xBE/255, blue: 0xFF/255, alpha: 1),
                cornerRadius: 12
            ),
            selectionLabelColor: UIColor.white,
            name: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            serialNumber: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
