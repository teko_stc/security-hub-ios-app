//
//  XApnCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 10.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

struct XApnCellModel {
    let name: String
    let serialNumber: String
}

extension XApnCell {
    typealias Model = XApnCellModel
}
