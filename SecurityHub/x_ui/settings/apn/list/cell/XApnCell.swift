//
//  XApnCell.swift
//  SecurityHub
//
//  Created by Daniil on 10.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XApnCell: UITableViewCell {
    
    public static let identifier = "XApnCell.identifier"
    
    private var selectedView: UIView = UIView()
    private var nameLabel: UILabel = UILabel()
    private var serialNumberLabel: UILabel = UILabel()
    
    private var style: Style!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.style = cellStyle()
        
        initViews()
        setContraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews() {
        selectedView.backgroundColor = style.selection.color
        selectedView.layer.cornerRadius = style.selection.cornerRadius
        selectedBackgroundView = selectedView
        
        nameLabel.font = style.name.font
        nameLabel.textColor = style.name.color
        addSubview(nameLabel)
        
        serialNumberLabel.font = style.serialNumber.font
        serialNumberLabel.textColor = style.serialNumber.color
        addSubview(serialNumberLabel)
    }
    
    private func setContraints() {
        [nameLabel, serialNumberLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 7),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 23),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -23),
            
            serialNumberLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 2),
            serialNumberLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 23),
            serialNumberLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -23),
            
            bottomAnchor.constraint(equalTo: serialNumberLabel.bottomAnchor, constant: 12)
        ])
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        nameLabel.textColor = highlighted ? style.selectionLabelColor : style.name.color
        serialNumberLabel.textColor = highlighted ? style.selectionLabelColor : style.serialNumber.color
    }
    
    public func setContent(_ model: Model) {
        nameLabel.text = model.name
        serialNumberLabel.text = model.serialNumber
    }
}

extension XApnView {
    typealias Cell = XApnCell
}
