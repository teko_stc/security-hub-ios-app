//
//  XApnControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 09.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import Localize_Swift

class XApnControllerStrings {
    var title: String { "PROFILE_DEVICE_APN".localized() }
    var error: String { "error".localized() }
    var pinError: String { "PN_ENTER_ENTER_PN".localized() }
    
    var defaultControllerName: String { "Security Hub" }
    
    var pinAlertStrings: XPinPasswordAlertView.Strings = XPinPasswordAlertView.Strings(
        title: "",
        subtitle: "LOCAL_CONFIG_PIN_DIALOG".localized(),
        paginationTitle: "PINCODERUS".localized(),
        cancel: "ADB_CANCEL".localized(),
        confirm: "WIZ_NEXT".localized()
    )
    
    var wifiAlertStrings: XAlertViewStrings = XAlertViewStrings(
        title: "ERROR_NO_WIFI".localized(),
        text: "WIFI_SUGGESTION".localized(),
        positive: "N_BUTTON_CHANGE".localized(),
        negative: "CLOSE".localized()
    )
}

class XApnViewStrings {
    var info: String { "LOCAL_CONFIG_DEVICE_INFO".localized() }
    var tableTitle: String { "LOCAL_CONFIG_FOUND_DEVICES".localized() }
    var finding: String { "LOCAL_CONFIG_SEARCH_DEVICES".localized() }
    var pullToUpdate: String { "N_PULL_TO_UPDATE".localized() }
}
