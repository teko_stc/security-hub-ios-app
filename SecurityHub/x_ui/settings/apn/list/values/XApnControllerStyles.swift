//
//  XApnControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 09.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XApnController {
    func wifiAlertStyle() -> XAlertViewStyle {
        return XAlertViewStyle(
            backgroundColor: UIColor.white,
            title: XAlertViewStyle.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x414042)
            ),
            text: XAlertViewStyle.Lable(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.colorFromHex(0x414042)
            ),
            buttonOrientation: .vertical,
            positive: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0xf9543e)
            ),
            negative: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x414042)
            )
        )
    }
}

extension XApnView {
    func style() -> Style {
        return Style(
            background: UIColor.white,
            loaderColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
            info: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 14)
            ),
            tableTitle: XBaseLableStyle(
                color: UIColor(red: 0xBC/255, green: 0xBC/255, blue: 0xBC/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            finding: XBaseLableStyle(
                color: UIColor(red: 0xBC/255, green: 0xBC/255, blue: 0xBC/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            ),
            pullToUpdate: XBaseLableStyle(
                color: UIColor(red: 0xBC/255, green: 0xBC/255, blue: 0xBC/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            )
        )
    }
}
