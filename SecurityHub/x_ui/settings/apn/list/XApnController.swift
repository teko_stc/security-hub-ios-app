//
//  XApnController.swift
//  SecurityHub
//
//  Created by Daniil on 09.11.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import CocoaAsyncSocket
import RxSwift

class XApnController: XBaseViewController<XApnView> {

    override var tabBarIsHidden: Bool { true }

    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        ),
        titleView: XBaseLableStyle(
            color: UIColor.colorFromHex(0x414042),
            font: UIFont(name: "Open Sans", size: 25)
        ),
        rightViews: [
            XNavigationViewRightViewBuilder(
                imageName: "ic_reload",
                color: UIColor.colorFromHex(0x414042),
                viewTapped: {
                    DispatchQueue.main.async { [self] in
                        if sendingSurveyDisposable == nil {
                            startSendingSurveyRequests()
                        }
                    }
                }
            )
        ]
    )

    private let strings: XApnControllerStrings = XApnControllerStrings()
    private let connection: LocalConnection = LocalConnection.instance

    private let sendingSurveyTime: Int = 20
    private let surveyRequestTimeout: Double = 1.0
    private let testModeRequestTimeout: Double = 20.0

    private var sendingSurveyDisposable: Disposable?
    private var sendingTestModeDisposable: Disposable?

    private var devices: [LocalDevice] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationViewBuilder(navigationViewBuilder)
        title = strings.title
        mainView.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if NetworkUtils.getWiFiAddress() == nil {
            showWifiAlert()
        } else {
            configConnection()
            startSendingSurveyRequests()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if sendingSurveyDisposable != nil {
            sendingSurveyDisposable!.dispose()
            sendingSurveyDisposable = nil
        }
    }
}

extension XApnController {

    private func configConnection() {
        connection.debugEnabled(false)

        connection.setHandlers { data, host in
            DispatchQueue.main.async {
                self.onReceive(data, host: host)
            }
        } errorHandler: { error in
            print("Local connection error: \(error)")
        } closeHandler: { error in
            self.connection.createSocket()
        }
    }

    private func onReceive(_ data: Data, host: String) {
        let bytesArray = [UInt8] (data)
        let int8BytesArray = BytesUtils.uInt8ArrayToInt8(bytesArray)
        
        if (bytesArray[0] & 0xff) == 255 && bytesArray.count > 10 {
            // Убрать вообще возможность конфигурировать 4G хабы локально
            guard int8BytesArray[4] < 4 else {
                return
            }
            
            if devices.allSatisfy({ $0.ip != host }) {
                if host != NetworkUtils.getWiFiAddress() && host != "0.0.0.0" {
                    let serial = BytesUtils.byteArrayToUInt32(
                        BytesUtils.invertBytes(
                            Array(int8BytesArray[5..<9])
                        )
                    )

                    let version = "\(int8BytesArray[4]).\(int8BytesArray[3])"

                    let device = LocalDevice(
                        name: strings.defaultControllerName,
                        serialNumber: "S/N \(serial)",
                        ip: host
                    )
                    devices.append(device)

                    mainView.addDevice(
                        XApnView.Cell.Model(
                            name: "\(device.name) (\(version))",
                            serialNumber: device.serialNumber
                        )
                    )
                }
            }
        } else if (bytesArray[0] & 0x7F) == TestModeRequest.command {
            if sendingTestModeDisposable != nil {
                sendingTestModeDisposable!.dispose()
            }
        }
    }

    private func startSendingSurveyRequests() {
        mainView.changeFindingState(true)

        devices.removeAll()
        mainView.resetDevices()

        sendingSurveyDisposable = Observable<Int>.timer(.seconds(sendingSurveyTime), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [self] _ in
                mainView.changeFindingState(false)

                sendingSurveyDisposable = nil
            })

        sendingSurveyRequest()
    }

    private func sendingSurveyRequest() {
        if sendingSurveyDisposable != nil {
            connection.sendData(
                host: LocalConnection.broadcastAddress,
                data: SurveyRequest.toData(),
                timeout: surveyRequestTimeout
            )

            DispatchQueue.main.asyncAfter(deadline: .now() + surveyRequestTimeout) {
                self.sendingSurveyRequest()
            }
        }
    }

    private func sendTestModeRequest(_ device: LocalDevice, state: TestModeRequest.TestModeState) {
        let alert = XLoadingAlertController()
        navigationController?.present(alert, animated: false) { [self] in
            connection.sendData(
                host: device.ip,
                data: TestModeRequest(pin: device.pin, state: state).toData(),
                timeout: testModeRequestTimeout
            )
        }

        sendingTestModeDisposable = Observable<Int>.timer(.seconds(Int(testModeRequestTimeout)), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [self] _ in
                showToast(message: strings.error)

                alert.dismiss(animated: true)

                sendingTestModeDisposable = nil
            }, onDisposed: { [self] in
                guard sendingTestModeDisposable != nil else { return }
                
                alert.dismiss(animated: true) {
                    showApnDeviceController(device)
                }
            })
    }

    private func showApnDeviceController(_ device: LocalDevice) {
        let controller = XApnDeviceController(device: device)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showWifiAlert() {
        let alert = XAlertController(
            style: wifiAlertStyle(),
            strings: strings.wifiAlertStrings,
            positive: self.openSettings,
            negative: self.back
        )
        alert.canDismiss = false
        
        navigationController?.present(alert, animated: true)
    }
    
    private func openSettings() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
}

extension XApnController: XApnViewDelegate {
    func deviceSelected(device: Int) {
        var device = devices[device]

        var alertStrings = strings.pinAlertStrings
        alertStrings.title = "\(device.name) \(device.serialNumber)"

        let alert = XPinPasswordAlertController(strings: alertStrings) { [self] controller, pin in
            guard pin.count == 4 else { return showToast(message: strings.pinError) }

            controller.dismiss(animated: false)

            device.pin = Int(pin) ?? 0
            sendTestModeRequest(device, state: .on)
        }
        navigationController?.present(alert, animated: true)
    }
    
    func refreshDevices() {
        if sendingSurveyDisposable == nil {
            startSendingSurveyRequests()
        }
    }
}
