//
//  XCompaniesController.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCompaniesController: XBaseViewController<XCompaniesView> {
    override var tabBarIsHidden: Bool { true }

    private let strings: XCompaniesControllerStrings = XCompaniesControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)

        mainView.delegate = self
    
        getCompanies()
    }
    
    private func getCompanies() {
        let companies =
            try! JSONDecoder().decode(
                [Company].self,
                from: NSData(contentsOfFile:
                                Bundle.main.path(forResource: "security_companies",
                                                 ofType: "json"
                                )!
                )! as Data
            )
        
        mainView.setCompanies(companies)
    }
}

extension XCompaniesController: XCompaniesViewDelegate {
    func companySelected(_ company: Company) {
        if let _ = company.departs {
            showDepartsBottomSheet(company)
        } else {
            navigationController?.present(
                XCompaniesAlertController(company, rc: nil),
                animated: true
            )
        }
    }
    
    func showDepartsBottomSheet(_ company: Company) {
        let departs = company.departs!.depart
        
        var items: [XBottomSheetViewItem] = []
        for depart in departs {
            items.append(XBottomSheetViewItem(title: depart.subject))
        }
        
        let bottomSheet = XBottomSheetController(
            style: bottomSheetStyle(),
            title: strings.departsTitle,
            selectedIndex: -1,
            items: items
        )
        
        bottomSheet.onSelectedItem = {_, index, _ in
            bottomSheet.dismiss(animated: true)
            
            self.showRcsBottomSheet(company, rcs: departs[index].rcs.rc)
        }
        
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func showRcsBottomSheet(_ company: Company, rcs: [RC]) {
        var items: [XBottomSheetViewItem] = []
        for rc in rcs {
            items.append(XBottomSheetViewItem(title: rc.caption))
        }
        
        let bottomSheet = XBottomSheetController(
            style: bottomSheetStyle(),
            title: strings.rcsTitle,
            selectedIndex: -1,
            items: items
        )
        
        bottomSheet.onSelectedItem = {_, index, _ in
            bottomSheet.dismiss(animated: true)
            
            self.navigationController?.present(
                XCompaniesAlertController(company, rc: rcs[index]),
                animated: true
            )
        }
        
        navigationController?.present(bottomSheet, animated: true)
    }
}
