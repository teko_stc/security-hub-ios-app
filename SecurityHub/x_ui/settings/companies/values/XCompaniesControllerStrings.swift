//
//  XCompaniesControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XCompaniesControllerStrings {
    var title: String { "drawer_item_security".localized() }
    
    /// Выберите регион
    var departsTitle: String { "TITLE_SELECT_SEC_COMP_DEPART".localized() }
    
    /// Выберите РЦ
    var rcsTitle: String { "TITLE_SELECT_SEC_COMP_RC".localized() }
}
