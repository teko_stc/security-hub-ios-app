//
//  XCompaniesViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XCompaniesViewDelegate {
    func companySelected(_ company: Company)
}
