//
//  XCompaniesCellStyle.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCompaniesCellStyle {
    struct Selection {
        let cornerRadius: CGFloat
        let backgroundColor: UIColor
    }
    
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let selection: Selection
    let name: Label
}

extension XCompaniesCell {
    typealias Style = XCompaniesCellStyle
    
    func cellStyle() -> Style {
        return Style(
            selection: Style.Selection(
                cornerRadius: 18,
                backgroundColor: UIColor(red: 0xEE/255, green: 0xEE/255, blue: 0xEE/255, alpha: 1)
            ),
            name: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
