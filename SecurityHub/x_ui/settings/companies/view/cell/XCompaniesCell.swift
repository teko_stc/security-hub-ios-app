//
//  XCompaniesCell.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCompaniesCell: UITableViewCell {
    
    static var identifier = "XLanguageCell.identifier"
    
    private var selectionView: UIView = UIView()
    private var logoView: UIImageView = UIImageView()
    private var nameView: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews(cellStyle())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        selectionView.layer.cornerRadius = style.selection.cornerRadius
        selectionView.backgroundColor = style.selection.backgroundColor
        selectionView.clipsToBounds = false
        selectedBackgroundView = selectionView
        
        addSubview(logoView)
        
        nameView.textColor = style.name.color
        nameView.font = style.name.font
        nameView.numberOfLines = 3
        nameView.lineBreakMode = .byTruncatingTail
        addSubview(nameView)
    }
    
    private func setConstraints() {
        logoView.translatesAutoresizingMaskIntoConstraints = false
        nameView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 126),
            
            logoView.widthAnchor.constraint(equalToConstant: 86),
            logoView.heightAnchor.constraint(equalToConstant: 86),
            logoView.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            
            nameView.topAnchor.constraint(equalTo: logoView.topAnchor, constant: 7),
            nameView.leadingAnchor.constraint(equalTo: logoView.trailingAnchor, constant: 17),
            nameView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        ])
    }
    
    public func setContent(_ model: Model) {
        logoView.image = UIImage(named: model.logo)
        nameView.text = model.name
    }
}

extension XCompaniesView {
    typealias Cell = XCompaniesCell
}
