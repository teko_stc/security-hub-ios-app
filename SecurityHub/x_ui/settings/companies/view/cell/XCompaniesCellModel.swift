//
//  XCompaniesCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XCompaniesCellModel {
    let logo: String
    let name: String
}

extension XCompaniesCell {
    typealias Model = XCompaniesCellModel
}
