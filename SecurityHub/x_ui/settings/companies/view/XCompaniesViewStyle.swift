//
//  XCompaniesViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCompaniesViewStyle {
    let backgroundColor: UIColor
}

extension XCompaniesView {
    typealias Style = XCompaniesViewStyle
}
