//
//  XCompaniesAlertController.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCompaniesAlertController: XBaseAlertController<XCompaniesAlertView> {
    
    override var widthAnchor: CGFloat { 0.85 }
    
    private let strings: XCompaniesAlertViewStrings = XCompaniesAlertViewStrings()
    
    private var company: Company!
    private var rc: RC?
    
    init(_ company: Company, rc: RC?) {
        super.init()
        
        self.company = company
        self.rc = rc
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        
        mainView.setContent(company, rc: rc)
    }
}

extension XCompaniesAlertController: XCompaniesAlertViewDelegate {
    func callTapped(_ company: Company, rc: RC?) {
        if let rc = rc {
            showCallBottomSheet(rc.phones.string)
        } else if let cphones = company.cphones {
            var numbers: [String] = []
            
            for cphone in cphones.cphone {
                numbers.append(cphone.phone)
            }
            
            showCallBottomSheet(numbers)
        } else {
            showCallBottomSheet(company.phones!.string)
        }
    }
    
    func mailTapped(_ mail: String) {
        guard let url = URL(string: "mailto:\(mail)") else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        
        dismiss(animated: true)
    }
    
    func siteTapped(_ site: String) {
        guard let url = URL(string: site) else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        
        dismiss(animated: true)
    }
    
    func showCallBottomSheet(_ phones: [String]) {
        var items: [XBottomSheetViewItem] = []
        for phone in phones {
            items.append(XBottomSheetViewItem(title: phone))
        }
        
        let bottomSheet = XBottomSheetController(
            style: bottomSheetStyle(),
            title: strings.phoneTitle,
            selectedIndex: -1,
            items: items
        )
        bottomSheet.onSelectedItem = {_, index, _ in
            bottomSheet.dismiss(animated: true)
            
            self.callNumber(phones[index])
        }
        
        let presentingVC = presentingViewController
        dismiss(animated: true) {
            presentingVC?.present(bottomSheet, animated: true)
        }
    }
    
    func callNumber(_ phone: String) {
        let phoneCorrect = phone.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+7", with: "8") 
        guard let url = URL(string: "tel://\(phoneCorrect)") else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
