//
//  XCompaniesAlertView.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCompaniesAlertView: UIView {
    
    public var delegate: XCompaniesAlertViewDelegate?
    
    private let strings = XCompaniesAlertViewStrings()
    
    private var company: Company!
    private var rc: RC?
    
    private var titleView: UILabel = UILabel()
    private var textView: UILabel = UILabel()
    private var buttonsStackView: UIStackView = UIStackView()
    private var callButtonView: XZoomButton!
    private var mailButtonView: XZoomButton!
    private var siteButtonView: XZoomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.numberOfLines = 0
        titleView.textAlignment = .center
        addSubview(titleView)
        
        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.numberOfLines = 0
        textView.textAlignment = .center
        addSubview(textView)
        
        buttonsStackView.axis = .vertical
        buttonsStackView.distribution = .equalSpacing
        buttonsStackView.spacing = 14
        addSubview(buttonsStackView)
        
        callButtonView = XZoomButton(style: style.call)
        callButtonView.text = strings.call
        callButtonView.addTarget(self, action: #selector(callTapped), for: .touchUpInside)
        buttonsStackView.addArrangedSubview(callButtonView)
        
        mailButtonView = XZoomButton(style: style.mail)
        mailButtonView.text = strings.mail
        mailButtonView.addTarget(self, action: #selector(mailTapped), for: .touchUpInside)
        buttonsStackView.addArrangedSubview(mailButtonView)

        siteButtonView = XZoomButton(style: style.site)
        siteButtonView.text = strings.site
        siteButtonView.addTarget(self, action: #selector(siteTapped), for: .touchUpInside)
        buttonsStackView.addArrangedSubview(siteButtonView)
    }
    
    private func setConstraints() {
        [titleView, textView, buttonsStackView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 23),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 26),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -26),
            
            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 23),
            textView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 26),
            textView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -26),
            
            buttonsStackView.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 23),
            buttonsStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -23),
            buttonsStackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    public func setContent(_ company: Company, rc: RC?) {
        self.company = company
        self.rc = rc
        
        var text = ""
        
        if let message = company.message {
            text.append("\(message)\n\n")
        }
        
        if let rc = rc {
            text.append("\(rc.caption)\n\(rc.city)\n\n")
            text.append("\(rc.phones.string.joined(separator: "\n"))\n\n")
            text.append("\(rc.sites.string)\n\n")
            
            mailButtonView.removeFromSuperview()
        } else {
            if let addresses = company.addresses {
                text.append("\(addresses.string.joined(separator: "\n"))\n\n")
            }
            
            if let cphones = company.cphones {
                var phonesText = ""
                
                for phone in cphones.cphone {
                    phonesText.append("\(phone.phone) (\(phone.caption))\n")
                }
                
                text.append("\(phonesText)\n\n")
            }
            
            if let phones = company.phones {
                text.append("\(phones.string.joined(separator: "\n"))\n\n")
            }
            
            if let email = company.emails {
                text.append("\(email.string)\n\n")
            } else {
                mailButtonView.removeFromSuperview()
            }
            
            if let sites = company.sites {
                text.append("\(sites.string)\n\n")
            } else {
                siteButtonView.removeFromSuperview()
            }
        }
        
        titleView.text = company.caption
        textView.text = String(text.dropLast(2))
    }
    
    @objc func callTapped() {
        delegate?.callTapped(company, rc: rc)
    }
    
    @objc func mailTapped() {
        delegate?.mailTapped(company.emails!.string)
    }
    
    @objc func siteTapped() {
        delegate?.siteTapped(rc != nil
                                ? rc!.sites.string
                                : company.sites!.string
        )
    }
}
