//
//  XCompaniesAlertViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XCompaniesAlertViewDelegate {
    func callTapped(_ company: Company, rc: RC?)
    
    func mailTapped(_ mail: String)
    
    func siteTapped(_ site: String)
}
