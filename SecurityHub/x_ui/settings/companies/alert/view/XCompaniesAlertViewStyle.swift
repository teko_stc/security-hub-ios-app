//
//  XCompaniesAlertViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCompaniesAlertViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    
    let title: Label
    let text: Label
    
    let call: XZoomButtonStyle
    let mail: XZoomButtonStyle
    let site: XZoomButtonStyle
}

extension XCompaniesAlertView {
    typealias Style = XCompaniesAlertViewStyle
}
