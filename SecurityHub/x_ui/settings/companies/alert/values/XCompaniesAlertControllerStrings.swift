//
//  XCompaniesAlertControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XCompaniesAlertViewStrings {
    var call: String { "SEC_COMP_CALL".localized() }
    
    var mail: String { "SEC_COMP_MAIL".localized() }
    
    /// Перейти на сайт
    var site: String { "SEC_COMP_SITE".localized() }
    
    /// Выберите номер
    var phoneTitle: String { "WIZ_ENTER_ZONE_NUMBER".localized() }
}
