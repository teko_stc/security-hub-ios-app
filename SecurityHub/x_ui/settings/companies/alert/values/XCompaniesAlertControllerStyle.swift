//
//  XCompaniesAlertControllerStyle.swift
//  SecurityHub
//
//  Created by Daniil on 07.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCompaniesAlertView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: Style.Label(
                font: UIFont(name: "Open Sans", size: 24),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: Style.Label(
                font: UIFont(name: "Open Sans", size: 18),
                color: UIColor(red: 0xBC/255, green: 0xBC/255, blue: 0xBC/255, alpha: 1)
            ),
            call: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x3A/255, green: 0xBE/255, blue: 0xFF/255, alpha: 1)
            ),
            mail: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            site: XZoomButtonStyle(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}

extension XCompaniesAlertController {
    func bottomSheetStyle() -> XBottomSheetViewStyle {
        return XBottomSheetView.Style(
            backgroundColor: UIColor.white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0xEE/255, green: 0xEE/255, blue: 0xEE/255, alpha: 1),
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                ),
                text: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    select: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8)
                )
            )
        )
    }
}
