//
//  XBaseSettingsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


protocol XBaseSettingsViewDelegate {
    func selectedCell(model: XBaseSettingsViewLayerModelItemProtocol)
    func switchCell(model: XBaseSettingsViewLayerModelItemProtocol, index: IndexPath, oldValue: Bool)
    func switchHeader(model: XBaseSettingsViewLayerModel, section: Int, oldValue: Bool)
}

protocol XBaseSettingsViewLayerModelItemProtocol {
    var title: String? { get set}
    var subTitle: String? { get set }
    var isActive: Bool { get set }
}

protocol XBaseSettingsViewLayer {
    func setItems(_ items: [XBaseSettingsViewLayerModel])
		func getItems() -> [XBaseSettingsViewLayerModel]
    func updItem(index: IndexPath, model: XBaseSettingsViewLayerModelItemProtocol)
    func updHeader(section: Int, model: XBaseSettingsViewLayerModel)
    func deactiveItems(indexs: [IndexPath])
    func activeItems(indexs: [IndexPath])
    func setAltBackgroundColor()
}

struct XBaseSettingsViewLayerModelPushClassHeader {
    let icon: String
    let switchTitle: String
    let isOn: Bool
}

struct XBaseSettingsViewLayerModel {
    let title: String?
    var headerObject: Any? = nil
    var items: [XBaseSettingsViewLayerModelItemProtocol]
}

struct XBaseSettingsViewStyle {
    let backgroundColor, altBackgroundColor: UIColor
    let header: XBaseLableStyle
    let cellActive: XBaseSettingsSwitchCellStyle
    let cellAnactive: XBaseSettingsSwitchCellStyle
    let pushHeaderStyle: XEditButtonStyle?
}

class XBaseSettingsView: UIView {
    public var delegate: XBaseSettingsViewDelegate?
    
    private var tableView: UITableView = UITableView(frame: .zero, style: .grouped)
    private var items: [XBaseSettingsViewLayerModel] = []
    
    private lazy var style: XBaseSettingsViewStyle = self.viewStyles()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        tableView.backgroundColor = style.backgroundColor
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
        tableView.register(XBaseSettingsCell.self, forCellReuseIdentifier: XBaseSettingsCellIdentifier)
        tableView.register(XBaseSettingsSwitchCell.self, forCellReuseIdentifier: XBaseSettingsSwitchCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func setAltBackgroundColor() {
        backgroundColor = style.altBackgroundColor
        tableView.backgroundColor = style.altBackgroundColor
    }
}

extension XBaseSettingsView: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int { self.items.count }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerObject = self.items[section].headerObject as? XBaseSettingsViewLayerModelPushClassHeader, let pushHeaderStyle = style.pushHeaderStyle {
            let iconView = UIImageView(image: UIImage(named: headerObject.icon)), _view = UIView(), switchView = XEditButton(style: pushHeaderStyle), _layer = CAShapeLayer()
            _layer.path = UIBezierPath(roundedRect:  CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 204)), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 0)).cgPath
            _layer.fillColor = style.backgroundColor.cgColor
            _view.layer.addSublayer(_layer)
            _view.backgroundColor = style.altBackgroundColor
            switchView.title = headerObject.switchTitle
            switchView.isSelected = headerObject.isOn
            switchView.onSelected = { b in self.delegate?.switchHeader(model: self.items[section], section: section, oldValue: headerObject.isOn); switchView.isSelected = headerObject.isOn }
            switchView.onTapped = { self.delegate?.switchHeader(model: self.items[section], section: section, oldValue: headerObject.isOn); switchView.isSelected = headerObject.isOn }
            _view.addSubview(iconView)
            iconView.translatesAutoresizingMaskIntoConstraints = false
            iconView.widthAnchor.constraint(equalToConstant: 80).isActive = true; iconView.heightAnchor.constraint(equalToConstant: 80).isActive = true;
            iconView.topAnchor.constraint(equalTo: _view.topAnchor, constant: 30).isActive = true; iconView.centerXAnchor.constraint(equalTo: _view.centerXAnchor).isActive = true;
            _view.addSubview(switchView)
            switchView.translatesAutoresizingMaskIntoConstraints = false
            switchView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 20).isActive = true; switchView.bottomAnchor.constraint(equalTo: _view.bottomAnchor, constant: -20).isActive = true
            switchView.heightAnchor.constraint(equalToConstant: 54).isActive = true;
            switchView.leadingAnchor.constraint(equalTo: _view.leadingAnchor, constant: 20).isActive = true; switchView.trailingAnchor.constraint(equalTo: _view.trailingAnchor, constant: -20).isActive = true;
            return _view
        } else if let title = self.items[section].title {
            let lableView = UILabel.create(style.header), _view = UIView()
            lableView.text = title
            lableView.numberOfLines = 0
            lableView.translatesAutoresizingMaskIntoConstraints = false
            _view.addSubview(lableView)
            lableView.topAnchor.constraint(equalTo: _view.topAnchor, constant: 30).isActive = true
            lableView.leadingAnchor.constraint(equalTo: _view.leadingAnchor, constant: 20).isActive = true
            lableView.trailingAnchor.constraint(equalTo: _view.trailingAnchor, constant: -20).isActive = true
            lableView.bottomAnchor.constraint(equalTo: _view.bottomAnchor, constant: 0).isActive = true
            return _view
        } else {
            return UIView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 30)))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { self.items[section].items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let model = self.items[indexPath.section].items[indexPath.row] as? XBaseSettingsCellModel {
            let cell = tableView.dequeueReusableCell(withIdentifier: XBaseSettingsCellIdentifier, for: indexPath) as! XBaseSettingsCell
            cell.setStyle(model.isActive ? style.cellActive.baseCell : style.cellAnactive.baseCell)
            cell.setContent(model)
            return cell
        } else if let model = self.items[indexPath.section].items[indexPath.row] as? XBaseSettingsSwitchCellModel {
            let cell = tableView.dequeueReusableCell(withIdentifier: XBaseSettingsSwitchCellIdentifier, for: indexPath) as! XBaseSettingsSwitchCell
            cell.setStyle(model.isActive ? style.cellActive : style.cellAnactive)
            cell.setDelegate(didSelectRowAtVoid: { t, i in self.tableView(t, didSelectRowAt: i) })
            cell.setContent(model)
            return cell
        }
        return tableView.dequeueReusableCell(withIdentifier: "nil", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        tableView.deselectRow(at: indexPath, animated: true)
        if let model = self.items[indexPath.section].items[indexPath.row] as? XBaseSettingsCellModel, model.isActive {
            delegate?.selectedCell(model: model)
        } else if let model = self.items[indexPath.section].items[indexPath.row] as? XBaseSettingsSwitchCellModel, model.isActive {
            delegate?.switchCell(model: model, index: indexPath, oldValue: model.isOn)
        }
    }
}

extension XBaseSettingsView: XBaseSettingsViewLayer {
    func setItems(_ items: [XBaseSettingsViewLayerModel]) {
        self.items = items
        self.tableView.reloadData()
    }
	
		func getItems() -> [XBaseSettingsViewLayerModel] {
			return self.items
		}
		
    func updItem(index: IndexPath, model: XBaseSettingsViewLayerModelItemProtocol) {
        self.items[index.section].items[index.row] = model
        self.tableView.reloadRows(at: [index], with: .fade)
    }
    
    func deactiveItems(indexs: [IndexPath]) {
        indexs.forEach({ self.items[$0.section].items[$0.row].isActive = false })
        self.tableView.reloadRows(at: indexs, with: .fade)
    }
    
    func activeItems(indexs: [IndexPath]) {
        indexs.forEach({ self.items[$0.section].items[$0.row].isActive = true })
        self.tableView.reloadRows(at: indexs, with: .fade)
    }
    
    func updHeader(section: Int, model: XBaseSettingsViewLayerModel) {
        self.items[section] = model
        self.tableView.reloadSections([section], with: .fade)
    }
}
