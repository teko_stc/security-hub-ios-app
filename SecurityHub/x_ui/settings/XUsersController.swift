//
//  XUsersController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XUsersControllerStringsProtocol {
    var title: String { get }
    var adminDesc: String { get }
    var managerDesc: String { get }
    var viewerDesc: String { get }
}

class XUsersController: XBaseViewController<XUsersView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.showAddUserController)]
    )
    
    private lazy var viewLayer: XUsersViewLayer = self.mainView
    private lazy var strings: XUsersControllerStringsProtocol = XUsersVCStrings()
    
    private var disposable: Disposable?
    
    override func viewDidLoad() {
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewLayer.clear()
        disposable = DataManager.shared.getOperators()
            .subscribe(onNext: { (operators: [Operators], type: NUpdateType) in
                operators.forEach({ o in
                    let role: String = o.roles & Roles.ORG_ADMIN != 0 || o.roles & Roles.DOMEN_ADMIN != 0 ? self.strings.adminDesc :
                        o.roles == 40960 ? self.strings.managerDesc : self.strings.viewerDesc
                    let model = XUsersViewLayerModel(id: o.id, name: o.name, role: role, isActive: o.active != 0)
                    self.viewLayer.userUpdate(model: model, type: type == .insert ? .insert : type == .update ? .update : .delete)
                })
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
}

extension XUsersController: XUsersViewDelegate {
    func userViewTapped(model: XUsersViewLayerModel) {
        let controller = XChangeUserController(userId: model.id)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showAddUserController() {
        let controller = AddOperatorController()
        navigationController?.pushViewController(controller, animated: true)
    }
}
