//
//  XBaseSettingsCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBaseSettingsCellModel: XBaseSettingsViewLayerModelItemProtocol {
    var isActive: Bool
    
    var title: String?
    var subTitle: String?
}

struct XBaseSettingsCellStyle {
    let backgroundColor, selectedColor: UIColor
    let titleView, subTitleView: XBaseLableStyle
}

let XBaseSettingsCellIdentifier: String = "XBaseSettingsCell.identifier"
class XBaseSettingsCell: UITableViewCell {
    private var titleView = UILabel(), subTitleView = UILabel()
    public var viewTrailingAnchors: [NSLayoutConstraint] = []
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()
        selectedBackgroundView?.layer.cornerRadius = 10
        [titleView, subTitleView].forEach({ contentView.addSubview($0); $0.translatesAutoresizingMaskIntoConstraints = false; $0.numberOfLines = 0; viewTrailingAnchors.append($0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)) })
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            subTitleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            subTitleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
        ].join(viewTrailingAnchors))
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setStyle(_ style: XBaseSettingsCellStyle) {
        backgroundColor = .clear
        selectedBackgroundView?.backgroundColor = style.selectedColor
        titleView.setStyle(style.titleView)
        subTitleView.setStyle(style.subTitleView)
    }
    
    public func setContent(_ model: XBaseSettingsViewLayerModelItemProtocol) {
        titleView.text = model.title
        subTitleView.text = model.subTitle
    }
}
