//
//  XZoneSettingsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XZoneSettingsViewDelegate {
    func changeNameViewTapped(name: String)
    func delayViewTapped()
    func deleteViewTapped()
    func temperatureThresholdsViewTapped()
		func bypassViewTapped()
}

protocol XZoneSettingsViewLayer {
    func setData(name: String)
    func setData(isDelay: Bool)
		func setData(isBypass: Bool)
    func setData(isTemperatureThresholdsNeeded: Bool)
}

protocol XZoneSettingsViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var cardColor: UIColor { get }
    var baseView: XEditButtonStyle { get }
    var deleteView: XEditButtonStyle { get }
}

protocol XZoneSettingsViewStringsProtocol {
    var nameTitle: String { get }
    var deleteText: String { get }
    var delay: String { get }
		var bypass: String { get }
		var bypassDesc: String { get }
}

class XZoneSettingsView: UIView {
    public var delegate: XZoneSettingsViewDelegate?
    
    private lazy var style: XZoneSettingsViewStyleProtocol = XZoneSettingsViewStyle()
    private lazy var strings: XZoneSettingsViewStringsProtocol = XZoneSettingsViewStrings()

    private var holderView: UIStackView!
    private var nameView, delayView, temperatureThresholdsView, bypassView, deleteView: XEditButton!
    private var delayIconView: UIImageView!
		private var bypassIconView: UIImageView!
		private let bypassSubView = UILabel()
		private let bypassSubSpaceView = UILabel()
    private var cardLayer: CAShapeLayer!, isReady = false
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        cardLayer = CAShapeLayer()
        cardLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(cardLayer)
        
        holderView = UIStackView()
        holderView.axis = .vertical
        holderView.spacing = 10
        addSubview(holderView)
        
        holderView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            holderView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            holderView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.iconSize + XNavigationView.marginLeft * 2),
            holderView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
        ])

        nameView = XEditButton(style: style.baseView)
        nameView.headerTitle = strings.nameTitle
        nameView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        holderView.addArrangedSubview(nameView)
        
        delayView = XEditButton(
            style: XEditButtonStyle(
                title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20))
            )
        )
        delayView.isHidden = true
        delayView.title = strings.delay
        delayView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        holderView.addArrangedSubview(delayView)
        
        delayIconView = UIImageView()
        delayView.addSubview(delayIconView)
        
        delayIconView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            delayIconView.centerYAnchor.constraint(equalTo: delayView.centerYAnchor),
            delayIconView.trailingAnchor.constraint(equalTo: delayView.trailingAnchor),
            delayIconView.heightAnchor.constraint(equalToConstant: 40),
            delayIconView.widthAnchor.constraint(equalToConstant: 40)
        ])
        
        temperatureThresholdsView = XEditButton(
            style: XEditButtonStyle(
                header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20)),
                title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
            )
        )
        temperatureThresholdsView.isHidden = true
        temperatureThresholdsView.title = "N_SECTION_TEMP_LIMITS_SET".localized()
        temperatureThresholdsView.headerTitle = "N_WIZ_SECTION_TEMP_LIMITS".localized()
        temperatureThresholdsView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        holderView.addArrangedSubview(temperatureThresholdsView)
			
				bypassView = XEditButton(
						style: XEditButtonStyle(
								title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20))
						)
				)
				bypassView.isHidden = true
				bypassView.title = strings.bypass
				bypassView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
				holderView.addArrangedSubview(bypassView)
				
				bypassIconView = UIImageView()
				bypassIconView.tintColor = UIColor.colorFromHex (0x3ABEFF)
				bypassView.addSubview(bypassIconView)
			
				bypassIconView.translatesAutoresizingMaskIntoConstraints = false
				NSLayoutConstraint.activate([
						bypassIconView.centerYAnchor.constraint(equalTo: bypassView.centerYAnchor),
						bypassIconView.trailingAnchor.constraint(equalTo: bypassView.trailingAnchor, constant: -1.5),
						bypassIconView.heightAnchor.constraint(equalToConstant: 37),
						bypassIconView.widthAnchor.constraint(equalToConstant: 37)
				])
			
				bypassSubView.numberOfLines = 0
				bypassSubView.lineBreakMode = .byWordWrapping
				bypassSubView.textColor = UIColor.colorFromHex(0x414042)
				bypassSubView.font = UIFont(name: "Open Sans", size: 15.5)
				bypassSubView.text = strings.bypassDesc
				bypassView.addSubview(bypassSubView)
				bypassSubView.translatesAutoresizingMaskIntoConstraints = false
				NSLayoutConstraint.activate([
					bypassSubView.topAnchor.constraint(equalTo: bypassView.topAnchor, constant: 40),
					bypassSubView.leadingAnchor.constraint(equalTo: bypassView.leadingAnchor),
					bypassSubView.trailingAnchor.constraint(equalTo: bypassView.trailingAnchor, constant: -45),
				])
			
				bypassSubSpaceView.isHidden = true
				bypassSubSpaceView.translatesAutoresizingMaskIntoConstraints = false
				holderView.addArrangedSubview(bypassSubSpaceView)

        deleteView = XEditButton(style: style.deleteView)
        deleteView.title = strings.deleteText
        deleteView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        holderView.addArrangedSubview(deleteView)
        
        [nameView, delayView, temperatureThresholdsView, deleteView, bypassView]
            .forEach({
                $0?.translatesAutoresizingMaskIntoConstraints = false
                $0?.heightAnchor.constraint(equalToConstant: 54).isActive = true
            })
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        isReady = true
        cardLayer.path = UIBezierPath(
            roundedRect: CGRect(
                origin: .zero,
                size: CGSize(
                    width: frame.size.width,
                    height: holderView.frame.size.height + 20
                )
            ),
            byRoundingCorners: [.bottomLeft, .bottomRight],
            cornerRadii: CGSize(width: 20, height: 0)
        ).cgPath
    }
    
    @objc private func viewTapped(view: UIView) {
        if view == nameView { delegate?.changeNameViewTapped(name: nameView.title ?? "") }
        else if view == deleteView { delegate?.deleteViewTapped() }
        else if view == delayView { delegate?.delayViewTapped() }
				else if view == bypassView { delegate?.bypassViewTapped() }
        else if view == temperatureThresholdsView { delegate?.temperatureThresholdsViewTapped() }
    }
}

extension XZoneSettingsView: XZoneSettingsViewLayer {
    func setData(name: String) {
        nameView.title = name
    }
    
    func setData(isDelay: Bool) {
        delayView.isHidden = false
        delayIconView.image = UIImage(named: !isDelay ? "ic_delay_off" : "ic_delay_on")
        if isReady { layoutIfNeeded() }
    }
	
		func setData(isBypass: Bool) {
				bypassView.isHidden = false
				bypassSubSpaceView.isHidden = false
				var height = bypassSubView.frame.size.height + bypassSubView.frame.origin.y - bypassView.frame.size.height
				if height < 0 { height = 1 }
				bypassSubSpaceView.heightAnchor.constraint(equalToConstant: height).isActive = true
				bypassIconView.image = isBypass ? UIImage(named: "ic_bypass_active") : UIImage(named: "ic_bypass")?.withRenderingMode(.alwaysTemplate)
				if isReady { layoutIfNeeded() }
		}

    func setData(isTemperatureThresholdsNeeded: Bool) {
        temperatureThresholdsView.isHidden = !isTemperatureThresholdsNeeded
        if isReady { layoutIfNeeded() }
    }
}
