//
//  XSiteDelegationListViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSiteDelegationListViewCellStyleProtocol {
    var selectedColor: UIColor { get }
    var titleView: XBaseLableStyle { get }
    var subTitleView: XBaseLableStyle { get }
}

class XSiteDelegationListViewCell: UITableViewCell {
    static var identifier = "XSiteDelegationListViewCell.identifier"

    private var model: XSiteDelegationListViewLayerModel?
    
    private var titleView, subTitleView: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()
        selectedBackgroundView?.layer.cornerRadius = 10
        
        titleView = UILabel()
        contentView.addSubview(titleView)
        
        subTitleView = UILabel()
        subTitleView.numberOfLines = 0
        contentView.addSubview(subTitleView)

        [titleView, subTitleView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 68),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            subTitleView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            subTitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            subTitleView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -16),
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: XSiteDelegationListViewLayerModel) {
        self.model = model
        titleView.text = model.userLogin
        subTitleView.text = model.domain
    }

    public func setStyle(_ style: XSiteDelegationListViewCellStyleProtocol) {
        backgroundColor = .clear
        selectedBackgroundView?.backgroundColor = style.selectedColor
                
        titleView.font = style.titleView.font
        titleView.textColor = style.titleView.color
        titleView.textAlignment = style.titleView.alignment

        subTitleView.font = style.subTitleView.font
        subTitleView.textColor = style.subTitleView.color
        subTitleView.textAlignment = style.subTitleView.alignment
    }
}
