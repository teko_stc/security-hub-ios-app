//
//  XSiteDelegateViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteDelegateViewStrings: XSiteDelegateViewStringsProtocol {
    /// Код делегирования:
    var delegationCode: String { "DELEG_CODE".localized() }
    
    /// Получить код
    var getDelegationCode: String { "DELEG_GET_CODE".localized() }
    
    /// Вы можете передать ваш объект под управление в другой домен. Для этого получите код объекта и передайте его администратору этого домена.
    var delegationDescription: String { "DELEG_MESS".localized() }
}
