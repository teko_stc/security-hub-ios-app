//
//  XPushNotificationSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XPushNotificationSettingsControllerStrings: XPushNotificationSettingsControllerStringsProtocol {
    
    /// Настройки событий
    var title: String { "PREF_EVENT_SETTINGS_SUBTITLE".localized() }
    
    /// Включить уведомления
    var needPushTitle: String { "NOTIF_STATUS_SWITCH_TITLE".localized() }
    
    /// Мелодия
    var soundTitle: String { "NOTIFICATION_MELODY".localized() }
    
    /// Вы отключили уведомления для данного класса событий
    var noPushRight: String { "NOTIF_PREF_CLASS_OFF_MESS".localized() }
    
    var sounds: [String : String] {
        [
            /// По умолчанию
            "default" : "SUMMARY_DEFAULT".localized(),
            /// Монетка
            "coin.wav" : "sounds_coin".localized(),
            /// Сирена
            "sirena.wav": "sounds_sirena".localized(),
            /// SMS
            "sms.wav": "sounds_sms".localized()
        ]
    }
}
