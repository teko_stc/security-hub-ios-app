//
//  XAgreementControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAgreementView {
    func style() -> Style {
        return Style(backgroungColor: UIColor.white,
                     text: Style.Label(color: UIColor.colorFromHex(0x414042),
                                       font: UIFont(name: "Open Sans",
                                                    size: 20))
        )
    }
}
