//
//  XAgreementControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XAgreementControllerStrings {
    var title: String { "LA_TITLE".localized() }
}

class XAgreementViewStrings {
    /// Об условиях использования программного обеспечения
    var agreementText: String { "LICENSE_TEXT".localized() }
}
