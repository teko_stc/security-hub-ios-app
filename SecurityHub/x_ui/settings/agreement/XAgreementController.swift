//
//  XAgreementController.swift
//  SecurityHub
//
//  Created by Daniil on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAgreementController: XBaseViewController<XAgreementView> {
    override var tabBarIsHidden: Bool { true }
    
    private let string = XAgreementControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = string.title
        setNavigationViewBuilder(navigationViewBuilder)
    }
}
