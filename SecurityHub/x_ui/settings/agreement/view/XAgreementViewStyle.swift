//
//  XAgreementViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XAgreementViewStyle {
    struct Label {
        let color: UIColor
        let font: UIFont?
    }
    
    let backgroungColor: UIColor
    let text: Label
}

extension XAgreementView {
    typealias Style = XAgreementViewStyle
}
