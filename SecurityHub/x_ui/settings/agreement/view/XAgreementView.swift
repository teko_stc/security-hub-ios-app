//
//  XAgreementView.swift
//  SecurityHub
//
//  Created by Daniil on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAgreementView: UIView {
    
    private let strings = XAgreementViewStrings()
    
    private var scrollView: UIScrollView = UIScrollView()
    private var textView: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroungColor
        
        addSubview(scrollView)
        
        textView.text = strings.agreementText
        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.numberOfLines = 0
        scrollView.addSubview(textView)
    }
    
    private func setConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,
                                            constant: 14),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: widthAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            
            textView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            textView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,
                                              constant: 24),
            textView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            textView.widthAnchor.constraint(equalTo: widthAnchor,
                                            constant: -24),
            textView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor,
                                             constant: -14),
        ])
    }
}
