//
//  XChangeUserView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XChangeUserViewDelegate {
    func changeNameViewTapped(name: String)
    func changeLoginViewTapped(login: String)
    func changePasswordViewTapped()
    func activateDeactivateViewTapped()
    func changeRoleViewTapped()
    func deteleViewTapped()
}

protocol XChangeUserViewLayer {
    func setUserData(name: String, login: String, isActive: Bool, roleName: String)
}

protocol XChangeUserViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var cardColor: UIColor { get }
    var baseView: XEditButtonStyle { get }
    var deleteView: XEditButtonStyle { get }
}

protocol XChangeUserViewStringsProtocol {
    var nameTitle: String { get }
    var loginTitle: String { get }
    var changePasswordText: String { get }
    var activateText: String { get }
    var deactivateText: String { get }
    var roleTitle: String { get }
    var deleteText: String { get }
}

class XChangeUserView: UIView {
    public var delegate: XChangeUserViewDelegate?
    
    private lazy var style: XChangeUserViewStyleProtocol = XChangeUserViewStyle()
    private lazy var strings: XChangeUserViewStringsProtocol = XChangeUserViewStrings()

    private var nameView, loginView, passwordView, activateDeactivateView, roleView, deleteView: XEditButton!
    private var cardLayer: CAShapeLayer!
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        cardLayer = CAShapeLayer()
        cardLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(cardLayer)
        
        nameView = XEditButton(style: style.baseView)
        nameView.headerTitle = strings.nameTitle
        nameView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(nameView)
        
        loginView = XEditButton(style: style.baseView)
        loginView.headerTitle = strings.loginTitle
        loginView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(loginView)
        
        passwordView = XEditButton(style: style.baseView)
        passwordView.title = strings.changePasswordText
        passwordView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(passwordView)
        
        activateDeactivateView = XEditButton(style: style.baseView)
        activateDeactivateView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(activateDeactivateView)
         
        roleView = XEditButton(style: style.baseView)
        roleView.headerTitle = strings.roleTitle
        roleView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(roleView)
        
        deleteView = XEditButton(style: style.deleteView)
        deleteView.title = strings.deleteText
        deleteView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(deleteView)
        
        [nameView, loginView, passwordView, activateDeactivateView, roleView, deleteView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            nameView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.iconSize + XNavigationView.marginLeft * 2),
            nameView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            nameView.heightAnchor.constraint(equalToConstant: 54),
            
            loginView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 20),
            loginView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            loginView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            loginView.heightAnchor.constraint(equalToConstant: 54),

            passwordView.topAnchor.constraint(equalTo: loginView.bottomAnchor, constant: 20),
            passwordView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            passwordView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),

            activateDeactivateView.topAnchor.constraint(equalTo: passwordView.bottomAnchor, constant: 20),
            activateDeactivateView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            activateDeactivateView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),

            roleView.topAnchor.constraint(equalTo: activateDeactivateView.bottomAnchor, constant: 20),
            roleView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            roleView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            roleView.heightAnchor.constraint(equalToConstant: 54),

            deleteView.topAnchor.constraint(equalTo: roleView.bottomAnchor, constant: 20),
            deleteView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            deleteView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            deleteView.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardLayer.path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: CGSize(width: frame.size.width, height: deleteView.frame.origin.y + deleteView.frame.size.height + 40)), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 0)).cgPath
    }
    
    @objc private func viewTapped(view: UIView) {
        if view == nameView { delegate?.changeNameViewTapped(name: nameView.title ?? "") }
        else if view == loginView { delegate?.changeLoginViewTapped(login: loginView.title ?? "") }
        else if view == passwordView { delegate?.changePasswordViewTapped() }
        else if view == activateDeactivateView { delegate?.activateDeactivateViewTapped() }
        else if view == roleView { delegate?.changeRoleViewTapped() }
        else if view == deleteView { delegate?.deteleViewTapped() }
    }
}

extension XChangeUserView: XChangeUserViewLayer {
    func setUserData(name: String, login: String, isActive: Bool, roleName: String) {
        nameView.title = name
        loginView.title = login
        activateDeactivateView.title = !isActive ? strings.activateText : strings.deactivateText
        roleView.title = roleName
    }
}
 
