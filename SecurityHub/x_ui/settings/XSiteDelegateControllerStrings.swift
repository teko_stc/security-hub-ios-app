//
//  XSiteDelegateControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteDelegateControllerStrings: XSiteDelegateControllerStringsProtocol {
    /// Делегирование объекта
    var title: String { "TITLE_OBJECT_DELEGATE".localized() }
    
    var copyAlert: XAlertViewStrings {
        XAlertViewStrings(
            /// Скопировано в буфер обмена
            title: "COPIED_IN_BUFFER".localized(),
            /// OK
            positive: "N_BUTTON_OK".localized()
        )
    }
}
