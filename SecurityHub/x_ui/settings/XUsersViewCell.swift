//
//  XUsersViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XUsersViewCellStyle {
    let backgroundColor, selectColor: UIColor
    let titleView, subTitleView: XBaseLableStyle
}

class XUsersViewCell: UITableViewCell {
    static var identifier = "XUsersViewCell.identifier"

    private var model: XUsersViewLayerModel?
    
    private var iconView: UIImageView!
    private var titleView, subTitleView: UILabel!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()
        selectedBackgroundView?.layer.cornerRadius = 10
        
        iconView = UIImageView()
        contentView.addSubview(iconView)
        
        titleView = UILabel()
        contentView.addSubview(titleView)
        
        subTitleView = UILabel()
        subTitleView.numberOfLines = 0
        contentView.addSubview(subTitleView)

        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
 
    private func setConstraints() {
        [iconView, titleView, subTitleView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            iconView.heightAnchor.constraint(equalToConstant: 40),
            iconView.widthAnchor.constraint(equalToConstant: 40),
            
            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 14),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            subTitleView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            subTitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            subTitleView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -16),
        ])
    }
    
    public func setContent(model: XUsersViewLayerModel) {
        self.model = model
        iconView.image = UIImage(named: model.isActive ? "ic_pozharny_small-2" : "ic_pozharny_small")
        titleView.text = model.name
        subTitleView.text = model.role
    }

    public func setStyle(_ style: XUsersViewCellStyle) {
        backgroundColor = style.backgroundColor
        selectedBackgroundView?.backgroundColor = style.selectColor
                
        titleView.font = style.titleView.font
        titleView.textColor = style.titleView.color
        titleView.textAlignment = style.titleView.alignment

        subTitleView.font = style.subTitleView.font
        subTitleView.textColor = style.subTitleView.color
        subTitleView.textAlignment = style.subTitleView.alignment
    }
}
