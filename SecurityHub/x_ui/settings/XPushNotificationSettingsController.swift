//
//  XPushNotofocationSettingsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


protocol XPushNotificationSettingsControllerStringsProtocol {
    var title: String { get }
    var needPushTitle: String { get }
    var soundTitle: String { get }
    var noPushRight: String { get }
    var sounds: [String:String] { get }
}

class XPushNotificationSettingsController: XBaseViewController<XBaseSettingsView>, XBaseSettingsViewDelegate {
    override var tabBarIsHidden: Bool { true }
    
    private var _class: D3ClassModel?
    
    private lazy var viewLayer: XBaseSettingsViewLayer = self.mainView
    private lazy var strings: XPushNotificationSettingsControllerStringsProtocol = XPushNotificationSettingsControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    init(_class: D3ClassModel? = nil) {
        self._class = _class
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
        if let _class = _class {
            title = _class.caption
            let enable = (DataManager.settingsHelper.pushMask & (1 << _class.id)) != 0
            var items: [XBaseSettingsViewLayerModelItemProtocol] = []
            if enable {
                let name = strings.sounds[DataManager.settingsHelper.getPushSound(class_id: _class.id)]
                items.append(XBaseSettingsCellModel(isActive: true, title: strings.soundTitle, subTitle: name))
            } else {
                items.append(XBaseSettingsCellModel(isActive: false, title: strings.noPushRight, subTitle: nil))
            }
            viewLayer.setItems(
                [
                    XBaseSettingsViewLayerModel(
                        title: nil,
                        headerObject: XBaseSettingsViewLayerModelPushClassHeader(icon: _class.iconBig, switchTitle: strings.needPushTitle, isOn: enable),
                        items: items
                    )
                ]
            )
            viewLayer.setAltBackgroundColor()
        } else {
            title = strings.title
            viewLayer.setItems(
                [
                    XBaseSettingsViewLayerModel(
                        title: nil,
                        items: DataManager.shared.d3Const.getClassTypes().map({ XBaseSettingsCellModel(isActive: true, title: $0.caption, subTitle: nil) })
                    )
                ]
            )
        }
    }
    
    func selectedCell(model: XBaseSettingsViewLayerModelItemProtocol) {
        if let _class = DataManager.shared.d3Const.getClassTypes().first(where: { $0.caption == model.title }) {
            let controller = XPushNotificationSettingsController(_class: _class)
            navigationController?.pushViewController(controller, animated: true)
        } else if let _class = _class {
            let keyClosser: (Int) -> String = { id in
                switch (id) {
                case 0: return "alarm"
                case 1: return "sabotage"
                case 2: return "malfunction"
                case 3: return "attention"
                case 4: return "arm"
                default: return "nil"
                }
            }
            let path = (DataManager.shared.sounds[keyClosser(_class.id)] as? String) ?? "default"
            let controller = SelectSoundController(path: path, sounds: strings.sounds, void: { [weak self] name in
                if let id = self?._class?.id {
                    DataManager.settingsHelper.setPushSound(class_id: id, value: name ?? "default")
                    self?.viewDidLoad()
                    _ = DataManager.shared.setSound(class_id: id.int64, name: name ?? "default").subscribe()
                }
            })
            navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    func switchHeader(model: XBaseSettingsViewLayerModel, section: Int, oldValue: Bool) {
        if oldValue {
            DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask & ~(1 << _class!.id)
        } else {
            DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask | (1 << _class!.id)
        }
        DataManager.shared.setFCM()
        var items: [XBaseSettingsViewLayerModelItemProtocol] = []
        if !oldValue {
            let name = strings.sounds[DataManager.settingsHelper.getPushSound(class_id: _class!.id)]
            if _class!.id < 5 { items.append(XBaseSettingsCellModel(isActive: true, title: strings.soundTitle, subTitle: name)) }
        } else {
            items.append(XBaseSettingsCellModel(isActive: false, title: strings.noPushRight, subTitle: nil))
        }
        viewLayer.updHeader(
            section: 0,
            model: XBaseSettingsViewLayerModel(
                title: nil,
                headerObject: XBaseSettingsViewLayerModelPushClassHeader(icon: _class!.iconBig, switchTitle: strings.needPushTitle, isOn: !oldValue),
                items: items
            )
        )
    }
    
    func switchCell(model: XBaseSettingsViewLayerModelItemProtocol, index: IndexPath, oldValue: Bool) { }
}

