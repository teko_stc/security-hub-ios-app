//
//  XDeviceSettingsLocalUsersController.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsLocalUsersController: XBaseViewController<XDeviceSettingsLocalUsersView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        ),
        titleView: XBaseLableStyle(
            color: UIColor.colorFromHex(0x414042),
            font: UIFont(name: "Open Sans", size: 25),
            alignment: .left
        ),
        rightViews: [
            XNavigationViewRightViewBuilder(
                imageName: "ic_add",
                color: UIColor.colorFromHex(0x414042),
                viewTapped: self.add
            )
        ]
    )

    private var deviceId: Int64
    
    init(deviceId: Int64) {
        self.deviceId = deviceId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getDeviceInfo()
    }

    func add() {
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId) else {
            return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized())
        }
        
        let sheet = XBottomSheetController(style: mainView.addCameraBottomSheetStyle(), title: nil, selectedIndex: -1, items: [
                XBottomSheetViewItem(title: "button_add_wireline_user".localized(), text: nil, any: 0),
                XBottomSheetViewItem(title: "real_id".localized(), text: "\n", any: 1)
            ]
        )
        sheet.onSelectedItem = { (controller, index, item) in
            if (index == 0) {
                let deviceIds = DataManager.shared.dbHelper.getDeviceIds()
                self.navigationController?.pushViewController(XAddWirelineUserController(device_ids: deviceIds), animated: true)
            } else {
                let deviceIds = DataManager.shared.dbHelper.getDeviceIds()
                self.navigationController?.pushViewController(XFindWirelessController(device_ids: deviceIds, section_id: nil, type: .user), animated: true)
            }
            sheet.dismiss(animated: true)
        }
        navigationController?.present(sheet, animated: true)
    }
    
    private func getDeviceInfo() {
        _ = DataManager.shared.getDevice(device: deviceId)
            .subscribe(onNext: { device in
                self.title = "\(device!.name) (\(device!.id))"
            })
        
        _ = DataManager.shared.getDeviceUsers(device_ids: [deviceId])
            .subscribe(onNext: { deviceUsers, type in
                if deviceUsers.isEmpty {
                    self.mainView.showEmpty()
                } else {
                    deviceUsers.forEach { deviceUser in
                        switch type {
                        case .insert: self.mainView.addDeviceUsers(deviceUsers: deviceUser)
                        case .delete: self.mainView.deleteDeviceUsers(deviceUsers: deviceUser)
                        case .update: self.mainView.updateDeviceUsers(deviceUsers: deviceUser)
                        }
                    }
                }
            })
    }
}

extension XDeviceSettingsLocalUsersController: XDeviceSettingsLocalUsersViewDelegate {
    func userSelected(_ user: DeviceUsers) {
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId) else {
            return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized())
        }

        let controller = XDeviceLocalUserController(user)
        navigationController?.pushViewController(controller, animated: true)
    }
}
