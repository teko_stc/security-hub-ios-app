//
//  XDeviceSettingsLocalUsersViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsLocalUsersViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    let empty: Label
}

extension XDeviceSettingsLocalUsersView {
    typealias Style = XDeviceSettingsLocalUsersViewStyle
}
