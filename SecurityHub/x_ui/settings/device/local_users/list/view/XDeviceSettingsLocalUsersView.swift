//
//  XDeviceSettingsLocalUsersView.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsLocalUsersView: UIView {
    
    public var delegate: XDeviceSettingsLocalUsersViewDelegate!
    
    private let strings: XDeviceSettingsLocalUserViewStrings = XDeviceSettingsLocalUserViewStrings()
    
    private var deviceUsers: [DeviceUsers] = []
    
    private var tableView: UITableView = UITableView()
    private var emptyLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        addSubview(tableView)
        
        emptyLabel.text = strings.empty
        emptyLabel.font = style.empty.font
        emptyLabel.textColor = style.empty.color
        emptyLabel.isHidden = true
        addSubview(emptyLabel)
    }
    
    private func setConstraints() {
        [tableView, emptyLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -24),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            emptyLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            emptyLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    public func addDeviceUsers(deviceUsers: DeviceUsers) {
        self.deviceUsers.append(deviceUsers)
        
        tableView.reloadData()
    }
    
    public func updateDeviceUsers(deviceUsers: DeviceUsers) {
        self.deviceUsers = self.deviceUsers.map {
            $0.id == deviceUsers.id ? deviceUsers : $0
        }
        
        tableView.reloadData()
    }
    
    public func deleteDeviceUsers(deviceUsers: DeviceUsers) {
        self.deviceUsers.removeAll(where: { $0.id == deviceUsers.id })
        
        tableView.reloadData()
    }
    
    public func showEmpty() {
        emptyLabel.isHidden = false
    }
}

extension XDeviceSettingsLocalUsersView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deviceUser = deviceUsers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        
        var sections = "\(DataManager.shared.dbHelper.getUserSectionIds(deviceUser: deviceUser))"
        sections.removeFirst(); sections.removeLast()
        
        cell.setContent(
            Cell.Model(
                name: deviceUser.name,
                device: deviceUser.device,
                userIndex: deviceUser.userIndex,
                sections: sections
            )
        )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate.userSelected(deviceUsers[indexPath.row])
    }
}
