//
//  XDeviceSettingsLocalUsersCellStrings.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XDeviceSettingsLocalUsersCellStrings {
    func infoText(userId: Int64, sections: String) -> String { "USERS_USER_NUM".localized() + " \(userId)\n" + "USERS_SECTIONS_TITLE".localized() + " \(sections)" }
}
