//
//  XDeviceSettingsLocalUserCellStyle.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsLocalUsersCellStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }

    let image: String
    let name: Label
    let info: Label
}

extension XDeviceSettingsLocalUsersCell {
    typealias Style = XDeviceSettingsLocalUsersCellStyle
    
    func cellStyle() -> Style {
        return Style(
            image: "ic_local_user",
            name: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            info: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.9)
            )
        )
    }
}
