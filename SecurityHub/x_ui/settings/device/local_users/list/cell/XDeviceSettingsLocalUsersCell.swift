//
//  XDeviceSettingsLocalUsersCell.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsLocalUsersCell: UITableViewCell {
    
    static let identifier = "XDeviceSettingsLocalUsersCell.identifier"
    
    private let strings: XDeviceSettingsLocalUsersCellStrings = XDeviceSettingsLocalUsersCellStrings()
    
    private var iconView: UIImageView = UIImageView()
    private var nameLabel: UILabel = UILabel()
    private var infoLabel: UILabel = UILabel()
    
    private var model: Model?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        initViews(cellStyle())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        iconView.image = UIImage(named: style.image)
        addSubview(iconView)
        
        nameLabel.font = style.name.font
        nameLabel.textColor = style.name.color
        nameLabel.numberOfLines = 0
        addSubview(nameLabel)
        
        infoLabel.font = style.info.font
        infoLabel.textColor = style.info.color
        infoLabel.numberOfLines = 0
        addSubview(infoLabel)
    }
    
    private func setConstraints() {
        [iconView, nameLabel, infoLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: 76),
            iconView.heightAnchor.constraint(equalToConstant: 76),
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 23),
            
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            nameLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            infoLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 6),
            infoLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: nameLabel.trailingAnchor),
            
            bottomAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 12)
        ])
    }
    
    public func setContent(_ model: Model) {
        self.model = model
        
        nameLabel.text = model.name
        
        infoLabel.text = strings.infoText(userId: model.userIndex, sections: model.sections)
    }
}

extension XDeviceSettingsLocalUsersView {
    typealias Cell = XDeviceSettingsLocalUsersCell
}
