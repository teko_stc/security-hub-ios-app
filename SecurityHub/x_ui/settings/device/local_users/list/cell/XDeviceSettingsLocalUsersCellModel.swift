//
//  XDeviceSettingsLocalUsersCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 16.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XDeviceSettingsLocalUsersCellModel {
    let name: String
    let device: Int64
    let userIndex: Int64
    let sections: String
}

extension XDeviceSettingsLocalUsersCell {
    typealias Model = XDeviceSettingsLocalUsersCellModel
}
