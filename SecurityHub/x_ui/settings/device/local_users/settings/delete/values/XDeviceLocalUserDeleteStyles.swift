//
//  XDeviceLocalUserDeleteStyles.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XDeviceLocalUserDeleteView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            title: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 25)
            ),
            text: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20)
            ),
            button: XZoomButton.Style(
                backgroundColor: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor.white
            )
        )
    }
}
