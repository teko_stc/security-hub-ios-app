//
//  XDeviceLocalUserDeleteStrings.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Localize_Swift

class XDeviceLocalUserDeleteStrings {
    func title(user: String) -> String {
        "N_DELETE_USER_TITLE".localized() + " \(user)\"?".localized()
    }
    
    var text: String { "N_DELETE_USER_MESSAGE".localized() }
    var confirm: String { "N_DELETE".localized() }
}
