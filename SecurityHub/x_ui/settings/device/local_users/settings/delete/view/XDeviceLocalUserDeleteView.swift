//
//  XDeviceLocalUserDeleteView.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XDeviceLocalUserDeleteView: UIView {
    
    private let strings: XDeviceLocalUserDeleteStrings = XDeviceLocalUserDeleteStrings()
    
    public var delegate: XDeviceLocalUserDeleteViewDelegate!
    
    private var titleLabel: UILabel = UILabel()
    private var textLabel: UILabel = UILabel()
    private var confirmButton: XZoomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        titleLabel.font = style.title.font
        titleLabel.textColor = style.title.color
        titleLabel.numberOfLines = 0
        addSubview(titleLabel)
        
        textLabel.text = strings.text
        textLabel.font = style.text.font
        textLabel.textColor = style.text.color
        textLabel.numberOfLines = 0
        addSubview(textLabel)
        
        confirmButton = XZoomButton(style: style.button)
        confirmButton.text = strings.confirm
        confirmButton.addTarget(self, action: #selector(confirmTapped), for: .touchUpInside)
        addSubview(confirmButton)
    }
    
    private func setConstraints() {
        [titleLabel, textLabel, confirmButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            
            textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 60),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 60),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60),
            
            confirmButton.heightAnchor.constraint(equalToConstant: 40),
            confirmButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -40),
            confirmButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 80),
            confirmButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -80)
        ])
    }
    
    public func setContent(_ user: String) {
        titleLabel.text = strings.title(user: user)
    }
    
    @objc private func confirmTapped() {
        delegate?.confirmTapped()
    }
}
