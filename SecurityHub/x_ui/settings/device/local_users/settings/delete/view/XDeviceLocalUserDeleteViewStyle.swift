//
//  XDeviceLocalUserDeleteViewStrings.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XDeviceLocalUserDeleteViewStyle {
    let backgroundColor: UIColor
    
    let title: XBaseLableStyle
    let text: XBaseLableStyle
    
    let button: XZoomButtonStyle
}

extension XDeviceLocalUserDeleteView {
    typealias Style = XDeviceLocalUserDeleteViewStyle
}
