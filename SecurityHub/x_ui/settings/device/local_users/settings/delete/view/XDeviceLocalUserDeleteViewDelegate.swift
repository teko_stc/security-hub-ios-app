//
//  XDeviceLocalUserDeleteViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

protocol XDeviceLocalUserDeleteViewDelegate {
    func confirmTapped()
}
