//
//  XDeviceLocalUserDeleteController.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 30.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XDeviceLocalUserDeleteController: XBaseViewController<XDeviceLocalUserDeleteView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceUsers: DeviceUsers
    
    init(_ deviceUsers: DeviceUsers) {
        self.deviceUsers = deviceUsers
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        mainView.setContent(deviceUsers.name)
        setNavigationViewBuilder(navigationViewBuilder)
    }
}

extension XDeviceLocalUserDeleteController: XDeviceLocalUserDeleteViewDelegate {
    func confirmTapped() {
        _ = DataManager.shared.delHozOrgan(
            device: deviceUsers.device,
            index: deviceUsers.userIndex
        ).subscribe(onNext: { _ in
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.popViewController(animated: true)
        })
    }
}
