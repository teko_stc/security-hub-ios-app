//
//  XDeviceLocalUserController.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDeviceLocalUserController: XBaseViewController<XDeviceLocalUserView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private let strings: XDeviceLocalUserStrings = XDeviceLocalUserStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceUsers: DeviceUsers
    
    init(_ deviceUsers: DeviceUsers) {
        self.deviceUsers = deviceUsers
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        updateViewsData()
        setNavigationViewBuilder(navigationViewBuilder)
        
        addDeviceObserver()
    }
    
    private func addDeviceObserver() {
        NotificationCenter.default.addObserver(
            forName: HubNotification.deviceUsersUpdate(deviceUsers.device),
            object: nil,
            queue: OperationQueue.main
        ) { notification in
            let notification = notification.object as! (deviceUsers: [DeviceUsers], _: NUpdateType)
            
            self.deviceUsers = notification.deviceUsers.first!
            
            self.updateViewsData()
        }
    }
    
    private func updateViewsData() {
        mainView.setData(name: deviceUsers.name)
    }
}

extension XDeviceLocalUserController: XDeviceLocalUserViewDelegate {
    func nameViewClicked() {
        let controller = XTextEditController(
            title: strings.nameViewHeader,
            value: deviceUsers.name
        ) { controller, value in
            self.deviceUsers.name = value
            
            _ = DataManager.shared.setHozOrgan(
                device: self.deviceUsers.device,
                name: self.deviceUsers.name,
                index: self.deviceUsers.userIndex,
                sections: nil
            )
            .subscribe(onNext: { _ in
                DataManager.shared.dbHelper.updateDeviceUsers(deviceUsers: self.deviceUsers)
                
                controller.navigationController?.popViewController(animated: true)
            })
        }
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func delegationViewClicked() {
        _ = DataManager.shared.getSections(device: deviceUsers.device)
            .concatMap({ (res) -> Observable<Sections> in
                return Observable.from(res)
            })
            .filter({ (sec) -> Bool in return sec.detector == 1 })
            .concatMap({ (sec) -> Observable<(Sections, [Int64])> in
                return Observable.zip(
                    Observable.just(sec),
                    DataManager.shared.getUserSection(user: self.deviceUsers)
                )
            })
            .map({ (sec, sections) -> SelectElement in
                return SelectElement(
                    name: sec.name,
                    selected: sections.contains(sec.section),
                    obj: (sec, sections)
                )
            })
            .toArray()
            .asObservable()
            .do(onNext: { list in
                var items: [XMultiChoiceAlertView.Cell.Model] = []
                
                for i in 0..<list.count {
                    items.append(
                        XMultiChoiceAlertView.Cell.Model(
                            title: list[i].name,
                            subTitle: self.strings.sectionNumber(i + 1),
                            isChecked: list[i].selected ?? false,
                            value: list[i].obj!
                        )
                    )
                }
                
                let alert = XMultiChoiceAlertController(
                    title: self.strings.selectSections,
                    items: items,
                    positiveText: self.strings.positive,
                    negativeText: self.strings.negative
                ) { selectedItems in
                    guard !selectedItems.isEmpty else { return self.showToast(message: self.strings.noSectionsSelected) }
                    
                    let items = selectedItems as! [(Sections, [Int64])]
                    
                    _ = DataManager.shared.setHozOrgan(
                        device: self.deviceUsers.device,
                        name: self.deviceUsers.name,
                        index: self.deviceUsers.userIndex,
                        sections: items.map { $0.0.section }
                    )
                    .subscribe(onNext: { _ in
                        let di = self.deviceUsers.device, ui = self.deviceUsers.userIndex
                        DataManager.shared.dbHelper.deleteUserSection(userId: DeviceUsers.getId(di, ui))
                        
                        items.forEach { (section, _) in
                            let s = section.section
                            let us = UserSection(id: UserSection.getId(di, ui, s),
                                                 userId: DeviceUsers.getId(di, ui),
                                                 sectionId: Sections.getId(di, s), time: self.deviceUsers.time)
                            DataManager.shared.dbHelper.add(userSection: us)
                        }
                    })
                }
                
                self.navigationController?.present(alert, animated: true)
            })
            .subscribe()
    }
    
    func deleteViewClicked() {
        let controller = XDeviceLocalUserDeleteController(deviceUsers)
        navigationController?.pushViewController(controller, animated: true)
    }
}
