//
//  XDeviceLocalUserStrings.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

class XDeviceLocalUserStrings {
    var nameViewHeader: String { "N_LOCAL_USER_NAME".localized() }
    var delegationViewTitle: String { "N_LOCAL_USER_SETTINGS_PARTITIONS".localized() }
    var deleteViewTitle: String { "N_LOCAL_USER_SETTINGS_DELETE".localized() }
    
    var selectSections: String { "N_LOCAL_USER_SETTINGS_SELECT_PARTITIONS".localized() }
    var positive: String { "N_BUTTON_OK".localized() }
    var negative: String { "N_BUTTON_CANCEL".localized() }
    var noSectionsSelected: String { "N_LOCAL_USER_SETTINGS_PARTITIONS_NOT_SELECTED".localized() }
    
    func sectionNumber(_ number: Int) -> String { "\("N_LOCAL_USER_SETTINGS_PARTITIONS_NUMBER".localized()) \(number)" }
}
