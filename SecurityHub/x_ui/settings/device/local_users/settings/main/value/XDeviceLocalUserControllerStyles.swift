//
//  XDeviceLocalUserControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XDeviceLocalUserView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.colorFromHex(0xECECEC),
            cardViewBackgroundColor: UIColor.white,
            nameView: XEditButtonStyle(
                header: XBaseLableStyle(
                    color: UIColor.colorFromHex(0x414042),
                    font: UIFont(name: "Open Sans", size: 15.5)
                ),
                title: XBaseLableStyle(
                    color: UIColor.colorFromHex(0x414042),
                    font: UIFont(name: "Open Sans", size: 20)
                )
            ),
            delegationView: XEditButtonStyle(
                title: XBaseLableStyle(
                    color: UIColor.colorFromHex(0x414042),
                    font: UIFont(name: "Open Sans", size: 20)
                )
            ),
            deleteView: XEditButtonStyle(
                title: XBaseLableStyle(
                    color: UIColor.colorFromHex(0xF9543E),
                    font: UIFont(name: "Open Sans", size: 20)
                )
            )
        )
    }
}
