//
//  XDeviceLocalUserViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

protocol XDeviceLocalUserViewDelegate {
    func nameViewClicked()
    func delegationViewClicked()
    func deleteViewClicked()
}
