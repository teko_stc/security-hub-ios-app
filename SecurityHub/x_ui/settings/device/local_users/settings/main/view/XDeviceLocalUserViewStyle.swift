//
//  XDeviceLocalUserViewStyle.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XDeviceLocalUserViewStyle {
    let backgroundColor: UIColor
    let cardViewBackgroundColor: UIColor
    
    let nameView, delegationView, deleteView: XEditButtonStyle
}

extension XDeviceLocalUserView {
    typealias Style = XDeviceLocalUserViewStyle
}
