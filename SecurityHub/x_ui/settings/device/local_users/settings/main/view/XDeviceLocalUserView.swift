//
//  XDeviceLocalUserController.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XDeviceLocalUserView : UIView {
    
    public var delegate: XDeviceLocalUserViewDelegate!
    
    private let strings: XDeviceLocalUserStrings = XDeviceLocalUserStrings()
    
    private var cardView: UIView = UIView()
    private var nameView, delegationView, deleteView: XEditButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        cardView.backgroundColor = style.cardViewBackgroundColor
        cardView.layer.cornerRadius = 40
        addSubview(cardView)
        
        nameView = XEditButton(style: style.nameView)
        nameView.headerTitle = strings.nameViewHeader
        nameView.addTarget(self, action: #selector(nameViewClicked), for: .touchUpInside)
        addSubview(nameView)
        
        delegationView = XEditButton(style: style.delegationView)
        delegationView.title = strings.delegationViewTitle
        delegationView.addTarget(self, action: #selector(delegationViewClicked), for: .touchUpInside)
        addSubview(delegationView)
        
        deleteView = XEditButton(style: style.deleteView)
        deleteView.title = strings.deleteViewTitle
        deleteView.addTarget(self, action: #selector(deleteViewClicked), for: .touchUpInside)
        addSubview(deleteView)
    }
    
    private func setConstraints() {
        [cardView, nameView, delegationView, deleteView].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            nameView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.iconSize + XNavigationView.marginLeft * 2),
            nameView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            nameView.heightAnchor.constraint(equalToConstant: 54),
            
            delegationView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 8),
            delegationView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            delegationView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            delegationView.heightAnchor.constraint(equalToConstant: 54),
            
            deleteView.topAnchor.constraint(equalTo: delegationView.bottomAnchor, constant: 8),
            deleteView.leadingAnchor.constraint(equalTo: delegationView.leadingAnchor),
            deleteView.trailingAnchor.constraint(equalTo: delegationView.trailingAnchor),
            deleteView.heightAnchor.constraint(equalToConstant: 54),
            
            cardView.bottomAnchor.constraint(equalTo: deleteView.bottomAnchor, constant: 30)
        ])
    }
    
    public func setData(name: String) {
        nameView.title = name
    }
    
    @objc private func nameViewClicked() {
        delegate.nameViewClicked()
    }
    
    @objc private func delegationViewClicked() {
        delegate.delegationViewClicked()
    }
    
    @objc private func deleteViewClicked() {
        delegate.deleteViewClicked()
    }
}
