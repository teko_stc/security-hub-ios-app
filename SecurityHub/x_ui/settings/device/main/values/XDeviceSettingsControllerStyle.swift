//
//  XDeviceSettingsControllerStyle.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDeviceSettingsView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
            cardBackground: UIColor.white,
            debugInfo: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectedColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            localUsers: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectedColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            ussdRequest: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectedColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            restart: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectedColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            update: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectedColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.5)
            ),
            detach: Style.Button(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1),
                selectedColor: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 0.5)
            ),
            name: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            title: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            serial: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            version: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}

extension XDeviceSettingsController {
    func confirmAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1)
            ),
            negative: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
