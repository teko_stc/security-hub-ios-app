//
//  XDeviceSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XDeviceSettingsControllerStrings {
    /// USSD-запрос
    var ussdTitle: String { "STA_USSD_REQUEST_MENU_TITLE".localized() }
    var ussdDefault: String { "*100#" }
    
    /// Недостаточно полномочий
    var noRight: String { "N_NO_PERMISSION".localized() }

    var restartAlertText = XAlertView.Strings(
        /// Перезагрузить контроллер?
        title: "CARD_REBOOT_DEVICE".localized(),
        text: nil,
        /// Да
        positive: "YES".localized(),
        /// Нет
        negative: "NO".localized()
    )
    
    var loadUpdateAlertText = XAlertView.Strings(
        /// Загрузить обновление?
        title: "N_DEVICE_SETTINGS_UPDATE_LOAD_Q".localized(),
        text: nil,
        /// Да
        positive: "YES".localized(),
        /// Нет
        negative: "NO".localized()
    )
    
    var updateAlertText = XAlertView.Strings(
        /// Загрузить обновление?
        title: "N_DEVICE_SETTINGS_UPDATE_Q".localized(),
        text: nil,
        /// Да
        positive: "YES".localized(),
        /// Нет
        negative: "NO".localized()
    )
}

class XDeviceSettingsViewStrings {
    /// Отладочная информация
    var debugInfo: String { "N_DEBUG_INFO".localized() }
    
    /// Локальные пользователи контроллера
    var localUsers: String { "N_DEVICE_SETTINGS_LOCAL_USERS".localized() }
    
    /// USSD-запрос
    var ussdRequest: String { "N_DEVICE_SETTINGS_USSD".localized() }
    
    /// Перезагрузить
    var restart: String { "N_DEVICE_SETTINGS_REBOOT".localized() }
    
    /// Загрузить обновление
    var update: String { "N_DEVICE_SETTINGS_UPDATE".localized() }
    
    /// Отвязать от объекта
    var detach: String { "N_DEVICE_SETTINGS_REVOKE".localized() }
    
    /// Серийный номер
    var serial: String { "N_BOTTOM_INFO_SERIAL_NUMBER".localized() }
    
    /// Версия ПО
    var version: String { "N_DEVICE_SETTINGS_TITLE_VERSION".localized() }
}
