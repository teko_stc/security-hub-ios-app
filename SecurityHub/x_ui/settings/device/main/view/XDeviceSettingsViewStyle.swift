//
//  XDeviceSettingsViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsViewStyle {
    struct Button {
        let font: UIFont?
        let color: UIColor
        let selectedColor: UIColor
    }
    
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    let cardBackground: UIColor
    
    let debugInfo: Button
    let localUsers: Button
    let ussdRequest: Button
    let restart: Button
    let update: Button
    let detach: Button
    
    let name: Label
    let title: Label
    let serial: Label
    let version: Label
}

extension XDeviceSettingsView {
    typealias Style = XDeviceSettingsViewStyle
}
