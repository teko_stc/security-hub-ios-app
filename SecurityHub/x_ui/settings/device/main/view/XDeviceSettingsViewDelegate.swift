//
//  XDeviceSettingsViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XDeviceSettingsViewDelegate {
    func debugInfoTapped()
    func localUsersTapped()
    func ussdTapped()
    func restartTapped()
    func updateTapped()
    func detachTapped()
}
