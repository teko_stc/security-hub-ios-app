//
//  XDeviceSettingsView.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsView: UIView {
    
    public var delegate: XDeviceSettingsViewDelegate?
    
    private let strings: XDeviceSettingsViewStrings = XDeviceSettingsViewStrings()
    
    private var scrollView: UIScrollView!
    private var cntView: UIView = UIView()

    private var cardView: UIView = UIView()
    private var optionsStackView: UIStackView = UIStackView()
    private var debugInfoButton: UIButton = UIButton()
    private var localUsersButton: UIButton = UIButton()
    private var ussdRequestButton: UIButton = UIButton()
    private var restartButton: UIButton = UIButton()
    private var updateButton: UIButton = UIButton()
    private var detachButton: UIButton = UIButton()
    private var nameLabel: UILabel = UILabel()
    private var imageView: UIImageView = UIImageView()
    private var serialNumberTitleLabel: UILabel = UILabel()
    private var serialNumberLabel: UILabel = UILabel()
    private var versionTitleLabel: UILabel = UILabel()
    private var versionLabel: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        scrollView = UIScrollView()
        addSubview(scrollView)
        
        scrollView.addSubview(cntView)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
        
        cardView.backgroundColor = style.cardBackground
        cardView.layer.cornerRadius = 40
        cntView.addSubview(cardView)
        
        optionsStackView.axis = .vertical
        optionsStackView.distribution = .equalSpacing
        optionsStackView.spacing = 20
        cntView.addSubview(optionsStackView)
        
        debugInfoButton.setTitle(strings.debugInfo, for: .normal)
        debugInfoButton.setTitleColor(style.debugInfo.color, for: .normal)
        debugInfoButton.setTitleColor(style.debugInfo.selectedColor, for: .highlighted)
        debugInfoButton.titleLabel?.font = style.debugInfo.font
        debugInfoButton.titleLabel?.numberOfLines = 0
        debugInfoButton.contentHorizontalAlignment = .left
        debugInfoButton.addTarget(self, action: #selector(debugInfoTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(debugInfoButton)
        
        localUsersButton.setTitle(strings.localUsers, for: .normal)
        localUsersButton.setTitleColor(style.localUsers.color, for: .normal)
        localUsersButton.setTitleColor(style.localUsers.selectedColor, for: .highlighted)
        localUsersButton.titleLabel?.font = style.localUsers.font
        localUsersButton.titleLabel?.numberOfLines = 0
        localUsersButton.contentHorizontalAlignment = .left
        localUsersButton.addTarget(self, action: #selector(localUsersTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(localUsersButton)
        
        ussdRequestButton.setTitle(strings.ussdRequest, for: .normal)
        ussdRequestButton.setTitleColor(style.ussdRequest.color, for: .normal)
        ussdRequestButton.setTitleColor(style.ussdRequest.selectedColor, for: .highlighted)
        ussdRequestButton.titleLabel?.font = style.ussdRequest.font
        ussdRequestButton.titleLabel?.numberOfLines = 0
        ussdRequestButton.contentHorizontalAlignment = .left
        ussdRequestButton.addTarget(self, action: #selector(ussdTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(ussdRequestButton)
        
        restartButton.setTitle(strings.restart, for: .normal)
        restartButton.setTitleColor(style.restart.color, for: .normal)
        restartButton.setTitleColor(style.restart.selectedColor, for: .highlighted)
        restartButton.titleLabel?.font = style.restart.font
        restartButton.titleLabel?.numberOfLines = 0
        restartButton.contentHorizontalAlignment = .left
        restartButton.addTarget(self, action: #selector(restartTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(restartButton)
        
        updateButton.setTitle(strings.update, for: .normal)
        updateButton.setTitleColor(style.update.color, for: .normal)
        updateButton.setTitleColor(style.update.selectedColor, for: .highlighted)
        updateButton.titleLabel?.font = style.update.font
        updateButton.titleLabel?.numberOfLines = 0
        updateButton.contentHorizontalAlignment = .left
        updateButton.addTarget(self, action: #selector(updateTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(updateButton)
        
        detachButton.setTitle(strings.detach, for: .normal)
        detachButton.setTitleColor(style.detach.color, for: .normal)
        detachButton.setTitleColor(style.detach.selectedColor, for: .highlighted)
        detachButton.titleLabel?.font = style.detach.font
        detachButton.titleLabel?.numberOfLines = 0
        detachButton.contentHorizontalAlignment = .left
        detachButton.addTarget(self, action: #selector(detachTapped), for: .touchUpInside)
        optionsStackView.addArrangedSubview(detachButton)
        
        nameLabel.textColor = style.name.color
        nameLabel.font = style.name.font
        cntView.addSubview(nameLabel)
        
        cntView.addSubview(imageView)
        
        serialNumberTitleLabel.text = strings.serial
        serialNumberTitleLabel.font = style.title.font
        serialNumberTitleLabel.textColor = style.title.color
        cntView.addSubview(serialNumberTitleLabel)
        
        serialNumberLabel.font = style.serial.font
        serialNumberLabel.textColor = style.serial.color
        cntView.addSubview(serialNumberLabel)
        
        versionTitleLabel.text = strings.version
        versionTitleLabel.font = style.title.font
        versionTitleLabel.textColor = style.title.color
        cntView.addSubview(versionTitleLabel)
        
        versionLabel.font = style.version.font
        versionLabel.textColor = style.version.color
        cntView.addSubview(versionLabel)
    }
    
    private func setConstraints() {
        [scrollView, cntView, cardView, optionsStackView, nameLabel, imageView, serialNumberTitleLabel, serialNumberLabel, versionTitleLabel, versionLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            cntView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            cntView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            cntView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),

            cardView.topAnchor.constraint(equalTo: cntView.topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: cntView.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: cntView.trailingAnchor),
            
            optionsStackView.topAnchor.constraint(equalTo: cntView.topAnchor),
            optionsStackView.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 60),
            optionsStackView.trailingAnchor.constraint(equalTo: cntView.trailingAnchor, constant: -60),
            
            cardView.bottomAnchor.constraint(equalTo: optionsStackView.bottomAnchor, constant: 36),
            
            nameLabel.topAnchor.constraint(equalTo: cardView.bottomAnchor, constant: 26),
            nameLabel.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 24),
            
            imageView.widthAnchor.constraint(equalToConstant: 32),
            imageView.heightAnchor.constraint(equalToConstant: 32),
            imageView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            imageView.trailingAnchor.constraint(equalTo: cntView.trailingAnchor, constant: -24),
            
            serialNumberTitleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 26),
            serialNumberTitleLabel.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 24),
            
            serialNumberLabel.topAnchor.constraint(equalTo: serialNumberTitleLabel.bottomAnchor),
            serialNumberLabel.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 24),
            
            versionTitleLabel.topAnchor.constraint(equalTo: serialNumberLabel.bottomAnchor, constant: 26),
            versionTitleLabel.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 24),
            
            versionLabel.topAnchor.constraint(equalTo: versionTitleLabel.bottomAnchor),
            versionLabel.leadingAnchor.constraint(equalTo: cntView.leadingAnchor, constant: 24),
            versionLabel.bottomAnchor.constraint(equalTo: cntView.bottomAnchor, constant: -20)
        ])
    }
    
    public func setUpdateButtonTitle(_ title: String) {
        updateButton.setTitle(title, for: .normal)
    }
    
    public func setContent(device: Devices) {
        let deviceType = HubConst.getDeviceType(device.configVersion, cluster: device.cluster_id)
        let icon = DataManager.shared.d3Const.getDeviceIcons(type: deviceType.int)
        imageView.image = UIImage(named: icon.list)
        
        nameLabel.text = HubConst.getDeviceTypeInfo(device.configVersion, cluster: device.cluster_id).name
        serialNumberLabel.text = String(device.account)
        versionLabel.text = HubConst.getHubVersion(device.configVersion)
    }
    
    @objc private func debugInfoTapped() {
        delegate?.debugInfoTapped()
    }
    
    @objc private func localUsersTapped() {
        delegate?.localUsersTapped()
    }
    
    @objc private func ussdTapped() {
        delegate?.ussdTapped()
    }
    
    @objc private func restartTapped() {
        delegate?.restartTapped()
    }
    
    @objc private func updateTapped() {
        delegate?.updateTapped()
    }
    
    @objc private func detachTapped() {
        delegate?.detachTapped()
    }
}
