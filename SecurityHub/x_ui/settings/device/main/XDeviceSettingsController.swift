//
//  XDeviceSettingsController.swift
//  SecurityHub
//
//  Created by Daniil on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDeviceSettingsController: XBaseViewController<XDeviceSettingsView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private let strings: XDeviceSettingsControllerStrings = XDeviceSettingsControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceId: Int64, isAdmin: Bool
    
    init(deviceId: Int64) {
        self.deviceId = deviceId
        self.isAdmin = DataManager.shared.getUser().roles & Roles.ORG_ADMIN != 0 || DataManager.shared.getUser().roles & Roles.DOMEN_ADMIN != 0
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getDeviceInfo()
    }
    
    private var haveUpdate = false
    private var isSettingNeeded = false
    private func getDeviceInfo() {
        _ = DataManager.shared.getDevice(device: deviceId)
            .subscribe(onNext: { [weak self] device in
                guard let selfUw = self, let deviceUw = device else {
                    return
                }

								selfUw.isSettingNeeded = HubConst.isSettingNeeded(deviceUw.configVersion, cluster: deviceUw.cluster_id)
								selfUw.mainView.setContent(device: deviceUw)
                
                let affs = DataManager.shared.dbHelper.getAffects(device_id: selfUw.deviceId, section_id: 0, zone_id: 100)
								selfUw.haveUpdate = affs.first(where: { $0._class == 8 && $0.reason == 7 }) != nil
                if selfUw.haveUpdate { selfUw.mainView.setUpdateButtonTitle("N_DEVICE_SETTINGS_UPDATE".localized()) } else {
									selfUw.mainView.setUpdateButtonTitle("N_DEVICE_SETTINGS_UPDATE_LOAD".localized())
                }
            })
       
    }
}

extension XDeviceSettingsController: XDeviceSettingsViewDelegate {
    func debugInfoTapped() {
        let controller = XDeviceSettingsDebugInfoController(deviceId: deviceId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func localUsersTapped() {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XDeviceSettingsLocalUsersController(deviceId: deviceId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func ussdTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XTextEditController(title: strings.ussdTitle, value: strings.ussdDefault) { controller, value in
            _ = self.doOnConnect(deviceId: self.deviceId)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["command": ["USSD":["text": value]], "device": self.deviceId]) })
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] result in
                        self?.resultProcessing(result) })
                .do(onError: { [weak self] error in self?.errorProcessing(error) })
                .asCompletable().subscribe()
            controller.navigationController?.popViewController(animated: true)
        }
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func restartTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let alert = XAlertController(
            style: confirmAlertStyle(),
            strings: strings.restartAlertText,
            positive: {
                _ = self.doOnConnect(deviceId: self.deviceId)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                    .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["REBOOT" : []]])  .observeOn(ThreadUtil.shared.mainScheduler) })
                    .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                    .do(onError: { [weak self] error in self?.errorProcessing(error) })
                    .asCompletable().subscribe()
            }
        )
        
        navigationController?.present(alert, animated: true)
    }
    
    func updateTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else {
            return showErrorColtroller(message: "error_device_armed".localized())
        }
        let alert = XAlertController(
            style: confirmAlertStyle(),
            strings: haveUpdate ? strings.updateAlertText : strings.loadUpdateAlertText,
            positive: {
                self.showToast(message: "COMMAND_BEEN_SEND".localized())
                if self.haveUpdate {
                    _ = self.doOnConnect(deviceId: self.deviceId)
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                        .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["UPDATE_APPLY" : [] ]]) })
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                        .do(onError: { [weak self] error in self?.errorProcessing(error) })
                        .asCompletable().subscribe()
                } else {
                    _ = self.doOnConnect(deviceId: self.deviceId)
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                        .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["UPDATE" : [ "file": 0 ] ] ]) })
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .do(onSuccess: { [weak self] result in self?.resultProcessing(result) })
                        .do(onError: { [weak self] error in self?.errorProcessing(error) })
                        .asCompletable().subscribe()
                }
            }
        )
        
        navigationController?.present(alert, animated: true)
    }
    
    func detachTapped() {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XDeviceSettingsDetachController(deviceId: deviceId)
        navigationController?.pushViewController(controller, animated: true)
    }
}
