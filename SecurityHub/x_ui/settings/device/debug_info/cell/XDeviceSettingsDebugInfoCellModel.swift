//
//  XDeviceSettingsDebugInfoCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XDeviceSettingsDebugInfoCellModel {
    struct Names {
        let site, device, section, zone: String
    }
    
    let id: Int64
    let icon: String
    let description: String
    let record: String?
    let time: Int64
    let names: Names
}

extension XDeviceSettingsDebugInfoCell {
    typealias Model = XDeviceSettingsDebugInfoCellModel
}
