//
//  XDeviceSettingsDebugInfoCellStyle.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsDebugInfoCellStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let description: Label
    let date: Label
    let info: Label
}

extension XDeviceSettingsDebugInfoCell {
    typealias Style = XDeviceSettingsDebugInfoCellStyle
    
    func cellStyle() -> Style {
        return Style(
            description: Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            date: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            info: Style.Label(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
