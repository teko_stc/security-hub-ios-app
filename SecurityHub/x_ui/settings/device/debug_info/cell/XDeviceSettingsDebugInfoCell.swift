//
//  XDeviceSettingsDebugInfoCell.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsDebugInfoCell: UITableViewCell {
    
    static let identifier = "XDeviceSettingsDebugInfoCell.identifier"
    
    private var iconView: UIImageView = UIImageView()
    private var descriptionLabel: UILabel = UILabel()
    private var dateLabel: UILabel = UILabel()
    private var infoLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews(cellStyle())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        addSubview(iconView)
        
        descriptionLabel.font = style.description.font
        descriptionLabel.textColor = style.description.color
        descriptionLabel.numberOfLines = 0
        addSubview(descriptionLabel)
        
        dateLabel.font = style.date.font
        dateLabel.textColor = style.date.color
        dateLabel.numberOfLines = 0
        addSubview(dateLabel)
        
        infoLabel.font = style.info.font
        infoLabel.textColor = style.info.color
        infoLabel.numberOfLines = 0
        addSubview(infoLabel)
    }
    
    private func setConstraints() {
        [iconView, descriptionLabel, dateLabel, infoLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: 50),
            iconView.heightAnchor.constraint(equalToConstant: 50),
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            
            descriptionLabel.topAnchor.constraint(equalTo: iconView.topAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: dateLabel.leadingAnchor, constant: -40),
            
            dateLabel.widthAnchor.constraint(equalToConstant: 50),
            dateLabel.topAnchor.constraint(equalTo: iconView.topAnchor),
            dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            infoLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor),
            
            bottomAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 12)
        ])
    }
    
    public func setContent(_ model: Model) {
        iconView.image = UIImage(named: model.icon)
        descriptionLabel.text = model.description
        infoLabel.text = "\(model.names.site)\n\(model.names.device)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM\nHH:mm"
        dateLabel.text = dateFormatter.string(from: Date(timeIntervalSince1970: Double(model.time)))
    }
}

extension XDeviceSettingsDebugInfoView {
    typealias Cell = XDeviceSettingsDebugInfoCell
}
