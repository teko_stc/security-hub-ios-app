//
//  XDeviceSettingsDebugInfoController.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDeviceSettingsDebugInfoController: XBaseViewController<XDeviceSettingsDebugInfoView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_stair",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceId: Int64
    private var disposable: Disposable?
    
    init(deviceId: Int64) {
        self.deviceId = deviceId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
        
        getEvents()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: { [weak self] in
            self?.mainView.hideLoader()
        })
    }
    
    private func getEvents() {
        disposable?.dispose()
        disposable = DataManager.shared.getEvents(device_id: deviceId)
            .filter { $0.event._class == 9 || $0.event._class == 15 }
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { (result) in
                self.mainView.hideLoader()

                let event = result.event

                self.mainView.addEvent(
                    XDeviceSettingsDebugInfoCellModel(
                        id: event.id,
                        icon: event.icon,
                        description: event.affect_desc,
                        record: nil,
                        time: event.time,
                        names: XDeviceSettingsDebugInfoCellModel.Names(
                            site: result.names.site,
                            device: result.names.device,
                            section: result.names.section,
                            zone: result.names.zone
                        )
                    )
                )
            })
    }
}

extension XDeviceSettingsDebugInfoController: XDeviceSettingsDebugInfoViewDelegate {
    func eventSelected(_ model: XDeviceSettingsDebugInfoCell.Model) {
			let controller = XCameraEventsController(model: XCameraEventViewLayerModel(id: model.id, icon: model.icon, description: model.description, record: nil, time: model.time, names: XCameraEventViewLayerModel.Names(site: model.names.site, device: model.names.device, section: model.names.section, zone: model.names.zone),location: nil))
        
        navigationController?.pushViewController(controller, animated: true)
    }
}
