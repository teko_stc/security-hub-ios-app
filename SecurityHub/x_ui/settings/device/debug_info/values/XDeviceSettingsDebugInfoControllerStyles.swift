//
//  XDeviceSettingsDebugInfoControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDeviceSettingsDebugInfoView {
    func style() -> XDeviceSettingsDebugInfoView.Style {
        return XDeviceSettingsDebugInfoView.Style(
            backgroundColor: UIColor.white,
            loaderColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
        )
    }
}
