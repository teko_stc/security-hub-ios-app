//
//  XDeviceSettingsDebugInfoViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsDebugInfoViewStyle {
    let backgroundColor: UIColor
    let loaderColor: UIColor
}

extension XDeviceSettingsDebugInfoView {
    typealias Style = XDeviceSettingsDebugInfoViewStyle
}
