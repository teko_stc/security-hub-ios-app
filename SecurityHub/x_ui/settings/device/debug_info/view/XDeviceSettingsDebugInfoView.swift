//
//  XDeviceSettingsDebugInfoView.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsDebugInfoView: UIView {
    
    public var delegate: XDeviceSettingsDebugInfoViewDelegate?
    
    private var events: [Cell.Model] = []
    
    private var tableView: UITableView = UITableView()
    private var loaderView: XLoaderView = XLoaderView()
    private var emptyTitleView: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        tableView.separatorStyle = .none
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        addSubview(tableView)

        loaderView.color = style.loaderColor
        addSubview(loaderView)
        
        emptyTitleView.setStyle(
            XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            )
        )
        emptyTitleView.text = "N_EVENTS_NO_EVENTS".localized()
        emptyTitleView.isHidden = true
        emptyTitleView.numberOfLines = 1
        addSubview(emptyTitleView)
    }
    
    private func setConstraints() {
        [tableView, loaderView, emptyTitleView].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -24),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
            loaderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            loaderView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            emptyTitleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            emptyTitleView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    public func addEvent(_ event: Cell.Model) {
        events.append(event)
        
        emptyTitleView.isHidden = true
        tableView.reloadData()
    }
    
    public func hideLoader() {
        if !loaderView.isHidden {
            loaderView.isHidden = true
        }
        emptyTitleView.isHidden = events.count > 0
    }
}

extension XDeviceSettingsDebugInfoView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        
        cell.setContent(events[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate?.eventSelected(events[indexPath.row])
    }
}
