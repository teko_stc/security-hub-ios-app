//
//  XDeviceSettingsDebugInfoViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XDeviceSettingsDebugInfoViewDelegate {
    func eventSelected(_ model: XDeviceSettingsDebugInfoCell.Model)
}
