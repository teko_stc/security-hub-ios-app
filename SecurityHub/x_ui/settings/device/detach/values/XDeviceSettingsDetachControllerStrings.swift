//
//  XDeviceSettingsDetachControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XDeviceSettingsDetachViewStrings {
    func title(serialNumber: Int64) -> String {
        "N_DELETE_DEVICE_TITLE_1".localized() + " \"\(serialNumber)\" " + "N_DELETE_DEVICE_TITLE_2".localized()
    }
    
    /// Контроллер больше не будет связан с вашей учетной записью и вашими объектами.\nБудут удалены все устройства, разделы, сценарии и локальные пользователи, привязанные к этому контроллеру. Это озночает, что контроллер и все устройства продолжат свою работу автономно, но потеряют связь с вашей учетной записью.
    var text: String { "N_DELETE_DEVICE_MESSAGE".localized() }
    
    /// Отвязать
    var confirm: String { "N_DELETE_DEVICE_BUTTON".localized() }
}
