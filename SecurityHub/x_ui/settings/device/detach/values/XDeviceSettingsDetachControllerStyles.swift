//
//  XDeviceSettingsDetachControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDeviceSettingsDetachView {
    func style() -> XDeviceSettingsDetachView.Style {
        return XDeviceSettingsDetachView.Style(
            backgroundColor: UIColor.white,
            title: XDeviceSettingsDetachView.Style.Label(
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: XDeviceSettingsDetachView.Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            button: XZoomButton.Style(
                backgroundColor: UIColor(red: 0xF9/255, green: 0x54/255, blue: 0x3E/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 25),
                color: UIColor.white
            )
        )
    }
}
