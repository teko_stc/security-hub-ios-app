//
//  XDeviceSettingsDetachController.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsDetachController: XBaseViewController<XDeviceSettingsDetachView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        )
    )
    
    private var deviceId: Int64
    
    init(deviceId: Int64) {
        self.deviceId = deviceId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        setNavigationViewBuilder(navigationViewBuilder)
    
        getDeviceSerialNumber()
    }
    
    private func getDeviceSerialNumber() {
        _ = DataManager.shared.getDevice(device: deviceId)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { device in
                self.mainView.setContent(device!.account)
            })
    }
}

extension XDeviceSettingsDetachController: XDeviceSettingsDetachViewDelegate {
    func confirmTapped() {
        _ = DataManager.shared.removeDevice(device_id: deviceId)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { _ in
                DataManager.shared.updateSites()
                DataManager.shared.updateAffects()
                
                self.navigationController?.popToRootViewController(animated: true)
            })
    }
}
