//
//  XDeviceSettingsDetachViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceSettingsDetachViewStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
    }
    
    let backgroundColor: UIColor
    
    let title: Label
    let text: Label
    
    let button: XZoomButtonStyle
}

extension XDeviceSettingsDetachView {
    typealias Style = XDeviceSettingsDetachViewStyle
}
