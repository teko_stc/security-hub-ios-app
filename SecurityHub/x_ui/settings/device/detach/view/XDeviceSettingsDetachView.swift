//
//  XDeviceSettingsDetachView.swift
//  SecurityHub
//
//  Created by Daniil on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceSettingsDetachView: UIView {
    
    public var delegate: XDeviceSettingsDetachViewDelegate?
    
    private let strings: XDeviceSettingsDetachViewStrings = XDeviceSettingsDetachViewStrings()
    
    private var scrollView = UIScrollView()
    private var contentView = UIView()
    private var titleLabel: UILabel = UILabel()
    private var textLabel: UILabel = UILabel()
    private var confirmButton: XZoomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        titleLabel.font = style.title.font
        titleLabel.textColor = style.title.color
        titleLabel.numberOfLines = 0
        contentView.addSubview(titleLabel)
        
        textLabel.text = strings.text
        textLabel.font = style.text.font
        textLabel.textColor = style.text.color
        textLabel.numberOfLines = 0
        contentView.addSubview(textLabel)
        
        confirmButton = XZoomButton(style: style.button)
        confirmButton.text = strings.confirm
        confirmButton.addTarget(self, action: #selector(confirmTapped), for: .touchUpInside)
        addSubview(confirmButton)
    }
    
    private func setConstraints() {
        [scrollView, contentView, titleLabel, textLabel, confirmButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 60),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -60),
            
            textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 60),
            textLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 60),
            textLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -60),
            textLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            confirmButton.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            confirmButton.heightAnchor.constraint(equalToConstant: 40),
            confirmButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -40),
            confirmButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 80),
            confirmButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -80)
        ])
    }
    
    public func setContent(_ serialNumber: Int64) {
        titleLabel.text = strings.title(serialNumber: serialNumber)
    }
    
    @objc private func confirmTapped() {
        delegate?.confirmTapped()
    }
}
