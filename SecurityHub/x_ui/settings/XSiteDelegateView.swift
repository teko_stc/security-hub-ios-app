//
//  XSiteDelegateView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSiteDelegateViewDelegate {
    func getCodeViewTapped()
    func copyButtonViewTapped()
    func shareButtonViewTapped()
}

protocol XSiteDelegateViewLayer {
    func setSiteName(_ name: String)
    func setCode(_ code: String)
}

protocol XSiteDelegateViewStringsProtocol {
    var delegationCode: String { get }
    var getDelegationCode: String { get }
    var delegationDescription: String { get }
}

protocol XSiteDelegateViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var cardColor: UIColor { get }
    var tintColor: UIColor { get }
    var titleView: XBaseLableStyle { get }
    var textView: XBaseLableStyle { get }
    var getCodeView: XZoomButtonStyle { get }
//    var cell: XSiteDelegationListViewCellStyleProtocol { get }
}

class XSiteDelegateView: UIView {
    public var delegate: XSiteDelegateViewDelegate?
    
    private var siteView, codeTitleView, codeTextView, infoView: UILabel!
    private var iconView: UIImageView!
    private var getCodeView: XZoomButton!
    private var shareButtonView, copyButtonView: UIButton!
    private var cardLayer: CAShapeLayer!
    
    private var style: XSiteDelegateViewStyleProtocol = XSiteDelegateViewStyle()
    private var strings: XSiteDelegateViewStringsProtocol = XSiteDelegateViewStrings()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        cardLayer = CAShapeLayer()
        cardLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(cardLayer)
        
        siteView = UILabel.create(style.titleView)
        addSubview(siteView)
        
        iconView = UIImageView(image: UIImage(named: "ic_user_del"))
        iconView.contentMode = .scaleAspectFit
        addSubview(iconView)
        
        codeTitleView = UILabel.create(style.textView)
        codeTitleView.text = strings.delegationCode
        addSubview(codeTitleView)
        
        codeTextView = UILabel.create(style.titleView)
        codeTextView.numberOfLines = 2
        codeTextView.text = " "
        addSubview(codeTextView)
        
        getCodeView = XZoomButton(style: style.getCodeView)
        getCodeView.text = strings.getDelegationCode
        getCodeView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(getCodeView)
        
        copyButtonView = UIButton()
        copyButtonView.setImage(UIImage(named: "ic_copy_2")?.withRenderingMode(.alwaysTemplate), for: .normal)
        copyButtonView.tintColor = style.tintColor
        copyButtonView.imageView?.contentMode = .scaleAspectFill
        copyButtonView.contentVerticalAlignment = .fill
        copyButtonView.contentHorizontalAlignment = .fill
        copyButtonView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(copyButtonView)
        
        shareButtonView = UIButton()
        shareButtonView.setImage(UIImage(named: "ic_share_2")?.withRenderingMode(.alwaysTemplate), for: .normal)
        shareButtonView.tintColor = style.tintColor
        shareButtonView.imageView?.contentMode = .scaleAspectFill
        shareButtonView.contentVerticalAlignment = .fill
        shareButtonView.contentHorizontalAlignment = .fill
        shareButtonView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(shareButtonView)
        
        infoView = UILabel.create(style.textView)
        infoView.text = strings.delegationDescription
        infoView.textAlignment = .left
        infoView.numberOfLines = 0
        addSubview(infoView)
        
        [siteView, iconView, codeTitleView, getCodeView, codeTextView, infoView, shareButtonView, copyButtonView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            siteView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
            siteView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            siteView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
        
            iconView.topAnchor.constraint(equalTo: siteView.bottomAnchor, constant: 4),
            iconView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            iconView.widthAnchor.constraint(equalToConstant: 100),
            iconView.heightAnchor.constraint(equalToConstant: 100),
            
            codeTitleView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 4),
            codeTitleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            codeTitleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
        
            getCodeView.topAnchor.constraint(equalTo: codeTitleView.bottomAnchor, constant: 20),
            getCodeView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            getCodeView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            getCodeView.heightAnchor.constraint(equalToConstant: 40),

            codeTextView.topAnchor.constraint(equalTo: codeTitleView.bottomAnchor, constant: 20),
            codeTextView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            codeTextView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),

            shareButtonView.topAnchor.constraint(greaterThanOrEqualTo: getCodeView.bottomAnchor, constant: 20),
            shareButtonView.topAnchor.constraint(greaterThanOrEqualTo: codeTextView.bottomAnchor, constant: 10),
            shareButtonView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor, constant: -35),
            shareButtonView.heightAnchor.constraint(equalToConstant: 26),
            shareButtonView.widthAnchor.constraint(equalToConstant: 26),

            copyButtonView.topAnchor.constraint(greaterThanOrEqualTo: shareButtonView.topAnchor),
            copyButtonView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor, constant: 35),
            copyButtonView.heightAnchor.constraint(equalToConstant: 30),
            copyButtonView.widthAnchor.constraint(equalToConstant: 30),

            infoView.topAnchor.constraint(equalTo: copyButtonView.bottomAnchor, constant: 50),
            infoView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            infoView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
//            infoView.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardLayer.path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: CGSize(width: frame.size.width, height: shareButtonView.frame.origin.y + copyButtonView.frame.size.height + 30)), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 0)).cgPath
    }
    
    @objc private func viewTapped(view: UIView) {
        if view == getCodeView { delegate?.getCodeViewTapped() }
        else if view == copyButtonView { delegate?.copyButtonViewTapped() }
        else  if view == shareButtonView { delegate?.shareButtonViewTapped() }
    }
}

extension XSiteDelegateView: XSiteDelegateViewLayer {
    func setSiteName(_ name: String) {
        siteView.text = name
    }
    
    func setCode(_ code: String) {
        getCodeView.isHidden = true
        codeTextView.text = code
    }
}
