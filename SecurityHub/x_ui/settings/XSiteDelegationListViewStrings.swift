//
//  XSiteDelegationListViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteDelegationListViewStrings: XSiteDelegationListViewStringsProtocol {
    /// Объект еще никому не делегирован
    var listEmpty: String { "N_DELEGATION_EMPTY_LIST".localized() }
}
