//
//  XInterfaceSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 25.10.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation

class XInterfaceSettingsControllerStrings: XInterfaceSettingsControllerStringsProtocol {
    /// Интерфейс
    var title: String { "N_PROFILE_INTERFACE".localized() }
    
    /// Общий главный экран
    var needSharedMainScreenTitle: String { "MAIN_SCREEN_PREF_INTERFACE".localized() }
    
    /// У каждого объекта свои настройки главного экрана
    var needSharedMainScreenOff: String { "MAIN_SCREEN_PREF_INTERFACE_OFF".localized() }
    
    /// Элементы всех доступных объектов помещаются на общий главный экран
    var needSharedMainScreenOn: String { "MAIN_SCREEN_PREF_INTERFACE_ON".localized() }
    
    var needSharedMainScreenAlert: XAlertViewStrings {
        XAlertViewStrings(
            /// Внимание
            title: "WARN_BIG".localized(),
            /// Все ваши настройки главного экрана будут удалены. Вы уверены что хотите переключить режим работы главного экрана?
            text: "shared_main_screen_warning_dialog_text".localized(),
            /// Переключить
            positive: "shared_main_screen_warning_dialog_ok".localized(),
            /// Отменить
            negative: "CANCEL_SMALL".localized()
        )
    }
}
