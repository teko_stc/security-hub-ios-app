//
//  XChangeUserController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XChangeUserControllerStringsProtocol : XRemoveControllerStringsProtocol {
    var adminRole: String { get }
    var managerRole: String { get }
    var viewerRole: String { get }
    var renameTile: String { get }
    var changeLoginTitle: String { get }
    var changePasswordTitle: String { get }
    var noRight: String { get }
    var rightTitle: String { get }
    var rightNegativeText: String { get }
    var rightPositiveText: String { get }
    var activateTitle: String { get }
    var activateNegativeText: String { get }
    var activatePositiveText: String { get }
    var deActivateTitle: String { get }
}

class XChangeUserController: XBaseViewController<XChangeUserView> {
    override var tabBarIsHidden: Bool { true }
    
    private let userId: Int64
    private var role: Int64?, isActive: Bool = true
    private var disposable: Disposable?
    
    private lazy var navigationViewBuilder: XNavigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    private lazy var strings: XChangeUserControllerStringsProtocol = XChangeUserControllerStrings()
    private lazy var viewLayer: XChangeUserViewLayer = self.mainView
    
    init(userId: Int64) {
        self.userId = userId
        super.init(nibName: nil, bundle: nil)
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.getOperators()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { result in
                guard let user = result.operators.first(where: { $0.id == self.userId }) else { return }
                self.role = user.roles
                self.isActive = user.active != 0
                let roleName = user.roles & Roles.ORG_ADMIN != 0 || user.roles & Roles.DOMEN_ADMIN != 0 ? self.strings.adminRole :
                    user.roles == 40960 ? self.strings.managerRole : self.strings.viewerRole
                self.viewLayer.setUserData(name: user.name, login: user.login, isActive: user.active != 0, roleName: roleName)
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
    
    private func deleteUser(b: Bool?) {
        _ = DataManager.shared.hubHelper.setCommandWithResult(.OPERATOR_DEL, D: ["id" : self.userId])
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onSuccess: { [weak self] result in
                if (result.success) { self?.back() }
                else { self?.showErrorColtroller(message: result.message) }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })
    }
    
    private func changeUser(attr: String, value: Any) {
        _ = DataManager.shared.hubHelper.setCommandWithResult(.OPERATOR_SET, D: ["id" : self.userId, attr: value])
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onSuccess: { [weak self] result in
                if (!result.success) { self?.showErrorColtroller(message: result.message) }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })

    }
}

extension XChangeUserController: XChangeUserViewDelegate {
    func changeNameViewTapped(name: String) {
        let controller = XTextEditController(title: strings.renameTile, value: name, onCompleted: { c, v in self.changeUser(attr: "name", value: v); self.back() })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func changeLoginViewTapped(login: String) {
        let controller = XTextEditController(title: strings.changeLoginTitle, value: login, onCompleted: { c, v in self.changeUser(attr: "login", value: v); self.back()  })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func changePasswordViewTapped() {
        guard let role = role else { return }
        if role & Roles.ORG_ADMIN != 0 || role & Roles.DOMEN_ADMIN != 0 { return showErrorColtroller(message: strings.noRight) }
        let alert = XChangePasswordAlertController(title: strings.changePasswordTitle, confirm: { p in self.changeUser(attr: "password", value: p.md5) })
        navigationController?.present(alert, animated: true)
    }
    
    func activateDeactivateViewTapped() {
        guard let role = role else { return }
        if role & Roles.ORG_ADMIN != 0 || role & Roles.DOMEN_ADMIN != 0 { return showErrorColtroller(message: strings.noRight) }
        let alert = XAlertController(
            style: XAlertView.Style(
                backgroundColor: .white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                buttonOrientation: .horizontal,
                positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0xf9543a)),
                negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042))
            ),
            strings: XAlertView.Strings(title: isActive ? strings.deActivateTitle : strings.activateTitle, text: nil, positive: strings.activatePositiveText, negative: strings.activateNegativeText),
            positive: { self.changeUser(attr: "active", value: !self.isActive) })
        navigationController?.present(alert, animated: true)
    }
    
    func changeRoleViewTapped() {
        guard let role = role else { return }
        if role & Roles.ORG_ADMIN != 0 || role & Roles.DOMEN_ADMIN != 0 { return showErrorColtroller(message: strings.noRight) }
        let alert = XSelectorAlertController(
            title: strings.rightTitle,
            negativeText: strings.rightNegativeText,
            positiveText: strings.rightPositiveText,
            items: [
                XSelectorAlertControllerModel(title: strings.managerRole, value: 40960),
                XSelectorAlertControllerModel(title: strings.viewerRole, value: 8192)
            ],
            selectedIndex: role == 40960 ? 0 : 1
        )
        alert.onPositiveViewTapped = { value in if let value = value { self.changeUser(attr: "roles", value: value) } }
        navigationController?.present(alert, animated: true)
    }
    
    func deteleViewTapped() {
        let controller = XRemoveController(strings: strings, action: self.deleteUser)
        navigationController?.pushViewController(controller, animated: true)
    }
}
