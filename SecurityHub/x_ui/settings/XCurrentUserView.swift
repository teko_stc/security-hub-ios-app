//
//  XCurrentUserView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XCurrentUserViewLayer {
    static var ROLE_ADMIN: Int { get }
    static var ROLE_MANAGER: Int { get }
    static var ROLE_VIEWER: Int { get }

    func setInfo(name: String, login: String, role: Int)
}

protocol XCurrentUserViewDelegate {
    func leftViewTapped()
    func nameViewTapped()
    func changePasswordViewTapped()
    func exitViewTapped()
}

protocol XCurrentUserViewStringsProtocol {
    var login: String { get }
    var changePassowrd: String { get }
    var exit: String { get }
    var admin: String { get }
    var manager: String { get }
    var viewer: String { get }
}

struct XCurrentUserViewStyle {
    let backgroundColor, cardColor, tintColor, actionColor: UIColor
    let titleFont, textFont, actionFont: UIFont?
}

class XCurrentUserView: UIView {
    public var delegate: XCurrentUserViewDelegate?
    
    private var cardLayer: CAShapeLayer!
    private var nameView, leftView, changePasswordView, exitView: UIButton!
    private var iconView: UIImageView!
    private var loginAndRightView: UILabel!
    private var attrNormalName, attrHighlightedName: [NSAttributedString.Key: Any]!
    
    private lazy var style: XCurrentUserViewStyle = viewStyles()
    private lazy var strings: XCurrentUserViewStringsProtocol = XCurrentUserVCStrings()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        cardLayer = CAShapeLayer()
        cardLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(cardLayer)
        
        leftView = UIButton(type: .infoLight)
        leftView.setImage(UIImage(named: "ic_stair")?.withRenderingMode(.alwaysTemplate), for: .normal)
        leftView.tintColor = style.tintColor
        leftView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(leftView)
        
        iconView = UIImageView(image: UIImage(named: "ic_user"))
        iconView.contentMode = .scaleAspectFit
        addSubview(iconView)
        
        attrNormalName = [.foregroundColor: style.tintColor]
        attrHighlightedName = [.foregroundColor: style.tintColor.withAlphaComponent(0.4)]
        if let font = style.titleFont { attrNormalName[.font] = font; attrHighlightedName[.font] = font }
        nameView = UIButton()
        nameView.contentHorizontalAlignment = .left
        nameView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(nameView)
        
        loginAndRightView = UILabel()
        loginAndRightView.font = style.textFont
        loginAndRightView.textColor = style.tintColor
        loginAndRightView.numberOfLines = 0
        addSubview(loginAndRightView)
        
        var attrNormal: [NSAttributedString.Key: Any] = [.foregroundColor: style.tintColor]
        var attrHighlighted: [NSAttributedString.Key: Any] = [.foregroundColor: style.tintColor.withAlphaComponent(0.4)]
        if let font = style.actionFont { attrNormal[.font] = font; attrHighlighted[.font] = font }
        changePasswordView = UIButton()
        changePasswordView.contentHorizontalAlignment = .left
        changePasswordView.setAttributedTitle(NSAttributedString(string: strings.changePassowrd, attributes: attrNormal), for: .normal)
        changePasswordView.setAttributedTitle(NSAttributedString(string: strings.changePassowrd, attributes: attrHighlighted), for: .highlighted)
        changePasswordView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(changePasswordView)
        
        var attrNormalExit: [NSAttributedString.Key: Any] = [.foregroundColor: style.actionColor]
        if let font = style.actionFont { attrNormalExit[.font] = font }
        exitView = UIButton()
        exitView.contentHorizontalAlignment = .left
        exitView.setAttributedTitle(NSAttributedString(string: strings.exit, attributes: attrNormalExit), for: .normal)
        exitView.setAttributedTitle(NSAttributedString(string: strings.exit, attributes: attrHighlighted), for: .highlighted)
        exitView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(exitView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardLayer.path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: CGSize(width: frame.size.width, height: loginAndRightView.frame.origin.y + loginAndRightView.frame.size.height + 40)), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 0)).cgPath
    }
    
    private func setConstraints() {
        [leftView, iconView, nameView, loginAndRightView, changePasswordView, exitView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            leftView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: XNavigationView.marginTop),
            leftView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.marginLeft),
            leftView.heightAnchor.constraint(equalToConstant: XNavigationView.iconSize),
            leftView.widthAnchor.constraint(equalToConstant: XNavigationView.iconSize),
            
            iconView.topAnchor.constraint(equalTo: leftView.topAnchor),
            iconView.leadingAnchor.constraint(equalTo: leftView.trailingAnchor, constant: XNavigationView.marginLeft),
            iconView.heightAnchor.constraint(equalToConstant: 60),
            iconView.widthAnchor.constraint(equalToConstant: 60),
            
            nameView.topAnchor.constraint(equalTo: leftView.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 10),
            nameView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            loginAndRightView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 20),
            loginAndRightView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            loginAndRightView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),

            changePasswordView.topAnchor.constraint(equalTo: loginAndRightView.bottomAnchor, constant: 60),
            changePasswordView.heightAnchor.constraint(equalToConstant: 40),
            changePasswordView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            changePasswordView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            exitView.topAnchor.constraint(equalTo: changePasswordView.bottomAnchor, constant: 10),
            exitView.heightAnchor.constraint(equalTo: changePasswordView.heightAnchor),
            exitView.leadingAnchor.constraint(equalTo: changePasswordView.leadingAnchor),
            exitView.trailingAnchor.constraint(equalTo: changePasswordView.trailingAnchor),
            exitView.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
     @objc private func viewTapped(view: UIView) {
        if view == leftView { delegate?.leftViewTapped() }
        else if view == nameView { delegate?.nameViewTapped() }
        else if view == changePasswordView { delegate?.changePasswordViewTapped() }
        else if view == exitView { delegate?.exitViewTapped() }
    }
}

extension XCurrentUserView: XCurrentUserViewLayer {
    static var ROLE_ADMIN: Int { 2 }
    
    static var ROLE_MANAGER: Int { 1 }
    
    static var ROLE_VIEWER: Int { 0 }
    
    func setInfo(name: String, login: String, role: Int) {
        nameView.setAttributedTitle(NSAttributedString(string: name, attributes: attrNormalName), for: .normal)
        nameView.setAttributedTitle(NSAttributedString(string: name, attributes: attrHighlightedName), for: .highlighted)
        loginAndRightView.text = "\(strings.login): \(login)\n\(role == XCurrentUserView.ROLE_ADMIN ? strings.admin : role == XCurrentUserView.ROLE_MANAGER ? strings.manager : strings.viewer)"
    }
}
