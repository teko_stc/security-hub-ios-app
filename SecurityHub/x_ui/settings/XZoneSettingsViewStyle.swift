//
//  XZoneSettingsViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XZoneSettingsViewStyle: XZoneSettingsViewStyleProtocol {
    var backgroundColor: UIColor { UIColor.colorFromHex(0xefefef) }
    
    var cardColor: UIColor { .white }
    
    var baseView: XEditButtonStyle {
        XEditButtonStyle(
            header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5)),
            title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20))
        )
    }
    
    var deleteView: XEditButtonStyle {
        XEditButtonStyle(
            title: XBaseLableStyle(color: UIColor.colorFromHex(0xf9543a), font: UIFont(name: "Open Sans", size: 20))
        )
    }
}
