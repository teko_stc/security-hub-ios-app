//
//  XLanguageController.swift
//  SecurityHub
//
//  Created by Daniil on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import Localize_Swift

class XLanguageController: XBaseViewController<XLanguageView> {
    override var tabBarIsHidden: Bool { true }
    
    private var isDomain: Bool
    private lazy var viewLayer: XLanguageViewLayer = self.mainView

    private let strings = XLanguageControllerStrings()
    
    init(isDomain: Bool = false) {
        self.isDomain = isDomain
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rightViews: [XNavigationViewRightViewBuilder] = []
        if (DataManager.defaultHelper.roles & Roles.ORG_ADMIN != 0 || DataManager.defaultHelper.roles & Roles.DOMEN_ADMIN != 0) && !isDomain {
            rightViews.append(
                XNavigationViewRightViewBuilder(
                    imageName: "ic_options",
                    color: UIColor.colorFromHex(0x414042),
                    viewTapped: {},
                    groupViews: [
                        XNavigationViewRightViewBuilder(
                            title: "Язык домена",
                            font: UIFont(name: "Open Sans", size: 15.5),
                            color: UIColor.colorFromHex(0x414042),
                            viewTapped: self.showChangeDomainLanAlert
                        )
                    ]
                )
            )
        }
        
        let navigationViewBuilder = XNavigationViewBuilder(
            backgroundColor: .white,
            popupColor: .white,
            leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
            titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left),
            rightViews: rightViews
        )
        
        title = !isDomain ? strings.title : strings.titleDomain
        setNavigationViewBuilder(navigationViewBuilder)

        mainView.delegate = self
        viewLayer.setCurrentValue(isDomain ? DataManager.defaultHelper.domainLangIso : DataManager.defaultHelper.language )
    }
    
    func showChangeDomainLanAlert() {
        let title = DataManager.defaultHelper.domainLangIso == DataManager.defaultHelper.language ? strings.changeDomainAlertTitleOne : strings.changeDomainAlertTitleTwo
        let style = XAlertView.Style(
            backgroundColor: .white,
            title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
            text: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042)),
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xf9543e)),
            negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042))
        )
        let alert = XAlertController(style: style, strings: XAlertView.Strings(title: title, positive: strings.changeDomainAlertPositive, negative: strings.changeDomainAlertNegative), positive: {
            let controller = XLanguageController(isDomain: true)
            self.navigationController?.pushViewController(controller, animated: true)
        })
        navigationController?.present(alert, animated: true)
    }
}


extension XLanguageController: XLanguageViewDelegate {
     
    func languageSelected(language: String) {
        if isDomain {
            DataManager.defaultHelper.domainLangIso = language
            let _ = DataManager.shared.setLocale(language)
                .subscribe(onNext: { [weak self] _ in
                    DataManager.defaultHelper.domainLangIso = language
                    self?.viewLayer.setCurrentValue(language)
                })
        } else {
            Localize.setCurrentLanguage(language)
            DataManager.defaultHelper.language = language
            DataManager.shared.clearAfterLang()
            R.reinit()
            DataManager.shared.hubHelper.disconnect()
            DataManager.shared.dbHelper = DBHelper(login: "nil")
            NavigationHelper.shared.show(XSplashController())
        }
    }
}

