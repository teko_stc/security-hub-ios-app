//
//  XLanguageViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 29.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XLanguageViewDelegate {
    func languageSelected(language: String) 
}
