//
//  XLanguageView.swift
//  SecurityHub
//
//  Created by Daniil on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import Localize_Swift

protocol XLanguageViewLayer {
    func setCurrentValue(_ value: String)
}

class XLanguageView: UIView {
    
    private let strings: XLanguageViewStrings = XLanguageViewStrings()
     
    public var delegate: XLanguageViewDelegate?
    
    private var tableView: UITableView = UITableView()
    
    private var currenValue: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        initViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews() {
        tableView.separatorStyle = .none
        tableView.register(XLanguageCell.self,
                           forCellReuseIdentifier: XLanguageCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        addSubview(tableView)
    }
    
    private func setConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,
                                           constant: 20),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

extension XLanguageView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return strings.languagesCodes.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier,
                                                 for: indexPath) as! Cell
        
        cell.setContent(model: Cell.Model(isTargetCell: strings.languagesCodes.firstIndex(where: { $0 == currenValue }) == indexPath.row, cellText: strings.languagesNames[indexPath.row]))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        delegate?.languageSelected(language: strings.languagesCodes[indexPath.row])
    }
}

extension XLanguageView: XLanguageViewLayer {
    public func setCurrentValue(_ value: String) {
        currenValue = value
        tableView.reloadData()
    }
}
