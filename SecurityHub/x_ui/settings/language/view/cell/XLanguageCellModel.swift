//
//  XLanguageCellModel.swift
//  SecurityHub
//
//  Created by Daniil on 29.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XLanguageCellModel {
    let isTargetCell: Bool
    let cellText: String
}
 
extension XLanguageCell {
    typealias Model = XLanguageCellModel
}
