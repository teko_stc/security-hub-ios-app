//
//  XLanguageCell.swift
//  SecurityHub
//
//  Created by Daniil on 29.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XLanguageCell: UITableViewCell {
    
    static var identifier = "XLanguageCell.identifier"
     
    private var model: Model?
    
    private var selectionView: UIView = UIView()
    private var languageNameView: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        
        initViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setContent(model: Model) {
        self.model = model
        
        selectionView.backgroundColor = model.isTargetCell ? UIColor.colorFromHex(0x3abeff) : UIColor.white
        languageNameView.textColor = model.isTargetCell ? UIColor.white : UIColor.colorFromHex(0x414042)
        languageNameView.text = model.cellText
    }
    
    private func initViews() {
        selectedBackgroundView = UIView()
        
        selectionView.layer.cornerRadius = 12
        selectionView.clipsToBounds = false
        addSubview(selectionView)
        
        languageNameView.font = UIFont(name: "Open Sans", size: 20)
        addSubview(languageNameView)
    }
    
    private func setConstraints() {
        selectionView.translatesAutoresizingMaskIntoConstraints = false
        languageNameView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 64),
            
            selectionView.widthAnchor.constraint(equalTo: widthAnchor),
            selectionView.heightAnchor.constraint(equalTo: heightAnchor),
            
            languageNameView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            languageNameView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}

extension XLanguageView {
    typealias Cell = XLanguageCell
}
