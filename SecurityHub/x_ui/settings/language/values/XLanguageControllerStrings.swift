//
//  XLanguageControllerStrings.swift
//  SecurityHub
//
//  Created by Daniil on 28.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

class XLanguageControllerStrings {
    var title: String { "PROF_LANG_TITLE".localized() }
    
    /// Язык домена
    var titleDomain: String { "DOMAIN_LANG_TITLE".localized() }
    
    /// В данный момент у вашего домена тот же язык, что используется в приложении. Вы действительно хотите перейти к смене языка домена?
    var changeDomainAlertTitleOne: String { "DOMAIN_LANG_DIALOG_SAME".localized() }
    
    /// Для вашего домена и приложения выбраны разные языки. Вы действительно хотите перейти к смене языка домена?
    var changeDomainAlertTitleTwo: String { "DOMAIN_LANG_DIALOG_DIFF".localized() }
    
    /// Да
    var changeDomainAlertPositive: String { "YES".localized() }
    
    /// Нет
    var changeDomainAlertNegative: String { "NO".localized() }
}

class XLanguageViewStrings {
    /// Язык домена
    var changeDomainTitle: String { "DOMAIN_LANG_TITLE".localized() }
    
    var languagesNames: [String] { ["pref_language_select[0]".localized(), "pref_language_select[1]".localized(), "pref_language_select[2]".localized(), "pref_language_select[3]".localized(), "pref_language_select[4]".localized()] }
    
    var languagesCodes: [String] { ["pref_language_values[0]".localized(), "pref_language_values[1]".localized(), "pref_language_values[2]".localized(), "pref_language_values[3]".localized(), "pref_language_values[4]".localized()] }
}
