//
//  XSiteSettingsView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSiteSettingsViewDelegate {
    func changeNameViewTapped(name: String)
    func delegationListViewTapped()
    func deleteDelegationViewTapped()
		func autoarmViewTapped()
    func deleteViewTapped()
}

protocol XSiteSettingsViewLayer {
    func setData(name: String, isDelegated: Bool)
		func setData(autoarm: DAutoArmEntity)
}

protocol XSiteSettingsViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var cardColor: UIColor { get }
    var baseView: XEditButtonStyle { get }
		var autoarmView: XEditButtonStyle { get }
    var deleteView: XEditButtonStyle { get }
}

protocol XSiteSettingsViewStringsProtocol {
    var nameTitle: String { get }
    var delegationListText: String { get }
    var deleteDelegationText: String { get }
    var deleteText: String { get }
		var autoarmTitle: String { get }
		var autoarmDisable: String { get }
		var autoarmSchedule: String { get }
		var autoarmLoading: String { get }
}

class XSiteSettingsView: UIView {
    public var delegate: XSiteSettingsViewDelegate?
		
    private lazy var style: XSiteSettingsViewStyleProtocol = XSiteSettingsViewStyle()
    private lazy var strings: XSiteSettingsViewStringsProtocol = XSiteSettingsViewStrings()

    private var nameView, delegationView, autoarmView, deleteView: XEditButton!
    private var cardLayer: CAShapeLayer!
    private var deleteViewHeightAnchor: NSLayoutConstraint!
    
    private var isDelegated: Bool = false

    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        cardLayer = CAShapeLayer()
        cardLayer.fillColor = style.cardColor.cgColor
        layer.addSublayer(cardLayer)
        
        nameView = XEditButton(style: style.baseView)
        nameView.headerTitle = strings.nameTitle
        nameView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(nameView)
        
        delegationView = XEditButton(style: style.baseView)
        delegationView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(delegationView)
			
				autoarmView = XEditButton(style: style.autoarmView)
				autoarmView.headerTitle = strings.autoarmTitle
				autoarmView.isMultiLine = true
				autoarmView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
				addSubview(autoarmView)
        
        deleteView = XEditButton(style: style.deleteView)
        deleteView.title = strings.deleteText
        deleteView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(deleteView)
        
        [nameView, delegationView, autoarmView, deleteView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            nameView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: XNavigationView.iconSize + XNavigationView.marginLeft * 2),
            nameView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            nameView.heightAnchor.constraint(equalToConstant: 54),
        
            delegationView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 20),
            delegationView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            delegationView.heightAnchor.constraint(equalToConstant: 60),
            delegationView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
						
						autoarmView.topAnchor.constraint(equalTo: delegationView.bottomAnchor, constant: 20),
						autoarmView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
						autoarmView.heightAnchor.constraint(equalToConstant: 60),
						autoarmView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),

            deleteView.topAnchor.constraint(equalTo: autoarmView.bottomAnchor, constant: 20),
            deleteView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            deleteView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            deleteView.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let v: UIView = isDelegated ? delegationView : deleteView
        cardLayer.path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: CGSize(width: frame.size.width, height: v.frame.origin.y + v.frame.size.height + 40)), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20, height: 0)).cgPath
    }
    
    @objc private func viewTapped(view: UIView) {
        if view == nameView { delegate?.changeNameViewTapped(name: nameView.title ?? "") }
        else if view == delegationView && !isDelegated { delegate?.delegationListViewTapped() }
        else if view == delegationView { delegate?.deleteDelegationViewTapped()}
				else if view == autoarmView { delegate?.autoarmViewTapped()}
        else if view == deleteView { delegate?.deleteViewTapped() }
    }
}

extension XSiteSettingsView: XSiteSettingsViewLayer {
		func setLoading() {
			autoarmView.subTitle = strings.autoarmLoading
		}
	
    func setData(name: String, isDelegated: Bool) {
        nameView.title = name
        self.isDelegated = isDelegated
        deleteView.isHidden = isDelegated
				autoarmView.isHidden = isDelegated
        delegationView.title = isDelegated ? strings.deleteDelegationText : strings.delegationListText
        delegationView.titleLabel?.numberOfLines = 2
        layoutSubviews()
    }
	
		func setData(autoarm: DAutoArmEntity) {
			if autoarm.id < 1 {
				autoarmView.subTitle = strings.autoarmDisable
				return
			}
			
			if autoarm.active == 0
			{
				let date = Date(timeIntervalSince1970: Double(autoarm.runAt))
				let dateFormatter = DateFormatter()
				dateFormatter.dateFormat = "dd.MM.yy HH:mm"
				dateFormatter.timeZone = .current
				autoarmView.subTitle = dateFormatter.string(from: date)
				return
			}
			
			var schedule = strings.autoarmSchedule
			var bitMask: Int64 = 1
			var calendar = Calendar(identifier: .gregorian)
			calendar.locale = Locale (identifier: DataManager.defaultHelper.language)
			var daysOfWeek = calendar.shortWeekdaySymbols
			daysOfWeek.append(daysOfWeek[0])
			daysOfWeek.remove(at: 0)
			for item in daysOfWeek {
				if (autoarm.daysOfWeek & bitMask) != 0 {
					schedule.append(" \(item.capitalized),")
				}
				bitMask <<= 1
			}
			schedule.removeLast()
			schedule.append(String(format:" %02d:%02d",autoarm.hour,autoarm.minute))
			autoarmView.subTitle = schedule
		}
}
