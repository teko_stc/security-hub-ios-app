//
//  XNotificationSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XNotificationSettingsControllerStrings: XNotificationSettingsControllerStringsProtocol {
    /// Настройки событий
    var title: String { "PREF_EVENT_SETTINGS_SUBTITLE".localized() }
    
    /// Уведомления о событиях
    var pushTitle: String { "NOTIF_ABOUT_EVENTS_TITLE".localized() }
    
    /// Настройка по классам событий
    var pushClassTitle: String { "NOTIF_BY_TYPES".localized() }
    
    /// Настройка уведомлений по типам событий
    var pushClassSubTitle: String { "NOTIF_BY_TYPES_SUMM".localized() }
    
    /// Уведомления о событиях внутри приложения
    var pushInAppTitle: String { "NOTIFICATION_INAPP".localized() }
    
    /// Монитор тревог
    var alartMonitorTitle: String { "NOTIF_INAPP_AM".localized() }
    
    /// Функционал монитора тревог выключен
    var alartMonitorSubTitle: String { "NOTIF_INAPP_AM_OFF".localized() }
    
    /// Функционал монитора тревог включен
    var alartMonitorSubTitleIsOn: String { "NOTIF_INAPP_AM_ON".localized() }
}
