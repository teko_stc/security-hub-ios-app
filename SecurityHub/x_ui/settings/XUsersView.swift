//
//  XUsersView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XUsersViewLayerModel {
    let id: Int64
    let name: String
    let role: String
    let isActive: Bool
}

protocol XUsersViewDelegate {
    func userViewTapped(model: XUsersViewLayerModel)
}

enum XUsersViewLayerUpdateType {
    case insert, update, delete
}

protocol XUsersViewLayer {
    func clear()
    func userUpdate(model: XUsersViewLayerModel, type: XUsersViewLayerUpdateType)
}

protocol XUsersViewStringsProtocol {
    var usersViewEmpty: String { get }
}

struct XUsersViewStyle {
    let backgroundColor: UIColor
    let emptyView: XBaseLableStyle
    let cell: XUsersViewCellStyle
}

class XUsersView: UIView {
    public var delegate: XUsersViewDelegate?
    
    private var emptyTitleView: UILabel!
    private var tableView: UITableView!
    
    private lazy var style: XUsersViewStyle = self.viewStyles()
    private lazy var strings: XUsersViewStringsProtocol = XUsersVCStrings()
    
    private var items: [XUsersViewLayerModel] = []
    private var tempIndexes: [IndexPath] = []
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.register(XUsersViewCell.self, forCellReuseIdentifier: XUsersViewCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
        
        emptyTitleView = UILabel.create(style.emptyView)
        emptyTitleView.text = strings.usersViewEmpty
        emptyTitleView.numberOfLines = 0
        addSubview(emptyTitleView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setConstraints() {
        [emptyTitleView, tableView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            emptyTitleView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            emptyTitleView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor, constant: 20),
        ])
    }
}

extension XUsersView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XUsersViewCell.identifier, for: indexPath) as! XUsersViewCell
        cell.setContent(model: items[indexPath.row])
        cell.setStyle(style.cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.userViewTapped(model: items[indexPath.row])
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        tableView.deselectRow(at: indexPath, animated: true)
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        UIView(frame: CGRect(origin: .zero, size: CGSize(width: 6, height: 6)))
    }
}

extension XUsersView: XUsersViewLayer {
    func clear() {
        self.items = []
        self.tableView.reloadData()
    }
    
    func userUpdate(model: XUsersViewLayerModel, type: XUsersViewLayerUpdateType) {
        switch type {
        case .insert:
            self.items.append(model)
            tableView.insertRows(at: [IndexPath(row: self.items.count - 1, section: 0)], with: .automatic)
            break;
        case .update:
            if let index = self.items.firstIndex(where: { $0.id == model.id }) {
                self.items[index] = model
                tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
            break;
        case .delete:
            if let index = self.items.firstIndex(where: { $0.id == model.id }) {
                self.items.remove(at: index)
                tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .right)
            }
            break;
        }
        emptyTitleView.isHidden = self.items.count != 0
    }
}
