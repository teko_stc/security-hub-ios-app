//
//  XCurrentUserController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XCurrentUserControllerStringsProtocol: XRemoveControllerStringsProtocol {
    var renameTitle: String { get }
    var changePasswordTitle: String { get }
}

class XCurrentUserController: XBaseViewController<XCurrentUserView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var viewLayer: XCurrentUserViewLayer = self.mainView
    private lazy var strings: XCurrentUserControllerStringsProtocol = XCurrentUserVCStrings()
    private var disposable: Disposable?
    private var user: Operators?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.getOperatorObservable()
            .subscribe(onNext: { user in
                self.user = user
                let role = user.roles & Roles.ORG_ADMIN != 0 || user.roles & Roles.DOMEN_ADMIN != 0 ? XCurrentUserView.ROLE_ADMIN :
                    user.roles == 40960 ? XCurrentUserView.ROLE_MANAGER : XCurrentUserView.ROLE_VIEWER
                self.viewLayer.setInfo(name: user.name, login: user.login, role: role)
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
    
    private func renameViewCompleted(vc: UIViewController, value: String) {
        if user!.name == value { return vc.dismiss(animated: true) }
        //TODO нет обработки ошибки
        _ = DataManager.shared.changeOperator(id: user!.id, value: value, name: "name").subscribe()
    }
    
    private func changePasswordViewTapped(value: String) {
        if user!.name == value { return }
        //TODO нет обработки ошибки
        _ = DataManager.shared.changeOperator(id: user!.id, value: value.md5, name: "password").subscribe()
    }
    
    private func exit(isSaveConfig: Bool?) {
        if isSaveConfig == false {
            UserDefaults.standard.removePersistentDomain(forName: DataManager.defaultHelper.login + "v.1")
        }
        DataManager.disconnectDisposable = DataManager.shared.disconnect()
            .subscribe(onNext: { _ in NavigationHelper.shared.show(XSplashController()) })
    }
}

extension XCurrentUserController: XCurrentUserViewDelegate {
    func leftViewTapped() { self.back() }
    
    func nameViewTapped() {
        guard let user = user else { return }
        let controller = XTextEditController(title: strings.renameTitle, value: user.name, onCompleted: renameViewCompleted)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func changePasswordViewTapped() {
        let alert = XChangePasswordAlertController(title: strings.changePasswordTitle, confirm: changePasswordViewTapped)
        navigationController?.present(alert, animated: true)
    }
    
    func exitViewTapped() {
        let controller = XRemoveController(strings: strings, action: self.exit)
        navigationController?.pushViewController(controller, animated: true)
    }
}
