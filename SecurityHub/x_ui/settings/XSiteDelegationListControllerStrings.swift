//
//  XSiteDelegationListControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteDelegationListControllerStrings: XSiteDelegationListControllerStringsProtocol {
    func deleteDelegationTitle(siteName: String, delegatedDomain: Int64) -> String {
        "MESSAGE_REVOKE_DELEGATION".localized() + " \(siteName) " + "MESSAGE_2_REVOKE_DELEGATION".localized() + " \(delegatedDomain)"
    }
    
    /// Да
    var deleteDelegationPositiveText: String { "YES".localized() }
    
    /// Нет
    var deleteDelegationNegativeText: String { "NO".localized() }
    
    /// Домен номер:
    func domainNumber(id: Int64) -> String {
        "DELEGATE_DOMAIN_NUMBER_TEXT".localized() + " \(id)"
    }
}
