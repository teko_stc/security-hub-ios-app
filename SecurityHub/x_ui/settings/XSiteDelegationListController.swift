//
//  XSiteDelegationListController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XSiteDelegationListControllerStringsProtocol {
    func deleteDelegationTitle(siteName: String, delegatedDomain: Int64) -> String
    var deleteDelegationPositiveText: String { get }
    var deleteDelegationNegativeText: String { get }

    func domainNumber(id: Int64) -> String
}

class XSiteDelegationListController: XBaseViewController<XSiteDelegationListView> {
    override var tabBarIsHidden: Bool { true }
    
    private let siteId: Int64, siteName: String
    private var disposable: Disposable?
    
    private lazy var navigationViewBuilder: XNavigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.addDelegation)]
    )
    private lazy var strings: XSiteDelegationListControllerStringsProtocol = XSiteDelegationListControllerStrings()
    private lazy var viewLayer: XSiteDelegationListViewLayer = self.mainView
    
    init(siteId: Int64, siteName: String) {
        self.siteId = siteId
        self.siteName = siteName
        super.init(nibName: nil, bundle: nil)
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.delegate_sites(site_id: self.siteId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .map({ r in r.map({ XSiteDelegationListViewLayerModel(id: $0.id, userLogin: $0.delegated_domain_name, domain: self.strings.domainNumber(id: $0.delegated_domain), delegated_domain: $0.delegated_domain) }) })
            .subscribe(onNext: { result in
                self.viewLayer.siteDelegationUpdate(models: result)
            })
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
    
    private func addDelegation() {
        let controller = XSiteDelegateController(siteId: self.siteId, siteName: self.siteName)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func deleteDelegation(delegated_domain: Int64) {
        _ = DataManager.shared.delegate_delete(site: self.siteId, domain_id: delegated_domain)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ [weak self] _ in self?.viewWillAppear(false) })
    }
}

extension XSiteDelegationListController: XSiteDelegationListViewDelegate {
    func delegationViewTapped(model: XSiteDelegationListViewLayerModel) {
        let alert = XAlertController(
            style: XAlertView.Style(
                backgroundColor: .white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                buttonOrientation: .horizontal,
                positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0xf9543a)),
                negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042))
            ),
            strings: XAlertView.Strings(title: strings.deleteDelegationTitle(siteName: self.siteName, delegatedDomain: model.delegated_domain), text: nil, positive: strings.deleteDelegationPositiveText, negative: strings.deleteDelegationNegativeText),
            positive: { self.deleteDelegation(delegated_domain: model.delegated_domain) })
        navigationController?.present(alert, animated: true)
    }
}
