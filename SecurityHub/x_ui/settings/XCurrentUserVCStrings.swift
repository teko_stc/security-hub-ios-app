//
//  XCurrentUserVCStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCurrentUserVCStrings: XCurrentUserViewStringsProtocol, XCurrentUserControllerStringsProtocol {
    /// Изменить пароль к учетной записи
    var changePasswordTitle: String { "PA_PW_CHANGE".localized() }
    
    /// Администратор
    var admin: String { "OPERATOR_ADMIN".localized() }
    
    /// Просмотр состояния и Взятие/Снятие
    var manager: String { "OPERATOR_VIEW_AND_CONTROL".localized() }
    
    /// Только просмотр состояния
    var viewer: String { "OPERATOR_VIEW_ONLY".localized() }
    
    /// Имя пользователя
    var renameTitle: String { "N_TITLE_USER_NAME".localized() }
    
    /// Логин
    var login: String { "N_TITLE_LOGIN".localized() }
    /// Изменить пароль
    var changePassowrd: String { "OP_CHANGE_PASS".localized() }
    /// Выйти
    var exit: String { "N_CURRENT_USER_LOGOUT".localized() }
    
    /// Выйти из учетной записи?
    var removeViewTitle: String { "N_CURRENT_USER_LOGOUT_MESSAGE".localized() }
    
    /// Учётные данные пользователя и данные приложения будут удалены. Безвозвратно будет утеряна информация о настройках главного экрана, настройках приложения и уведомлений, а также о созданных группах разделов и IP-камерах.\n\nОстальные настройки объектов, устройств и камер iVideon хранятся в облачном хранилище Security Hub и будут загружены в приложение при повторной авторизации.
    var removeViewText: String { "N_PROFILE_LOGOUT_M".localized() }
    
    /// Сохранить мои данные
    var removeViewCheckBoxText: String? { "N_LOGOUT_SAVE_APP_DATA".localized() }
    
    /// Выйти
    var removeViewActionText: String? { "N_CURRENT_USER_LOGOUT".localized() }
}
