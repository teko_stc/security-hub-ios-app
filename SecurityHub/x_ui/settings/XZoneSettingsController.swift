//
//  XZoneSettingsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XZoneSettingsControllerStringsProtocol {
    var changeNameTitle: String { get }
    var noRight: String { get }

    func getRemoveAlertStrings(siteName: String?) -> XRemoveControllerStringsProtocol
}

class XZoneSettingsController: XBaseViewController<XZoneSettingsView> {
    override var tabBarIsHidden: Bool { true }

    private let deviceId: Int, sectionId: Int, zoneId: Int, isAdmin: Bool
    private var zoneName: String?
    private var disposable: Disposable?
    
    private lazy var navigationViewBuilder: XNavigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    private lazy var strings: XZoneSettingsControllerStringsProtocol = XZoneSettingsControllerStrings()
    private lazy var viewLayer: XZoneSettingsViewLayer = self.mainView
    private var isSettingNeeded = false

    init(deviceId: Int, sectionId: Int, zoneId: Int) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        self.zoneId = zoneId
        self.isAdmin = DataManager.shared.getUser().roles & Roles.ORG_ADMIN != 0 || DataManager.shared.getUser().roles & Roles.DOMEN_ADMIN != 0
        super.init(nibName: nil, bundle: nil)
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
		
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.getZoneObserver(device_id: deviceId.int64, section_id: sectionId.int64, zone_id: zoneId.int64)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] result in
                guard let selfUw = self else {
                    return
                }

								selfUw.isSettingNeeded = HubConst.isSettingNeeded(result.configVersion, cluster: result.cluster)

								selfUw.zoneName = result.zone.name
								selfUw.viewLayer.setData(name: result.zone.name)
                if (result.sectionDetector == HubConst.SECTION_TYPE_SECURITY) { selfUw.viewLayer.setData(isDelay: result.zone.delay > 0)
									if HubConst.isDeviceSupportBypass (result.configVersion, cluster: result.cluster)
										{ selfUw.viewLayer.setData(isBypass: result.zone.bypass > 0) } }
                let model = DataManager.shared.d3Const.getSensorType(uid: result.zone.uidType.int)
//                let isTemperatureThresholdsNeeded = model == 3 || model == 5 || model == 12 || model == 13 || model == 431 || model == 421 || model == 4511
                let isTemperatureThresholdsNeeded = false
								selfUw.viewLayer.setData(isTemperatureThresholdsNeeded: isTemperatureThresholdsNeeded)
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
    
    private func change(value: Any, attr: String = "name") {
        _ = doOnAvailable(deviceId: self.deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["ZONE_SET": [ "section": self.sectionId, "index": self.zoneId, attr: value ]]]) })
            .subscribe(onSuccess: { [weak self] result in
                if (!result.success) { self?.showErrorColtroller(message: result.message) }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })
    }
    
    private func deleteZone(b: Bool?) {
        _ = doOnAvailable(deviceId: self.deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["ZONE_DEL" : ["section" : self.sectionId, "index" : self.zoneId]] ]) })
            .flatMapCompletable({ [weak self] _ in return self?.deleteZoneTimeout() ?? .empty() })
            .andThen(Single.just(true)).asObservable()
            .concatMap({ _ -> Single<DCommandResult> in return DataManager.shared.hubHelper.setCommandWithResult(.ZONE_DEL, D: ["device_id" : self.deviceId, "device_section": self.sectionId, "zone": self.zoneId]) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { result in
                if (!result.success) { self.showErrorColtroller(message: result.message) }
                else { self.navigationController?.popToRootViewController(animated: true) }
            }, onError: { [weak self] error in
                if let m = error as? MyError, m == MyError.haveZones{ self?.showErrorColtroller(message: "error_need_delete_zone".localized()) }
                else { self?.showErrorColtroller(message: error.localizedDescription) }
            })
    }
    
    private func deleteZoneTimeout() -> Completable {
        let o1 = Completable.create { [deviceId, sectionId, zoneId] observer in
            let o = NotificationCenter.default.addObserver(forName: NSNotification.Name("Affect"), object: nil, queue: .current) { n in
                guard
                    let hubEvent = n.object as? HubEvent,
                    hubEvent.classField == 5, hubEvent.reason == 24, hubEvent.active == 1,
                    hubEvent.device == deviceId, hubEvent.section == sectionId, hubEvent.zone == zoneId
                else {
                    return
                }
                observer(.completed)
            }

            return Disposables.create{ NotificationCenter.default.removeObserver(o) }
        }
        
        let o2 = Completable.empty().delay(.seconds(10), scheduler: ThreadUtil.shared.backScheduler)

        return o1 //Observable.merge(o1.asObservable(), o2.asObservable()).first().asCompletable()
    }
}

extension XZoneSettingsController: XZoneSettingsViewDelegate {
    func changeNameViewTapped(name: String) {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId.int64) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId.int64) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = XTextEditController(title: strings.changeNameTitle, value: name, onCompleted: { vc, value in self.change(value: value); self.back(); })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func delayViewTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId.int64) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId.int64) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        guard let zone = DataManager.shared.getZone(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64) else { return }
        change(value: zone.delay == 0 ? 45 : 0, attr: "delay")
    }
	
		func bypassViewTapped() {
				guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId.int64) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
				guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId.int64) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
				guard let zone = DataManager.shared.getZone(device: deviceId.int64, section: sectionId.int64, zone: zoneId.int64) else { return }
				change(value: zone.bypass == 0 ? 1 : 0, attr: "bypass")
		}
    
    func deleteViewTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId.int64) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
        let controller = XRemoveController(strings: strings.getRemoveAlertStrings(siteName: self.zoneName), action: self.deleteZone)
        navigationController?.pushViewController(controller, animated: true)
    }

    func temperatureThresholdsViewTapped() {
        guard isSettingNeeded else { return showErrorColtroller(message: "N_ONLY_FOR_SH".localized()) }
        guard DataManager.shared.dbHelper.isDeviceConnected(device_id: deviceId.int64) else { return showErrorColtroller(message: "N_FUNC_NO_CONNECTION".localized()) }
        guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceId.int64) == .disarmed else { return showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) }
        let controller = TemperatureThresholdsViewController(deviceId: deviceId, sectionId: sectionId, zoneId: zoneId)
        navigationController?.pushViewController(controller, animated: true)
    }
}
