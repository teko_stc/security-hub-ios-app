//
//  XNotificationSettingsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XNotificationSettingsControllerStringsProtocol {
    var title: String { get }
    var pushTitle: String { get }
    var pushClassTitle: String { get }
    var pushClassSubTitle: String { get }
    var pushInAppTitle: String { get }
    var alartMonitorTitle: String { get }
    var alartMonitorSubTitle: String { get }
    var alartMonitorSubTitleIsOn: String { get }
}

class XNotificationSettingsController: XBaseViewController<XBaseSettingsView>, XBaseSettingsViewDelegate {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var viewLayer: XBaseSettingsViewLayer = self.mainView
    private lazy var strings: XNotificationSettingsControllerStringsProtocol = XNotificationSettingsControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    override func viewDidLoad() {
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
        viewLayer.setItems(
            [
                XBaseSettingsViewLayerModel(
                    title: strings.pushTitle, items: [
                        XBaseSettingsCellModel(isActive: true, title: strings.pushClassTitle, subTitle: strings.pushClassSubTitle),
                    ]
                ),
                XBaseSettingsViewLayerModel(
                    title: strings.pushInAppTitle, items: [
                        XBaseSettingsSwitchCellModel(isActive: true, title: strings.alartMonitorTitle, subTitle: strings.alartMonitorSubTitle, subTitleIsOn: strings.alartMonitorSubTitleIsOn, isOn: DataManager.settingsHelper.needAlarmMonitor),
                    ]
                )
            ]
        )
    }
    
    func selectedCell(model: XBaseSettingsViewLayerModelItemProtocol) {
        switch model.title {
        case strings.pushClassTitle: return showPushClassController()
        default: break;
        }
    }
    
    func switchCell(model: XBaseSettingsViewLayerModelItemProtocol, index: IndexPath, oldValue: Bool) {
        switch model.title {
        case strings.alartMonitorTitle: return changeAlarmMonitorValue(newValue: !oldValue, index: index)
        default: break;
        }
    }
    
    func switchHeader(model: XBaseSettingsViewLayerModel, section: Int, oldValue: Bool) { }
    
    func showPushClassController() {
        guard XServerChanger.fcm != nil else {
            return showErrorColtroller(message: "ios_no_fcm".localized())
        }

        let controller = XPushNotificationSettingsController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func changeAlarmMonitorValue(newValue: Bool, index: IndexPath) {
        DataManager.settingsHelper.needAlarmMonitor = newValue
        DataManager.shared.nCenter.post(name: NSNotification.Name(rawValue: "DataManager.settingsHelper.needAlarmMonitor"), object: newValue)
        viewLayer.updItem(index: index, model: XBaseSettingsSwitchCellModel(isActive: true, title: strings.alartMonitorTitle, subTitle: strings.alartMonitorSubTitle, subTitleIsOn: strings.alartMonitorSubTitleIsOn, isOn: newValue))
    }
}
