//
//  XCurrentUserVCStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCurrentUserView {
    func viewStyles() -> XCurrentUserViewStyle {
        XCurrentUserViewStyle(
            backgroundColor: UIColor.colorFromHex(0xececec), cardColor: .white,
            tintColor: UIColor.colorFromHex(0x414042),
            actionColor: UIColor.colorFromHex(0xf9543e),
            titleFont: UIFont(name: "Open Sans", size: 25),
            textFont: UIFont(name: "Open Sans", size: 15.5),
            actionFont: UIFont(name: "Open Sans", size: 18.5)
        )
    }
}
