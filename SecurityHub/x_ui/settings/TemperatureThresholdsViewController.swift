//
//  TemperatureThresholdsViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин.
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit

class TemperatureThresholdsView: XBaseView {
    var tempView: XEditButton!
    var lowerView, upperView: XTextField!
    private var tempViewHeightConstant: NSLayoutConstraint!
    
    public var lower: Int? {
        get {
            guard tempView.isSelected == true, let value = lowerView.text, let result = Int(value) else {
                return nil
            }
            
            return result
        }
    }
    
    public var upper: Int? {
        get {
            guard tempView.isSelected == true, let value = upperView.text, let result = Int(value) else {
                return nil
            }
            
            return result
        }
    }

    internal override func setContent() {
        backgroundColor = UIColor.white

        tempView = XEditButton(
            style: XEditButtonStyle(
                header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20)),
                title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5)),
                _switch: XSwitch.Style(
                    select: XSwitchStyle.State(
                        backgroundColor: UIColor.clear,
                        borderColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                        thunmColor: UIColor(red: 0x91/255, green: 0xf2/255, blue: 0x79/255, alpha: 1)
                    ),
                    unselet: XSwitchStyle.State(
                        backgroundColor: UIColor.clear,
                        borderColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                        thunmColor: UIColor(red: 0xff/255, green: 0x95/255, blue: 0x4d/255, alpha: 1)
                    )
                )
            )
        )
        tempView.isMultiLine = true
        tempView.headerTitle = "N_WIZ_SECTION_TEMP_LIMITS".localized()
        tempView.title = "N_WIZ_SECTION_LIMITS_SUBTITLE".localized()
        tempView.onSelected = { [weak self] value in
            self?.tempView.title = !value ? "N_WIZ_SECTION_LIMITS_SUBTITLE".localized() : "N_WIZ_SECTION_LIMITS_ALREADY_SET".localized()
            self?.lowerView.isHidden = !value
            self?.upperView.isHidden = !value
            if !value {
                if self?.lowerView.isFirstResponder == true { self?.lowerView.resignFirstResponder()}
                if self?.upperView.isFirstResponder == true { self?.upperView.resignFirstResponder()}
            }
            let vv = (self?.tempView.title?.count ?? 0), v: CGFloat = vv < 34 ? 58 : vv < 67 ? 80 : 100
            self?.tempViewHeightConstant.constant = v
            self?.layoutIfNeeded()
        }
        tempView.onTapped = { [weak self] in
            let value = self?.tempView.isSelected ?? false
            self?.tempView.title = !value ? "N_WIZ_SECTION_LIMITS_SUBTITLE".localized() : "N_WIZ_SECTION_LIMITS_ALREADY_SET".localized()
            self?.lowerView.isHidden = !value
            self?.upperView.isHidden = !value
            if !value {
                if self?.lowerView.isFirstResponder == true { self?.lowerView.resignFirstResponder()}
                if self?.upperView.isFirstResponder == true { self?.upperView.resignFirstResponder()}
            }
            let vv = (self?.tempView.title?.count ?? 0), v: CGFloat = vv < 34 ? 58 : vv < 67 ? 80 : 100
            self?.tempViewHeightConstant.constant = v
            self?.layoutIfNeeded()
        }
        addSubview(tempView)
        
        lowerView = XTextField(
            style: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            )
        )
        lowerView.keyboardType = .numberPad
        lowerView.returnKeyType = .next
        lowerView.isHidden = true
        lowerView.placeholder = "N_TEMP_LIMIT_BOTTOM".localized()
        addSubview(lowerView)
        
        upperView = XTextField(
            style: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            )
        )
        upperView.keyboardType = .numberPad
        upperView.returnKeyType = .done
        upperView.isHidden = true
        upperView.placeholder = "N_TEMP_LIMIT_TOP".localized()
        addSubview(upperView)
        
        tempViewHeightConstant = tempView.heightAnchor.constraint(equalToConstant: 58)
        [tempView, lowerView, upperView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            tempView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
            tempView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 46),
            tempView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            tempViewHeightConstant,
            
            lowerView.topAnchor.constraint(equalTo: tempView.bottomAnchor, constant: 20),
            lowerView.leadingAnchor.constraint(equalTo: tempView.leadingAnchor),
            lowerView.heightAnchor.constraint(equalToConstant: 48.0),
            
            upperView.topAnchor.constraint(equalTo: lowerView.topAnchor),
            upperView.leadingAnchor.constraint(equalTo: lowerView.trailingAnchor, constant: 10),
            upperView.trailingAnchor.constraint(equalTo: tempView.trailingAnchor),
            upperView.widthAnchor.constraint(equalTo: lowerView.widthAnchor),
            upperView.heightAnchor.constraint(equalToConstant: 48.0)
        ])
    }
}

class TemperatureThresholdsViewController: XBaseViewController<TemperatureThresholdsView> {

    override var tabBarIsHidden: Bool { true }
    
    private var deviceId, sectionId: Int, zoneId: Int?

    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(
            imageName: "ic_close",
            color: UIColor.colorFromHex(0x414042),
            viewTapped: self.back
        ),
        rightViews: [
            XNavigationViewRightViewBuilder(
                title: "N_BUTTON_CHANGE".localized(),
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.colorFromHex(0x414042),
                viewTapped: self.confirmViewTapped
            )
        ]
    )

    init(deviceId: Int, sectionId: Int, zoneId: Int?) {
        self.deviceId = deviceId
        self.sectionId = sectionId
        self.zoneId = zoneId
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        
        let upper = Int(userDefaults().string(forKey: "high_temp_d_\(deviceId)_s\(sectionId)") ?? "")
        let lower = Int(userDefaults().string(forKey: "low_temp_d_\(deviceId)_s\(sectionId)") ?? "")
        mainView.upperView.text = (upper == nil || upper == -128) ? nil : String(upper!)
        mainView.lowerView.text = (lower == nil || lower == -128) ? nil : String(lower!)
        mainView.tempView.isSelected = mainView.lowerView.text?.isEmpty == false || mainView.upperView.text?.isEmpty == false
        mainView.tempView.onSelected?(mainView.tempView.isSelected)
    }

    func confirmViewTapped() {
        guard (mainView.upper == nil && mainView.lower == nil) || (mainView.upper != nil && mainView.lower != nil) else {
            let alert = XAlertController(
                style: XAlertView.Style(
                    backgroundColor: .white,
                    title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                    positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xf9543a))
                ),
                strings: XAlertView.Strings(
                    title: "N_SECTION_TEMO_LIMIT_ALERT_ONLY_ONE_LIMIT".localized(),
                    text: nil,
                    positive: "YES".localized(),
                    negative: "NO".localized()
                ),
                positive: doit
            )
            return navigationController?.present(alert, animated: true) ?? ()
        }

        guard mainView.upper ?? 1 > mainView.lower ?? 0 else {
            return showErrorColtroller(message: "N_SECTION_TEMP_LIMIT_ALERT_TOP_SMALLER_BOTTOM".localized())
        }

        doit()
    }
    
    private func doit() {
        if zoneId == nil {
            _ = doOnAvailable(deviceId: self.deviceId.int64)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ _ in
                    var SECTION_SET_D: [String: Any] = [ "index": self.sectionId ]
                    if let lower = self.mainView.lower { SECTION_SET_D["low_temp"] = lower }
                    if let upper = self.mainView.upper { SECTION_SET_D["high_temp"] = upper }
                    return DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["SECTION_SET": SECTION_SET_D]])
                })
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onSuccess: { [weak self] result in
                    if (!result.success) { self?.showErrorColtroller(message: result.message) }
                    else { self?.back() }
                }, onError: { [weak self] error in
                    self?.showErrorColtroller(message: error.localizedDescription)
                })
        } else {
            _ = doOnAvailable(deviceId: self.deviceId.int64)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
                .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.COMMAND_SET, D: ["device" : self.deviceId, "command" : ["ZONE_SET": [ "section": self.sectionId, "index": self.zoneId, "low_temp": self.mainView.lower, "high_temp": self.mainView.upper ]]]) })
                .subscribe(onSuccess: { [weak self] result in
                    if (!result.success) { self?.showErrorColtroller(message: result.message) }
                    else { self?.back() }
                }, onError: { [weak self] error in
                    self?.showErrorColtroller(message: error.localizedDescription)
                })
        }
    }
}
