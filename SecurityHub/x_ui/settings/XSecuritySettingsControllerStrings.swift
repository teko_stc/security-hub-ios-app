//
//  XSecuritySettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 12.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSecuritySettingsControllerStrings: XSecuritySettingsControllerStringsProtocol {
    /// Настройки безопасности
    var title: String { "SECA_TITLE".localized() }
    
    /// Защита PIN-кодом
    var needPinCodeTitle: String { "PIN_ENABLE_TITLE".localized() }
    
    /// Защита PIN-кодом выключена
    var needPinCodeSubTitle: String { "PIN_ENABLE_SUM_OFF".localized() }
    
    /// Защита PIN-кодом включена
    var needPinCodeSubTitleIsOn: String { "PIN_ENABLE_SUM_ON".localized() }
    
    /// Установить или заменить PIN-код
    var setPinCodeTitle: String { "PROFILE_PIN_CHANGE_TITLE".localized() }
    
    /// Изменения четырехзначного кода для входа в приложение
    var setPinCodeSubTitle: String { "PROFILE_PIN_CHANGE_SUM".localized() }
    
    /// Сбросить PIN-код
    var resetPinCodeTitle: String { "PA_PN_CHANGE_TITLE".localized() }
    
    /// Сброс четырехзначного кода для входа в приложение
    var resetPinCodeSubTitle: String { "PROFILE_PIN_RESETING".localized() }
    
    /// Вход по отпечатку пальца
    var needTouchTitle: String { "PROFILE_FINGERPRINT".localized() }
	
		/// Отправка координат
		var sendLocationTitle: String { "PANIC_LOCATION_PREF_TITLE".localized() }
		var sendLocationDescription: String { "PANIC_BUTTON_PREF_SUMMARY_ON".localized() }
    
    var setPinAlert: XAlertViewStrings {
        XAlertViewStrings(
            /// Внимание
            title: "WARN_BIG".localized(),
            /// Вы можете установить PIN-код для входа в приложение прямо сейчас либо при следующем входе в приложение.
            text: "SPF_ENABLE_PIN_DIALOG".localized(),
            /// Установить
            positive: "N_SPF_PIN_ENABLE_DIALOG_OK".localized(),
            /// Пропустить
            negative: "N_SPF_PIN_ENABLE_DIALOG_CANCEL".localized()
        )
    }
}
