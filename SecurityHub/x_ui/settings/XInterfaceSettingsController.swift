//
//  XInterfaceSettingsController.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 25.10.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

protocol XInterfaceSettingsControllerStringsProtocol {
    var title: String { get }
    var needSharedMainScreenTitle: String { get }
    var needSharedMainScreenOff: String { get }
    var needSharedMainScreenOn: String { get }
    
    var needSharedMainScreenAlert: XAlertViewStrings { get }
}

class XInterfaceSettingsController: XBaseViewController<XBaseSettingsView>, XBaseSettingsViewDelegate {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var viewLayer: XBaseSettingsViewLayer = self.mainView
    private lazy var strings: XInterfaceSettingsControllerStringsProtocol = XInterfaceSettingsControllerStrings()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    override func viewDidLoad() {
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
        viewLayer.setItems(
            [
                XBaseSettingsViewLayerModel(
                    title: nil, items: [
                        XBaseSettingsSwitchCellModel(isActive: true,
                                                     title: strings.needSharedMainScreenTitle,
                                                     subTitle: strings.needSharedMainScreenOff,
                                                     subTitleIsOn: strings.needSharedMainScreenOn,
                                                     isOn: DataManager.settingsHelper.needSharedMain),
                    ]
                )
            ]
        )
    }
    
    func selectedCell(model: XBaseSettingsViewLayerModelItemProtocol) { }
    
    func switchCell(model: XBaseSettingsViewLayerModelItemProtocol, index: IndexPath, oldValue: Bool) {
        switch model.title {
        case strings.needSharedMainScreenTitle:
            switchNeedSharedMainAlert(index: index, newValue: !oldValue)
        default:
            break
        }
    }
    
    func switchHeader(model: XBaseSettingsViewLayerModel, section: Int, oldValue: Bool) { }
    
    func switchNeedSharedMainAlert(index: IndexPath, newValue: Bool) {
        let positive: () -> () = {
            DataManager.settingsHelper.needSharedMain = newValue
            if newValue {
                userDefaults().set(0, forKey: "SharedDashboardCount")
            } else {
                DataManager.shared.dbHelper.getSites().forEach { site in
                    for item in 0...5 {
                        let objectTypeKeyName = "DashboardItemType_\(site.id)_\(item)"
                        let objectIdKeyName = "DashboardItemId_\(site.id)_\(item)"
                        userDefaults().setValue(nil, forKey: objectTypeKeyName)
                        userDefaults().setValue(0, forKey: objectIdKeyName)
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DataManager.settingsHelper.needSharedMain"), object: newValue)
            self.viewLayer.updItem(index: index,
                                   model: XBaseSettingsSwitchCellModel(isActive: true,
                                                                       title: self.strings.needSharedMainScreenTitle,
                                                                       subTitle: self.strings.needSharedMainScreenOff,
                                                                       subTitleIsOn: self.strings.needSharedMainScreenOn,
                                                                       isOn: newValue)
            )
        }
        let alert = XAlertController(style: self.baseAlertStyle(),
                                     strings: strings.needSharedMainScreenAlert,
                                     positive: positive)
        navigationController?.present(alert, animated: true)
    }
}
