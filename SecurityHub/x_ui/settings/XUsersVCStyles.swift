//
//  XUsersVCStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XUsersView {
    func viewStyles() -> XUsersViewStyle {
        XUsersViewStyle(
            backgroundColor: .white,
            emptyView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 15.5), alignment: .center),
            cell: XUsersViewCellStyle(
                backgroundColor: .white,
                selectColor: UIColor.colorFromHex(0xefefef),
                titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18.5)),
                subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 15.5))
            )
        )
    }
}
