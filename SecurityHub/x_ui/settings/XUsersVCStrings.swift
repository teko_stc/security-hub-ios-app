//
//  XUsersVCStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XUsersVCStrings: XUsersViewStringsProtocol, XUsersControllerStringsProtocol {
    /// Пользователей нет
    var usersViewEmpty: String { "N_NO_OPERATORS".localized() }
    
    /// Список пользователей домена
    var title: String { "N_PROFILE_ACCESS".localized() }
    
    /// Администратор
    var adminDesc: String { "OPERATOR_ADMIN".localized() }
    
    /// Просмотр состояния и Взятие/Снятие
    var managerDesc: String { "OPERATOR_VIEW_AND_CONTROL".localized() }
    
    /// Просмотр состояния
    var viewerDesc: String { "OPERATOR_VIEW_ONLY".localized() }
}
