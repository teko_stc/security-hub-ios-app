//
//  XSiteDelegationListViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSiteDelegationListViewStyle: XSiteDelegationListViewStyleProtocol {
    struct Cell: XSiteDelegationListViewCellStyleProtocol {
        var selectedColor: UIColor
        var titleView: XBaseLableStyle
        var subTitleView: XBaseLableStyle
    }
    
    var backgroundColor: UIColor { .white }
    
    var emptyView: XBaseLableStyle {
        XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18))
    }
    
    var cell: XSiteDelegationListViewCellStyleProtocol {
        Cell(
            selectedColor: UIColor.colorFromHex(0xe0e0e0),
            titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20)),
            subTitleView:  XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
        )
    }
}
