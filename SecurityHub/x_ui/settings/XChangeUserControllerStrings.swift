//
//  XChangeUserControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XChangeUserControllerStrings: XChangeUserControllerStringsProtocol {
    /// Вы уверены что хотите включить пользователя?
    var activateTitle: String { "PLA_USER_ON_MESS".localized() }
    
    /// Нет
    var activateNegativeText: String { "NO".localized() }
    
    /// Да
    var activatePositiveText: String { "YES".localized() }
    
    /// Вы уверены что хотите отключить пользователя?
    var deActivateTitle: String { "PLA_USER_OFF_MESS".localized() }
    
    /// Изменение пароля пользователя
    var changePasswordTitle: String { "TITLE_USER_PASS_CHANGE".localized() }
    
    /// У вас нет полномочий для выполнения данного действия
    var noRight: String { "EA_NO_RIGHTS".localized() }
    
    /// Выберите права доступа для пользователя
    var rightTitle: String { "PLA_CHANGE_ROLES_MESS".localized() }
    
    /// Нет
    var rightNegativeText: String { "NO".localized() }
    
    /// Ок
    var rightPositiveText: String { "OK".localized() }
    
    /// Имя пользователя
    var renameTile: String { "PA_NAME_CHANGE_TITLE".localized() }
    
    /// Логин
    var changeLoginTitle: String { "N_TITLE_LOGIN".localized() }
    
    /// Удалить доменного пользователя?
    var removeViewTitle: String { "N_DELETE_DOMAIN_USER_TITLE".localized() + "?" }
    
    /// Учетная запись пользователя домена будет удалена./nПользователь больше не сможет авторизоваться в приложении Security Hub.
    var removeViewText: String { "N_DELETE_DOMAIN_USER_MESSAGE".localized() }
    
    var removeViewCheckBoxText: String? { nil }
    
    /// Удалить
    var removeViewActionText: String? { "DELETE".localized() }
    
    /// Администратор
    var adminRole: String { "OPERATOR_ADMIN".localized() }
    
    /// Просмотр состояния и Взятие/Снятие
    var managerRole: String { "OPERATOR_VIEW_AND_CONTROL".localized() }
    
    /// Только просмотр состояния
    var viewerRole: String { "OPERATOR_VIEW_ONLY".localized() }
}
