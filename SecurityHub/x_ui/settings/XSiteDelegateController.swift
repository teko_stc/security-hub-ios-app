//
//  XSiteDelegateController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XSiteDelegateControllerStringsProtocol {
    var title: String { get }
    var copyAlert: XAlertViewStrings { get }
}

class XSiteDelegateController: XBaseViewController<XSiteDelegateView> {
    override var tabBarIsHidden: Bool { true }
    
    private let siteId: Int64, siteName: String
    private var code: String?
    private var disposable: Disposable?
    
    private lazy var navigationViewBuilder: XNavigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25))
    )
    private lazy var strings: XSiteDelegateControllerStringsProtocol = XSiteDelegateControllerStrings()
    private lazy var viewLayer: XSiteDelegateViewLayer = self.mainView
    
    init(siteId: Int64, siteName: String) {
        self.siteId = siteId
        self.siteName = siteName
        super.init(nibName: nil, bundle: nil)
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        viewLayer.setSiteName(siteName)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
}

extension XSiteDelegateController: XSiteDelegateViewDelegate {
    func getCodeViewTapped() {
        _ = DataManager.shared.delegate_new(site_id: self.siteId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] result in self?.code = result.user_code; self?.viewLayer.setCode(result.user_code) })
    }
    
    func copyButtonViewTapped() {
        guard let code = self.code else { return }
        UIPasteboard.general.string = code
        let controller = XAlertController(
            style: XAlertView.Style(
                backgroundColor: UIColor.white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)),
                positive: XZoomButton.Style (
                    backgroundColor: UIColor.clear,
                    font: UIFont(name: "Open Sans", size: 15.5),
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
                ),
                negative: XZoomButton.Style (
                    backgroundColor: UIColor.clear,
                    font: UIFont(name: "Open Sans", size: 15.5),
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
                )
            ),
            strings: strings.copyAlert
        )
        navigationController?.present(controller, animated: true)
    }
    
    func shareButtonViewTapped() {
        guard let code = self.code else { return }
        let textToShare = [ code ]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        navigationController?.present(activityViewController, animated: true, completion: nil)
    }
}
