//
//  XSettingsController.swift
//  SecurityHub
//
//  Created by Timerlan on 08/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

@available(*, deprecated, message: "Please use XMenuController")
class XSettingsController: XBaseController<XSettingsView> {
    
    init(_ title: String = "drawer_item_settings".localized()){
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private var observer: Any?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
