//
//  XBaseSettingsSwitchCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 12.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


struct XBaseSettingsSwitchCellStyle {
    let baseCell: XBaseSettingsCellStyle
    let switchView: XSwitchStyle
}

struct XBaseSettingsSwitchCellModel: XBaseSettingsViewLayerModelItemProtocol {
    var isActive: Bool
    
    var title: String?
    var subTitle: String?
    var subTitleIsOn: String?
    var isOn: Bool
}

let XBaseSettingsSwitchCellIdentifier: String = "XBaseSettingsSwitchCell.identifier"
class XBaseSettingsSwitchCell: XBaseSettingsCell, XSwitchDelegate {
    private var switchView = XSwitch()
    private var didSelectRowAtVoid: ((_ tableView: UITableView, _ indexPath: IndexPath) -> ())?
    private var model: XBaseSettingsSwitchCellModel?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(switchView)
        switchView.translatesAutoresizingMaskIntoConstraints = false
        switchView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        switchView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        switchView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        switchView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        switchView.delegate = self
        viewTrailingAnchors.forEach({ $0.constant = -20 - 10 - 30 })
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    
    public func setStyle(_ style: XBaseSettingsSwitchCellStyle) {
        super.setStyle(style.baseCell)
        switchView.style = style.switchView
    }
    
    public func setContent(_ model: XBaseSettingsSwitchCellModel) {
        self.model = model
        super.setContent(XBaseSettingsCellModel(isActive: model.isActive, title: model.title, subTitle: model.isOn ? model.subTitleIsOn : model.subTitle))
        switchView.isOn = model.isOn
    }
    
    public func setDelegate(didSelectRowAtVoid: @escaping (_ tableView: UITableView, _ indexPath: IndexPath) -> ()) {
        self.didSelectRowAtVoid = didSelectRowAtVoid
    }
    
    func switchStateChange(switch: XSwitch, isOn: Bool) {
        switchView.isOn = model?.isOn ?? isOn
        if let tv = (self.superview as? UITableView), let index = tv.indexPath(for: self) { didSelectRowAtVoid?(tv, index) }
    }
}
