//
//  XSiteSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteSettingsControllerStrings: XSiteSettingsControllerStringsProtocol {
    /// Отказаться от делегирования?
    var deleteDelegationTitle: String { "N_SITE_SETTINGS_DELEGATION_REFUSE".localized() }
    
    /// Да
    var deleteDelegationPositiveText: String { "YES".localized() }
    
    /// Нет
    var deleteDelegationNegativeText: String { "NO".localized() }

    struct RemoveAlert: XRemoveControllerStringsProtocol {
        var removeViewTitle: String
        
        var removeViewText: String
        
        var removeViewCheckBoxText: String? = nil
        
        var removeViewActionText: String?
        
        init(siteName: String?) {
            if let siteName = siteName { removeViewTitle = "N_DELETE_SITE_TITLE".localized() + " \(siteName)\"?" }
            else { removeViewTitle = "N_DELETE_SITE_TITLE".localized() + "?" }
            /// Из учетной записи будут удалены все данные об объекте. \nБудут удалены все контроллеры, зарегистрированные на объекте, вместе с зарегистрированными в них устройствами, разделами, сценариями и локальными пользователями. \nПотеряется возможность управления удаленными устройствами, однако они продолжат свою работу автономно.\n
            removeViewText = "N_DELETE_SITE_MESSAGE".localized()
            /// Удалить
            removeViewActionText = "N_DELETE".localized()
        }
    }
    
    /// Название объекта
    var changeNameTitle: String { "N_SITE_NAME".localized() }
    
    /// Недостаточно полномочий
    var noRight: String { "N_NO_PERMISSION".localized() }
    
    func getRemoveAlertStrings(siteName: String?) -> XRemoveControllerStringsProtocol {
        RemoveAlert(siteName: siteName)
    }
}
