//
//  XSiteDelegateViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 20.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSiteDelegateViewStyle: XSiteDelegateViewStyleProtocol {
    var backgroundColor: UIColor { UIColor.colorFromHex(0xefefef) }
    
    var cardColor: UIColor { .white }
    
    var tintColor: UIColor { UIColor.colorFromHex(0x3abeff) }
    
    var titleView: XBaseLableStyle { XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 20), alignment: .center) }
    
    var textView: XBaseLableStyle { XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5), alignment: .center) }
    
    var getCodeView: XZoomButtonStyle { XZoomButtonStyle(backgroundColor: UIColor.colorFromHex(0x3abeff), font: UIFont(name: "Open Sans", size: 20), color: .white) }
}
