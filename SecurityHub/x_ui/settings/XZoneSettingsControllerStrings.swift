//
//  XZoneSettingsControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XZoneSettingsControllerStrings: XZoneSettingsControllerStringsProtocol {
    struct RemoveAlert: XRemoveControllerStringsProtocol {
        var removeViewTitle: String
        
        var removeViewText: String
        
        var removeViewCheckBoxText: String? = nil
        
        var removeViewActionText: String?
        
        init(siteName: String?) {
            if let siteName = siteName { removeViewTitle = "N_DELETE_ZONE_TITLE_1".localized() + " \"\(siteName)\"?" }
            else { removeViewTitle = "N_DELETE_ZONE_TITLE_1".localized() + "?" }
            /// Зона будет удалена из памяти контроллера.\nПосле удаления устройство можно будет зарегистрировать повторно.
            removeViewText = "N_DELETE_ZONE_MESSAGE".localized()
            /// Удалить
            removeViewActionText = "N_DELETE".localized()
        }
    }
    
    /// Название зоны
    var changeNameTitle: String { "N_TITLE_ZONE_NAME".localized() }
    
    /// Недостаточно полномочий
    var noRight: String { "N_NO_PERMISSION".localized() }
    
    func getRemoveAlertStrings(siteName: String?) -> XRemoveControllerStringsProtocol {
        RemoveAlert(siteName: siteName)
    }
}
