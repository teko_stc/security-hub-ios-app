//
//  XZoneSettingsViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 21.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XZoneSettingsViewStrings: XZoneSettingsViewStringsProtocol {
    /// Название
    var nameTitle: String { "N_RELAY_SETTIGNS_NAME".localized() }
    
    /// Удалить
    var deleteText: String { "N_RELAY_SETTIGNS_DELETE".localized() }
    
    /// Задержка
    var delay: String { "N_ZONE_SETTINGS_DELAY".localized() }
	
		/// Обход
		var bypass: String { "N_BYPASS_ZONE".localized() }
		var bypassDesc: String { "N_EXCLUDE_ZONE_TITLE".localized() }
}
