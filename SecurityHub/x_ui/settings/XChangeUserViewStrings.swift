//
//  XChangeUserViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XChangeUserViewStrings: XChangeUserViewStringsProtocol {
    /// Название
    var nameTitle: String { "N_RELAY_SETTIGNS_NAME".localized() }
    
    /// Логин
    var loginTitle: String { "N_TITLE_LOGIN".localized() }
    
    /// Изменить пароль
    var changePasswordText: String { "OP_CHANGE_PASS".localized() }
    
    /// Включить пользователся
    var activateText: String { "PLA_ENABLE_USER".localized() }
    
    /// Отключить пользователся
    var deactivateText: String { "PLA_DISABLE_USER".localized() }
    
    /// Права доступа
    var roleTitle: String { "N_USER_SETTINGS_TITLE_PERMISSIONS".localized() }
    
    /// Удалить
    var deleteText: String { "DELETE".localized() }
}
