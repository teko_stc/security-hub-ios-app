//
//  XBaseSettingsVCStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 12.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XBaseSettingsView {
    func viewStyles() -> XBaseSettingsViewStyle {
        XBaseSettingsViewStyle(
            backgroundColor: .white,
            altBackgroundColor: UIColor.colorFromHex(0xefefef),
            header: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5)),
            cellActive: XBaseSettingsSwitchCellStyle(
                baseCell: XBaseSettingsCellStyle(
                    backgroundColor: .clear,
                    selectedColor: UIColor.colorFromHex(0xe0e0e0),
                    titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 22), alignment: .left),
                    subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 18.5), alignment: .left)
                ),
                switchView: XSwitchStyle(
                    select: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0x414042), thunmColor: UIColor.colorFromHex(0x91f279)),
                    unselet: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0xbcbcbc), thunmColor: UIColor.colorFromHex(0xff954d))
                )
            ),
            cellAnactive: XBaseSettingsSwitchCellStyle(
                baseCell: XBaseSettingsCellStyle(
                    backgroundColor: .clear,
                    selectedColor: .clear,
                    titleView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 22), alignment: .left),
                    subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 18.5), alignment: .left)
                ),
                switchView: XSwitchStyle(
                    select: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0xbcbcbc), thunmColor: UIColor.colorFromHex(0xbcbcbc)),
                    unselet: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0xbcbcbc), thunmColor: UIColor.colorFromHex(0xbcbcbc))
                )
            ),
            pushHeaderStyle: XEditButtonStyle(
                title: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 22)),
                _switch: XSwitchStyle(
                    select: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0x414042), thunmColor: UIColor.colorFromHex(0x91f279)),
                    unselet: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0xbcbcbc), thunmColor: UIColor.colorFromHex(0xff954d))
                )
            )
        )
    }
}
