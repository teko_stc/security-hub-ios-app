//
//  XSettingsView.swift
//  SecurityHub
//
//  Created by Timerlan on 08/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XSettingsView: XBaseView {
    private let tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = DEFAULT_BACKGROUND
        return view
    }()
    
    override func setContent() {
        xView.addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
