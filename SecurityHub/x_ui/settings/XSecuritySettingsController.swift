//
//  XBaseSettingsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 10.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSecuritySettingsControllerStringsProtocol {
    var title: String { get }
    var needPinCodeTitle: String { get }
    var needPinCodeSubTitle: String { get }
    var needPinCodeSubTitleIsOn: String { get }
    var setPinCodeTitle: String { get }
    var setPinCodeSubTitle: String { get }
    var resetPinCodeTitle: String { get }
    var resetPinCodeSubTitle: String { get }
    var needTouchTitle: String { get }
    var setPinAlert: XAlertViewStrings { get }
		var sendLocationTitle: String { get }
		var sendLocationDescription: String { get }
}

class XSecuritySettingsController: XBaseViewController<XBaseSettingsView>, XBaseSettingsViewDelegate, CLLocationManagerDelegate {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var viewLayer: XBaseSettingsViewLayer = self.mainView
    private lazy var strings: XSecuritySettingsControllerStringsProtocol = XSecuritySettingsControllerStrings()
		private let locationManager = CLLocationManager()
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25), alignment: .left)
    )
    
    override func viewDidLoad() {
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
				locationManager.delegate = self
        viewLayer.setItems(
            [
                XBaseSettingsViewLayerModel(
                    title: nil, items: [
                        XBaseSettingsSwitchCellModel(isActive: true, title: strings.needPinCodeTitle, subTitle: strings.needPinCodeSubTitle, subTitleIsOn: strings.needPinCodeSubTitleIsOn, isOn: DataManager.settingsHelper.needPin),
                        XBaseSettingsCellModel(isActive: DataManager.settingsHelper.needPin, title: strings.setPinCodeTitle, subTitle: strings.setPinCodeSubTitle),
                        XBaseSettingsCellModel(isActive: DataManager.settingsHelper.needPin, title: strings.resetPinCodeTitle, subTitle: strings.resetPinCodeSubTitle),
                        XBaseSettingsSwitchCellModel(isActive: DataManager.settingsHelper.needPin, title: strings.needTouchTitle, subTitle: nil, subTitleIsOn: nil, isOn: DataManager.settingsHelper.touchId),
												XBaseSettingsSwitchCellModel(isActive: locationStatus(), title: strings.sendLocationTitle, subTitle: strings.sendLocationDescription, subTitleIsOn: strings.sendLocationDescription, isOn: DataManager.settingsHelper.sendLocation),
                    ]
                )
            ]
        )
    }
    
    func selectedCell(model: XBaseSettingsViewLayerModelItemProtocol) {
        switch model.title {
        case strings.setPinCodeTitle: return setNewPinCode()
        case strings.resetPinCodeTitle: return resetPinCode()
        default: break;
        }
    }
    
    func switchCell(model: XBaseSettingsViewLayerModelItemProtocol, index: IndexPath, oldValue: Bool) {
        switch model.title {
        case strings.needPinCodeTitle:
            if oldValue { return deactivatePinCode(index: index) }
            else { return activatePinCode(index: index) }
        case strings.needTouchTitle: return updTouchId(index: index, newValue: !oldValue)
				case strings.sendLocationTitle: return updSendLocation(index: index, newValue: !oldValue)
        default: break;
        }
    }
    
    func switchHeader(model: XBaseSettingsViewLayerModel, section: Int, oldValue: Bool) { }
	
		private func locationStatus () -> Bool
		{
			let status = CLLocationManager.authorizationStatus()
			
			if status == .authorizedWhenInUse || status == .authorizedAlways { return true }
			if status == .notDetermined { locationManager.requestWhenInUseAuthorization() }
			
			return false
		}

		func locationManager (_ manager: CLLocationManager,
													didChangeAuthorization status: CLAuthorizationStatus) {
			var itemIndex = 0
			var itemActive = false
			viewLayer.getItems().forEach { model in
				for (index,item) in model.items.enumerated() {
					if item.title == strings.sendLocationTitle {
						itemIndex = index
						itemActive = item.isActive
						break
					}
				}
			}

			if status == .authorizedWhenInUse || status == .authorizedAlways {
				if itemActive == false {
					self.viewLayer.activeItems(indexs: [IndexPath(row: itemIndex, section: 0)]) }
			} else {
				if itemActive == true {
					self.viewLayer.deactiveItems(indexs: [IndexPath(row: itemIndex, section: 0)]) }
			}
		}
}

extension XSecuritySettingsController {
    func updTouchId(index: IndexPath, newValue: Bool) {
        DataManager.settingsHelper.touchId = newValue
        viewLayer.updItem(index: index, model: XBaseSettingsSwitchCellModel(isActive: true, title: strings.needTouchTitle, subTitle: nil, subTitleIsOn: nil, isOn: newValue))
    }
    
    func deactivatePinCode(index: IndexPath) {
        let alert = XAlertResetPinCodeController(
            confirm: { a, p in
                if p.md5 == DataManager.defaultHelper.password {
                    DataManager.settingsHelper.needPin = false
                    DataManager.defaultHelper.pin = ""
                    self.viewLayer.updItem(index: index, model: XBaseSettingsSwitchCellModel(isActive: true, title: self.strings.needPinCodeTitle, subTitle: self.strings.needPinCodeSubTitle, subTitleIsOn: self.strings.needPinCodeSubTitleIsOn, isOn: false))
                    self.viewLayer.deactiveItems(indexs: [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0), IndexPath(row: 3, section: 0)])
                    a.dismiss(animated: true)
                } else { a.passwordNotCorrect() }
            })
        navigationController?.present(alert, animated: true)
    }
    
    func activatePinCode(index: IndexPath) {
        let alert = XAlertController(style: self.baseAlertStyle(), strings: strings.setPinAlert, positive: {
            DataManager.settingsHelper.needPin = true
            self.viewLayer.updItem(index: index, model: XBaseSettingsSwitchCellModel(isActive: true, title: self.strings.needPinCodeTitle, subTitle: self.strings.needPinCodeSubTitle, subTitleIsOn: self.strings.needPinCodeSubTitleIsOn, isOn: true))
            self.viewLayer.activeItems(indexs: [IndexPath(row: 1, section: 0), IndexPath(row: 2, section: 0), IndexPath(row: 3, section: 0)])
            self.setNewPinCode()
        })
        navigationController?.present(alert, animated: true)
    }
    
    func setNewPinCode() {
        let controller = XPinCodeController(state: .recreate)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func resetPinCode() {
        let alert = XAlertResetPinCodeController(
            confirm: { a, p in
                if p.md5 == DataManager.defaultHelper.password { DataManager.defaultHelper.pin = ""; a.dismiss(animated: true) }
                else { a.passwordNotCorrect() }
            }, isResetFromSettings: true)
        navigationController?.present(alert, animated: true)
    }
	
		func updSendLocation(index: IndexPath, newValue: Bool) {
				DataManager.settingsHelper.sendLocation = newValue
				viewLayer.updItem(index: index, model: XBaseSettingsSwitchCellModel(isActive: true, title: strings.sendLocationTitle, subTitle: strings.sendLocationDescription, subTitleIsOn: strings.sendLocationDescription, isOn: newValue))
		}
}
