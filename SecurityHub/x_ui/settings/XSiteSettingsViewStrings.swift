//
//  XSiteSettingsViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XSiteSettingsViewStrings: XSiteSettingsViewStringsProtocol {
    /// Название объекта
    var nameTitle: String { "N_TITLE_SITE_NAME".localized() }
		
    /// Управление делегированием
    var delegationListText: String { "N_SITE_CONTROL_DELEGATION".localized() }
    
    /// Отказаться от делегирования
    var deleteDelegationText: String { "SITE_MENU_REFUSE_DELEGATION".localized() }
	
		/// Автовзятие
		var autoarmTitle: String { "SITE_AUTOARM_TITLE".localized() }
		var autoarmDisable: String { "SITE_AUTOARM_NO_SET".localized() }
		var autoarmSchedule: String { "SITE_AUTOARM_ON_SCHEDULE".localized() }
		var autoarmLoading: String { "SITE_AUTOARM_LOADING".localized() }
    
    /// Удалить
    var deleteText: String { "USERS_MENU_DELETE".localized() }
}
