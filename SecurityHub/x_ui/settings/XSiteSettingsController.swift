//
//  XSiteSettingsController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 19.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XSiteSettingsControllerStringsProtocol {
    var changeNameTitle: String { get }
    var noRight: String { get }
    
    var deleteDelegationTitle: String { get }
    var deleteDelegationPositiveText: String { get }
    var deleteDelegationNegativeText: String { get }

    func getRemoveAlertStrings(siteName: String?) -> XRemoveControllerStringsProtocol
}

class XSiteSettingsController: XBaseViewController<XSiteSettingsView> {
    override var tabBarIsHidden: Bool { true }

    private let siteId: Int64, isAdmin: Bool
    private var siteName: String?, isDelegated: Bool
    private var disposable: Disposable?
		private var autoArm: DAutoArmEntity?
		private var autoArmNeedUpdate = false
    
    private lazy var navigationViewBuilder: XNavigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    private lazy var strings: XSiteSettingsControllerStringsProtocol = XSiteSettingsControllerStrings()
    private lazy var viewLayer: XSiteSettingsViewLayer = self.mainView
    
    init(siteId: Int64) {
        self.siteId = siteId
        self.isAdmin = DataManager.shared.getUser().roles & Roles.ORG_ADMIN != 0 || DataManager.shared.getUser().roles & Roles.DOMEN_ADMIN != 0
        self.isDelegated = false
        super.init(nibName: nil, bundle: nil)
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
				mainView.setLoading()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.getSites()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .filter({ $0.site.id == self.siteId })
            .subscribe(onNext: { result in
                self.siteName = result.site.name
                self.viewLayer.setData(name: result.site.name, isDelegated: result.site.delegated == 1)
                self.isDelegated = result.site.delegated == 1

							if self.autoArmNeedUpdate == false {
								self.getAutoArm (siteId: result.site.id, autoArmId: result.site.autoarmId) }
								else {
									self.autoArm = DataManager.shared.getAutoArm(site: result.site.id)
									self.viewLayer.setData (autoarm: self.autoArm!) }
							self.autoArmNeedUpdate = false
            })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
	
		private func getAutoArm (siteId: Int64, autoArmId: Int64) {
			_ = DataManager.shared.hubHelper.get(.AUTOARM_GET, D: ["site_id": siteId])
					.observeOn(ThreadUtil.shared.mainScheduler)
					.subscribe(onSuccess: { [weak self] (result: DCommandResult, value: HubAutoArm?) in
							guard let self = self else { return }
							if result.success, let value = value {
								if ( autoArmId < 1 && value.runAt == 0 && value.active == 0 ) ||
										( autoArmId > 0 && (value.runAt + value.active) > 0 ) {
									self.autoArm = DAutoArmEntity (id: autoArmId, site: siteId, device: 0, section: 0, runAt: value.runAt, active: value.active, daysOfWeek: value.daysOfWeek, hour: value.hour, minute: value.minute, timezone: value.timezone, lastExecuted: value.lastExecuted)
									self.viewLayer.setData(autoarm: self.autoArm!)
									DataManager.shared.updateAutoArm(autoArm: self.autoArm!) }
							} else {
								self.showErrorColtroller(message: result.message)
							}
					}, onError: { [weak self] error in
							self?.showErrorColtroller(message: error.localizedDescription)
					})
		}
    
    private func changeName(value: String) {
        _ = DataManager.shared.rename(site_id: self.siteId, name: value)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onSuccess: { [weak self] result in
                if (!result.success) { self?.showErrorColtroller(message: result.message) }
            }, onError: { [weak self] error in
                self?.showErrorColtroller(message: error.localizedDescription)
            })
    }
    
    //TODO
    private func deleteSite(b: Bool?) {
				DataManager.shared.removeAutoArm(site: self.siteId)
        _ = Observable.from(DataManager.shared.getDeviceIds(site_id: self.siteId))
            .concatMap({ id -> Observable<Bool> in DataManager.shared.removeDevice(device_id: id, site: 0) })
            .toArray()
            .asObservable()
            .concatMap({ r -> Observable<Bool> in DataManager.shared.delete(site_id: self.siteId)})
            .concatMap({ r -> Observable<Int> in Observable.timer(.seconds(1), scheduler: ThreadUtil.shared.backScheduler)})
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] b in self?.back(); DataManager.shared.updateSites() })
    }
    
    //TODO
    private func deleteDelegation() {
        let _ = DataManager.shared.delegate_delete(site: self.siteId, domain_id: DataManager.shared.getUser().domainId)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] b in self?.back() })
    }
}

extension XSiteSettingsController: XSiteSettingsViewDelegate {
    func changeNameViewTapped(name: String) {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        if isDelegated { return }
        let controller = XTextEditController(title: strings.changeNameTitle, value: name, onCompleted: { vc, value in self.changeName(value: value); self.back(); })
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func delegationListViewTapped() {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        let controller = XSiteDelegationListController(siteId: self.siteId, siteName: self.siteName ?? "")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func deleteDelegationViewTapped() {
        guard isAdmin  else { return showErrorColtroller(message: strings.noRight) }
        let alert = XAlertController(
            style: XAlertView.Style(
                backgroundColor: .white,
                title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
                buttonOrientation: .horizontal,
                positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0xf9543a)),
                negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042))
            ),
            strings: XAlertView.Strings(title: strings.deleteDelegationTitle, text: nil, positive: strings.deleteDelegationPositiveText, negative: strings.deleteDelegationNegativeText),
            positive: { self.deleteDelegation() })
        navigationController?.present(alert, animated: true)
    }
	
		func autoarmViewTapped() {
			if autoArm == nil { return }
			
			if !isAdmin || isDelegated {
				showErrorColtroller(message: strings.noRight)
				return
			}
			let aac = XAutoArmController()
			aac.autoArm = autoArm
			navigationController?.pushViewController(aac, animated: true)
			autoArmNeedUpdate = true
		}
    
    func deleteViewTapped() {
        guard isAdmin else { return showErrorColtroller(message: strings.noRight) }
        guard DataManager.shared.dbHelper.getArmStatus(site_id: siteId) == .disarmed else { return showErrorColtroller(message: "SITE_DISARM_MESSAGE_DEL".localized()) }
        let controller = XRemoveController(strings: strings.getRemoveAlertStrings(siteName: self.siteName), action: self.deleteSite)
        navigationController?.pushViewController(controller, animated: true)
    }
}
