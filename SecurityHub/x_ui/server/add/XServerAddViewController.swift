//
//  XServerAddViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.09.2023.
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit

final class XServerAddViewController: XBaseViewController<XServerAddView> {
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: nil,
        rightViews: [
            XNavigationViewRightViewBuilder(
                title: "N_BUTTON_SAVE".localized(),
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.colorFromHex(0x414042),
                viewTapped: self.change
            )
        ]
    )
    
    private var item: XServerChangerView.Item?
    private var completion: (XServerChangerView.Item) -> Void
    
    init(item: XServerChangerView.Item? = nil, _ completion: @escaping (XServerChangerView.Item) -> Void) {
        self.item = item
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
        
        mainView.nameView.text = item?.name
        mainView.urlView.text = item?.url
        mainView.portView.text = item?.port
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationViewBuilder(navigationViewBuilder)
    }
    
    private func isValidUrl(_ url: String) -> Bool {
        guard
            let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue),
            let match = detector.firstMatch(in: url, options: [], range: NSRange(location: 0, length: url.utf16.count))
        else {
            return false
        }
        
        return match.range.length == url.utf16.count
    }
    
    private func isValidIP(_ url: String) -> Bool {
        let regex = "(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})"
        return NSPredicate(format:"SELF MATCHES %@", regex).evaluate(with: url)
      }
    
    private func change() {
        guard mainView.nameView.text?.isEmpty == false else {
            return mainView.nameView.showValidationError(message: "N_ERROR_ENTER_SERVER_NAME".localized())
        }
        guard let urlString = mainView.urlView.text, (isValidUrl(urlString) || isValidIP(urlString)) else {
            return mainView.urlView.showValidationError(message: "N_ERROR_ENTER_SERVER_ADDRESS".localized())
        }
        guard let port = mainView.portView.text, let _ = Int(port) else {
            return mainView.portView.showValidationError(message: "N_ERROR_ENTER_PORT".localized())
        }
        
        let item = XServerChangerView.Item(name: mainView.nameView.text ?? "", url: urlString, port: port, isSelected: item?.isSelected ?? false)
        completion(item)
        navigationController?.popViewController(animated: true)
    }
}
