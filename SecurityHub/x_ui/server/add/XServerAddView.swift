//
//  XServerAddView.swift
//  SecurityHub
//
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit

final class XServerAddView: UIView {
    
    private let style = XTextField.Style(
        backgroundColor: UIColor.white,
        unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
        selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
        textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
        font: UIFont(name: "Open Sans", size: 15.5)
    )
    
    lazy var nameView = XTextField(style: style)
    
    lazy var urlView = XTextField(style: style)

    lazy var portView = XTextField(style: style)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        nameView.placeholder = "N_TITLE_SERVER_NAME".localized()
        nameView.keyboardType = .namePhonePad
        urlView.placeholder = "N_TITLE_SERVER_ADDRESS".localized()
        urlView.keyboardType = .URL
        portView.placeholder = "N_TITLE_SERVER_PORT".localized()
        portView.keyboardType = .numberPad
        addSubview(nameView)
        addSubview(urlView)
        addSubview(portView)
        
        nameView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(20.0)
            $0.height.equalTo(58.0)
        }
        urlView.snp.makeConstraints {
            $0.top.equalTo(nameView.snp.bottom).offset(20.0)
            $0.leading.trailing.equalToSuperview().inset(20.0)
            $0.height.equalTo(58.0)
        }
        portView.snp.makeConstraints {
            $0.top.equalTo(urlView.snp.bottom).offset(20.0)
            $0.leading.equalToSuperview().inset(20.0)
            $0.height.equalTo(58.0)
            $0.width.equalTo(200)
            $0.bottom.lessThanOrEqualToSuperview()
        }
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
}
