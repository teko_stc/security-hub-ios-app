//
//  XServerChanger.swift
//  SecurityHub
//
//  Copyright © 2023 TEKO. All rights reserved.
//

import Foundation

class XServerChanger {
    class func url() -> String {
        return "ws://\(item.url):\(item.port)/"
    }
	
		class func regUrl() -> String {
			return "https://\(item.url)"
		}

    static var ivideon: (url: String, id: String)?
    
    static var fcm: (gcm_sender_id: String, application_id: String, api_key: String)?
    
    static var item: XServerChangerView.Item {
        let userDefaults = UserDefaults(suiteName: "XServerChangerViewController")
        guard
            let data = userDefaults?.object(forKey: "XServerChangerView.Item") as? Data,
            let items = try? JSONDecoder().decode(Array<XServerChangerView.Item>.self, from: data),
            let item = items.first(where: \.isSelected)
        else {
            let item = XServerChangerView.Item(
                name: XTargetUtils.defaultName,
                url: XTargetUtils.defaultTarger,
                port: "1122",
                isSelected: true
            )
            if let data = try? JSONEncoder().encode([item]) {
                userDefaults?.set(data, forKey: "XServerChangerView.Item")
            }
            return item
        }
        return item
    }

}
