//
//  XServerChangerView.swift
//  SecurityHub
//
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit

final class XServerChangerView: UIView {
    
    struct Item: Equatable, Codable {
        let name: String
        let url: String
        let port: String
        var isSelected: Bool
        
        static func == (lhs: Item, rhs: Item) -> Bool {
            lhs.url == rhs.url && lhs.port == rhs.port
        }
    }
    
    private let tableView = UITableView()
    
    private var items: [Item] = []
    
    private var selected, removed: ((XServerChangerView.Item) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.alwaysBounceVertical = false
        tableView.register(UINib(nibName: "DashboardAddCell", bundle: nil), forCellReuseIdentifier: "DashboardAddCellIdentifier")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        addSubview(tableView)
        
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func reload(_ items: [Item]) {
        self.items = items
        tableView.reloadData()
        if let index = items.firstIndex(where: \.isSelected) {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .middle)
        }
    }
    
    func callbacks(selected: ((XServerChangerView.Item) -> Void)?, removed: ((XServerChangerView.Item) -> Void)?) {
        self.selected = selected
        self.removed = removed
    }
    
}

// MARK: - UITableViewDataSource

extension XServerChangerView: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(indexPath: indexPath)
        let item = items[indexPath.row]
        cell.setup(item: DashboardAddItem(
            image: nil,
            selectedImage: nil,
            title: item.name,
            description: "Адрес: \(item.url)\nПорт: \(item.port)\n"
        ))
        cell.setForService()
        
        return cell
    }
    
    func dequeueReusableCell(indexPath: IndexPath) -> DashboardAddCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardAddCellIdentifier", for: indexPath) as! DashboardAddCell
        cell.tag = indexPath.item
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(sender:)))
        cell.addGestureRecognizer(longPress)
        return cell
    }

}

// MARK: - UITableViewDelegate

extension XServerChangerView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        selected?(items[indexPath.row])
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == .began, let cellTag = sender.view?.tag {
            removed?(items[cellTag])
        }
    }
}
