//
//  XServerChangerViewController.swift
//  SecurityHub
//
//  Copyright © 2023 TEKO. All rights reserved.
//

import UIKit

final class XServerChangerViewController: XBaseViewController<XServerChangerView> {
    
    private var userDefaults = UserDefaults(suiteName: "XServerChangerViewController")
    
    func alertStyle(_ buttonOrientation: CTFontOrientation) -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: .white,
            title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
            text: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042)),
            buttonOrientation: buttonOrientation,
            positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 18), color: UIColor.red),
            negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 18), color: UIColor.colorFromHex(0x414042))
        )
    }
    
    private var items: [XServerChangerView.Item] {
        let items: [XServerChangerView.Item]
        if 
            let data = userDefaults?.object(forKey: "XServerChangerView.Item") as? Data,
            let it = (try? JSONDecoder().decode(Array<XServerChangerView.Item>.self, from: data))
        {
            items = it
        } else {
            items = [
                XServerChangerView.Item(
                    name: XTargetUtils.defaultName,
                    url: XTargetUtils.defaultTarger,
                    port: "1122",
                    isSelected: true
                )
            ]
            if let data = try? JSONEncoder().encode(items) {
                userDefaults?.set(data, forKey: "XServerChangerView.Item")
            }
        }
        
        return items
    }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: nil,
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.add)]
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationViewBuilder(navigationViewBuilder)
        mainView.reload(items)
        mainView.callbacks(selected: selected, removed: removed)
    }
    
    private func add() {
        let controller = XServerAddViewController() { [weak self] item in
            guard let selfUw = self else {
                return
            }
            
            var items = selfUw.items
            if let index = items.firstIndex(where: { $0 == item }) {
                items[index] = item
            } else {
                items.insert(item, at: 0)
            }
            if let data = try? JSONEncoder().encode(items) {
								selfUw.userDefaults?.set(data, forKey: "XServerChangerView.Item")
								selfUw.mainView.reload(items)
            }
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func edit(item: XServerChangerView.Item? = nil) {
        let controller = XServerAddViewController(item: item) { [weak self] newItem in
            guard let selfUw = self else {
                return
            }
            
            var items = selfUw.items
            if let index = items.firstIndex(where: { $0 == item }) {
                items[index] = newItem
            } else {
                items.insert(newItem, at: 0)
            }
            if let data = try? JSONEncoder().encode(items) {
								selfUw.userDefaults?.set(data, forKey: "XServerChangerView.Item")
								selfUw.mainView.reload(items)
            }
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func selected(item: XServerChangerView.Item) {
        self.mainView.reload(items)
        guard item != items.first(where: \.isSelected) else { return }

        let alertStrings = XAlertView.Strings(
            title: "N_TITLE_SERVER_CONFIG_APPLY".localized(), text: nil, positive: "Ok".localized(), negative: "button_cancel".localized())
        let alertVC = XAlertController(style: alertStyle(.horizontal), strings: alertStrings,
        positive: { [weak self] in
            guard let selfUw = self else {
                return
            }

            var items = selfUw.items
            items.indices.forEach { items[$0].isSelected = items[$0] == item  }
            if let data = try? JSONEncoder().encode(items) {
								selfUw.userDefaults?.set(data, forKey: "XServerChangerView.Item")
								selfUw.mainView.reload(items)
            }
        }, negative: { [weak self] in
            guard let selfUw = self else {
                return
            }
            
						let items = selfUw.items
						selfUw.mainView.reload(items)
        })
        self.present(alertVC, animated: true, completion: nil)
    }
    
    private func removed(item: XServerChangerView.Item) {
        let alertStrings = XAlertView.Strings(
            title: nil,
            text: nil,
            positive: "SITE_NAME_CHANGE".localized(),
            negative: items.count > 1 ? "DELETE".localized() : nil
        )
        let alertVC = XAlertController(style: alertStyle(.vertical), strings: alertStrings) { [weak self] in
            self?.edit(item: item)
        } negative: {  [weak self] in
            guard let selfUw = self else {
                return
            }

            var items = selfUw.items
            items.removeAll(where: { $0 == item })
            if item.isSelected { items[0].isSelected = true }
            if let data = try? JSONEncoder().encode(items) {
								selfUw.userDefaults?.set(data, forKey: "XServerChangerView.Item")
								selfUw.mainView.reload(items)
            }
        }
        self.present(alertVC, animated: true, completion: nil)
    }
}
