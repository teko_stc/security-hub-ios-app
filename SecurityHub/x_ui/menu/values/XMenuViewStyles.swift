//
//  XMenuControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XMenuView {
    func xMenuHeaderViewStyle() -> XMenuHeaderView.Style {
        return XMenuHeaderView.Style(
            backgroundColor: UIColor.white,
            lineColor: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 0.7),
            title: XMenuHeaderViewStyle.Title(
                font: UIFont(name: "Open Sans", size: 22),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            icon: XMenuHeaderViewStyle.Icon(
                name: "ic_user"
            ),
            button: XImageButton.Style(
                imageName: "ic_bezopasnost",
                font: UIFont(name: "Open Sans", size: 15.5),
                selectColor: UIColor(red: 0xff/255, green: 0x95/255, blue: 0x4d/255, alpha: 1),
                unselectColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                backgroundSelectColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1)
            )
        )
    }
    
    func xMenuSelectorViewCellStyle() -> XSelectorView.Cell.Style {
        return XSelectorView.Cell.Style(
            backgroundColor: UIColor.white,
            selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
            title: XSelectorViewCell.Style.Label(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                margins: UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 24)
            ),
            icon: XSelectorViewCell.Style.Icon(
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                size: CGSize(width: 36, height: 36),
                margins: UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 6)
            )
        )
    }
    
    func xMenuSelectorViewHeaderStyle() -> XSelectorView.Header.Style {
        return XSelectorView.Header.Style(
            backgroundColor: UIColor.white,
            title: XSelectorView.Header.Style.Title(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                margins: UIEdgeInsets(top: 20, left: 24, bottom: 6, right: 24)
            )
        )
    }
}
