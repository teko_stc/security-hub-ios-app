//
//  XMenuControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Localize_Swift

class XMenuViewStrings: XMenuHeaderViewStrings {
    /// Звонок в техподдержку
    var buttonCall: String { "N_PROFILE_SUPPORT_CALL".localized() }
    
    /// Приложение
    var section_1: String { "N_PROFILE_APP".localized() }
    
    /// Безопасность
    var section_1_item_1: String { "N_PROFILE_SECURITY".localized() }
    
    /// Уведомления
    var section_1_item_2: String { "N_PROFILE_NOTIFICATIONS".localized() }
    
    /// Соединение
    var section_1_item_3: String { "N_PROFILE_CONNECTION".localized() }
    
    /// Список пользователей
    var section_1_item_4: String { "N_PROFILE_ACCESS".localized() }
    
    /// Язык
    var section_1_item_5: String { "N_PROFILE_LANGUAGE".localized() }

    /// Интерфейс
    var section_1_item_6: String { "N_PROFILE_INTERFACE".localized() }
    
    /// Устройства
    var section_2: String { "DEVICES_TITLE".localized() }
    
    /// Установка APN
    var section_2_item_1: String { "N_LOCAL_CONFIG".localized() }
    
    /// Информация
    var section_3: String { "N_PROFILE_INFO".localized() }
    
    /// Охранные организации
    var section_3_item_1: String { "N_PROFILE_SEC_CONPANIES".localized() }
    
    /// Интернет-магазин
    var section_3_item_2: String { "N_PROFILE_SHOP".localized() }
    
    /// Лиц. соглашение
    var section_3_item_3: String { "N_PROFILE_LEGAL".localized() }
    
    /// О приложении
    var section_3_item_4: String { "N_PROFILE_ABOUT".localized() }

}
