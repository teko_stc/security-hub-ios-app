//
//  XMenuVCStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XMenuVCStrings: XMenuControllerStringsProtocol {
    /// У вас нет полномочий для выполнения данного действия
    var noRoleError: String { "EA_NO_RIGHTS".localized() }
    
    /// Вы действительно хотите посетить интернет-магазин? Нажмите Да, чтобы открыть его в окне браузера?
    var shopAlertTitle: String { "OPEN_SHOP_DIALOG_MESSAGE".localized() }
    
    /// Да
    var shopAlertPositive: String { "YES".localized() }
    
    /// Нет
    var shopAlertNegative: String { "NO".localized() }
}
