//
//  XMenuController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

extension XMenuController: XMenuViewDelegate {
    func profileInfoClick() {
        let controller = XCurrentUserController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func callButtonClick() {
        guard DataManager.settingsHelper.needPin else {
            return UIApplication.shared.open(URL(string: "tel://88001008945")!, completionHandler: { _ in self.dismiss(animated: true) })
        }
        
        let alert: UIViewController = XPinCodeAlertController()
        navigationController?.present(alert, animated: true)
    }
    
    func securityClick() {
        let controller = XSecuritySettingsController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func notificationClick() {
        let controller = XNotificationSettingsController()
        navigationController?.pushViewController(controller, animated: true)
//        navigationController?.pushViewController(OptionsController("options_notification_classes".localized()), animated: false)
    }
    
    func sharedAccessClick() {
        if !(DataManager.defaultHelper.roles & Roles.ORG_ADMIN != 0 || DataManager.defaultHelper.roles & Roles.DOMEN_ADMIN != 0) {
            return showErrorColtroller(message: strings.noRoleError)
        }
        let controller = XUsersController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func languageClick() {
        navigationController?.pushViewController(XLanguageController(), animated: true)
    }
    
    func apnClick() {
        navigationController?.pushViewController(XApnController(), animated: true)
    }
    
    func securityOrganizationClick() {
        navigationController?.pushViewController(XCompaniesController(),
                                                 animated: true)
    }
    
    func shopClick() {
        let style = XAlertView.Style(
            backgroundColor: .white,
            title: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x414042)),
            text: XAlertView.Style.Lable(font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042)),
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0xf9543e)),
            negative: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042))
        )
        let alert = XAlertController(style: style, strings: XAlertView.Strings(title: strings.shopAlertTitle, positive: strings.shopAlertPositive, negative: strings.shopAlertNegative), positive: {
            UIApplication.shared.open(URL(string: "https://security-hub.ru")!)
        })
        navigationController?.present(alert, animated: true)
    }
    
    func personsAgreementClick() {
        navigationController?.pushViewController(XAgreementController(), animated: true)
    }
    
    func aboutAppClick() {
        let controller = XAboutController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func interfaceClick() {
        let controller = XInterfaceSettingsController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func ivideonClick() {
        isIvideonAccountConnectedDisposable?.dispose()
        isIvideonAccountConnectedDisposable = isIvideonAccountConnected().subscribe(onSuccess: showAddCameraBottomSheet)
    }
    
    func showAddCameraBottomSheet(_ isIvideonAccountConnected: Bool) {
        let items: [XBottomSheetViewItem]
        if isIvideonAccountConnected {
            items = [
                XBottomSheetViewItem(title: "Ivideon", text: "N_WIZ_ALL_IV_ALREADY_BINDED".localized())
            ]
        } else {
            items = [
                XBottomSheetViewItem(title: "Ivideon", text: "N_WIZ_ALL_IV_BIND_PRESS".localized())
            ]
        }
        
        let bottomSheet = XBottomSheetController(
            style: addCameraBottomSheetStyle(),
            title: "N_CAMERA_TYPE_SELECT".localized(),
            selectedIndex: isIvideonAccountConnected ? 0 : -1,
            items: items
        )
        bottomSheet.onSelectedItem = addCameraBottomSheetSelectedItem
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func addCameraBottomSheetSelectedItem(controller: XBottomSheetController, index: Int, item: XBottomSheetViewItem) {
        switch item.text {
        case "N_WIZ_ALL_IV_ALREADY_BINDED".localized(): controller.dismiss(animated: true) { self.showDisconectIvideonAccountAlert() }
        case "N_WIZ_ALL_IV_BIND_PRESS".localized(): controller.dismiss(animated: true) { self.showIvideonAuthController() }
        default: return controller.dismiss(animated: true)
        }
    }
    
    func showDisconectIvideonAccountAlert() {
        let alert = XAlertController(
            style: disconectIvideonAccountAlertStyle(),
            strings: XAlertView.Strings(
                /// Хотите отключить аккаунт iVideon от учетной записи Security Hub?
                title: "N_PROFILE_IV_UNBIND_Q".localized(),
                text: nil,
                /// Да
                positive: "YES".localized(),
                /// Нет
                negative: "NO".localized()
            ),
            positive: disconectIvideonAccountViewTapped
        )
        navigationController?.present(alert, animated: true)
    }

    func disconectIvideonAccountViewTapped() {
        disconectIvideonAccountDisposable?.dispose()
        disconectIvideonAccountDisposable = disconectIvideonAccount().subscribe(onSuccess: disconectIvideonAccountSucces, onError: showErrorAlert)
    }
    
    func showErrorAlert(error: Error) {
        let alert = XAlertController(
            style: errorAlertStyle(),
            strings: XAlertView.Strings(title: error.localizedDescription, text: nil, positive: "OK".localized(), negative: nil)
        )
        navigationController?.present(alert, animated: true)
    }
    
    func showErrorAlert(message: String) {
        let alert = XAlertController(
            style: errorAlertStyle(),
            strings: XAlertView.Strings(title: message, text: nil, positive: "OK".localized(), negative: nil)
        )
        navigationController?.present(alert, animated: true)
    }
    
    func disconectIvideonAccountSucces(result: DCommandResult) {
        if !result.success { return showErrorAlert(message: result.message) }
        navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: HubNotification.iviAuth, object: nil)
    }
    
    func showIvideonAuthController() { self.navigationController?.pushViewController(XIvideonAuthController(), animated: true) }
    
    func connectionClick() {
        let contoller = XConnectionsViewController()
        navigationController?.pushViewController(contoller, animated: true)
    }
}

