//
//  XMenuView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XMenuView: UIView {
    public var delegate: XMenuViewDelegate? {
        didSet {
            xMenuHeaderView.delegate = delegate
        }
    }
    
    private var xMenuHeaderView: XMenuHeaderView!
    private var xSelectorView: XSelectorView!
    private let strings = XMenuViewStrings()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
            
    public func setProfileInfo(userName: String) {
        xMenuHeaderView.setContent(userName: userName)
    }
    
    public func setLocalizationSettings(isRussian: Bool) {
        xMenuHeaderView.callButton(isHidden: !isRussian)
        initMenuItems(isRussian: isRussian)
    }
        
    private func initViews() {
        xSelectorView = XSelectorView(headerStyle: xMenuSelectorViewHeaderStyle(), cellStyle: xMenuSelectorViewCellStyle(), sectionFooterHeight: 42)
        xMenuHeaderView = XMenuHeaderView(style: xMenuHeaderViewStyle(), strings: XMenuViewStrings())
        addSubview(xMenuHeaderView)
        addSubview(xSelectorView)
    }
    
    private func setConstraints() {
        xMenuHeaderView.translatesAutoresizingMaskIntoConstraints = false
        xSelectorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            xMenuHeaderView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            xMenuHeaderView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            xMenuHeaderView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            
            xSelectorView.topAnchor.constraint(equalTo: xMenuHeaderView.bottomAnchor, constant: 0.5),
            xSelectorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            xSelectorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            xSelectorView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func initMenuItems(isRussian: Bool) {
        var items = [
            XSelectorView.Section(title: strings.section_1, items: [
                XSelectorView.Section.Item(
                    title: strings.section_1_item_1,
                    imageName: "ic_bezopasnost-1",
                    click: { self.delegate?.securityClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_1_item_2,
                    imageName: "ic_uvedomlenie",
                    click: { self.delegate?.notificationClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_1_item_3,
                    imageName: "ic_soedinenie",
                    click: { self.delegate?.connectionClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_1_item_6,
                    imageName: "ic_interface",
                    click: { self.delegate?.interfaceClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_1_item_4,
                    imageName: "ic_sovmest_dostup",
                    click: { self.delegate?.sharedAccessClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_1_item_5,
                    imageName: "ic_yazuk",
                    click: { self.delegate?.languageClick?() }
                )
            ]),
            
            XSelectorView.Section(title: strings.section_2, items: [
                XSelectorView.Section.Item(
                    title: strings.section_2_item_1,
                    imageName: "ic_ustanovka_apn",
                    click: { self.delegate?.apnClick?() }
                )
            ])
        ]
        
        if XTargetUtils.ivideon != nil {
            items.append(
                XSelectorView.Section(
                    title: "N_PROFILE_ACCOUNTS".localized(),
                    items: [
                        XSelectorView.Section.Item(
                            title: "Ivideon".localized(),
                            imageName: "ic_camera_border",
                            click: { [weak self] in self?.delegate?.ivideonClick?() }
                        )
                    ]
                )
            )
        }
        
        if XTargetUtils.isHubTarget {
            let val = XSelectorView.Section(title: strings.section_3, items: [
                XSelectorView.Section.Item(
                    title: strings.section_3_item_3,
                    imageName: "ic_soglashenie",
                    click: { self.delegate?.personsAgreementClick?() }
                ),
                XSelectorView.Section.Item(
                    title: strings.section_3_item_4,
                    imageName: "ic_o_prilozhenii",
                    click: { self.delegate?.aboutAppClick?() }
                )
            ])
            items.append(val)
            if isRussian {
                let index = XTargetUtils.ivideon == nil ? 2 : 3
                items[index].items.insert(contentsOf: [
                    XSelectorView.Section.Item(
                        title: strings.section_3_item_1,
                        imageName: "ic_ohr_org",
                        click: { self.delegate?.securityOrganizationClick?() }
                    ),
                    XSelectorView.Section.Item(
                        title: strings.section_3_item_2,
                        imageName: "ic_internet_magazin",
                        click: { self.delegate?.shopClick?() }
                    ),
                ], at: 0)
            }
        }
        
        xSelectorView.reloadData(items)
    }
}
