//
//  XMenuHeaderVIew.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 06.07.2021.
//

import UIKit

class XMenuHeaderView: UIView {
//    static let height: CGFloat = 140
    
    public var delegate: XMenuHeaderViewDelegate?

    private let titleView = UILabel()
    private let lineLayer: CALayer = CALayer()
    private var iconView: UIImageView!
    private var buttonCallView: XImageButton!
    private var buttonCallViewBottomAnchor, iconViewBottomAnchor: NSLayoutConstraint!
    
    init(style: Style, strings: Strings) {
        super.init(frame: .zero)
        iniViews(style: style, strings: strings)
        setConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
  
    override func layoutSubviews() {
        lineLayer.frame = CGRect(x: 0, y: frame.size.height, width: frame.size.width, height: 0.5)
    }
    
    public func setContent(userName: String) {
        titleView.text = userName
    }
    
    public func callButton(isHidden: Bool) {
        buttonCallView.isHidden = isHidden
        buttonCallViewBottomAnchor.isActive = !isHidden
        iconViewBottomAnchor.isActive = isHidden
    }
    
    private func iniViews(style: Style, strings: Strings) {
        backgroundColor = style.backgroundColor

        iconView = UIImageView(image: UIImage(named: "ic_user"))
        
        titleView.numberOfLines = 2
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        
        buttonCallView = XImageButton(style: style.button)
        buttonCallView.setTitle(strings.buttonCall, for: UIControl.State.normal)
        buttonCallView.addTarget(self, action: #selector(buttonCallClick), for: .touchUpInside)
        
        lineLayer.backgroundColor = style.lineColor.cgColor
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(buttonHeaderClick)))
         
        layer.addSublayer(lineLayer)
        addSubview(iconView)
        addSubview(titleView)
        addSubview(buttonCallView)
    }
    
    private func setConstraints() {
        iconView.translatesAutoresizingMaskIntoConstraints = false
        titleView.translatesAutoresizingMaskIntoConstraints = false
        buttonCallView.translatesAutoresizingMaskIntoConstraints = false
        buttonCallViewBottomAnchor = buttonCallView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1)
        iconViewBottomAnchor = iconView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: self.topAnchor, constant: 39),
            iconView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 19),
            iconView.heightAnchor.constraint(equalToConstant: 51),
            iconView.widthAnchor.constraint(equalToConstant: 46),
            
            titleView.topAnchor.constraint(equalTo: self.topAnchor, constant: 11 + 20),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 11),
            titleView.heightAnchor.constraint(equalToConstant: 62),
            titleView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -24),
            
            buttonCallView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 4),
            buttonCallView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            buttonCallView.heightAnchor.constraint(equalToConstant: 50),
            buttonCallView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            buttonCallViewBottomAnchor
        ])
    }
    
    @objc private func buttonCallClick() {
        delegate?.callButtonClick?()
    }
    
    @objc private func buttonHeaderClick(sender: UIGestureRecognizer) {
        if sender.state == .ended { delegate?.profileInfoClick?() }
    }
}
