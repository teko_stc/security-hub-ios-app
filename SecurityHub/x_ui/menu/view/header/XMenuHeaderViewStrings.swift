//
//  XMenuHeaderViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XMenuHeaderViewStrings {
    var buttonCall: String { get }
}

extension XMenuHeaderView {
    typealias Strings = XMenuHeaderViewStrings
}
