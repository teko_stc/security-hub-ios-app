//
//  XMenuHeaderViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XMenuHeaderViewStyle {
    struct Title {
        let font: UIFont?
        let color: UIColor
    }
    
    struct Icon {
        let name: String
    }

    let backgroundColor: UIColor
    
    let lineColor: UIColor

    let title: Title
    
    let icon : Icon
    
    let button: XImageButton.Style
}

extension XMenuHeaderView {
    typealias Style = XMenuHeaderViewStyle
}
