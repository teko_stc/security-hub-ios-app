//
//  XMenuHeaderViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

@objc protocol XMenuHeaderViewDelegate {
    @objc optional func profileInfoClick()

    @objc optional func callButtonClick()
}
