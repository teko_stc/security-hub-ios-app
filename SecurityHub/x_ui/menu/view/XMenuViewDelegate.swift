//
//  XMenuViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import Foundation

@objc protocol XMenuViewDelegate: XMenuHeaderViewDelegate {

    @objc optional func securityClick()
    
    @objc optional func notificationClick()
    
    @objc optional func connectionClick()
    
    @objc optional func sharedAccessClick()
    
    @objc optional func languageClick()
    
    @objc optional func apnClick()
    
    @objc optional func ivideonClick()
    
    @objc optional func securityOrganizationClick()
    
    @objc optional func shopClick()
    
    @objc optional func personsAgreementClick()
    
    @objc optional func aboutAppClick()

    @objc optional func interfaceClick()
}
