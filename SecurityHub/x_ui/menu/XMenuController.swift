//
//  XMenuController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

protocol XMenuControllerStringsProtocol {
    var noRoleError: String { get }
    var shopAlertTitle: String { get }
    var shopAlertPositive: String { get }
    var shopAlertNegative: String { get }
}

class XMenuController: XBaseViewController<XMenuView> {
    public lazy var strings: XMenuControllerStringsProtocol = XMenuVCStrings()
    
    private var disposable: Disposable?

    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        mainView.setLocalizationSettings(isRussian: DataManager.defaultHelper.language == "ru")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = DataManager.shared.getOperatorObservable()
            .subscribe(onNext: { user in self.mainView.setProfileInfo(userName: user.name) })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposable?.dispose()
    }
    
    var isIvideonAccountConnectedDisposable: Disposable?
    func isIvideonAccountConnected() -> Single<Bool> {
        DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return result.success })
            .catchErrorJustReturn(false)
            .observeOn(MainScheduler())
    }
    
    var disconectIvideonAccountDisposable: Disposable?
    func disconectIvideonAccount() -> Single<DCommandResult> {
        DataManager.shared.hubHelper.setCommandWithResult(.IV_DEL_USER, D: ["domain": DataManager.shared.getUser().domainId])
            .observeOn(MainScheduler())
    }
}

extension XMenuController {
    func addCameraBottomSheetStyle() -> XBottomSheetView.Style {
        return XBottomSheetView.Style(
            backgroundColor: UIColor.white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                ),
                text: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    select: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8)
                )
            )
        )
    }
    
    func disconectIvideonAccountAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                alignment: .center
            ),
            text: nil,
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func errorAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
