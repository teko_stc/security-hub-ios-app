//
//  XAddSectionController.swift
//  SecurityHub
//
//  Created by Timerlan on 30/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift


class XAddSectionController: XBaseFormController {
    private let dm: DataManager
    private let device_id: Int64
    private let complete: (()->Void)?
    private var nameRow: TextRow?
    private var type: DSectionTypeEntity?
    private var sectionInfoBlock: Section?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_id: Int64, _ complete: (()->Void)? = nil) {
        self.dm = DataManager.shared
        self.device_id = device_id
        self.complete = complete
        self.type = HubConst.getSectionsHUBTypes().first
        super.init(nibName: nil, bundle: nil)
        title = "title_add_section".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSectionInfoBlock()
        initCreateButton()
    }
    private func initSectionInfoBlock() {
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "section_add_name".localized()
            $0.placeholder = "section_add_input_name".localized()
        }
        sectionInfoBlock = Section("PARTITION".localized())
        <<< nameRow!
        <<< PushRow<String>() {
            $0.title = "section_add_type".localized()
            $0.options = HubConst.getSectionsHUBTypes().map{ $0.name }
            $0.value = HubConst.getSectionsHUBTypes().map{ $0.name }.first
            $0.onChange { (row) in self.selectType(row) }
        }
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        form +++ sectionInfoBlock!
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "button_add_section".localized(), style: .text)
        createButton?.click = self.create
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
    
    private func selectType(_ row: PushRow<String>) {
        guard let value = row.value, let _type = HubConst.getSectionsHUBTypes().first(where: { $0.name == value }) else { return }
        self.type = _type
    }
    
    private func create() {
        if nameRow?.value == nil || nameRow?.value?.count == 0 { view.endEditing(true); return AlertUtil.errorAlert("add_section_warning_put_name".localized()) }
        createButton?.state = .progress
        for row in sectionInfoBlock?.allRows ?? [] {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
        disposable = dm.addSection(device_id: device_id, name: nameRow?.value, type: type?.id)
            .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        guard let nV = navigationController else { return }
        complete?()
        nV.popViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in sectionInfoBlock?.allRows ?? [] {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
