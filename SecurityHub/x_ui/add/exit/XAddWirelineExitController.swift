//
//  XAddWirelineExitController.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift


class XAddWirelineExitController: XBaseFormController {
    private let dm: DataManager
    private var devices: [DDeviceWithSitesInfo] = []
    
    private var device: DDeviceWithSitesInfo?
    private var type = 4
    private var detector = 15
    
    private var selectedDeviceRow: ListCheckRow<String>?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var nameRow: TextRow?
    private var numberRow: PhoneRow?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_ids: [Int64]) {
        self.dm = DataManager.shared
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        title = "EXIT_SET_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDeviceSelectBlock()
        initExitInfoBlock()
        initCreateButton()
    }
    
    private func initDeviceSelectBlock() {
//        if devices.count == 1 { return device = devices[0] }
        deviceBlock = SelectableSection<ListCheckRow<String>>("DTA_CHOOSE_DEVICE".localized(), selectionType: .singleSelection(enableDeselection: false))
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                $0.disabled = Condition(booleanLiteral: !dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id))
                if _device.offset == 0,
                   HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id),
                   dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id),
                   dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed
                {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in
                    guard _row.value != nil else {
                        return
                    }

                    guard self.dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed else {
                        _row.value = nil
                        return AlertUtil.warAlert("error_device_armed".localized())
                    }
                    
                    self.deviceSelected(_row, _device.element)
                }
            }
        }
        form +++ deviceBlock!
    }
    private func initExitInfoBlock() {
        let typeRow = PushRow<String>() {
            $0.title = "add_wireline_exit_type".localized()
            $0.options = ["wireline_exit_mayak".localized(), "wireline_exit_sirena".localized()]
            $0.value = "wireline_exit_mayak".localized()
            $0.onChange { (row) in self.selectTypeRelay(row) }
        }
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireline_exit_name_title".localized()
            $0.placeholder = "wireline_exit_name_placeholder".localized()
        }
        numberRow = PhoneRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "add_wireline_exit_number".localized()
            $0.placeholder = "add_wireline_exit_put_number".localized()
            let numberRule = RuleClosure<String> { rowValue in
                if let row = rowValue, let value = Int64(row), (value > 0 && value < 7) { return nil }
                return ValidationError(msg: "Field required!")
            }
            $0.add(rule: numberRule)
            $0.validationOptions = .validatesOnChange
        }
        .cellUpdate { cell, row in if !row.isValid { cell.titleLabel?.textColor = .systemRed }  }
        form +++ Section("add_wireline_exit_info".localized())
        <<< nameRow!
        <<< numberRow!
        <<< typeRow
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "add_wireline_exit_add".localized(), style: .text)
        createButton?.click = self.createRelay
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
       
    private func deviceSelected(_ row: ListCheckRow<String>, _ device: DDeviceWithSitesInfo) {
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row.value = nil
            row.cell.isUserInteractionEnabled = false
            row.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert("error_device_not_hub".localized())
        }
        self.device = device
        self.selectedDeviceRow = row
    }
    private func selectTypeRelay(_ row: PushRow<String>) {
        self.type = row.value == "wireline_exit_mayak".localized() ? 4 : 5
        self.detector = row.value == "wireline_exit_mayak".localized() ? 15 : 14
    }
    private func showProgress() {
        createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }

    
    private func createRelay() {
        view.endEditing(true)
        guard let device_id = device?.device.id else { return AlertUtil.errorAlert("add_wireline_exit_warning_select_device".localized()) }
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireline_exit_warning_put_name".localized()) }
        guard numberRow?.isValid == true, let number = Int64(numberRow?.value ?? ".") else { return AlertUtil.errorAlert("add_wireline_exit_warning_put_correct_number".localized()) }
        let uid = String(format: "%02X", 5) + String(format: "%02X", number) + String(format: "%02X", type)
        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: 0, name: nameRow!.value!, detector: detector)
              .observeOn(MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
