//
//  XAddWirelessExitController.swift
//  SecurityHub
//
//  Created by Timerlan on 07/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift

class XAddWirelessExitController: XBaseFormController {
    private let dm: DataManager
    private let nc: NotificationCenter
    private let device_id: Int64
    private var section_id: Int64?
    private let uid: String
    private let zone_type: DZoneTypeEntity
    
    private var nameRow: TextRow?
    private var sectionBlock: SelectableSection<ListCheckRow<String>>?
    private var createZoneButton: XButtonView?
    
    private var disposable: Disposable?
    private var sectionsObserver: Any?

    init(device_id: Int64, section_id: Int64?, uid: String, zone_type: DZoneTypeEntity) {
        self.dm = DataManager.shared
        self.nc = DataManager.shared!.nCenter
        self.device_id = device_id
        self.section_id = section_id
        self.uid = uid
        self.zone_type = zone_type
        super.init(nibName: nil, bundle: nil)
        title = "EXIT_SET_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initZoneInfoBlock()
        initNameBlock()
        initSectionBlock()
        initCreateZoneButton()
    }
    
    override func viewDidDestroy() {
        disposable?.dispose()
        if let _ = sectionsObserver { nc.removeObserver(sectionsObserver!) }
    }
    
    private func initZoneInfoBlock() {
        let images = [zone_type.img]
            form +++ Section(header: zone_type.title, footer: zone_type.subTitle)
            <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images, height: 200, margin: 40); cell.update() }
    }
    private func initNameBlock() {
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireless_zone_name_title".localized()
            $0.placeholder = "wireless_zone_name_placeholder".localized()
        }
        form +++ Section("add_wireless_exit_info".localized())
        <<< nameRow!
    }
    private func initSectionBlock() {
        sectionBlock = SelectableSection<ListCheckRow<String>>(header: "add_wireless_exit_select_section".localized(), footer: "add_wireless_exit_select_section_desc".localized(), selectionType: .singleSelection(enableDeselection: true))
        form +++ sectionBlock!
        reloadSectionBlock()
    }
    private func initCreateZoneButton() {
        createZoneButton = XButtonView(XButtonView.defaultRect, title: "add_wireless_exit_add".localized(), style: .text)
        createZoneButton?.click = self.createZone
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createZoneButton; cell.update() }
    }
    private func reloadSectionBlock() {
        guard let sectionBlock = sectionBlock else { return }
        if sectionBlock.count > 0 { self.section_id = nil; UIView.performWithoutAnimation { sectionBlock.removeAll() } }
        if let _ = sectionsObserver { nc.removeObserver(sectionsObserver!) }
        self.sectionsObserver = nc.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.main) { n in
            guard let nSection = n.object as? NSectionEntity, nSection.type == .insert else { return }
            self.reloadSectionBlock()
        }
        let sections = dm.getDeviceSections(device_id: device_id)
            .filter { $0.key != 0 }
        for _section in sections {
            sectionBlock <<< ListCheckRow<String>(_section.value.name) { row in
                row.cell.tintColor = UIColor.hubMainColor
                row.title = "\(_section.value.name) - \(_section.value.type.name)"
                row.selectableValue = _section.value.name
                if _section.key == section_id { row.value = _section.value.name }
                row.onChange { (_row) in if _row.value != nil { self.selectSection(_row, _section.key) } }
            }
        }
        sectionBlock <<< ButtonRow() {
            $0.cellStyle = .value1; $0.title = "add_wireless_exit_add_section".localized(); $0.cell.tintColor = .hubMainColor
            $0.onCellSelection{ _, _ in self.createSection() }
        }
    }
    private func selectSection(_ row: ListCheckRow<String>, _ section: Int64) {
        self.section_id = section
    }
    
    private func createSection() {
        guard let nV = navigationController else { return }
        let vc = XAddSectionController(device_id: device_id) {  self.reloadSectionBlock() }
        nV.pushViewController(vc, animated: true)
    }
    
    private func showProgress() {
        createZoneButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }
    
    private func createZone() {
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireless_exit_warning_put_name".localized()) }
        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: section_id ?? 0, name: nameRow!.value!)
              .observeOn(MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createZoneButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
