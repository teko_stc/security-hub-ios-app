//
//  XAddMainController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 26.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//
import UIKit
import Gloss
import RxSwift

class XAddMenuController: XBaseViewController<XAddMenuView> {
    enum StartAction {
        case camera
    }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5)),
        lineColor: UIColor.colorFromHex(0xbcbcbc)
    )
    
    private let strings: XAddMenuControllerStrings = XAddMenuControllerStrings()
    private let dataManager: DataManager = DataManager.shared
    private let rtspCameraDataLayer: XRTSPCameraDataLayerProtocol = DataManager.shared.dbHelper
    public var startAction: StartAction?
    
    override var tabBarIsHidden: Bool { true }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.title
        subTitle = strings.navigatonBarDescription
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        defer {
            startAction = nil
        }

        switch startAction {
        case .camera:
            addCameraViewTapped()
        case .none:
            return
        }
    }
  
    private var isIvideonAccountConnectedDisposable: Disposable?
    private func isIvideonAccountConnected() -> Single<Bool> {
        dataManager.hubHelper.get(.IV_GET_TOKEN, D: ["domain": dataManager.getUser().domainId])
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return result.success })
            .catchErrorJustReturn(false)
            .observeOn(MainScheduler())
    }
    
    private var disconectIvideonAccountDisposable: Disposable?
    private func disconectIvideonAccount() -> Single<DCommandResult> {
        dataManager.hubHelper.setCommandWithResult(.IV_DEL_USER, D: ["domain": dataManager.getUser().domainId])
            .observeOn(MainScheduler())
    }
    
    private func addRtspCamera(name: String, url: String) -> Bool {
        let data = XRTSPCameraDataLayerModel(name: name, url: url)
        return rtspCameraDataLayer.add(data: data)
    }
}

extension XAddMenuController: XAddMenuViewDelegate {
    func addCameraViewTapped() {
        isIvideonAccountConnectedDisposable?.dispose()
        isIvideonAccountConnectedDisposable = isIvideonAccountConnected().subscribe(onSuccess: showAddCameraBottomSheet)
    }
    
    func showAddCameraBottomSheet(_ isIvideonAccountConnected: Bool) {
        var items = [XBottomSheetViewItem(title: strings.addCameraBottomSheetRTSPTitle, text: strings.addCameraBottomSheetRTSPText)]
        if XTargetUtils.ivideon != nil {
            items.insert(isIvideonAccountConnected ? XBottomSheetViewItem(title: strings.addCameraBottomSheetIvideonConnectedTitle, text: strings.addCameraBottomSheetIvideonConnectedText) : XBottomSheetViewItem(title: strings.addCameraBottomSheetIvideonDisconnectedTitle, text: strings.addCameraBottomSheetIvideonDisconnectedText), at: 0)
        }
        let bottomSheet = XBottomSheetController(style: addCameraBottomSheetStyle(), title: strings.addCameraBottomSheetTitle, selectedIndex: isIvideonAccountConnected ? 0 : -1, items: items)
        bottomSheet.onSelectedItem = addCameraBottomSheetSelectedItem
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func addCameraBottomSheetSelectedItem(controller: XBottomSheetController, index: Int, item: XBottomSheetViewItem) {
        switch item.text {
        case strings.addCameraBottomSheetRTSPText: return controller.dismiss(animated: true) { self.showAddRtspCameraAlert() }
        case strings.addCameraBottomSheetIvideonConnectedText: controller.dismiss(animated: true) { self.showDisconectIvideonAccountAlert() }
        case strings.addCameraBottomSheetIvideonDisconnectedText: controller.dismiss(animated: true) { self.showIvideonAuthController() }
        default: return controller.dismiss(animated: true)
        }
    }
    
    func showIvideonAuthController() { self.navigationController?.pushViewController(XIvideonAuthController(), animated: true) }
    
    func showDisconectIvideonAccountAlert() {
        let alert = XAlertController(style: disconectIvideonAccountAlertStyle(), strings: strings.disconectIvideonAccountAlert(), positive: disconectIvideonAccountViewTapped)
        navigationController?.present(alert, animated: true)
    }

    func disconectIvideonAccountViewTapped() {
        disconectIvideonAccountDisposable?.dispose()
        disconectIvideonAccountDisposable = disconectIvideonAccount().subscribe(onSuccess: disconectIvideonAccountSucces, onError: showErrorAlert)
    }
    
    func showErrorAlert(error: Error) {
        let alert = XAlertController(style: errorAlertStyle(), strings: strings.errorAlert(message: error.localizedDescription))
        navigationController?.present(alert, animated: true)
    }
    
    func showErrorAlert(message: String) {
        let alert = XAlertController(style: errorAlertStyle(), strings: strings.errorAlert(message: message))
        navigationController?.present(alert, animated: true)
    }
    
    func disconectIvideonAccountSucces(result: DCommandResult) {
        if !result.success { return showErrorAlert(message: result.message) }
        navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: HubNotification.iviAuth, object: nil)
    }
    
    func showAddRtspCameraAlert() {
        let alert = XAddRTSPAlertController()
        alert.onSuccess = addRTSPCameraViewTapped
        navigationController?.present(alert, animated: true)
    }
    
    func addRTSPCameraViewTapped(name: String, url: String) {
        let result = addRtspCamera(name: name, url: url)
        if result {
            navigationController?.popToRootViewController(animated: true)
        } else {
            showErrorAlert(message: strings.errorAddRTSPCamera)
        }
    }
}

extension XAddMenuController {
    func addNewObjectViewTapped() {
        let vc = SiteAddController(site_id: nil)
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func addDelegatedObjectViewTapped() {
        AlertUtil.delegateAlert()
    }

    func addControllerViewTapped() {
        _ = DataManager.shared.getDBSitesSingle()
            .map({ sites in sites.map({ XBottomSheetViewItem(title: $0.name, text: nil, any: $0.id) }) })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] items in
                guard let self = self else {
                    return
                }

                guard items.count > 1 else {
                    let vc = SiteAddController(site_id: items.first != nil ? (items.first!.any as! Int64) : nil)
                    self.navigationController?.pushViewController(vc, animated: false)
                    return
                }
                
                let sheet = XBottomSheetController(style: self.addCameraBottomSheetStyle(), title: "N_WIZARD_ALL_SELECT_SITE".localized(), selectedIndex: -1, items: items)
                sheet.onSelectedItem = { (controller, index, item) in
                    sheet.dismiss(animated: true)
                    
                    let vc = SiteAddController(site_id: item.any as? Int64)
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                self.navigationController?.present(sheet, animated: true)
            })
            .do(onError: { error in debugPrint(error.localizedDescription) })
            .subscribe()
    }
    func addWirelessDeviceViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] device_ids in
            self?.navigationController?.pushViewController(XFindWirelessController(device_ids: device_ids, section_id: nil, type: .zone), animated: true)
        }
    }
    func addWirelessRelayViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] device_ids in
            self?.navigationController?.pushViewController(XFindWirelessController(device_ids: device_ids, section_id: nil, type: .relay), animated: true)
        }
    }

    func addPartitionViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
            switch deviceIds.count {
            case 0:
                self?.showErrorAlert(message: "N_WA_NO_DEVICES_TO_SET_ELEMENTS".localized())
            case 1:
                guard DataManager.shared.dbHelper.getArmStatus(device_id: deviceIds[0]) == .disarmed else {
                    return self?.showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) ?? ()
                }

                let vc = XAddSectionController(device_id: deviceIds[0])
                self?.navigationController?.pushViewController(vc, animated: false)
            default:
                let void: ((_ sec: Sections) -> Void) = { sec in
                    guard DataManager.shared.dbHelper.getArmStatus(device_id: sec.device) == .disarmed else {
                        return self?.showErrorColtroller(message: "N_SECTION_NEED_DISARM".localized()) ?? ()
                    }

                    let vc = XAddSectionController(device_id: sec.device)
                    self?.navigationController?.pushViewController(vc, animated: false)
                }
                let controller = SectionSelectController(site: nil, device_ids: deviceIds, title: "DTA_CHOOSE_DEVICE".localized(), isDevice: true, select: void)
                self?.navigationController?.pushViewController(controller, animated: false)
            }
        }
    }
    func addZoneViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
            self?.navigationController?.pushViewController(XAddWirelineZoneController(device_ids: deviceIds, section_id: nil), animated: true)
        }
    }
    func addOutputViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
            self?.navigationController?.pushViewController(XAddWirelineExitController(device_ids: deviceIds), animated: true)
        }
    }
    func addHardwiredRelayViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
            self?.navigationController?.pushViewController(XAddWirelineRelayController(device_ids: deviceIds), animated: true)
        }
    }
    func addScriptViewTapped() {
        DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
            let vc = XSelectScriptTypeController(device_ids: deviceIds)
            self?.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func addDomainUserViewTapped() {
        guard let nV = navigationController else { return }
        nV.pushViewController(AddOperatorController(), animated: false)
    }
    func addControllerLocalUserViewTapped() {
        let sheet = XBottomSheetController(style: addCameraBottomSheetStyle(), title: nil, selectedIndex: -1, items: [
                XBottomSheetViewItem(title: "WIZ_USER_SET_CODE".localized(), text: nil, any: 0),
                XBottomSheetViewItem(title: "WIZ_USER_SET_HW".localized(), text: "\n", any: 1)
            ]
        )
        sheet.onSelectedItem = { (controller, index, item) in
            DataManager.shared.getOnlyHubDBDeviceIds { [weak self] deviceIds in
                let controller = index == 0 ? XAddWirelineUserController(device_ids: deviceIds) : XFindWirelessController(device_ids: deviceIds, section_id: nil, type: .user)
                self?.navigationController?.pushViewController(controller, animated: true)
            }
            sheet.dismiss(animated: true)
        }
        navigationController?.present(sheet, animated: true)
    }
}
