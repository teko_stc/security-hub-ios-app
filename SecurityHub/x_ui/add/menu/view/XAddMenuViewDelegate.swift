//
//  XAddMenuViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

@objc protocol XAddMenuViewDelegate {
    @objc optional func addNewObjectViewTapped()
    @objc optional func addDelegatedObjectViewTapped()
    
    @objc optional func addControllerViewTapped()
    @objc optional func addWirelessDeviceViewTapped()
    @objc optional func addWirelessRelayViewTapped()
    @objc optional func addCameraViewTapped()

    @objc optional func addPartitionViewTapped()
    @objc optional func addZoneViewTapped()
    @objc optional func addOutputViewTapped()
    @objc optional func addHardwiredRelayViewTapped()
    @objc optional func addScriptViewTapped()

    @objc optional func addDomainUserViewTapped()
    @objc optional func addControllerLocalUserViewTapped()
}
