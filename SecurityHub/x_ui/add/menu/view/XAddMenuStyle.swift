//
//  XAddMenuStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit

struct XAddMenuViewStyle {
    let backgroundColor: UIColor
    let cellHeader: XSelectorView.Header.Style
    let cell: XSelectorView.Cell.Style
}
extension XAddMenuView {
    typealias Style = XAddMenuViewStyle
}
