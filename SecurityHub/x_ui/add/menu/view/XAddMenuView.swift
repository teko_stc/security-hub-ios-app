//
//  XAddMenuController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit


class XAddMenuView: UIView {
    public var delegate: XAddMenuViewDelegate? {
        didSet {
            initSelectorViewContent()
        }
    }
    
    private var selectorView: XSelectorView!
    private let strings: XAddMenuViewStrings = XAddMenuViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(style: Style) {
        backgroundColor = style.backgroundColor
        
        selectorView = XSelectorView(headerStyle: style.cellHeader, cellStyle: style.cell)
        selectorView.backgroundColor = style.backgroundColor
        addSubview(selectorView)
    }
    
    private func initSelectorViewContent() {
        let items = [
            XSelectorView.Section(
                title: strings.section_1_title,
                items: [
                    XSelectorView.Section.Item(title: strings.section_1_item_1, imageName: "ic_new_object", click: delegate?.addNewObjectViewTapped),
                    XSelectorView.Section.Item(title: strings.section_1_item_2, imageName: "ic_deligate",   click: delegate?.addDelegatedObjectViewTapped),
                ]
            ),
            XSelectorView.Section(
                title: strings.section_2_title,
                items: [
                    XSelectorView.Section.Item(title: strings.section_2_item_1, imageName: "ic_sh_1111",            click: delegate?.addControllerViewTapped),
                    XSelectorView.Section.Item(title: strings.section_2_item_2, imageName: "ic_ustroistvo",         click: delegate?.addWirelessDeviceViewTapped),
                    XSelectorView.Section.Item(title: strings.section_2_item_3, imageName: "ic_astra_8731_small",   click: delegate?.addWirelessRelayViewTapped),
                    XSelectorView.Section.Item(title: strings.section_2_item_4, imageName: "ic_camera_border",      click: delegate?.addCameraViewTapped),
                ]
            ),
            XSelectorView.Section(
                title: strings.section_3_title,
                items: [
                    XSelectorView.Section.Item(title: strings.section_3_item_1, imageName: "ic_razdel",             click: delegate?.addPartitionViewTapped),
                    XSelectorView.Section.Item(title: strings.section_3_item_2, imageName: "ic_shleif",             click: delegate?.addZoneViewTapped),
                    XSelectorView.Section.Item(title: strings.section_3_item_3, imageName: "ic_vyhod",              click: delegate?.addOutputViewTapped),
                    XSelectorView.Section.Item(title: strings.section_3_item_4, imageName: "ic_rele_1",             click: delegate?.addHardwiredRelayViewTapped),
                    XSelectorView.Section.Item(title: strings.section_3_item_5, imageName: "ic_scenary_privyazan",  click: delegate?.addScriptViewTapped),
                ]
            ),
            XSelectorView.Section(
                title: strings.section_4_title,
                items: [
                    XSelectorView.Section.Item(title: strings.section_4_item_1, imageName: "ic_domenuy",    click: delegate?.addDomainUserViewTapped),
                    XSelectorView.Section.Item(title: strings.section_4_item_2, imageName: "ic_hoz_organ",  click: delegate?.addControllerLocalUserViewTapped),
                ]
            ),
        ]
        selectorView.reloadData(items)
    }
    
    private func setConstraints() {
        selectorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            selectorView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 14),
            selectorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            selectorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            selectorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
