//
//  XAddMenuViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAddMenuView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            cellHeader: XSelectorView.Header.Style(
                backgroundColor: UIColor.white,
                title: XSelectorView.Header.Style.Title(
                    font: UIFont(name: "Open Sans", size: 15.5),
                    color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                    margins: UIEdgeInsets(top: 20, left: 24, bottom: 6, right: 24)
                )
            ),
            cell: XSelectorView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
                title: XSelectorViewCell.Style.Label(
                    font: UIFont(name: "Open Sans", size: 20),
                    color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                    margins: UIEdgeInsets(top: 0, left: 12, bottom: 12, right: 24)
                ),
                icon: XSelectorViewCell.Style.Icon(
                    color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                    size: CGSize(width: 36, height: 36),
                    margins: UIEdgeInsets(top: 12, left: 24, bottom: 12, right: 6)
                )
            )
        )
    }
}

extension XAddMenuController {
    func addCameraBottomSheetStyle() -> XBottomSheetView.Style {
        return XBottomSheetView.Style(
            backgroundColor: UIColor.white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                ),
                text: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    select: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8)
                )
            )
        )
    }
    
    func disconectIvideonAccountAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                alignment: .center
            ),
            text: nil,
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func errorAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
