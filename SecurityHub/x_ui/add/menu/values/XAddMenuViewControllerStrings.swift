//
//  XAddMenuViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XAddMenuViewStrings {
    /// Объекты
    var section_1_title: String { "N_TTS_OBJECTS".localized() }
    
    /// Новый
    var section_1_item_1: String { "N_WIZARD_ALL_SITE_NEW".localized() }
    
    /// Делегированный
    var section_1_item_2: String { "N_WIZARD_ALL_SITE_DELEGATED".localized() }
    
    /// Устройства
    var section_2_title: String { "N_WIZARD_ALL_DEVICES".localized() }
    
    /// Контроллер
    var section_2_item_1: String { "N_WIZARD_ALL_CONTROLLER".localized() }
    
    /// РК устройство
    var section_2_item_2: String { "N_WIZARD_ALL_RK_DEVICE".localized() }
    
    /// РК реле
    var section_2_item_3: String { "N_WIZARD_ALL_RK_RELAY".localized() }
    
    /// Камера
    var section_2_item_4: String { "N_WIZARD_ALL_CAMERA".localized() }
    
    /// Контроллер
    var section_3_title: String { "N_MAIN_TITLE_CONTROLLER".localized() }
    
    /// Раздел
    var section_3_item_1: String { "N_MAIN_SUBTITLE_SECTION".localized() }
    
    /// Шлейф
    var section_3_item_2: String { "N_WIZARD_ALL_INPUT".localized() }
    
    /// Выход
    var section_3_item_3: String { "N_WIZARD_ALL_OUTPUT".localized() }
    
    /// Проводное реле
    var section_3_item_4: String { "N_WIZARD_ALL_WIRED_RELAY".localized() }
    
    /// Сценарий
    var section_3_item_5: String { "N_WIZARD_ALL_SCRIPT".localized() }
    
    /// Пользователь
    var section_4_title: String { "N_WIZARD_ALL_USER".localized() }
    
    /// Доменный
    var section_4_item_1: String { "N_WIZARD_ALL_DOMAIN_USER".localized() }
    
    /// Локальный пользователь контроллера
    var section_4_item_2: String { "N_WIZARD_ALL_LOCAL_USER".localized() }
}

class XAddMenuControllerStrings {
    
    /// Настройка
    var title: String { "N_WIZARD_ALL_TITLE".localized() }
    
    /// Выберите новый элемент системы, который вы хотите настроить
    var navigatonBarDescription: String { "N_WIZARD_ALL_SUBTITLE".localized() }
    
    /// Выберите тип камеры
    var addCameraBottomSheetTitle: String { "N_CAMERA_TYPE_SELECT".localized() }
    
    /// Ivideon
    var addCameraBottomSheetIvideonConnectedTitle: String { "Ivideon" }
    
    /// Ваш аккаунт iVideon уже подключен к учетной записи Security Hub. Информация со всех камер из аккаунта iVideon будет загружаться автоматически. Нажмите, чтобы подключить другой аккаунт iVideon
    var addCameraBottomSheetIvideonConnectedText: String { "N_WIZ_ALL_IV_ALREADY_BINDED".localized() }
    
    /// Ivideon
    var addCameraBottomSheetIvideonDisconnectedTitle: String { "Ivideon" }
    
    /// Нажмите, чтобы привязать аккаунт iVideon к вашей учетной записи Security Hub
    var addCameraBottomSheetIvideonDisconnectedText: String { "N_WIZ_ALL_IV_BIND_PRESS".localized() }
    
    /// IP-камера
    var addCameraBottomSheetRTSPTitle: String { "N_WIZ_ALL_IP_CAM".localized() }
    
    /// Нажмите, для подключения камеры с поддержкой протокола RTSP
    var addCameraBottomSheetRTSPText: String { "N_WIZ_ALL_RTSP_SET_PRESS".localized() }
    
    /// Некорректная ссылка
    var errorAddRTSPCamera: String { "N_WIZ_ALL_RTSP_LINK_ERROR".localized() }
    
    func disconectIvideonAccountAlert() -> XAlertView.Strings {
        return XAlertView.Strings(
            /// Хотите отключить аккаунт iVideon от учетной записи Security Hub?
            title: "N_PROFILE_IV_UNBIND_Q".localized(),
            text: nil,
            /// Да
            positive: "YES".localized(),
            /// Нет
            negative: "NO".localized()
        )
    }
    
    func errorAlert(message: String) -> XAlertView.Strings {
        return XAlertView.Strings(title: message, text: nil, positive: "OK".localized(), negative: nil)
    }
}
