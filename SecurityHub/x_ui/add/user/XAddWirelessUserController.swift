//
//  XAddWirelessUserController.swift
//  SecurityHub
//
//  Created by Timerlan on 28/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift
class XAddWirelessUserController: XBaseFormController {
    private let dm: DataManager
    private let device_id: Int64
    private let uid: String
    private let zone_type: DZoneTypeEntity?
    private var section_ids: [Int64] = []
    private var nameRow: TextRow?
    private var sectionBlock: SelectableSection<ListCheckRow<String>>?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_id: Int64, uid: String, zone_type: DZoneTypeEntity?) {
        self.dm = DataManager.shared
        self.device_id = device_id
        self.uid = uid
        self.zone_type = zone_type
        super.init(nibName: nil, bundle: nil)
        title = "USER_SET_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
         super.viewDidLoad()
         initInfoBlock()
         initNameBlock()
         initSectionBlock()
         initCreateButton()
     }
     
     private func initInfoBlock() {
         let images = [zone_type?.img ?? "hogorgan"]
         form +++ Section(header: zone_type?.title ?? "WIZ_USER_SET_HW".localized(), footer: zone_type?.subTitle ?? "")
             <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images, height: 200, margin: 40); cell.update() }
     }
     private func initNameBlock() {
         nameRow = TextRow() {
             $0.cell.tintColor = .hubMainColor
             $0.title = "wireless_user_name_title".localized()
             $0.placeholder = "wireless_user_name_placeholder".localized()
         }
         form +++ Section("add_wireless_user_info".localized())
         <<< nameRow!
     }
     private func initSectionBlock() {
         sectionBlock = SelectableSection<ListCheckRow<String>>(header: "add_wireless_user_section_select_section".localized(), footer: "add_wireless_user_select_section_desc".localized(), selectionType: .multipleSelection)
        let sections = dm.getDeviceSections(device_id: device_id).filter { $0.key != 0 && $0.value.type.id == HubConst.SECTION_TYPE_SECURITY }
        for _section in sections {
            self.section_ids.append(_section.key)
            sectionBlock! <<< ListCheckRow<String>(_section.value.name) { row in
                row.cell.tintColor = UIColor.hubMainColor
                row.title = _section.value.name
                row.selectableValue = _section.value.name
                row.value = _section.value.name
                row.onChange { (_row) in self.selectSection(_row, _section.key) }
            }
        }
         form +++ sectionBlock!
     }
     private func initCreateButton() {
         createButton = XButtonView(XButtonView.defaultRect, title: "add_wireless_user_add".localized(), style: .text)
         createButton?.click = self.create
         form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
     }
    
    private func selectSection(_ row: ListCheckRow<String>, _ section: Int64) {
        if row.value == nil { return self.section_ids.removeAll{ $0 == section } }
        self.section_ids.append(section)
    }
    
    private func showProgress() {
    createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }
    
    private func create() {
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireless_user_warning_put_name".localized()) }
        showProgress()
        disposable = dm.addHozOrgan(device_id: device_id, uid: uid, sections: self.section_ids, name: nameRow!.value!)
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
