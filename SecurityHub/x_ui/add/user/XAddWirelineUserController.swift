//
//  XAddWirelineUserController.swift
//  SecurityHub
//
//  Created by Timerlan on 29/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift
class XAddWirelineUserController: XBaseFormController {
    private let dm: DataManager
    private var devices: [DDeviceWithSitesInfo] = []
    private var device: DDeviceWithSitesInfo?
    private var selectedDeviceRow: ListCheckRow<String>?
    private var section_ids: [Int64] = []
    private var nameRow: TextRow?
    private var numberRow: PhoneRow?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var sectionBlock: SelectableSection<ListCheckRow<String>>?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_ids: [Int64]) {
        self.dm = DataManager.shared
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        title = "USER_SET_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
         super.viewDidLoad()
         initDeviceSelectBlock()
         initNameBlock()
         initSectionBlock()
         initCreateButton()
     }

    
    private func initDeviceSelectBlock() {
//        if devices.count == 1 { return device = devices[0] }
        deviceBlock = SelectableSection<ListCheckRow<String>>("DTA_CHOOSE_DEVICE".localized(), selectionType: .singleSelection(enableDeselection: false))
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                $0.disabled = Condition(booleanLiteral: !dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id))
                if _device.offset == 0,
                   HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id),
                   dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id),
                   dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed
                {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in
                    guard _row.value != nil else {
                        return
                    }

                    guard self.dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed else {
                        _row.value = nil
                        return AlertUtil.warAlert("error_device_armed".localized())
                    }
                    
                    self.deviceSelected(_row, _device.element)
                }
            }
        }
        form +++ deviceBlock!
    }
    private func initNameBlock() {
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireline_user_name_title".localized()
            $0.placeholder = "wireline_user_name_placeholder".localized()
        }
        numberRow = PhoneRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "add_wireline_user_number".localized()
            $0.placeholder = "add_wireline_user_put_number".localized()
        }
        form +++ Section("add_wireline_user_info".localized())
            <<< nameRow!
            <<< numberRow!
     }
    private func initSectionBlock() {
        sectionBlock = SelectableSection<ListCheckRow<String>>(header: "add_wireline_user_section_select_section".localized(), footer: "add_wireline_user_select_section_desc".localized(), selectionType: .multipleSelection)
        form +++ sectionBlock!
        reloadSectionBlock()
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "add_wireline_user_add".localized(), style: .text)
        createButton?.click = self.create
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
    
    private func reloadSectionBlock() {
        guard let sectionBlock = sectionBlock, let device_id = device?.device.id else { return }
        if sectionBlock.count > 0 { self.section_ids = []; UIView.performWithoutAnimation { sectionBlock.removeAll(); } }
        let sections = dm.getDeviceSections(device_id: device_id).filter { $0.key != 0 && $0.value.type.id == HubConst.SECTION_TYPE_SECURITY }
        for _section in sections {
            self.section_ids.append(_section.key)
            sectionBlock <<< ListCheckRow<String>(_section.value.name) { row in
                row.cell.tintColor = UIColor.hubMainColor
                row.title = _section.value.name
                row.selectableValue = _section.value.name
                row.value = _section.value.name
                row.onChange { (_row) in self.selectSection(_row, _section.key) }
            }
        }
    }
    
    private func selectSection(_ row: ListCheckRow<String>, _ section: Int64) {
        if row.value == nil { return self.section_ids.removeAll{ $0 == section } }
        self.section_ids.append(section)
    }
    private func deviceSelected(_ row: ListCheckRow<String>, _ device: DDeviceWithSitesInfo) {
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row.value = nil
            row.cell.isUserInteractionEnabled = false
            row.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert("error_device_not_hub".localized())
        }
        self.device = device
        self.selectedDeviceRow = row
        reloadSectionBlock()
    }
    
    private func showProgress() {
    createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }
    
    private func create() {
        guard let device = self.device else { return AlertUtil.warAlert("warning_select_device".localized()) }
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireline_user_warning_put_name".localized()) }
        if numberRow?.value == nil || numberRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireline_user_warning_put_number".localized()) }
        showProgress()
        disposable = dm.addHozOrgan(device_id: device.device.id, password: numberRow!.value!, sections: self.section_ids, name: nameRow!.value!)
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
