//
//  XAddWirelessZoneController.swift
//  SecurityHub
//
//  Created by Timerlan on 25/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift

class XAddWirelessZoneController: XBaseFormController {
    private let dm: DataManager
    private let nc: NotificationCenter
    private let device_id: Int64
    private var section_id: Int64?
    private var sections: [Int64: DSectionNameAndTypeEntity]
    private let zone_type: DZoneTypeEntity

    private let uid: String
    private var detector_type: DDetectorTypeEntity?
    private var alarm_type: DAlarmTypeEntity?
    private var delay = false
    
    private var nameRow: TextRow?
    private var sectionBlock: SelectableSection<ListCheckRow<String>>?
    private var delayBlock: Section?
    private var createZoneButton: XButtonView?
    
    private var disposable: Disposable?
    private var sectionsObserver: Any?
    
    init(device_id: Int64, section_id: Int64?, uid: String, zone_type: DZoneTypeEntity) {
        self.dm = DataManager.shared
        self.nc = DataManager.shared!.nCenter
        self.device_id = device_id
        self.section_id = section_id
        self.sections = dm.getDeviceSections(device_id: device_id)
        self.uid = uid
        self.zone_type = zone_type
        self.detector_type = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })
        self.alarm_type = self.detector_type?.alarmTypes.first(where: { (at) -> Bool in return at.isDefaultAlarm })
        super.init(nibName: nil, bundle: nil)
        if zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() }).count < 2 && self.detector_type?.getSectionMask() == 0 { self.section_id = 0 }
        title = "WIZ_DEVICE_ADD_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initZoneInfoBlock()
        initNameBlock()
        initAlarmTypeBlock()
        initSectionBlock()
        initDelayBlock()
        initCreateZoneButton()
    }
    
    override func viewDidDestroy() {
        disposable?.dispose()
        if let _ = sectionsObserver { nc.removeObserver(sectionsObserver!) }
    }
    
    private func initZoneInfoBlock() {
        let images = [zone_type.img]
            form +++ Section(header: zone_type.title, footer: zone_type.subTitle)
            <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images, height: 200, margin: 40); cell.update() }
    }
    private func initNameBlock() {
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireless_zone_name_title".localized()
            $0.placeholder = "wireless_zone_name_placeholder".localized()
        }
        form +++ Section("add_wireless_zone_info".localized())
        <<< nameRow!
    }
    private func initAlarmTypeBlock() {
        if  zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() }).count < 2 { return }
        let autoBlock = Section(header: "add_wireless_zone_destination_title".localized(), footer: "add_wireless_zone_destination_desc".localized())
        let pushRow = PushRow<String>() {
            $0.title = "add_wireless_zone_destination".localized()
            $0.options = zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() })
            $0.value = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })?.defaultAlarmName()
            $0.onChange { (row) in self.selectAlarmType(row) }
        }
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        form +++ autoBlock
        <<< SwitchRow() { $0.title = "add_wireless_zone_destination_auto_switch".localized(); $0.value = true; $0.cell.switchControl.onTintColor = .hubMainColor }
        .onChange { row in UIView.performWithoutAnimation{ if (row.value ?? false) {autoBlock.remove(at: 1)} else {autoBlock <<< pushRow} } }
    }
    private func initSectionBlock() {
        if  zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() }).count < 2 && self.detector_type?.getSectionMask() == 0 { return self.section_id = 0 }
        sectionBlock = SelectableSection<ListCheckRow<String>>(header: "add_wireless_zone_select_section".localized(), footer: "add_wireless_zone_select_section_desc".localized(), selectionType: .singleSelection(enableDeselection: false))
        form +++ sectionBlock!
        reloadSectionBlock()
    }
    private func initCreateZoneButton() {
        createZoneButton = XButtonView(XButtonView.defaultRect, title: "add_wireless_zone_add".localized(), style: .text)
        createZoneButton?.click = self.createZone
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createZoneButton; cell.update() }
    }
    private func reloadSectionBlock() {
        guard let sectionBlock = sectionBlock else { return }
        if sectionBlock.count > 0 { self.section_id = nil; UIView.performWithoutAnimation { sectionBlock.removeAll(); delayBlock?.removeAll() } }
        if let _ = self.sectionsObserver { nc.removeObserver(self.sectionsObserver!) }
        self.sectionsObserver = nc.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.main) { n in
            guard let nSection = n.object as? NSectionEntity, nSection.type == .insert else { return }
            self.reloadSectionBlock()
        }
        self.sections = dm.getDeviceSections(device_id: device_id)
        let sections = self.sections
            .filter { $0.key != 0 }
            .sorted(by: { (o1, o2) -> Bool in
                let id = Int64(detector_type?.getSectionMask() ?? 31)
                let r1: Int64 = id >> (o1.value.type.id - 1) & 1
                let r2: Int64 = id >> (o2.value.type.id - 1) & 1
                return r1 > r2
            })
        for _section in sections {
            sectionBlock <<< ListCheckRow<String>("\(_section.value.name)\(_section.key)") { row in
                row.cell.tintColor = UIColor.hubMainColor
                row.title = "\(_section.value.name) - \(_section.value.type.name)"
                row.selectableValue = _section.value.name
                if _section.key == section_id { row.value = _section.value.name }
                if (detector_type?.getSectionMask() ?? 31) >> (_section.value.type.id - 1) & 1 == 0 {
                    row.cell.isUserInteractionEnabled = false
                    row.cell.contentView.alpha = 0.5
                }
                row.onChange { (_row) in if _row.value != nil { self.selectSection(_section.key) } }
            }
        }
        sectionBlock <<< ButtonRow() {
            $0.cellStyle = .value1; $0.title = "add_wireless_zone_add_section".localized(); $0.cell.tintColor = .hubMainColor
            $0.onCellSelection{ _, _ in self.createSection() }
        }
    }
    private func initDelayBlock() {
        delayBlock = Section()
        form +++ delayBlock!
        if let section_id = self.section_id { selectSection(section_id) }
    }
        
    private func selectAlarmType(_ row: PushRow<String>) {
        guard  let value = row.value, let end = value.range(of: "\n")?.upperBound, let start = value.range(of: "\n")?.lowerBound,
               let dt = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.title == String(value.suffix(from: end)) }),
               let at = dt.alarmTypes.first(where: { (at) -> Bool in return at.title == String(value.prefix(upTo: start)) }) else { return }
        if alarm_type?.id == at.id && detector_type?.id == dt.id { return }
        detector_type = dt
        alarm_type = at
        reloadSectionBlock()
    }
    private func selectSection(_ section: Int64) {
        self.section_id = section
        guard let delayBlock = self.delayBlock, let type = self.sections.first(where: { $0.key == section })?.value.type else { return }
        if type.id == HubConst.SECTION_TYPE_SECURITY, delayBlock.count == 0 {
            let sw = SwitchRow() { $0.title = "zone_delay_on".localized(); $0.cell.switchControl.onTintColor = .hubMainColor }
            .onChange { row in self.delay =  (row.value ?? false) }
            UIView.performWithoutAnimation { delayBlock <<< sw }
        } else if type.id != HubConst.SECTION_TYPE_SECURITY {
            UIView.performWithoutAnimation { delayBlock.removeAll() }
        }
    }
    
    private func createSection() {
        guard let nV = navigationController else { return }
        let vc = XAddSectionController(device_id: device_id) {  self.reloadSectionBlock() }
        nV.pushViewController(vc, animated: true)
    }
    
    private func showProgress() {
        createZoneButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }
    
    private func createZone() {
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireless_zone_warning_put_name".localized()) }
        let detectorHEX = String(format: "%02X", detector_type?.id ?? 0) + String(format: "%02X", alarm_type?.id ?? 0)
//        if !DataManager.settingsHelper.needExpert && section_id == nil { return createSectionAndZone(name: nameRow!.value!, detector: detectorHEX.hexaToInt) }
        guard let section_id = self.section_id else { return AlertUtil.errorAlert("add_wireless_zone_warning_select_section".localized()) }
        showProgress()
        let detector = detectorHEX.hexaToInt == 0 ? nil : detectorHEX.hexaToInt
        disposable = dm.addZone(device_id: device_id, uid: uid, section: section_id, name: nameRow!.value!, delay: self.delay ? 1 : 0, detector: detector)
              .observeOn(MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func createSectionAndZone(name: String, detector: Int) {
        let detector = detector == 0 ? nil : detector
        guard let type = detector_type?.sectionDefault, type > 0 else { return AlertUtil.errorAlert("add_wireless_zone_warning_no_default_section".localized()) }
        if let section_id = dm.getSections(site_id: nil, device_id: device_id).first(where: { (s) -> Bool in return s.detector == type})?.section {
            self.section_id = section_id
            return createZone()
        }
        guard let index = DataManager.shared.getEmptySectionIndex(device_id: device_id) else { return AlertUtil.errorAlert("add_wireless_zone_warning_no_place_for_section".localized()) }
        showProgress()
        disposable = dm.addSection(device_id: device_id, name: nil, index: index, type: Int64(type)).asObservable()
            .concatMap({ (result) -> Single<DCommandResult> in
                if result.success { return self.dm.addZone(device_id: self.device_id, uid: self.uid, section: index, name: name, delay: self.delay ? 1 : 0, detector: detector) }
                return Single.just(result)
            })
            .observeOn(MainScheduler.init())
            .subscribe(onNext: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createZoneButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
