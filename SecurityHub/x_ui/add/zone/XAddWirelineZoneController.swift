//
//  XAddWirelineZoneController.swift
//  SecurityHub
//
//  Created by Timerlan on 31/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//


import UIKit
import Eureka
import RxSwift

class XAddWirelineZoneController: XBaseFormController {
    private let dm: DataManager
    private let nc: NotificationCenter
    private var section_id: Int64?
    private var sections: [Int64: DSectionNameAndTypeEntity]?
    private var devices: [DDeviceWithSitesInfo] = []
    
    private var device: DDeviceWithSitesInfo?
    private var zone_type: DInputTypeEntity
    private var detector_type: DDetectorTypeEntity?
    private var alarm_type: DAlarmTypeEntity?
    private var delay = false
    
    private var selectedDeviceRow: ListCheckRow<String>?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var sectionBlock: SelectableSection<ListCheckRow<String>>?
    private var nameRow: TextRow?
    private var numberRow: PhoneRow?
    private var autoBlock: Section?
    private var destionationRow: PushRow<String>?
    private var delayBlock: Section?
    private var createButton: XButtonView?
    
    private var disposable: Disposable?
    private var sectionsObserver: Any?
    
    init(device_ids: [Int64], section_id: Int64?) {
        self.dm = DataManager.shared
        self.nc = DataManager.shared!.nCenter
        self.section_id = section_id
        self.zone_type = R.inputTypes.first!
        self.detector_type = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })
        self.alarm_type = detector_type?.alarmTypes.first(where: { (at) -> Bool in return at.isDefaultAlarm })
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        title = "WIZ_DEVICE_ADD_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDeviceSelectBlock()
        initInfoBlock()
        initAlarmTypeBlock()
        initSectionBlock()
        initDelayBlock()
        initCreateZoneButton()
    }
    
    override func viewDidDestroy() {
        disposable?.dispose()
        if let _ = sectionsObserver { nc.removeObserver(sectionsObserver!) }
    }
    
    private func initDeviceSelectBlock() {
//        if devices.count == 1 { return device = devices[0] }
        deviceBlock = SelectableSection<ListCheckRow<String>>("DTA_CHOOSE_DEVICE".localized(), selectionType: .singleSelection(enableDeselection: false))
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                $0.disabled = Condition(booleanLiteral: !dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id))
                if _device.offset == 0,
                   HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id),
                   dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id),
                   dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed
                {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in
                    guard _row.value != nil else {
                        return
                    }

                    guard self.dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed else {
                        _row.value = nil
                        return AlertUtil.warAlert("error_device_armed".localized())
                    }
                    
                    self.deviceSelected(_row, _device.element)
                }
            }
        }
        form +++ deviceBlock!
    }
    private func initInfoBlock() {
        let typeRow = PushRow<String>() {
            $0.title = "add_wireline_zone_type".localized()
            $0.options = R.inputTypes.compactMap { $0.title }
            $0.value = R.inputTypes.compactMap { $0.title }.first
            $0.onChange { (row) in self.selectTypeZone(row) }
        }
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireless_zone_name_title".localized()
            $0.placeholder = "wireless_zone_name_placeholder".localized()
        }
        numberRow = PhoneRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "add_wireline_zone_number".localized()
            $0.placeholder = "add_wireline_zone_put_number".localized()
            let numberRule = RuleClosure<String> { rowValue in
                if let row = rowValue, let value = Int64(row), (value > 0 && value < 5) { return nil }
                return ValidationError(msg: "Field required!")
            }
            $0.add(rule: numberRule)
            $0.validationOptions = .validatesOnChange
        }
        .cellUpdate { cell, row in if !row.isValid { cell.titleLabel?.textColor = .systemRed }  }
        form +++ Section("add_wireline_zone_info".localized())
        <<< nameRow!
        <<< numberRow!
        <<< typeRow
     }
    private func initAlarmTypeBlock() {
        autoBlock = Section(header: "add_wireline_zone_destination_title".localized(), footer: "add_wireline_zone_destination_desc".localized())
        destionationRow = PushRow<String>() {
            $0.title = "add_wireline_zone_destination".localized()
            $0.options = zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() })
            $0.value = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })?.defaultAlarmName()
            $0.onChange { (row) in self.selectAlarmType(row) }
        }
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        form +++ autoBlock!
        <<< SwitchRow() { $0.title = "add_wireline_zone_destination_auto_switch".localized(); $0.value = true; $0.cell.switchControl.onTintColor = .hubMainColor }
            .onChange { row in UIView.performWithoutAnimation{ if (row.value ?? false) {self.autoBlock!.remove(at: 1)} else {self.autoBlock! <<< self.destionationRow!} } }
    }
    private func initSectionBlock() {
        sectionBlock = SelectableSection<ListCheckRow<String>>(header: "add_wireline_zone_select_section".localized(), footer: "add_wireline_zone_select_section_desc".localized(), selectionType: .singleSelection(enableDeselection: false))
        form +++ sectionBlock!
        reloadSectionBlock()
    }
    private func reloadSectionBlock() {
        guard let sectionBlock = sectionBlock, let device_id = device?.device.id else { return }
        if sectionBlock.count > 0 { self.section_id = nil; UIView.performWithoutAnimation { sectionBlock.removeAll(); delayBlock?.removeAll() } }
        if let _ = sectionsObserver { nc.removeObserver(sectionsObserver!) }
        self.sectionsObserver = nc.addObserver(forName: HubNotification.sectionsUpdate, object: nil, queue: OperationQueue.main) { n in
            guard let nSection = n.object as? NSectionEntity, nSection.type == .insert else { return }
            self.reloadSectionBlock()
        }
        self.sections = dm.getDeviceSections(device_id: device_id)
        let sections = self.sections!
            .filter { $0.key != 0 }
            .sorted(by: { (o1, o2) -> Bool in
                let id = Int64(detector_type?.getSectionMask() ?? 31)
                let r1: Int64 = id >> (o1.value.type.id - 1) & 1
                let r2: Int64 = id >> (o2.value.type.id - 1) & 1
                return r1 > r2
            })
        for _section in sections {
            sectionBlock <<< ListCheckRow<String>(_section.value.name) { row in
                row.cell.tintColor = UIColor.hubMainColor
                row.title = "\(_section.value.name) - \(_section.value.type.name)"
                row.selectableValue = _section.value.name
                if _section.key == section_id { row.value = _section.value.name }
                if (detector_type?.getSectionMask() ?? 31) >> (_section.value.type.id - 1) & 1 == 0 {
                    row.cell.isUserInteractionEnabled = false
                    row.cell.contentView.alpha = 0.5
                }
                row.onChange { (_row) in if _row.value != nil { self.selectSection(_section.key) } }
            }
        }
        sectionBlock <<< ButtonRow() {
            $0.cellStyle = .value1; $0.title = "add_wireline_zone_add_section".localized(); $0.cell.tintColor = .hubMainColor
            $0.onCellSelection{ _, _ in self.createSection() }
        }
    }
    private func initDelayBlock() {
        delayBlock = Section()
        form +++ delayBlock!
        if let section_id = self.section_id { selectSection(section_id) }
        
        guard let delayBlock = self.delayBlock else { return }
        if zone_type.id != 4 {
            let sw = SwitchRow() { $0.title = "LOCAL_CONFIG_EXIT_DELAY".localized(); $0.cell.switchControl.onTintColor = .hubMainColor } .onChange { row in self.delay = (row.value ?? false) }
            UIView.performWithoutAnimation { delayBlock <<< sw }
        } else {
            UIView.performWithoutAnimation { delayBlock.removeAll() }
        }
    }
    private func initCreateZoneButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "add_wireline_zone_add".localized(), style: .text)
        createButton?.click = self.createZone
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
    
    private func deviceSelected(_ row: ListCheckRow<String>, _ device: DDeviceWithSitesInfo) {
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row.value = nil
            row.cell.isUserInteractionEnabled = false
            row.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert("error_device_not_hub".localized())
        }
        self.device = device
        self.selectedDeviceRow = row
        self.reloadSectionBlock()
    }
    private func selectTypeZone(_ row: PushRow<String>) {
        guard let value = row.value, let zone_type = R.inputTypes.first(where: { $0.title == value }) else { return }
        self.zone_type = zone_type
        self.destionationRow?.options = zone_type.detectorTypes.flatMap({ (dt) -> [String] in return dt.alarmNames() })
        self.destionationRow?.value = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })?.defaultAlarmName()
        self.detector_type = self.zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.isDefaultDetector })
        self.alarm_type = self.detector_type?.alarmTypes.first(where: { (at) -> Bool in return at.isDefaultAlarm })
        
        autoBlock?.hidden = Condition(booleanLiteral: zone_type.id == 4)
        autoBlock?.evaluateHidden()
        
        self.reloadSectionBlock()
        
        guard let delayBlock = self.delayBlock else { return }
        if zone_type.id != 4 {
            let sw = SwitchRow() { $0.title = "LOCAL_CONFIG_EXIT_DELAY".localized(); $0.cell.switchControl.onTintColor = .hubMainColor } .onChange { row in self.delay = (row.value ?? false) }
            UIView.performWithoutAnimation { delayBlock <<< sw }
        } else {
            UIView.performWithoutAnimation { delayBlock.removeAll() }
        }
    }
    private func selectAlarmType(_ row: PushRow<String>) {
        guard  let value = row.value, let end = value.range(of: "\n")?.upperBound, let start = value.range(of: "\n")?.lowerBound,
               let dt = zone_type.detectorTypes.first(where: { (dt) -> Bool in return dt.title == String(value.suffix(from: end)) }),
               let at = dt.alarmTypes.first(where: { (at) -> Bool in return at.title == String(value.prefix(upTo: start)) }) else { return }
        if alarm_type?.id == at.id && detector_type?.id == dt.id { return }
        self.detector_type = dt
        self.alarm_type = at
        self.reloadSectionBlock()
    }
    private func selectSection(_ section: Int64) {
        self.section_id = section
    }
    
    private func createSection() {
        guard let nV = navigationController, let device_id = device?.device.id else { return }
        let vc = XAddSectionController(device_id: device_id) { self.reloadSectionBlock() }
        nV.pushViewController(vc, animated: true)
    }
    private func createZone() {
        view.endEditing(true)

        guard let device_id = device?.device.id else {
            return AlertUtil.errorAlert("add_wireline_zone_warning_select_device".localized())
        }
        
        if nameRow?.value == nil || nameRow?.value?.count == 0 {
            return AlertUtil.errorAlert("add_wireline_zone_warning_put_name".localized())
        }
        
        guard numberRow?.isValid == true, let number = Int64(numberRow?.value ?? ".") else {
            return AlertUtil.errorAlert("add_wireline_zone_warning_put_correct_number".localized())
        }

        let uid = String(format: "%02X", 4) + String(format: "%02X", number) + String(format: "%02X", zone_type.id)
        let detectorHEX: String?
        if self.zone_type.id != 4 {
            detectorHEX = String(format: "%02X", detector_type?.id ?? 0) + String(format: "%02X", alarm_type?.id ?? 0)
        } else {
            detectorHEX = nil
        }
      
//        if !DataManager.settingsHelper.needExpert && section_id == nil {
//            return createSectionAndZone(device_id: device_id, name: nameRow!.value!, detector: detectorHEX.hexaToInt, uid: uid) }
        if self.section_id == nil && self.zone_type.id != 4 {
            return AlertUtil.errorAlert("add_wireline_zone_warning_select_section".localized())
        }

        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: self.section_id ?? 0, name: nameRow!.value!, delay: self.delay ? 1 : 0, detector: detectorHEX?.hexaToInt )
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: self.finish)
    }
    private func createSectionAndZone(device_id: Int64, name: String, detector: Int, uid: String) {
        guard let type = detector_type?.sectionDefault, type > 0 else { return AlertUtil.errorAlert("add_wireline_zone_warning_no_default_section".localized()) }
        if let section_id = dm.getSections(site_id: nil, device_id: device_id).first(where: { (s) -> Bool in return s.detector == type})?.section {
            self.section_id = section_id
            return createZone()
        }
        guard let index = DataManager.shared.getEmptySectionIndex(device_id: device_id) else { return AlertUtil.errorAlert("add_wireline_zone_warning_no_place_for_section".localized()) }
        showProgress()
        disposable = dm.addSection(device_id: device_id, name: nil, index: index, type: Int64(type)).asObservable()
            .concatMap({ (result) -> Single<DCommandResult> in
                if result.success { return self.dm.addZone(device_id: device_id, uid: uid, section: index, name: name, delay: self.delay ? 1 : 0, detector: detector) }
                return Single.just(result)
            })
            .observeOn(MainScheduler.init())
            .subscribe(onNext: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
    
    private func showProgress() {
        createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }
}
