//
//  XDeviceSelectController.swift
//  SecurityHub
//
//  Created by Timerlan on 24/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift

enum XWirelessType {
    case zone, relay, user
}
class XFindWirelessController: XBaseFormController {
    private let dm: DataManager
    private let type: XWirelessType
    private let section_id: Int64?
    private var devices: [DDeviceWithSitesInfo] = []
    private var device: DDeviceWithSitesInfo?
    private var selectedDeviceRow: ListCheckRow<String>?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var interactiveModeButton: XButtonView?
    private var disposable: Disposable?
    
    init(device_ids: [Int64], section_id: Int64?, type: XWirelessType) {
        self.dm = DataManager.shared
        self.type = type
        self.section_id = section_id
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        switch (type) {
        case .zone: title = "WIZ_DEVICE_ADD_TITLE".localized()
        case .relay: title = "RELAY_ADD_TITLE".localized()
        case .user: title = "USER_SET_TITLE".localized()
        }
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDeviceSelectBlock()
        initInteractiveModeDescription()
        initInteractiveModeButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func initDeviceSelectBlock() {
//        if devices.count == 1 { return device = devices[0] }
        deviceBlock = SelectableSection<ListCheckRow<String>>("DTA_CHOOSE_DEVICE".localized(), selectionType: .singleSelection(enableDeselection: false))
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                $0.disabled = Condition(booleanLiteral: !dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id))
                if _device.offset == 0,
                   HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id),
                   dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id),
                   dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed
                {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in
                    guard _row.value != nil else {
                        return
                    }

                    guard self.dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed else {
                        _row.value = nil
                        return AlertUtil.warAlert("error_device_armed".localized())
                    }
                    
                    self.deviceSelected(_row, _device.element)
                }
            }
        }
        form +++ deviceBlock!
    }
    private func initInteractiveModeDescription() {
        //TODO: нужмы картинки и описания для добавления реле
        let images = type == .user ? ["hogorgan"] : ["device","device_1","device_2","device_3"]
        let desc = type == .user ? "interactive_hozorgan_desc".localized() : "interactive_zone_desc".localized()
        form +++ Section(header: "interactive_mode".localized(), footer: desc)
            <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images); cell.update() }
    }
    private func initInteractiveModeButton() {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        interactiveModeButton = XButtonView(frame, title: "button_interactive_zone_start".localized(), style: .text)
        interactiveModeButton?.click = self.startInteractiveMode
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.interactiveModeButton; cell.update() }
    }
    
    private func deviceSelected(_ row: ListCheckRow<String>, _ device: DDeviceWithSitesInfo) {
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row.value = nil
            row.cell.isUserInteractionEnabled = false
            row.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert("error_device_not_hub".localized())
        }
        self.device = device
        self.selectedDeviceRow = row
    }
    
    private func startInteractiveMode() {
        guard let device = self.device else { return AlertUtil.warAlert("warning_select_device".localized()) }
        interactiveModeButton?.state = .progress
        for row in deviceBlock?.allRows ?? [] {
            guard let _row = row as? ListCheckRow<String> else { break }
            _row.cell.isUserInteractionEnabled = false
            _row.cell.contentView.alpha = 0.5
        }
        disposable = dm.interactiveMode(device_id: device.device.id)
            .observeOn(MainScheduler.init())
            .subscribe(onNext: { result in self.interactiveModeResult(result) } )
    }
    private func endInteractiveMode() {
        disposable?.dispose()
        interactiveModeButton?.state = .enable
        for row in deviceBlock?.allRows ?? [] {
            guard let _row = row as? ListCheckRow<String> else { break }
            _row.cell.isUserInteractionEnabled = true
            _row.cell.contentView.alpha = 1
        }
    }
    private func interactiveModeResult(_ result: DInteractiveModeResult) {
        switch result.status {
        case .start: break;
        case .error:
            endInteractiveMode()
            AlertUtil.errorAlert(result.message)
        case .not_start:
            endInteractiveMode()
            AlertUtil.warAlert(title: "interactive_result".localized(), result.message)
        case .finish:
            endInteractiveMode()
            guard let uid = result.uid, let uid_type = result.uid_type else { return AlertUtil.warAlert(title: "interactive_result".localized(), result.message) }
            createZone(uid: uid, uid_type: uid_type)
        }
    }
    private func createZone(uid: String, uid_type: Int64) {
        let array = uid.hexaToArray
        guard array.count >= 2 else { return AlertUtil.errorAlert("error_interactive_no_name_zone".localized()) }
        let uid_format = array.last ?? 0 & 15
        let zone_type_id = uid_format == 1 ? array[0] >> 4 & 15 : uid_format == 2 ? array[0] << 8 | array[1] : 0
        let zone_type = R.zoneTypes.first(where: { (zt) -> Bool in return zt.id == zone_type_id})
        guard let nV = navigationController, let device = device else { return AlertUtil.errorAlert("ERROR".localized()) }
        
        switch type {
        case .zone:
            guard uid_type == 1 else { return AlertUtil.errorAlert("error_interactive_find_user_not_zone".localized()) }
            guard let _zone_type = zone_type else { return AlertUtil.errorAlert("error_interactive_no_name_zone".localized()) }
            if _zone_type.detectorTypes.count == 0 && _zone_type.id != 2331 { return AlertUtil.errorAlert("error_interactive_find_relay_not_zone".localized()) }
            let vc = _zone_type.detectorTypes.count > 0 ? XAddWirelessZoneController(device_id: device.device.id, section_id: section_id, uid: uid, zone_type: _zone_type) :
                XAddWirelessExitController(device_id: device.device.id, section_id: section_id, uid: uid, zone_type: _zone_type)
            nV.pushViewController(vc, animated: true)
        case .relay:
            guard uid_type == 1 else { return AlertUtil.errorAlert("error_interactive_find_user_not_relay".localized()) }
            guard let _zone_type = zone_type else { return AlertUtil.errorAlert("error_interactive_no_name_zone".localized()) }
            if _zone_type.detectorTypes.count > 0 || _zone_type.id == 2331 { return AlertUtil.errorAlert("error_interactive_find_zone_not_relay".localized()) }
            let vc = XAddWirelessRelayController(device_id: device.device.id, uid: uid, type: _zone_type)
            nV.pushViewController(vc, animated: true)
        case .user:
            guard uid_type == 2 else { return AlertUtil.errorAlert("error_interactive_find_zone_not_user".localized()) }
            let vc = XAddWirelessUserController(device_id: device.device.id, uid: uid, zone_type: zone_type)
            nV.pushViewController(vc, animated: true)
        }
    }
}
