//
//  XAddWirelessRelayController.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift


class XAddWirelessRelayController: XBaseFormController {
    private let dm: DataManager
    private let device_id: Int64
    private let uid: String
    private let type: DZoneTypeEntity
    private var detector = 23
    private var nameRow: TextRow?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_id: Int64, uid: String, type: DZoneTypeEntity) {
        self.dm = DataManager.shared
        self.device_id = device_id
        self.uid = uid
        self.type = type
        super.init(nibName: nil, bundle: nil)
        title = "RELAY_ADD_TITLE".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTypeInfoBlock()
        initRelayInfoBlock()
        initCreateButton()
    }
    
    private func initTypeInfoBlock() {
        let images = [type.img]
            form +++ Section(header: type.title, footer: type.subTitle)
            <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images, height: 200, margin: 40); cell.update() }
    }
    private func initRelayInfoBlock() {
        let typeRow = PushRow<String>() {
            $0.title = "add_wireless_relay_type".localized()
            if (XTargetUtils.isScripts)
            {
                $0.options = ["wireless_relay_defaul_manage".localized(), "wireless_relay_auto_manage".localized()]
            }
            else {$0.options = ["wireless_relay_defaul_manage".localized()]}
            $0.value = "wireless_relay_defaul_manage".localized()
            $0.onChange { (row) in self.selectTypeRelay(row) }
            $0.onPresent {form, selectorController in
                selectorController.enableDeselection = false }
        }
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = "wireless_relay_name_title".localized()
            $0.placeholder = "wireless_relay_name_placeholder".localized()
        }
        form +++ Section("add_wireless_relay_info".localized())
        <<< nameRow!
        <<< typeRow
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "add_wireless_relay_add".localized(), style: .text)
        createButton?.click = self.createRelay
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
       
    
    private func selectTypeRelay(_ row: PushRow<String>) {
        self.detector = row.value == "wireless_relay_defaul_manage".localized() ? 23 : 13
    }
    private func showProgress() {
        createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }

    
    private func createRelay() {
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert("add_wireless_relay_warning_put_name".localized()) }
        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: 0, name: nameRow!.value!, detector: detector)
              .observeOn(MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        navigationController?.popToRootViewController(animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
