//
//  XAddScriptController+UI.swift
//  SecurityHub
//
//  Created by Timerlan on 11/12/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka

extension XAddScriptController {
    func initInfoBlock() {
        form +++ Section(header: HubLibraryScript.Data.LocalazedString.getName(script.name) ?? "NO_NAME".localized(),
                         footer: HubLibraryScript.Data.LocalazedString.getName(script.description) ?? "NO_NAME".localized())
    }
    func initReleyBlock() {
        let relay = dm.getAutoRelayScriptUi(device_id: device_id, notSelectValue: "add_script_auto_relay_not_select".localized())
        let row = PushRow<String>() {
            $0.title = "add_script_auto_relay_title".localized()
            $0.options = relay.map{ $0.value }
            $0.value = relay.first(where: { $0.key == self.bind })?.value ?? relay.first?.value
            $0.onChange { (row) in
                guard let key = relay.first(where: { $0.value == row.value })?.key else { return }
                self.bind = key == 0 ? nil : key
            }
        }.onPresent { _, selectorController in  selectorController.enableDeselection = false }
        self.addRow(header: "add_script_auto_relay_header".localized(), footer: "add_script_auto_relay_footer".localized(), row: row)
    }
    func initParams(header: String?, footer: String?, params: [HubLibraryScript.Data.Params]) {
        if params.count == 0 { return }
        for param in params {
            switch param.type {
            case "int":             textParam(header: header, footer: footer, param: param)
            case "time":            groupParam(header: header, footer: footer, param: param)
            case "zone":            zoneOrSectionParam(header: header, footer: footer, param: param, isZone: true)
            case "zones":           zonesOrSectionsParam(header: header, footer: footer, param: param, isZone: true)
            case "group":           groupParam(header: header, footer: footer, param: param)
            case "section":         zoneOrSectionParam(header: header, footer: footer, param: param, isZone: false)
            case "sections":        zonesOrSectionsParam(header: header, footer: footer, param: param, isZone: false)
            case "time_zone":       timeZoneParam(header: header, footer: footer, param: param, correctTimeZones: false)
            case "time_zone_m":     timeZoneParam(header: header, footer: footer, param: param, correctTimeZones: true)
            case "list":            listParam(header: header, footer: footer, param: param);
            case "float":           textParam(header: header, footer: footer, param: param)
            case "xfloat":          textParam(header: header, footer: footer, param: param)
            case "time_utc_group":  groupParam(header: header, footer: footer, param: param)
            case "time_m_utc":      timePickerParam(header: header, footer: footer, param: param)
            default:                textParam(header: header, footer: footer, param: param)
            }
        }
    }
    func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: "add_script_add_button".localized(), style: .text)
        createButton?.click = self.create
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
    func initUpdateButton() {
        deleteButton?.state = .enable
        lockUI()
        let updateButton = UIBarButtonItem()
        updateButton.tintColor = DEFAULT_SELECTED
        updateButton.title = "add_script_change_button".localized()
        updateButton.target = self
        updateButton.action = #selector(initSaveButton)
        navigationItem.rightBarButtonItem = updateButton
        initDeleteButton()
    }
    @objc private func initSaveButton() {
        unlockUI()
        let updateButton = UIBarButtonItem()
        updateButton.tintColor = DEFAULT_SELECTED
        updateButton.title = "add_script_save_button".localized()
        updateButton.target = self
        updateButton.action = #selector(save)
        navigationItem.rightBarButtonItem = updateButton
        hideDeleteButton()
    }
    @objc private func save() {
        AlertUtil.deleteXAlert(text: "add_script_alert_save_changes_title".localized(), actionText: "add_script_alert_save_changes_button".localized()) {
            self.create()
        }
    }
    func initDeleteButton() {
        deleteButton = XButtonView(XButtonView.defaultRect, title: "add_script_delete_button".localized(), style: .fill, mainColor: UIColor.systemRed, rippleColor: UIColor.hubRedDarkColor)
        deleteButton?.click = self.deleteDialog
        deleteBlock = ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.deleteButton; cell.update() }
        UIView.performWithoutAnimation { form +++ deleteBlock! }
    }
    private func deleteDialog() {
        AlertUtil.deleteXAlert(text: "add_script_alert_delete_script".localized()) {
            self.delete()
        }
    }
    func hideDeleteButton() {
        if let _ = deleteBlock { UIView.performWithoutAnimation { form.remove(at: form.count - 1) } }
        deleteBlock = nil
    }
    
    private func groupParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params) {
        var _header = header, _footer = footer
        if (header == nil) { _header = HubLibraryScript.Data.LocalazedString.getName(param.description) }
        else if (footer == nil) { _footer = HubLibraryScript.Data.LocalazedString.getName(param.description) }
        else { _header = header! + "\n" + footer!; _footer = HubLibraryScript.Data.LocalazedString.getName(param.description) }
        if let params = param.group { initParams(header: _header, footer: _footer, params: params) }
    }
    private func textParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params) {
        guard let name = param.name else { return }
        if !self.params.contains(where: { $0.key == name }) { self.params.updateValue(param._default, forKey: name) }
        let hft = getHFT(header: header, dH: "add_script_text_param_header".localized(), footer: footer, title: "add_script_text_param_title".localized(), param: param)
        
        var def_value = self.params[name] ?? nil
        if param.type == "xfloat", let _v = Double(def_value ?? "") { def_value = "\( _v / 100)" }
        
        let row = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = hft.title
            $0.cell.textField.keyboardType = "-1" ~= param.format ? UIKeyboardType.numbersAndPunctuation : UIKeyboardType.numberPad
            $0.value = def_value
            $0.add(rule: getStringRule(param.format))
        }.cellUpdate { cell, row in
            if !row.isValid { cell.titleLabel?.textColor = .systemRed; self.params.updateValue(nil, forKey: name) }
            else {
                if param.type == "xfloat", var _v = Double(row.value ?? "") {
                    _v = _v * 100
                    self.params.updateValue("\(_v)", forKey: name)
                } else {
                    self.params.updateValue(row.value, forKey: name)
                }
            }
        }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        addRow(header: hft.header, footer: hft.footer, row: row)
    }
    private func zoneOrSectionParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params, isZone: Bool) {
        guard let name = param.name else { return }
        if !self.params.contains(where: { $0.key == name }) { self.params.updateValue(nil, forKey: name) }
        let values = isZone ?   self.dm.getOnlyZonesScriptUi(device_id: device_id, notSelectValue: "add_script_not_select_zone".localized()) :
        self.dm.getSectionsScriptUi (device_id: device_id, notSelectValue: "add_script_not_select_section".localized())
        let hft = self.getHFT(header: header, dH: isZone ? "add_script_select_zone_header".localized() : "add_script_select_section_header".localized(), footer: footer, title: isZone ? "add_script_select_zone_title".localized() : "add_script_select_section_title".localized(), param: param)
        let row = PushRow<String>() {
            $0.title = hft.title
            $0.options  =   values.map{ $0.value }
            $0.value    =   values.first(where: { String($0.key) == (self.params[name] as? String) })?.value ?? values.first?.value
            $0.onChange { (row) in
                guard let key = values.first(where: { $0.value == row.value })?.key  else { return }
                self.params.updateValue(key == 0 ? nil : String(key), forKey: name)
            }
        }
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        addRow(header: hft.header, footer: hft.footer, row: row)
    }
    func zonesOrSectionsParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params, isZone: Bool) {
        guard let name = param.name else { return }
        if !self.params.contains(where: { $0.key == name }) { self.params.updateValue(nil, forKey: name) }
        let values = isZone ?   self.dm.getOnlyZonesScriptUi(device_id: device_id) :
                                self.dm.getSectionsScriptUi(device_id: device_id)
        let hft = self.getHFT(header: header, dH: isZone ? "add_script_select_zones_header".localized() : "add_script_select_sections_header".localized(), footer: footer, title: isZone ? "add_script_select_zones_title".localized() : "add_script_select_sections_title".localized(), param: param)
        let row = MultipleSelectorRow<String>() {
            $0.title = hft.title
            $0.options = values.map{ $0.value }
            $0.value = self.getValues(value: self.params[name] ?? nil, values: values)
            $0.onChange { (row) in self.params.updateValue(self.getValue(value: row.value, values: values), forKey: name) }
        }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        self.addRow(header: hft.header, footer: hft.footer, row: row)
    }
    func listParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params) {
        guard let name = param.name else { return }
        if !self.params.contains(where: { $0.key == name }) { self.params.updateValue(param._default, forKey: name) }
        let values = self.getExtraList(param.extra)
        let hft = self.getHFT(header: header, dH: "add_script_select_param_header".localized(), footer: footer, title: "add_script_select_param_title".localized(), param: param)
        let row = PushRow<String>() {
            $0.title = hft.title
            $0.options = values.map{ $0.value }
            $0.value = values[(self.params[name] ?? nil) ?? ""] ?? values[param._default ?? ""]
            $0.onChange { (row) in
                guard let key = values.first(where: { $0.value == row.value })?.key else { return }
                self.params.updateValue(key, forKey: name)
            }
        }
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        self.addRow(header: hft.header, footer: hft.footer, row: row)
    }
    func timeZoneParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params, correctTimeZones: Bool) {
        guard let name = param.name else { return }
        if !self.params.contains(where: { $0.key == name }) { self.params.updateValue(self.getTimeValue(correctTimeZones: correctTimeZones, value: "+03:00"), forKey: name) }
        let values = self.getTimes(correctTimeZones: correctTimeZones)
        let hft = self.getHFT(header: header, dH: "add_script_select_time_zone_header".localized(), footer: footer, title: "add_script_select_time_zone_title".localized(), param: param)
        let row = PushRow<String>(param.name, {
            $0.title = hft.title
            $0.options = values
            $0.value = self.getTime(correctTimeZones: correctTimeZones, value: self.params[name]) ?? "+03:00"
            $0.onChange { (row) in if let v = row.value { self.params.updateValue(self.getTimeValue(correctTimeZones: correctTimeZones, value: v), forKey: name) } }
        })
        .onPresent { _, selectorController in selectorController.enableDeselection = false }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        self.addRow(header: hft.header, footer: hft.footer, row: row)
    }
    func timePickerParam(header: String?, footer: String?, param: HubLibraryScript.Data.Params) {
        guard let name = param.name else { return }
        
        let hft = getHFT(header: header, dH: "add_script_text_param_header".localized(), footer: footer, title: "add_script_text_param_title".localized(), param: param)
        let def_value = self.params[name] ?? nil
        var def_date: Date? = nil
        let dateFormatter = DateFormatter()

        if let def_value2 = def_value, let def_value3 = Int(def_value2) {
            let hour = Int(def_value3 / 60)
            let minute = Int(def_value3 % 60)
            dateFormatter.dateFormat = "HH:mm"
            def_date = dateFormatter.date(from: "\(hour):\(minute)")
        }
        
        let row = TimeRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = hft.title
            $0.value = def_date
        }
        .cellUpdate { _, row in
            guard let date = row.value else { return }
            dateFormatter.dateFormat = "HH"
            guard let hour = Int(dateFormatter.string(from: date)) else { return }
            dateFormatter.dateFormat = "mm"
            guard let minute = Int(dateFormatter.string(from: date)) else { return }

            self.params.updateValue("\(hour * 60 + minute)", forKey: name)
        }
        paramsInfo.updateValue((type: param.type, info: hft.header + "\n" + hft.title), forKey: name)
        addRow(header: hft.header, footer: hft.footer, row: row)
    }
}
