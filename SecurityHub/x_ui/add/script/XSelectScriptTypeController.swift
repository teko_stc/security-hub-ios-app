//
//  XSelectScriptTypeController.swift
//  SecurityHub
//
//  Created by Timerlan on 09/12/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Eureka
import RxSwift

class XSelectScriptTypeController: XBaseFormController {
    private let dm: DataManager
    private var devices: [DDeviceWithSitesInfo] = []
    private var device: DDeviceWithSitesInfo?
    private var script: HubLibraryScript.Data?
    private var selectedDeviceRow: ListCheckRow<String>?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var disposable: Disposable?
    private var progressBlock: ViewRow<UIActivityIndicatorView>?
    private var nextBlock: ViewRow<XButtonView>?
    private var typeBlock: Section?
    private var bind: Int64?

    init(device_ids: [Int64], bind: Int64? = nil) {
        self.dm = DataManager.shared
        self.bind = bind
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        title = "title_select_script_type".localized()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }

    override func viewDidLoad() {
        super.viewDidLoad()
        initDeviceSelectBlock()
    }

    private func initDeviceSelectBlock() {
//        if devices.count == 1 { return deviceSelected(nil, devices[0]) }
        deviceBlock = SelectableSection<ListCheckRow<String>>("DTA_CHOOSE_DEVICE".localized(), selectionType: .singleSelection(enableDeselection: false))
        form +++ deviceBlock!
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                $0.disabled = Condition(booleanLiteral: !dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id))
                if _device.offset == 0,
                   HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id),
                   dm.dbHelper.isDeviceConnected(device_id: _device.element.device.id),
                   dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed
                {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in
                    guard _row.value != nil else {
                        return
                    }

                    guard self.dm.dbHelper.getArmStatus(device_id: _device.element.device.id) == .disarmed else {
                        _row.value = nil
                        return AlertUtil.warAlert("error_device_armed".localized())
                    }
                    
                    self.deviceSelected(_row, _device.element)
                }
            }
        }
    }
    private func showProgress() {
        if progressBlock != nil { return }
        let progressView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        progressView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        progressView.center = view.center
        progressView.startAnimating()
        progressBlock = ViewRow<UIActivityIndicatorView>(tag: "UIActivityIndicatorView").cellSetup { (cell, row) in cell.view = progressView; cell.update() }
        form +++ progressBlock!
    }
    private func clear() {
        if deviceBlock == nil { form.removeAll() } else { while form.count > 1 { form.remove(at: form.count - 1) } }
        progressBlock = nil
        typeBlock = nil
        nextBlock = nil
    }
    
    private func deviceSelected(_ row: ListCheckRow<String>?, _ device: DDeviceWithSitesInfo) {
        if self.device?.device.id == device.device.id { return }
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row?.value = nil
            row?.cell.isUserInteractionEnabled = false
            row?.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert("error_device_not_hub".localized())
        }
        self.device = device
        self.selectedDeviceRow = row
        self.clear()
        self.disposable?.dispose()
        self.showProgress()
        disposable = dm.getLibraryScripts(config_version: device.device.configVersion)
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: { self.result(result: $0, value: $1) }, onError: { _ in self.error(DCommandResult.ERROR) })
    }
    private func categorySelected(_ row: PushRow<String>, _ value: HubLibraryScripts) {
        if (typeBlock?.count ?? 0 > 1) { typeBlock?.remove(at: 1) }
        if (row.value == "add_script_category_not_select".localized()) { return }
        if (nextBlock != nil) { form.remove(at: form.count - 1); nextBlock = nil }
        let scripts = value.items.compactMap{ $0.data }.filter{ (HubLibraryScript.Data.LocalazedString.getName($0.category) ?? "NO_NAME".localized()).replacingOccurrences(of: "/", with: " ") == row.value }
        var scripts_values = scripts.map{ HubLibraryScript.Data.LocalazedString.getName($0.name) ?? "NO_NAME".localized() }
        scripts_values.insert("add_script_not_select".localized(), at: 0)
        typeBlock! <<< PushRow<String>() {
            $0.title = "add_script_select".localized()
            $0.options = scripts_values
            $0.value = "add_script_not_select".localized()
            $0.onChange { (row) in self.scriptSelected(row, scripts) }
        }.onPresent { _, selectorController in selectorController.enableDeselection = false }
    }
    private func scriptSelected(_ row: PushRow<String>, _ values: [HubLibraryScript.Data]) {
        if (row.value == "add_script_not_select".localized()) { form.remove(at: form.count - 1); return nextBlock = nil }
        script = values.first{ (HubLibraryScript.Data.LocalazedString.getName($0.name) ?? "NO_NAME".localized()) == row.value }
        if (nextBlock != nil) { return }
        let button = XButtonView(XButtonView.defaultRect, title: "add_script_next".localized(), style: .text)
        button.click = self.next
        nextBlock = ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = button; cell.update() }
        form +++ nextBlock!
    }
    
    private func result(result: DCommandResult, value: HubLibraryScripts?) {
        self.clear()
        guard result.success, let value = value else { return error(result) }
        var categories = Array(
            Set(
                value.items.compactMap{ $0.data }
                    .map{ (HubLibraryScript.Data.LocalazedString.getName($0.category) ?? "NO_NAME".localized()).replacingOccurrences(of: "/", with: " ") }
            )
        ).sorted()
        categories.insert("add_script_category_not_select".localized(), at: 0)
        typeBlock = Section("add_script_category_select_header".localized())
        typeBlock! <<< PushRow<String>() {
            $0.title = "add_script_category_select".localized()
            $0.options = categories
            $0.value = "add_script_category_not_select".localized()
            $0.onChange { (row) in self.categorySelected(row, value) }
        }.onPresent { _, selectorController in selectorController.enableDeselection = false }
        form +++ typeBlock!
    }
    private func error(_ result: DCommandResult) { AlertUtil.errorAlert(result.message) }
    
    private func next() {
        guard let script = self.script, let device = device?.device else { return }
        guard let nV = navigationController else { return }
        let vc = XAddScriptController(device_id: device.id, script: script, bind: bind)
        nV.pushViewController(vc, animated: true)
    }
}
