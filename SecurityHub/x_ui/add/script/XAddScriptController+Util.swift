//
//  XAddScriptController+Util.swift
//  SecurityHub
//
//  Created by Timerlan on 11/12/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka

extension XAddScriptController {
    
    func showProgress() {
        createButton?.state = .progress
        lockUI()
    }
    
    func getHFT(header: String?, dH: String, footer: String?, title: String, param: HubLibraryScript.Data.Params) -> (header: String, footer: String, title: String) {
        var desc = ""; HubLibraryScript.Data.LocalazedString.getName(param.description)?.forEach{ desc += $0 == "(" ? " (" : "\($0)" }
        var title = title; let arr = desc.split(separator: " ")
        if arr.count > 2 { title = String(arr[0]) + " " + String(arr[1]) }
        else if desc.count > 1 { title = desc }

        let _header = header ?? dH
        let _footer = footer ?? desc
        return (header: _header, footer: _footer, title: title)
    }
    
    func getStringRule(_ format: String?) -> RuleClosure<String> {
        return RuleClosure<String> { rowValue in
            guard let rowValue = rowValue, !rowValue.isEmpty else { return ValidationError(msg: "Field required!") }
            if let format = format { return rowValue ~= format ? nil : ValidationError(msg: "Field required!") }
            return nil
        }
    }
    
    func addRow(header: String, footer: String, row: BaseRow) {
        if let _lastSection = lastSection, _lastSection.header?.title == header, _lastSection.footer?.title == footer { lastSection! <<< row; return }
        if lastSection?.header?.title == header { self.lastSection = Section(header: "", footer: footer) { $0.header?.height = { 0 } } }
        else { self.lastSection = Section(header: header, footer: footer)  }
        form +++ self.lastSection!
        self.lastSection! <<< row
    }
    
    func getValues(value: String?, values: [(key: Int64, value: String)]) -> Set<String> {
        var results: Set<String> = []
        guard let value = Int64(value ?? "") else { return results }
        for v in values {
            if !(value & (1 << (v.key - 1)) == 0) { results.insert(v.value) }
        }
        return results
    }
    
    func getValue(value: Set<String>?, values: [(key: Int64, value: String)]) -> String {
        var _value: Int64 = 0
        for v in value ?? [] {
            guard let key = values.first(where: { $0.value == v })?.key else { break }
            _value = _value | (1 << (key - 1))
        }
        return String(_value)
    }
    
    func getExtraList(_ extra: [HubLibraryScript.Data.Extra]) -> [String : String] {
        var values: [String : String] = [:]
        extra.forEach{ values.updateValue(HubLibraryScript.Data.LocalazedString.getName($0.description) ?? "", forKey: $0.value) }
        return values
    }
    
    func getTimes(correctTimeZones: Bool) -> [String] {
        var values: [String] = []
        for i in -11...14 { values.append( (i < 0 ? String(format: "%03d", i) : ("+" + String(format: "%02d", i)) ) + ":00") }
        if correctTimeZones {
            values.append("-02:30"); values.append("-03:30"); values.append("+03:30")
            values.append("+04:30"); values.append("+05:30"); values.append("+06:30")
            values.append("+09:30"); values.append("+10:30")
        }
        values = values.sorted(by: { (r1, r2) -> Bool in
            let f1 = Int(String(r1[r1.startIndex..<r1.index(r1.startIndex, offsetBy: 3)])) ?? 0
            let f2 = Int(String(r2[r2.startIndex..<r2.index(r2.startIndex, offsetBy: 3)])) ?? 0
            return f1 > f2
        })
        return values
    }
    
    func getTime(correctTimeZones: Bool, value: String??) -> String? {
        guard let time = Int64((value ?? "") ?? "") else { return nil }
        if correctTimeZones {
            let hour: Int64 = time / 60
            let min = abs(time) - (abs(hour) * 60)
            return (hour < 0 ? String(format: "%03d", hour) : ("+" + String(format: "%02d", hour)) ) + ":" + String(format: "%02d", min)
        }
        return (time < 0 ? String(format: "%03d", time) : ("+" + String(format: "%02d", time)) ) + ":00"
    }
    
    func getTimeValue(correctTimeZones: Bool, value: String) -> String {
        if correctTimeZones {
            guard let hour = Int64(String(value[value.startIndex..<value.index(value.startIndex, offsetBy: 3)])),
                  let min = Int64(String(value[value.index(value.startIndex, offsetBy: 4)..<value.index(value.startIndex, offsetBy: 6)])) else { return "3" }
            let time = hour * 60 + (hour > 0 ? min : -min)
            return "\(time)"
        }
        guard let time = Int64(String(value[value.startIndex..<value.index(value.startIndex, offsetBy: 3)])) else { return "3" }
        return "\(time)"
    }
    
    func lockUI() {
        for row in form.allRows {
            if deleteBlock?.baseCell != row.baseCell {
                row.baseCell.isUserInteractionEnabled = false
                row.baseCell.contentView.alpha = 0.5
            }
        }
    }
    func unlockUI() {
        for row in form.allRows {
            if deleteBlock?.baseCell != row.baseCell {
                row.baseCell.isUserInteractionEnabled = true
                row.baseCell.contentView.alpha = 1.0
            }
        }
    }
}
