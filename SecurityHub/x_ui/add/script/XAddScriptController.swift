//
//  XAddScriptController.swift
//  SecurityHub
//
//  Created by Timerlan on 09/12/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Eureka
import RxSwift

class XAddScriptController: XBaseFormController {
    let dm: DataManager
    let device_id: Int64
    let script: HubLibraryScript.Data
    var lastSection: Section?
    var createButton: XButtonView?
    var deleteButton: XButtonView?
    var deleteBlock: ViewRow<XButtonView>?
    var bind: Int64?
    var params: [String : String?] = [:]
    var paramsInfo: [String : (type: String, info: String)] = [:]
    var disposable: Disposable?
    private let isUpdate: Bool

    init(device_id: Int64, script: HubLibraryScript.Data, params: [[String: Any]]? = nil, name: String? = nil, bind: Int64? = nil) {
        self.dm = DataManager.shared
        self.device_id = device_id
        self.script = script
        for param in params ?? [] {
            guard let name = param["name"] as? String, let value = param["value"] as? String else { break; }
            self.params.updateValue(value, forKey: name)
        }
        self.bind = bind
        self.isUpdate = params != nil
				super.init(nibName: nil, bundle: nil)
        title = params == nil ? "title_add_script".localized() : name ??  "N_WIZARD_ALL_SCRIPT".localized()
        
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }

    override func viewDidLoad() {
        super.viewDidLoad()
        initInfoBlock()
        initReleyBlock()
        initParams(header: nil, footer: nil, params: script.params)
        if isUpdate {
            if (Roles.sendCommands && Roles.manageSectionZones) { initUpdateButton() }
        } else {
            initCreateButton()
        }
    }
    
    func delete() {
        view.endEditing(true)
        deleteButton?.state = .progress
        lockUI()
        disposable?.dispose()
        disposable = dm.deleteScript(device_id: device_id, bind: bind ?? 0)
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: { result in
                guard result.success else { return self.error(result) }
                if let nV = self.navigationController, let last = nV.viewControllers.last(where: { $0 is XScriptsController}) ?? nV.viewControllers.last(where: { $0 is XMainController}) {
                    nV.popToViewController(last, animated: true)
                } else if let nV = self.navigationController, let last = nV.viewControllers.last(where: { $0 is XScriptsController}) ?? nV.viewControllers.last(where: { $0 is XZoneController}) {
                    nV.popToViewController(last, animated: true)
                }
            }, onError: { _ in self.error(DCommandResult.ERROR) })
    }

    @objc func create() {
        view.endEditing(true)
        guard let bind = bind else { return AlertUtil.errorAlert("add_script_warning_select_auto_relay".localized()) }
        var _params: [[String:Any]] = []
        for param in params {
            guard let info = paramsInfo.first(where: { $0.key == param.key }) else { break; }
            guard let value = param.value else { return AlertUtil.errorAlert("\("add_script_warning_put_param".localized())\n" + info.value.info) }
            switch info.value.type {
            case "int", "zone", "zones", "section", "sections", "list", "float", "time_zone", "time_zone_m", "xfloat", "duration_m", "time_m_utc":
                _params.append([ "name" : param.key, "value" : value ])
            default:
                _params.append([ "name" : param.key, "value" : value ])
            }
        }
        let name = HubLibraryScript.Data.LocalazedString.getName(script.name) ?? "NO_NAME".localized()
        let extra = script.extra?.toData().toString() ?? ""
        self.showProgress()
        guard isUpdate else {
            disposable?.dispose()
            disposable = dm.addScript(device_id: device_id, bind: bind, uid: script.script_uid, params: _params, program: script.source, name: name, extra: extra)
                .observeOn(MainScheduler.init())
                .subscribe(onSuccess: self.finish, onError: { _ in self.error(DCommandResult.ERROR) })
            return
        }
        
        disposable?.dispose()
        disposable = dm.deleteScript(device_id: device_id, bind: bind)
            .flatMap { (_) -> Single<DCommandResult> in
                sleep(2)
                return self.dm.addScript(device_id: self.device_id, bind: bind, uid: self.script.script_uid, params: _params, program: self.script.source, name: name, extra: extra)
            }
            .observeOn(MainScheduler.init())
            .subscribe(onSuccess: self.finish, onError: { _ in self.error(DCommandResult.ERROR) })
    }
    private func finish(_ result: DCommandResult?) {
        guard let result = result else { return error(DCommandResult.ERROR) }
        guard result.success else { return error(result) }
        if isUpdate {
            initUpdateButton()
        } else if let nV = self.navigationController, let last = nV.viewControllers.last(where: { $0 is XScriptsController}) ?? nV.viewControllers.last(where: { $0 is XZoneController}) {
            nV.popToViewController(last, animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        deleteButton?.state = .enable
        unlockUI()
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
