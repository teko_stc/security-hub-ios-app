//
//  XRemoveVCStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XRemoveView {
    func viewStyles() -> XRemoveViewStyle {
        XRemoveViewStyle(
            backgroundColor: .white,
            textView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18.5)),
            checkBox: XCheckBoxViewStyle(
                icon: XCheckBoxViewStyle.Icon(select: UIColor.colorFromHex(0x3abeff), unselect: UIColor.colorFromHex(0x414042)),
                text: XCheckBoxViewStyle.Text(select: UIColor.colorFromHex(0x414042), unselect: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
            ),
            actionView: XZoomButtonStyle(backgroundColor: UIColor.colorFromHex(0xf9543a), font: UIFont(name: "Open Sans", size: 25), color: .white)
        )
    }
}
