//
//  XRemoveController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XRemoveControllerStringsProtocol: XRemoveViewStringsProtocol {
    var removeViewTitle: String { get }
}

class XRemoveController: XBaseViewController<XRemoveView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25))
    )
     
    private var strings: XRemoveControllerStringsProtocol
    private var actionVoid: (_ isSelected: Bool?) -> ()
    
    init(strings: XRemoveControllerStringsProtocol, action: @escaping (_ isSelected: Bool?) -> ()) {
        self.strings = strings
        self.actionVoid = action
        super.init(nibName: nil, bundle: nil)
        subTitle = strings.removeViewTitle
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        mainView.setStrings(strings)
        setNavigationViewBuilder(navigationViewBuilder)
        super.viewDidLoad()
        mainView.delegate = self
    }
    
    override func back() {
        if navigationController == nil { dismiss(animated: true, completion: nil) }
        else { navigationController?.popViewController(animated: true) }
    }
}

extension XRemoveController: XRemoveViewDelegate {
    func actionViewTapped(isSelected: Bool?) {
        if navigationController == nil { dismiss(animated: true, completion: nil) }
        else { navigationController?.popViewController(animated: true) }
        self.actionVoid(isSelected)
    }
}
