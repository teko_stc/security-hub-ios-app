//
//  XRemoveView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XRemoveViewDelegate {
    func actionViewTapped(isSelected: Bool?)
}

protocol XRemoveViewStringsProtocol {
    var removeViewText: String { get }
    var removeViewCheckBoxText: String? { get }
    var removeViewActionText: String? { get }
}

struct XRemoveViewStyle {
    let backgroundColor: UIColor
    let textView: XBaseLableStyle
    let checkBox: XCheckBoxViewStyle
    let actionView: XZoomButtonStyle
}

class XRemoveView: UIView {
    public var delegate: XRemoveViewDelegate?
    
    private var scrollView: UIScrollView!
    private var textView: UILabel!
    private var checkBox: XCheckBoxView!
    private var actionView: XZoomButton!
    
    private lazy var style: XRemoveViewStyle = self.viewStyles()
    private var strings: XRemoveViewStringsProtocol?
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        scrollView = UIScrollView()
        addSubview(scrollView)
        
        textView = UILabel.create(style.textView)
        textView.numberOfLines = 0
        scrollView.addSubview(textView)
        
        checkBox = XCheckBoxView(style.checkBox)
        checkBox.isSelected = true
        scrollView.addSubview(checkBox)
        
        actionView = XZoomButton(style: style.actionView)
        actionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        addSubview(actionView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setStrings(_ strings: XRemoveViewStringsProtocol) {
        self.strings = strings
        textView.text = strings.removeViewText
        if let text = strings.removeViewCheckBoxText { checkBox.text = text }
        else { checkBox.isHidden = true }
        actionView.text = strings.removeViewActionText
    }
    
    private func setConstraints() {
        [scrollView, textView, checkBox, actionView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1),
            
            textView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20),
            textView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -XNavigationView.marginLeft * 2 - XNavigationView.iconSize - 20),
            textView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: XNavigationView.marginLeft * 2 + XNavigationView.iconSize),
            textView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            checkBox.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 20),
            checkBox.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: XNavigationView.marginLeft * 2 + XNavigationView.iconSize),
            checkBox.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            checkBox.heightAnchor.constraint(equalToConstant: 40),
            checkBox.widthAnchor.constraint(equalTo: textView.widthAnchor),
            checkBox.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20),

            actionView.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            actionView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            actionView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.7),
            actionView.heightAnchor.constraint(equalToConstant: 40),
            actionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
    
    @objc private func actionViewTapped() {
        if strings?.removeViewCheckBoxText != nil { delegate?.actionViewTapped(isSelected: checkBox.isSelected) }
        else { delegate?.actionViewTapped(isSelected: nil) }
    }
}
