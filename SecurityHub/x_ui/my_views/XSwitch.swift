//
//  XSwitch.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSwitchStyle {
    struct State {
        let backgroundColor: UIColor
        let borderColor: UIColor
        let thunmColor: UIColor
    }
    
    let select, unselet: State
}
extension XSwitch {
    typealias Style = XSwitchStyle
}

protocol XSwitchDelegate {
    func switchStateChange(switch: XSwitch, isOn: Bool)
}

class XSwitch: UIView, UIGestureRecognizerDelegate {
    public var delegate: XSwitchDelegate?
    public var isOn: Bool {
        didSet {
            if (isOn) {
                thunmCenterXAnchor?.constant = frame.size.width - frame.size.height * 0.9
            } else {
                thunmCenterXAnchor?.constant = frame.size.height * 0.3
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 1
                self.thumbView.backgroundColor = self.isOn ? self.style.select.thunmColor : self.style.unselet.thunmColor
                self.thumbLayer.borderColor = self.isOn ? self.style.select.borderColor.cgColor : self.style.unselet.borderColor.cgColor
                self.layer.borderColor = self.isOn ? self.style.select.borderColor.cgColor : self.style.unselet.borderColor.cgColor
                self.layoutIfNeeded()
            })
        }
    }
    
    public var style: Style = Style(
        select: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0x414042), thunmColor: UIColor.colorFromHex(0x91f279)),
        unselet: XSwitchStyle.State(backgroundColor: .clear, borderColor: UIColor.colorFromHex(0xbcbcbc), thunmColor: UIColor.colorFromHex(0xff954d))
    ) {
        didSet {
            layer.borderColor = isOn ? style.select.borderColor.cgColor : style.unselet.borderColor.cgColor
            thumbLayer.borderColor = isOn ? style.select.borderColor.cgColor : style.unselet.borderColor.cgColor
        }
    }
    
    private let thumbView: UIView = UIView()
    private let thumbLayer = CALayer()
    private var thunmCenterXAnchor: NSLayoutConstraint?

    init(style: Style? = nil, isOn: Bool = false) {
        self.isOn = isOn
        if let style = style { self.style = style }
        super.init(frame: .zero)
        addSubview(thumbView)
        thumbView.translatesAutoresizingMaskIntoConstraints = false
        thumbView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        thumbView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.6).isActive = true
        thumbView.widthAnchor.constraint(equalTo: thumbView.heightAnchor).isActive = true
        
        thumbView.layer.addSublayer(thumbLayer)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesture))
        pan.delegate = self
        thumbView.isUserInteractionEnabled = true
        thumbView.addGestureRecognizer(pan)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        tap.delegate = self
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGesture))
        long.delegate = self
        addGestureRecognizer(long)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private var isInit = true
    override func layoutSubviews() {
        super.layoutSubviews()
        if isInit {
            layer.cornerRadius = frame.size.height / 2
            layer.borderWidth = frame.size.height * 0.07
            
            thumbView.layer.cornerRadius = frame.size.height * 0.6 / 2
            var con = frame.size.height * 0.3
            if (isOn) { con = frame.size.width - frame.size.height * 0.9 }
            thunmCenterXAnchor = thumbView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: con)
            thunmCenterXAnchor?.isActive = true
            
            thumbLayer.frame = CGRect(x: -0.1 * (frame.size.height * 0.6), y: -0.1 * (frame.size.height * 0.6), width: (frame.size.height * 0.6) * 1.2, height: (frame.size.height * 0.6) * 1.2)
            thumbLayer.cornerRadius = (frame.size.height * 0.6 * 1.2)/2
            thumbLayer.borderWidth = frame.size.height * 0.6 * 0.05
            isInit = false
        }
    }
    
    @objc private func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let point = recognizer.location(in: self)
        let x = point.x - frame.size.height * 0.3
        if (recognizer.state == .began) {
            UIView.animate(withDuration: 0.3, animations: { self.alpha = 0.7 })
        }
        if (x > frame.size.height * 0.3 && x < frame.size.width - frame.size.height * 0.9 ) {
            thunmCenterXAnchor?.constant = x
        } else if (x <= frame.size.height * 0.3) {
            thunmCenterXAnchor?.constant = frame.size.height * 0.3
        } else {
            thunmCenterXAnchor?.constant = frame.size.width - frame.size.height * 0.9
        }
        layoutIfNeeded()
        if (recognizer.state == .ended && x > frame.size.width / 2 && x <= frame.size.width) {
            isOn = true
        } else if (recognizer.state == .ended && x <= frame.size.width / 2 && x >= 0) {
            isOn = false
        }
        if (recognizer.state == .ended) {
            delegate?.switchStateChange(switch: self, isOn: isOn)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        }
    }
    
    @objc private func tapGesture(_ sender: UITapGestureRecognizer) {
        if (sender.state == .began) { UIView.animate(withDuration: 0.3, animations: { self.alpha = 0.7 }) }
        else if (sender.state == .ended) {
            isOn = !isOn
            delegate?.switchStateChange(switch: self, isOn: isOn)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        }
    }
    
    @objc private func longGesture(sender: UILongPressGestureRecognizer) {
        if (sender.state == .began) { UIView.animate(withDuration: 0.3, animations: { self.alpha = 0.7 }) }
        else if (sender.state == .ended) {
            isOn = !isOn
            delegate?.switchStateChange(switch: self, isOn: isOn)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        }
    }
}
