//
//  XImageButton.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//

import UIKit
import AudioToolbox

struct XImageButtonStyle {
    let imageName: String
    let font: UIFont?
    let selectColor, unselectColor, backgroundSelectColor: UIColor
}
extension XImageButton {
    typealias Style = XImageButtonStyle
}

class XImageButton: UIButton {
    private let style: Style
    private var title: String = ""
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.3, animations: {
                self.layer.backgroundColor = self.isHighlighted ? self.style.backgroundSelectColor.cgColor : UIColor.clear.cgColor
                self.setTitle(nil, for: .normal)
            })
        }
    }
    
    init(style: Style) {
        self.style = style
        super.init(frame: CGRect())
        contentHorizontalAlignment = .left
        imageView?.tintColor = style.selectColor
        imageEdgeInsets = UIEdgeInsets(top:0, left:23, bottom:0, right: 0)
        titleEdgeInsets = UIEdgeInsets(top:0, left:42, bottom:0, right: 0)
        setImage(UIImage(named: style.imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
        setImage(UIImage(named: style.imageName)?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        layer.cornerRadius = 5
        layer.backgroundColor = UIColor.clear.cgColor
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        self.title = title ?? self.title
        let attr = [
            NSAttributedString.Key.foregroundColor: (isHighlighted ? style.selectColor : style.unselectColor),
            NSAttributedString.Key.font : style.font,
        ]
        setAttributedTitle(NSAttributedString(string: title ?? self.title, attributes: attr as [NSAttributedString.Key : Any]), for: state)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        return true
    }
}
