//
//  SiteMainView.swift
//  SecurityHub
//
//  Created by Timerlan on 03.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class ObjectCloseView: UIView, UICollectionViewDataSource {
    static let height: CGFloat = 146
    
    private var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFit
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        return view
    }()
    private var smallIconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        return view
    }()
    private var locationView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 9)
        view.numberOfLines = 2
        return view
    }()
    private var titleView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        view.numberOfLines = 2
        return view
    }()
    private var infoView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 11)
        view.numberOfLines = 4
        return view
    }()
    private var statusView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        view.numberOfLines = 1
        return view
    }()
    
    private lazy var timeView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "--:--:--"
        view.isHidden = true
        return view
    }()
    private var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 20, height: 20)
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.minimumInteritemSpacing = -2
        flowLayout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(EventMiniCell.self, forCellWithReuseIdentifier: EventMiniCell.cellId)
        return collectionView
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return eventsMini.count }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventMiniCell.cellId, for: indexPath) as! EventMiniCell
        cell.setContent(img: eventsMini[indexPath.row])
        return cell
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.borderWidth = 0.7
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(iconView)
        addSubview(smallIconView)
        addSubview(locationView)
        addSubview(titleView)
        addSubview(statusView)
        addSubview(infoView)
        addSubview(collectionView)
        addSubview(timeView)
       
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(24)
            make.left.equalTo(8)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        smallIconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(10)
            make.right.equalTo(iconView.snp.right).offset(10)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        locationView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(6)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        titleView.snp.remakeConstraints{ make in
            make.top.equalTo(locationView.snp.bottom).offset(2)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        infoView.snp.remakeConstraints{ make in
            make.top.equalTo(titleView.snp.bottom).offset(6)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        timeView.snp.remakeConstraints{ make in
            make.left.equalTo(smallIconView.snp.right).offset(8)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalTo(timeView.snp.top).offset(-4)
        }
        statusView.snp.remakeConstraints{ make in
            make.left.equalTo(smallIconView.snp.right).offset(8)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
        }
        collectionView.snp.remakeConstraints{ make in
            make.left.equalTo(16)
            make.height.equalTo(40)
            make.width.equalTo(60)
            make.bottom.equalToSuperview().offset(-4)
        }
    }
    
    func set(title: String, info: String, status: String, time: String) {
        titleView.text = title
        infoView.text = info
        statusView.text = status
        timeView.text = time
    }
    
    func setLocation(object: String?, device: String?, section: String?) {
        var location: String?
        if let object = object {
            location = object
        }
        if let device = device, let _ = location {
            location! += " → \(device)"
        } else if let device = device {
            location = device
        }
        if let section = section, let _ = location {
            location! += " → \(section)"
        } else if let section = section {
            location = section
        }
        locationView.text = location ?? "  "
    }
    
    func setImage(_ imgMain: String, mainColor: UInt? = nil, imgSmall: String?) {
        if let mainColor = mainColor {
            iconView.image = UIImage(named: imgMain)?.withColor(color: UIColor.colorFromHex(mainColor))
        } else {
            iconView.image = UIImage(named: imgMain)
        }
        smallIconView.isHidden = imgSmall == nil
        if let small = imgSmall { smallIconView.image = UIImage(named: small) }
    }
    
    private var eventsMini: [String] = []
    func setAffects(_ affects: [String]) {
        collectionView.dataSource = self
        eventsMini = affects
        collectionView.reloadData()
    }
    
    @available(*, deprecated, message: "OlD")
    func setStatus(status: ARMSTATUS, isAlert: Bool, affects: [String]) {}
    
    @available(*, deprecated, message: "OlD")
    func setMenuListener(_ void: @escaping (()->Void)) { }
}
