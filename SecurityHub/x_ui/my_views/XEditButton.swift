//
//  XEditButton.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XEditButtonStyle {
    var header: XBaseLableStyle? = nil
    let title: XBaseLableStyle
		var subTitle: XBaseLableStyle? = nil
    var _switch: XSwitch.Style? = nil
}
extension XEditButton {
    typealias Style = XEditButtonStyle
}



class XEditButton: UIButton, XSwitchDelegate {
    public var onSelected: ((Bool) -> ())?
    public var onTapped: (() -> ())?
    
    private let headerTitleView: UILabel = UILabel()
		private let subTitleView: UILabel = UILabel()
    private var switchView: XSwitch? = nil
    private let style: Style
    
    public var title: String? {
        didSet {
            setTitle(title, for: .normal)
        }
    }
    
    public var headerTitle: String? {
        didSet {
            headerTitleView.text = headerTitle
        }
    }
	
		public var subTitle: String? {
				didSet {
						subTitleView.numberOfLines = isMultiLine ? 0 : 1
						subTitleView.text = subTitle
				}
		}
    
    public var isMultiLine: Bool = false {
        didSet {
            setTitle(title, for: .normal)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.3) { self.alpha = self.isHighlighted ? 0.5 : 1 }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            switchView?.isOn = isSelected
        }
    }
    
    init(style: Style) {
        self.style = style
        super.init(frame: .zero)
        if let headerStyle = style.header {
            headerTitleView.font = headerStyle.font
            headerTitleView.textColor = headerStyle.color
            headerTitleView.textAlignment = headerStyle.alignment
            addSubview(headerTitleView)
            headerTitleView.translatesAutoresizingMaskIntoConstraints = false
            headerTitleView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            headerTitleView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            headerTitleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: style._switch == nil ? 0 : 40).isActive = true
            contentVerticalAlignment = .bottom
        }
				if let subTitleStyle = style.subTitle {
						subTitleView.font = subTitleStyle.font
						subTitleView.textColor = subTitleStyle.color
						subTitleView.textAlignment = subTitleStyle.alignment
						subTitleView.lineBreakMode = .byWordWrapping
						addSubview(subTitleView)
						subTitleView.translatesAutoresizingMaskIntoConstraints = false
						subTitleView.topAnchor.constraint(equalTo: headerTitleView.bottomAnchor, constant: 3).isActive = true
						subTitleView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
						subTitleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: style._switch == nil ? 0 : 40).isActive = true
				}
        if let switchStyle = style._switch {
            switchView = XSwitch(style: switchStyle)
            addSubview(switchView!)
            switchView!.translatesAutoresizingMaskIntoConstraints = false
            switchView!.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
            switchView!.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            switchView!.heightAnchor.constraint(equalToConstant: 18).isActive = true
            switchView!.widthAnchor.constraint(equalToConstant: 30).isActive = true
            switchView!.delegate = self
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        }
        contentHorizontalAlignment = style.title.alignment == .center ? .center : style.title.alignment == .left ? .left : .right
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        var attr: [NSAttributedString.Key : Any] = [ .foregroundColor: style.title.color ]
        if let font = style.title.font { attr[.font] = font }
        setAttributedTitle(NSAttributedString(string: title ?? "", attributes: attr), for: state)
        titleLabel?.numberOfLines = isMultiLine ? 0 : style.header == nil ? 0 : 1
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        isSelected = !isSelected
        onTapped?()
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
    }
    
    func switchStateChange(switch: XSwitch, isOn: Bool) {
        isSelected = isOn
        onSelected?(isSelected)
    }
}
