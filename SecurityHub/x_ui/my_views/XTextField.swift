//
//  XTextField.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XTextFieldStyle {
    let backgroundColor, unselectColor, selectColor, textColor: UIColor
    var errorColor: UIColor = UIColor.red
    let font: UIFont?
    var borderWidth: CGFloat = 1
}
extension XTextField {
    typealias Style = XTextFieldStyle
}

@objc protocol XTextFieldDelegete {
    @objc func xTextFieldShouldReturn(_ textField: UITextField)
		@objc optional func xTextFieldShouldBeginEditing(_ textField: UITextField)
		@objc optional func xTextFieldShouldEndEditing(_ textField: UITextField)
    @objc optional func xTextFieldDidChangeSelection(_ textField: UITextField)
		@objc optional func xTextField(	_ textField: UITextField,
			shouldChangeCharactersIn range: NSRange,
			replacementString string: String
	) -> Bool
}

class XTextField: UITextField, UITextFieldDelegate {
    public var xDelegate: XTextFieldDelegete?
    private var placeholderView: UILabel = UILabel()
    private var validationView: UILabel = UILabel()
		private var validationBottomView: UILabel = UILabel()
    private let _layer = CALayer()
    private var centerYAnchorConstraint, leadingAnchorConstraint, vCenterYAnchorConstraint, vLeadingAnchorConstraint, vbCenterYAnchorConstraint, vbLeadingAnchorConstraint, vbTrailingAnchorConstraint: NSLayoutConstraint?
    private let style: Style
    
    override var placeholder: String? {
        set {
            placeholderView.text = newValue
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 0.1)])
        }
        get { placeholderView.text }
    }

    init(style: Style){
        self.style = style
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (leftView == nil) {
            leftView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: frame.size.height))
            if (text ?? "").count > 0 {
                centerYAnchorConstraint?.constant = frame.size.height/2 * -1
                leadingAnchorConstraint?.constant = 14 - placeholderView.frame.size.width * 0.1
                self.placeholderView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                self.layoutIfNeeded()
            }
            vCenterYAnchorConstraint?.constant = frame.size.height/2
						vbCenterYAnchorConstraint?.constant = frame.size.height/2 + 10
            self.layoutIfNeeded()
        }
        if (rightView == nil) { rightView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: frame.size.height)) }
        _layer.frame = CGRect(x: 0, y: 0, width: frame.size.width , height: frame.size.height)
    }
		
    public func showValidationError(message: String? = nil) {
        validationView.text = message
        centerYAnchorConstraint?.constant = frame.size.height/2 * -1
        leadingAnchorConstraint?.constant = 14 - placeholderView.frame.size.width * 0.1
        UIView.animate(withDuration: 0.3, animations: {
            self.validationView.alpha = (self.validationView.text ?? "").isEmpty ? 0 : 1
            self._layer.borderColor = self.style.errorColor.cgColor
            self.placeholderView.textColor = self.style.errorColor
            self.placeholderView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.layoutIfNeeded()
        })
    }
	
		public func showValidationBottomError(message: String? = nil) {
				validationBottomView.text = message
				centerYAnchorConstraint?.constant = frame.size.height/2 * -1
				leadingAnchorConstraint?.constant = 14 - placeholderView.frame.size.width * 0.1
				UIView.animate(withDuration: 0.3, animations: {
						self.validationBottomView.alpha = (self.validationBottomView.text ?? "").isEmpty ? 0 : 1
						self._layer.borderColor = self.style.errorColor.cgColor
						self.placeholderView.textColor = self.style.errorColor
						self.placeholderView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
						self.layoutIfNeeded()
				})
		}
    
    func textFieldDidChangeSelection(_ textField: UITextField) { xDelegate?.xTextFieldDidChangeSelection?(textField) }
	
		func textField(_ textField: UITextField,
				shouldChangeCharactersIn range: NSRange,
				replacementString string: String
		) -> Bool
		{
			let res = xDelegate?.xTextField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
			
			return res
		}
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
				centerYAnchorConstraint?.constant = frame.size.height/2 * -1
				leadingAnchorConstraint?.constant = 14 - placeholderView.frame.size.width * 0.1
				UIView.animate(withDuration: 0.3, animations: {
						self.validationView.alpha = 0
						self.validationBottomView.alpha = 0
						self._layer.borderColor = self.style.selectColor.cgColor
						self.placeholderView.textColor = self.style.selectColor
						self.placeholderView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
						self.layoutIfNeeded()
				})
			
				xDelegate?.xTextFieldShouldBeginEditing?(textField)
			
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        xDelegate?.xTextFieldShouldReturn(textField)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
				UIView.animate(withDuration: 0.3, animations: {
						self._layer.borderColor = self.style.unselectColor.cgColor
						self.placeholderView.textColor = self.style.unselectColor
						if ((self.text ?? "").count == 0) {
								self.placeholderView.transform = CGAffineTransform(scaleX: 1, y: 1)
								self.centerYAnchorConstraint?.constant = 0
								self.leadingAnchorConstraint?.constant = 14
								self.layoutIfNeeded()
						}
				})
			
				xDelegate?.xTextFieldShouldEndEditing?(textField)
			
        return true
    }
        
    private func initViews() {
        delegate = self
        leftViewMode = .always
        rightViewMode = .always
        textColor = style.textColor
        font = style.font
        _layer.borderColor = style.unselectColor.cgColor
        _layer.borderWidth = style.borderWidth
        _layer.cornerRadius = 10
        layer.addSublayer(_layer)
        placeholderView.font = style.font
        placeholderView.backgroundColor = style.backgroundColor
        placeholderView.textColor = style.unselectColor
        addSubview(placeholderView)
        validationView.font = style.font?.withSize((style.font?.pointSize ?? 1) * 0.8)
        validationView.backgroundColor = style.backgroundColor
        validationView.textColor = style.errorColor
        validationView.alpha = 0
        addSubview(validationView)
				validationBottomView.font = style.font?.withSize((style.font?.pointSize ?? 1) * 0.8)
				validationBottomView.backgroundColor = style.backgroundColor
				validationBottomView.textColor = style.errorColor
				validationBottomView.alpha = 0
				validationBottomView.numberOfLines = 0
				validationBottomView.lineBreakMode = .byWordWrapping
				validationBottomView.textAlignment = .center
				addSubview(validationBottomView)
    }
    
    private func setConstraints() {
        placeholderView.translatesAutoresizingMaskIntoConstraints = false
        centerYAnchorConstraint = placeholderView.centerYAnchor.constraint(equalTo: centerYAnchor)
        leadingAnchorConstraint = placeholderView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14)
        NSLayoutConstraint.activate([ centerYAnchorConstraint!, leadingAnchorConstraint! ])

        validationView.translatesAutoresizingMaskIntoConstraints = false
        vCenterYAnchorConstraint = validationView.centerYAnchor.constraint(equalTo: centerYAnchor)
        vLeadingAnchorConstraint = validationView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14)
        NSLayoutConstraint.activate([ vCenterYAnchorConstraint!, vLeadingAnchorConstraint! ])
			
				validationBottomView.translatesAutoresizingMaskIntoConstraints = false
				vbCenterYAnchorConstraint = validationBottomView.centerYAnchor.constraint(equalTo: centerYAnchor)
				vbLeadingAnchorConstraint = validationBottomView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14)
				vbTrailingAnchorConstraint = validationBottomView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14)
				NSLayoutConstraint.activate([ vbCenterYAnchorConstraint!, vbLeadingAnchorConstraint!, vbTrailingAnchorConstraint! ])
    }
}
