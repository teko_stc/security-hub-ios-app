//
//  SiteView.swift
//  SecurityHub test
//
//  Created by Timerlan on 17.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class ObjectOpenView: UIView, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate {
    static let minHeight: CGFloat = 130
    static let buttonLineHeight: CGFloat = 44
    
    private var cont: UIView = { return UIView()}()
    private var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFit
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        return view
    }()
    private var smallIconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        return view
    }()
    private var locationView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 9)
        view.numberOfLines = 2
        return view
    }()
    private var titleView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        view.numberOfLines = 2
        return view
    }()
    private var infoView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 11)
        view.numberOfLines = 3
        view.text = ""
        return view
    }()
    private var statusView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        view.numberOfLines = 1
        return view
    }()
    private var timeView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.isHidden = true
        view.text = "--:--:--"
        return view
    }()
    private var buttonsView: UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        flow.minimumInteritemSpacing = 4
        flow.minimumLineSpacing = 6
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flow)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(ObjectOpenButtonCell.self, forCellWithReuseIdentifier: ObjectOpenButtonCell.cellId)
        return collectionView
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return buttons.count }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var w = 0;
        if buttons.count % 2 == 1 && indexPath.row == (buttons.count - 1) { w = Int(UIScreen.main.bounds.width) - 32 }
        else { w = (Int(UIScreen.main.bounds.width) - 32) / 2 - 4 }
        return CGSize(width: CGFloat(w), height: ObjectOpenView.buttonLineHeight - 4 )
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ObjectOpenButtonCell.cellId, for: indexPath) as! ObjectOpenButtonCell
        cell.setContent(buttons[indexPath.row])
        return cell
    }
    
    private var tableView : UITableView = {
        let view = UITableView()
        view.rowHeight = EventView.height
        view.isScrollEnabled = false
        view.separatorStyle = .none
        view.register(EventCell.self, forCellReuseIdentifier: EventCell.cellId)
        return view
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return events.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.cellId, for: indexPath) as! EventCell
        cell.setContent(events[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        affectClick?(events[indexPath.row].id)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        layer.borderWidth = 0.7
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(cont)
        cont.addSubview(iconView)
        cont.addSubview(smallIconView)
        cont.addSubview(locationView)
        cont.addSubview(titleView)
        cont.addSubview(infoView)
        cont.addSubview(statusView)
        cont.addSubview(timeView)
        cont.addSubview(buttonsView)
        addSubview(tableView)
    }

    override func updateConstraints() {
        super.updateConstraints()
        let buttonHeight = ceil(CGFloat(buttons.count) / 2) * ObjectOpenView.buttonLineHeight + 2
        cont.snp.remakeConstraints{ make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(ObjectOpenView.minHeight + buttonHeight)
        }
        iconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(24)
            make.left.equalTo(8)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        smallIconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(10)
            make.right.equalTo(iconView.snp.right).offset(10)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        locationView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(6)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        titleView.snp.remakeConstraints{ make in
            make.top.equalTo(locationView.snp.bottom).offset(2)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        infoView.snp.remakeConstraints{ make in
            make.top.equalTo(titleView.snp.bottom).offset(6)
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
        }
        timeView.snp.remakeConstraints{ make in
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalTo(timeView.snp.top).offset(-4)
        }
        statusView.snp.remakeConstraints{ make in
            make.left.equalTo(iconView.snp.right).offset(18)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalTo(buttonsView.snp.top).offset(-8)
        }
        buttonsView.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(buttonHeight)
            make.bottom.equalToSuperview().offset(-4)
        }
        tableView.snp.remakeConstraints{ make in
            make.top.equalTo(cont.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-8)
        }
    }
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}

    private var buttons: [XObjectEntity.Button] = []
    func set(title: String, info: String, status: String, time: String) {
        titleView.text = title
        infoView.text = info
        statusView.text = status
        timeView.text = time
    }
    
    func setLocation(object: String?, device: String?, section: String?) {
        var location: String?
        if let object = object {
            location = object
        }
        if let device = device, let _ = location {
            location! += " → \(device)"
        } else if let device = device {
            location = device
        }
        if let section = section, let _ = location {
            location! += " → \(section)"
        } else if let section = section {
            location = section
        }
        locationView.text = location ?? "   "
    }
    
    func setButtons(_ buttons: [XObjectEntity.Button]){
        buttonsView.dataSource = self
        buttonsView.delegate = self
        self.buttons = buttons
        buttonsView.reloadData()
        updateConstraints()
    }
    
    func setImage(_ imgMain: String, mainColor: UInt? = nil, imgSmall: String?) {
        if let mainColor = mainColor {
            iconView.image = UIImage(named: imgMain)?.withColor(color: UIColor.colorFromHex(mainColor))
        } else {
            iconView.image = UIImage(named: imgMain)
        }
        smallIconView.isHidden = imgSmall == nil
        if let small = imgSmall { smallIconView.image = UIImage(named: small) }
    }

    private var events: [XObjectEntity.Affect] = []
    private var affectClick: ((_ affect_id: Int64) -> Void)?
    func setAffects(_ affects: [XObjectEntity.Affect], affectClick: ((_ affect_id: Int64) -> Void)?){
        self.affectClick = affectClick
        tableView.dataSource = self
        tableView.delegate = self
        self.events = affects
        tableView.reloadData()
    }
}

class ObjectOpenButtonCell: UICollectionViewCell {
    static let cellId = "ObjectOpenButtonCell"
    private var o: XObjectEntity.Button!
    
    private lazy var btn: ZFRippleButton = {
        let view = ZFRippleButton()
        view.shadowRippleEnable = false
        view.ripplePercent = 1
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        view.layer.cornerRadius = 15
        view.addTarget(self, action: #selector(click), for: .touchUpInside)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(btn)
        btn.snp.makeConstraints { (make) in make.top.leading.trailing.bottom.equalToSuperview() }
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    func setContent(_ o : XObjectEntity.Button) {
        self.o = o
        btn.setTitle(o.title.uppercased(), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        btn.backgroundColor         = UIColor.colorFromHex(o.color)
        btn.rippleBackgroundColor   = UIColor.colorFromHex(o.color)
        btn.rippleColor = DEFAULT_SELECTED.withAlphaComponent(0.4)
    }
    
    @objc private func click(_ button: UIButton){  o.void(o.obj, o.value) }
}

