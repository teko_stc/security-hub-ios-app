import UIKit

struct XTextEditViewStyle {
    let background: UIColor
    let field: XTextField.Style
}

extension XTextEditView {
    typealias Style = XTextEditViewStyle
}
