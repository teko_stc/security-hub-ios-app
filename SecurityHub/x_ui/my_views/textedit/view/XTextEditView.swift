import UIKit

class XTextEditView: UIView {
    public var delegate: XTextEditViewDelegate?
    
    private var textFieldView: XTextField!
    
    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(style: Style) {
        backgroundColor = UIColor.white

        textFieldView = XTextField(style: style.field)
        textFieldView.xDelegate = self
        textFieldView.keyboardType = .default
        textFieldView.returnKeyType = .done
        addSubview(textFieldView)
    }
    
    private func setConstraints() {
        textFieldView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textFieldView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 5),
            textFieldView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 46),
            textFieldView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            textFieldView.heightAnchor.constraint(equalToConstant: 58)
        ])
    }
    
    public func setDefaultData(placeholder: String, value: String) {
        textFieldView.placeholder = placeholder
        textFieldView.text = value
        
        if !value.isEmpty {
            textFieldView.becomeFirstResponder()
        }
    }
    
    public func getValue() -> String? { return textFieldView.text }
    
    public func hideKeyboard() {
        textFieldView.resignFirstResponder()
    }
}

extension XTextEditView: XTextFieldDelegete {
    func xTextFieldShouldReturn(_ textField: UITextField) {
        delegate?.valueEndEditing(value: textField.text)
    }
}
