//
//  XTextEditViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XTextEditViewDelegate {
    func valueEndEditing(value: String?)
}
