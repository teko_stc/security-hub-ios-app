import UIKit

class XTextEditController: XBaseViewController<XTextEditView>, XTextEditViewDelegate {
    override var tabBarIsHidden: Bool { true }
        
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        rightViews: [
            XNavigationViewRightViewBuilder(title: strings.confirm, font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042), viewTapped: self.confirmViewTapped)
        ]
    )
    
    private var _title: String
    private var value: String
    private var onCompleted: (_ controller: UIViewController, _ value: String) -> Void
    
    private let strings = XTextEditControllerStrings()
    
    init(title: String, value: String, onCompleted: @escaping (_ controller: UIViewController, _ value: String) -> Void) {
        self._title = title
        self.value = value
        self.onCompleted = onCompleted
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainView.setDefaultData(placeholder: _title, value: value)
        mainView.delegate = self
    }
    
    func valueEndEditing(value: String?) {
        if (value ?? "").isEmpty { showErrorAlert() }
        else {
            mainView.hideKeyboard()
            
            onCompleted(self, value!)
        }
    }
    
    func showErrorAlert() {
        let alert = XAlertController(style: errorAlertStyle(),  strings: strings.errorAlertText)
        navigationController?.present(alert, animated: true)
    }
    
    func confirmViewTapped() { valueEndEditing(value: mainView.getValue()) }
}
