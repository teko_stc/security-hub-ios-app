import UIKit

extension XTextEditView {
    
    func style() -> Style {
        return Style(
            background: UIColor.white,
            field: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                selectColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                borderWidth: 0
            )
        )
    }
}

extension XTextEditController {
    func errorAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
