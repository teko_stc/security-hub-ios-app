class XTextEditControllerStrings {
    var confirm: String { "N_BUTTON_NEXT".localized() }
    
    var errorAlertText = XAlertView.Strings(
        title: "N_RENAME_MESSAGE_CHECK_ENTERED_DATA".localized(),
        text: nil,
        positive: "N_BUTTON_OK".localized(),
        negative: nil
    )
}
