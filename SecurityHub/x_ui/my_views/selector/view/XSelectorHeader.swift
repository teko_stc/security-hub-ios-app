//
//  XBaseSelectorHeader.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 06.07.2021.
//

import UIKit

class XSelectorViewHeader: UITableViewHeaderFooterView {
    static let identifier = "XSelectorHeader.identifier"
    
    private let titleView = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setContent(title: String) {
        titleView.text = title
    }
    
    public func setStyle(_ style: Style) {
        contentView.backgroundColor = style.backgroundColor
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        setConstraints(style)
    }
    
    private func initViews() {
        addSubview(titleView)
    }
    
    private func setConstraints(_ style: Style) {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: style.title.margins.top),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: style.title.margins.left),
            titleView.heightAnchor.constraint(equalToConstant: (style.title.font?.pointSize ?? 1) * 1.5),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -style.title.margins.right),
            titleView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -style.title.margins.bottom)
        ])
    }
}

extension XSelectorView {
    typealias Header = XSelectorViewHeader
}
