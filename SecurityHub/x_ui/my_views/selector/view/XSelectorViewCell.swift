//
//  XBaseSelectorCell.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 06.07.2021.
//

import UIKit

class XSelectorViewCell: UITableViewCell  {
    
    static let identifier = "XSelectorViewCell.identifier"
    
    private let iconView = UIImageView()
    private let titleView = UILabel()
    private let subtitleView = UILabel()
    private let backView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(imageName: String, text: String, subtitle: String? = nil) {
        iconView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        titleView.text = text
        subtitleView.text = subtitle
    }
    
    public func setStyle(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        iconView.tintColor = style.icon.color
        
        titleView.textColor = style.title.color
        titleView.font = style.title.font
        titleView.numberOfLines = 0
        
        if let subtitle = style.subtitle {
            subtitleView.textColor = subtitle.color
            subtitleView.font = subtitle.font
            subtitleView.numberOfLines = 0
        }
        
        backView.backgroundColor = style.selectedBackgroundColor
        
        setConstraints(style)
    }
    
    private func initViews() {
        selectedBackgroundView = backView
        backView.layer.cornerRadius = 5
        
        contentView.addSubview(iconView)
        contentView.addSubview(titleView)
        contentView.addSubview(subtitleView)
    }
    
    private func setConstraints(_ style: Style) {
        [iconView, titleView, subtitleView].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: style.icon.size.width),
            iconView.heightAnchor.constraint(equalToConstant: style.icon.size.height),
            iconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: style.icon.margins.top),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: style.icon.margins.left),
            iconView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -style.icon.margins.bottom),

            titleView.topAnchor.constraint(equalTo: iconView.centerYAnchor, constant: -(style.title.font?.pointSize ?? 1) * 1.5 / 2),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: style.title.margins.left + style.icon.margins.right),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -style.title.margins.right)
        ])
        
        if let subtitle = style.subtitle {
            NSLayoutConstraint.activate([
                subtitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: subtitle.margins.top),
                subtitleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: subtitle.margins.left),
                subtitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -subtitle.margins.right),
                subtitleView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -subtitle.margins.bottom)
            ])
        } else {
            titleView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -style.title.margins.bottom).isActive = true
        }
    }
}

extension XSelectorView {
    typealias Cell = XSelectorViewCell
}
