//
//  XBaseSelectorController.swift
//  tabviewcontroller
//
//  Created by Тимерлан Рахматуллин on 01.07.2021.
//

import UIKit

class XSelectorView: UIView {
    private var headerStyle: Header.Style
    private var cellStyle: Cell.Style
    private var sections: [Section] = []
    private var tableView = UITableView(frame: .zero, style: .grouped)
    private var sectionFooterHeight: CGFloat
        
    override var backgroundColor: UIColor? {
        didSet {
            tableView.backgroundColor = backgroundColor
        }
    }
    
    init(headerStyle: Header.Style, cellStyle: Cell.Style, sectionFooterHeight: CGFloat = 0) {
        self.headerStyle = headerStyle
        self.cellStyle = cellStyle
        self.sectionFooterHeight = sectionFooterHeight
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func reloadData(_ sections: [Section] = []) {
        self.sections = sections
        self.tableView.reloadData()
    }
    
    private func initViews() {
//        tableView.bounces = false
//        tableView.alwaysBounceVertical = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.register(Header.self, forHeaderFooterViewReuseIdentifier: Header.identifier)
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        addSubview(tableView)
    }
    
    private func setConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1),
            tableView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1)
        ])
    }
}

extension XSelectorView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: Header.identifier) as! Header
        header.setStyle(headerStyle)
        header.setContent(title: sections[section].title)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        let item = sections[indexPath.section].items[indexPath.row]
        
        cell.setStyle(item.customStyle != nil ? item.customStyle! : cellStyle)
        cell.setContent(imageName: item.imageName, text: item.title, subtitle: item.subtitle)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerStyle.title.margins.top + headerStyle.title.margins.bottom + (headerStyle.title.font?.pointSize ?? 15.5) * 1.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? { UIView() }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { section == sections.count - 1 ? sectionFooterHeight : 0 }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return cellStyle.icon.margins.top + cellStyle.icon.margins.bottom + cellStyle.icon.size.height
//    }
}

extension XSelectorView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        sections[indexPath.section].items[indexPath.row].click?()
    }
}
