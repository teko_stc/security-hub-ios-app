//
//  XSelectorCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 07.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSelectorViewCellStyle {
    struct Label {
        let font: UIFont?
        let color: UIColor
        let margins: UIEdgeInsets
    }
    
    struct Icon {
        let color: UIColor
        let size: CGSize
        let margins: UIEdgeInsets
    }
    
    let backgroundColor: UIColor
    let selectedBackgroundColor: UIColor
    let title: Label
    var subtitle: Label? = nil
    let icon: Icon
}

extension XSelectorViewCell {
    typealias Style = XSelectorViewCellStyle
}
