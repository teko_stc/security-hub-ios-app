//
//  XSelectorViewSection.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XSelectorViewSection {
    var title: String
    var items: [Item]
    
    struct Item {
        var title: String
        var subtitle: String? = nil
        var imageName: String
        var click: (() -> ())?
        var customStyle: XSelectorViewCellStyle?
    }
}

extension XSelectorView {
    typealias Section = XSelectorViewSection
}
