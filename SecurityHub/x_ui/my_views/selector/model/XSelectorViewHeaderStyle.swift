//
//  XSelectorViewHeaderStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 08.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSelectorViewHeaderStyle {
    struct Title {
        let font: UIFont?
        let color: UIColor
        let margins: UIEdgeInsets
    }
    
    let backgroundColor: UIColor
    let title: Title
}

extension XSelectorViewHeader {
    typealias Style = XSelectorViewHeaderStyle
}

