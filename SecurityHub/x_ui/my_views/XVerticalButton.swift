//
//  XVerticalButton.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XVerticalButtonStyle {
    let font: UIFont?
    let color: UIColor?
}
extension XVerticalButton {
    typealias Style = XVerticalButtonStyle
}

class XVerticalButton: UIView {
    private var containerView: UIView = UIView()
    private var iconView: UIImageView = UIImageView()
    private var titleView: UILabel    = UILabel()
    
    public var title: String? {
        didSet {
            titleView.text = title
        }
    }
    
    public var image: UIImage? {
        didSet {
            iconView.image = image
        }
    }
    
    public var font: UIFont? {
        didSet {
             titleView.font = font
        }
    }
    
    public override var tintColor: UIColor! {
        didSet {
            iconView.tintColor  = tintColor
            titleView.textColor = tintColor
        }
    }
    
    public var onTapped: (() -> ())?
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setStyle(_ style: Style) {
        tintColor = style.color
        font = style.font
    }
    
    private func initViews() {
        containerView.isUserInteractionEnabled = true
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
        containerView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longPressed)))
        addSubview(containerView)
        
        iconView.contentMode = .scaleAspectFit
        containerView.addSubview(iconView)
        
        titleView.textAlignment = .center
        containerView.addSubview(titleView)
    }
    
    private func setConstraints() {
        [containerView, iconView, titleView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.heightAnchor.constraint(equalTo: heightAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor),

            iconView.topAnchor.constraint(equalTo: containerView.topAnchor),
            iconView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            iconView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.7),
            iconView.widthAnchor.constraint(equalTo: iconView.heightAnchor),

            titleView.topAnchor.constraint(equalTo: iconView.bottomAnchor),
            titleView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            titleView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 4),
            titleView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -4),
        ])
    }
    
    @objc private func tapped(_ sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.containerView.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            self.onTapped?()
        }
    }
    
    @objc private func longPressed(_ sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.containerView.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                self.onTapped?()
            })
        }
    }
}
