//
//  File.swift
//  SecurityHub
//
//  Created by Timerlan on 26.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XSpinerView: UIView {
    private var charSize: Int { get { return isCal() ? 13 : 16 } }
    public var title: String? {
        didSet {
            let text = String(title?.prefix(charSize) ?? "") + (title?.count ?? 0 > charSize ? ".." : "")
            let attr = [ NSAttributedString.Key.font :  UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.semibold)]
            titleView.setAttributedTitle(NSAttributedString(string: text, attributes: attr), for: .normal)
        }
    }
    public var isOpen = false {
        didSet { if (oldValue != isOpen) { spin() } }
    }
    public var items: [XSpinerItem]!{
        didSet{ initItems() }
    }
    public var onSpin: ((_ item: XSpinerItem)->Void)?
    
    private var rootView: UIView
    private var itemViews: [XSpinerItemView] = []
    private lazy var titleView: UIButton = {
        let view = UIButton()
        view.setTitleColor(UIColor.black, for: .normal)
        return view
    }()
    private lazy var arrowView: UIButton = {
        let view = UIButton()
        view.setImage(XImages.view_arrow_bottom?.withColor(color: UIColor.black), for: .normal)
        return view
    }()
    private lazy var spinButtonView: UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(titleView_Up), for: UIControl.Event.touchUpInside)
        view.addTarget(self, action: #selector(titleView_Down), for: UIControl.Event.touchDown)
        return view
    }()
    private var contView: UIStackView = {
        let view = UIStackView()
        view.isHidden = true
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.alignment = .fill
        return view
    }()
    private lazy var altContView: UIStackView = {
        let view = UIStackView()
        view.isHidden = true
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.alignment = .fill
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        view.layer.shadowRadius = 2
        return view
    }()
    private var contShadowLayer: CALayer?

    private lazy var altButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.backgroundColor = UIColor.clear
        view.rippleBackgroundColor = DEFAULT_COLOR_SELECT
        view.rippleColor = UIColor.clear
        view.alpha = 0.1
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.addTarget(self, action: #selector(altButton_Click), for: UIControl.Event.touchUpInside)
        return view
    }()
    @objc private func altButton_Click(){ isOpen = false }
    
    init(rootView: UIView) {
        self.rootView = rootView
        super.init(frame: CGRect())
        addSubview(titleView)
        addSubview(arrowView)
        addSubview(spinButtonView)
        rootView.addSubview(contView)
        rootView.addSubview(altContView)
        setConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setConstraints() {
        titleView.snp.remakeConstraints { (make) in
            make.top.equalTo(5.5)
            make.centerX.equalToSuperview()
        }
        arrowView.snp.remakeConstraints{ (make) in
            make.left.equalTo(titleView.snp.right).offset(4)
            make.centerY.equalTo(titleView.snp.centerY).offset(1)
            make.height.width.equalTo(16)
        }
        contView.snp.remakeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(0)
            make.top.equalTo(0)
        }
        altContView.snp.remakeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalTo(contView.snp.bottom)
            make.bottom.equalTo(0)
        }
        spinButtonView.snp.remakeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(titleView.snp.width).offset(40)
            make.center.equalToSuperview()
        }
        
        altButton.snp.remakeConstraints { (make) in make.height.equalTo(60) }
        altContView.addArrangedSubview(altButton)
    }
    
    @objc private func titleView_Up(){
        isOpen = !isOpen
        UIView.transition(with: self, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.arrowView.alpha = 1
            self.titleView.alpha = 1
        })
    }
    @objc private func titleView_Down(){
        UIView.transition(with: self, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.arrowView.alpha = 0.8
            self.titleView.alpha = 0.8
        })
    }
    
    private func spin(){
        if items.count == 1 { return }
        UIView.transition(with: self, duration: 0.3, options: .transitionCrossDissolve, animations: {
            let img = (!self.isOpen ? XImages.view_arrow_bottom?.withColor(color: UIColor.black) : XImages.view_arrow_top)?
                .withColor(color: UIColor.black)
            self.arrowView.setImage(img, for: .normal)
        })
        rootView.endEditing(true)
        contView.isHidden = !isOpen
        altContView.isHidden = !isOpen
        let height: CGFloat = isOpen ? (CGFloat(items.count * XSpinerItemView.HEIGHT)) : 0
        contView.snp.updateConstraints { (make) in make.height.equalTo(height) }
        contShadowLayer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height)
        UIView.transition(with: rootView, duration: (self.isOpen ? 0.15 : 0.075), options: .curveLinear, animations: {
            self.rootView.layoutIfNeeded()
        })
    }
    
    private func initItems(){
        itemViews.forEach { $0.removeFromSuperview() }
        itemViews = []
        contShadowLayer = contView.addBackground(color: UIColor.colorFromHex(0xfefefe))
        for i in 0...items.count-1 {
            let itemView = XSpinerItemView(item: items[i], select: select, isEnd: i == items.count-1)
            itemViews.append(itemView)
            itemView.snp.remakeConstraints({ (make) in make.height.equalTo(XSpinerItemView.HEIGHT) })
            contView.addArrangedSubview(itemView)
        }
        itemViews.first?.isSelected = true
        title = items.first?.title
        if items.count < 2 {
            titleView.isEnabled = false
            arrowView.isHidden = true
            titleView.snp.updateConstraints { (make) in make.centerX.equalToSuperview() }
        }
    }
    
    private func select(_ view: XSpinerItemView){
        isOpen = false
        if itemViews.first(where: { (_view) -> Bool in return _view.isSelected })?.item.title == view.item.title { return }
        itemViews.forEach { (view) in view.isSelected = false }
        view.isSelected = true
        title = view.item.title
        onSpin?(view.item)
    }
    
    private func seHidden(_ isHidden: Bool){
        titleView.isHidden = isHidden
    }
}

struct XSpinerItem {
    var image: UIImage?
    var title: String
    var subTitle: String
}

class XSpinerItemView: UIView {
    static let HEIGHT = 50

    public var isSelected: Bool = false {
        didSet{ checkView.isHidden = !isSelected }
    }
    public let item: XSpinerItem
    private let isEnd: Bool
    private let select: ((_ view: XSpinerItemView) -> Void)
    private lazy var imageView = UIImageView()
    private lazy var titleView: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_TEXT_DARK
        return view
    }()
    private lazy var checkView: UIImageView = {
        let view = UIImageView()
        view.image = XImages.view_check?.withColor(color: DEFAULT_COLOR_MAIN)
        view.isHidden = true
        return view
    }()
    private lazy var buttonView: ZFRippleButton = {
        let lineLayer = CALayer()
        let margin = XSizes.MARGIN_STANDART * 2 + XSizes.ICON_SMALL
        lineLayer.frame = CGRect(x: margin, y: margin,
            width: UIScreen.main.bounds.width - margin, height: 1)
        lineLayer.backgroundColor = DEFAULT_COLOR_MAIN.cgColor
        let view = ZFRippleButton()
        view.backgroundColor = UIColor.clear
        view.rippleBackgroundColor = DEFAULT_COLOR_SELECT
        view.rippleColor = UIColor.clear
        view.alpha = 0.2
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.addTarget(self, action: #selector(button_Click), for: UIControl.Event.touchUpInside)
        if (!isEnd) { view.layer.addSublayer(lineLayer) }
        return view
    }()
    
    init(item: XSpinerItem, select: @escaping ((_ view: XSpinerItemView)->Void), isEnd: Bool = false) {
        self.item = item
        self.select = select
        self.isEnd = isEnd
        super.init(frame: CGRect())
        addSubview(buttonView)
        addSubview(imageView)
        addSubview(titleView)
        addSubview(checkView)
        updateConstraints()
        
        imageView.image = item.image
        titleView.text = item.title
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        imageView.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(XSizes.MARGIN_STANDART)
            make.height.width.equalTo(XSizes.ICON_SMALL)
        }
        titleView.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(imageView.snp.right).offset(XSizes.MARGIN_STANDART)
        }
        checkView.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.width.equalTo(XSizes.ICON_SMALL)
            make.left.equalTo(titleView.snp.right).offset(XSizes.MARGIN_STANDART)
            make.right.equalTo(-XSizes.MARGIN_STANDART)
        }
        buttonView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    @objc private func button_Click(){
        select(self)
    }
}
