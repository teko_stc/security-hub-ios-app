//
//  XDeviceBottomListView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XDeviceBottomListView: UIView {
    enum UpdateType { case insert, update, delete }
    
    public weak var delegate: XDeviceBottomListViewDelegate?
    
    private lazy var strings: XDeviceBottomListViewStrings = XDeviceBottomListViewStrings()
    
    private var tableView: UICollectionView!, tableViewHeightAnchor: NSLayoutConstraint!
    private var items: [Cell.Model] = []
    private var headers: [Header.Model] = []
    private var cellStyle: Cell.Style, headerStyle: Header.Style
    private var isReady = false

    init(cellStyle: Cell.Style, headerStyle: Header.Style) {
        self.cellStyle = cellStyle
        self.headerStyle = headerStyle
        super.init(frame: .zero)
        
        let flowLayout = XDeviceBottomListViewFlowLayout()
        flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 80)
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width / 2, height: 130)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        tableView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        tableView.backgroundColor = cellStyle.backgroundColor
        tableView.register(Header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier)
        tableView.register(Cell.self, forCellWithReuseIdentifier: Cell.identifier)
        tableView.register(EmptyCell.self, forCellWithReuseIdentifier: EmptyCell.identifier)
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.addObserver(self, forKeyPath: "contentSize", options: [.new, .old, .prior], context: nil)
        addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeightAnchor = tableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableViewHeightAnchor
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func layoutSubviews() {
        super.layoutSubviews()
        isReady = true
    }
    
    @objc override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            tableViewHeightAnchor.constant = tableView.contentSize.height
            layoutIfNeeded()
        }
    }
    
    // TODO: Подумать
    public func updHeader(model: Header.Model, type: UpdateType) {
        switch type {
        case .insert:
            var max_j = 0
            if (headers.count != 0) { for j in 0...headers.count-1 { if model.id > headers[j].id { max_j = j + 1 } } }
            headers.insert(model, at: max_j)
            if isReady { eTableUpdateEndDetecting() } //TODO
            break;
        case .update:
            if let index = headers.firstIndex(where: { $0.id == model.id }) {
                if (headers[index].icon == model.icon && headers[index].name == model.name && headers[index].action == model.action) { return }
                headers[index] = model
//                tableView.reloadSections([index])
                eTableUpdateEndDetecting()
            }
        case .delete:
            if let index = headers.firstIndex(where: { $0.id == model.id }) {
                headers.remove(at: index)
//                tableView.deleteSections([index])
                eTableUpdateEndDetecting()
            }
            break;
        }
    }
    // TODO: Подумать
    public func updItem(model: Cell.Model, type: UpdateType) {
        switch type {
        case .insert:
            var max_j = 0
            if (items.count != 0) { for j in 0...items.count-1 { if model.id > items[j].id { max_j = j + 1 } } }
            items.insert(model, at: max_j)
            if isReady { eTableUpdateEndDetecting() } //TODO
            break;
        case .update:
            if let index = items.firstIndex(where: { $0.id == model.id && $0.headerId == model.headerId }) {
                items[index] = model
//                if let section = headers.firstIndex(where: { $0.id == model.headerId }), let row = items.filter({ $0.headerId == model.headerId }).firstIndex(where: { $0.id == model.id }) {
//                    tableView.reloadItems(at: [IndexPath(row: row, section: section)])
//                    _ = row
//                    tableView.reloadSections([section])
                    eTableUpdateEndDetecting()
//                }
            }
        case .delete:
            if let index = items.firstIndex(where: { $0.id == model.id && $0.headerId == model.headerId }) {
                items.remove(at: index)
//                if let section = headers.firstIndex(where: { $0.id == model.headerId }), let row = items.filter({ $0.headerId == model.headerId }).firstIndex(where: { $0.id == model.id }) {
//                    tableView.deleteItems(at: [IndexPath(row: row, section: section)])
//                    _ = row
//                    tableView.reloadSections([section])
                    eTableUpdateEndDetecting()
//                }
            }
        }
    }

    private var eTUDisp: Disposable?
    private var time: Double?
    private func eTableUpdateEndDetecting() {
        if time == nil {
            time = NSDate().timeIntervalSince1970
        }
        if let t = time, Int(NSDate().timeIntervalSince1970 - t) == 1 {
            tableView.reloadData()
        }
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: ThreadUtil.shared.backScheduler)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { [weak self] _ in
                self?.tableView.reloadData()
                self?.eTUDisp = nil
            }, onError: { [weak self] _ in
                self?.eTUDisp = nil
            })
    }
}

extension XDeviceBottomListView: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int { return headers.count }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Header.identifier, for: indexPath) as! Header
        headerView.setStyle(headerStyle)
        if (headers[indexPath.section].id == 0) { headerView.setContent(model: Header.Model(id: 0, icon: "ic_nil", name: "", action: "ic_nil")) }
        else {
            headerView.setContent(model: headers[indexPath.section])
            headerView.setDelegate(onViewTapped: onHeaderViewTapped, onActionViewTapped: onHeaderActionViewTapped)
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { max(items.filter({ $0.headerId == headers[section].id }).count, 1) }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (items.filter({ $0.headerId == headers[indexPath.section].id }).count == 0) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EmptyCell.identifier, for: indexPath) as! EmptyCell
            cell.setStyle(style: cellStyle)
            cell.setContent(title: headers[indexPath.section].id > 0 ? strings.emptySection : "")
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath) as! Cell
        let items = items.filter({ $0.headerId == headers[indexPath.section].id })
        cell.setStyle(style: cellStyle)
        cell.setContent(model: items[indexPath.row])
        cell.setDelegate(onViewTapped: onItemViewTapped)
        return cell
    }
        
    private func onItemViewTapped(model: Cell.Model) { delegate?.zoneViewTapped(section: model.headerId, zone: model.id) }
    
    private func onHeaderViewTapped(model: Header.Model) { delegate?.sectionViewTapped(section: model.id) }
    
    private func onHeaderActionViewTapped(model: Header.Model) { delegate?.sectionActionViewTapped(section: model.id) }
}

class XDeviceBottomListViewFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect)->[UICollectionViewLayoutAttributes]? {
        guard let att = super.layoutAttributesForElements(in: rect) else { return [] }
        var x: CGFloat = sectionInset.left
        var y: CGFloat = -1.0
        
        for a in att {
            if a.representedElementCategory != .cell { continue }
            
            if a.frame.origin.y >= y { x = sectionInset.left }
            
            a.frame.origin.x = x
            x += a.frame.width + minimumInteritemSpacing
            
            y = a.frame.maxY
        }
        return att
    }
}
