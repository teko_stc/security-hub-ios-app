//
//  XDeviceBottomListViewHeader.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//


import UIKit

class XDeviceBottomListViewHeader: UICollectionReusableView {
    static let identifier = "XDeviceBottomListView.identifier"
    
    private var baseView = UIButton(), ButtonObserverContext = 1
    private let titleView = UILabel()
    private let iconView = UIImageView(), actionView = UIImageView()
    private var style: Style!
    
    private var model: Model?, onViewTapped: ((Model) -> ())?, onActionViewTapped: ((Model) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        baseView.layer.cornerRadius = 10
        baseView.addObserver(self, forKeyPath: #keyPath(UIButton.isHighlighted), options: [.old, .new], context: &ButtonObserverContext)
        baseView.addTarget(self, action: #selector(baseViewTapped), for: .touchUpInside)
        addSubview(baseView)
        iconView.contentMode = .scaleAspectFit
        baseView.addSubview(iconView)
        baseView.addSubview(titleView)
        actionView.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(tabGesture))
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGesture))
        actionView.isUserInteractionEnabled = true
        actionView.addGestureRecognizer(tap)
        actionView.transform = CGAffineTransform.identity
        actionView.addGestureRecognizer(long)
        baseView.addSubview(actionView)
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: Model) {
        self.model = model
        iconView.image = UIImage(named: model.icon)?.withRenderingMode(.alwaysTemplate)
        titleView.text = model.name
        actionView.image = UIImage(named: model.action ?? "ic_nil")
    }
    
    public func setStyle(_ style: Style) {
        self.style = style
        backgroundColor = style.background
        baseView.backgroundColor = style.background
        titleView.font = style.font
        titleView.textColor = style.tint
        iconView.tintColor = style.tint
    }
    
    public func setDelegate(onViewTapped: ((Model) -> ())?, onActionViewTapped: ((Model) -> ())?) {
        self.onViewTapped = onViewTapped
        self.onActionViewTapped = onActionViewTapped
    }
    
    private func setConstraints() {
        [baseView, iconView, titleView, actionView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            baseView.topAnchor.constraint(equalTo: topAnchor),
            baseView.leadingAnchor.constraint(equalTo: leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: bottomAnchor),

            iconView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            iconView.centerYAnchor.constraint(equalTo: baseView.centerYAnchor),
            iconView.heightAnchor.constraint(equalToConstant: 36),
            iconView.widthAnchor.constraint(equalToConstant: 36),

            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 20),
            titleView.centerYAnchor.constraint(equalTo: baseView.centerYAnchor),
            
            actionView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 20),
            actionView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),
            actionView.centerYAnchor.constraint(equalTo: baseView.centerYAnchor),
            actionView.heightAnchor.constraint(equalToConstant: 36),
            actionView.widthAnchor.constraint(equalToConstant: 36),
        ])
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &ButtonObserverContext && keyPath == #keyPath(UIButton.isHighlighted), let isHighlighted = change?[NSKeyValueChangeKey.newKey] as? Bool  {
            if (onViewTapped == nil) { return }
            if (!isHighlighted) { baseView.backgroundColor = style.background } else { baseView.backgroundColor = style.selected }
        }
    }
    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: { self.actionView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1) })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: { self.actionView.transform = CGAffineTransform.identity })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.onActionViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: { self.actionView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1) })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: { self.actionView.transform = CGAffineTransform.identity }) { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.onActionViewTapped?(model) }
            }
        }
    }
    
    @objc private func baseViewTapped() {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        if let model = self.model { self.onViewTapped?(model) }
    }
}
struct XDeviceBottomListViewHeaderModel {
    let id: Int
    let icon, name: String
    let action: String?
}
struct XDeviceBottomListViewHeaderStyle {
    let background, selected, tint: UIColor
    let font: UIFont?
}
extension XDeviceBottomListViewHeader {
    typealias Model = XDeviceBottomListViewHeaderModel
    typealias Style = XDeviceBottomListViewHeaderStyle
}
extension XDeviceBottomListView {
    typealias Header = XDeviceBottomListViewHeader
}
