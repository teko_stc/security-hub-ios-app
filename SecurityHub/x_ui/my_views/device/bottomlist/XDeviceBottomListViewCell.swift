//
//  XDeviceBottomListViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceBottomListViewCellModel {
    let id, headerId: Int
    let title, iconName: String
    let leftTopState, rightTopState, rightBottomState: String?
    let tempState: String?
}
struct XDeviceBottomListViewCellStyle {
    let backgroundColor, tintColor: UIColor
    let titleFont, tempFont: UIFont?
}
extension XDeviceBottomListViewCell {
    typealias Style = XDeviceBottomListViewCellStyle
    typealias Model = XDeviceBottomListViewCellModel
}

class XDeviceBottomListViewCell: UICollectionViewCell {
    static let identifier: String = "XDeviceBottomListViewCell.identifier"
    
    private let baseView = UIView()
    private let titleView = UILabel(), tempView = UILabel()
    private let iconView = UIImageView(), leftTopStateView = UIImageView(), rightTopStateView = UIImageView(), rightBottomStateView = UIImageView()
    
    private var model: Model?, onViewTapped: ((Model) -> ())?

    override init(frame: CGRect) {
        super.init(frame: frame)
        selectedBackgroundView = UIView()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tabGesture))
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGesture))
        baseView.isUserInteractionEnabled = true
        baseView.addGestureRecognizer(tap)
        baseView.transform = CGAffineTransform.identity
        baseView.addGestureRecognizer(long)
        contentView.addSubview(baseView)
        iconView.contentMode = .scaleAspectFit
        baseView.addSubview(iconView)
        baseView.addSubview(titleView)
        leftTopStateView.contentMode = .scaleAspectFit
        baseView.addSubview(leftTopStateView)
        baseView.addSubview(rightTopStateView)
        baseView.addSubview(rightBottomStateView)
        baseView.addSubview(tempView)
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: Model) {
        self.model = model
        titleView.text = model.title
        iconView.image = UIImage(named: model.iconName)
        leftTopStateView.image = UIImage(named: model.leftTopState ?? "ic_nil")
        rightTopStateView.image = UIImage(named: model.rightTopState ?? "ic_nil")
        if let temp = model.tempState { tempView.text = "\(temp)°"; rightBottomStateView.image = UIImage(named: "ic_nil") }
        else { rightBottomStateView.image = UIImage(named: model.rightBottomState ?? "ic_nil"); tempView.text = nil  }
    }
    
    public func setStyle(style: Style) {
        baseView.backgroundColor = style.backgroundColor
        
        titleView.font = style.titleFont
        titleView.textColor = style.tintColor
        titleView.textAlignment = .center
        titleView.numberOfLines = 0
        
        tempView.font = style.tempFont
        tempView.textColor = style.tintColor
        tempView.numberOfLines = 0
        tempView.backgroundColor = style.backgroundColor
        tempView.textAlignment = .center
    }
    
    public func setDelegate(onViewTapped: @escaping (Model) -> ()) {
        self.onViewTapped = onViewTapped
    }

    private func setConstraints() {
        [baseView, titleView, tempView, iconView, leftTopStateView, rightTopStateView, rightBottomStateView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            baseView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            baseView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            baseView.topAnchor.constraint(equalTo: contentView.topAnchor),
            baseView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
//            baseView.heightAnchor.constraint(equalToConstant: 130),
                
            iconView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 4),
            iconView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 4),
            iconView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -4),
            iconView.heightAnchor.constraint(equalToConstant: 80),
            iconView.centerXAnchor.constraint(equalTo: baseView.centerXAnchor),

            titleView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 6),
            titleView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 4),
            titleView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -4),
            titleView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 2 - 10),
            titleView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -10),
            
            leftTopStateView.topAnchor.constraint(equalTo: iconView.topAnchor, constant: 6),
            leftTopStateView.leadingAnchor.constraint(equalTo: iconView.centerXAnchor, constant: -35),
            leftTopStateView.heightAnchor.constraint(equalToConstant: 30),
            leftTopStateView.widthAnchor.constraint(equalToConstant: 30),
               
            rightTopStateView.topAnchor.constraint(equalTo: iconView.topAnchor, constant: 6),
            rightTopStateView.trailingAnchor.constraint(equalTo: iconView.centerXAnchor, constant: 35),
            rightTopStateView.heightAnchor.constraint(equalToConstant: 30),
            rightTopStateView.widthAnchor.constraint(equalToConstant: 30),
            
            rightBottomStateView.bottomAnchor.constraint(equalTo: iconView.bottomAnchor, constant: -6),
            rightBottomStateView.trailingAnchor.constraint(equalTo: iconView.centerXAnchor, constant: 35),
            rightBottomStateView.heightAnchor.constraint(equalToConstant: 30),
            rightBottomStateView.widthAnchor.constraint(equalToConstant: 30),
             
            tempView.bottomAnchor.constraint(equalTo: iconView.bottomAnchor, constant: -6),
            tempView.trailingAnchor.constraint(equalTo: iconView.centerXAnchor, constant: 35),
            tempView.widthAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: { self.baseView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1) })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: { self.baseView.transform = CGAffineTransform.identity })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.onViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: { self.baseView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1) })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: { self.baseView.transform = CGAffineTransform.identity }) { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.onViewTapped?(model) }
            }
        }
    }
}
extension XDeviceBottomListView {
    typealias Cell = XDeviceBottomListViewCell
    typealias EmptyCell = XDeviceBottomListViewEmptyCell
}

class XDeviceBottomListViewEmptyCell: UICollectionViewCell {
    static let identifier: String = "XDeviceBottomListViewEmptyCell.identifier"
    
    private let titleView = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        selectedBackgroundView = UIView()
        addSubview(titleView)
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(title: String) {
        titleView.text = title
    }
    
    public func setStyle(style: XDeviceBottomListView.Cell.Style) {
        backgroundColor = style.backgroundColor
        
        titleView.font = style.titleFont
        titleView.textColor = style.tintColor
        titleView.textAlignment = .center
        titleView.numberOfLines = 0
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            titleView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 20),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -6),
        ])
    }
}
