//
//  XDeviceBottomListViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 16.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XDeviceBottomListViewStrings {
    
    /// Раздел пуст
    var emptySection: String { "N_SECTIONS_GRID_EMPTY".localized() }
}
