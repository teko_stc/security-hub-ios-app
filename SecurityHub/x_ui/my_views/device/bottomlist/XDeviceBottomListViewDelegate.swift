//
//  XDeviceBottomListViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 17.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XDeviceBottomListViewDelegate: AnyObject {
    func zoneViewTapped(section: Int, zone: Int)
    func sectionViewTapped(section: Int)
    func sectionActionViewTapped(section: Int)
}
