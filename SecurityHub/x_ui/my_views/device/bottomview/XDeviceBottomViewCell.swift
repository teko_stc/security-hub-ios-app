//
//  XDeviceBottomViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XDeviceBottomViewCellModel {
    let title: String?
    let text: String
    let iconName: String?
}
struct XDeviceBottomViewCellStyle {
    let backgroundColor, tintColor: UIColor
    let title, text: XBaseLableStyle
}
extension XDeviceBottomViewCell {
    typealias Style = XDeviceBottomViewCellStyle
    typealias Model = XDeviceBottomViewCellModel
}

class XDeviceBottomViewCell: UITableViewCell {
    static let identifier: String = "XDeviceBottomViewCell.identifier"
    
    private let titleView = UILabel(), textView = UILabel(), iconView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()
        contentView.addSubview(titleView)
        contentView.addSubview(textView)
        contentView.addSubview(iconView)
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: Model) {
        titleView.text = model.title
        textView.text = model.text
        iconView.image = UIImage(named: model.iconName ?? "ic_nil")?.withRenderingMode(.alwaysTemplate)
    }
    
    public func setStyle(style: Style) {
        backgroundColor = style.backgroundColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        titleView.numberOfLines = 0
        
        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.textAlignment = style.text.alignment
        textView.numberOfLines = 0
        
        iconView.tintColor = style.tintColor
    }

    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        iconView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            iconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            iconView.leadingAnchor.constraint(equalTo: textView.trailingAnchor, constant: 10),
            iconView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            iconView.heightAnchor.constraint(equalToConstant: 36),
            iconView.widthAnchor.constraint(equalToConstant: 36),
            
            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
    }
}
extension XDeviceBottomView {
    typealias Cell = XDeviceBottomViewCell
}
