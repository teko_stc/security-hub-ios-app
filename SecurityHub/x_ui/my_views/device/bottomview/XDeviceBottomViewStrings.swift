//
//  XDeviceBottomViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XDeviceBottomViewStrings {
    
    /// Зона
    var zoneName: String { "DEVICE".localized() }
    
    /// Номер клеммы
    var zoneIO: String { "N_BOTTOM_INFO_INPUT_NUMBER".localized() }
    
    /// Номер зоны
    var zoneId: String { "N_BOTTOM_INFO_ZONE_NUMBER".localized() }
    
    /// Раздел
    var sectionName: String { "PARTITION".localized() }
    
    /// Контроллер
    var deviceName: String { "CONTROLLER".localized() }
    
    /// Объект
    var siteName: String { "OBJECT".localized() }
}
