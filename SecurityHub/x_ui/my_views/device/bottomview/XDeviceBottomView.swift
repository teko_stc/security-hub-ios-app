//
//  XDeviceBottomView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 03.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceBottomView: UIView {
    private var tableView = UITableView(), tableViewHeightAnchor: NSLayoutConstraint!
    private var items: [Cell.Model] = []
    private var _items: [Int:Cell.Model] = [:]
    private var cellStyle: Cell.Style
    private var isReady = false
    
    private lazy var strings: XDeviceBottomViewStrings = XDeviceBottomViewStrings()
    
    init(cellStyle: Cell.Style) {
        self.cellStyle = cellStyle
        super.init(frame: .zero)
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.separatorStyle = .none
        tableView.sectionFooterHeight = 0
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.addObserver(self, forKeyPath: "contentSize", options: [.new, .old, .prior], context: nil)
        addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeightAnchor = tableView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableViewHeightAnchor
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func layoutSubviews() {
        super.layoutSubviews()
        isReady = true
    }
    
    @objc override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            tableViewHeightAnchor.constant = tableView.contentSize.height
            layoutIfNeeded()
        }
    }
    
    public func setModelType(name: String) { _items[0] = Cell.Model(title: nil, text: name, iconName: nil) }
    
    public func setModel(name: String, icon: String) { _items[1] = Cell.Model(title: nil, text: name, iconName: icon) }
        
    public func setAlarmType(alarmName: String, detectorName: String) { _items[2] = Cell.Model(title: alarmName, text: detectorName, iconName: nil) }

    public func setZone(name: String) { _items[3] = Cell.Model(title: strings.zoneName, text: name, iconName: nil) }
    
    public func setZone(io: Int) { _items[4] = Cell.Model(title: strings.zoneIO, text: "\(io)", iconName: nil) }

    public func setZone(id: Int) { _items[5] = Cell.Model(title: strings.zoneId, text: "\(id)", iconName: nil) }

    public func setSection(name: String) { _items[6] = Cell.Model(title: strings.sectionName, text: name, iconName: nil) }

    public func setDevice(name: String) { _items[7] = Cell.Model(title: strings.deviceName, text: name, iconName: nil) }
    
    public func setSite(name: String) { _items[8] = Cell.Model(title: strings.siteName, text: name, iconName: nil) }

    public func build() {
        items = []
        for i in 0...8 { if let item = _items[i] { items.append(item) } }
        if isReady { tableView.reloadData() }
    }
}

extension XDeviceBottomView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setContent(model: items[indexPath.row])
        cell.setStyle(style: cellStyle)
        return cell
    }
}
