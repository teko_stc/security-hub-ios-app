//
//  XDeviceTopViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XDeviceTopViewDelegate: XDeviceNavigationBarDelegate {
    func xDeviceTopViewStateListViewItemTapped(model: XDeviceTopView.Cell.Model)
    func xLeftActionViewTapped(type: XDeviceTopView.ActionType?)
    func xRightActionViewTapped(type: XDeviceTopView.ActionType?)
    func xStateItemTapped(type: StateType)
    func xScriptButtonViewTapped(value: Int)
}
