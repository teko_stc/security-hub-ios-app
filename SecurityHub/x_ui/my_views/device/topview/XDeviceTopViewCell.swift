//
//  XDeviceTopViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.09.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDeviceTopView {
    typealias Cell = XDeviceTopViewCell
    
    enum ActionType {
        case security, technical, reset, relay, script
    }
}

extension XDeviceTopViewCell {
    struct Model {
        let eventId: Int, description: String
    }
}

class XDeviceTopViewCell: UITableViewCell {
    static let identifier = "XDeviceTopViewCell.identifier"
    
    private var baseView: UIView = UIView()
    private var titleView: UILabel = UILabel()
    private var model: Model?, onViewTapped: ((Model) -> ())?
    
    public func setContent(model: Model) {
        self.model = model
        titleView.text = model.description
    }
    
    public func setDelegate(onViewTapped: @escaping (Model) -> ()) {
        self.onViewTapped = onViewTapped
    }
    
    public func setStyle(font: UIFont?, backgroundColor: UIColor?, tintColor: UIColor?) {
        contentView.backgroundColor = backgroundColor
        titleView.font = font
        titleView.textColor = tintColor
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()

        let tap = UITapGestureRecognizer(target: self, action: #selector(tabGesture))
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGesture))
        baseView.isUserInteractionEnabled = true
        baseView.addGestureRecognizer(tap)
        baseView.transform = CGAffineTransform.identity
        baseView.addGestureRecognizer(long)
        contentView.addSubview(baseView)

        titleView.textAlignment = .center
        baseView.addSubview(titleView)
        
        [baseView, titleView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            baseView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            baseView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            baseView.topAnchor.constraint(equalTo: contentView.topAnchor),
            baseView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            titleView.topAnchor.constraint(equalTo: baseView.topAnchor),
            titleView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor),
            titleView.centerXAnchor.constraint(equalTo: baseView.centerXAnchor),
            titleView.widthAnchor.constraint(equalTo: baseView.widthAnchor, multiplier: 0.8),
       ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: { self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9) })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: { self.baseView.transform = CGAffineTransform.identity })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.onViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: { self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9) })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: { self.baseView.transform = CGAffineTransform.identity }) { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.onViewTapped?(model) }
            }
        }
    }
}
