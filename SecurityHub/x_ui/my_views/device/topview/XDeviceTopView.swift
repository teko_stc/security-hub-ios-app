//
//  XDeviceTopView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDeviceTopView: UIView {
    private lazy var strings: XArmAlertViewStrings = XArmAlertViewStrings()
    
    private var iconStateContentView = UIView(), actionContentView = UIView(), mainView = UIView()
    private var navigationView = XDeviceNavigationBar(), iconView = XDeviceIconView(), statesView = XDeviceStatesView()
    private var stateListView: UITableView = UITableView(), stateListViewHeightAnchor: NSLayoutConstraint!
    private var cardLayer: CAShapeLayer = CAShapeLayer(), actionLayer: CAShapeLayer = CAShapeLayer()
    private var leftActionView: XVerticalButton?, rightActionView: XVerticalButton?, actionViewHeightAnchor: NSLayoutConstraint!, leftStatesMarginAnchor: NSLayoutConstraint!, scriptButtons: [XVerticalButton] = []
    private var scriptLable: UILabel?, scriptActionView: XZoomButton?, scriptButtonStyle: XZoomButton.Style?
    private var stateListViewItems: [Cell.Model] = []
    private var isReady = false
    private var actionType: ActionType?
    
    public weak var delegate: XDeviceTopViewDelegate? {
        didSet {
            navigationView.delegate = delegate
            statesView.delegate = delegate
        }
    }
    
    public var font: UIFont? {
        didSet {
            navigationView.font = font
            scriptButtons.forEach { v in v.font = font?.withSize((font?.pointSize ?? 1) * 0.6) }
            leftActionView?.font = font?.withSize((font?.pointSize ?? 1) * 0.6)
            rightActionView?.font = font?.withSize((font?.pointSize ?? 1) * 0.6)
            scriptLable?.font = font?.withSize((font?.pointSize ?? 1) * 0.8)
            scriptActionView?.style.font = font?.withSize((font?.pointSize ?? 1) * 0.6)
            scriptActionView?.styleDidUpdate()
        }
    }
    
    public var leftStatesMargin: CGFloat = 40 {
        didSet {
            leftStatesMarginAnchor?.constant = leftStatesMargin
            if isReady { layoutIfNeeded() }
        }
    }
    
    public var cardColor: UIColor? {
        didSet {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            cardLayer.fillColor = cardColor?.cgColor
            [navigationView, iconView, statesView, stateListView].forEach({ $0.backgroundColor = cardColor })
            CATransaction.commit()
        }
    }
    
    public var cardTextColor: UIColor? {
        didSet {
            navigationView.textColor = cardTextColor ?? .clear
            statesView.textColor = cardTextColor ?? .clear
        }
    }
    
    public var cardTintColor: UIColor! {
        didSet {
            [navigationView, iconView, statesView].forEach({ $0.tintColor = cardTintColor })
        }
    }
    
    public var actionCardColor: UIColor? {
        didSet {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            actionLayer.fillColor = actionCardColor?.cgColor
            CATransaction.commit()
        }
    }
  
    public var actionTintColor: UIColor? {
        didSet {
            [leftActionView, rightActionView].join(scriptButtons).forEach({ $0?.tintColor = actionTintColor })
            scriptLable?.textColor = actionTintColor
        }
    }
    
    public var actionImageRenderMode: UIImage.RenderingMode = .alwaysTemplate {
        didSet {
            leftActionView?.image = leftActionView?.image?.withRenderingMode(actionImageRenderMode)
            rightActionView?.image = rightActionView?.image?.withRenderingMode(actionImageRenderMode)
            scriptButtons.forEach { v in v.image = v.image?.withRenderingMode(actionImageRenderMode) }
        }
    }
    
    public var isActionHidden: Bool = false {
        didSet {
            [scriptLable, scriptActionView, leftActionView, rightActionView].forEach({ $0?.isHidden = isActionHidden })
            scriptButtons.forEach { v in v.isHidden = isActionHidden }
            actionViewHeightAnchor.constant = (actionType == .none || isActionHidden) ? 0 : (actionType == .script && scriptButtons.count > 0) ? 240 : 120
        }
    }

    public var isHiddenSettings: Bool = false {
        didSet {
            navigationView.isHiddenSettings = isHiddenSettings
        }
    }

    init(scriptButtonStyle: XZoomButton.Style? = nil) {
        self.scriptButtonStyle = scriptButtonStyle
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        isReady = true
        cardLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: mainView.frame.size.width, height: mainView.frame.size.height), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20.0, height: 0.0)).cgPath
        actionLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 20.0, height: 0.0)).cgPath
    }

    private func initViews() {
        cardColor = .white
        actionCardColor = .white

        layer.addSublayer(actionLayer)
        
        mainView.layer.addSublayer(cardLayer)
        addSubview(mainView)
        mainView.addSubview(navigationView)
        mainView.addSubview(iconStateContentView)
        iconStateContentView.addSubview(iconView)
        iconStateContentView.addSubview(statesView)
        
        stateListView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        if #available(iOS 15.0, *) {
            stateListView.sectionHeaderTopPadding = 0
        }
        stateListView.dataSource = self
        stateListView.delegate = self
        stateListView.separatorStyle = .none
        stateListView.sectionFooterHeight = 0
        stateListView.alwaysBounceVertical = false
        mainView.addSubview(stateListView)
        
        addSubview(actionContentView)
    }
    
    private func setConstraints() {
        stateListViewHeightAnchor = stateListView.heightAnchor.constraint(equalToConstant: 0)
        [mainView, navigationView, iconStateContentView, iconView, statesView, stateListView, actionContentView].forEach { view in view?.translatesAutoresizingMaskIntoConstraints = false }
        actionViewHeightAnchor = actionContentView.heightAnchor.constraint(equalToConstant: 0)
        leftStatesMarginAnchor = statesView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: leftStatesMargin)
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: topAnchor),
            mainView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            navigationView.topAnchor.constraint(equalTo: mainView.topAnchor),
            navigationView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            navigationView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            
            iconStateContentView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
//            iconStateContentView.leadingAnchor.constraint(greaterThanOrEqualTo: mainView.leadingAnchor),
//            iconStateContentView.trailingAnchor.constraint(lessThanOrEqualTo: mainView.trailingAnchor),
            iconStateContentView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),

            iconView.topAnchor.constraint(greaterThanOrEqualTo: iconStateContentView.topAnchor, constant: 30),
            iconView.centerYAnchor.constraint(equalTo: statesView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: iconStateContentView.leadingAnchor),
            iconView.bottomAnchor.constraint(lessThanOrEqualTo: iconStateContentView.bottomAnchor),
            iconView.heightAnchor.constraint(equalToConstant: 120),
            iconView.widthAnchor.constraint(equalToConstant: 120),
            
            statesView.topAnchor.constraint(equalTo: iconStateContentView.topAnchor),
            leftStatesMarginAnchor,
            statesView.trailingAnchor.constraint(lessThanOrEqualTo: iconStateContentView.trailingAnchor),
            statesView.bottomAnchor.constraint(lessThanOrEqualTo: iconStateContentView.bottomAnchor),
            
            stateListView.topAnchor.constraint(equalTo: iconStateContentView.bottomAnchor),
            stateListView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            stateListView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            stateListView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -20),
            stateListViewHeightAnchor,
            
            actionContentView.topAnchor.constraint(equalTo: mainView.bottomAnchor),
            actionContentView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            actionContentView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            actionContentView.centerXAnchor.constraint(equalTo: centerXAnchor),
            actionContentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            actionViewHeightAnchor
        ])
    }
    
    private func createActionButton(image: String, title: String) -> XVerticalButton {
        let view = XVerticalButton()
        view.setStyle(XVerticalButton.Style(font: font?.withSize((font?.pointSize ?? 1) * 0.6), color: actionTintColor))
        view.image = UIImage(named: image)?.withRenderingMode(actionImageRenderMode)
        view.title = title
        actionContentView.addSubview(view)
        return view
    }
}

extension XDeviceTopView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return stateListViewItems.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setContent(model: stateListViewItems[indexPath.row])
        cell.setStyle(font: font, backgroundColor: cardColor, tintColor: cardTextColor)
        cell.setDelegate(onViewTapped: stateListViewItemTapped)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { UIView() }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { 20 }
    
    func numberOfSections(in tableView: UITableView) -> Int { stateListViewItems.count == 0 ? 0 : 1 }
    
    private func stateListViewItemTapped(_ model: Cell.Model) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        delegate?.xDeviceTopViewStateListViewItemTapped(model: model)
    }
}

extension XDeviceTopView {
    public func setNavigationViewTitle(title: String) { navigationView.title = title }
    
    public func setNavigationViewIsFavoriteDefault(isFavoritDevice: Bool) { navigationView.isFavoritDevice = isFavoritDevice }

    public func setIconViewImage(name: String, renderMode: UIImage.RenderingMode) { iconView.icon = UIImage(named: name)?.withRenderingMode(renderMode) }

    public func setActionType(_ type: ActionType?, extraActions: [String:Int64]? = nil) {
        if actionType == type { return }
        self.actionType = type
        [leftActionView, rightActionView, scriptLable, scriptActionView].join(scriptButtons).forEach({ $0?.removeFromSuperview() })
        leftActionView = nil; rightActionView = nil; scriptLable = nil; scriptActionView = nil; scriptButtons = []
        isActionHidden = false
        switch actionType {
        case .security:
            leftActionView = createActionButton(image: "ic_arm", title: strings.arm)
            rightActionView = createActionButton(image: "ic_disarm", title: strings.disarm)
            break;
        case .relay:
            leftActionView = createActionButton(image: "ic_on", title: strings.on)
            rightActionView = createActionButton(image: "ic_off", title: strings.off)
            break;
        case .technical:
            leftActionView = createActionButton(image: "ic_control", title: strings.on)
            rightActionView = createActionButton(image: "ic_no_control", title: strings.off)
            break;
        case .reset:
            leftActionView = createActionButton(image: "ic_reset", title: strings.reset)
            break;
        case .script:
            scriptLable = UILabel.create(XBaseLableStyle(color: actionTintColor ?? .clear, font: font?.withSize((font?.pointSize ?? 1) * 0.8), alignment: .center))
            actionContentView.addSubview(scriptLable!)
            scriptActionView = XZoomButton(style: scriptButtonStyle!)
            scriptActionView?.addTarget(self, action: #selector(scriptActionViewTapped), for: .touchUpInside)
            actionContentView.addSubview(scriptActionView!)
            if (extraActions?.count ?? 0) > 0 {
                extraActions!.forEach { (key: String, value: Int64) in
                    let btn = createActionButton(image: "ic_on", title: key)
                    btn.onTapped = { [value] in self.delegate?.xScriptButtonViewTapped(value: value.int) }
                    scriptButtons.append(btn)
                }
            }
            break;
        case .none: break;
        }
        leftActionView?.onTapped = { self.delegate?.xLeftActionViewTapped(type: self.actionType) }
        rightActionView?.onTapped = { self.delegate?.xRightActionViewTapped(type: self.actionType) }
        [scriptLable, scriptActionView, leftActionView, rightActionView].join(scriptButtons).forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        if (actionType == .security || actionType == .relay || actionType == .technical) {
            NSLayoutConstraint.activate([
                leftActionView!.leadingAnchor.constraint(equalTo: actionContentView.leadingAnchor),
                leftActionView!.centerYAnchor.constraint(equalTo: actionContentView.centerYAnchor),
                leftActionView!.heightAnchor.constraint(equalTo: actionContentView.heightAnchor, multiplier: 0.9),

                rightActionView!.topAnchor.constraint(equalTo: leftActionView!.topAnchor),
                rightActionView!.leadingAnchor.constraint(equalTo: leftActionView!.trailingAnchor, constant: 80),
                rightActionView!.trailingAnchor.constraint(equalTo: actionContentView.trailingAnchor),
                rightActionView!.heightAnchor.constraint(equalTo: leftActionView!.heightAnchor),
                rightActionView!.widthAnchor.constraint(equalTo: leftActionView!.widthAnchor),
            ])
        } else if (actionType == .reset) {
            NSLayoutConstraint.activate([
                leftActionView!.leadingAnchor.constraint(equalTo: actionContentView.leadingAnchor),
                leftActionView!.centerYAnchor.constraint(equalTo: actionContentView.centerYAnchor),
                leftActionView!.heightAnchor.constraint(equalTo: actionContentView.heightAnchor, multiplier: 0.9),
                leftActionView!.trailingAnchor.constraint(equalTo: actionContentView.trailingAnchor),
            ])
        } else if (actionType == .script) {
            if scriptButtons.count > 0 {
                scriptButtons[0].leadingAnchor.constraint(equalTo: actionContentView.leadingAnchor).isActive = true
                scriptButtons[0].widthAnchor.constraint(equalTo: actionContentView.widthAnchor, multiplier: 1.0/CGFloat(scriptButtons.count)).isActive = true
                scriptButtons[0].topAnchor.constraint(equalTo: actionContentView.topAnchor).isActive = true
                scriptButtons[0].heightAnchor.constraint(equalTo: actionContentView.heightAnchor, multiplier: 0.5).isActive = true
                if (scriptButtons.count > 1) {
                    for i in 1...scriptButtons.count - 1 {
                        scriptButtons[i].leadingAnchor.constraint(equalTo: scriptButtons[i - 1].trailingAnchor).isActive = true
                        scriptButtons[i].topAnchor.constraint(equalTo: actionContentView.topAnchor).isActive = true
                        scriptButtons[i].widthAnchor.constraint(equalTo: actionContentView.widthAnchor, multiplier: 1.0/CGFloat(scriptButtons.count)).isActive = true
                        scriptButtons[i].heightAnchor.constraint(equalTo: actionContentView.heightAnchor, multiplier: 0.5).isActive = true
                    }
                }
                scriptButtons[scriptButtons.count - 1].trailingAnchor.constraint(equalTo: actionContentView.trailingAnchor).isActive = true
                scriptLable!.topAnchor.constraint(equalTo: scriptButtons[0].bottomAnchor, constant: 10).isActive = true
                actionViewHeightAnchor.constant = 240
            } else {
                scriptLable!.topAnchor.constraint(equalTo: actionContentView.centerYAnchor, constant: -(scriptLable?.font?.pointSize ?? 1) - 20).isActive = true
            }
            NSLayoutConstraint.activate([
                scriptLable!.leadingAnchor.constraint(equalTo: actionContentView.leadingAnchor),
                scriptLable!.trailingAnchor.constraint(equalTo: actionContentView.trailingAnchor),

                scriptActionView!.topAnchor.constraint(equalTo: scriptLable!.bottomAnchor, constant: 10),
                scriptActionView!.centerXAnchor.constraint(equalTo: actionContentView.centerXAnchor),
                scriptActionView!.heightAnchor.constraint(equalToConstant: 48),
                scriptActionView!.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            ])
        }
    }
    
    @objc private func scriptActionViewTapped() { self.delegate?.xLeftActionViewTapped(type: self.actionType) }
    
    public func setState(state: XDeviceIconViewState?) { iconView.state = state }
    
    public func setIconStateRenderMode(mode: UIImage.RenderingMode) { iconView.stateRenderMode = mode }
    
    public func setStateManualRelay() { statesView.updRelayTypeView(isAutoRelay: false) }
    
    public func setStateAutoRelay() { statesView.updRelayTypeView(isAutoRelay: true) }
    
    public func setState(isHaveScript: Bool) { statesView.updRelayScriptView(isHidden: !isHaveScript) }
    
    public func setScriptActionDefaults(lable: String?, button: String?) {
        scriptLable?.text = lable
        scriptActionView?.text = button
    }
    
    public func setStatesListItems(items: [Cell.Model]) {
        stateListViewItems = items
        stateListViewHeightAnchor.constant = items.count == 0 ? 0 : (20 + CGFloat(items.count) * 40.0)
        if isReady { stateListView.reloadData() }
    }
    
    public func setState(isHaveConnection: Bool) { statesView.updConnectionView(isHaveConnection: isHaveConnection) }
    
    public func setState(isHidden: Bool, level: Int?, isHaveBattery: Bool, isBatteryError: Bool) { statesView.updBatteryView(isHidden: isHidden, level: level, isHaveBattery: isHaveBattery, isBatteryError: isBatteryError) }
    
    public func setState(isSignalLevelHidden: Bool, level: Int?) { statesView.updSignalLevelView(isHidden: isSignalLevelHidden, level: level) }
    
    public func setState(isTemperatureHidden: Bool, celsius: Int?) { statesView.updTemperatureView(isHidden: isTemperatureHidden, celsius: celsius) }

    public func setState(isTemperature2Hidden: Bool, celsius: Int?) { statesView.updTemperature2View(isHidden: isTemperature2Hidden, celsius: celsius) }

    public func setState(isUpdateHidden: Bool) {  statesView.updUpdateView(isHidden: isUpdateHidden) }
    
    public func setState(isDelayHidden: Bool, isDelayArm: Bool) {  statesView.updDelayView(isHidden: isDelayHidden, isDelayArm: isDelayArm) }
	
		public func setState(isBypassHidden: Bool) {
			statesView.updBypassView(isHidden: isBypassHidden) }

    public func setState(isHaveSocket: Bool) { statesView.updSocketView(isHaveSocket: isHaveSocket) }

    public func setState(isHaveLan: Bool) { statesView.updLanView(isHaveLan: isHaveLan) }
    
    public func setState(isHidden: Bool, isDualSim: Bool, isHaveSim: Bool, isSimNoSignal: Bool, level: Int?) { statesView.updSimView(isHidden: isHidden, isDualSim: isDualSim, isHaveSim: isHaveSim, isSimNoSignal: isSimNoSignal, level: level) }
    
    public func setState(isHidden: Bool, isHaveSim2: Bool, isSim2NoSignal: Bool, level: Int?) { statesView.updSim2View(isHidden: isHidden, isHaveSim: isHaveSim2, isSimNoSignal: isSim2NoSignal, level: level) }

    public func test() {
//
//        statesView.updConnectionView(isHaveConnection: true)
////        statesView.updDelayView(isDelayArm: true)
////        statesView.updUpdateView()
        statesView.updSocketView()
        statesView.updLanView(isHaveLan: false)
//        statesView.updRelayTypeView(isAutoRelay: true)
//        statesView.updRelayScriptView()
//        statesView.updBatteryView(level: 100)
//        statesView.updSignalLevelView(level: 95)
////        statesView.updSimView(isDualSim: true, isHaveSim: true, isSimNoSignal: false, level: 50)
//        statesView.updTemperatureView(celsius: 4)
//        statesView.updSim2View(isHaveSim: true, isSimNoSignal: true, level: nil)
//        statesView.updTemperature2View(celsius: nil)
    }
}
