//
//  XNavigationBar.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XDeviceNavigationBarDelegate: AnyObject {
    func xDeviceNavigationBarBackViewTapped()
    func xDeviceNavigationBarFavoritViewTapped(isSelected: Bool)
    func xDeviceNavigationBarSettingsViewTapped()
}

class XDeviceNavigationBar: UIView {
    public weak var delegate: XDeviceNavigationBarDelegate?
    
    private var topStateView: UIView = UIView()
    private var backView: UIButton = UIButton(type: .infoDark)
    private var titleView: UILabel = UILabel()
    private var favoritView: UIButton = UIButton(type: .custom)
    private var settingView: UIButton = UIButton(type: .infoDark)
    
    public var font: UIFont? {
        didSet {
            titleView.font = font
        }
    }
    
    public var title: String? {
        didSet {
            titleView.text = title
        }
    }
    
    public var isFavoritDevice: Bool = false {
        didSet {
            favoritView.isSelected = isFavoritDevice
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            topStateView.backgroundColor = backgroundColor
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            favoritView.tintColor   = tintColor
            settingView.tintColor   = tintColor
        }
    }
    
    public var textColor: UIColor = .clear {
        didSet {
            titleView.textColor     = textColor
            backView.tintColor      = textColor
        }
    }

    public var isHiddenSettings: Bool = false {
        didSet {
            settingView.isHidden = isHiddenSettings
            
            constraints.forEach { removeConstraint($0) }
            setConstraints()
        }
    }

    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private func initViews() {
        addSubview(topStateView)
        
        backView.setImage(UIImage(named: "ic_stair-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backView.contentVerticalAlignment = .top
        backView.addTarget(self, action: #selector(backViewTapped), for: .touchUpInside)
        addSubview(backView)
        
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        favoritView.setImage(UIImage(named: "ic_main_border")?.withRenderingMode(.alwaysTemplate), for: .normal)
        favoritView.setImage(UIImage(named: "ic_main_border")?.withRenderingMode(.alwaysTemplate), for: .selected.union(.highlighted))
        favoritView.setImage(UIImage(named: "ic_main")?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        favoritView.setImage(UIImage(named: "ic_main")?.withRenderingMode(.alwaysTemplate), for: .selected)
        favoritView.contentVerticalAlignment = .top
        favoritView.addTarget(self, action: #selector(favoritViewTapped), for: .touchUpInside)
        addSubview(favoritView)
        
        settingView.setImage(UIImage(named: "ic_settings")?.withRenderingMode(.alwaysTemplate), for: .normal)
        settingView.contentVerticalAlignment = .top
        settingView.addTarget(self, action: #selector(settingViewTapped), for: .touchUpInside)
        addSubview(settingView)
    }
    
    private func setConstraints() {
        [backView, titleView, favoritView, settingView, topStateView].forEach({ view in view.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            topStateView.bottomAnchor.constraint(equalTo: topAnchor),
            topStateView.leadingAnchor.constraint(equalTo: leadingAnchor),
            topStateView.trailingAnchor.constraint(equalTo: trailingAnchor),
            topStateView.heightAnchor.constraint(equalToConstant: 600),
            
            backView.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            backView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            backView.heightAnchor.constraint(equalToConstant: 30),
            backView.widthAnchor.constraint(equalToConstant: 30),

            titleView.topAnchor.constraint(equalTo: backView.topAnchor, constant: -8),
            titleView.leadingAnchor.constraint(equalTo: backView.trailingAnchor, constant: 10),
            titleView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -10),

            favoritView.topAnchor.constraint(equalTo: backView.topAnchor, constant: -6),
            favoritView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: settingView.isHidden ? 0 : 20),
            favoritView.heightAnchor.constraint(equalToConstant: 30),
            favoritView.widthAnchor.constraint(equalToConstant: 30),

            settingView.topAnchor.constraint(equalTo: backView.topAnchor, constant: -6),
            settingView.isHidden ?  favoritView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20) :
                                    settingView.leadingAnchor.constraint(equalTo: favoritView.trailingAnchor, constant: 20),
            settingView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            settingView.heightAnchor.constraint(equalTo: favoritView.heightAnchor),
            settingView.widthAnchor.constraint(equalTo: favoritView.widthAnchor),
            settingView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -10),
        ])
    }
    
    @objc private func backViewTapped() {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        delegate?.xDeviceNavigationBarBackViewTapped()
    }
    
    @objc private func favoritViewTapped() {
        isFavoritDevice = !isFavoritDevice
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        delegate?.xDeviceNavigationBarFavoritViewTapped(isSelected: isFavoritDevice)
    }

    @objc private func settingViewTapped() {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        delegate?.xDeviceNavigationBarSettingsViewTapped()
    }
}
