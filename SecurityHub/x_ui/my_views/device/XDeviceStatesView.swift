//
//  XDeviceStatesView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XDeviceStatesView {
    class LineView: UIView {
        private var views: [UIButton] = []
        private var height, width: CGFloat
        private var heightAnchors: [NSLayoutConstraint] = [], widthAnchors: [NSLayoutConstraint] = []
        private var mainHeightAnchor, mainWidthAnchor: NSLayoutConstraint?
        private var isReady = false

        override var tintColor: UIColor! {
            didSet {
                views.forEach({ $0.tintColor = tintColor })
            }
        }
        
        public var textColor: UIColor = UIColor.black {
            didSet {
                views.forEach({ $0.setTitleColor(textColor, for: .normal) })
            }
        }
        
        init(heigth: CGFloat, width: CGFloat, insets: UIEdgeInsets, count: Int, type: CTFontOrientation) {
            self.height = heigth
            self.width = width
            super.init(frame: .zero)
            for i in 0...count-1 {
                views.append(UIButton())
                views[i].contentHorizontalAlignment = .left
                views[i].imageEdgeInsets = insets
                views[i].titleEdgeInsets = UIEdgeInsets(top: insets.top, left: insets.left * 2, bottom: insets.bottom, right: 0)
                views[i].isHidden = true
                addSubview(views[i])
            }
            for i in 0...views.count - 1 {
                views[i].translatesAutoresizingMaskIntoConstraints = false
                if (type == .horizontal) {
                    views[i].topAnchor.constraint(equalTo: topAnchor).isActive = true
                    views[i].bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor).isActive = true
                    if (i == 0) { views[i].leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true }
                    else { views[i].leadingAnchor.constraint(equalTo: views[i-1].trailingAnchor).isActive = true }
                    if (i == views.count - 1) {  views[i].trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true }
                } else {
                    views[i].leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
                    views[i].trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor).isActive = true
                    if (i == 0) { views[i].topAnchor.constraint(equalTo: topAnchor).isActive = true }
                    else { views[i].topAnchor.constraint(equalTo: views[i-1].bottomAnchor).isActive = true }
                    if (i == views.count - 1) { views[i].bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true }
                }
                heightAnchors.append(views[i].heightAnchor.constraint(equalToConstant: 0))
                widthAnchors.append(views[i].widthAnchor.constraint(equalToConstant: 0))
            }
            if (type == .horizontal) { mainHeightAnchor = heightAnchor.constraint(equalToConstant: 0); mainHeightAnchor?.isActive = true }
            else { mainWidthAnchor = widthAnchor.constraint(equalToConstant: 0); mainWidthAnchor?.isActive = true }
            NSLayoutConstraint.activate(heightAnchors.join(widthAnchors))
        }
        required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
     
        override func layoutSubviews() {
            super.layoutSubviews()
            isReady = true
        }
        
        public func updElement(index: Int, imageName: String?, title: String?, imageRender: UIImage.RenderingMode = .alwaysTemplate, onClick: @escaping () -> ()) {
            if let name = imageName {
                let image = UIImage(named: name)?.resized(to: CGSize(width: 36, height: 36)).withRenderingMode(imageRender)
                views[index].setImage(image, for: .normal)
            }
            if let title = title { views[index].setTitle(title, for: .normal) }
            views[index].isHidden = (imageName == nil && title == nil)
            heightAnchors[index].constant = (imageName == nil && title == nil) ? 0 : height
            widthAnchors[index].constant = (imageName == nil && title == nil) ? 0 : width
            let i = views.map({$0.isHidden ? 0 : 1}).reduce(0, +)
            mainHeightAnchor?.constant = i == 0 ? 0 : height
            mainWidthAnchor?.constant = i == 0 ? 0 : width
            
            let tapGesture = StateTapGestureRecognizer(target: self, action: #selector(stateButtonTapped(_:)))
            tapGesture.onClick = onClick
            views[index].addGestureRecognizer(tapGesture)
            
            if isReady { layoutIfNeeded() }
        }
        
        @objc func stateButtonTapped(_ gesture: StateTapGestureRecognizer) {
            gesture.onClick()
        }
    }
    
    class StateTapGestureRecognizer: UITapGestureRecognizer {
        var onClick: (() -> ())!
    }
}

class XDeviceStatesView: UIView {
    public weak var delegate: XDeviceTopViewDelegate?
    
    private var firstView   = LineView(heigth: 44, width: 54 + 9, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 4, type: .vertical)
    private var secondView  = LineView(heigth: 44, width: 54 + 9, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 4, type: .horizontal)
    private var thirdView   = LineView(heigth: 44, width: 140, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 1, type: .horizontal)
    private var fourthView  = LineView(heigth: 44, width: 140, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 2, type: .horizontal)
    private var fifthView   = LineView(heigth: 44, width: 140, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 2, type: .horizontal)
    private var sixthView   = LineView(heigth: 44, width: 140, insets: UIEdgeInsets(top: 4, left: 18, bottom: 4, right: 9), count: 2, type: .horizontal)
 
    override var tintColor: UIColor! {
        didSet {
            [firstView, secondView, thirdView, fourthView, fifthView, sixthView].forEach({ $0.tintColor = tintColor })
        }
    }
    
    public var textColor: UIColor = .black {
        didSet {
            [secondView, thirdView, fourthView, fifthView, sixthView].forEach({ $0.textColor = textColor })
        }
    }
    
    init() {
        super.init(frame: .zero)
        [firstView, secondView, thirdView, fourthView, fifthView, sixthView].forEach({ addSubview($0) })
        [firstView, secondView, thirdView, fourthView, fifthView, sixthView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        firstView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        firstView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        firstView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor).isActive = true
        let views = [secondView, thirdView, fourthView, fifthView, sixthView]
        for i in 0...4 {
            if i == 0 { views[i].topAnchor.constraint(equalTo: topAnchor).isActive = true }
            else { views[i].topAnchor.constraint(equalTo: views[i-1].bottomAnchor).isActive = true }
            if i == 4 { views[i].bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor).isActive = true }
            views[i].leadingAnchor.constraint(equalTo: firstView.trailingAnchor).isActive = true
            views[i].trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor).isActive = true
        }
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
       
    public func updConnectionView(isHidden: Bool = false, isHaveConnection: Bool? = nil) {
        if isHaveConnection == true { firstView.updElement(index: 0, imageName: "ic_connection_on", title: nil, onClick: { self.stateItemTapped(type: .connectionState) }) }
        else if isHaveConnection == false { firstView.updElement(index: 0, imageName: "ic_connection_off", title: nil, onClick:  { self.stateItemTapped(type: .connectionState) }) }
        if isHidden { firstView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .connectionState) }) }
    }
    
    public func updDelayView(isHidden: Bool = false, isDelayArm: Bool? = nil) {
        if isDelayArm == false { firstView.updElement(index: 1, imageName: "ic_delay_list", title: nil, onClick: { self.stateItemTapped(type: .delay) }) }
        else if isDelayArm == true { firstView.updElement(index: 1, imageName: "ic_delay_on", title: nil, imageRender: .alwaysOriginal, onClick: { self.stateItemTapped(type: .delayActive) }) }
        if isHidden { firstView.updElement(index: 1, imageName: nil, title: nil, onClick: { }) }
    }
	
		public func updBypassView(isHidden: Bool = false) {
				if isHidden { firstView.updElement(index: 2, imageName: nil, title: nil, onClick: { }) }
				else {
					firstView.updElement(index: 2, imageName: "ic_bypass", title: nil, onClick: { self.stateItemTapped(type: .bypass) }) }
		}
    
    public func updUpdateView(isHidden: Bool = false) {
        if isHidden { firstView.updElement(index: 3, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .update) }) }
        else { firstView.updElement(index: 3, imageName: "ic_update", title: nil, onClick: { self.stateItemTapped(type: .update) }) }
    }
    
    public func updSocketView(isHidden: Bool = false, isHaveSocket: Bool? = nil) {
        if isHaveSocket == true { secondView.updElement(index: 0, imageName: "ic_socket", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        else if isHaveSocket == false { secondView.updElement(index: 0, imageName: "ic_socket_no", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        if isHidden { secondView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
    }
    
    public func updLanView(isHidden: Bool = false, isHaveLan: Bool? = nil) {
        if isHaveLan == true { secondView.updElement(index: 1, imageName: "ic_lan", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        else if isHaveLan == false { secondView.updElement(index: 1, imageName: "ic_lan_no", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        if isHidden { secondView.updElement(index: 1, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
    }
    
    public func updRelayTypeView(isHidden: Bool = false, isAutoRelay: Bool? = nil) {
        if isAutoRelay == true { secondView.updElement(index: 2, imageName: "ic_relay_type_auto_black", title: nil, onClick: { self.stateItemTapped(type: .relay) }) }
        else if isAutoRelay == false { secondView.updElement(index: 2, imageName: "ic_relay_type", title: nil, onClick: { self.stateItemTapped(type: .relay) }) }
        if isHidden { secondView.updElement(index: 2, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .relay) }) }
    }
    
    public func updRelayScriptView(isHidden: Bool = false) {
        if isHidden { secondView.updElement(index: 3, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .relay) }) }
        else { secondView.updElement(index: 3, imageName: "ic_relay_scripted_list_offline", title: nil, onClick: { self.stateItemTapped(type: .relay) }) }
    }
    
    public func updBatteryView(isHidden: Bool = false, level: Int? = nil, isHaveBattery: Bool = true, isBatteryError: Bool = false) {
        if (isBatteryError) { thirdView.updElement(index: 0, imageName: "ic_battery_error", title: "——", onClick: { self.stateItemTapped(type: .battery) }) }
        if (!isHaveBattery) { thirdView.updElement(index: 0, imageName: "ic_battery_no", title: "——", onClick: { self.stateItemTapped(type: .battery) }) }
        else { thirdView.updElement(index: 0, imageName: "ic_battery_mid", title: "——", onClick: { self.stateItemTapped(type: .battery) }) }
        if let level = level, level < 10 { thirdView.updElement(index: 0, imageName: "ic_battery_empty", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        else if let level = level, level < 50 { thirdView.updElement(index: 0, imageName: "ic_battery_low", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        else if let level = level, level < 90 { thirdView.updElement(index: 0, imageName: "ic_battery_mid", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        else if let _ = level { thirdView.updElement(index: 0, imageName: "ic_battery_high", title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
        if let level = level { thirdView.updElement(index: 0, imageName: nil, title: "\(level) %", onClick: { self.stateItemTapped(type: .battery) }) }
        else { thirdView.updElement(index: 0, imageName: nil, title: "——", onClick: { self.stateItemTapped(type: .battery) }) }
        if isHidden { thirdView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .battery) }) }
    }
 
    public func updSignalLevelView(isHidden: Bool = false, level: Int? = nil) {
        if let level = level, level < 10 { fourthView.updElement(index: 0, imageName: "ic_level_low", title: "\(level) %", onClick: { self.stateItemTapped(type: .connectionLevel) }) }
        else if let level = level, level < 50 { fourthView.updElement(index: 0, imageName: "ic_level_mid", title: "\(level) %", onClick: { self.stateItemTapped(type: .connectionLevel) }) }
        else if let level = level, level < 90 { fourthView.updElement(index: 0, imageName: "ic_level_high", title: "\(level) %", onClick: { self.stateItemTapped(type: .connectionLevel) }) }
        else if let level = level { fourthView.updElement(index: 0, imageName: "ic_level_top", title: "\(level) %", onClick: { self.stateItemTapped(type: .connectionLevel) }) }
        else { fourthView.updElement(index: 0, imageName: "ic_level", title: "——", onClick: { self.stateItemTapped(type: .connectionLevel) }) }
        if isHidden { fourthView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .connectionLevel) }) }
    }
    
    public func updSimView(isHidden: Bool = false, isDualSim: Bool = false, isHaveSim: Bool = false, isSimNoSignal: Bool = false, level: Int? = nil) {
        if isSimNoSignal { fourthView.updElement(index: 1, imageName: isDualSim ? "ic_sim_1_no_signal" : "ic_sim_no_signal", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        if isHaveSim { fourthView.updElement(index: 1, imageName: isDualSim ? "ic_sim_1" :"ic_sim", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        else { fourthView.updElement(index: 1, imageName: isDualSim ? "ic_sim_1_no" :"ic_sim_no", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        if let level = level { fourthView.updElement(index: 1, imageName: nil, title: "\(level) %", onClick: { self.stateItemTapped(type: .connection) }) }
        else { fourthView.updElement(index: 1, imageName: nil, title: "——", onClick: { self.stateItemTapped(type: .connection) }) }
        if isHidden { fourthView.updElement(index: 1, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
    }
    
    public func updTemperatureView(isHidden: Bool = false, celsius: Int? = nil) {
        if let celsius = celsius { fifthView.updElement(index: 0, imageName: "ic_temp", title: "\(celsius)°C", onClick: { self.stateItemTapped(type: .temperature) }) }
        else { fifthView.updElement(index: 0, imageName: "ic_temp", title: "——", onClick: { self.stateItemTapped(type: .temperature) }) }
        if isHidden { fifthView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .temperature) }) }
    }
    
    public func updSim2View(isHidden: Bool = false, isHaveSim: Bool = false, isSimNoSignal: Bool = false, level: Int? = nil) {
        if isSimNoSignal { fifthView.updElement(index: 1, imageName: "ic_sim_2_no_signal", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        if isHaveSim { fifthView.updElement(index: 1, imageName: "ic_sim_2", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        else { fifthView.updElement(index: 1, imageName: "ic_sim_2_no", title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
        if let level = level { fifthView.updElement(index: 1, imageName: nil, title: "\(level) %", onClick: { self.stateItemTapped(type: .connection) }) }
        else { fifthView.updElement(index: 1, imageName: nil, title: "——", onClick: { self.stateItemTapped(type: .connection) }) }
        if isHidden { fifthView.updElement(index: 1, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .connection) }) }
    }
    
    public func updTemperature2View(isHidden: Bool = false, celsius: Int? = nil) {
        if let celsius = celsius { sixthView.updElement(index: 0, imageName: "ic_temp_2", title: "\(celsius)°C", onClick: { self.stateItemTapped(type: .temperature) }) }
        else { sixthView.updElement(index: 0, imageName: "ic_temp_2", title: "——", onClick: { self.stateItemTapped(type: .temperature) }) }
        if isHidden { sixthView.updElement(index: 0, imageName: nil, title: nil, onClick: { self.stateItemTapped(type: .temperature) }) }
    }
    
    private func stateItemTapped(type: StateType) {
        delegate?.xStateItemTapped(type: type)
    }
}

enum StateType {
    case battery
    case delay
    case delayActive
    case update
    case connection
    case connectionState
    case connectionLevel
    case temperature
    case relay
		case bypass
}
