//
//  XDeviceIcon.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

enum XDeviceIconViewState {
    case security_armed
    case security_disarmed
    case security_part_armed
    case relay_on
    case relay_off
    case technical_armed
    case technical_disarmed
		case security_bypass
}
extension XDeviceIconView {
    typealias State = XDeviceIconViewState
}

class XDeviceIconView: UIView {
    private var mainView: UIImageView = UIImageView()
    private var stateView: UIImageView = UIImageView()
    private var stateBackgroundView: UIImageView = UIImageView()
    
    public var stateRenderMode: UIImage.RenderingMode = .alwaysTemplate {
        didSet {
            updState()
        }
    }

    public var state: State? {
        didSet {
            updState()
        }
    }
    
    public var icon: UIImage? {
        didSet {
            mainView.image = icon
        }
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            stateBackgroundView.tintColor = backgroundColor
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            stateView.tintColor = tintColor
            mainView.tintColor = tintColor
        }
    }
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private func initViews() {
        mainView.contentMode = .scaleAspectFit
        addSubview(mainView)
        
        stateBackgroundView.contentMode = .scaleAspectFit
        addSubview(stateBackgroundView)
        
        stateView.contentMode = .scaleAspectFit
        addSubview(stateView)
    }
    
    private func setConstraints() {
        [mainView, stateBackgroundView, stateView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: topAnchor),
            mainView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            stateBackgroundView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 20),
            stateBackgroundView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            stateBackgroundView.heightAnchor.constraint(equalToConstant: 36),
            stateBackgroundView.widthAnchor.constraint(equalToConstant: 36),
            
            stateView.centerXAnchor.constraint(equalTo: stateBackgroundView.centerXAnchor),
            stateView.centerYAnchor.constraint(equalTo: stateBackgroundView.centerYAnchor),
            stateView.heightAnchor.constraint(equalToConstant: 36),
            stateView.widthAnchor.constraint(equalToConstant: 36),
        ])
    }
    
    private func updState() {
        switch state {
        case .security_armed:
            stateView.image = UIImage(named: "ic_arm_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_ad_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .security_disarmed:
            stateView.image = UIImage(named: "ic_disarm_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_ad_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .security_part_armed:
            stateView.image = UIImage(named: "ic_partly_arm_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_ad_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .relay_on:
            stateView.image = UIImage(named: "ic_turn_on_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_oo_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .relay_off:
            stateView.image = UIImage(named: "ic_turn_off_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_oo_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .technical_armed:
            stateView.image = UIImage(named: "ic_control_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_oo_background")?.withRenderingMode(.alwaysTemplate)
            break
        case .technical_disarmed:
            stateView.image = UIImage(named: "ic_no_control_list")?.withRenderingMode(stateRenderMode)
            stateBackgroundView.image = UIImage(named: "ic_oo_background")?.withRenderingMode(.alwaysTemplate)
            break
				case .security_bypass:
						stateView.image = UIImage(named: "ic_bypass")?.withRenderingMode(stateRenderMode)
						stateBackgroundView.image = UIImage(named: "ic_delay_off")?.withRenderingMode(.alwaysTemplate)
						break
        default:
            stateView.image = UIImage(named: "ic_nil")
            stateBackgroundView.image = UIImage(named: "ic_nil")
            break
        }
    }
}
