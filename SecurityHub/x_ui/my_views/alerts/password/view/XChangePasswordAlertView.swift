//
//  XChangePasswordAlert.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XChangePasswordAlertViewDelegate {
    func changePasswordViewTapped(value: String)
    func closeViewTapped()
}

protocol XChangePasswordAlertViewStringsProtocol {
    var newPassword: String { get }
    var repeatNewPassword: String { get }
    var changePassword: String { get }
    var close: String { get }
}

struct XChangePasswordAlertViewStyle {
    let backgroundColor: UIColor
    let titleView: XBaseLableStyle
    let passwordView: XTextFieldStyle
    let changeView, closeView: XZoomButtonStyle
}

class XChangePasswordAlertView: UIView {
    public var delegate: XChangePasswordAlertViewDelegate?
    
    private var titleView: UILabel!
    private var newPasswordView, repeatNewPasswordView: XPasswordView!
    private var changeView, closeView: XZoomButton!
    
    private lazy var style: XChangePasswordAlertViewStyle = self.viewStyle()
    private lazy var strings: XChangePasswordAlertViewStringsProtocol = XChangePasswordAlertVCStrings()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))
        
        titleView = UILabel.create(style.titleView)
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        newPasswordView = XPasswordView(style: style.passwordView)
        newPasswordView.placeholder = strings.newPassword
        newPasswordView.returnKeyType = .next
        newPasswordView.xDelegate = self
        addSubview(newPasswordView)
        
        repeatNewPasswordView = XPasswordView(style: style.passwordView)
        repeatNewPasswordView.placeholder = strings.repeatNewPassword
        repeatNewPasswordView.returnKeyType = .done
        repeatNewPasswordView.xDelegate = self
        addSubview(repeatNewPasswordView)
        
        changeView = XZoomButton(style: style.changeView)
        changeView.text = strings.changePassword
        changeView.addTarget(self, action: #selector(changeViewTapped), for: .touchUpInside)
        addSubview(changeView)
        
        closeView = XZoomButton(style: style.closeView)
        closeView.text = strings.close
        closeView.addTarget(self, action: #selector(closeViewTapped), for: .touchUpInside)
        addSubview(closeView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setTitle(_ title: String) { titleView.text = title }
    
    private func setConstraints() {
        [titleView, newPasswordView, repeatNewPasswordView, changeView, closeView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            titleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            titleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            newPasswordView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 30),
            newPasswordView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 10),
            newPasswordView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -10),
            newPasswordView.heightAnchor.constraint(equalToConstant: 52),
            newPasswordView.widthAnchor.constraint(equalTo: widthAnchor, constant: -20),
            
            repeatNewPasswordView.topAnchor.constraint(equalTo: newPasswordView.bottomAnchor, constant: 20),
            repeatNewPasswordView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 10),
            repeatNewPasswordView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -10),
            repeatNewPasswordView.heightAnchor.constraint(equalToConstant: 52),
            repeatNewPasswordView.widthAnchor.constraint(equalTo: widthAnchor, constant: -20),
            
            changeView.topAnchor.constraint(equalTo: repeatNewPasswordView.bottomAnchor, constant: 20),
            changeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            changeView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            changeView.heightAnchor.constraint(equalToConstant: 40),
            changeView.widthAnchor.constraint(equalTo: widthAnchor),
            
            closeView.topAnchor.constraint(equalTo: changeView.bottomAnchor, constant: 6),
            closeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            closeView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            closeView.heightAnchor.constraint(equalToConstant: 40),
            closeView.widthAnchor.constraint(equalTo: widthAnchor),
            closeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
        ])
    }
    
    @objc private func viewTapped() {
        if newPasswordView.isFirstResponder { newPasswordView.resignFirstResponder()}
        if repeatNewPasswordView.isFirstResponder { repeatNewPasswordView.resignFirstResponder()}
    }
    
    @objc private func changeViewTapped() {
        if (newPasswordView.text ?? "").isEmpty { newPasswordView.showValidationError() }
        else if newPasswordView.text != repeatNewPasswordView.text { repeatNewPasswordView.showValidationError() }
        else { delegate?.changePasswordViewTapped(value: newPasswordView.text!) }
    }
    
    @objc private func closeViewTapped() {
        delegate?.closeViewTapped()
    }
}


extension XChangePasswordAlertView: XTextFieldDelegete {
    func xTextFieldShouldReturn(_ textField: UITextField) {
        if textField == newPasswordView { repeatNewPasswordView.becomeFirstResponder() }
        else { changeViewTapped() }
    }
    
    func xTextFieldDidChangeSelection(_ textField: UITextField) {
        if newPasswordView == textField { _ = newPasswordView.textFieldShouldBeginEditing(newPasswordView) }
        else if repeatNewPasswordView == textField { _ = repeatNewPasswordView.textFieldShouldBeginEditing(repeatNewPasswordView) }
    }
}
