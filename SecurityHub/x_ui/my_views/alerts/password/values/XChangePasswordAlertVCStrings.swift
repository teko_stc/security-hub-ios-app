//
//  XChangePasswordAlertVCStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XChangePasswordAlertVCStrings: XChangePasswordAlertViewStringsProtocol {
    
    /// Новый пароль
    var newPassword: String { "N_USER_NEW_PASS_HINT".localized() }
    
    /// Повторите пароль
    var repeatNewPassword: String { "DOMAIN_USER_ENTER_PASS_REPEAT".localized() }
    
    /// Изменить
    var changePassword: String { "SITE_NAME_CHANGE".localized() }
    
    /// Закрыть
    var close: String { "CLOSE_POPUP".localized() }
}
