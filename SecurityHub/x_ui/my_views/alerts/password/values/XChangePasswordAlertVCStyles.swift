//
//  XChangePasswordAlertVCStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XChangePasswordAlertView {
    func viewStyle() -> XChangePasswordAlertViewStyle {
        XChangePasswordAlertViewStyle(
            backgroundColor: .white,
            titleView: XBaseLableStyle(
                color: UIColor.colorFromHex(0x414042),
                font: UIFont(name: "Open Sans", size: 18.5),
                alignment: .center
            ),
            passwordView: XTextFieldStyle(
                backgroundColor: .white,
                unselectColor: UIColor.colorFromHex(0xbcbcbc),
                selectColor: UIColor.colorFromHex(0x3abeff),
                textColor: UIColor.colorFromHex(0x414042),
                errorColor: UIColor.colorFromHex(0xf9543e),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            changeView: XZoomButtonStyle(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.colorFromHex(0x414042)
            ),
            closeView: XZoomButtonStyle(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.colorFromHex(0xf9543e)
            )
        )
    }
}
