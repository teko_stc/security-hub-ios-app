//
//  XChangePasswordAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XChangePasswordAlertController: XBaseAlertController<XChangePasswordAlertView> {
    override var widthAnchor: CGFloat { 0.8 }
    
    private let confirmVoid: (_: String) -> ()
    private let cancelVoid: (()->())?
    
    init(title: String, confirm: @escaping (_: String) -> (), cancel: (()->())? = nil) {
        self.confirmVoid = confirm
        self.cancelVoid = cancel
        super.init()
        self.title = title
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.setTitle(title ?? "")
        mainView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) { verticalAlignment = .top }
    
    @objc func keyboardWillDissmiss(notification: Notification) { verticalAlignment = .center }
}

extension XChangePasswordAlertController: XChangePasswordAlertViewDelegate {
    func changePasswordViewTapped(value: String) {
        dismiss(animated: true, completion: { self.confirmVoid(value) })
    }
    
    func closeViewTapped() {
        dismiss(animated: true, completion: { self.cancelVoid?() })
    }
}

