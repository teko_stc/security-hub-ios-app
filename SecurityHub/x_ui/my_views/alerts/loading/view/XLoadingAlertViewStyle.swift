//
//  XLoadingAlerViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 08.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XLoadingAlertViewStyle {
    let backgroundColor: UIColor
    let loaderColor: UIColor
}

extension XLoadingAlertView {
    typealias Style = XLoadingAlertViewStyle
}
