//
//  XLoadingAlertView.swift
//  SecurityHub
//
//  Created by Daniil on 08.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XLoadingAlertView: UIView {
    
    private var loaderView: XLoaderView = XLoaderView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        initViews(viewStyles())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        loaderView.color = style.loaderColor
        addSubview(loaderView)
    }
    
    private func setConstraints() {
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            loaderView.widthAnchor.constraint(equalToConstant: 40),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.centerXAnchor.constraint(equalTo: centerXAnchor),
            loaderView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            loaderView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        ])
    }
}
