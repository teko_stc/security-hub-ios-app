//
//  XLoadingAlertController.swift
//  SecurityHub
//
//  Created by Daniil on 08.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XLoadingAlertController: XBaseAlertController<XLoadingAlertView> {
    
    override var widthAnchor: CGFloat { 0.3 }
    
    override init() {
        super.init()
        
        canDismiss = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
