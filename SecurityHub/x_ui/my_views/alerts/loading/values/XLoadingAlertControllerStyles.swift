//
//  XLoadingAlertControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 08.01.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XLoadingAlertView {
    func viewStyles() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            loaderColor: UIColor.colorFromHex(0x414042)
        )
    }
}
