//
//  XSelectorAlertViewCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XSelectorAlertViewStyle: XSelectorAlertViewStyleProtocol {
    var backgroundColor: UIColor { .white }
    
    var titleView: XBaseLableStyle {
        XBaseLableStyle(color: UIColor.colorFromHex(0x404142), font: UIFont(name: "Open Sans", size: 20), alignment: .center)
    }
    
    var cell: XSelectorAlertViewCellStyleProtocol { XSelectorAlertViewCellStyle() }
    
    var negativeView: XZoomButtonStyle {
        XZoomButtonStyle(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x404142))
    }
    
    var positiveView: XZoomButtonStyle {
        XZoomButtonStyle(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 20), color: UIColor.colorFromHex(0x3abeff))
    }
}

class XSelectorAlertViewCellStyle: XSelectorAlertViewCellStyleProtocol {
    var selectedColor: UIColor { UIColor.colorFromHex(0xefefef) }
    
    var unselectedBorderIconColor: UIColor { UIColor.colorFromHex(0xbcbcbc) }
    
    var selectedBorderIconColor: UIColor { UIColor.colorFromHex(0x404142) }
    
    var selectedIconColor: UIColor { UIColor.colorFromHex(0x3abeff) }
    
    var titleView: XBaseLableStyle { XBaseLableStyle(color: UIColor.colorFromHex(0x404142), font: UIFont(name: "Open Sans", size: 15.5)) }
}
