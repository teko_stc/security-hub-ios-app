//
//  XSelectorAlertViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSelectorAlertViewCellStyleProtocol {
    var selectedColor: UIColor { get }
    var unselectedBorderIconColor: UIColor { get }
    var selectedBorderIconColor: UIColor { get }
    var selectedIconColor: UIColor { get }
    var titleView: XBaseLableStyle { get }
}

class XSelectorAlertViewCell: UITableViewCell {
    static let identifier = "XSelectorAlertViewCell.identifier"
    
    private var chekView: UIView = UIView()
    private var ovalLayer: CALayer = CALayer()
    private var titleView: UILabel = UILabel()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectedBackgroundView = UIView()
        selectedBackgroundView?.layer.shadowRadius = 10
        
        chekView.layer.cornerRadius = 6
        chekView.layer.borderWidth = 1
        contentView.addSubview(chekView)
        
        ovalLayer.cornerRadius = 4
        ovalLayer.frame = CGRect(x: 2, y: 2, width: 8, height: 8)
        chekView.layer.addSublayer(ovalLayer)
        
        titleView.numberOfLines = 2
        contentView.addSubview(titleView)
        
        [chekView, titleView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            chekView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            chekView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            chekView.heightAnchor.constraint(equalToConstant: 12),
            chekView.widthAnchor.constraint(equalToConstant: 12),

            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: chekView.trailingAnchor, constant: 10),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            titleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        ])
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(style: XSelectorAlertViewCellStyleProtocol, title: String, isSelected: Bool) {
        titleView.text = title
        self.isSelected = isSelected
        setStyle(style: style)
    }
    
    private func setStyle(style: XSelectorAlertViewCellStyleProtocol) {
        contentView.backgroundColor = .clear
        selectedBackgroundView?.layer.backgroundColor = style.selectedColor.cgColor
        chekView.layer.borderColor = (isSelected ? style.selectedBorderIconColor : style.unselectedBorderIconColor).cgColor
        ovalLayer.backgroundColor = (isSelected ? style.selectedIconColor : .clear).cgColor
        titleView.setStyle(style.titleView)
    }
}
