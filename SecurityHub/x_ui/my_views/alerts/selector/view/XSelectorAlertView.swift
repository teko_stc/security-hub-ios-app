//
//  XSelectorAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XSelectorAlertViewDelegate {
    func positiveViewTapped(selectedValue: Any?)
    func negativeViewTapped()
}

protocol XSelectorAlertViewLayerModelProtocol {
    var title: String { get set }
    var value: Any? { get set }
}

protocol XSelectorAlertViewLayer {
    func setData(title: String, negativeText: String, positiveText: String, items: [XSelectorAlertViewLayerModelProtocol], selectedIndex: Int, buttonsOrientation: CTFontOrientation)
}

protocol XSelectorAlertViewStyleProtocol {
    var backgroundColor: UIColor { get }
    var titleView: XBaseLableStyle { get }
    var cell: XSelectorAlertViewCellStyleProtocol { get }
    var negativeView: XZoomButtonStyle { get }
    var positiveView: XZoomButtonStyle { get }
}

class XSelectorAlertView: UIView {
    
    public var delegate: XSelectorAlertViewDelegate?
    
    private var titleView: UILabel!
    private var tableView: XDynamicSizeTableView!
    private var negativeView, positiveView: XZoomButton!
    
    private var style: XSelectorAlertViewStyleProtocol = XSelectorAlertViewStyle()
    private var items: [XSelectorAlertViewLayerModelProtocol] = []
    private var selectedIndex: Int = -1
    private var buttonsOrientation: CTFontOrientation = .horizontal
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        titleView = UILabel.create(style.titleView)
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        tableView = XDynamicSizeTableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = style.backgroundColor
        tableView.register(XSelectorAlertViewCell.self, forCellReuseIdentifier: XSelectorAlertViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        addSubview(tableView)
        
        positiveView = XZoomButton(style: style.positiveView)
        positiveView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(positiveView)
        
        negativeView = XZoomButton(style: style.negativeView)
        negativeView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(negativeView)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setConstraints() {
        [titleView, tableView, negativeView, positiveView].forEach({ $0?.translatesAutoresizingMaskIntoConstraints = false })
        
        var constraints: [NSLayoutConstraint] = [
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),

            tableView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ]
        
        constraints.append(contentsOf: (buttonsOrientation == .horizontal) ? [
            negativeView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 10),
            negativeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            positiveView.topAnchor.constraint(equalTo: negativeView.topAnchor),
            positiveView.leadingAnchor.constraint(equalTo: negativeView.trailingAnchor),
            positiveView.trailingAnchor.constraint(equalTo: trailingAnchor),
            positiveView.widthAnchor.constraint(equalTo: negativeView.widthAnchor),
            positiveView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ] : [
            positiveView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 6),
            positiveView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            positiveView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            negativeView.heightAnchor.constraint(equalToConstant: 40),
            positiveView.widthAnchor.constraint(equalTo: widthAnchor),
            
            negativeView.topAnchor.constraint(equalTo: positiveView.bottomAnchor, constant: 6),
            negativeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
            negativeView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            negativeView.heightAnchor.constraint(equalToConstant: 40),
            negativeView.widthAnchor.constraint(equalTo: widthAnchor),
            negativeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6),
        ])
        
        NSLayoutConstraint.activate(constraints)
    }
 
    @objc private func viewTapped(view: UIView) {
        if (view == negativeView) { delegate?.negativeViewTapped() }
        else if view == positiveView { delegate?.positiveViewTapped(selectedValue: self.items[self.selectedIndex].value) }
    }
}

extension XSelectorAlertView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { self.items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XSelectorAlertViewCell.identifier, for: indexPath) as! XSelectorAlertViewCell
        cell.setContent(style: style.cell, title: self.items[indexPath.row].title, isSelected: selectedIndex == indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var indexes = [indexPath]
        if (selectedIndex != -1) { indexes.append(IndexPath(row: selectedIndex, section: 0)) }
        selectedIndex = indexPath.row
        tableView.reloadRows(at: indexes, with: .fade)
        tableView.deselectRow(at: indexPath, animated: false)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
    }
}

extension XSelectorAlertView: XSelectorAlertViewLayer {
    func setData(title: String, negativeText: String, positiveText: String, items: [XSelectorAlertViewLayerModelProtocol], selectedIndex: Int, buttonsOrientation: CTFontOrientation) {
        titleView.text = title
        negativeView.text = negativeText
        positiveView.text = positiveText
        
        self.items = items
        self.selectedIndex = selectedIndex
        self.buttonsOrientation = buttonsOrientation
        
        tableView.reloadData()
        setConstraints()
    }
}
 
