//
//  XSelectorAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 15.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XSelectorAlertControllerModel: XSelectorAlertViewLayerModelProtocol {
    var title: String
    var value: Any?
}

class XSelectorAlertController: XBaseAlertController<XSelectorAlertView>, XSelectorAlertViewDelegate {
    override var widthAnchor: CGFloat { 0.8 }
    
    public var onPositiveViewTapped: ((_ selectedValue: Any?) -> ())?

    private lazy var viewLayer: XSelectorAlertViewLayer = self.mainView
    
    init(title: String, negativeText: String, positiveText: String, items: [XSelectorAlertControllerModel], selectedIndex: Int, buttonsOrientation: CTFontOrientation = .horizontal) {
        super.init()
        mainView.delegate = self
        viewLayer.setData(title: title, negativeText: negativeText, positiveText: positiveText, items: items, selectedIndex: selectedIndex, buttonsOrientation: buttonsOrientation)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func positiveViewTapped(selectedValue: Any?) {
        dismiss(animated: true, completion: {
            self.onPositiveViewTapped?(selectedValue)
        })
    }
    
    func negativeViewTapped() {
        dismiss(animated: true)
    }
}
