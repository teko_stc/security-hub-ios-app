//
//  XPinCodeAlertVCStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XPinCodeAlertView {
    func viewStyles() -> XPinCodeAlertViewStyle {
        XPinCodeAlertViewStyle(
            backgroundColor: .white,
            titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 18.5), alignment: .center),
            subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0xbcbcbc), font: UIFont(name: "Open Sans", size: 15.5), alignment: .center),
            paginatorTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5), alignment: .center),
            paginatorView: XPinCodeViewStyle.Pagination(select: UIColor.colorFromHex(0x3abeff), unselect: UIColor.colorFromHex(0xbcbcbc), error: UIColor.colorFromHex(0xf9543a)),
            cancelView: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 18.5), color: UIColor.colorFromHex(0x414042)),
            callView: XZoomButton.Style(backgroundColor: .clear, font: UIFont(name: "Open Sans", size: 18.5), color: UIColor.colorFromHex(0x3abeff))
        )
    }
}
