//
//  XPinCodeAlertVCStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XPinCodeAlertVCStrings: XPinCodeAlertViewStringsProtocol {
    
    /// Техническая поддержка
    var title: String { "TECH_SUPPORT".localized() }
    
    /// Хотите обратиться в техническую поддержку?
    var subTitle: String { "MA_CALL_TITLE".localized() }
    
    /// Введите PIN-код:
    var paginatorTitle: String { "WIZ_REG_ENTER_PIN".localized() }
    
    /// Отмена
    var cancel: String { "CANCEL_SMALL".localized() }
    
    /// Позвонить
    var call: String { "SEC_COMP_CALL".localized() }
}
