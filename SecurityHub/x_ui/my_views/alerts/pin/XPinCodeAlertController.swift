//
//  XPinCodeAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XPinCodeAlertController: XBaseAlertController<XPinCodeAlertView> {
    override var widthAnchor: CGFloat { 0.8 }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verticalAlignment = .top
        mainView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) { verticalAlignment = .top }
    
    @objc func keyboardWillDissmiss(notification: Notification) { verticalAlignment = .center }
}

extension XPinCodeAlertController: XPinCodeAlertViewDelegate {
    func cancelViewTapped() {
        dismiss(animated: true)
    }
    
    func callViewTapped(pin: String) {
        if pin != DataManager.defaultHelper.pin { mainView.pinError() }
        else {
            UIApplication.shared.open(URL(string: "tel://88001008945")!, completionHandler: { _ in self.dismiss(animated: true) })
        }
    }
}
