//
//  XPinCodeAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AudioToolbox

protocol XPinCodeAlertViewDelegate {
    func cancelViewTapped()
    func callViewTapped(pin: String)
}

protocol XPinCodeAlertViewStringsProtocol {
    var title: String { get }
    var subTitle: String { get }
    var paginatorTitle: String { get }
    var cancel: String { get }
    var call: String { get }
}

struct XPinCodeAlertViewStyle {
    let backgroundColor: UIColor
    let titleView, subTitleView, paginatorTitleView: XBaseLableStyle
    let paginatorView: XPinCodeViewStyle.Pagination
    let cancelView, callView: XZoomButton.Style
}

class XPinCodeAlertView: UIView, UITextFieldDelegate{
    public var delegate: XPinCodeAlertViewDelegate?
    
    private var titleView, subTitleView: UILabel!
    private var pagTitleView: UILabel!, pagView: [UIView] = [], pagViewCenterXAnchors: [NSLayoutConstraint] = []
    private var cancelView, callView: XZoomButton!
    private var hiddenView: UITextField!
    private var errorK: CGFloat? = nil
    
    private lazy var style: XPinCodeAlertViewStyle = self.viewStyles()
    private lazy var strings: XPinCodeAlertViewStringsProtocol = XPinCodeAlertVCStrings()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = style.backgroundColor
        
        hiddenView = UITextField()
        hiddenView.keyboardType = .numberPad
        hiddenView.delegate = self
        addSubview(hiddenView)
        hiddenView.becomeFirstResponder()
        
        titleView = UILabel.create(style.titleView)
        titleView.text = strings.title
        addSubview(titleView)
        
        subTitleView = UILabel.create(style.subTitleView)
        subTitleView.numberOfLines = 0
        subTitleView.text = strings.subTitle
        addSubview(subTitleView)
        
        pagTitleView = UILabel.create(style.paginatorTitleView)
        pagTitleView.text = strings.paginatorTitle
        addSubview(pagTitleView)
        
        for _ in 0...3 {
            let _view = UIView()
            _view.backgroundColor = style.paginatorView.unselect
            _view.layer.cornerRadius = 4
            addSubview(_view)
            pagView.append(_view)
        }
        
        cancelView = XZoomButton(style: style.cancelView)
        cancelView.text = strings.cancel
        cancelView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(cancelView)
        
        callView = XZoomButton(style: style.callView)
        callView.text = strings.call
        callView.addTarget(self, action: #selector(viewTapped), for: .touchUpInside)
        addSubview(callView)
        
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewTapped(view: callView)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if (textField.text?.count ?? 0) > 4 { return }
        updatePaginationState(count: textField.text?.count ?? 0)
    }
   
    public func pinError() {
        errorK = -20
        pinErrorAnimation()
    }
    
    private func setConstraints() {
        [titleView, subTitleView, pagTitleView, cancelView, callView].forEach({ $0.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 10),
            subTitleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            subTitleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            pagTitleView.topAnchor.constraint(equalTo: subTitleView.bottomAnchor, constant: 30),
            pagTitleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            pagTitleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
        ])
        for i in 0...(pagView.count - 1) {
            let k = CGFloat(Float(i) - Float(pagView.count) / 2 + 0.5)
            pagView[i].translatesAutoresizingMaskIntoConstraints = false
            pagView[i].topAnchor.constraint(equalTo: pagTitleView.bottomAnchor, constant: 20).isActive = true
            pagViewCenterXAnchors.append(pagView[i].centerXAnchor.constraint(equalTo: centerXAnchor, constant: k * 40))
            pagViewCenterXAnchors[i].isActive = true
            pagView[i].heightAnchor.constraint(equalToConstant: 8).isActive = true
            pagView[i].widthAnchor.constraint(equalToConstant: 8).isActive = true
        }
        NSLayoutConstraint.activate([
            cancelView.topAnchor.constraint(equalTo: pagView[0].bottomAnchor, constant: 30),
            cancelView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cancelView.heightAnchor.constraint(equalToConstant: 40),
            cancelView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            callView.topAnchor.constraint(equalTo: cancelView.topAnchor),
            callView.leadingAnchor.constraint(equalTo: cancelView.trailingAnchor),
            callView.trailingAnchor.constraint(equalTo: trailingAnchor),
            callView.heightAnchor.constraint(equalTo: cancelView.heightAnchor),
            callView.widthAnchor.constraint(equalTo: cancelView.widthAnchor),
        ])
    }
    
    @objc private func viewTapped(view: UIView) {
        if view == cancelView { delegate?.cancelViewTapped() }
        else if view == callView { delegate?.callViewTapped(pin: String((hiddenView.text ?? "").prefix(4))) }
    }
    
    private func updatePaginationState(count: Int) {
        UIView.animate(withDuration: 0.2, animations: {
            if count > 0 { for i in 0...count-1 { self.pagView[i].backgroundColor = self.style.paginatorView.select } }
            if (count >= self.pagView.count) { return }
            for i in count...self.pagView.count-1 { self.pagView[i].backgroundColor = self.style.paginatorView.unselect }
        })
    }
    
    private func pinErrorAnimation() {
        guard let errorK = errorK else { return }
        hiddenView.text = ""
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        for i in 0...(pagView.count - 1) {
            let k = CGFloat(Float(i) - Float(pagView.count) / 2 + 0.5)
            pagViewCenterXAnchors[i].constant = k * 40 + errorK * 2
        }
        UIView.animate(
            withDuration: 0.1,
            animations: {
                self.pagView.forEach { view in view.backgroundColor = self.style.paginatorView.error }
                self.layoutIfNeeded()
            },
            completion: { _ in
                if errorK == -20 { self.errorK = 20 }
                else if errorK == 20 { self.errorK = -10 }
                else if errorK == -10 { self.errorK = 10 }
                else if errorK == 10 { self.errorK = 0 }
                else if errorK == 0 {
                    self.errorK = nil;
                    self.updatePaginationState(count: 0);
                }
                self.pinErrorAnimation()
            })
    }
}
