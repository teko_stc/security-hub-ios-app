//
//  XMultiChoiceAlertController.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XMultiChoiceAlertController: XBaseAlertController<XMultiChoiceAlertView> {
    
    override var widthAnchor: CGFloat { 0.8 }
    
    private var onPositiveClick: ((_ checkValues: [Any]) -> ())!
    private var onNegativeClick: (() -> ())? = nil
    
    init(
        title: String,
        items: [XMultiChoiceAlertView.Cell.Model],
        positiveText: String, negativeText: String,
        onPositiveClick: @escaping (_ checkValues: [Any]) -> (), onNegativeClick: (() -> ())? = nil
    ) {
        super.init()
        
        mainView.setData(title: title, items: items, positive: positiveText, negative: negativeText)
        mainView.setDelegate(self)
        
        self.onPositiveClick = onPositiveClick
        self.onNegativeClick = onNegativeClick
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension XMultiChoiceAlertController: XMultiChoiceAlertViewDelegate {
    
    func positiveClicked(_ items: [XMultiChoiceAlertView.Cell.Model]) {
        dismiss(animated: true) {
            self.onPositiveClick(
                items.filter { $0.isChecked }
                    .map { $0.value }
            )
        }
    }
    
    func negativeClicked() {
        dismiss(animated: true) {
            self.onNegativeClick?()
        }
    }
}
