//
//  XMultiChoiceAlertViewStyle.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XMultiChoiceAlertViewStyle {
    let backgroundColor: UIColor
    
    let titleView: XBaseLableStyle
    let negativeView, positiveView: XZoomButtonStyle
}

extension XMultiChoiceAlertView {
    typealias Style = XMultiChoiceAlertViewStyle
}
