//
//  XMultiChoiceAlertView.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XMultiChoiceAlertView: UIView {
    
    private var delegate: XMultiChoiceAlertViewDelegate!
    
    private var items: [Cell.Model] = []
    
    private var titleView: UILabel!
    private var tableView: XDynamicSizeTableView = XDynamicSizeTableView()
    private var negativeView, positiveView: XZoomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews(style())
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.backgroundColor
        
        titleView = UILabel.create(style.titleView)
        addSubview(titleView)
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = style.backgroundColor
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        addSubview(tableView)
        
        negativeView = XZoomButton(style: style.negativeView)
        negativeView.addTarget(self, action: #selector(negativeClicked), for: .touchUpInside)
        addSubview(negativeView)
        
        positiveView = XZoomButton(style: style.positiveView)
        positiveView.addTarget(self, action: #selector(positiveClicked), for: .touchUpInside)
        addSubview(positiveView)
    }
    
    private func setConstraints() {
        [titleView, tableView, negativeView, positiveView].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),

            tableView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 16),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            negativeView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 16),
            negativeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            positiveView.topAnchor.constraint(equalTo: negativeView.topAnchor),
            positiveView.leadingAnchor.constraint(equalTo: negativeView.trailingAnchor),
            positiveView.trailingAnchor.constraint(equalTo: trailingAnchor),
            positiveView.widthAnchor.constraint(equalTo: negativeView.widthAnchor),
            positiveView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
    }
    
    public func setData(title: String, items: [Cell.Model], positive: String, negative: String) {
        self.items = items
        tableView.reloadData()
        
        titleView.text = title
        positiveView.text = positive
        negativeView.text = negative
    }
    
    public func setDelegate(_ delegate: XMultiChoiceAlertViewDelegate) {
        self.delegate = delegate
    }
    
    @objc private func positiveClicked() {
        delegate.positiveClicked(items)
    }
    
    @objc private func negativeClicked() {
        delegate.negativeClicked()
    }
}

extension XMultiChoiceAlertView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        
        cell.setData(items[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        items[indexPath.row].isChecked = !items[indexPath.row].isChecked
        
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
