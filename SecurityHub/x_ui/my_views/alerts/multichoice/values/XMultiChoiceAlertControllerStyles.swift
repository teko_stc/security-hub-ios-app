//
//  XMultiChoiceAlertControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

extension XMultiChoiceAlertView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            titleView: XBaseLableStyle(
                color: UIColor.colorFromHex(0x404142),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .center
            ),
            negativeView: XZoomButtonStyle(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x404142)
            ),
            positiveView: XZoomButtonStyle(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.colorFromHex(0x3ABEFF)
            )
        )
    }
}
