//
//  XMultiChoiceAlertCellStyle.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

struct XMultiChoiceAlertCellStyle {
    let activeCheckBoxColor, disableCheckBoxColor: UIColor
    
    let title, subTitle: XBaseLableStyle
}

extension XMultiChoiceAlertCell {
    typealias Style = XMultiChoiceAlertCellStyle
    
    func cellStyle() -> Style {
        return Style(
            activeCheckBoxColor: UIColor.colorFromHex(0x3ABEFF),
            disableCheckBoxColor: UIColor.colorFromHex(0xBCBCBC),
            title: XBaseLableStyle(
                color: UIColor.colorFromHex(0x404142),
                font: UIFont(name: "Open Sans", size: 20)
            ),
            subTitle: XBaseLableStyle(
                color: UIColor.colorFromHex(0xBCBCBC),
                font: UIFont(name: "Open Sans", size: 15.5)
            )
        )
    }
}
