//
//  XMultiChoiceAlertCell.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import UIKit

class XMultiChoiceAlertCell: UITableViewCell {
    
    public static let identifier = "XMultiChoiceAlertCell.identifier"
    
    private var style: Style!
    
    private var checkBox: UIImageView = UIImageView()
    private var titleView, subTitleView: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.style = cellStyle()
        
        initViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews() {
        addSubview(checkBox)
        
        titleView = UILabel.create(style.title)
        addSubview(titleView)
        
        subTitleView = UILabel.create(style.subTitle)
        addSubview(subTitleView)
    }
    
    private func setConstraints() {
        [checkBox, titleView, subTitleView].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            titleView.leadingAnchor.constraint(equalTo: checkBox.trailingAnchor, constant: 16),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -22),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            subTitleView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            subTitleView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor),
            subTitleView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            
            checkBox.widthAnchor.constraint(equalToConstant: 18),
            checkBox.heightAnchor.constraint(equalToConstant: 18),
            checkBox.centerYAnchor.constraint(equalTo: centerYAnchor),
            checkBox.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 22),
        ])
    }
    
    public func setData(_ model: Model) {
        checkBox.image = UIImage(named: model.isChecked ? "checkbox_selected" : "checkbox_unselected")?.withRenderingMode(.alwaysTemplate)
        
        checkBox.tintColor = model.isChecked ? style.activeCheckBoxColor : style.disableCheckBoxColor
        
        titleView.text = model.title
        subTitleView.text = model.subTitle
    }
}

extension XMultiChoiceAlertView {
    typealias Cell = XMultiChoiceAlertCell
}
