//
//  XMultiChoiceAlertCellModel.swift
//  SecurityHub
//
//  Created by Daniil Shchepkin on 28.05.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

struct XMultiChoiceAlertCellModel {
    let title: String
    let subTitle: String
    
    var isChecked: Bool
    let value: Any
}

extension XMultiChoiceAlertCell {
    typealias Model = XMultiChoiceAlertCellModel
}
