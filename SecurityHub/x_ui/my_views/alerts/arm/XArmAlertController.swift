//
//  XArmAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

enum XArmAlertControllerType {
    case security
    case relay
    case tech
    case sos
}

class XArmAlertController: XBaseAlertController<XArmAlertView>, XArmAlertViewDelegate {
    override var widthAnchor: CGFloat { 0.8 }
		
    public var onArmed: (() -> ())?
    public var onDisarmed: (() -> ())?
    public var onInfoTapped: (() -> ())?
		public var onSos: (((latitude: Double,longitude: Double)?) -> ())?
 
    private let type: XArmAlertControllerType
    private let deviceName: String
    private let siteName: String
    private let hideInfoButton: Bool
		private var locationManager: LocationManager?
		private var location: (latitude: Double,longitude: Double)?
    
    private lazy var viewLayer: XArmAlertViewLayer = self.mainView
    
    init(type: XArmAlertControllerType, deviceName: String, siteName: String, hideInfoButton: Bool = false, onArmed: (() -> ())? = nil, onDisarmed: (() -> ())? = nil, onInfoTapped: (() -> ())? = nil) {
        self.type = type
        self.deviceName = deviceName
        self.siteName = siteName
        self.hideInfoButton = hideInfoButton
        self.onArmed = onArmed
        self.onDisarmed = onDisarmed
        self.onInfoTapped = onInfoTapped
        super.init()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        switch type {
        case .security:
            viewLayer.showSecurityViews()
        case .relay:
            viewLayer.showRelayViews()
        case .tech:
            viewLayer.showTechViews()
        case .sos:
					viewLayer.showSOSViews()
					if DataManager.settingsHelper.sendLocation == true {
						viewLayer.showWaitLocation()
						locationManager = LocationManager()
						let needLocation = locationManager!.requestUserLocation { location, error in
							if let err = error {
								self.viewLayer.showReceivedLocation(message: err) }
							else {
								self.location = location
								self.viewLayer.showReceivedLocation(message: nil) }
						}
						if !needLocation { self.viewLayer.showReceivedLocation(message: nil) }
					}
        }
        viewLayer.setTitles(title: deviceName, subTitle: siteName)
        if hideInfoButton {
            viewLayer.hideInfoView()
        }
    }
	
		override func viewWillDisappear(_ animated: Bool) {
			super.viewWillDisappear(animated)
			
			if let locationManager = locationManager {
				locationManager.stopUpdateLocation()
			}
		}
    
    func armViewTapped() {
        dismiss(animated: true) {
            self.onArmed?()
						self.onSos?(self.location)
        }
    }
    
    func disarmViewTapped() {
        dismiss(animated: true) {
            self.onDisarmed?()
        }
    }
    
    func infoViewTapped() {
        dismiss(animated: true) {
            self.onInfoTapped?()
        }
    }
}
