//
//  XArmAlertViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XArmAlertView {
    func styles() -> Style {
        Style(
            backrgound: UIColor.white,
            title: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 18.5),
                alignment: .center
            ),
            subtitle: XBaseLableStyle(
                color: UIColor(red: 0xbe/255, green: 0xbe/255, blue: 0xbe/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 11.3),
                alignment: .center
            ),
            action: XVerticalButton.Style(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            info: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.white
            )
        )
    }
}

