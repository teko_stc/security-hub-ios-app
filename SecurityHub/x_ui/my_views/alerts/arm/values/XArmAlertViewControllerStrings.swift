//
//  XArmAlertViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XArmAlertViewStrings {
    
    /// Подробнее
    var info: String { "N_TOP_INFO_SCRIPT_INFO".localized() }
    
    /// Взять
    var arm: String { "N_BUTTON_ARM".localized() }
    
    /// Снять
    var disarm: String { "N_BUTTON_DISARM".localized() }
    
    /// Вкл
    var on: String { "N_BUTTON_ON".localized() }
    
    /// Выкл
    var off: String { "N_BUTTON_OFF".localized() }
    
    /// Сбросить
    var reset: String { "N_BUTTON_RESET".localized() }
	
		/// Определям геопозицию
		var detectLocation: String { "PANIC_DETECT_LOCATION".localized() }
}
