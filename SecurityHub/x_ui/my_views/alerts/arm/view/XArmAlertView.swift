//
//  XArmAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RappleProgressHUD

protocol XArmAlertViewLayer {
    func setTitles(title: String, subTitle: String)
    func showSecurityViews()
    func showRelayViews()
    func showTechViews()
    func hideInfoView()
    func showSOSViews()
		func showWaitLocation()
		func showReceivedLocation(message: String?)
}

class XArmAlertView: UIView {
    public var delegate: XArmAlertViewDelegate?
    
    private var titleView, subTitleView, messageView: UILabel!
    private var armView, disarmView: XVerticalButton!
    private var infoView: XZoomButton!
		private var loaderView = XLoaderView()
    private var infoViewHeightConstraint, infoViewBottomConstraint, disarmViewLeadingConstraint, armViewTrailingConstraint: NSLayoutConstraint!
    
    private lazy var style: Style = styles()
    private lazy var strings: XArmAlertViewStrings = XArmAlertViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.backrgound
        
        titleView = UILabel.create(style.title)
        addSubview(titleView)
        
        subTitleView = UILabel.create(style.subtitle)
        subTitleView.numberOfLines = 0
        addSubview(subTitleView)
			
				messageView = UILabel.create(style.subtitle)
				messageView.numberOfLines = 0
				addSubview(messageView)
        
        armView = XVerticalButton()
        armView.setStyle(style.action)
        armView.onTapped = { self.delegate?.armViewTapped() }
        addSubview(armView)
			
				loaderView.color = UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
				loaderView.isHidden = true
				addSubview(loaderView)
        
        disarmView = XVerticalButton()
        disarmView.setStyle(style.action)
        disarmView.onTapped = { self.delegate?.disarmViewTapped() }
        addSubview(disarmView)
        
        infoView = XZoomButton(style: style.info)
        infoView.text = strings.info
        infoView.addTarget(self, action: #selector(infoViewTapped), for: .touchUpInside)
        addSubview(infoView)
    }

    private func setConstraints() {
        [titleView, subTitleView, armView, disarmView, infoView, loaderView, messageView].forEach { view in view.translatesAutoresizingMaskIntoConstraints = false }
        infoViewHeightConstraint = infoView.heightAnchor.constraint(equalToConstant: 40)
        infoViewBottomConstraint = infoView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        disarmViewLeadingConstraint = disarmView.leadingAnchor.constraint(equalTo: armView.trailingAnchor, constant: 10)
        armViewTrailingConstraint = armView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),

            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 0),
            subTitleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            subTitleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
						
						messageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
						messageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
						messageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            armView.topAnchor.constraint(equalTo: subTitleView.bottomAnchor, constant: 20),
            armView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            armView.heightAnchor.constraint(equalToConstant: 100),
						
						loaderView.topAnchor.constraint(equalTo: subTitleView.bottomAnchor, constant: 90),
						loaderView.centerXAnchor.constraint(equalTo: centerXAnchor),
						loaderView.widthAnchor.constraint(equalToConstant: 20),
						loaderView.heightAnchor.constraint(equalToConstant: 20),

            disarmView.topAnchor.constraint(equalTo: armView.topAnchor),
            disarmViewLeadingConstraint,
            disarmView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            disarmView.heightAnchor.constraint(equalTo: armView.heightAnchor),
            disarmView.widthAnchor.constraint(equalTo: armView.widthAnchor),
            
            infoView.topAnchor.constraint(equalTo: armView.bottomAnchor, constant: 20),
            infoView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            infoView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            infoViewHeightConstraint,
            infoViewBottomConstraint,
        ])
    }
    
    @objc private func infoViewTapped() {
        delegate?.infoViewTapped()
    }
}

extension XArmAlertView: XArmAlertViewLayer {
    func setTitles(title: String, subTitle: String) {
        titleView.text = title
        subTitleView.text = subTitle
    }
    
    func showSecurityViews() {
        armView.image = UIImage(named: "ic_arm")
        armView.title = strings.arm
        disarmView.image = UIImage(named: "ic_disarm")
        disarmView.title = strings.disarm
    }
    
    func showRelayViews() {
        armView.image = UIImage(named: "ic_on")
        armView.title = strings.on
        disarmView.image = UIImage(named: "ic_off")
        disarmView.title = strings.off
    }
    
    func showTechViews() {
        armView.image = UIImage(named: "ic_no_control")
        armView.title = strings.on
        disarmView.image = UIImage(named: "ic_control")
        disarmView.title = strings.off
    }
    
    func showSOSViews() {
        armView.image = UIImage(named: "ic_sos_b")
        armView.title = ""
        hideInfoView()
        hideDisarmView()
    }
	
		func showWaitLocation() {
				loaderView.isHidden = false
				messageView.textColor = subTitleView.textColor
				messageView.text = strings.detectLocation
		}
	
		func showReceivedLocation(message: String?) {
				loaderView.isHidden = true
				if message != nil {
					messageView.textColor = UIColor(red: 0xf9/255, green: 0x54/255, blue: 0x3e/255, alpha: 1) }
				messageView.text = message
		}

    func hideDisarmView() {
        disarmView.isHidden = true
        
        armViewTrailingConstraint.isActive = true
        disarmViewLeadingConstraint.isActive = false
    }
    
    func hideInfoView() {
        infoView.isHidden = true
        
        infoViewHeightConstraint.constant = 0
        infoViewBottomConstraint.constant = 0
    }
}
