//
//  XArmAlertViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XArmAlertViewStyle {
    let backrgound: UIColor
    let title, subtitle: XBaseLableStyle
    let action: XVerticalButton.Style
    let info: XZoomButton.Style
}
extension XArmAlertView {
    typealias Style = XArmAlertViewStyle
}
