//
//  XArmAlertViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 25.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XArmAlertViewDelegate {
    func armViewTapped()
    func disarmViewTapped()
    func infoViewTapped()
}
