//
//  XAlertViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XAlertViewStyle {
    struct Lable {
        let font: UIFont?
        let color: UIColor
        var alignment: NSTextAlignment = .center
    }
    
    let backgroundColor: UIColor
    let title: Lable
    var text: Lable? = nil
    var buttonOrientation: CTFontOrientation = .vertical
    let positive: XZoomButton.Style
    var negative: XZoomButton.Style? = nil
}
extension XAlertView {
    typealias Style = XAlertViewStyle
}
