//
//  XAlertViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XAlertViewDelegate {
    func XAlertViewPositiveButtonTapped()
    
    func XAlertViewNegativeButtonTapped()
}
