//
//  XAlertViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

struct XAlertViewStrings {
    var title: String? = nil
    var text: String? = nil
    let positive: String
    var negative: String? = nil
}
extension XAlertView {
    typealias Strings = XAlertViewStrings
}
