//
//  XAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertView: UIView {
    public var delegate: XAlertViewDelegate?
    
    private let titleView = UILabel()
    private let textView = UILabel()
    private var positiveView, negativeView: XZoomButton!
    
    public func build(style: Style, strings: Strings) {
        initViews(style: style, strings: strings)
        setConstraints(style: style, strings: strings)
    }
    
    private func initViews(style: Style, strings: Strings) {
        backgroundColor = style.backgroundColor
        
        titleView.numberOfLines = 0
        titleView.text = strings.title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        addSubview(titleView)
        
        textView.numberOfLines = 0
        if let style = style.text {
            textView.text = strings.text
            textView.font = style.font
            textView.textColor = style.color
            textView.textAlignment = style.alignment
        }
        addSubview(textView)
        
        positiveView = XZoomButton(style: style.positive)
        positiveView.text = strings.positive
        positiveView.addTarget(self, action: #selector(positiveViewTapped), for: .touchUpInside)
        addSubview(positiveView)

        negativeView = XZoomButton(style: style.negative ?? style.positive)
        negativeView.text = strings.negative ?? strings.positive
        negativeView.addTarget(self, action: #selector(negativeViewTapped), for: .touchUpInside)
        if (strings.negative == nil) { negativeView.isHidden = true }
        addSubview(negativeView)
    }
    
    private func setConstraints(style: Style, strings: Strings) {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        positiveView.translatesAutoresizingMaskIntoConstraints = false
        negativeView.translatesAutoresizingMaskIntoConstraints = false
        if (strings.title != nil) {
            NSLayoutConstraint.activate([
                titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
                titleView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
                titleView.centerXAnchor.constraint(equalTo: centerXAnchor),
                titleView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
                titleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
                
                textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 10),
                textView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
                textView.centerXAnchor.constraint(equalTo: centerXAnchor),
                textView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
                textView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            ])
        }
        let topOffset = strings.title != nil ? 20.0 : 10.0
        if (strings.text == nil) { textView.heightAnchor.constraint(equalToConstant: 0).isActive = true }
        if (style.buttonOrientation == .horizontal) {
            NSLayoutConstraint.activate([
                positiveView.topAnchor.constraint(equalTo: strings.title != nil ? textView.bottomAnchor : topAnchor, constant: topOffset),
                positiveView.heightAnchor.constraint(equalToConstant: 40),
                positiveView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
                positiveView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            ])
            if ( strings.negative == nil ) {
                positiveView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor).isActive = true
                positiveView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
            } else {
                NSLayoutConstraint.activate([
                    negativeView.topAnchor.constraint(equalTo: positiveView.topAnchor),
                    negativeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
                    negativeView.heightAnchor.constraint(equalToConstant: 40),
                    negativeView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
                    negativeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
                                                
                    positiveView.leadingAnchor.constraint(equalTo: negativeView.trailingAnchor),
                    positiveView.widthAnchor.constraint(equalTo: negativeView.widthAnchor)
                ])
            }
        } else {
            NSLayoutConstraint.activate([
                positiveView.topAnchor.constraint(equalTo: strings.title != nil ? textView.bottomAnchor : topAnchor, constant: topOffset),
                positiveView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
                positiveView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
                negativeView.heightAnchor.constraint(equalToConstant: 40),
                positiveView.widthAnchor.constraint(equalTo: widthAnchor),
            ])
            if ( strings.negative != nil ) {
                NSLayoutConstraint.activate([
                    negativeView.topAnchor.constraint(equalTo: positiveView.bottomAnchor, constant: 6),
                    negativeView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor),
                    negativeView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
                    negativeView.heightAnchor.constraint(equalToConstant: 40),
                    negativeView.widthAnchor.constraint(equalTo: widthAnchor),
                    negativeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
                ])
            } else {
                positiveView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
            }
        }
    }
    
    @objc private func positiveViewTapped() { delegate?.XAlertViewPositiveButtonTapped() }
    
    @objc private func negativeViewTapped() { delegate?.XAlertViewNegativeButtonTapped() }
}

