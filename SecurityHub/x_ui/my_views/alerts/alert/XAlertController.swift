//
//  XAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAlertController: XBaseAlertController<XAlertView> {
    override var widthAnchor: CGFloat { 0.8 }
    
    private let xAlertControllerPositiveButtonTapped: () -> ()

    private let xAlertControllerNegativeButtonTapped: (() -> ())?

    init(style: XAlertView.Style, strings: XAlertView.Strings, positive: @escaping () ->() = {}, negative: (() -> ())? = nil) {
        self.xAlertControllerPositiveButtonTapped = positive
        self.xAlertControllerNegativeButtonTapped = negative
        super.init()
        mainView.delegate = self
        mainView.build(style: style, strings: strings)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension XAlertController: XAlertViewDelegate {
    func XAlertViewPositiveButtonTapped() {
        dismiss(animated: true) {
            self.xAlertControllerPositiveButtonTapped()
        }
    }
    
    func XAlertViewNegativeButtonTapped() {
        dismiss(animated: true) {
            self.xAlertControllerNegativeButtonTapped?()
        }
    }
}

