//
//  XAlertControllerDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

@objc protocol XAlertControllerDelegate {
    @objc func xAlertControllerPositiveButtonTapped()
    
    @objc optional func xAlertControllerNegativeButtonTapped()
}
