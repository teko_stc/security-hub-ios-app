//
//  XPinPasswordAlertViewStyle.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XPinPasswordAlertViewStyle {
    let backgroundColor: UIColor
    
    let activeStepColor: UIColor
    let inactiveStepColor: UIColor
    
    let title: XBaseLableStyle
    let subtitle: XBaseLableStyle
    let paginationTitle: XBaseLableStyle
    
    let cancel: XZoomButton.Style
    let confirm: XZoomButton.Style
}

extension XPinPasswordAlertView {
    typealias Style = XPinPasswordAlertViewStyle
}
