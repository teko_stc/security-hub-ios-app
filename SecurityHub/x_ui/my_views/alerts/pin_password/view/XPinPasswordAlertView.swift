//
//  XPinPasswordAlertView.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XPinPasswordAlertView: UIView {
    
    private lazy var style: Style = self.viewStyles()
    
    public var delegate: XPinPasswordAlertViewDelegate!
    
    private var titleLabel: UILabel!
    private var subtitleLabel: UILabel!
    private var paginationTitleLabel: UILabel!
    private var paginationStackView: UIStackView = UIStackView()
    private var hiddenTextField: UITextField = UITextField()
    private var cancelButton: XZoomButton!
    private var confirmButton: XZoomButton!
    
    init() {
        super.init(frame: .zero)
        
        initViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews() {
        backgroundColor = style.backgroundColor
        
        titleLabel = UILabel.create(style.title)
        addSubview(titleLabel)
        
        subtitleLabel = UILabel.create(style.subtitle)
        subtitleLabel.numberOfLines = 0
        addSubview(subtitleLabel)
        
        paginationTitleLabel = UILabel.create(style.paginationTitle)
        addSubview(paginationTitleLabel)
        
        paginationStackView.axis = .horizontal
        paginationStackView.distribution = .equalSpacing
        paginationStackView.spacing = 20
        addSubview(paginationStackView)
        
        hiddenTextField.keyboardType = .numberPad
        hiddenTextField.delegate = self
        addSubview(hiddenTextField)
        hiddenTextField.becomeFirstResponder()
        
        cancelButton = XZoomButton(style: style.cancel)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        addSubview(cancelButton)
        
        confirmButton = XZoomButton(style: style.confirm)
        confirmButton.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        addSubview(confirmButton)
        
        addPaginationSteps(hiddenTextField.text ?? "")
    }
    
    private func setConstraints() {
        [titleLabel, subtitleLabel, paginationTitleLabel, paginationStackView, cancelButton, confirmButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            subtitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            subtitleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            paginationTitleLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 30),
            paginationTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            paginationTitleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            paginationStackView.topAnchor.constraint(equalTo: paginationTitleLabel.bottomAnchor, constant: 20),
            paginationStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cancelButton.topAnchor.constraint(equalTo: paginationStackView.bottomAnchor, constant: 30),
            cancelButton.leadingAnchor.constraint(equalTo: leadingAnchor),
            cancelButton.heightAnchor.constraint(equalToConstant: 40),
            cancelButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            confirmButton.topAnchor.constraint(equalTo: cancelButton.topAnchor),
            confirmButton.leadingAnchor.constraint(equalTo: cancelButton.trailingAnchor),
            confirmButton.trailingAnchor.constraint(equalTo: trailingAnchor),
            confirmButton.heightAnchor.constraint(equalTo: cancelButton.heightAnchor),
            confirmButton.widthAnchor.constraint(equalTo: cancelButton.widthAnchor),
        ])
    }
    
    private func addPaginationSteps(_ text: String) {
        paginationStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        for i in 1...4 {
            let step = UIView()
            
            step.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                step.widthAnchor.constraint(equalToConstant: 8),
                step.heightAnchor.constraint(equalToConstant: 8)
            ])
            
            step.layer.cornerRadius = 4
            step.backgroundColor = i > text.count ? style.inactiveStepColor : style.activeStepColor
            
            paginationStackView.addArrangedSubview(step)
        }
    }
    
    @objc private func cancelButtonTapped() {
        delegate.cancelTapped()
    }
    
    @objc private func confirmButtonTapped() {
        delegate.confirmTapped(pin: hiddenTextField.text ?? "")
    }
    
    public func build(strings: Strings) {
        titleLabel.text = strings.title
        subtitleLabel.text = strings.subtitle
        paginationTitleLabel.text = strings.paginationTitle
        cancelButton.text = strings.cancel
        confirmButton.text = strings.confirm
    }
}

extension XPinPasswordAlertView: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard textField.text?.count ?? 0 <= 4 else { return textField.deleteBackward() }
        
        addPaginationSteps(textField.text ?? "")
    }
}
