//
//  XPinPasswordAlertViewStrings.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

struct XPinPasswordAlertViewStrings {
    var title: String
    var subtitle: String? = nil
    var paginationTitle: String? = nil
    
    let cancel: String
    var confirm: String? = nil
}

extension XPinPasswordAlertView {
    typealias Strings = XPinPasswordAlertViewStrings
}
