//
//  XPinPasswordAlertViewDelegate.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

protocol XPinPasswordAlertViewDelegate {
    func confirmTapped(pin: String)
    func cancelTapped()
}
