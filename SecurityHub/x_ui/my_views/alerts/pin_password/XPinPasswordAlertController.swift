//
//  XPinPasswordAlertController.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import UIKit

class XPinPasswordAlertController: XBaseAlertController<XPinPasswordAlertView> {
    
    override var widthAnchor: CGFloat { 0.8 }
    
    private var strings: XPinPasswordAlertView.Strings!
    private var confirmHandler: ((_ controller: UIViewController, _ pin: String) -> ())!
    private var cancelHandler: (() -> ())?
    
    init(strings: XPinPasswordAlertView.Strings, confirmHandler: @escaping (_ controller: UIViewController, _ pin: String) -> (), cancelHandler: (() -> ())? = nil) {
        self.strings = strings
        self.confirmHandler = confirmHandler
        self.cancelHandler = cancelHandler
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.delegate = self
        mainView.build(strings: self.strings)
        
        verticalAlignment = .top
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillDissmiss(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        verticalAlignment = .top
    }
    
    @objc func keyboardWillDissmiss(notification: Notification) {
        verticalAlignment = .center
    }
}

extension XPinPasswordAlertController: XPinPasswordAlertViewDelegate {
    func confirmTapped(pin: String) {
        confirmHandler(self, pin)
    }
    
    func cancelTapped() {
        dismiss(animated: true) {
            self.cancelHandler?()
        }
    }
}
