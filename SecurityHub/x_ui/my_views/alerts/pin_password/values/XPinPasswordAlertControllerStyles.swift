//
//  XPinPasswordAlertControllerStyles.swift
//  SecurityHub
//
//  Created by Daniil on 23.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XPinPasswordAlertView {
    func viewStyles() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            activeStepColor: UIColor.colorFromHex(0x3abeff),
            inactiveStepColor: UIColor.colorFromHex(0xbcbcbc),
            title: XBaseLableStyle(
                color: UIColor.colorFromHex(0x414042),
                font: UIFont(name: "Open Sans", size: 18.5),
                alignment: .center
            ),
            subtitle: XBaseLableStyle(
                color: UIColor.colorFromHex(0xbcbcbc),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            ),
            paginationTitle: XBaseLableStyle(
                color: UIColor.colorFromHex(0x414042),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            ),
            cancel: XZoomButton.Style(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor.colorFromHex(0x414042)
            ),
            confirm: XZoomButton.Style(
                backgroundColor: .clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor.colorFromHex(0x3abeff)
            )
        )
    }
}
