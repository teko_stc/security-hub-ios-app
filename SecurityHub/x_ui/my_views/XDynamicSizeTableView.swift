//
//  XDynamicSizeTableView.swift
//  SecurityHub
//
//  Created by Daniil on 15.12.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XDynamicSizeTableView: UITableView {
    
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        
        return CGSize(
            width: UIView.noIntrinsicMetric,
            height: contentSize.height
        )
    }
}
