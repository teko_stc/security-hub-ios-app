//
//  XSearchBar.swift
//  SecurityHub
//
//  Created by Timerlan on 19.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XSearchBar: UISearchBar {
    public var Height: CGFloat!
    
    private enum SubviewKey: String {
        case searchField, clearButton, cancelButton,  placeholderLabel
    }
    
    // Button/Icon images
    public var clearButtonImage: UIImage?
    public var resultsButtonImage: UIImage?
    public var searchImage: UIImage?
    
    // Button/Icon colors
    public var searchIconColor: UIColor = DEFAULT_COLOR_MAIN
    public var clearButtonColor: UIColor = DEFAULT_COLOR_MAIN
    public var cancelButtonColor: UIColor = DEFAULT_COLOR_MAIN
    public var capabilityButtonColor: UIColor = DEFAULT_COLOR_MAIN
    
    // Text
    public lazy var textField: UITextField = value(forKey: "searchField") as! UITextField
    public var imageView: UIImageView?
    public var ladel: UILabel?
    public var clearButton: UIButton?
    public var textColor: UIColor?
    public var placeholderColor: UIColor = DEFAULT_COLOR_TEXT_DARK.withAlphaComponent(0.7)
    public var cancelTitle: String?
    
    // Cancel button to change the appearance.
    public var cancelButton: UIButton? {
        guard showsCancelButton else { return nil }
        return self.value(forKey: SubviewKey.cancelButton.rawValue) as? UIButton
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.cancelButton?.setTitleColor(cancelButtonColor, for: .normal)
        if let cancelTitle = cancelTitle {
            self.cancelButton?.setTitle(cancelTitle, for: .normal)
        }
        
        guard let textField = self.value(forKey: SubviewKey.searchField.rawValue) as? UITextField else { return }
        textField.font = UIFont.systemFont(ofSize: 15.0)
        if let clearButton = textField.value(forKey: SubviewKey.clearButton.rawValue) as? UIButton {
            update(button: clearButton, image: clearButtonImage, color: clearButtonColor)
            self.clearButton = clearButton
        }
        if let resultsButton = textField.rightView as? UIButton {
            update(button: resultsButton, image: resultsButtonImage, color: capabilityButtonColor)
            self.clearButton = resultsButton
        }
        if let searchView = textField.leftView as? UIImageView {
            searchView.image = (searchImage ?? searchView.image)?.withRenderingMode(.alwaysTemplate)
            self.imageView = searchView
            self.imageView?.tintColor = searchIconColor
            self.imageView?.frame = CGRect(x: 0, y: 0, width: 40, height: 14)
            self.imageView?.contentMode = .right
        }
        self.ladel = textField.value(forKey: SubviewKey.placeholderLabel.rawValue) as? UILabel
        if let placeholderLabel = textField.value(forKey: SubviewKey.placeholderLabel.rawValue) as? UILabel {
            placeholderLabel.textColor = placeholderColor
        }
        if let textColor = textColor  {
            textField.textColor = textColor
        }
    }
    
    private func update(button: UIButton, image: UIImage?, color: UIColor?) {
        let image = (image ?? button.currentImage)?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        if let color = color {
            button.tintColor = color
        }
    }
    
    func update() {
        guard let textField = self.value(forKey: SubviewKey.searchField.rawValue) as? UITextField else { return }
        if let clearButton = textField.value(forKey: SubviewKey.clearButton.rawValue) as? UIButton {
            update(button: clearButton, image: clearButtonImage, color: clearButtonColor)
            self.clearButton = clearButton
        }
        if let resultsButton = textField.rightView as? UIButton {
            update(button: resultsButton, image: resultsButtonImage, color: capabilityButtonColor)
            self.clearButton = resultsButton
        }
    }
    
    func setHeight(_ height: CGFloat){
        snp.updateConstraints{ (make) in make.height.equalTo(height) }
        let th = height - 0.9 * XSizes.MARGIN_STANDART > 0 ? height - 0.9 * XSizes.MARGIN_STANDART : 0
        textField.snp.updateConstraints{ (make) in make.height.equalTo(th)}

        var alpha = height / Height
        alpha = alpha > 0.7 ? alpha : 0
        UIView.animate(withDuration: 0.25) {
            self.ladel?.alpha = alpha
            self.imageView?.alpha = alpha
            self.clearButton?.alpha = alpha
            self.textField.textColor = self.textField.textColor?.withAlphaComponent(alpha)
            self.textField.alpha = th < 10 ? th / 10 : 1
        }
    }
}
