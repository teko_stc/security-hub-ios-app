//
//  OnboardingView.swift
//  SecurityHub
//
//  Created by Timerlan Rakhmatullin on 08.06.2022.
//  Copyright © 2022 TEKO. All rights reserved.
//

import Foundation
import UIKit

class OnboardingView: UIView {
    private let titleView = UILabel()
    private let actionView = XZoomButton(style: Appearance.actionViewStyle)
    
    private var action: (() -> ())? = nil
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setupViews() {
        backgroundColor = .white
        
        titleView.setStyle(Appearance.titleViewStyle)
        titleView.numberOfLines = 0
        addSubview(titleView)
        
        actionView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        addSubview(actionView)
        
        [titleView, actionView].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            titleView.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -40),
            titleView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.8),
            
            actionView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 40),
            actionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            actionView.heightAnchor.constraint(equalToConstant: 40),
            actionView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
        ])
    }
    
    @objc private func actionViewTapped() {
        action?()
    }
    
    public func set(title: String, actionTitle: String, action: @escaping () -> ()) {
        titleView.text = title
        actionView.text = actionTitle
        self.action = action
    }
}

extension OnboardingView {
    struct Appearance {
        static let actionViewStyle = XZoomButton.Style(backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                                                       font: UIFont(name: "Open Sans", size: 17.5),
                                                       color: UIColor.white,
                                                       disable: UIColor.white,
                                                       disableBackground: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1))
        
        static let titleViewStyle = XBaseLableStyle(color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                                                    font: UIFont(name: "Open Sans", size: 23.5),
                                                    alignment: .center)
    }
}
