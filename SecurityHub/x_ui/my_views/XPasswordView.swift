//
//  XPasswordView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 14.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XPasswordView: XTextField {
    private let default_value = "······"
    private let style: Style
    
    override init(style: Style) {
        self.style = style
        super.init(style: style)
        textContentType = .password
        isSecureTextEntry = true
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func layoutSubviews() {
        if (rightView == nil) {
						if #available(iOS 15.0, *) {
							var conf = UIButton.Configuration.plain()
							conf.baseBackgroundColor = UIColor.clear
							rightView = UIButton(configuration: conf)
						} else {
							rightView = UIButton()
							if #available(iOS 13.0, *) {
								(rightView as? UIButton)?.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10) } }
            (rightView as? UIButton)?.setImage(UIImage(named: "ic_technology_lock")?.withRenderingMode(.alwaysTemplate), for: .normal)
            (rightView as? UIButton)?.setImage(UIImage(named: "ic_technology")?.withRenderingMode(.alwaysTemplate), for: .selected)
            (rightView as? UIButton)?.tintColor = style.unselectColor
            (rightView as? UIButton)?.addTarget(self, action: #selector(tapped), for: .touchUpInside)
            rightView?.frame = CGRect(x: 0, y: 0, width: frame.size.height, height:  frame.size.height)
        }
        super.layoutSubviews()
    }
    
    public func setDefaultValue() { text = default_value }
    
    public func isDefaulValue() -> Bool { return text == default_value }
    
    override func showValidationError(message: String? = nil) {
        super.showValidationError(message: message)
        (rightView as? UIButton)?.tintColor = style.errorColor
    }
    
    override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isDefaulValue() { text = nil }
        (rightView as? UIButton)?.tintColor = style.selectColor
        return super.textFieldShouldBeginEditing(textField)
    }
    
    override func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        (rightView as? UIButton)?.tintColor = style.unselectColor
        return super.textFieldShouldEndEditing(textField)
    }
    
    @objc private func tapped() {
        if isDefaulValue() { text = nil }
        if let rightView = (rightView as? UIButton){ rightView.isSelected = !rightView.isSelected }
        isSecureTextEntry = !isSecureTextEntry
    }
}
