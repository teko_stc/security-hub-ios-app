//
//  XCheckBoxView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 13.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AudioToolbox

struct XCheckBoxViewStyle {
    struct Icon {
        let select, unselect: UIColor
    }
    
    struct Text {
        let select, unselect: UIColor
        let font: UIFont?
    }
    
    let icon: Icon
    let text: Text
}
extension XCheckBoxView {
    typealias Style = XCheckBoxViewStyle
}

protocol XCheckBoxViewDelegate {
    func xCheckBoxViewChangeState(isSelected: Bool)
}

class XCheckBoxView: UIButton {
    var delegate: XCheckBoxViewDelegate?
    private let style: Style
    
    var text: String? {
        didSet {
            var attr: [NSAttributedString.Key : Any] = [ .foregroundColor : style.text.unselect ]
            var attr2: [NSAttributedString.Key : Any] = [ .foregroundColor : style.text.select ]
            if let font = style.text.font { attr[.font] = font; attr2[.font] = font }
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr), for: .normal)
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr2), for: .selected)
            titleLabel?.numberOfLines = 0
            titleLabel?.textAlignment = .left
        }
    }
    
    override var isSelected: Bool {
        didSet {
            tintColor = isSelected ? style.icon.select : style.icon.unselect
        }
    }
    
    init(_ style: Style) {
        self.style = style
        super.init(frame: .zero)
        setImage(UIImage(named: "checkbox_unselected")?.withRenderingMode(.alwaysTemplate), for: .normal)
        setImage(UIImage(named: "checkbox_selected")?.withRenderingMode(.alwaysTemplate), for: .selected)
        tintColor = style.icon.unselect
        imageView?.contentMode = .scaleAspectFit
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 0)
        contentHorizontalAlignment = .left
        addTarget(self, action: #selector(click), for: .touchUpInside)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    @objc private func click() {
        isSelected = !isSelected
        delegate?.xCheckBoxViewChangeState(isSelected: isSelected)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
    }
}
