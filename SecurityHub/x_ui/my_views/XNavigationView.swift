//
//  XNavigationView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.10.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XNavigationViewLeftViewBuilder {
    var imageName: String
    var color: UIColor
    var viewTapped: () -> ()
}

struct XNavigationViewRightViewBuilder {
    var title: String? = nil
    var font: UIFont? = nil
    var imageName: String? = nil
    var color: UIColor
    var viewTapped: (() -> ())? = nil
    var groupViews: [XNavigationViewRightViewBuilder] = []
		var disabled: Bool?
}

struct XNavigationViewBuilder {
    var backgroundColor: UIColor
    var popupColor: UIColor = .white
    var leftView: XNavigationViewLeftViewBuilder?
    var titleView: XBaseLableStyle?
    var subTitleView: XBaseLableStyle?
    var rightViews: [XNavigationViewRightViewBuilder] = []
    var lineColor: UIColor? = nil
}

class XNavigationView: UIView {
    static let marginTop: CGFloat = 22, marginLeft: CGFloat = 21, iconSize: CGFloat = 25
    
    public var title: String? { didSet { titleView?.text = title } }
    public var subTitle: String? { didSet { subTitleView?.text = subTitle } }
    
    private var leftView: UIButton?
    private var titleView: UILabel?, subTitleView: UILabel?
    private var stackView: UIStackView?
    private var popupView: UIView?, lineView: UIView?, topView: UIView?

    private var builder: XNavigationViewBuilder?
    
    public func clearView() {
        topView?.removeFromSuperview(); topView = nil
        leftView?.removeFromSuperview(); leftView = nil
        titleView?.removeFromSuperview(); titleView = nil
        subTitleView?.removeFromSuperview(); subTitleView = nil
        stackView?.removeFromSuperview(); stackView = nil
        lineView?.removeFromSuperview(); lineView = nil
        popupView?.removeFromSuperview(); popupView = nil
    }
    
    public func buildView(_ builder: XNavigationViewBuilder) {
        let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        let marginTop = XNavigationView.marginTop, marginLeft = XNavigationView.marginLeft, ih = XNavigationView.iconSize
        self.clearView()
        self.builder = builder
        self.backgroundColor = builder.backgroundColor
        topView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: topPadding))
        topView!.backgroundColor = builder.backgroundColor
        addSubview(topView!)
        leftView = UIButton(type: .infoLight)
        leftView!.contentMode = .scaleAspectFill
        leftView!.tintColor = builder.leftView?.color
        leftView!.setImage(UIImage(named: builder.leftView?.imageName ?? "ic_nil")?.withRenderingMode(.alwaysTemplate), for: .normal)
        leftView!.addTarget(self, action: #selector(leftViewTapped), for: .touchUpInside)
        leftView!.translatesAutoresizingMaskIntoConstraints = false
        addSubview(leftView!)
        leftView!.topAnchor.constraint(equalTo: topView!.bottomAnchor, constant: marginTop).isActive = true
        leftView!.leadingAnchor.constraint(equalTo: leadingAnchor, constant: marginLeft).isActive = true
        leftView!.heightAnchor.constraint(equalToConstant: ih).isActive = true
        leftView!.widthAnchor.constraint(equalToConstant: ih).isActive = true
        if let titleViewStyle = builder.titleView {
            titleView = UILabel.create(titleViewStyle)
            titleView!.text = title
            titleView!.numberOfLines = 0
            titleView!.translatesAutoresizingMaskIntoConstraints = false
            addSubview(titleView!)
            titleView!.topAnchor.constraint(equalTo: topView!.bottomAnchor, constant: marginTop - (titleViewStyle.font?.pointSize ?? 25) / 4).isActive = true
            titleView!.leadingAnchor.constraint(equalTo: leftView!.trailingAnchor, constant: marginLeft).isActive = true
        }
        if let subTitleViewStyle = builder.subTitleView {
            subTitleView = UILabel.create(subTitleViewStyle)
            subTitleView!.text = subTitle
            subTitleView!.numberOfLines = 0
            subTitleView!.translatesAutoresizingMaskIntoConstraints = false
            addSubview(subTitleView!)
            subTitleView!.topAnchor.constraint(equalTo: (titleView ?? topView!).bottomAnchor, constant: (titleView != nil ? 4 : (marginTop - (subTitleViewStyle.font?.pointSize ?? 25) / 4))).isActive = true
            subTitleView!.leadingAnchor.constraint(equalTo: leftView!.trailingAnchor, constant: marginLeft).isActive = true
            if titleView != nil { subTitleView!.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -marginLeft).isActive = true }
        }
        stackView = UIStackView()
        stackView!.axis = .horizontal
        stackView!.distribution = .equalCentering
        stackView!.alignment = .center
        stackView!.spacing = 25
        stackView!.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView!)
        stackView!.topAnchor.constraint(equalTo: topView!.bottomAnchor, constant: marginTop).isActive = true
        if titleView != nil || subTitleView != nil { stackView!.leadingAnchor.constraint(equalTo: (titleView ?? subTitleView)!.trailingAnchor, constant: builder.rightViews.count == 0 ? 0 : marginLeft).isActive = true }
        stackView!.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -marginLeft).isActive = true
        var stackViewWidth: CGFloat = builder.rightViews.count == 0 ? 0 : ((CGFloat(builder.rightViews.count) - 1) * stackView!.spacing)
        if builder.rightViews.count > 0 {
            for i in 0...builder.rightViews.count-1 {
                let rightBuilder = builder.rightViews[i]
                let result = createRightButton(rightBuilder: rightBuilder, height: ih, index: i)
                stackViewWidth += result.width
                stackView!.addArrangedSubview(result.view)
            }
        }
        if stackViewWidth != 0 || titleView != nil { stackView!.widthAnchor.constraint(equalToConstant: stackViewWidth == 0 ? ih : stackViewWidth).isActive = true }
        if let lineColor = builder.lineColor {
            lineView = UIView()
            lineView?.layer.addSublayer({
                let layer = CALayer()
                layer.backgroundColor = lineColor.cgColor
                layer.frame = CGRect(x: 0, y: 0.5, width: UIScreen.main.bounds.width, height: 0.5)
                return layer
            }())
            lineView?.translatesAutoresizingMaskIntoConstraints = false
            addSubview(lineView!)
            lineView!.topAnchor.constraint(equalTo: bottomAnchor, constant: -1).isActive = true
            lineView!.heightAnchor.constraint(equalToConstant: 1).isActive = true
            lineView!.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            lineView!.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        }
        (subTitleView ?? titleView ?? leftView!).bottomAnchor.constraint(equalTo: bottomAnchor, constant: subTitleView != nil || lineView != nil ? -20 : -6).isActive = true
    }
    
    private func createRightButton(rightBuilder: XNavigationViewRightViewBuilder, height: CGFloat, index: Int) -> (view: UIButton, width: CGFloat) {
        let rightButton = UIButton(type: .infoLight)
        var widthRightButton: CGFloat = 0
        var attr: [NSAttributedString.Key : Any] = [ .foregroundColor : rightBuilder.color ]; if let font = rightBuilder.font { attr[.font] = font }
        if let _title = rightBuilder.title {
            let t = NSAttributedString(string: _title, attributes: attr)
            rightButton.setAttributedTitle(t, for: .normal)
            widthRightButton += t.size().width + 1
        }
        if let imageName = rightBuilder.imageName {
            rightButton.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
            widthRightButton += height
        } else {
            rightButton.setImage(UIImage(named: "ic_nil")?.resized(to: CGSize(width: 0, height: 0)), for: .normal)
        }
        if let _ = rightBuilder.title, let _ = rightBuilder.imageName {
            widthRightButton += 10
            rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
            rightButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        }
        rightButton.tintColor = rightBuilder.color
        rightButton.tag = index
        rightButton.addTarget(self, action: #selector(rightViewTapped), for: .touchUpInside)
        rightButton.widthAnchor.constraint(equalToConstant: widthRightButton).isActive = true
        rightButton.heightAnchor.constraint(equalToConstant: height).isActive = true
				if rightBuilder.disabled ?? false { rightButton.isEnabled = false }
        return (view: rightButton, width: widthRightButton)
    }
    
    @objc private func leftViewTapped() { builder?.leftView?.viewTapped() }
    
    @objc private func rightViewTapped(button: UIButton) {
        guard let builder = builder else { return }
        if button.tag >= 100 {
            builder.rightViews[Int(button.tag / 100) - 1].groupViews[button.tag % 100].viewTapped?()
            return UIView.animate(withDuration: 0.4, animations: { self.popupView?.alpha = 0 }, completion: { _ in self.popupView = nil })
        } else {
            builder.rightViews[button.tag].viewTapped?()
        }
        guard let superview = superview else { return }
        if builder.rightViews[button.tag].groupViews.count > 0 {
            popupView = {
                let view = UIView()
                view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onPopupTapped)))
                view.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(onPopupTapped)))
                view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(onPopupTapped)))
                view.translatesAutoresizingMaskIntoConstraints = false
                superview.addSubview(view)
                view.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
                view.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
                view.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
                view.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
                view.alpha = 0
                return view
            }()
            var stackViewWidth: CGFloat = 0, stackViewHeight: CGFloat = 34 * CGFloat(builder.rightViews[button.tag].groupViews.count)
            let stackView: UIStackView = {
                let view = UIStackView()
                view.axis = .vertical
                view.distribution = .equalCentering
                view.alignment = .center
                view.spacing = 0
                view.translatesAutoresizingMaskIntoConstraints = false
                popupView?.addSubview(view)
                view.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 6).isActive = true
                view.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -20).isActive = true
                return view
            }()
            for i in 0...builder.rightViews[button.tag].groupViews.count-1 {
                let rightBuilder = builder.rightViews[button.tag].groupViews[i]
                let result = createRightButton(rightBuilder: rightBuilder, height: 34, index: i + 100 * (button.tag + 1))
                stackViewWidth = max(stackViewWidth, result.width)
                stackView.addArrangedSubview(result.view)
            }
            stackView.widthAnchor.constraint(equalToConstant: stackViewWidth).isActive = true
            stackView.leadingAnchor.constraint(greaterThanOrEqualTo: superview.leadingAnchor, constant: 20).isActive = true
            stackView.heightAnchor.constraint(equalToConstant: stackViewHeight).isActive = true
            let cardLayer: CALayer = {
                let layer = CALayer()
                let position = self.stackView!.convert(button.frame.origin, to: superview)
                layer.frame = CGRect(x: position.x - stackViewWidth - 40 + button.frame.size.width, y: position.y + 30, width: stackViewWidth + 40, height: stackViewHeight + 6)
                layer.backgroundColor = builder.popupColor.cgColor
                layer.cornerRadius = 10
                layer.shadowOffset = CGSize(width: 2, height: 2)
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOpacity = 0.3
                layer.zPosition = -2
                return layer
            }()
            popupView?.layer.addSublayer(cardLayer)
            let blurLayer: CALayer = {
                let layer = CALayer()
                layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                layer.backgroundColor = UIColor.black.cgColor
                layer.opacity = 0.07
                layer.zPosition = -3
                return layer
            }()
            popupView?.layer.addSublayer(blurLayer)
            UIView.animate(withDuration: 0.4, animations: { self.popupView?.alpha = 1 })
        }
    }
    
    @objc private func onPopupTapped(sender: UIGestureRecognizer) {
        guard let popupView = popupView, sender.state == .ended else { return }
        UIView.animate(withDuration: 0.4, animations: { popupView.alpha = 0 }, completion: { _ in self.popupView = nil })
    }
}
