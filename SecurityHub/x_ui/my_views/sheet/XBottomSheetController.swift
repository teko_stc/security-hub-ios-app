//
//  XBottomSheetController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 27.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import UIKit

class XBottomSheetController: XBaseBottomSheetController<XBottomSheetView>, XBottomSheetViewDelegate {
    public var onDissmised: ( () -> () )?
    public var onSelectedItem: ( (XBottomSheetController, Int, XBottomSheetViewItem) -> () )?
    
    private let style: XBottomSheetView.Style
    private let items: [XBottomSheetViewItem]
    private let selectedIndex: Int
    
    init(style: XBottomSheetView.Style, title: String? = nil, selectedIndex: Int = -1, items: [XBottomSheetViewItem]) {
        self.style = style
        self.items = items
        self.selectedIndex = selectedIndex
        super.init()
        self.title = title
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundColor = style.backgroundColor
        mainView.delegate = self
        mainView.buildViews(style: style, title: title, items: items, selectedIndex: selectedIndex)
    }
    
    override func viewDidDissmised() {
        onDissmised?()
    }
    
    func selectedItem(index: Int, item: XBottomSheetViewItem) {
        onSelectedItem?(self, index, item)
    }
}
