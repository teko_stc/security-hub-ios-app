//
//  XBottomSheetControllerDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XBottomSheetControllerDelegate {
    func xBottomSheetControllerDissmised(controller: XBottomSheetController)
    func xBottomSheetControllerSelectedItem(controller: XBottomSheetController, index: Int, item: XBottomSheetViewItem)
}
