//
//  XBottomSheetViewCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBottomSheetViewCellStyle {
    struct Label {
        let color: UIColor
        let font: UIFont?
        let select: UIColor
    }
    
    let backgroundColor: UIColor
    let selectedBackgroundColor: UIColor
    let title: Label
    var text: Label? = nil
}
extension XBottomSheetViewCell {
    typealias Style = XBottomSheetViewCellStyle
}
