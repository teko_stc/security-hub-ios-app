//
//  XBottomSheetViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XBottomSheetViewCell: UITableViewCell {
    static let identifier: String = "XBottomSheetViewCell.identifier"
    
    private let titleView = UILabel()
    private let textView = UILabel()
    private let backView = UIView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(title: String, text: String?) {
        titleView.text = title
        textView.text = text
    }
    
    public func setStyle(style: Style, isSelected: Bool) {
        backgroundColor = style.backgroundColor
        
        titleView.textColor = isSelected ? style.title.select : style.title.color
        titleView.font = style.title.font
        titleView.numberOfLines = 1
        
        if let textStyle = style.text {
            textView.textColor = isSelected ? textStyle.select : textStyle.color
            textView.font = textStyle.font
            textView.numberOfLines = 0
        } else {
            textView.font = UIFont.systemFont(ofSize: 0.1)
        }
        
        backView.backgroundColor = style.selectedBackgroundColor
    }
    
    private func initViews() {
        selectedBackgroundView = backView
        backView.layer.cornerRadius = 5
        contentView.addSubview(titleView)
        contentView.addSubview(textView)
        setConstraints()
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            textView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
    }
}

extension XBottomSheetView {
    typealias Cell = XBottomSheetViewCell
}

