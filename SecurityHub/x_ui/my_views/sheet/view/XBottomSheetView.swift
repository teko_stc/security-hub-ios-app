//
//  XBottomSheetView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBottomSheetViewItem {
    let title: String
    var text: String? = nil
    var any: Any? = nil
}

class XBottomSheetView: UIView {
    public var delegate: XBottomSheetViewDelegate?
    
    private let titleView: UILabel = UILabel()
    private let tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var tableViewHeightAnchor: NSLayoutConstraint!
    private var style: Style?
    private var items: [XBottomSheetViewItem] = []
    private var selectedIndex: Int = -1

    public func buildViews(style: Style, title: String?, items: [XBottomSheetViewItem], selectedIndex: Int) {
        self.items = items
        self.style = style
        self.selectedIndex = selectedIndex
        backgroundColor = style.backgroundColor
        
        titleView.text = title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.aligment
        addSubview(titleView)
        
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.separatorStyle = .none
        tableView.sectionFooterHeight = 0
        tableView.dataSource = self
        tableView.delegate = self
        tableView.addObserver(self, forKeyPath: "contentSize", options: [.new, .old, .prior], context: nil)
        addSubview(tableView)
        
        setConstraints()
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeightAnchor = tableView.heightAnchor.constraint(equalToConstant: 10)
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
            tableView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableViewHeightAnchor
        ])
    }
    
    @objc override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" && tableViewHeightAnchor.constant <= UIScreen.main.bounds.height * 0.7 {
            tableViewHeightAnchor.constant = min(tableView.contentSize.height, UIScreen.main.bounds.height * 0.7)
            layoutIfNeeded()
        }
    }
}

extension XBottomSheetView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setStyle(style: style!.cell, isSelected: indexPath.row == selectedIndex)
        cell.setContent(title: items[indexPath.row].title, text: items[indexPath.row].text)
        return cell
    }
}

extension XBottomSheetView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.selectedItem(index: indexPath.row, item: items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { 40 }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
    }
}
