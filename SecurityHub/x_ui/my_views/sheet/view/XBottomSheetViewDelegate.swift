//
//  XBottomSheetViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XBottomSheetViewDelegate {
    func selectedItem(index: Int, item: XBottomSheetViewItem)
}
