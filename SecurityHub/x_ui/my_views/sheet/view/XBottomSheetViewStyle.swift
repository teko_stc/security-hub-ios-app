//
//  XBottomSheetViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBottomSheetViewStyle {
    struct Title {
        let color: UIColor
        let font: UIFont?
        let aligment: NSTextAlignment
    }
    
    let backgroundColor: UIColor
    let title: Title
    let cell: XBottomSheetView.Cell.Style
}
extension XBottomSheetView {
    typealias Style = XBottomSheetViewStyle
}
