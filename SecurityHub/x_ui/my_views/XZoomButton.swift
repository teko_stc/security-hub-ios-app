//
//  XZoomButton.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 11.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AudioToolbox

struct XZoomButtonStyle {
    var backgroundColor: UIColor
    var font: UIFont?
    var color: UIColor
    var disable: UIColor = UIColor.lightGray
    var selected: UIColor = UIColor.black
    var disableBackground: UIColor = UIColor.gray
    var alignment: UIControl.ContentHorizontalAlignment = .center
}
extension XZoomButton {
    typealias Style = XZoomButtonStyle
}

class XZoomButton: UIButton {
	
		var backgroundHighligh = false
	
    override var isHighlighted: Bool {
        didSet {
					if backgroundHighligh {
						layer.backgroundColor = self.isHighlighted ? style.disableBackground.cgColor : style.backgroundColor.cgColor
					} else {
						UIView.animate(withDuration: 0.4, animations: {
							self.transform = self.isHighlighted ? CGAffineTransform(scaleX: 0.8, y: 0.8) : CGAffineTransform(scaleX: 1, y: 1) //Scale label area
						})
					}
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            layer.backgroundColor = isEnabled ? style.backgroundColor.cgColor : style.disableBackground.cgColor
        }
    }
    
    public var text: String? {
        didSet {
            var attr: [NSAttributedString.Key : Any] = [ .foregroundColor : style.color ]
            var attr2: [NSAttributedString.Key : Any] = [ .foregroundColor : style.disable ]
            var attr3: [NSAttributedString.Key : Any] = [ .foregroundColor : style.selected ]
            if let font = style.font { attr[.font] = font; attr2[.font] = font; attr3[.font] = font; }
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr), for: .normal)
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr2), for: .disabled)
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr3), for: .selected)
        }
    }
    
    public var loadingText: String?
    
    public var style: Style {
        didSet {
            layer.backgroundColor = isEnabled ? style.backgroundColor.cgColor : style.disableBackground.cgColor
            contentHorizontalAlignment = style.alignment
            var attr: [NSAttributedString.Key : Any] = [ .foregroundColor : style.color ]
            var attr2: [NSAttributedString.Key : Any] = [ .foregroundColor : style.disable ]
            var attr3: [NSAttributedString.Key : Any] = [ .foregroundColor : style.selected ]
            if let font = style.font { attr[.font] = font; attr2[.font] = font; attr3[.font] = font; }
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr), for: .normal)
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr2), for: .disabled)
            setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr3), for: .selected)
        }
    }
    
    public func styleDidUpdate() {
        layer.backgroundColor = isEnabled ? style.backgroundColor.cgColor : style.disableBackground.cgColor
        contentHorizontalAlignment = style.alignment
        var attr: [NSAttributedString.Key : Any] = [ .foregroundColor : style.color ]
        var attr2: [NSAttributedString.Key : Any] = [ .foregroundColor : style.disable ]
        var attr3: [NSAttributedString.Key : Any] = [ .foregroundColor : style.selected ]
        if let font = style.font { attr[.font] = font; attr2[.font] = font; attr3[.font] = font; }
        setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr), for: .normal)
        setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr2), for: .disabled)
        setAttributedTitle(NSAttributedString(string: text ?? "", attributes: attr3), for: .selected)
    }
    
    init(style: Style){
        self.style = style
        super.init(frame: .zero)
        layer.backgroundColor = style.backgroundColor.cgColor
        layer.cornerRadius = 10
        contentHorizontalAlignment = style.alignment
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private var oldText: String?
    public func startLoading() {
        isEnabled = false
        oldText = text
        text = loadingText ?? text ?? ""
        loading()
    }
    
    public func endLoading() { isEnabled = true }
    
    public func accent() {
        UIView.animate(withDuration: 0.4, animations: { self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)}) { _ in
            self.isHighlighted = false
        }
    }

    private func loading() {
        UIView.transition(with: self, duration: 0.25, options: .transitionCrossDissolve, animations: {
            guard let text = self.text else { return }
            if text.filter({ $0 == "." }).count < 4 {
                if (self.style.alignment == .left) { self.text = text + "." }
                else { self.text = " " + text + "." }
            } else { self.text = self.loadingText ?? self.oldText ?? "" }
        }, completion: { _ in
            if !self.isEnabled { self.loading() } else { self.text = self.oldText }
        })
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        return true
    }
}

