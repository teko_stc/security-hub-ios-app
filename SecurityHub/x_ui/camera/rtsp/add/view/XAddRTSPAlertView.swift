//
//  XAddRTSPAlertView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XAddRTSPAlertViewDelegate {
    func addViewTapped(name: String, url: String)
    func closeViewTapped()
}

class XAddRTSPAlertView: UIView {
    public var delegate: XAddRTSPAlertViewDelegate?
    
    private let titleView: UILabel = UILabel()
    private var nameView, rtspView: XTextField!
    private var pasteView: UIButton = UIButton(type: .infoLight)
    private var addView, closeView: XZoomButton!
    private let strings: XAddRTSPAlertViewStrings = XAddRTSPAlertViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(style: Style) {
        backgroundColor = style.background
        addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(viewTapped)))

        titleView.text = strings.title
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        addSubview(titleView)
        
        nameView = XTextField(style: style.name)
        nameView.placeholder = strings.name
        nameView.textContentType = .name
        nameView.autocorrectionType = .no
        nameView.returnKeyType = .next
        nameView.xDelegate = self
        addSubview(nameView)
        
        pasteView.setImage(UIImage(named: "ic_copy_2")?.withRenderingMode(.alwaysTemplate), for: .normal)
        pasteView.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        pasteView.tintColor = style.rtsp.selectColor
        pasteView.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        pasteView.imageView?.contentMode = .scaleAspectFit
        pasteView.addTarget(self, action: #selector(pasteViewTapped), for: .touchUpInside)
        
        rtspView = XTextField(style: style.rtsp)
        rtspView.placeholder = strings.rtsp
        rtspView.rightView = pasteView
        rtspView.autocorrectionType = .no
        rtspView.textContentType = .URL
        rtspView.returnKeyType = .done
        rtspView.xDelegate = self
        addSubview(rtspView)
        
        addView = XZoomButton(style: style.add)
        addView.text = strings.add
        addView.addTarget(self, action: #selector(addViewTapped), for: .touchUpInside)
        addSubview(addView)
        
        closeView = XZoomButton(style: style.close)
        closeView.text = strings.close
        closeView.addTarget(self, action: #selector(closeViewTapped), for: .touchUpInside)
        addSubview(closeView)
    }
    
    private func setConstraints() {
        titleView.translatesAutoresizingMaskIntoConstraints = false
        nameView.translatesAutoresizingMaskIntoConstraints = false
        rtspView.translatesAutoresizingMaskIntoConstraints = false
        addView.translatesAutoresizingMaskIntoConstraints = false
        closeView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: trailingAnchor),

            nameView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 30),
            nameView.leadingAnchor.constraint(equalTo: leadingAnchor),
            nameView.trailingAnchor.constraint(equalTo: trailingAnchor),
            nameView.heightAnchor.constraint(equalToConstant: 52),
            
            rtspView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 20),
            rtspView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rtspView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rtspView.heightAnchor.constraint(equalToConstant: 52),

            addView.topAnchor.constraint(equalTo: rtspView.bottomAnchor, constant: 20),
            addView.leadingAnchor.constraint(equalTo: leadingAnchor),
            addView.trailingAnchor.constraint(equalTo: trailingAnchor),
            addView.heightAnchor.constraint(equalToConstant: 40),
            
            closeView.topAnchor.constraint(equalTo: addView.bottomAnchor, constant: 10),
            closeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            closeView.trailingAnchor.constraint(equalTo: trailingAnchor),
            closeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            closeView.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    @objc private func addViewTapped() {
        if (nameView.text ?? "").isEmpty { nameView.becomeFirstResponder(); nameView.showValidationError() }
        else if (rtspView.text ?? "").isEmpty { rtspView.becomeFirstResponder(); rtspView.showValidationError() }
        else if !(rtspView.text ?? "").lowercased().contains("rtsp://") { rtspView.becomeFirstResponder(); rtspView.showValidationError(message: strings.rtspValidationError)  }
        else { viewTapped(); delegate?.addViewTapped(name: nameView.text!, url: rtspView.text!) }
    }
    
    @objc private func pasteViewTapped() {
        rtspView.text = UIPasteboard.general.string
        if (rtspView.text ?? "").isEmpty { rtspView.becomeFirstResponder(); rtspView.showValidationError() }
        else if !(rtspView.text ?? "").lowercased().contains("rtsp://") { rtspView.becomeFirstResponder(); rtspView.showValidationError(message: strings.rtspValidationError)  }
        else {  _ = rtspView.textFieldShouldBeginEditing(rtspView) }
    }

    @objc private func closeViewTapped() { delegate?.closeViewTapped() }
    
    @objc private func viewTapped() {
        if nameView.isFirstResponder { nameView.resignFirstResponder()}
        if rtspView.isFirstResponder { rtspView.resignFirstResponder()}
    }
}

extension XAddRTSPAlertView: XTextFieldDelegete {
    func xTextFieldShouldReturn(_ textField: UITextField) {
        if (textField == nameView) { rtspView.becomeFirstResponder() }
        if (textField == rtspView) { rtspView.resignFirstResponder(); addViewTapped() }
    }
}
