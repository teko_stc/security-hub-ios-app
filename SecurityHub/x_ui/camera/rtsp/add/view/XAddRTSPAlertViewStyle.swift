//
//  XAddRTSPAlertViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XBaseLableStyle {
    let color: UIColor
    let font: UIFont?
    var alignment: NSTextAlignment = .left
}
extension UILabel {
    static func create(_ style: XBaseLableStyle) -> UILabel {
        let lable = UILabel()
        lable.font = style.font
        lable.textColor = style.color
        lable.textAlignment = style.alignment
        return lable
    }
    
    func setStyle(_ style: XBaseLableStyle) {
        self.font = style.font
        self.textColor = style.color
        self.textAlignment = style.alignment
    }
}

struct XAddRTSPAlertViewStyle {
    let background: UIColor
    let title: XBaseLableStyle
    let name, rtsp: XTextField.Style
    let add, close: XZoomButton.Style
}
extension XAddRTSPAlertView {
    typealias Style = XAddRTSPAlertViewStyle
}
