//
//  XAddRTSPAlertController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XAddRTSPAlertController: XBaseAlertController<XAddRTSPAlertView> {
    override var widthAnchor: CGFloat { 0.9 }
    
    public var onSuccess: ((String, String) -> ())? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissmiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) { verticalAlignment = .top }
    
    @objc func keyboardWillDissmiss(notification: Notification) { verticalAlignment = .center }
}

extension XAddRTSPAlertController: XAddRTSPAlertViewDelegate {
    func addViewTapped(name: String, url: String) {
        dismiss(animated: true, completion: {
            self.onSuccess?(name, url)
        })
    }
    
    func closeViewTapped() { dismiss(animated: true) }
}
