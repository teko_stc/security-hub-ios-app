//
//  XAddRTSPAlertViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XAddRTSPAlertViewStrings {
    /// Новая IP-камера
    var title: String { "N_WIZ_ALL_RTSP_NEW".localized() }
    
    /// Имя
    var name: String { "N_CAMERAS_SETTINGS_NAME".localized() }
    
    /// RTSP-ссылка
    var rtsp: String { "N_CAMERAS_SETTINGS_LINK".localized() }
    
    /// Добавить
    var add: String { "N_CAMERAS_BUTTON_ADD".localized() }
    
    /// Закрыть
    var close: String { "CLOSE".localized() }
    
    /// Некорректная ссылка
    var rtspValidationError: String { "N_RTSP_LINK_ERROR".localized() }
}
