//
//  XAddRTSPAlertViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XAddRTSPAlertView {
    func style() -> Style {
        return Style(
            background: UIColor.white,
            title: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 18.5),
                alignment: .center
            ),
            name: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                errorColor: UIColor.red,
                font: UIFont(name: "Open Sans", size: 15.5),
                borderWidth: 0
            ),
            rtsp: XTextField.Style(
                backgroundColor: UIColor.white,
                unselectColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                textColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                errorColor: UIColor.red,
                font: UIFont(name: "Open Sans", size: 15.5),
                borderWidth: 0
            ),
            add: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor.red
            ),
            close: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}
