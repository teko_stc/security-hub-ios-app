//
//  XCameraBindViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraBindViewStrings {
    /// Выберите объект и датчик, к которым вы хотели бы привязать камеру
    var description: String { "CAMERA_BIND_TITLE_CHOOSE".localized() }
    
    /// Объект:
    var objectTitle: String { "CAMERA_BIND_OBJECT".localized() }
    
    /// Датчик:
    var zoneTitle: String { "CAMERA_BIND_SENSOR".localized() }
    
    /// Выбрать
    var select: String { "CAMERA_BIND_SELECT".localized() }
    
    /// Привязать
    var bind: String { "WIZ_BIND".localized() }
}

class XCameraBindControllerStrings {
    /// Привязка к датчику
    var subTitle: String { "CAMERA_BINDING_TITLE".localized() }
    
    /// Выберите Объект
    var objectSelectTitle: String { "CAMERA_TITLE_CHOOSE_OBJECT".localized() }
    
    /// Привязка к датчику
    var zoneSelectTitle: String { "CAMERA_BINDING_TITLE".localized() }
    
    func notObjectErrorAlert() -> XAlertView.Strings {
        XAlertView.Strings (
            /// Сначала выберите объект
            title: "CAMERA_MESSAGE_CHOOSE_OBJECT".localized(),
            text: nil,
            /// Ok
            positive: "OK".localized(),
            negative: nil
        )
    }
    
    func errorAlert(message: String) -> XAlertView.Strings {
        XAlertView.Strings (
            title: message,
            text: nil,
            /// Ok
            positive: "OK".localized(),
            negative: nil
        )
    }
    
    func errorBindingAlert() -> XAlertView.Strings {
        XAlertView.Strings (
            /// Не удалось привязать камеру к датчику. Попробуйте повторить попытку позже.
            title: "D3_SUCCEED".localized(),
            text: nil,
            /// Ok
            positive: "OK".localized(),
            negative: nil
        )
    }
}
