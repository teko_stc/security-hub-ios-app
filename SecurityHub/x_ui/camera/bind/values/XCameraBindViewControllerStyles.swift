//
//  XCameraBindViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraBindView {
    func style() -> Style {
        Style(
            background: UIColor.white,
            description: XBaseLableStyle(
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            buttonTitle: XBaseLableStyle(
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            button: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font:  UIFont(name: "Open Sans", size: 18.5),
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 0.5),
                selected: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                alignment: .left
            ),
            action: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font:  UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.white,
                disable: UIColor.white,
                disableBackground: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 0.4)
            )
        )
    }
}

extension XCameraBindController {
    func errorAlet() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: nil
        )
    }
    
    func objectAndZoneBottomSheet() -> XBottomSheetView.Style {
        XBottomSheetView.Style(
            backgroundColor: UIColor.white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.2),
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                )
            )
        )
    }
}
