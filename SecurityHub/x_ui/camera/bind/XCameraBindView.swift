//
//  XCameraBindView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XCameraBindViewLayerProtocol {
    func setObjectValue(name: String)
    func setZoneValue(name: String?)
    func showProgress()
    func hideProgress()
}

class XCameraBindView: UIView {
    public var delegate: XCameraBindViewDelegate?
    
    private let strings: XCameraBindViewStrings = XCameraBindViewStrings()
    private let descriptionView: UILabel = UILabel()
    private let objectView: UILabel = UILabel()
    private let zoneView: UILabel = UILabel()
    private var objectButtonView, zoneButtonView, actionButtonView: XZoomButton!
    private var subTitleViewLeading: NSLayoutConstraint!

    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(style: Style) {
        backgroundColor = style.background
        
        descriptionView.text = strings.description
        descriptionView.font = style.description.font
        descriptionView.textColor = style.description.color
        descriptionView.textAlignment = style.description.alignment
        descriptionView.numberOfLines = 0
        addSubview(descriptionView)
        
        objectView.text = strings.objectTitle
        objectView.font = style.buttonTitle.font
        objectView.textColor = style.buttonTitle.color
        objectView.textAlignment = style.buttonTitle.alignment
        addSubview(objectView)
        
        objectButtonView = XZoomButton(style: style.button)
        objectButtonView.text = strings.select
        objectButtonView.addTarget(self, action: #selector(objectButtonViewTapped), for: .touchUpInside)
        addSubview(objectButtonView)

        zoneView.text = strings.zoneTitle
        zoneView.font = style.buttonTitle.font
        zoneView.textColor = style.buttonTitle.color
        zoneView.textAlignment = style.buttonTitle.alignment
        addSubview(zoneView)
        
        zoneButtonView = XZoomButton(style: style.button)
        zoneButtonView.text = strings.select
        zoneButtonView.addTarget(self, action: #selector(zoneButtonViewTapped), for: .touchUpInside)
        addSubview(zoneButtonView)
        
        actionButtonView = XZoomButton(style: style.action)
        actionButtonView.text = strings.bind
        actionButtonView.isEnabled = false
        actionButtonView.addTarget(self, action: #selector(actionViewTapped), for: .touchUpInside)
        addSubview(actionButtonView)
    }
    
    private func setConstraints() {
        descriptionView.translatesAutoresizingMaskIntoConstraints = false
        objectView.translatesAutoresizingMaskIntoConstraints = false
        objectButtonView.translatesAutoresizingMaskIntoConstraints = false
        zoneView.translatesAutoresizingMaskIntoConstraints = false
        zoneButtonView.translatesAutoresizingMaskIntoConstraints = false
        actionButtonView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
            descriptionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            descriptionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            objectView.topAnchor.constraint(equalTo: descriptionView.bottomAnchor, constant: 30),
            objectView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor),
            objectView.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor),
            
            objectButtonView.topAnchor.constraint(equalTo: objectView.bottomAnchor, constant: 10),
            objectButtonView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor),
            objectButtonView.heightAnchor.constraint(equalToConstant: 40),
            
            zoneView.topAnchor.constraint(equalTo: objectButtonView.bottomAnchor, constant: 30),
            zoneView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor),
            zoneView.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor),
            
            zoneButtonView.topAnchor.constraint(equalTo: zoneView.bottomAnchor, constant: 10),
            zoneButtonView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor),
            zoneButtonView.heightAnchor.constraint(equalToConstant: 40),
            
            
            actionButtonView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            actionButtonView.heightAnchor.constraint(equalToConstant: 40),
            actionButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.7),
            actionButtonView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -40)
        ])
    }
    
    @objc private func objectButtonViewTapped() { delegate?.objectSelectViewTapped() }
    
    @objc private func zoneButtonViewTapped() { delegate?.zoneSelectViewTapped() }
    
    @objc private func actionViewTapped() { delegate?.bindViewTapped() }
}

extension XCameraBindView: XCameraBindViewLayerProtocol {
    func hideProgress() {
        actionButtonView.endLoading()
    }
    
    func showProgress() {
        actionButtonView.startLoading()
    }
    
    func setObjectValue(name: String) {
        objectButtonView.text = name
        objectButtonView.isSelected = true
    }
    
    func setZoneValue(name: String?) {
        zoneButtonView.text = name
        zoneButtonView.isSelected = name != nil
        actionButtonView.isEnabled = name != nil
    }
}
