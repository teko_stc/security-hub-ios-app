//
//  XCameraBindController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XCameraBindDataLayerModelMapper {
    static func cast(site: Sites) -> XCameraBindSiteDataLayerModel {
        XCameraBindSiteDataLayerModel(siteId: Int(site.id), name: site.name)
    }
    static func cast(dz: DZoneWithSitesDeviceSectionInfo) -> XCameraBindZoneDataLayerModel {
        XCameraBindZoneDataLayerModel(siteId: Int(dz.siteIds[0]), deviceId: Int(dz.zone.device), sectionId: Int(dz.zone.section), zoneId: Int(dz.zone.zone), name: dz.zone.name)
    }
}
struct XCameraBindSiteDataLayerModel {
    let siteId: Int
    let name: String
}
struct XCameraBindZoneDataLayerModel {
    let siteId, deviceId, sectionId, zoneId: Int
    let name: String
}

class XCameraBindController: XBaseViewController<XCameraBindView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuider = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        subTitleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 15.5))
    )
    
    private let model: XCameraViewLayerModel
    private var siteId: Int? = nil
    private var zoneModel: XCameraBindZoneDataLayerModel? = nil
    
    private lazy var viewLayer: XCameraBindViewLayerProtocol = self.mainView
    private lazy var strings: XCameraBindControllerStrings =  XCameraBindControllerStrings()
    
    init(model: XCameraViewLayerModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = model.name
        subTitle = strings.subTitle
        setNavigationViewBuilder(navigationViewBuider)
        mainView.delegate = self
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            sitesDisposable?.dispose()
            zonesDisposable?.dispose()
            bindDisposable?.dispose()
        }
    }
    
    private var sitesDisposable: Disposable?
    private func sitesSingle() -> Single<[XCameraBindSiteDataLayerModel]> {
        DataManager.shared.getSites(completable: true)
            .map({ result in XCameraBindDataLayerModelMapper.cast(site: result.site) })
            .toArray()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var zonesDisposable: Disposable?
    private func zonesSingle() -> Single<[XCameraBindZoneDataLayerModel]> {
        return DataManager.shared.getZonesWithoutBindingCamera()
            .asObservable()
            .concatMap({ dzs in Observable.from(dzs) })
            .filter({ dz in dz.siteIds.contains(where: { siteId in siteId == Int64(self.siteId ?? 0) }) })
            .map({ dz in XCameraBindDataLayerModelMapper.cast(dz: dz) })
            .toArray()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var bindDisposable: Disposable?
    private func bindCameraSingle() -> Single<DCommandResult> {
        doOnAvailable(deviceId: zoneModel!.deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.IV_BIND_CAM, D: ["device": self.zoneModel!.deviceId, "section": self.zoneModel!.sectionId, "zone": self.zoneModel!.zoneId, "id": self.model.id]) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
}

extension XCameraBindController: XCameraBindViewDelegate {
    func bindViewTapped() {
        bindDisposable?.dispose()
        viewLayer.showProgress()
        bindDisposable = bindCameraSingle().subscribe(onSuccess: showResult, onError: showErrorAlert)
    }
    
    func showErrorAlert(error: Error) {
        viewLayer.hideProgress()
        showErrorAlert(message: error.localizedDescription)
    }

    func showResult(result: DCommandResult? = nil) {
        viewLayer.hideProgress()
        if (result?.success == true) {
            navigationController?.popViewController(animated: true)
        } else {
            showErrorAlert(message: result?.message)
        }
    }
    
    func showErrorAlert(message: String?) {
        let alert = XAlertController(style: errorAlet(), strings: message == nil ? strings.errorBindingAlert() : strings.errorAlert(message: message!))
        navigationController?.present(alert, animated: true)
    }
    
    func objectSelectViewTapped() {
        sitesDisposable?.dispose()
        sitesDisposable = sitesSingle().subscribe(onSuccess: showObjectSelectBottomSheet)
    }
    
    func showObjectSelectBottomSheet(sites: [XCameraBindSiteDataLayerModel]) {
        var items: [XBottomSheetViewItem] = []
        sites.forEach { site in items.append(XBottomSheetViewItem(title: site.name, any: site.siteId)) }
        let bottomSheet = XBottomSheetController(style: objectAndZoneBottomSheet(), title: strings.objectSelectTitle, items: items)
        bottomSheet.onSelectedItem = objectSelectBottomSheetSelectedItem
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func objectSelectBottomSheetSelectedItem(bottomSheet: XBottomSheetController, index: Int, item: XBottomSheetViewItem) {
        bottomSheet.dismiss(animated: true, completion: { self.viewLayer.setObjectValue(name: item.title) })
        guard let siteId = item.any as? Int else { return }
        if (self.siteId != nil && self.siteId != siteId) { self.viewLayer.setZoneValue(name: nil) }
        self.siteId = siteId
    }
    
    func zoneSelectViewTapped() {
        if (siteId == nil) {
            let alert = XAlertController(style: errorAlet(), strings: strings.notObjectErrorAlert())
            navigationController?.present(alert, animated: true)
        } else {
            zonesDisposable?.dispose()
            zonesDisposable = zonesSingle().subscribe(onSuccess: showZoneSelectBottomSheet)
        }
    }
    
    func showZoneSelectBottomSheet(zones: [XCameraBindZoneDataLayerModel]) {
        var items: [XBottomSheetViewItem] = []
        zones.forEach { zone in items.append(XBottomSheetViewItem(title: zone.name, any: zone)) }
        let bottomSheet = XBottomSheetController(style: objectAndZoneBottomSheet(), title: strings.objectSelectTitle, items: items)
        bottomSheet.onSelectedItem = zoneSelectBottomSheetSelectedItem
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func zoneSelectBottomSheetSelectedItem(bottomSheet: XBottomSheetController, index: Int, item: XBottomSheetViewItem) {
        bottomSheet.dismiss(animated: true, completion: { self.viewLayer.setZoneValue(name: item.title) })
        guard let zone = item.any as? XCameraBindZoneDataLayerModel else { return }
        self.zoneModel = zone
    }
}
