//
//  XCameraBindViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraBindViewStyle {
    let background: UIColor
    let description, buttonTitle: XBaseLableStyle
    let button, action: XZoomButton.Style
}
extension XCameraBindView {
    typealias Style = XCameraBindViewStyle
}
