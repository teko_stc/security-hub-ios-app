//
//  XCameraBindListController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XCameraBindListController: XBaseViewController<XCameraBindListView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.showAddBindCameraController)]
    )
    
    private let model: XCameraViewLayerModel
   
    private lazy var viewLayer: XCameraBindListViewLayerProtocol = self.mainView
    private lazy var strings: XCameraBindListControllerStrings = XCameraBindListControllerStrings()

    init(model: XCameraViewLayerModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindsDisposable?.dispose()
        bindsDisposable = bindsSingle().subscribe(onSuccess: viewLayer.reloadData)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            bindsDisposable?.dispose()
            deleteBindCameraDisposable?.dispose()
        }
    }
    
    private var bindsDisposable: Disposable?
    private func bindsSingle() -> Single<[XCameraBindViewLayerModel]> {
        DataManager.shared.hubHelper.get(.IV_GET_RELATIONS, D: ["device": 0, "section": 0, "zone": 0])            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubRelations?) in if let r = value?.items { return r } else { return [] } })
            .do(onSuccess: { binds in DataManager.shared.relation = binds })
            .asObservable().concatMap ({ binds in return Observable.from(binds) })
            .filter({ bind in bind.camId == self.model.id })
            .map({ bind in return (bind, DataManager.shared.dbHelper.getZone(device_id: bind.device, section_id: bind.section, zone_id: bind.zone)) })
            .filter({ (bind, dz) in dz != nil })
            .map({ (bind, dz) in return XCameraBindViewLayerModelMapper.cast(bind: bind, dz: dz) })
            .toArray()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var deleteBindCameraDisposable: Disposable?
    private func deleteBindCameraSingle(model: XCameraBindViewLayerModel) -> Single<DCommandResult> {
        doOnAvailable(deviceId: model.deviceId.int64)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { [weak self] _ in self?.showToast(message: "COMMAND_BEEN_SEND".localized()) })
            .flatMap({ _ in DataManager.shared.hubHelper.setCommandWithResult(.IV_DEL_RELATION, D: ["device": model.deviceId, "section": model.sectionId, "zone": model.zoneId]) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
}

extension XCameraBindListController: XCameraBindListViewDelegate {
    func cameraBindItemLongPress(model: XCameraBindViewLayerModel) {
        let alert = XAlertController(style:deleteAlet(), strings: strings.deleteAlert, positive: { self.deleteCameraBindViewTapped(model: model) })
        navigationController?.present(alert, animated: true)
    }
    
    func deleteCameraBindViewTapped(model: XCameraBindViewLayerModel) {
        deleteBindCameraDisposable?.dispose()
        deleteBindCameraDisposable = deleteBindCameraSingle(model: model).subscribe(onSuccess: showResult, onError: { _ in self.showResult() })
    }
    
    func showResult(result: DCommandResult? = nil) {
        if (result?.success == true) {
            bindsDisposable?.dispose()
            bindsDisposable = bindsSingle().subscribe(onSuccess: viewLayer.reloadData)
        } else {
            showErrorAlert(message: result?.message)
        }
    }
    
    func showErrorAlert(message: String?) {
        let alert = XAlertController(style: errorAlet(), strings: message == nil ? strings.errorAnBindingAlert() : strings.errorAlert(message: message!))
        navigationController?.present(alert, animated: true)
    }
    
    func showAddBindCameraController() {
        let controller = XCameraBindController(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
}
