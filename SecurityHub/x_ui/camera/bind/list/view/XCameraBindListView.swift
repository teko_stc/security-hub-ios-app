//
//  XCameraBindListView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraBindViewLayerModelMapper {
    //TODO Icons
    static func cast(bind: HubRelation, dz: DZoneWithSitesDeviceSectionInfo?) -> XCameraBindViewLayerModel {
        return XCameraBindViewLayerModel(imageName: "ic_datchik_obshaya", siteId: Int(dz?.siteIds.first ?? 0), deviceId: Int(bind.device), sectionId: Int(bind.section), zoneId: Int(bind.zone), siteName: dz?.siteNames ?? "", zoneName: dz?.zone.name ?? "")
    }
}
struct XCameraBindViewLayerModel {
    let imageName: String
    let siteId, deviceId, sectionId, zoneId: Int
    let siteName, zoneName: String
}
protocol XCameraBindListViewLayerProtocol {
    func reloadData(_ items: [XCameraBindViewLayerModel])
}

class XCameraBindListView: UIView, UITableViewDataSource, UITableViewDelegate {
    public var delegate: XCameraBindListViewDelegate?
    
    private lazy var strings: XCameraBindListViewStrings = XCameraBindListViewStrings()
    private lazy var style: Style = styles()
    private var descriptionView: UILabel = UILabel()
    private var tableView = UITableView(frame: .zero, style: .plain)
    private var items: [XCameraBindViewLayerModel] = []

    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.background
        
        descriptionView.text = strings.description
        descriptionView.font = style.description.font
        descriptionView.textColor = style.description.color
        descriptionView.textAlignment = style.description.alignment
        descriptionView.numberOfLines = 0
        descriptionView.isHidden = true
        descriptionView.alpha = 0
        addSubview(descriptionView)
        
        tableView.separatorStyle = .none
        tableView.sectionFooterHeight = 0
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = style.background
        addSubview(tableView)
    }
    
    private func setConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        descriptionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            descriptionView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            descriptionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            descriptionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
        ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        cell.setStyle(style.cell)
        cell.setContent(model: items[indexPath.row]) { model in
            tableView.deselectRow(at: indexPath, animated: false)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            self.delegate?.cameraBindItemLongPress(model: model)
        }
        return cell
    }
}

extension XCameraBindListView: XCameraBindListViewLayerProtocol {
    func reloadData(_ items: [XCameraBindViewLayerModel]) {
        self.items = items
        self.tableView.reloadData()
        if (items.count == 0) { self.tableView.isHidden = true } else { self.descriptionView.isHidden = true }
        UIView.animate(withDuration: 0.3, animations: {
            self.descriptionView.alpha = items.count == 0 ? 1 : 0
            self.tableView.alpha = items.count == 0 ? 0 : 1
        }) { _ in
            if (items.count == 0) { self.descriptionView.isHidden = false } else { self.tableView.isHidden = false }
        }
    }
}
 
