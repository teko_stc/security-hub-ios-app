//
//  XCameraBindListViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XCameraBindListViewDelegate {
    func cameraBindItemLongPress(model: XCameraBindViewLayerModel)
}
