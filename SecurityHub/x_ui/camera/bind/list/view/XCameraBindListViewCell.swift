//
//  XCameraBindListViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraBindListViewCell: UITableViewCell {
    static var identifier = "XCameraBindListViewCell.identifier"
    private var model: XCameraBindViewLayerModel?
    private var onLong: ((XCameraBindViewLayerModel) -> ())?
    private let iconView: UIImageView = UIImageView()
    private let titleView: UILabel = UILabel()
    private let subTitleView: UILabel = UILabel()
    private let backView = UIView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: XCameraBindViewLayerModel, onLong: @escaping (XCameraBindViewLayerModel) -> ()) {
        self.model = model
        self.onLong = onLong
        iconView.image = UIImage(named: model.imageName)?.withRenderingMode(.alwaysTemplate)
        titleView.text = model.zoneName
        subTitleView.text = model.siteName
    }
    
    public func setStyle(_ style: Style) {
        backgroundColor = style.background
        backView.backgroundColor = style.selectedBackground
        
        iconView.tintColor = style.iconColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        titleView.numberOfLines = 0
        
        subTitleView.font = style.subTitle.font
        subTitleView.textColor = style.subTitle.color
        subTitleView.textAlignment = style.subTitle.alignment
        subTitleView.numberOfLines = 0
        
        setConstraints()
    }
    
    private func initViews() {
        selectedBackgroundView = backView
        backView.layer.cornerRadius = 5
        contentView.addSubview(iconView)
        contentView.addSubview(titleView)
        contentView.addSubview(subTitleView)
        
        contentView.isUserInteractionEnabled = true
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGestured))
        contentView.addGestureRecognizer(long)
    }
    
    private func setConstraints() {
        iconView.translatesAutoresizingMaskIntoConstraints = false
        titleView.translatesAutoresizingMaskIntoConstraints = false
        subTitleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: 36),
            iconView.heightAnchor.constraint(equalToConstant: 36),
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),

            titleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
            titleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            subTitleView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 4),
            subTitleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 16),
            subTitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            subTitleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ])
    }
    
    @objc private func longGestured(sender: UILongPressGestureRecognizer) {
        if sender.state == .ended, let model = model, let onLong = onLong { onLong(model) }
    }
}
extension XCameraBindListView {
    typealias Cell = XCameraBindListViewCell
}
