//
//  XCameraBindListViewCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraBindListViewCellStyle {
    let background, selectedBackground, iconColor: UIColor
    let title, subTitle: XBaseLableStyle
}
extension XCameraBindListViewCell {
    typealias Style = XCameraBindListViewCellStyle
}
