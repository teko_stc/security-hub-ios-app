//
//  XCameraBindListViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraBindListViewStyle {
    let background: UIColor
    let description: XBaseLableStyle
    let cell: XCameraBindListView.Cell.Style
}
extension XCameraBindListView {
    typealias Style = XCameraBindListViewStyle
}
