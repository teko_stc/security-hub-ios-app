//
//  XCameraBindListViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraBindListView {
    func styles() -> Style {
        Style(
            background: UIColor.white,
            description: XBaseLableStyle(
                color: UIColor(red: 0xbe/255, green: 0xbe/255, blue: 0xbe/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            ),
            cell: Cell.Style(
                background: UIColor.white,
                selectedBackground: UIColor(red: 0xbe/255, green: 0xbe/255, blue: 0xbe/255, alpha: 0.5),
                iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .left
                ),
                subTitle: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 11.3),
                    alignment: .left
                )
            )
        )
    }
}

extension XCameraBindListController {
    func deleteAlet() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .horizontal,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func errorAlet() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: nil
        )
    }
}
