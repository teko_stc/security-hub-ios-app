//
//  XCameraBindListViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 01.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraBindListViewStrings {
    /// Камера еще не привязана ни к одному из ваших датчиков
    var description: String { "CAMERA_NOT_BINDED_TEXT".localized() }
}

class XCameraBindListControllerStrings {
    /// Привязка к зонам
    var title: String { "CAMEA_TITLE_BIND_LIST".localized() }
    
    var deleteAlert: XAlertView.Strings {
        XAlertView.Strings(
            /// Вы действительно хотите удалить привязку камеры к зоне?
            title: "CAMERA_DEL_RELATION_DIALOG_MESS".localized(),
            text: nil,
            /// Да
            positive: "YES".localized(),
            /// Нет
            negative: "NO".localized()
        )
    }
    
    func errorAlert(message: String) -> XAlertView.Strings {
        XAlertView.Strings (
            title: message,
            text: nil,
            /// Ok
            positive: "OK".localized(),
            negative: nil
        )
    }
    
    func errorAnBindingAlert() -> XAlertView.Strings {
        XAlertView.Strings (
            /// Не удалось отвязать камеру от датчика. Попробуйте повторить попытку позже.
            title: "D3_SUCCEED".localized(),
            text: nil,
            /// Ok
            positive: "OK".localized(),
            negative: nil
        )
    }
}
