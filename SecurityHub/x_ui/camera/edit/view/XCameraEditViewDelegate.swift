//
//  XCameraEditViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XCameraEditViewDelegate {
    func nameChangeViewTapped()
    func bindCameraViewTapped()
    func videoQualityChangeViewTapped()
    func videoVoiceChangeViewTapped(isOn: Bool)
    func rtspLinkChangeViewTapped()
    func deleteCameraViewTapped()
}
