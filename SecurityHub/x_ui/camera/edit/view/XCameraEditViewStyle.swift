//
//  XCameraEditViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraEditViewStyle {
    let background, card: UIColor
    let _default, onlyTitle, delete, _switch: XEditButton.Style
}

extension XCameraEditView {
    typealias Style = XCameraEditViewStyle
}
