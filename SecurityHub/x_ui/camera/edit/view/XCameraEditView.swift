//
//  XCameraEditView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XCameraEditIvideonViewProtocol {
    func show(name: String, videoQuality: String, videoVoice: Bool)
    
    func updateName(_ name: String)
    
    func updateVideoQuality(_ videoQuality: String)

    func updateVideoVoice(_ isOn: Bool)
}

protocol XCameraEditRTSPViewProtocol {
    func show(name: String)
    
    func updateName(_ name: String)
}

class XCameraEditView: UIView {
    public var delegate: XCameraEditViewDelegate?
    
    private let strings: XCameraEditViewStrings = XCameraEditViewStrings()
    private let cardView: UIView = UIView()
    private var nameView, bindOrRtspView: XEditButton!
    
    private var videoQualityView, videoVoiceView, deleteView: XEditButton?

    init() {
        super.init(frame: .zero)
        initViews(style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews(_ style: Style) {
        backgroundColor = style.background
        
        cardView.backgroundColor = style.card
        cardView.layer.cornerRadius = 20
        addSubview(cardView)
        
        nameView = XEditButton(style: style._default)
        nameView.onTapped = { self.delegate?.nameChangeViewTapped() }
        nameView.headerTitle = strings.editCameraName
        addSubview(nameView)
        
        bindOrRtspView = XEditButton(style: style.onlyTitle)
        addSubview(bindOrRtspView)
    }
    
    private func setConstraints() {
        cardView.translatesAutoresizingMaskIntoConstraints = false
        nameView.translatesAutoresizingMaskIntoConstraints = false
        bindOrRtspView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),

            nameView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            nameView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 60),
            nameView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant:  -20),
            nameView.heightAnchor.constraint(equalToConstant: 54),
            
            bindOrRtspView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 10),
            bindOrRtspView.leadingAnchor.constraint(equalTo: nameView.leadingAnchor),
            bindOrRtspView.trailingAnchor.constraint(equalTo: nameView.trailingAnchor),
            bindOrRtspView.heightAnchor.constraint(equalTo: nameView.heightAnchor),
        ])
    }
}

extension XCameraEditView: XCameraEditIvideonViewProtocol {
    func updateName(_ name: String) { nameView.title = name }
    
    func updateVideoQuality(_ videoQuality: String) { videoQualityView?.title = videoQuality }
    
    func updateVideoVoice(_ isOn: Bool) { videoVoiceView?.isSelected = isOn }
    
    func show(name: String, videoQuality: String, videoVoice: Bool) {
        let style = style()
        nameView.title = name
    
        bindOrRtspView.title = strings.editBindingCamera
        bindOrRtspView.onTapped = { self.delegate?.bindCameraViewTapped() }

        videoQualityView = XEditButton(style: style._default)
        videoQualityView!.onTapped = { self.delegate?.videoQualityChangeViewTapped() }
        videoQualityView!.headerTitle = strings.editQualityVideo
        videoQualityView!.title = videoQuality
        addSubview(videoQualityView!)
        videoQualityView!.translatesAutoresizingMaskIntoConstraints = false
        videoQualityView!.topAnchor.constraint(equalTo: bindOrRtspView.bottomAnchor, constant: 10).isActive = true
        videoQualityView!.leadingAnchor.constraint(equalTo: nameView.leadingAnchor).isActive = true
        videoQualityView!.trailingAnchor.constraint(equalTo: nameView.trailingAnchor).isActive = true
        videoQualityView!.heightAnchor.constraint(equalTo: nameView.heightAnchor).isActive = true
        
        videoVoiceView = XEditButton(style: style._switch)
        videoVoiceView!.onSelected = { isOn in self.delegate?.videoVoiceChangeViewTapped(isOn: isOn) }
        videoVoiceView!.title = strings.editVideoVoice
        videoVoiceView!.isSelected = videoVoice
        addSubview(videoVoiceView!)
        videoVoiceView!.translatesAutoresizingMaskIntoConstraints = false
        videoVoiceView!.topAnchor.constraint(equalTo: videoQualityView!.bottomAnchor, constant: 10).isActive = true
        videoVoiceView!.leadingAnchor.constraint(equalTo: nameView.leadingAnchor).isActive = true
        videoVoiceView!.trailingAnchor.constraint(equalTo: nameView.trailingAnchor).isActive = true
        videoVoiceView!.heightAnchor.constraint(equalTo: nameView.heightAnchor).isActive = true
        
        cardView.bottomAnchor.constraint(equalTo: videoVoiceView!.bottomAnchor, constant: 20).isActive = true
    }
}

extension XCameraEditView: XCameraEditRTSPViewProtocol {
    func show(name: String) {
        let style = style()
        nameView.title = name
    
        bindOrRtspView.title = strings.editRTSPLinkCamera
        bindOrRtspView.onTapped = { self.delegate?.rtspLinkChangeViewTapped() }
        
        deleteView = XEditButton(style: style.delete)
        deleteView!.onTapped = { self.delegate?.deleteCameraViewTapped() }
        deleteView!.title = strings.delete
        addSubview(deleteView!)
        deleteView!.translatesAutoresizingMaskIntoConstraints = false
        deleteView!.topAnchor.constraint(equalTo: bindOrRtspView.bottomAnchor, constant: 10).isActive = true
        deleteView!.leadingAnchor.constraint(equalTo: nameView.leadingAnchor).isActive = true
        deleteView!.trailingAnchor.constraint(equalTo: nameView.trailingAnchor).isActive = true
        deleteView!.heightAnchor.constraint(equalTo: nameView.heightAnchor).isActive = true
        
        cardView.bottomAnchor.constraint(equalTo: deleteView!.bottomAnchor, constant: 20).isActive = true
    }
}
