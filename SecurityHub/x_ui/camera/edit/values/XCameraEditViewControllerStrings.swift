//
//  XCameraEditViewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraEditViewStrings {
    /// Имя камеры
    var editCameraName: String { "N_TITLE_CAMERA_NAME".localized() }
    
    /// Изменить привязку камеры к зонам
    var editBindingCamera: String { "CAMERA_BIND_SET".localized() }
    
    /// Качество видео
    var editQualityVideo: String { "CAMERA_SETTINGS_VIDEO_Q".localized() }
    
    /// Звук видео
    var editVideoVoice: String { "CAMERA_SETTINGS_AUDIO_SETTINGS".localized() }
    
    /// RTSP-ссылка
    var editRTSPLinkCamera: String { "N_CAMERAS_SETTINGS_LINK".localized() }
    
    /// Удалить
    var delete: String { "DELETE".localized() }
}

class XCameraEditControllerStrings {
    func deleteAlert(name: String) -> XAlertView.Strings {
        XAlertView.Strings(
            /// Удалить IP-камеру
            title: "N_RTSP_DELETE_Q".localized() + " \"\(name)\"?",
            /// Камера будет удалена из системы без возможности востановления.
            text: "N_DEL_RTSP_SUBTITLE".localized(),
            /// Удалить
            positive: "DELETE".localized(),
            /// Отмена
            negative: "ADB_CANCEL".localized()
        )
    }
    
    /// Выберите качество видео
    var videoQualityTitle: String { "CAMERA_SETTINGS_VIDEO_Q_SET".localized() }
    
    /// По умолчанию
    var videoQualityItem1: String { "SUMMARY_DEFAULT".localized() }
    
    /// Низкое
    var videoQualityItem2: String { "menu_camera_quality_low".localized() }
    
    /// Среднее
    var videoQualityItem3: String { "menu_camera_quality_medium".localized() }
    
    /// Высокое
    var videoQualityItem4: String { "menu_camera_quality_high".localized() }
    
    var rtspEnterLink: String { "N_CAMERAS_SETTINGS_LINK".localized() }
    var cameraEnterName: String { "N_CAMERAS_SETTINGS_NAME".localized() }
    
    func errorRenameAlert() -> XAlertView.Strings {
        XAlertView.Strings (
            /// Не удалось переименовать камеру. Попробуйте повторить попытку позже.
            title: "D3_SUCCEED".localized(),
            text: nil,
            /// OK
            positive: "OK".localized(),
            negative: nil
        )
    }
}
