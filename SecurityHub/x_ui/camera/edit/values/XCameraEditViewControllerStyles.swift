//
//  XCameraEditViewControllerStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraEditView {
    func style() -> Style {
        Style(
            background: UIColor.lightGray,
            card: UIColor.white,
            _default: XEditButton.Style(
                header: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .left
                ), title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    alignment: .left
                )
            ),
            onlyTitle: XEditButton.Style(
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    alignment: .left
                )
            ),
            delete: XEditButton.Style(
                title: XBaseLableStyle(
                    color: UIColor.red,
                    font: UIFont(name: "Open Sans", size: 18.5),
                    alignment: .left
                )
            ),
            _switch: XEditButton.Style(
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 18.5),
                    alignment: .left
                ),
                _switch: XSwitch.Style(
                    select: XSwitchStyle.State(
                        backgroundColor: UIColor.clear,
                        borderColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                        thunmColor: UIColor(red: 0x91/255, green: 0xf2/255, blue: 0x79/255, alpha: 1)
                    ),
                    unselet: XSwitchStyle.State(
                        backgroundColor: UIColor.clear,
                        borderColor: UIColor(red: 0xbc/255, green: 0xbc/255, blue: 0xbc/255, alpha: 1),
                        thunmColor: UIColor(red: 0xff/255, green: 0x95/255, blue: 0x4d/255, alpha: 1)
                    )
                )
            )
        )
    }
}

extension XCameraEditController {
    func deleteAlet() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.8)
            ),
            buttonOrientation: .vertical,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
    
    func videoQuality() -> XBottomSheetView.Style {
        XBottomSheetView.Style(
            backgroundColor: UIColor.white,
            title: XBottomSheetView.Style.Title(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                aligment: .left
            ),
            cell: XBottomSheetView.Cell.Style(
                backgroundColor: UIColor.white,
                selectedBackgroundColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 0.2),
                title: XBottomSheetView.Cell.Style.Label(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    select: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
                )
            )
        )
    }
    
    func errorAlet() -> XAlertView.Style {
        XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style(
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.red
            ),
            negative: nil
        )
    }
}


