//
//  XCameraEditController+Routing.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 04.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraEditController {
    func showIvideonRenameController(name: String) {
        let controller = XTextEditController(title: strings.cameraEnterName, value: name, onCompleted: rtspRenameViewConfirmTapped)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showRtspRenameController(name: String) {
        let controller = XTextEditController(title: strings.cameraEnterName, value: name, onCompleted: iviRenameViewConfirmTapped)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showRenameErrorAlert() {
        let alert = XAlertController(style: self.errorAlet(), strings: self.strings.errorRenameAlert())
        self.navigationController?.present(alert, animated: true)
    }
    
    func showCameraBindListController(_ model: XCameraViewLayerModel) {
        let controller = XCameraBindListController(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showVideoQualityChangeBottomSheet(seletedItemIndex: Int) {
        let items = [
            XBottomSheetViewItem(title: strings.videoQualityItem2),
            XBottomSheetViewItem(title: strings.videoQualityItem3),
            XBottomSheetViewItem(title: strings.videoQualityItem4)
        ]
        let bottomSheet = XBottomSheetController(style: videoQuality(), title: strings.videoQualityTitle, selectedIndex: seletedItemIndex, items: items)
        bottomSheet.onSelectedItem = videoQualityItemSelected
        navigationController?.present(bottomSheet, animated: true)
    }
    
    func showRtspChangeLinkAlert(url: String) {
        let controller = XTextEditController(title: strings.rtspEnterLink, value: url, onCompleted: rtspLinkChangeViewConfirmTapped)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showDeleteCameraAlert(name: String) {
        let alert = XAlertController(style: deleteAlet(), strings: strings.deleteAlert(name: name), positive: deleteRtpsCameraViewConfirmTapped)
        navigationController?.present(alert, animated: true)
    }
}
