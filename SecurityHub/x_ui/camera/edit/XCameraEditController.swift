//
//  XCameraEditController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 31.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XCameraEditController: XBaseViewController<XCameraEditView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair", color: UIColor.colorFromHex(0x414042), viewTapped: self.back)
    )
    
    private var model: XCameraViewLayerModel
    
    private lazy var ivideonViewLayer: XCameraEditIvideonViewProtocol = self.mainView
    private lazy var rtspViewLayer: XCameraEditRTSPViewProtocol = self.mainView
    
    private lazy var rtspCameraDataLayer: XRTSPCameraDataLayerProtocol = DataManager.shared.dbHelper
    
    lazy var strings: XCameraEditControllerStrings = XCameraEditControllerStrings()

    init(model: XCameraViewLayerModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        switch model.type {
        case .ivideon: ivideonViewLayer.show(name: model.name, videoQuality: "Auto", videoVoice: false); break;
        case .rtsp: rtspViewLayer.show(name: model.name);  break;
        }
        videoInfoDisposable = videoInfoSingle().subscribe(onSuccess: setVideoInfo)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            videoInfoDisposable?.dispose()
            videoQualityDisposable?.dispose()
            videoVoiceDisposable?.dispose()
            iviNameChangeDisposable?.dispose()
        }
    }
    
    private func setVideoInfo(videoQuality: Int, videoVoice: Bool) {
        switch videoQuality {
        case 0: ivideonViewLayer.updateVideoQuality(strings.videoQualityItem2); break;
        case 1: ivideonViewLayer.updateVideoQuality(strings.videoQualityItem3); break;
        case 2: ivideonViewLayer.updateVideoQuality(strings.videoQualityItem4); break;
        default: ivideonViewLayer.updateVideoQuality(strings.videoQualityItem1); break;
        }
        ivideonViewLayer.updateVideoVoice(videoVoice)
    }
    
    private var videoInfoDisposable: Disposable?
    private func videoInfoSingle() -> Single<(videoQuality: Int, videoVoice: Bool)> {
        DataManager.shared.getCamSet(model.id)
            .map({ cam in return (videoQuality: Int(cam.q), videoVoice: cam.audio) })
            .asSingle()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var videoQualityDisposable: Disposable?
    private func videoQualitySingle() -> Single<Int>{
        DataManager.shared.getCamSet(model.id)
            .map({ Int($0.q) })
            .asSingle()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    private func videoQualityChangeCompletable(videoQuality: Int) -> Completable {
        DataManager.shared.getCamSet(model.id)
            .do(onNext: { cam in var camSet = cam;camSet.q = Int64(videoQuality);DataManager.shared.updCamSet(camSet) })
            .asSingle().asCompletable()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var videoVoiceDisposable: Disposable?
    private func videoVoiceChangeCompletable(isOn: Bool) -> Completable {
        DataManager.shared.getCamSet(model.id)
            .do(onNext: { cam in var camSet = cam;camSet.audio = isOn; DataManager.shared.updCamSet(camSet) })
            .asSingle().asCompletable()
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private var iviNameChangeDisposable: Disposable?
    private func iviNameChangeSingle(name: String) -> Single<Bool> {
        DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
            .flatMap({ token in IvideonApi.updateCameraName(id: self.model.id, accessToken: token ?? "", value: name) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
}

extension XCameraEditController: XCameraEditViewDelegate {
    func nameChangeViewTapped() {
        switch model.type {
        case .rtsp:     return showIvideonRenameController(name: model.name)
        case .ivideon:  return showRtspRenameController(name: model.name)
        }
    }
    
    func rtspRenameViewConfirmTapped(controller: UIViewController, name: String){
        let result = rtspCameraDataLayer.update(id: Int(model.id) ?? -1, name: name)
        if result {
            controller.navigationController?.popViewController(animated: true)
            self.model.name = name
            self.rtspViewLayer.updateName(name)
        } else {
            self.showRenameErrorAlert()
        }
    }
    
    func iviRenameViewConfirmTapped(controller: UIViewController, name: String){
        iviNameChangeDisposable?.dispose()
        iviNameChangeDisposable = iviNameChangeSingle(name: name).subscribe(onSuccess: { result in
            if (result) {
                controller.navigationController?.popViewController(animated: true)
                self.model.name = name
                self.ivideonViewLayer.updateName(name)
            } else {
                self.showRenameErrorAlert()
            }
        })
    }
    
    func bindCameraViewTapped() {
        showCameraBindListController(model)
    }
    
    func videoQualityChangeViewTapped() {
        videoQualityDisposable?.dispose()
        videoQualityDisposable = videoQualitySingle().subscribe(onSuccess: showVideoQualityChangeBottomSheet)
    }
    
    func videoQualityItemSelected(bottomSheet: XBottomSheetController, index: Int, item: XBottomSheetViewItem) {
        videoQualityDisposable?.dispose()
        videoQualityDisposable = videoQualityChangeCompletable(videoQuality: index)
            .subscribe(onCompleted: { bottomSheet.dismiss(animated: true) { self.ivideonViewLayer.updateVideoQuality(item.title) } })
    }
    
    func videoVoiceChangeViewTapped(isOn: Bool) {
        videoVoiceDisposable?.dispose()
        videoVoiceDisposable = videoVoiceChangeCompletable(isOn: isOn).subscribe()
    }
    
    func rtspLinkChangeViewTapped() {
        showRtspChangeLinkAlert(url: model.url)
    }
    
    func rtspLinkChangeViewConfirmTapped(controller: UIViewController, url: String) {
        let result = rtspCameraDataLayer.update(id: Int(model.id) ?? -1, url: url)
        if result {
            controller.navigationController?.popViewController(animated: true)
            self.model.url = url
        }
    }
    
    func deleteCameraViewTapped() {
        showDeleteCameraAlert(name: model.name)
    }
    
    func deleteRtpsCameraViewConfirmTapped() {
        let result = rtspCameraDataLayer.delete(id: Int(model.id) ?? -1)
        if result { navigationController?.popToRootViewController(animated: true) }
    }
}
