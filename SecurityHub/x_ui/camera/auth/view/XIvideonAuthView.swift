//
//  XIvideonAuthView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import WebKit

class XIvideonAuthView: UIView {
    public var delegate: XIvideonAuthViewDelegate?
    
    private var webView: WKWebView = WKWebView()
    private var loaderView: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    init(){
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
        
    public func loadUrl(_ url_string: String) {
        guard let url = URL(string: url_string) else { return }
        webView.load(URLRequest(url: url))
    }
    
    private func initViews() {
        webView.isOpaque = false
        webView.navigationDelegate = self
        webView.backgroundColor = UIColor.colorFromHex(0x1a96e5)
        addSubview(webView)
        
        loaderView.center = center
        loaderView.hidesWhenStopped = true
        loaderView.startAnimating()
        addSubview(loaderView)
    }
    
    private func setConstraints() {
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loaderView.heightAnchor.constraint(equalToConstant: 50),
            loaderView.widthAnchor.constraint(equalToConstant: 50),
            loaderView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),

            webView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension XIvideonAuthView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loaderView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else { return decisionHandler(.allow) }
        delegate?.webViewNavigationAction(to: url, decisionHandler: decisionHandler)
    }
}
