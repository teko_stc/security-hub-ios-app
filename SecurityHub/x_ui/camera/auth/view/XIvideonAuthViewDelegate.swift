//
//  XIvideonAuthViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import WebKit

protocol XIvideonAuthViewDelegate {
    func webViewNavigationAction(to url: URL, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void)
}
