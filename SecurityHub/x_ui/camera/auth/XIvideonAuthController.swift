//
//  XIvideonAuthController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import WebKit

class XIvideonAuthController: XBaseViewController<XIvideonAuthView> {

    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: UIColor.colorFromHex(0x1a96e5),
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: .white, viewTapped: self.back),
        titleView: XBaseLableStyle(color: .white, font: UIFont(name: "Open Sans", size: 25))
    )
    
    private let strings: XIvideonAuthControllerStrings = XIvideonAuthControllerStrings()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        clearCookie()
        startSession()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil { NotificationCenter.default.post(name: HubNotification.iviAuth, object: nil) }
    }
    
    private func clearCookie(){
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { (records) in
            records.filter { $0.displayName.contains("ivideon") }.forEach { record in dataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: [record], completionHandler: {}) }
        }
    }

    private func startSession() {
        _ = DataManager.shared.getIviSaveSession()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ses in if let url = XTargetUtils.ivideon { self.mainView.loadUrl(url + "&session=" + ses) } })
    }
    
    private func endSession() {
        _ = DataManager.shared.getIvideonToken()
            .observeOn(MainScheduler())
            .subscribe(onNext: { [weak self] (token) in
                if let _ = token {
                    self?.navigationController?.popToRootViewController(animated: true)
                } else {
                    self?.endSession()
                }
            })
    }
}

extension XIvideonAuthController: XIvideonAuthViewDelegate {
    func webViewNavigationAction(to url: URL, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (url.absoluteString.contains("sign-up") || url.absoluteString.contains("password-recover")) {
            UIApplication.shared.open(url)
            return decisionHandler(.cancel)
        }
        if (url.absoluteString.contains("go.ivideon")) {
            return decisionHandler(.cancel)
        }
        if url.absoluteString.contains("iv.php") && url.absoluteString.contains("code=") {
            endSession()
        }
        return decisionHandler(.allow)
    }
}
