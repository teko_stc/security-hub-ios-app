//
//  XIvideonAuthViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 28.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XIvideonAuthControllerStrings {
    /// Aвторизация iVideon
    var title: String { "CAMERA_TITLE_IV_REG".localized() }
}
