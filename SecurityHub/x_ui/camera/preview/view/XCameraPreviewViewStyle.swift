//
//  XCameraPreviewViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraPreviewViewStyle {
    let background, card, loader, errorIcon: UIColor
    
    let cell: XCameraEventCell.Style
    let bindDescription: XBaseLableStyle
    let bindButton: XImageButton.Style
    let bindSecButton: XZoomButton.Style
}
extension XCameraPreviewView {
    typealias Style = XCameraPreviewViewStyle
}
