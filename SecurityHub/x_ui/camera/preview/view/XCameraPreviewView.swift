//
//  XCameraPreviewView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AVKit
import MobilePlayer
import MobileVLCKit

class XCameraEventViewLayerMapper {
    static func cast(data: DEventCameraEntity) -> XCameraEventViewLayerModel {
			return XCameraEventViewLayerModel(id: data.eventId, icon: data.icon, description: data.description, record: data.record, time: data.time, names: XCameraEventViewLayerModel.Names(site: data.siteName, device: data.deviceName, section: data.sectionName, zone: data.zoneName),location: data.location)
    }
}

struct XCameraEventViewLayerModel {
    struct Names {
        let site, device, section, zone: String
    }
    
    let id: Int64
    let icon: String
    let description: String
    let record: String?
    let time: Int64
    let names: Names
		let location: (latitude: Double,longitude: Double)?
}

protocol XCameraPreviewViewLayerProtocol {
    func startAllStreams()
    func pauseAllStreams()
}

protocol XCameraPreviewRTSPViewLayerProtocol {
    func loadRtspStream(url: String)
}

protocol XCameraPreviewIvideonViewLayerProtocol {
    func loadIvideonStream(url: String, isVoiceOn: Bool)
    func errorIvideonStream()
    func updateIvideonCameraBindInfo(isBind: Bool)
    func addCameraEvent(model: XCameraEventViewLayerModel)
}

class XCameraPreviewView: UIView, VLCMediaPlayerDelegate, UITableViewDataSource, UITableViewDelegate {
    public var delegate: XCameraPreviewViewDelegate?
    
    private let cardView: UIView = UIView()
    private var playerView: UIView = UIView()
    private var loaderView: XLoaderView = XLoaderView()
    private var errorView: UIImageView = UIImageView(image: UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate))
    
    private var playerBottomSheetView: UIView = UIView()
    private var playerPlayPlauseButtonView: UIButton = UIButton()
    private var playerFullButtonView: UIButton = UIButton()

    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var bindButtonView: UIButton = UIButton(type: .infoLight)
    private var bindDescView: UILabel = UILabel()
    private var bindSecButtonView: XZoomButton!
    
    private var PlayerItemObserverContext = 0
    private var oldPlayerState: VLCMediaPlayerState? = nil
    private var rtspPlayer: VLCMediaPlayer?
    private var iviPlayer: AVPlayer?
    private var items: [XCameraEventViewLayerModel] = []
    
    private lazy var style: Style = styles()
    private lazy var strings: XCameraPreviewViewStrings = XCameraPreviewViewStrings()
    
    private var l_isBind: Bool = false
    private var playerViewHeightAnchor, playerViewWidthAnchor, playerViewBottomAnchor, playerViewCenterYAnchor, cardViewBottomAnchor, playerViewTopAnchor: NSLayoutConstraint!

    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.background
        
        cardView.backgroundColor = style.card
        cardView.layer.cornerRadius = 40
        addSubview(cardView)
        
        addSubview(playerView)
        
        loaderView.color = style.loader
        loaderView.isHidden = true
        addSubview(loaderView)
        
        errorView.tintColor = style.errorIcon
        errorView.isHidden = true
        errorView.contentMode = .scaleAspectFit
        addSubview(errorView)
        
        playerBottomSheetView.backgroundColor = UIColor.black
        playerBottomSheetView.alpha = 0.7
        playerBottomSheetView.isHidden = true
        playerBottomSheetView.alpha = 0
        playerView.addSubview(playerBottomSheetView)
        
        playerPlayPlauseButtonView.setImage(UIImage(named: "ic_pause_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerPlayPlauseButtonView.setImage(UIImage(named: "ic_play_button")?.withRenderingMode(.alwaysTemplate), for: .selected)
        playerPlayPlauseButtonView.tintColor = UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
        playerPlayPlauseButtonView.imageView?.contentMode = .scaleAspectFill
        playerPlayPlauseButtonView.contentVerticalAlignment = .fill
        playerPlayPlauseButtonView.contentHorizontalAlignment = .fill
        playerPlayPlauseButtonView.isHidden = true
        playerPlayPlauseButtonView.addTarget(self, action: #selector(playerPlayPlauseButtonViewTapped), for: .touchUpInside)
        playerBottomSheetView.addSubview(playerPlayPlauseButtonView)

        playerFullButtonView.setImage(UIImage(named: "ic_full_screen_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerFullButtonView.setImage(UIImage(named: "ic_full_screen_exit")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerFullButtonView.tintColor = UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1)
        playerFullButtonView.imageView?.contentMode = .scaleAspectFill
        playerFullButtonView.contentVerticalAlignment = .fill
        playerFullButtonView.contentHorizontalAlignment = .fill
        playerFullButtonView.addTarget(self, action: #selector(playerFullButtonViewTapped), for: .touchUpInside)
        playerBottomSheetView.addSubview(playerFullButtonView)
        
        tableView.separatorStyle = .none
        tableView.sectionFooterHeight = 0
        tableView.register(XCameraEventCell.self, forCellReuseIdentifier: XCameraEventCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = style.background
        addSubview(tableView)
        
        var attr: [ NSAttributedString.Key: Any] = [ .foregroundColor: style.bindButton.unselectColor ]
        if let font = style.bindButton.font { attr[.font] = font }
        bindButtonView.setAttributedTitle(NSAttributedString(string: strings.haveBind, attributes: attr), for: .normal)
        bindButtonView.setImage(UIImage(named: style.bindButton.imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
        bindButtonView.tintColor = style.bindButton.selectColor
        bindButtonView.semanticContentAttribute = .forceRightToLeft
        bindButtonView.isHidden = true
        bindButtonView.addTarget(self, action: #selector(bindButtonViewTapped), for: .touchUpInside)
        addSubview(bindButtonView)
        
        bindDescView.text = strings.bindDescription
        bindDescView.font = style.bindDescription.font
        bindDescView.textColor = style.bindDescription.color
        bindDescView.textAlignment = style.bindDescription.alignment
        bindDescView.numberOfLines = 0
        bindDescView.isHidden = true
        addSubview(bindDescView)

        bindSecButtonView = XZoomButton(style: style.bindSecButton)
        bindSecButtonView.text = strings.bindButton
        bindSecButtonView.isHidden = true
        bindSecButtonView.addTarget(self, action: #selector(bindSecButtonViewTapped), for: .touchUpInside)
        addSubview(bindSecButtonView)
    }
    
    private func setConstraints() {
        cardView.translatesAutoresizingMaskIntoConstraints = false
        playerView.translatesAutoresizingMaskIntoConstraints = false
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        errorView.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        bindButtonView.translatesAutoresizingMaskIntoConstraints = false
        bindDescView.translatesAutoresizingMaskIntoConstraints = false
        bindSecButtonView.translatesAutoresizingMaskIntoConstraints = false
        playerBottomSheetView.translatesAutoresizingMaskIntoConstraints = false
        playerPlayPlauseButtonView.translatesAutoresizingMaskIntoConstraints = false
        playerFullButtonView.translatesAutoresizingMaskIntoConstraints = false

        playerViewHeightAnchor = playerView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.57)
        playerViewWidthAnchor = playerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        cardViewBottomAnchor = cardView.bottomAnchor.constraint(equalTo: playerView.bottomAnchor, constant: 65)
        playerViewTopAnchor = playerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20)

        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
         
            playerViewTopAnchor!,
            playerView.centerXAnchor.constraint(equalTo: cardView.centerXAnchor),
            playerViewHeightAnchor!,
            playerViewWidthAnchor!,

            loaderView.centerYAnchor.constraint(equalTo: playerView.centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: playerView.centerXAnchor),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
            
            errorView.centerYAnchor.constraint(equalTo: loaderView.centerYAnchor),
            errorView.centerXAnchor.constraint(equalTo: loaderView.centerXAnchor),
            errorView.heightAnchor.constraint(equalToConstant: 30),
            errorView.widthAnchor.constraint(equalToConstant: 30),
            
            cardViewBottomAnchor!,
            
            bindButtonView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            bindButtonView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -15),
            
            tableView.topAnchor.constraint(equalTo: cardView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            bindSecButtonView.heightAnchor.constraint(equalToConstant: 40),
            bindSecButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.8),
            bindSecButtonView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            bindSecButtonView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            
            bindDescView.bottomAnchor.constraint(equalTo: bindSecButtonView.topAnchor, constant: -30),
            bindDescView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            bindDescView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.8),
        ])
    }
    
    @objc private func tapGesture(sender: UIGestureRecognizer) {
        if (playerBottomSheetView.isHidden) {
            playerBottomSheetView.isHidden = false;
            UIView.animate(withDuration: 0.3,
                           animations: { self.playerBottomSheetView.alpha = 1 })
        } else {
            UIView.animate(withDuration: 0.3,
                           animations: { self.playerBottomSheetView.alpha = 0 },
                           completion: { _ in self.playerBottomSheetView.isHidden = true; })
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .ended) { tapGesture(sender: sender) }
    }
    
    @objc private func playerPlayPlauseButtonViewTapped() {
//        if (playerView.player?.rate == 1.0) { playerView.player?.pause() }
//        else { playerView.player?.play(); }
    }
    
    private var isFull: Bool = false
    @objc private func playerFullButtonViewTapped() {
        self.isFull = !self.isFull
        playerFullButtonView.isSelected = !playerFullButtonView.isSelected
        playerViewHeightAnchor.constant = self.isFull ? UIScreen.main.bounds.width : (UIScreen.main.bounds.width * 0.57)
        playerViewWidthAnchor.constant = self.isFull ? frame.size.height : UIScreen.main.bounds.width
        cardViewBottomAnchor.isActive = !isFull
        playerViewTopAnchor.isActive = !isFull
        playerViewCenterYAnchor.isActive = isFull
//        delegate?.navigationStateNeedChange(isHidden: isFull)
        UIView.animate(withDuration: 0.5, animations: {
            self.playerView.transform = self.isFull ? CGAffineTransform(rotationAngle: 90 * .pi/180) : CGAffineTransform.identity
            self.layoutIfNeeded()
        })
        
        bindButtonView.isHidden = !(!isFull && l_isBind)
    }
    
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        guard let rtspPlayer = rtspPlayer else { return }
        if rtspPlayer.state == .stopped || rtspPlayer.state == .error { self.errorStream() }
        else if oldPlayerState == rtspPlayer.state && rtspPlayer.state == .buffering { self.hideLoader() }
        oldPlayerState = rtspPlayer.state
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
       if context == &PlayerItemObserverContext && keyPath == #keyPath(AVPlayerItem.status), let status = change?[NSKeyValueChangeKey.newKey] as? NSNumber  {
            switch (status.intValue as AVPlayer.Status.RawValue) {
            case AVPlayer.Status.readyToPlay.rawValue:  return hideLoader()
            case AVPlayer.Status.failed.rawValue:       return errorStream()
            default: return
            }
        }
    }
 
    private func hideLoader() {
        loaderView.isHidden = true
        errorView.isHidden = true
    }
    
    private func showLoader() {
        loaderView.isHidden = false
        errorView.isHidden = true
    }
    
    private func errorStream() {
        loaderView.isHidden = true
        errorView.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XCameraEventCell.identifier, for: indexPath) as! XCameraEventCell
        cell.setStyle(style.cell)
        cell.setDelegate(onViewTapped: { model in self.delegate?.eventCameraInfoTapped(event: model) }, onRecordTapped: { model in self.delegate?.recorpEventCameraInfoTapped(event: model) })
        cell.setContent(model: items[indexPath.row])
        return cell
    }
    
    @objc private func bindButtonViewTapped() {
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        delegate?.bindsCameraViewTapped()
    }
    
    @objc private func bindSecButtonViewTapped() {
        delegate?.addCameraBindViewTapped()
    }
}

extension XCameraPreviewView: XCameraPreviewViewLayerProtocol {
    func startAllStreams() {
        rtspPlayer?.play()
        iviPlayer?.playImmediately(atRate: 1.0)
    }
    
    func pauseAllStreams() {
        rtspPlayer?.pause()
        iviPlayer?.pause()
    }
}

extension XCameraPreviewView: XCameraPreviewRTSPViewLayerProtocol {
    func loadRtspStream(url: String) {
        self.showLoader()
        rtspPlayer = VLCMediaPlayer()
        rtspPlayer?.media = VLCMedia(url: URL(string: url)!)
        rtspPlayer?.drawable = playerView
        rtspPlayer?.delegate = self
    }
}

extension XCameraPreviewView: XCameraPreviewIvideonViewLayerProtocol {
    func loadIvideonStream(url: String, isVoiceOn: Bool) {
        self.showLoader()
        iviPlayer = AVPlayer()
        let playerItem = AVPlayerItem(url: URL(string: url)!)
        playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &PlayerItemObserverContext)
        iviPlayer?.replaceCurrentItem(with: playerItem);
        iviPlayer?.isMuted = !isVoiceOn

        let _playerView = PlayerView(frame: CGRect(x: 0, y: 0, width: playerView.frame.size.width, height: playerView.frame.size.height))
        _playerView.player = iviPlayer
        _playerView.backgroundColor = .black

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        _playerView.isUserInteractionEnabled = true
        _playerView.addGestureRecognizer(tap)
        let long = UITapGestureRecognizer(target: self, action: #selector(longGesture))
        _playerView.addGestureRecognizer(long)
        
        playerView.addSubview(_playerView)
        playerView.addSubview(playerBottomSheetView)
        
//        playerViewBottomAnchor = playerView.bottomAnchor.constraint(equalTo: centerYAnchor)
        playerViewCenterYAnchor = playerView.centerYAnchor.constraint(equalTo: centerYAnchor)
        _playerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _playerView.heightAnchor.constraint(equalTo: playerView.heightAnchor),
            _playerView.widthAnchor.constraint(equalTo: playerView.widthAnchor),
            
            playerBottomSheetView.bottomAnchor.constraint(equalTo: playerView.bottomAnchor),
            playerBottomSheetView.heightAnchor.constraint(equalToConstant: 60),
            playerBottomSheetView.widthAnchor.constraint(equalTo: playerView.widthAnchor),
            playerBottomSheetView.centerXAnchor.constraint(equalTo: playerView.centerXAnchor),
            
            playerPlayPlauseButtonView.centerYAnchor.constraint(equalTo: playerBottomSheetView.centerYAnchor),
            playerPlayPlauseButtonView.centerXAnchor.constraint(equalTo: playerBottomSheetView.centerXAnchor),
            playerPlayPlauseButtonView.heightAnchor.constraint(equalTo: playerBottomSheetView.heightAnchor, multiplier: 0.4),
            playerPlayPlauseButtonView.widthAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
            
            playerFullButtonView.centerYAnchor.constraint(equalTo: playerBottomSheetView.centerYAnchor),
            playerFullButtonView.trailingAnchor.constraint(equalTo: playerBottomSheetView.trailingAnchor, constant: -20),
            playerFullButtonView.heightAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
            playerFullButtonView.widthAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
        ])
    }
    
    func errorIvideonStream() {
        errorStream()
    }
    
    func updateIvideonCameraBindInfo(isBind: Bool) {
        l_isBind = isBind
        bindButtonView.isHidden = !isBind
        tableView.isHidden = !isBind
        bindDescView.isHidden = isBind
        bindSecButtonView.isHidden = isBind
    }
    
    func addCameraEvent(model: XCameraEventViewLayerModel) {
        items.insert(model, at: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        tableView.endUpdates()
    }
}
