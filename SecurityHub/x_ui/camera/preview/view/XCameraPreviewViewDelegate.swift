//
//  XCameraPreviewViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AVKit
import MobilePlayer
import MobileVLCKit

protocol XCameraPreviewViewDelegate {
    func addCameraBindViewTapped()
    func bindsCameraViewTapped()
    func fullScreenViewTapped(rtspPlayer: VLCMediaPlayer?, viPlayer: AVPlayer?)
    func eventCameraInfoTapped(event: XCameraEventViewLayerModel)
    func recorpEventCameraInfoTapped(event: XCameraEventViewLayerModel)
}
