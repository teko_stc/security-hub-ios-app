//
//  XCameraPreviewController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import AVKit
import MobileVLCKit

class XCameraPreviewController: XBaseViewController<XCameraPreviewView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_stair-1", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25)),
        rightViews: [XNavigationViewRightViewBuilder(imageName: "ic_settings", color: UIColor.colorFromHex(0x3abeff), viewTapped: self.settingsButtonViewTapped)]
    )

    private var model: XCameraViewLayerModel
    
    private lazy var viewLayer: XCameraPreviewViewLayerProtocol = self.mainView
    
    private lazy var rtspDataLayer: XRTSPCameraDataLayerProtocol = DataManager.shared.dbHelper
    private lazy var rtspViewLayer: XCameraPreviewRTSPViewLayerProtocol = self.mainView
    
    private lazy var ivideonViewLayer: XCameraPreviewIvideonViewLayerProtocol = self.mainView
    
    init(model: XCameraViewLayerModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = model.name
        setNavigationViewBuilder(navigationViewBuilder)
        
        mainView.delegate = self
        switch model.type {
        case .rtsp:
            rtspCameraDisposable?.dispose()
            rtspCameraDisposable = rtspCameraObserbable.subscribe()
            break;
        case .ivideon:
            ivideonCameraDisposable?.dispose()
            ivideonCameraDisposable = ivideonCameraObserbable.subscribe()
            
            eventsDisposable?.dispose()
            eventsDisposable = eventsObservable.subscribe()
            break;
        }
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            rtspCameraDisposable?.dispose()
            ivideonCameraDisposable?.dispose()
            bindsDisposable?.dispose()
            eventsDisposable?.dispose()
        }
    }
    
    @objc private func settingsButtonViewTapped() {
        let controller = XCameraEditController(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if model.type == .ivideon {
            bindsDisposable?.dispose()
            bindsDisposable = bindsSingle.subscribe()
        }
        viewLayer.pauseAllStreams()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewLayer.startAllStreams()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewLayer.pauseAllStreams()
    }
    
    private var rtspCameraDisposable: Disposable? = nil
    private var rtspCameraObserbable: Observable<XCameraViewLayerModel> {
        rtspDataLayer.item(id: Int(model.id) ?? -1)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onNext: { data in if data.state == .insert { self.rtspViewLayer.loadRtspStream(url: data.url) } })
            .do(onNext: { data in if data.state == .update && self.model.name != data.name { self.title = data.name } })
            .do(onNext: { data in if data.state == .update && self.model.url != data.url { self.rtspViewLayer.loadRtspStream(url: data.url) } })
            .map({ data in XCameraViewLayerMapper.cast(model: data) })
            .do(onNext: { model in self.model = model })
    }
    
    private var ivideonCameraDisposable: Disposable? = nil
    private var ivideonCameraObserbable: Single<(String, Bool)> {
        DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
            .asObservable()
            .concatMap({ token in return Observable.zip(Observable.just(token ?? ""), DataManager.shared.getCamSet(self.model.id)) })
            .map { (token, cs) in return ("\(IvideonApi.urlIvideonApi)/cameras/\(self.model.id)/live_stream?access_token=\(token)&format=hls&q=\(cs.q)", cs.audio)}
            .observeOn(ThreadUtil.shared.mainScheduler)
            .asSingle()
            .do(onSuccess: { url, isOn in
                print("loadIvideonStream")
                self.ivideonViewLayer.loadIvideonStream(url: url, isVoiceOn: isOn)
            })
            .do(onError: { _ in self.ivideonViewLayer.errorIvideonStream() })
    }
    
    private var bindsDisposable: Disposable?
    private var bindsSingle: Single<[XCameraBindViewLayerModel]> {
        DataManager.shared.hubHelper.get(.IV_GET_RELATIONS, D: ["device": 0, "section": 0, "zone": 0])            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubRelations?) in if let r = value?.items { return r } else { return [] } })
            .do(onSuccess: { binds in DataManager.shared.relation = binds })
            .asObservable().concatMap ({ binds in return Observable.from(binds) })
            .filter({ bind in bind.camId == self.model.id })
            .map({ bind in return (bind, DataManager.shared.dbHelper.getZone(device_id: bind.device, section_id: bind.section, zone_id: bind.zone)) })
            .filter({ (bind, dz) in dz != nil })
            .map({ (bind, dz) in return XCameraBindViewLayerModelMapper.cast(bind: bind, dz: dz) })
            .toArray()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onSuccess: { binds in self.ivideonViewLayer.updateIvideonCameraBindInfo(isBind: binds.count > 0) })
    }
    
    private var eventsDisposable: Disposable?
    private var eventsObservable: Observable<XCameraEventViewLayerModel> {
        DataManager.shared.getEvents(cameraId: model.id)
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ data in XCameraEventViewLayerMapper.cast(data: data)  })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onNext: { model in self.ivideonViewLayer.addCameraEvent(model: model) })
    }
}

extension XCameraPreviewController: XCameraPreviewViewDelegate {
    func addCameraBindViewTapped() {
        let controller = XCameraBindController(model: self.model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func bindsCameraViewTapped() {
        let controller = XCameraBindListController(model: self.model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func fullScreenViewTapped(rtspPlayer: VLCMediaPlayer?, viPlayer: AVPlayer?) {
        
    }
    
    func eventCameraInfoTapped(event: XCameraEventViewLayerModel) {
        let model = XCameraEventViewLayerModel(
            id: event.id,
            icon: event.icon,
            description: event.description,
            record: event.record == nil ? "clip/failed" : event.record,
            time: event.time,
            names: event.names,
						location: event.location
        )
        let controller = XCameraEventsController(model: model)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func recorpEventCameraInfoTapped(event: XCameraEventViewLayerModel) {
        if event.record == "clip/failed" || event.record == nil {
            showErrorColtroller(message: "IV_CLIP_FAILED".localized())
        } else {
            let controller = XCameraEventRecordController(model: event)
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
