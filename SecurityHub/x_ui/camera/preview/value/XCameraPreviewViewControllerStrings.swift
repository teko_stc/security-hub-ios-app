//
//  XCameraPreviewViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraPreviewViewStrings {
    var haveBind: String { "С привязкой к зонам" }
    var bindDescription: String { "Камера еще привязана ни к одному из ваших датчиков" }
    var bindButton: String { "Привязать" }
}
