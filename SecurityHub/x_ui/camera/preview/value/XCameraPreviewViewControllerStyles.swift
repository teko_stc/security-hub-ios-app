//
//  XCameraPreviewViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraPreviewView {
    func styles() -> Style {
        Style(
            background: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
            card: UIColor.white,
            loader: UIColor.lightGray,
            errorIcon: UIColor.red,
            cell: XCameraEventCell.Style(
                background: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
                iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                title: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 20),
                    alignment: .left
                ),
                text: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 15.5),
                    alignment: .left
                ),
                time: XBaseLableStyle(
                    color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                    font: UIFont(name: "Open Sans", size: 14.5),
                    alignment: .left
                )
            ),
            bindDescription: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .center
            ),
            bindButton: XImageButton.Style(
                imageName: "ic_information",
                font: UIFont(name: "Open Sans", size: 17),
                selectColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                unselectColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                backgroundSelectColor: UIColor.clear
            ),
            bindSecButton: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor.white
            )
        )
    }
}

extension XCameraPreviewController {
    func errorAlertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
