//
//  XCameraEventController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraEventsController: XBaseViewController<XCameraEventView> {
    
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        popupColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        rightViews: [
            XNavigationViewRightViewBuilder(imageName: "ic_options", color: UIColor.colorFromHex(0x414042), groupViews: [
                XNavigationViewRightViewBuilder(title: "WIZ_REG_NOTIF_INF".localized(), font: UIFont(name: "Open Sans", size: 15.5), color: UIColor.colorFromHex(0x414042), viewTapped: self.infoViewTapped)
            ])
        ]
    )
    
    private var model: XCameraEventViewLayerModel
    
    private lazy var viewLayer: XCameraEventViewLayerProtocol = self.mainView
    
    private var noVideo: Bool = false
    
    init(model: XCameraEventViewLayerModel) {
        if model.record == nil || XTargetUtils.ivideon == nil { self.noVideo = true }
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        viewLayer.setEventInfo(model)
        if noVideo { mainView.hideVideo() }
    }
    
    private func infoViewTapped() {
        guard let url = URL(string: "https://cloud.security-hub.ru/wiki/doku.php?id=start") else { return }
        UIApplication.shared.open(url)
    }
}

extension XCameraEventsController: XCameraEventViewDelegate {
    func recordViewTapped() {
        if model.record == "clip/failed" || model.record == nil {
            showErrorColtroller(message: "IV_CLIP_FAILED".localized())
            return
        }

        let controller = XCameraEventRecordController(model: self.model)
        navigationController?.pushViewController(controller, animated: true)
    }
	
		func locationViewTapped() {
			let elc = XEventsLocationController()
			elc.location = model.location
			elc.eventTitle = "\(model.description), \(model.names.site), \(model.names.device)"
			navigationController?.present(elc, animated: true)
		}
}
