//
//  XCameraEventViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XCameraEventViewDelegate {
    func recordViewTapped()
		func locationViewTapped()
}
