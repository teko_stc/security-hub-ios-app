//
//  XCameraEventView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

protocol XCameraEventViewLayerProtocol {
    func setEventInfo(_ model: XCameraEventViewLayerModel)
}

class XCameraEventView: UIView {
    public var delegate: XCameraEventViewDelegate?
    
    private var cardView: UIView = UIView()
    private var topTitleView, topSubTitleView: UILabel!
    private var recordView: UIButton = UIButton()
		private var locationView: UIButton = UIButton()
    private var recordViewContext = 1
    private var lableViews: [UILabel] = []
    
    private lazy var style: Style = self.styles()
    private lazy var strings: XCameraEventViewStrings = XCameraEventViewStrings()
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func initViews() {
        backgroundColor = style.background
        
        cardView.backgroundColor = style.card
        cardView.layer.cornerRadius = 40
        addSubview(cardView)
        
        topTitleView = UILabel.create(style.topTitle)
        topTitleView.numberOfLines = 0
        addSubview(topTitleView)
        
        topSubTitleView = UILabel.create(style.topSubTitle)
        topSubTitleView.numberOfLines = 0
        addSubview(topSubTitleView)
        
        recordView.tintColor = style.recordIcon
        recordView.imageView?.contentMode = .scaleAspectFit
        recordView.contentVerticalAlignment = .fill
        recordView.contentHorizontalAlignment = .fill
        recordView.addObserver(self, forKeyPath: #keyPath(UIButton.isHighlighted), options: [.new], context: &recordViewContext)
        recordView.addTarget(self, action: #selector(recordViewTapped), for: .touchUpInside)
        addSubview(recordView)
			
				locationView.tintColor = style.recordIcon
				locationView.imageView?.contentMode = .scaleAspectFit
				locationView.contentVerticalAlignment = .fill
				locationView.contentHorizontalAlignment = .fill
				locationView.addTarget(self, action: #selector(locationViewTapped), for: .touchUpInside)
				addSubview(locationView)
        
        for i in 0...7 {
            let label = UILabel.create(i % 2 == 0 ? style.baseTitle : style.baseSubTitle)
            if i % 2 == 0 { label.text = [strings.zone, strings.section, strings.device, strings.zone][Int(i/2)] }
            label.numberOfLines = 0
            addSubview(label)
            lableViews.append(label)
        }
    }
    
    private func setConstraints() {
        [cardView, topTitleView, topSubTitleView, recordView, locationView].forEach{ $0.translatesAutoresizingMaskIntoConstraints = false }
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: -200),
            cardView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            cardView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
         
            topTitleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            topTitleView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 66),
            topTitleView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            topSubTitleView.topAnchor.constraint(equalTo: topTitleView.bottomAnchor, constant: 4),
            topSubTitleView.leadingAnchor.constraint(equalTo: topTitleView.leadingAnchor),
            topSubTitleView.trailingAnchor.constraint(equalTo: topTitleView.trailingAnchor),
            
            recordView.topAnchor.constraint(equalTo: topSubTitleView.bottomAnchor, constant: 10),
            recordView.trailingAnchor.constraint(equalTo: topTitleView.trailingAnchor),
            recordView.widthAnchor.constraint(equalToConstant: 44),
            recordView.heightAnchor.constraint(equalToConstant: 44),
						
						locationView.topAnchor.constraint(equalTo: topSubTitleView.bottomAnchor, constant: 10),
						locationView.trailingAnchor.constraint(equalTo: topTitleView.trailingAnchor),
						locationView.widthAnchor.constraint(equalToConstant: 44),
						locationView.heightAnchor.constraint(equalToConstant: 44),

            cardView.bottomAnchor.constraint(equalTo: recordView.bottomAnchor, constant: 20),
        ])
        for i in 0...7 {
            lableViews[i].translatesAutoresizingMaskIntoConstraints = false
            let top = i == 0 ? cardView.bottomAnchor : lableViews[i - 1].bottomAnchor
            let constant: CGFloat = i % 2 == 0 ? 20 : 4
            NSLayoutConstraint.activate([
                lableViews[i].topAnchor.constraint(equalTo: top, constant: constant),
                lableViews[i].leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
                lableViews[i].trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            ])
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &recordViewContext && keyPath == #keyPath(UIButton.isHighlighted), let isHighlighted = change?[.newKey] as? Bool {
            UIView.animate(withDuration: 0.2, animations: { self.recordView.transform = isHighlighted ? CGAffineTransform(scaleX: 1.2, y: 1.2) : .identity })
        }
    }
    
    @objc private func recordViewTapped() {
        delegate?.recordViewTapped()
    }
	
		@objc private func locationViewTapped() {
				delegate?.locationViewTapped()
		}
    
    public func hideVideo() {
        recordView.isHidden = true
    }
}

extension XCameraEventView: XCameraEventViewLayerProtocol {
    func setEventInfo(_ model: XCameraEventViewLayerModel) {
        topTitleView.text = model.description
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss EEEE, dd MMM"
				dateFormatter.locale = Locale(identifier: DataManager.defaultHelper.language)
        topSubTitleView.text = dateFormatter.string(from: Date(timeIntervalSince1970: Double(model.time)))
        lableViews[1].text = model.names.zone
        lableViews[3].text = model.names.section
        lableViews[5].text = model.names.device
        lableViews[7].text = model.names.site
        
        let ic_camera = model.record == "clip/failed" ? "ic_camera_failed_filled" : "ic_camera_filled"
        recordView.setImage(UIImage(named: ic_camera), for: .normal)
        recordView.isHidden = XTargetUtils.ivideon == nil || model.record == nil
			
				locationView.setImage(UIImage(named: "ic_location"), for: .normal)
				locationView.isHidden = model.location == nil
    }
}
