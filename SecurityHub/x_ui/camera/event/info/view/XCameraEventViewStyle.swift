//
//  XCameraEventViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraEventViewStyle {
    let background, card: UIColor
    let recordIcon: UIColor
    let topTitle, topSubTitle, baseTitle, baseSubTitle: XBaseLableStyle
}
extension XCameraEventView {
    typealias Style = XCameraEventViewStyle
}
