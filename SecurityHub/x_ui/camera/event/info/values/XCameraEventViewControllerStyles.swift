//
//  XCameraEventViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraEventView {
    func styles() -> Style {
        Style(
            background: UIColor(red: 0xec/255, green: 0xec/255, blue: 0xec/255, alpha: 1),
            card: UIColor.white,
            recordIcon: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            topTitle: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .left
            ),
            topSubTitle: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .left
            ),
            baseTitle: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 17),
                alignment: .left
            ),
            baseSubTitle: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .left
            )
        )
    }
}
