//
//  XCameraEventViewStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraEventViewStrings {
    /// Название датчика
    var zone: String { "N_BOTTOM_INFO_ZONE_NAME".localized() }
    
    /// Раздел
    var section: String { "N_WIZARD_ALL_SECTION".localized() }
    
    /// Контроллер
    var device: String { "N_ALARM_DEVICE".localized() }
    
    /// Объект
    var site: String { "N_ALARM_SITE".localized() }
}
