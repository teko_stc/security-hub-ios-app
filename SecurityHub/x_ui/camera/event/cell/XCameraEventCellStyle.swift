//
//  XCameraEventCellStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraEventCellStyle {
    let background, iconColor: UIColor
    let title, text, time: XBaseLableStyle
}
extension XCameraEventCell {
    typealias Style = XCameraEventCellStyle
}
