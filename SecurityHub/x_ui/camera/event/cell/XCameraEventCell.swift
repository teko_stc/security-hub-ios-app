//
//  XCameraEventCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 05.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraEventCell: UITableViewCell {
    static var identifier = "XCameraEventCell.identifier"
    
    private var model: XCameraEventViewLayerModel?
    private var onViewTapped: ((XCameraEventViewLayerModel) -> ())? = nil
    private var onRecordTapped: ((XCameraEventViewLayerModel) -> ())? = nil
    
    private var baseView: UIView = UIView()
    private var iconView: UIImageView = UIImageView()
    private var titleView: UILabel = UILabel()
    private var textView: UILabel = UILabel()
    private var timeView: UILabel = UILabel()
    private var recordButtonView: UIButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(model: XCameraEventViewLayerModel) {
        self.model = model
        iconView.image = UIImage(named: model.icon)
        titleView.text = model.description
        textView.text = "\(model.names.site)\n\(model.names.zone) · \(model.names.section)"
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM\nHH:mm"
        timeView.text = dateFormatterGet.string(from: Date(timeIntervalSince1970: Double(model.time)))
        
        let ic_camera = (model.record == "clip/failed" || model.record == nil) ? "ic_camera_failed_filled" : "ic_camera_filled"
        recordButtonView.setImage(UIImage(named: ic_camera), for: .normal)
    }
    
    public func setDelegate(onViewTapped: @escaping (XCameraEventViewLayerModel) -> (), onRecordTapped: @escaping (XCameraEventViewLayerModel) -> ()) {
        self.onViewTapped = onViewTapped
        self.onRecordTapped = onRecordTapped
    }
    
    public func setStyle(_ style: Style) {
        backgroundColor = style.background
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = style.background
        
        iconView.tintColor = style.iconColor
        
        titleView.font = style.title.font
        titleView.textColor = style.title.color
        titleView.textAlignment = style.title.alignment
        titleView.numberOfLines = 2

        textView.font = style.text.font
        textView.textColor = style.text.color
        textView.textAlignment = style.text.alignment
        textView.numberOfLines = 2
        
        timeView.font = style.time.font
        timeView.textColor = style.time.color
        timeView.textAlignment = style.time.alignment
        timeView.numberOfLines = 0
    }
    
    private func initViews() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tabGesture))
        let long = UILongPressGestureRecognizer(target: self, action: #selector(longGesture))
        baseView.isUserInteractionEnabled = true
        baseView.addGestureRecognizer(tap)
        baseView.transform = CGAffineTransform.identity
        baseView.addGestureRecognizer(long)
        contentView.addSubview(baseView)
        
        baseView.addSubview(iconView)
        baseView.addSubview(titleView)
        baseView.addSubview(textView)
        baseView.addSubview(timeView)
        
        recordButtonView.imageView?.contentMode = .scaleAspectFit
        recordButtonView.contentVerticalAlignment = .fill
        recordButtonView.contentHorizontalAlignment = .fill
        recordButtonView.addTarget(self, action: #selector(recordButtonViewTapped), for: .touchUpInside)
        baseView.addSubview(recordButtonView)
        
        baseView.translatesAutoresizingMaskIntoConstraints = false
        iconView.translatesAutoresizingMaskIntoConstraints = false
        titleView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        timeView.translatesAutoresizingMaskIntoConstraints = false
        recordButtonView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            baseView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            baseView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            baseView.topAnchor.constraint(equalTo: contentView.topAnchor),
            baseView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            baseView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            iconView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 20),
            iconView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 20),
            iconView.heightAnchor.constraint(equalToConstant: 40),
            iconView.widthAnchor.constraint(equalToConstant: 40),
            
            titleView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 10),
            titleView.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 14),
            
            textView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 2),
            textView.leadingAnchor.constraint(equalTo: titleView.leadingAnchor),
            textView.bottomAnchor.constraint(lessThanOrEqualTo: baseView.bottomAnchor, constant: -10),
            
            timeView.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 4),
            timeView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 20),
            timeView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -20),
            timeView.widthAnchor.constraint(equalToConstant: 42),
            
            recordButtonView.topAnchor.constraint(equalTo: timeView.bottomAnchor),
            recordButtonView.leadingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: 22),
            recordButtonView.trailingAnchor.constraint(equalTo: timeView.trailingAnchor),
            recordButtonView.bottomAnchor.constraint(lessThanOrEqualTo: baseView.bottomAnchor, constant: -4),
            recordButtonView.heightAnchor.constraint(equalToConstant: 44),
        ])
    }
    
    @objc private func tabGesture(sender: UIGestureRecognizer) {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
            UIView.addKeyframe(withRelativeStartTime: 1/2, relativeDuration: 1/2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            })
        }) { _ in
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
            if let model = self.model { self.onViewTapped?(model) }
        }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .began) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        } else if (sender.state == .ended) {
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                if let model = self.model { self.onViewTapped?(model) }
            })
        }
    }
    
    @objc private func recordButtonViewTapped() {
        if let model = self.model { self.onRecordTapped?(model) }
    }
}
