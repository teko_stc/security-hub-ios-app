//
//  XCameraEventRecordController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraEventRecordController: XBaseViewController<XCameraEventRecordView> {
    override var tabBarIsHidden: Bool { true }
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_close", color: UIColor.colorFromHex(0x414042), viewTapped: self.back),
        titleView: XBaseLableStyle(color: UIColor.colorFromHex(0x414042), font: UIFont(name: "Open Sans", size: 25))
    )
    
    private var model: XCameraEventViewLayerModel
    
    private lazy var viewLayer: XCameraEventViewLayerProtocol = self.mainView
    private lazy var strings: XCameraEventRecordControllerStrings = XCameraEventRecordControllerStrings()
    
    init(model: XCameraEventViewLayerModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strings.title
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        viewLayer.setEventInfo(model)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainView.pauseStream()
    }
}
 
extension XCameraEventRecordController: XCameraEventRecordViewDelegate {
    func copyToClipboardViewTapped() {
        UIPasteboard.general.string = model.record
        let controller = XAlertController(style: alertStyle(), strings: strings.copyAlertStrings)
        navigationController?.present(controller, animated: true)
    }
    
    func shareRecordViewTapped() {
        let textToShare = [ model.record ]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
              
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func navigationStateNeedChange(isHidden: Bool) {
        navigationController?.setNavigationBarHidden(isHidden, animated: true)
    }
}
