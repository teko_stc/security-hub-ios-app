//
//  XCameraEventRecordViewControlerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCameraEventRecordView {
    func styles() -> Style {
        Style(
            background: UIColor.white,
            loaderColor: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
            errorColor: UIColor.red,
            iconColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
            timeLable: XBaseLableStyle(
                color: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            ),
            titleLable: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 20),
                alignment: .left
            ),
            subTitleLable: XBaseLableStyle(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 17),
                alignment: .left
            )
        )
    }
}


extension XCameraEventRecordController {
    func alertStyle() -> XAlertView.Style {
        return XAlertView.Style(
            backgroundColor: UIColor.white,
            title: XAlertView.Style.Lable(
                font: UIFont(name: "Open Sans", size: 20),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            text: nil,
            buttonOrientation: .vertical,
            positive: XZoomButton.Style (
                backgroundColor: UIColor.clear,
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            ),
            negative: nil
        )
    }
}
