//
//  XCameraEventRecordViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCameraEventRecordControllerStrings {
    /// Просмотр записи
    var title: String { "CAMERA_TITLE_RECORD_VIEW".localized() }
    
    var copyAlertStrings: XAlertViewStrings {
        XAlertViewStrings(
            /// Скопировано в буфер обмена
            title: "COPIED_IN_BUFFER".localized(),
            text: nil,
            /// OK
            positive: "OK".localized(),
            negative: nil
        )
    }
}
