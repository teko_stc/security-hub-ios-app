//
//  XCameraEventRecordViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation
import AVKit

protocol XCameraEventRecordViewDelegate {
    func copyToClipboardViewTapped()
    func shareRecordViewTapped()
    func navigationStateNeedChange(isHidden: Bool) 
}
