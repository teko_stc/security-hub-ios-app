//
//  XCameraEventRecordView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import AVKit

class XCameraEventRecordView: UIView {
    public var delegate: XCameraEventRecordViewDelegate?
    
    private var playerView: PlayerView = PlayerView()
    private var playerLoaderView: XLoaderView = XLoaderView()
    private var playerErrorView: UIImageView = UIImageView(image: UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate))
    private var playerBottomSheetView: UIView = UIView()
    private var playerPlayPlauseButtonView: UIButton = UIButton()
    private var playerFullButtonView: UIButton = UIButton()
    private var eventIconView: UIImageView = UIImageView()
    private var eventTimeView, eventTitleView, eventSubTitleView: UILabel!
    private var copyButtonView: UIButton = UIButton(type: .infoLight)
    private var shareButtonView: UIButton = UIButton(type: .infoLight)
    
    private var playerViewHeightAnchor, playerViewWidthAnchor, playerViewBottomAnchor, playerViewCenterYAnchor: NSLayoutConstraint!

    private var PlayerItemObserverContext = 1
    private lazy var style: Style = self.styles()
    
    init() {
        super.init(frame: .zero)
        initViews()
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
   
    private func initViews() {
        backgroundColor = style.background
            
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        playerView.isUserInteractionEnabled = true
        playerView.addGestureRecognizer(tap)
        let long = UITapGestureRecognizer(target: self, action: #selector(longGesture))
        playerView.addGestureRecognizer(long)
        playerView.player = AVPlayer()
        playerView.playerLayer.backgroundColor = UIColor.black.cgColor
        addSubview(playerView)
        
        playerLoaderView.color = style.loaderColor
        playerLoaderView.isHidden = true
        addSubview(playerLoaderView)
        
        playerErrorView.tintColor = style.errorColor
        playerErrorView.isHidden = true
        addSubview(playerErrorView)
        
        playerBottomSheetView.backgroundColor = UIColor.black
        playerBottomSheetView.alpha = 0.7
        playerBottomSheetView.isHidden = true
        playerBottomSheetView.alpha = 0
        playerView.addSubview(playerBottomSheetView)
        
        playerPlayPlauseButtonView.setImage(UIImage(named: "ic_pause_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerPlayPlauseButtonView.setImage(UIImage(named: "ic_play_button")?.withRenderingMode(.alwaysTemplate), for: .selected)
        playerPlayPlauseButtonView.tintColor = style.iconColor
        playerPlayPlauseButtonView.imageView?.contentMode = .scaleAspectFill
        playerPlayPlauseButtonView.contentVerticalAlignment = .fill
        playerPlayPlauseButtonView.contentHorizontalAlignment = .fill
        playerPlayPlauseButtonView.addTarget(self, action: #selector(playerPlayPlauseButtonViewTapped), for: .touchUpInside)
        playerBottomSheetView.addSubview(playerPlayPlauseButtonView)
        
        playerFullButtonView.setImage(UIImage(named: "ic_full_screen_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerFullButtonView.setImage(UIImage(named: "ic_full_screen_exit")?.withRenderingMode(.alwaysTemplate), for: .normal)
        playerFullButtonView.tintColor = style.iconColor
        playerFullButtonView.imageView?.contentMode = .scaleAspectFill
        playerFullButtonView.contentVerticalAlignment = .fill
        playerFullButtonView.contentHorizontalAlignment = .fill
        playerFullButtonView.addTarget(self, action: #selector(playerFullButtonViewTapped), for: .touchUpInside)
        playerBottomSheetView.addSubview(playerFullButtonView)
        
        eventTimeView = UILabel.create(style.timeLable)
        addSubview(eventTimeView)
        
        eventIconView.tintColor = style.iconColor
        eventIconView.contentMode = .scaleAspectFit
        addSubview(eventIconView)
        
        eventTitleView = UILabel.create(style.titleLable)
        eventTitleView.numberOfLines = 0
        addSubview(eventTitleView)
        
        eventSubTitleView = UILabel.create(style.subTitleLable)
        eventSubTitleView.numberOfLines = 0
        addSubview(eventSubTitleView)
        
        copyButtonView.setImage(UIImage(named: "ic_copy_2"), for: .normal)
        copyButtonView.tintColor = style.iconColor
        copyButtonView.imageView?.contentMode = .scaleAspectFill
        copyButtonView.contentVerticalAlignment = .fill
        copyButtonView.contentHorizontalAlignment = .fill
        copyButtonView.addTarget(self, action: #selector(copyButtonViewTapped), for: .touchUpInside)
        addSubview(copyButtonView)
        
        shareButtonView.setImage(UIImage(named: "ic_share_2"), for: .normal)
        shareButtonView.tintColor = style.iconColor
        shareButtonView.imageView?.contentMode = .scaleAspectFill
        shareButtonView.contentVerticalAlignment = .fill
        shareButtonView.contentHorizontalAlignment = .fill
        shareButtonView.addTarget(self, action: #selector(shareButtonViewTapped), for: .touchUpInside)
        addSubview(shareButtonView)
        
        bringSubviewToFront(playerView)
    }
    
    private func setConstraints() {
        playerViewHeightAnchor = playerView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.57)
        playerViewWidthAnchor = playerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        playerViewBottomAnchor = playerView.bottomAnchor.constraint(equalTo: centerYAnchor)
        playerViewCenterYAnchor = playerView.centerYAnchor.constraint(equalTo: centerYAnchor)
        [playerView, playerLoaderView, playerErrorView, playerBottomSheetView, playerPlayPlauseButtonView, playerFullButtonView, eventTimeView, eventIconView, eventTitleView, eventSubTitleView, copyButtonView, shareButtonView].forEach({ view in view?.translatesAutoresizingMaskIntoConstraints = false })
        NSLayoutConstraint.activate([
            playerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            playerViewBottomAnchor,
            playerViewHeightAnchor!,
            playerViewWidthAnchor!,
            
            playerLoaderView.centerYAnchor.constraint(equalTo: playerView.centerYAnchor),
            playerLoaderView.centerXAnchor.constraint(equalTo: playerView.centerXAnchor),
            playerLoaderView.heightAnchor.constraint(equalToConstant: 40),
            playerLoaderView.widthAnchor.constraint(equalToConstant: 40),
            
            playerErrorView.centerYAnchor.constraint(equalTo: playerLoaderView.centerYAnchor),
            playerErrorView.centerXAnchor.constraint(equalTo: playerLoaderView.centerXAnchor),
            playerErrorView.heightAnchor.constraint(equalToConstant: 30),
            playerErrorView.widthAnchor.constraint(equalToConstant: 30),
            
            playerBottomSheetView.bottomAnchor.constraint(equalTo: playerView.bottomAnchor),
            playerBottomSheetView.heightAnchor.constraint(equalToConstant: 60),
            playerBottomSheetView.widthAnchor.constraint(equalTo: playerView.widthAnchor),
            playerBottomSheetView.centerXAnchor.constraint(equalTo: playerView.centerXAnchor),
            
            playerPlayPlauseButtonView.centerYAnchor.constraint(equalTo: playerBottomSheetView.centerYAnchor),
            playerPlayPlauseButtonView.centerXAnchor.constraint(equalTo: playerBottomSheetView.centerXAnchor),
            playerPlayPlauseButtonView.heightAnchor.constraint(equalTo: playerBottomSheetView.heightAnchor, multiplier: 0.4),
            playerPlayPlauseButtonView.widthAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
            
            playerFullButtonView.centerYAnchor.constraint(equalTo: playerBottomSheetView.centerYAnchor),
            playerFullButtonView.trailingAnchor.constraint(equalTo: playerBottomSheetView.trailingAnchor, constant: -20),
            playerFullButtonView.heightAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
            playerFullButtonView.widthAnchor.constraint(equalTo: playerPlayPlauseButtonView.heightAnchor),
            
            eventTimeView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor, constant: 40),
            eventTimeView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            eventTimeView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            
            eventIconView.topAnchor.constraint(equalTo: eventTimeView.bottomAnchor, constant: 10),
            eventIconView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 20),
            eventIconView.heightAnchor.constraint(equalToConstant: 80),
            eventIconView.widthAnchor.constraint(equalToConstant: 80),

            eventTitleView.topAnchor.constraint(equalTo: eventTimeView.bottomAnchor, constant: 10),
            eventTitleView.leadingAnchor.constraint(equalTo: eventIconView.trailingAnchor, constant: 10),

            eventSubTitleView.topAnchor.constraint(equalTo: eventTitleView.bottomAnchor, constant: 4),
            eventSubTitleView.leadingAnchor.constraint(equalTo: eventTitleView.leadingAnchor),
            
            copyButtonView.centerYAnchor.constraint(equalTo: eventTitleView.centerYAnchor, constant: 10),
            copyButtonView.leadingAnchor.constraint(equalTo: eventTitleView.trailingAnchor, constant: 10),
            copyButtonView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            copyButtonView.heightAnchor.constraint(equalToConstant: 30),
            copyButtonView.widthAnchor.constraint(equalToConstant: 26),

            shareButtonView.centerYAnchor.constraint(equalTo: eventSubTitleView.centerYAnchor, constant: 10),
            shareButtonView.leadingAnchor.constraint(equalTo: eventSubTitleView.trailingAnchor, constant: 10),
            shareButtonView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -20),
            shareButtonView.heightAnchor.constraint(equalToConstant: 26),
            shareButtonView.widthAnchor.constraint(equalToConstant: 26),
        ])
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &PlayerItemObserverContext && keyPath == #keyPath(AVPlayerItem.status), let status = change?[NSKeyValueChangeKey.newKey] as? NSNumber  {
             switch (status.intValue as AVPlayer.Status.RawValue) {
             case AVPlayer.Status.readyToPlay.rawValue: return playerLoaderView.isHidden = true
             case AVPlayer.Status.failed.rawValue:      return playerErrorView.isHidden = false
             default: return
             }
        } else if context == &PlayerItemObserverContext && keyPath == #keyPath(AVPlayer.rate), let rate = change?[NSKeyValueChangeKey.newKey] as? Float  {
            if (rate == 0.0) { playerView.player?.seek(to: .zero) }
            self.playerPlayPlauseButtonView.isSelected = rate == 0.0
        }
    }
    
    @objc private func tapGesture(sender: UIGestureRecognizer) {
        if (playerBottomSheetView.isHidden) { playerBottomSheetView.isHidden = false; UIView.animate(withDuration: 0.3, animations: { self.playerBottomSheetView.alpha = 1 }) }
        else { UIView.animate(withDuration: 0.3, animations: { self.playerBottomSheetView.alpha = 0 }, completion: { _ in self.playerBottomSheetView.isHidden = true; }) }
    }
    
    @objc private func longGesture(sender: UIGestureRecognizer) {
        if (sender.state == .ended) { tapGesture(sender: sender) }
    }
    
    @objc private func playerPlayPlauseButtonViewTapped() {
        if (playerView.player?.rate == 1.0) { playerView.player?.pause() }
        else { playerView.player?.play(); }
    }
    
    private var isFull: Bool = false
    @objc private func playerFullButtonViewTapped() {
        self.isFull = !self.isFull
        playerFullButtonView.isSelected = !playerFullButtonView.isSelected
        playerViewHeightAnchor.constant = self.isFull ? UIScreen.main.bounds.width : (UIScreen.main.bounds.width * 0.57)
        playerViewWidthAnchor.constant = self.isFull ? frame.size.height : UIScreen.main.bounds.width
        playerViewBottomAnchor.isActive = !isFull
        playerViewCenterYAnchor.isActive = isFull
//        delegate?.navigationStateNeedChange(isHidden: isFull)
        UIView.animate(withDuration: 0.5, animations: {
            self.playerView.transform = self.isFull ? CGAffineTransform(rotationAngle: 90 * .pi/180) : CGAffineTransform.identity
            self.layoutIfNeeded()
        })
    }
    
    @objc private func copyButtonViewTapped() {
        delegate?.copyToClipboardViewTapped()
    }
    
    @objc private func shareButtonViewTapped() {
        delegate?.shareRecordViewTapped()
    }
    
    public func pauseStream() {
        if (playerView.player?.rate == 1.0) {
            playerView.player?.pause()
        }
    }
}

extension XCameraEventRecordView: XCameraEventViewLayerProtocol {
    func setEventInfo(_ model: XCameraEventViewLayerModel) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM / HH:mm:ss "
        eventTimeView.text = dateFormatter.string(from: Date(timeIntervalSince1970: Double(model.time)))
        
        eventIconView.image = UIImage(named: model.icon)?.withRenderingMode(.alwaysTemplate)
        eventTitleView.text = model.description
        eventSubTitleView.text = "\(model.names.site)\n\(model.names.section) \(model.names.zone)"
        
        guard let url = model.record else { return playerErrorView.isHidden = false }
        playerLoaderView.isHidden = false
        
        let _ = DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
            .subscribe(onSuccess: { [weak self] token in
                guard let token = token else { return }
                
                self?.setAVP(url: url, token: token)
            })
    }
    
    private func setAVP(url: String, token: String) {
        guard let tempUrl = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
              let id = XTargetUtils.ivideonId,
              let resultUrl = URL(string: "https://openapi-alpha-eu01.ivideon.com/media_storages/ivideon/event_clip?url=\(tempUrl)&access_token=\(token)&partner_id=ivideon&app_id=\(id)") else {
            return
        }
        
        print(resultUrl)
        
        let headers: [String: String] = [
           "Authorization": "Bearer \(token)"
        ]
        let asset = AVURLAsset(url: resultUrl, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
        let playerItem = AVPlayerItem(asset: asset)
        playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.new], context: &PlayerItemObserverContext)
        playerView.player?.addObserver(self, forKeyPath: #keyPath(AVPlayer.rate), options: [.new], context: &PlayerItemObserverContext)
        playerView.player?.replaceCurrentItem(with: playerItem);
        
        playerView.player?.play()
    }
}
