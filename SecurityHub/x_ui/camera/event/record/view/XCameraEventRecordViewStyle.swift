//
//  XCameraEventRecordViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 06.08.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCameraEventRecordViewStyle {
    let background, loaderColor, errorColor: UIColor
    let iconColor: UIColor
    let timeLable, titleLable, subTitleLable: XBaseLableStyle
}
extension XCameraEventRecordView {
    typealias Style = XCameraEventRecordViewStyle
}
