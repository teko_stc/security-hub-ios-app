//
//  XCamerasMainViewControllerStyles.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 23.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

extension XCamerasMainView {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            addTitle: Style.Lable(
                color: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 17.5)
            ),
            addButton: XZoomButton.Style(
                backgroundColor: UIColor(red: 0x3a/255, green: 0xbe/255, blue: 0xff/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                color: UIColor.white,
                disable: UIColor.white,
                disableBackground: UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
            )
        )
    }
}

extension XCamerasMainViewCell {
    func style() -> Style {
        return Style(
            backgroundColor: UIColor.white,
            selectedBackgroundColor: UIColor(red: 0xd6/255, green: 0xd6/255, blue: 0xd6/255, alpha: 1),
            preview: Style.Preview(
                defaultImageName: "ic_user"
            ),
            name: Style.Label(
                color: UIColor.black,
                font: UIFont(name: "Open Sans", size: 15.5)
            ),
            asignState: Style.Label(
                color: UIColor(red: 0xbc/255, green: 0xbc/255, blue:  0xbc/255, alpha: 0.8),
                font: UIFont(name: "Open Sans", size: 12.3)
            )
        )
    }
}

extension XCamerasMainViewHeader {
    func style() -> Style {
        return Style(
            type: XBaseLableStyle(
                color: UIColor(red: 0x44/255, green: 0x40/255, blue:  0x42/255, alpha: 1),
                font: UIFont(name: "Open Sans", size: 15.5),
                alignment: .center
            )
        )
    }
}
