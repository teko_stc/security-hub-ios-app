//
//  XCamerasMainViewControllerStrings.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 24.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

class XCamerasMainViewStrings {
    /// You haven't added any cameras to your Security Hub account yet.
    var addTitle: String { "N_CAMERAS_NO_CAMERAS".localized() }
    
    /// Add
    var addButton: String { "N_MAINSET_ADD".localized() }
    
    var sectionIvideonTitle: String { "Ivideon" }
    
    /// IP-камеры
    var sectionRtspTitle: String { "N_CAMERAS_RTSP_TITLE".localized() }
}
