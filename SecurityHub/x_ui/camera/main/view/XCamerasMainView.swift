//
//  XCamerasMainView.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 23.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

class XCameraViewLayerMapper {
    static func cast(model: XRTSPCameraDataLayerRxModel) -> XCameraViewLayerModel {
        return XCameraViewLayerModel(type: .rtsp, id: String(model.id), name: model.name, url: model.url)
    }
    static func cast(model: Camera, accessToken: String) -> XCameraViewLayerModel {
        return XCameraViewLayerModel(type: .ivideon, id: model.id, name: model.name, url: model.getPreviewUrl(accessToken: accessToken))
    }
}

enum XCameraViewLayerModelType: Int {
    case ivideon = 0
    case rtsp = 1
}

struct XCameraViewLayerModel {
    let type: XCameraViewLayerModelType
    let id: String
    var name: String
    var url: String
}

protocol XRTSPCameraViewLayerProtocol {
    func rtspCamera(content: XCameraViewLayerModel, state: XRxModelState)
}

protocol XIvideonCameraViewLayerProtocol {
    func ivideonCameras(content: [XCameraViewLayerModel])
}

class XCamerasMainView: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    public var delegate: XCamerasMainViewDelegate?
    private let addTitleView: UILabel = UILabel()
    private var loaderView: XLoaderView!
    private var addButtonView: XZoomButton!
    private var collectionView: UICollectionView!
    private let strings: XCamerasMainViewStrings = XCamerasMainViewStrings()
    private var cameraItems: [XCameraViewLayerModelType.RawValue: [XCameraViewLayerModel]] = [:]
    
    init() {
        super.init(frame: .zero)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
        
    private func initViews(style: Style) {
        backgroundColor = style.backgroundColor
        
        addTitleView.text = strings.addTitle
        addTitleView.font = style.addTitle.font
        addTitleView.textColor = style.addTitle.color
        addTitleView.textAlignment = .center
        addTitleView.numberOfLines = 0
        addSubview(addTitleView)
        
        addButtonView = XZoomButton(style: style.addButton)
        addButtonView.text = strings.addButton
        addButtonView.alpha = 0
        addButtonView.isHidden = true
        addButtonView.addTarget(self, action: #selector(addViewTapped), for: .touchUpInside)
        addSubview(addButtonView)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width / 2, height: 180)
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = style.backgroundColor
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(Header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier)
        collectionView.register(Cell.self, forCellWithReuseIdentifier: Cell.identifier)
        addSubview(collectionView)
        
        loaderView = XLoaderView()
        loaderView.color = UIColor(red: 0x41/255, green: 0x40/255, blue: 0x42/255, alpha: 1)
        addSubview(loaderView)
    }
    
    private func setConstraints() {
        addTitleView.translatesAutoresizingMaskIntoConstraints = false
        addButtonView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addTitleView.bottomAnchor.constraint(equalTo: addButtonView.topAnchor, constant: -40),
            addTitleView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            addTitleView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.8),

            loaderView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor),
            loaderView.heightAnchor.constraint(equalToConstant: 40),
            loaderView.widthAnchor.constraint(equalToConstant: 40),
            
            addButtonView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            addButtonView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            addButtonView.heightAnchor.constraint(equalToConstant: 40),
            addButtonView.widthAnchor.constraint(equalTo: safeAreaLayoutGuide.widthAnchor, multiplier: 0.8),
            
            collectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func checkAddView() {
        loaderView.isHidden = true
        UIView.animate(withDuration: 0.4, animations: {
            self.addTitleView.alpha = (self.cameraItems.count == 0) ? 1 : 0
            self.addButtonView.alpha = (self.cameraItems.count == 0) ? 1 : 0
            self.collectionView.alpha = !(self.cameraItems.count == 0) ? 1 : 0
        }, completion: { _ in
            self.addTitleView.isHidden = !(self.cameraItems.count == 0)
            self.addButtonView.isHidden = !(self.cameraItems.count == 0)
            self.collectionView.isHidden = (self.cameraItems.count == 0)
        })
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { return cameraItems.count }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Header.identifier, for: indexPath) as! Header
        let section = (cameraItems[XCameraViewLayerModelType.ivideon.rawValue]?.count ?? 0 == 0) ? XCameraViewLayerModelType.rtsp.rawValue : indexPath.section
        if (section == XCameraViewLayerModelType.ivideon.rawValue) { headerView.setContent(type: strings.sectionIvideonTitle) }
        if (section == XCameraViewLayerModelType.rtsp.rawValue) { headerView.setContent(type: strings.sectionRtspTitle) }
        return headerView
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = (cameraItems[XCameraViewLayerModelType.ivideon.rawValue]?.count ?? 0 == 0) ? XCameraViewLayerModelType.rtsp.rawValue : section
        return cameraItems[section]?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath) as! Cell
        let section = (cameraItems[XCameraViewLayerModelType.ivideon.rawValue]?.count ?? 0 == 0) ? XCameraViewLayerModelType.rtsp.rawValue : indexPath.section
        cell.setContent(cameraItems[section]![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = (cameraItems[XCameraViewLayerModelType.ivideon.rawValue]?.count ?? 0 == 0) ? XCameraViewLayerModelType.rtsp.rawValue : indexPath.section
        delegate?.cameraItemViewTapped(item: cameraItems[section]![indexPath.row])
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    @objc private func addViewTapped() {
        delegate?.showAddViewTapped()
    }
}

extension XCamerasMainView: XRTSPCameraViewLayerProtocol {
    func rtspCamera(content: XCameraViewLayerModel, state: XRxModelState) {
        if (cameraItems[XCameraViewLayerModelType.rtsp.rawValue] == nil) { cameraItems[XCameraViewLayerModelType.rtsp.rawValue] = [] }
        switch state {
        case .insert:
            cameraItems[XCameraViewLayerModelType.rtsp.rawValue]!.append(content)
            break;
        case .update:
            if let index = cameraItems[XCameraViewLayerModelType.rtsp.rawValue]!.firstIndex(where: { $0.id == content.id }) {
                cameraItems[XCameraViewLayerModelType.rtsp.rawValue]![index] = content
            }
            break;
        case .delete:
            if let index = cameraItems[XCameraViewLayerModelType.rtsp.rawValue]!.firstIndex(where: { $0.id == content.id }) {
                cameraItems[XCameraViewLayerModelType.rtsp.rawValue]!.remove(at: index)
            }
            break;
        }
        if (cameraItems[XCameraViewLayerModelType.rtsp.rawValue]?.count ?? 0 == 0) { cameraItems.removeValue(forKey: XCameraViewLayerModelType.rtsp.rawValue) }
        collectionView.reloadData()
        checkAddView()
    }
}

extension XCamerasMainView: XIvideonCameraViewLayerProtocol {
    func ivideonCameras(content: [XCameraViewLayerModel]) {
        cameraItems[XCameraViewLayerModelType.ivideon.rawValue] = content
        if (cameraItems[XCameraViewLayerModelType.ivideon.rawValue]?.count ?? 0 == 0) { cameraItems.removeValue(forKey: XCameraViewLayerModelType.ivideon.rawValue) }
        collectionView.reloadData()
        checkAddView()
    }
}
