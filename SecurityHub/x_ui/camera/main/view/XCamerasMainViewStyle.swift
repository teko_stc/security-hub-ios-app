//
//  XCamerasMainViewStyle.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 24.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCamerasMainViewStyle {
    struct Lable {
        let color: UIColor
        let font: UIFont?
    }
    
    let backgroundColor: UIColor
    let addTitle: Lable
    let addButton: XZoomButton.Style
}
extension XCamerasMainView {
    typealias Style = XCamerasMainViewStyle
}

