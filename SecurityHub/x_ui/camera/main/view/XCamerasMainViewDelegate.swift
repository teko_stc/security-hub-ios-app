//
//  XCamerasMainViewDelegate.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 30.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import Foundation

protocol XCamerasMainViewDelegate {
    func cameraItemViewTapped(item: XCameraViewLayerModel)
    func showAddViewTapped() 
}
