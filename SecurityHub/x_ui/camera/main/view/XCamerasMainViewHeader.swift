//
//  XCamerasMainViewHeader.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 29.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCamerasMainViewHeaderStyle {
    let type: XBaseLableStyle
}
extension XCamerasMainViewHeader {
    typealias Style = XCamerasMainViewHeaderStyle
}

class XCamerasMainViewHeader: UICollectionReusableView {
    static let identifier = "XCamerasMainViewHeader.identifier"

    private let typeView: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        let style = style()
        typeView.textColor = style.type.color
        typeView.font = style.type.font
        typeView.textAlignment = .left
        addSubview(typeView)
        typeView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            typeView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            typeView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 120),
            typeView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            typeView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
    }
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    public func setContent(type: String) {
        typeView.text = type
    }
}
extension XCamerasMainView {
    typealias Header = XCamerasMainViewHeader
}

