//
//  XCamerasMainViewCell.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 24.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit

struct XCamerasMainViewCellStyle {
    struct Label {
        let color: UIColor
        let font: UIFont?
    }
    
    struct Preview {
        let defaultImageName: String
    }
    
    let backgroundColor: UIColor
    let selectedBackgroundColor: UIColor
    let preview: Preview
    let name, asignState: Label
}
extension XCamerasMainViewCell {
    typealias Style = XCamerasMainViewCellStyle
}

class XCamerasMainViewCell: UICollectionViewCell {
    static let identifier = "XCamerasMainViewCell.identifier"
    private let preView: UIImageView = UIImageView()
    private let nameView: UILabel = UILabel()
    private let asignStateView: UILabel = UILabel()
    private let backView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews(style: style())
        setConstraints()
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setContent(_ content: XCameraViewLayerModel) {
        nameView.text = content.name
        switch content.type {
        case .ivideon:
            asignStateView.isHidden = false
            do {
                if let url = URL(string: content.url) {
                    let data = try Data(contentsOf: url)
                    preView.image = UIImage(data: data)
                }
            } catch { preView.image = #imageLiteral(resourceName: "camera_btn") }
            //TODO
            asignStateView.text = "Is Assign to device"
            break;
        case .rtsp:
            preView.image = #imageLiteral(resourceName: "camera_btn") 
            asignStateView.isHidden = true
            break;
        }
        
    }
    
    private func initViews(style: Style) {
        backgroundColor = style.backgroundColor
        selectedBackgroundView = backView
        backView.backgroundColor = style.selectedBackgroundColor
        
        preView.image = UIImage(named: style.preview.defaultImageName)
        preView.contentMode = .scaleAspectFit
        addSubview(preView)

        nameView.textColor = style.name.color
        nameView.font = style.name.font
        nameView.textAlignment = .center
        addSubview(nameView)
        
        asignStateView.textColor = style.asignState.color
        asignStateView.font = style.asignState.font
        asignStateView.textAlignment = .center
        addSubview(asignStateView)
    }
    
    private func setConstraints() {
        preView.translatesAutoresizingMaskIntoConstraints = false
        nameView.translatesAutoresizingMaskIntoConstraints = false
        asignStateView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            preView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            preView.leadingAnchor.constraint(equalTo: leadingAnchor),
            preView.trailingAnchor.constraint(equalTo: trailingAnchor),
            preView.heightAnchor.constraint(equalToConstant: 100),
            
            nameView.topAnchor.constraint(equalTo: preView.bottomAnchor, constant: 10),
            nameView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            nameView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            asignStateView.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 0),
            asignStateView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            asignStateView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            asignStateView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
    }
}
extension  XCamerasMainView {
    typealias Cell = XCamerasMainViewCell
}
