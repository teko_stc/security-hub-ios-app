//
//  XCamerasMainController.swift
//  SecurityHub
//
//  Created by Тимерлан Рахматуллин on 23.07.2021.
//  Copyright © 2021 TEKO. All rights reserved.
//

import UIKit
import RxSwift

class XCamerasMainController: XBaseViewController<XCamerasMainView> {
    
    private lazy var navigationViewBuilder = XNavigationViewBuilder(
        backgroundColor: .white,
        leftView: XNavigationViewLeftViewBuilder(imageName: "ic_add", color: UIColor.colorFromHex(0x414042), viewTapped: self.showAddMenuController)
    )
    
    private lazy var ivideonCameraViewLayer: XIvideonCameraViewLayerProtocol = self.mainView

    private lazy var rtspCameraViewLayer: XRTSPCameraViewLayerProtocol = self.mainView
    private lazy var rtspCameraDataLayer: XRTSPCameraDataLayerProtocol = DataManager.shared.dbHelper

    private var ivideonCamerasDisposable: Disposable?
    private var rtspCamerasDisposable: Disposable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationViewBuilder(navigationViewBuilder)
        mainView.delegate = self
        initObservers()
        //TODO Refresh List
        //TODO NO Internet connection
    }
        
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            ivideonCamerasDisposable?.dispose()
            rtspCamerasDisposable?.dispose()
        }
    }
    
    private func ivideonCamerasSingle() -> Single<[XCameraViewLayerModel]> {
        let singleIV_GET_TOKEN: Single<String?> = DataManager.shared.hubHelper.get(.IV_GET_TOKEN, D: ["domain": DataManager.shared.getUser().domainId])
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .map({ (result: DCommandResult, value: HubIvideonToken?) in return value?.access_token })
        let singleIV_GET_CAMERAS: Single<(String, [Camera])> = singleIV_GET_TOKEN
            .flatMap({ token in
                guard let token = token else {
                    return Single.zip(Single.just(""), Single.just([]))
                }
                
                return Single.zip(Single.just(token), IvideonApi().getCameras(accessToken: token))
            })
        let singleResult: Single<[XCameraViewLayerModel]> = singleIV_GET_CAMERAS.map({ (token, cameras) in
            let result: [XCameraViewLayerModel] = cameras.map { XCameraViewLayerMapper.cast(model: $0, accessToken: token) }
            
            return result
        }).subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.mainScheduler)
        
        return singleResult
    }

    private func ivideonCamerasObservable() -> Observable<[XCameraViewLayerModel]> {
        Observable<Any>.create({ observer in
            observer.onNext(true)
            let o = NotificationCenter.default.addObserver(forName: HubNotification.iviAuth, object: nil, queue: nil) { _ in observer.onNext(true) }
            return Disposables.create { NotificationCenter.default.removeObserver(o) }
        }).concatMap({ _ in self.ivideonCamerasSingle().asObservable() })
    }
        
    private func rtspCamerasObservable() -> Observable<(content: XCameraViewLayerModel, state: XRxModelState)> {
        rtspCameraDataLayer.items()
            .map({ model in return (content: XCameraViewLayerMapper.cast(model: model), state: model.state) })
            .observeOn(ThreadUtil.shared.mainScheduler)
    }
    
    private func initObservers() {
        ivideonCamerasDisposable?.dispose()
        ivideonCamerasDisposable = ivideonCamerasObservable().subscribe(onNext: ivideonCameraViewLayer.ivideonCameras, onError: showErrorAlert)

        rtspCamerasDisposable?.dispose()
        rtspCamerasDisposable = rtspCamerasObservable().subscribe(onNext: rtspCameraViewLayer.rtspCamera)
    }
}

extension XCamerasMainController: XCamerasMainViewDelegate {
    func showAddViewTapped() {
        showAddMenuController()
    }
    
    func cameraItemViewTapped(item: XCameraViewLayerModel) {
        let controller = XCameraPreviewController(model: item)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showErrorAlert(error: Error) {
        showErrorColtroller(message: error.localizedDescription)
    }
    
    func showAddMenuController() {
        let controller = XAddMenuController()
        controller.startAction = .camera
        navigationController?.pushViewController(controller, animated: true)
    }
}
